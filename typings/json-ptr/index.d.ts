declare module 'json-ptr' {
  export class JsonPointer {
    /**
     *
     * @param ptr
     * @return
     */
    public static create(ptr : any): JsonPointer

    /**
     *
     * @param target
     * @param fragmentId
     * @return
     */
    public static list(target : any, fragmentId : any): any

    /**
     *
     * @param target
     * @param fragmentId
     * @return
     */
    public static flatten(target : any, fragmentId : any): any

    /**
     *
     * @param target
     * @param fragmentId
     * @return
     */
    public static map(target : any, fragmentId : any): any

    /**
     *
     * @param target
     * @param visitor
     * @param cycle
     */
    public static visit(target: any, visitor: any, cycle: any): void

    /**
     *
     * @param ptr
     * @return
     */
    public static decodePointer(ptr : any[] | string): any[]

    /**
     *
     * @param path
     * @return
     */
    public static encodePointer(path : any[] | string): string

    /**
     *
     * @param ptr
     * @return
     */
    public static decodeUriFragmentIdentifier(ptr : any[] | string): any

    /**
     *
     * @param path
     * @return
     */
    public static encodeUriFragmentIdentifier(path : any[] | string): string

    public pointer: JsonPointer

    /**
     *
     * @param target
     * @return
     */
    public has(target : any): boolean

    /**
     *
     */
    public get(obj: any): void

    /**
     *
     * @param target
     * @param value
     * @param force
     * @return
     */
    public set(target : any, value : any, force? : any): any

    /**
     *
     * @param ptr
     * @return
     */
    public decode(ptr : any): any
  }

  export class JsonReference {
    public constructor (pointer : JsonPointer)

    public $ref : string

    public resolve(target : any): any

    public toString(): string

    public isReference(obj : any): any

    public noConflict(): (ptr : any[] | string) => void

    public pointer : string

    public uriFragmentIdentifier : string

    public toString(): string
  }
}
