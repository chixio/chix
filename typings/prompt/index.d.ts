// Type definitions for ../../node_modules/prompt/lib/prompt.js
// Project: [LIBRARY_URL_HERE]
// Definitions by: [YOUR_NAME_HERE] <[YOUR_URL_HERE]>
// Definitions: https://github.com/borisyankov/DefinitelyTyped
// history.<i>

declare module 'prompt' {

  /**
   *
   */
  export interface I {

    /**
     *
     */
    property: {

      /**
       *
       */
      schema: {

        /**
         *
         */
        properties: {}

        /**
         *
         */
        required: boolean;
      }

      /**
       *
       */
      required: boolean;
    }
  }

  export namespace prompt {
    // prompt.get.!0
    type Get0 = any[]
  }
  namespace prompt {
    // prompt.get.!1
    type Get1 = ((err: any, results: prompt.get.Get11) => void)
  }
  namespace prompt.get {
    // prompt.get.!1.!1

    /**
     *
     */
    interface Get11 {
    }
  }
  namespace prompt {
    // prompt.getInput.!1
    type GetInput1 = ((err: any, line: any) => void)
  }
  namespace prompt {
    // prompt._performValidation.!2

    /**
     *
     */
    interface _performValidation2 {
    }
  }

  /**
   *
   */
  var history: any[]

  /**
   *
   */
  export class prompt {

    /**
     *
     */
    public started: boolean

    /**
     *
     */
    public paused: boolean

    /**
     *
     */
    public stopped: boolean

    /**
     *
     */
    public allowEmpty: boolean

    /**
     *
     */
    public message: string

    /**
     *
     */
    public delimiter: string

    /**
     *
     */
    public colors: boolean

    /**
     * By default: Remember the last `10` prompt property /
     * answer pairs and don't allow empty responses globally.
     */
    public memory: number

    /**
     * ### function start (options)
     * #### @options {Object} **Optional** Options to consume by prompt
     * Starts the prompt by listening to the appropriate events on `options.stdin`
     * and `options.stdout`. If no streams are supplied, then `process.stdin`
     * and `process.stdout` are used, respectively.
     * @param options
     * @return
     */
    public start (options?: any): /* prompt */ any

    /**
     * ### function pause ()
     * Pauses input coming in from stdin
     * @return
     */
    public pause (): /* prompt */ any

    /**
     * ### function stop ()
     * Stops input coming in from stdin
     * @return
     */
    public stop (): /* prompt */ any

    /**
     * ### function resume ()
     * Resumes input coming in from stdin
     * @return
     */
    public resume (): /* prompt */ any

    /**
     * ### function history (search)
     * #### @search {Number|string} Index or property name to find.
     * Returns the `property:value` pair from within the prompts
     * `history` array.
     * @param search
     * @return
     */
    public history (search: any): /* history.<i> */ any

    /**
     * ### function get (schema, callback)
     * #### @schema {Array|Object|string} Set of variables to get input for.
     * #### @callback {function} Continuation to pass control to when complete.
     * Gets input from the user via stdin for the specified message(s) `msg`.
     * @param schema
     * @param callback
     * @return
     */
    public get (schema: any, callback: (error: Error, result: any) => void): /* prompt */ any

    /**
     * }
     */
    public confirm (): void

    /**
     * ### function getInput (prop, callback)
     * #### @prop {Object|string} Variable to get input for.
     * #### @callback {function} Continuation to pass control to when complete.
     * Gets input from the user via stdin for the specified message `msg`.
     * @param prop
     * @param callback
     * @return
     */
    public getInput (prop: /* history.<i>.property */ any, callback: prompt.GetInput1): /* error */ any

    /**
     * ### function performValidation (name, prop, against, schema, line, callback)
     * #### @name {Object} Variable name
     * #### @prop {Object|string} Variable to get input for.
     * #### @against {Object} Input
     * #### @schema {Object} Validation schema
     * #### @line {String|Boolean} Input line
     * #### @callback {function} Continuation to pass control to when complete.
     * Perfoms user input validation, print errors if needed and returns value according to validation
     * @param name
     * @param prop
     * @param against
     * @param schema
     * @param line
     * @param callback
     * @return
     */
    public _performValidation (name: /* history.<i>.property */ any, prop: /* history.<i>.property */ any, against: /* prompt._performValidation2 */ any, schema: /* history.<i>.property */ any, line: any, callback: () => void): boolean

    /**
     * ### function addProperties (obj, properties, callback)
     * #### @obj {Object} Object to add properties to
     * #### @properties {Array} List of properties to get values for
     * #### @callback {function} Continuation to pass control to when complete.
     * Prompts the user for values each of the `properties` if `obj` does not already
     * have a value for the property. Responds with the modified object.
     * @param obj
     * @param properties
     * @param callback
     * @return
     */
    public addProperties (obj: any, properties: any, callback: any): /* prompt */ any

    /**
     * ### @private function _remember (property, value)
     * #### @property {Object|string} Property that the value is in response to.
     * #### @value {string} User input captured by `prompt`.
     * Prepends the `property:value` pair into the private `history` Array
     * for `prompt` so that it can be accessed later.
     * @param property
     * @param value
     */
    public _remember (property: /* history.<i>.property */ any, value: any): void
  }

  /**
   * Make sure `tmp` is emptied
   */
  var tmp: any[]

  /**
   * ### @private function convert (schema)
   * #### @schema {Object} Schema for a property
   * Converts the schema into new format if it is in old format
   * @param schema
   * @return
   */
  export function convert (schema: /* history.<i>.property */ any): /* history.<i>.property */ any
}
