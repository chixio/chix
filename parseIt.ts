import {TypescriptParser} from 'typescript-parser'

const parser = new TypescriptParser()

// either:
// const parsed = await parser.parseSource(/* typescript source code as string */);

// or a filepath

;(async () => {
  const parsed = await parser.parseFile(
    'packages/chix-flow/src/link.ts',
    'workspace root'
  )

  console.log(JSON.stringify(parsed, null, 2))
})()
