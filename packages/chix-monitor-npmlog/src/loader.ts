import {ILogger} from '@chix/common'
import {
  ILoader,
  LoadCacheEvent,
  LoadFileEvent,
  LoadUrlEvent,
  PurgeCacheEvent,
  WriteCacheEvent,
} from '@chix/loader'

export function NpmLogLoaderMonitor(Logger: ILogger, loader: ILoader) {
  loader.on('loadUrl', (event: LoadUrlEvent) => {
    Logger.info('loadUrl', event.url)
  })

  loader.on('loadFile', (event: LoadFileEvent) => {
    Logger.info('loadFile', event.path)
  })

  loader.on('loadCache', (event: LoadCacheEvent) => {
    Logger.debug('cache', 'loaded cache file %s', event.file)
  })

  loader.on('purgeCache', (event: PurgeCacheEvent) => {
    Logger.debug('cache', 'purged cache file %s', event.file)
  })

  loader.on('writeCache', (event: WriteCacheEvent) => {
    Logger.debug('cache', 'wrote cache file %s', event.file)
  })

  return Logger
}
