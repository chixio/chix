import {ILogger} from '@chix/common'
import {Actor} from '@chix/flow'

export function NpmLogActorMonitor(Logger: ILogger, actor: Actor) {
  const io = actor.ioHandler

  actor.on('removeLink', (event: any) => {
    Logger.debug(
      event.node ? event.node.identifier : 'Some Actor',
      'removed link'
    )
  })

  io.on('output', (data: any) => {
    switch (data.port) {
      case ':plug':
        Logger.debug(
          data.node.identifier,
          'port %s plugged (%d)',
          data.out.read().port,
          data.out.read().connections
        )
        break

      case ':unplug':
        Logger.debug(
          data.node.identifier,
          'port %s unplugged (%d)',
          data.out.read().port,
          data.out.read().connections
        )
        break

      case ':portFill':
        Logger.info(
          data.node.identifier,
          'port %s filled with data',
          data.out.read().port
        )
        break

      case ':contextUpdate':
        Logger.info(
          data.node.identifier,
          'port %s filled with context',
          data.out.read().port
        )
        break

      case ':inputValidated':
        Logger.debug(data.node.identifier, 'input validated')
        break

      case ':start':
        Logger.info(data.node.identifier, 'START')
        break

      case ':freePort':
        Logger.debug(data.node.identifier, 'free port %s', data.out.read().port)
        break

      /*
             case ':queue':
               Logger.debug(
                 data.node,
                 'queue: %s',
                 data.port
               )
             break
      */

      case ':openPort':
        Logger.info(
          data.node.identifier,
          'opened port %s (%d)',
          data.out.read().port,
          data.out.read().connections
        )
        break

      case ':closePort':
        Logger.info(
          data.node.identifier,
          'closed port %s',
          data.out.read().port
        )
        break

      case ':index':
        Logger.info(
          data.node.identifier,
          '[%s] set on port `%s`',
          data.out.read().index,
          data.out.read().port
        )
        break

      case ':nodeComplete':
        // console.log('nodeComplete', data)
        Logger.info(data.node.identifier, 'completed')
        break

      case ':portReject':
        Logger.debug(
          data.node.identifier,
          'rejected input on port %s',
          data.out.read().port
        )
        break

      case ':inputRequired':
        Logger.error(
          data.node.identifier,
          'input required on port %s',
          data.out.read().port
        )
        break

      case ':error':
        Logger.error(data.node.identifier, data.out.read().msg)
        break

      case ':nodeTimeout':
        Logger.error(data.node.identifier, 'node timeout')
        break

      case ':executed':
        Logger.info(data.node.identifier, 'EXECUTED')
        break

      case ':inputTimeout':
        Logger.info(
          data.node.identifier,
          'input timeout, got %s need %s',
          Object.keys(data.node.input).join(', '),
          data.node.openPorts.join(', ')
        )
        break

      default:
        Logger.info(data.node.identifier, 'output on port %s', data.port)
        break
    }
  })

  return Logger
}
