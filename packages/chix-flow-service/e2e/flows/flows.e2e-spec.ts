import {FlowModel} from '@chix/common'
// tslint:disable-next-line:no-implicit-dependencies
import {expect} from 'chai'
// tslint:disable-next-line:no-implicit-dependencies
import * as request from 'supertest'
import {email as emailFlow} from '../../src/modules/flows/__mocks__/email'
import {ProjectService} from '../../src/modules/projects'
import {createTestApp, TestContext} from '../../src/support/createTestApp'

describe('Flows', () => {
  let flowModel: FlowModel
  let testApp: TestContext
  let projectService: ProjectService

  before(async () => {
    testApp = await createTestApp('flows')

    projectService = testApp.app.get(ProjectService)

    // Flushes *all* projects.
    await projectService.flush(
      {
        repoManager: testApp.repoManager,
      },
      {}
    )
  })

  after(async () => {
    await projectService.flush(
      {
        repoManager: testApp.repoManager,
      },
      {}
    )

    await testApp.app.close()
  })

  describe('With non-existant project', () => {
    it(`/POST user cannot create flow`, () => {
      return request(testApp.server)
        .post(`/flows/${testApp.user.name}`)
        .send(emailFlow)
        .set('Authorization', testApp.user.authorizationHeader)
        .expect(422)
    })

    it(`/PUT user cannot PUT flow`, () => {
      return request(testApp.server)
        .put(`/flows/${testApp.user.name}`)
        .set('Authorization', testApp.user.authorizationHeader)
        .send({
          ...emailFlow,
          name: 'flow2',
        })
        .expect(422)
    })
  })

  describe('With existing project', () => {
    before(async () => {
      await projectService.create(
        testApp.user,
        {
          name: 'mail',
          description: 'test project',
        },
        {
          repoManager: testApp.repoManager,
        }
      )
    })

    after(async () => {})

    it(`/POST user can create flow`, () => {
      return request(testApp.server)
        .post(`/flows/${testApp.user.name}`)
        .send(emailFlow)
        .set('Authorization', testApp.user.authorizationHeader)
        .expect(201)
        .then(({body: flow}) => {
          expect(flow.id).to.be.a('string')
          expect(flow.ns).to.eql('mail')
          expect(flow.name).to.eql('service')
          flowModel = flow
        })
    })

    it(`/PUT user can create flow using PUT`, () => {
      return request(testApp.server)
        .put(`/flows/${testApp.user.name}`)
        .send({
          ...emailFlow,
          name: 'flow2',
        })
        .set('Authorization', testApp.user.authorizationHeader)
        .expect(200)
        .then(({body: flow}) => {
          expect(flow.id).to.be.a('string')
          expect(flow.ns).to.eql('mail')
          expect(flow.name).to.eql('flow2')
        })
    })

    it(`/HEAD user can test whether provider exists`, () => {
      return request(testApp.server)
        .head(`/flows/${testApp.user.name}`)
        .set('Authorization', testApp.user.authorizationHeader)
        .expect(200)
    })

    it(`/HEAD user can test whether namespace exists`, () => {
      return request(testApp.server)
        .head(`/flows/${testApp.user.name}/${emailFlow.ns}`)
        .set('Authorization', testApp.user.authorizationHeader)
        .expect(200)
    })

    it(`/HEAD user can test whether namespace not exists`, () => {
      return request(testApp.server)
        .head(`/flows/${testApp.user.name}/no`)
        .set('Authorization', testApp.user.authorizationHeader)
        .expect(404)
    })

    it(`/HEAD user can test whether flow exists`, () => {
      return request(testApp.server)
        .head(`/flows/${testApp.user.name}/${emailFlow.ns}/${emailFlow.name}`)
        .set('Authorization', testApp.user.authorizationHeader)
        .expect(200)
    })

    it(`/HEAD user can test whether flow not exists`, () => {
      return request(testApp.server)
        .head(`/flows/${testApp.user.name}/no/way`)
        .set('Authorization', testApp.user.authorizationHeader)
        .expect(404)
    })

    it(`/GET Other Authorized user can list all flows`, () => {
      return request(testApp.server)
        .get('/flows')
        .set('Authorization', testApp.user2.authorizationHeader)
        .expect(200)
        .then(({body: flows}) => {
          expect(flows.length).to.equal(2)
        })
    })

    it(`/GET Other Authorized user can search all flows`, () => {
      return request(testApp.server)
        .get('/flows/search')
        .set('Authorization', testApp.user2.authorizationHeader)
        .expect(200)
    })

    it(`/GET user can list own flows`, () => {
      return request(testApp.server)
        .get(`/flows/${testApp.user.name}`)
        .set('Authorization', testApp.user.authorizationHeader)
        .expect(200)
        .then(({body: flows}) => {
          expect(flows.length).to.eql(2)
        })
    })

    it(`/GET user can retrieve own flows from namespace`, () => {
      return request(testApp.server)
        .get(`/flows/${testApp.user.name}/${emailFlow.ns}`)
        .set('Authorization', testApp.user.authorizationHeader)
        .expect(200)
        .then(({body: flows}) => {
          expect(flows.length).to.eql(2)
        })
    })

    it(`/GET user can retrieve own flow by namespace and name`, () => {
      return request(testApp.server)
        .get(`/flows/${testApp.user.name}/${emailFlow.ns}/${emailFlow.name}`)
        .set('Authorization', testApp.user.authorizationHeader)
        .expect(200)
        .then(({body}) => {
          expect(body.ns).to.eql(emailFlow.ns)
          expect(body.name).to.eql(emailFlow.name)
        })
    })

    it(`/PUT user can update own flow using ns and name`, () => {
      return request(testApp.server)
        .put(`/flows/${testApp.user.name}`)
        .send({
          ...emailFlow,
          description: 'Flow was updated.',
        })
        .set('Authorization', testApp.user.authorizationHeader)
        .expect(200)
        .then(({body: flow}) => {
          expect(flow.description).to.eql('Flow was updated.')
        })
    })

    it(`/PUT user can update own flow with id`, () => {
      return request(testApp.server)
        .put(`/flows/${testApp.user.name}`)
        .send({
          ...flowModel,
          name: 'new-flow-name',
        })
        .set('Authorization', testApp.user.authorizationHeader)
        .expect(200)
        .then(({body: flow}) => {
          expect(flow.name).to.eql('new-flow-name')
        })
    })

    it(`/GET user can remove own flow`, () => {
      return request(testApp.server)
        .delete(`/flows/${testApp.user.name}/${emailFlow.ns}/new-flow-name`)
        .set('Authorization', testApp.user.authorizationHeader)
        .expect(204)
    })
  })
})
