import {ProjectModel} from '@chix/common'
// tslint:disable-next-line:no-implicit-dependencies
import {expect} from 'chai'
// tslint:disable-next-line:no-implicit-dependencies
import * as request from 'supertest'
import {ProjectService} from '../../src/modules/projects'
import {createTestApp, TestContext} from '../../src/support/createTestApp'

const project = {
  name: 'mongodb',
  description: 'MongoDB Project',
}

const nodes = [
  {
    ns: 'mongodb',
    name: 'test',
    ports: {
      input: {
        in: {
          type: 'string',
        },
      },
      output: {
        out: {
          type: 'string',
        },
      },
    },
  },
  {
    ns: 'mongodb',
    name: 'test2',
    ports: {
      input: {
        in: {
          type: 'string',
        },
      },
      output: {
        out: {
          type: 'string',
        },
      },
    },
  },
]

describe('Projects', () => {
  let projectModel: ProjectModel
  let testApp: TestContext
  let projectService: ProjectService

  before(async () => {
    testApp = await createTestApp('projects')

    projectService = testApp.app.get(ProjectService)

    // Flushes *all* projects.
    await projectService.flush(
      {
        repoManager: testApp.repoManager,
      },
      {}
    )
  })

  after(async () => {
    // Flushes *all* projects.
    await projectService.flush(
      {
        repoManager: testApp.repoManager,
      },
      {}
    )

    await testApp.app.close()
  })

  describe('Projects Controller', () => {
    it(`/POST user can create project`, () => {
      expect(nodes.length).to.equal(2)
      return request(testApp.server)
        .post(`/projects/${testApp.user.name}`)
        .send(project)
        .set('Authorization', testApp.user.authorizationHeader)
        .expect(201)
        .then(async ({body}) => {
          expect(body.name).to.equal('mongodb')
          expect(body.description).to.equal('MongoDB Project')
          projectModel = body
        })
    })

    it(`/PUT user can create project using put`, () => {
      return request(testApp.server)
        .put(`/projects/${testApp.user.name}`)
        .send({
          ...project,
          name: 'other-project',
        })
        .set('Authorization', testApp.user.authorizationHeader)
        .expect(200)
        .then(({body}) => {
          expect(body.name).to.eql('other-project')
        })
    })

    it(`/GET Other Authorized user can list all projects`, () => {
      return request(testApp.server)
        .get('/projects')
        .set('Authorization', testApp.user2.authorizationHeader)
        .expect(200)
        .then(({body}) => {
          expect(body.length).to.eql(2)
        })
    })

    it(`/GET Other Authorized user can search all projects`, () => {
      return request(testApp.server)
        .get('/projects/search?q=')
        .set('Authorization', testApp.user2.authorizationHeader)
        .expect(200)
    })

    it(`/GET user retrieve own project`, () => {
      return request(testApp.server)
        .get(`/projects/${testApp.user.name}/${project.name}`)
        .set('Authorization', testApp.user.authorizationHeader)
        .expect(200)
    })

    it(`/GET user can list own projects`, () => {
      return request(testApp.server)
        .get(`/projects/${testApp.user.name}`)
        .set('Authorization', testApp.user.authorizationHeader)
        .expect(200)
        .then(result => {
          expect(result.body.length).to.equal(2)
        })
    })

    it(`/PUT user can update own project using name only`, () => {
      return request(testApp.server)
        .put(`/projects/${testApp.user.name}`)
        .send({
          ...projectModel,
          description: 'Updated project.',
        })
        .set('Authorization', testApp.user.authorizationHeader)
        .expect(200)
    })

    it(`/PUT user can update own project using id`, () => {
      const payload = {
        ...projectModel,
        name: 'new-project-name',
      }
      return request(testApp.server)
        .put(`/projects/${testApp.user.name}`)
        .send(payload)
        .set('Authorization', testApp.user.authorizationHeader)
        .expect(200)
    })

    it(`/DELETE user can remove own project`, () => {
      return request(testApp.server)
        .delete(`/projects/${testApp.user.name}/new-project-name`)
        .set('Authorization', testApp.user.authorizationHeader)
        .expect(204)
    })

    it(`/DELETE user can remove other project`, () => {
      return request(testApp.server)
        .delete(`/projects/${testApp.user.name}/other-project`)
        .set('Authorization', testApp.user.authorizationHeader)
        .expect(204)
    })

    it(`/POST user can create own project with nodes payload`, () => {
      const payload = {
        ...projectModel,
        name: 'mongodb',
        nodes,
      }
      return request(testApp.server)
        .post(`/projects/${testApp.user.name}`)
        .send(payload)
        .set('Authorization', testApp.user.authorizationHeader)
        .expect(201)
        .then(async () => {
          const _nodes = await testApp.mongoAdapter.find('nodes', {
            ns: 'mongodb',
          })

          expect(_nodes.length).to.equal(2)
        })
    })

    // Ensure repositories are closed when a request is finished
    it(`/PUT user can update own project with nodes payload`, () => {
      const payload = {
        ...projectModel,
        name: 'mongodb',
        nodes,
      }
      return request(testApp.server)
        .put(`/projects/${testApp.user.name}`)
        .send(payload)
        .set('Authorization', testApp.user.authorizationHeader)
        .expect(200)
    })
  })
})
