import {NodeModel} from '@chix/common'
// tslint:disable-next-line:no-implicit-dependencies
import {assert, expect} from 'chai'
// tslint:disable-next-line:no-implicit-dependencies
import * as request from 'supertest'
import {node as nodeMock} from '../../src/modules/nodes/__mocks__/node'
import {ProjectService} from '../../src/modules/projects'
import {createTestApp, TestContext} from '../../src/support/createTestApp'

describe('Nodes', () => {
  let nodeModel: NodeModel
  let testApp: TestContext
  let projectService: ProjectService

  before(async () => {
    testApp = await createTestApp('nodes')

    projectService = testApp.app.get<ProjectService>(ProjectService)

    // Flushes *all* projects.
    await projectService.flush(
      {
        repoManager: testApp.repoManager,
      },
      {}
    )
  })

  after(async () => {
    await testApp.app.close()

    await projectService.flush(
      {
        repoManager: testApp.repoManager,
      },
      {}
    )
  })

  describe('With non-existant project', () => {
    it(`/POST user cannot create node`, () => {
      return request(testApp.server)
        .post(`/nodes/${testApp.user.name}`)
        .send(nodeMock)
        .set('Authorization', testApp.user.authorizationHeader)
        .expect(422)
    })

    it(`/PUT user cannot PUT node`, () => {
      return request(testApp.server)
        .put(`/nodes/${testApp.user.name}`)
        .send({
          ...nodeMock,
          ns: 'different',
          name: 'node',
        })
        .set('Authorization', testApp.user.authorizationHeader)
        .expect(422)
    })
  })

  describe('With existing project', () => {
    before(async () => {
      projectService = testApp.app.get(ProjectService)

      await projectService.create(
        testApp.user,
        {
          name: 'console',
          description: 'console test project',
        },
        {
          repoManager: testApp.repoManager,
        }
      )

      await projectService.create(
        testApp.user,
        {
          name: 'different',
          description: 'different test project',
        },
        {
          repoManager: testApp.repoManager,
        }
      )
    })

    it(`/POST user can create node`, () => {
      return request(testApp.server)
        .post(`/nodes/${testApp.user.name}`)
        .send(nodeMock)
        .set('Authorization', testApp.user.authorizationHeader)
        .expect(201)
        .then(({body: node}) => {
          expect(node.ns).to.equal('console')
          expect(node.name).to.equal('log')
          assert.isNotOk(node._id)
          assert.isNotOk(node.providerId)
          nodeModel = node
        })
    })

    it(`/PUT user can create node using PUT`, () => {
      return request(testApp.server)
        .put(`/nodes/${testApp.user.name}`)
        .send({
          ...nodeMock,
          ns: 'different',
          name: 'node',
        })
        .set('Authorization', testApp.user.authorizationHeader)
        .expect(200)
        .then(({body: node}) => {
          expect(node.ns).to.equal('different')
          expect(node.name).to.equal('node')
          assert.isNotOk(node._id)
          assert.isNotOk(node.providerId)
        })
    })

    it(`/GET Other Authorized user can list all nodes`, () => {
      return request(testApp.server)
        .get('/nodes')
        .set('Authorization', testApp.user2.authorizationHeader)
        .expect(200)
        .then(({body: nodes}) => {
          expect(nodes.length).to.equal(2)
          assert.isNotOk(nodes[0]._id)
          assert.isNotOk(nodes[0].providerId)

          expect(nodes[0].provider).to.eql({
            id: testApp.user.id,
            name: testApp.user.name,
          })
        })
    })

    it(`/GET Other Authorized user can list all nodes from specific provider`, () => {
      return request(testApp.server)
        .get(`/nodes/${testApp.user.name}/${nodeMock.ns}`)
        .set('Authorization', testApp.user2.authorizationHeader)
        .expect(200)
        .then(({body}) => {
          expect(body.length).to.equal(1)
          expect(body[0].ns).to.equal(nodeMock.ns)
          expect(body[0].name).to.equal(nodeMock.name)
          assert.isNotOk(body[0]._id)
          assert.isNotOk(body[0].providerId)

          expect(body[0].provider).to.eql({
            id: testApp.user.id,
            name: testApp.user.name,
          })
        })
    })

    it(`/GET Other Authorized user can list all nodes from specific provider and use paging`, () => {
      return request(testApp.server)
        .get(`/nodes/${testApp.user.name}/${nodeMock.ns}?page=1&per_page=12`)
        .set('Authorization', testApp.user2.authorizationHeader)
        .expect(200)
        .then(({body}) => {
          expect(body.length).to.equal(1)
          expect(body[0].ns).to.equal(nodeMock.ns)
          expect(body[0].name).to.equal(nodeMock.name)
          assert.isNotOk(body[0]._id)
          assert.isNotOk(body[0].providerId)

          expect(body[0].provider).to.eql({
            id: testApp.user.id,
            name: testApp.user.name,
          })
        })
    })

    it(`/GET Other Authorized user can search all nodes`, () => {
      return request(testApp.server)
        .get('/nodes/search')
        .set('Authorization', testApp.user2.authorizationHeader)
        .expect(200)
    })

    it(`/GET user retrieve own node`, () => {
      return request(testApp.server)
        .get(`/nodes/${testApp.user.name}/${nodeMock.ns}/${nodeMock.name}`)
        .set('Authorization', testApp.user.authorizationHeader)
        .expect(200)
        .then(({body}) => {
          expect(body.name).to.equal(nodeMock.name)
          expect(body.ns).to.equal(nodeMock.ns)
          assert.isNotOk(body._id)
          assert.isNotOk(body.providerId)
          assert.isOk(body.provider)

          expect(body.provider).to.eql({
            id: testApp.user.id,
            name: testApp.user.name,
          })
        })
    })

    it(`/GET user can list all nodes from ns`, () => {
      return request(testApp.server)
        .get(`/nodes/${testApp.user.name}/${nodeMock.ns}`)
        .set('Authorization', testApp.user.authorizationHeader)
        .expect(200)
        .then(({body}) => {
          expect(body.length).to.equal(1)
          expect(body[0].ns).to.equal(nodeMock.ns)
          expect(body[0].name).to.equal(nodeMock.name)
          assert.isNotOk(body[0]._id)
          assert.isNotOk(body[0].providerId)

          expect(body[0].provider).to.eql({
            id: testApp.user.id,
            name: testApp.user.name,
          })
        })
    })

    it(`/GET user can list all nodes from ns and use paging`, () => {
      return request(testApp.server)
        .get(`/nodes/${testApp.user.name}/${nodeMock.ns}?page=1&per_page=12`)
        .set('Authorization', testApp.user.authorizationHeader)
        .expect(200)
        .then(({body}) => {
          expect(body.length).to.equal(1)
          expect(body[0].ns).to.equal(nodeMock.ns)
          expect(body[0].name).to.equal(nodeMock.name)
          assert.isNotOk(body[0]._id)
          assert.isNotOk(body[0].providerId)

          expect(body[0].provider).to.eql({
            id: testApp.user.id,
            name: testApp.user.name,
          })
        })
    })

    it(`/GET user can list own nodes`, () => {
      return request(testApp.server)
        .get(`/nodes/${testApp.user.name}`)
        .set('Authorization', testApp.user.authorizationHeader)
        .expect(200)
        .then(({body}) => {
          expect(body.length).to.equal(2)
          assert.isNotOk(body[0]._id)
          assert.isNotOk(body[0].providerId)

          expect(body[0].provider).to.eql({
            id: testApp.user.id,
            name: testApp.user.name,
          })
        })
    })

    it(`/PUT user can update own node using ns and name`, () => {
      return request(testApp.server)
        .put(`/nodes/${testApp.user.name}`)
        .send({
          ...nodeModel,
          description: 'Node updated.',
        })
        .set('Authorization', testApp.user.authorizationHeader)
        .expect(200)
        .then(({body}) => {
          expect(body.description).to.equal('Node updated.')
          assert.isNotOk(body._id)
          assert.isNotOk(body.providerId)

          expect(body.provider).to.eql({
            id: testApp.user.id,
            name: testApp.user.name,
          })
        })
    })

    it(`/PUT user can update own node using id`, () => {
      return request(testApp.server)
        .put(`/nodes/${testApp.user.name}`)
        .send({
          ...nodeModel,
          name: 'new-node-name',
        })
        .set('Authorization', testApp.user.authorizationHeader)
        .expect(200)
        .then(({body}) => {
          expect(body.name).to.equal('new-node-name')
          assert.isNotOk(body._id)
          assert.isNotOk(body.providerId)

          expect(body.provider).to.eql({
            id: testApp.user.id,
            name: testApp.user.name,
          })
        })
    })

    it(`/GET user can remove own node`, () => {
      return request(testApp.server)
        .delete(`/nodes/${testApp.user.name}/${nodeMock.ns}/new-node-name`)
        .set('Authorization', testApp.user.authorizationHeader)
        .expect(204)
    })
  })
})
