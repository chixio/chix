import { connectDb } from './src/support/typeorm/connectDb'
import * as objenv from 'objenv'
import { environment } from './src/environment/environment.test'
import { Column, Entity, getRepository, PrimaryColumn } from 'typeorm'
// import { User } from './src/gogs/entities/user.entity'

@Entity()
export class User {
  @PrimaryColumn()
  id: number

  @Column()
  name: string
}

(async () => {
  const config = objenv(environment).mysql

  console.log('start connect')
  await connectDb(config, [User])
  console.log('connected.')

  await getRepository(User)
})()
