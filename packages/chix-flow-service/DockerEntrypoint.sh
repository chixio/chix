#!/bin/sh
set -e

if [ ! -f $HOME/.ssh/id_rsa.pub ]
then
  git config --global user.name "Chix Api"
  git config --global user.email "git@chix.io"
  ssh-keygen -t rsa -q -f $HOME/.ssh/id_rsa -N ""
  echo "Public key created:"
  cat $HOME/.ssh/id_rsa.pub
  echo "Trying to AutoAccept hostkey:"
  ssh -o "StrictHostKeyChecking no" -p $GOGS_GIT_PORT "${GOGS_GIT_USER}@${GOGS_GIT_HOST}"
else
  echo "Public key already exists, skipping..."
fi

exec "$@"