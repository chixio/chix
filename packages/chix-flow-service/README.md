# Chiχ Flow Service

## Description

This is the implementation of the Flow Service api.

It consists of the following modules:
* flows
* nodes
* projects
* runtimes

### Flows API

The flows api gives access to the Flows created by a user.
Flows can be seen as code graphs consisting of nodes and subgraphs.

Each flow itself is also considered to be a node which can be used in other graphs.

### Nodes API

The nodes api gives access to the node definitions created by a user.
A node contains a set of input and output ports and a core function which is able
to process a certain tasks. 

### Projects

Each flow and node definition belongs to a certain project and can also be considered as a namespace.
e.g. a Math project contains nodes and flows related to math.

  
## Installation

```bash
$ yarn install
```

## Start

```
$ yarn run start
```

## Testing

The tests must be run on a full test setup environment, no mocking is being done.

## Deployment

In order to deploy the image first run:
 
```
yarn run build
yarn run push
```
The build command will first transpile the code and run the test and then build the docker image from the current source tree.
The push command will push the new image to the docker registry.


