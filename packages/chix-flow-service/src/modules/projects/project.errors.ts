import {HttpStatus} from '@nestjs/common'
import {IErrorMessages} from '@nestling/errors'

export const projectErrors: IErrorMessages = {
  type: 'project',
  errors: [
    {
      code: 'notFound',
      statusCode: HttpStatus.NOT_FOUND,
      message: 'Unable to find the project.',
    },
    {
      code: 'alreadyExists',
      statusCode: HttpStatus.CONFLICT,
      message: 'Project already exists.',
    },
  ],
}
