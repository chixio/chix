import {Node} from '@chix/common'

export function getIdentifier(node: Node) {
  return `${node.ns}:${node.name}`
}
