// tslint:disable-next-line:no-implicit-dependencies
import {assert, expect} from 'chai'

import {Logger} from '@nestjs/common'
import {ContextModel} from '@nestling/context'

import {createTestApp, TestContext} from '../../support/createTestApp'
import {project as projectMock} from './__mocks__/project'
import {ProjectService} from './index'

const context = new ContextModel()

describe('Project Service', () => {
  let projectService: ProjectService
  let testApp: TestContext

  before(async () => {
    testApp = await createTestApp('projects')

    const logger = new Logger()

    logger.log('Test App created')

    projectService = testApp.app.get(ProjectService)

    // Flushes *all* projects.
    await projectService.flush(
      {
        repoManager: testApp.repoManager,
      },
      {}
    )
  })

  after(async () => {
    await projectService.flush(
      {
        repoManager: testApp.repoManager,
      },
      {}
    )

    const projects = await testApp.mongoAdapter.find('projects')
    expect(projects, 'Project cleanup').to.deep.equal([])

    const nodes = await testApp.mongoAdapter.find('nodes')
    expect(nodes, 'Node cleanup').to.deep.equal([])

    const flows = await testApp.mongoAdapter.find('flows')
    expect(flows, 'Flow cleanup').to.deep.equal([])

    if (projectService) {
      await projectService.close()
    }

    await testApp.close()
  })

  it('ensures text index', async () => {
    await projectService.ensureIndex()
  })

  it('creates new project', async () => {
    const _project = await projectService.create(testApp.user, projectMock, {
      repoManager: testApp.repoManager,
    })

    assert.isOk(_project.id)
    expect((_project as any).provider).to.eql({
      id: testApp.user.id,
      name: testApp.user.name,
    })

    delete (_project as any).id
    delete (_project as any).providerId

    expect(_project).to.deep.equal({
      ...projectMock,
      provider: {
        id: testApp.user.id,
        name: testApp.user.name,
      },
    })
  })

  it('finds all projects', async () => {
    const projects = await projectService.find(context)

    assert.isOk(projects.length === 1)
    assert.isOk(projects[0].id)
  })

  it('can find project as part of name', async () => {
    const projects = await projectService.search(context, 'mongojs')

    assert.isOk(projects.length === 1)
    assert.isOk(projects[0].id)
  })

  it('can find project as part of description', async () => {
    const projects = await projectService.search(context, 'MongoJS')

    assert.isOk(projects.length === 1)
    assert.isOk(projects[0].id)
  })

  it('can get project by user and namespace', async () => {
    const project = await projectService.getByUserAndName(
      testApp.user,
      'mongojs'
    )

    assert.isOk(project && project.id)

    expect((project as any).provider).to.eql({
      id: testApp.user.id,
      name: testApp.user.name,
    })

    expect((project as any).name).to.equal('mongojs')
  })

  it('updates project', async () => {
    const project = (await projectService.getByUserAndName(
      testApp.user,
      'mongojs'
    )) as any

    const updated = await projectService.update(
      testApp.user,
      {
        ...project,
        name: 'mongodb',
      },
      {
        repoManager: testApp.repoManager,
      }
    )

    expect(updated.name).to.equal('mongodb')

    const oldNodes = await testApp.mongoAdapter.find('nodes', {
      ns: 'mongojs',
    })

    expect(oldNodes).to.deep.equal([])

    const renamedNodeCount = await testApp.mongoAdapter.count('nodes', {
      ns: 'mongodb',
    })

    expect(renamedNodeCount).to.equal(8)
  })

  it('removes project', async () => {
    const project = (await projectService.getByUserAndName(
      testApp.user,
      'mongodb'
    )) as any

    const removed = await projectService.removeByUserAndName(
      testApp.user,
      project.name,
      {
        repoManager: testApp.repoManager,
      }
    )

    expect(removed).to.equal(true)
  })
})
