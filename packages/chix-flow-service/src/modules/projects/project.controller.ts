import {ProjectDefinition, ProjectModel, UserModel} from '@chix/common'
import {
  Body,
  Controller,
  Delete,
  Get,
  Head,
  HttpCode,
  HttpStatus,
  OnModuleInit,
  Param,
  Post,
  Put,
  Query,
  Res,
} from '@nestjs/common'
import {Context, ContextModel} from '@nestling/context'
import {ErrorMessage} from '@nestling/errors'
import {Response} from 'express'
import {getRepoManager} from '../../git/getRepoManager'
import {Page, Pagination} from '../../paginate'
import {User} from '../../user'
import {UserService} from '../../user/user.service'
import {publicOrOwnFilter} from '../shared'
import {ProjectService} from './project.service'

@Controller('projects')
export class ProjectController implements OnModuleInit {
  constructor(
    private readonly projectService: ProjectService,
    private readonly userService: UserService
  ) {}

  public onModuleInit() {
    console.log('ProjectController, trying to ensure index')
    return this.projectService.ensureIndex()
  }

  /**
   * Returns *all* projects.
   */
  @Get('')
  @HttpCode(HttpStatus.OK)
  public async find(
    @Context() context: ContextModel,
    @User() user: UserModel,
    @Page() page: Pagination
  ): Promise<ProjectDefinition[]> {
    const $or: any = publicOrOwnFilter(user)

    return this.projectService.find(context, {$or}, {...page})
  }

  /**
   * Search for projects.
   */
  @Get('search')
  @HttpCode(HttpStatus.OK)
  public async search(
    @Context() context: ContextModel,
    @User() user: UserModel,
    @Query('q') q: string,
    @Page() page: Pagination
  ): Promise<ProjectDefinition[]> {
    const $or: any = publicOrOwnFilter(user)

    return this.projectService.search(context, q, {...page}, {$or})
  }

  /**
   * Get a list of all projects for this user.
   *
   * In case these are the user's own projects it will list also include private.
   */
  @Get(':provider')
  @HttpCode(HttpStatus.OK)
  public async findByProvider(
    @Context() context: ContextModel,
    @User() user: UserModel,
    @Param('provider') providerName: string,
    @Page() page: Pagination
  ): Promise<ProjectDefinition[]> {
    if (user && user.name === providerName) {
      return this.projectService.findByUser(context, user, {}, {...page})
    }

    const provider = await this.userService.getUserByName(providerName)

    return this.projectService.findByUser(
      context,
      provider,
      {
        private: {
          $ne: true,
        },
      },
      {
        ...page,
      }
    )
  }

  @Head(':provider/:name')
  @HttpCode(HttpStatus.OK)
  public async providerProjectExists(
    @User() user: UserModel,
    @Param('provider') providerName: string,
    @Param('name') name: string,
    @Res() response: Response
  ) {
    let exists: boolean

    if (user && user.name === providerName) {
      exists = await this.projectService.exists(user, {name})
    } else {
      const provider = await this.userService.getUserByName(providerName)

      exists = await this.projectService.exists(provider, {
        name,
        private: {
          $ne: true,
        },
      })
    }

    if (exists) {
      return response.status(HttpStatus.OK)
    }

    return response.status(HttpStatus.NOT_FOUND)
  }

  /**
   * Retrieve project by username and project name.
   */
  @Get(':provider/:name')
  @HttpCode(HttpStatus.OK)
  public async getByProviderAndName(
    @Param('provider') providerName: string,
    @User() user: UserModel,
    @Param('name') name: string
  ): Promise<ProjectDefinition | null> {
    if (user && user.name === providerName) {
      return this.projectService.getByUserAndName(user, name)
    }

    const provider = await this.userService.getUserByName(providerName)

    return this.projectService.getByUserAndName(provider, name, {
      private: {
        $ne: true,
      },
    })
  }

  /**
   * Create a new project
   */
  @Post(':provider')
  @HttpCode(HttpStatus.CREATED)
  public async create(
    @User({required: true}) user: UserModel,
    @Context() context: ContextModel,
    @Param('provider') provider: string,
    @Body() project: ProjectModel
  ) {
    if (user && user.name === provider) {
      const repoManager = getRepoManager(context, provider)

      const result = await this.projectService.create(user, project, {
        repoManager,
      })

      await repoManager.closeRepository()

      return result
    }

    throw new ErrorMessage('user:invalid')
  }

  /**
   * Update given project for this user.
   *
   * Note: nodes belonging to this project are updated at /nodes/:user
   */
  @Put(':provider')
  @HttpCode(HttpStatus.OK)
  public async update(
    @User({required: true}) user: UserModel,
    @Context() context: ContextModel,
    @Param('provider') provider: string,
    @Body() project: ProjectModel
  ) {
    if (user && user.name === provider) {
      const repoManager = getRepoManager(context, provider)

      let result

      if (Array.isArray(project)) {
        await this.projectService.updateMany(user, project, {
          repoManager,
        })
      } else {
        result = await this.projectService.update(user, project, {
          repoManager,
        })
      }

      await repoManager.closeRepository()

      return result
    }

    throw new ErrorMessage('user:invalid')
  }

  /**
   * Remove project.
   */
  @Delete(':provider/:name')
  @HttpCode(HttpStatus.NO_CONTENT)
  public async remove(
    @User({required: true}) user: UserModel,
    @Context() context: ContextModel,
    @Param('provider') provider: string,
    @Param('name') name: string
  ) {
    if (user && user.name === provider) {
      const repoManager = getRepoManager(context, provider)

      await repoManager.openRepository(name)

      await this.projectService.removeByUserAndName(user, name, {
        repoManager,
      })

      await repoManager.unmountRepository()
    } else {
      throw new ErrorMessage('user:invalid')
    }
  }
}
