import {Module} from '@nestjs/common'

import {ContextModule} from '@nestling/context'
import {ErrorMessage} from '@nestling/errors'

import {DatabaseAdapterModule} from '../../adapter'
import {SharedModule} from '../../shared/shared.module'
import {UserModule} from '../../user/user.module'
import {FlowModule} from '../flows'
import {NodeModule} from '../nodes'
import {ProjectController} from './project.controller'
import {projectErrors} from './project.errors'
import {ProjectService} from './project.service'

ErrorMessage.addErrorMessages(projectErrors)

@Module({
  imports: [
    SharedModule,
    DatabaseAdapterModule,
    UserModule,
    NodeModule,
    FlowModule,
    ContextModule,
  ],
  controllers: [ProjectController],
  providers: [ProjectService],
  exports: [ProjectService],
})
export class ProjectModule {}
