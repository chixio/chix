// tslint:disable-next-line:no-implicit-dependencies
import {assert, expect} from 'chai'
import {FlowService} from './flow.service'

import {ContextModel} from '@nestling/context'
import {Logger} from '@nestjs/common'
import {createTestApp, TestContext} from '../../support/createTestApp'
import {ProjectService} from '../projects'
import {email as emailFlow} from './__mocks__/email'

const context = new ContextModel()

describe('Flow Service', () => {
  let flowService: FlowService
  let testApp: TestContext
  let projectService: ProjectService

  before(async () => {
    testApp = await createTestApp('flows')

    const logger = new Logger()

    logger.log('Test App created')

    projectService = testApp.app.get(ProjectService)

    // Flushes *all* projects.
    await projectService.flush(
      {
        repoManager: testApp.repoManager,
      },
      {}
    )

    await projectService.create(
      testApp.user,
      {
        name: 'mail',
        description: 'test mail project',
      },
      {
        repoManager: testApp.repoManager,
      }
    )

    flowService = testApp.app.get(FlowService)

    await flowService.flush()
  })

  after(async () => {
    await projectService.flush(
      {
        repoManager: testApp.repoManager,
      },
      {}
    )

    if (flowService) {
      await flowService.close()
    }

    await testApp.close()
  })

  it('ensures text index', async () => {
    await flowService.ensureIndex()
  })

  it('creates new flow', async () => {
    const _flow = await flowService.create(testApp.user, emailFlow, {
      repoManager: testApp.repoManager,
    })

    assert.isOk(_flow.id)
    expect(_flow.provider).to.eql({
      id: testApp.user.id,
      name: testApp.user.name,
    })

    delete (_flow as any).providerId

    expect(_flow).to.deep.equal({
      ...emailFlow,
      provider: {
        id: testApp.user.id,
        name: testApp.user.name,
      },
    })
  })

  it('finds all flows', async () => {
    const flows = await flowService.find(context)

    assert.isOk(flows.length === 1)
    assert.isOk(flows[0].id)
  })

  it('finds flow by namespace', async () => {
    const flows = await flowService.findByNamespace(context, 'mail')

    assert.isOk(flows.length === 1)
    assert.isOk(flows[0].id)
  })

  it('finds flow by user and namespace', async () => {
    const flows = await flowService.findByUserNamespace(
      context,
      testApp.user,
      'mail'
    )

    assert.isOk(flows.length === 1)
    assert.isOk(flows[0].id)
  })

  it('finds flow by namespace and name', async () => {
    const flow = await flowService.findByNamespaceAndName('mail', 'service')

    assert.isOk(flow && flow.id)
  })

  it('finds flow by user, namespace and name', async () => {
    const flow = await flowService.findByUserNamespaceAndName(
      testApp.user,
      'mail',
      'service'
    )

    assert.isOk(flow && flow.id)
  })

  it('updates flow', async () => {
    const flow = (await flowService.findByUserNamespaceAndName(
      testApp.user,
      'mail',
      'service'
    )) as any

    assert.isOk(flow && flow.id)

    const updated = await flowService.update(
      testApp.user,
      {
        ...flow,
        name: 'service2',
        title: 'Some Title',
      },
      {
        repoManager: testApp.repoManager,
      }
    )

    expect(updated.ns).to.equal('mail')
    expect(updated.name).to.equal('service2')
    expect(updated.title).to.equal('Some Title')
  })

  it('removes flow', async () => {
    const flow = (await flowService.findByUserNamespaceAndName(
      testApp.user,
      'mail',
      'service2'
    )) as any

    assert.isOk(flow && flow.id)

    const removed = await flowService.removeByUserNamespaceAndName(
      testApp.user,
      flow.ns,
      flow.name,
      {
        repoManager: testApp.repoManager,
      }
    )

    expect(removed).to.equal(true)
  })
})
