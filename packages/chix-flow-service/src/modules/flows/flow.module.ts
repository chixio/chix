import {Module} from '@nestjs/common'
import {ContextModule} from '@nestling/context'
import {ErrorMessage} from '@nestling/errors'
import {LoggerModule} from '@nestling/logger'
import {DatabaseAdapterModule} from '../../adapter'
import {SharedModule} from '../../shared/shared.module'
import {UserModule} from '../../user/user.module'
import {FlowController} from './flow.controller'
import {flowErrors} from './flow.errors'
import {FlowService} from './flow.service'

ErrorMessage.addErrorMessages(flowErrors)

@Module({
  imports: [
    SharedModule,
    LoggerModule,
    DatabaseAdapterModule,
    ContextModule,
    UserModule,
  ],
  controllers: [FlowController],
  providers: [FlowService],
  exports: [FlowService],
})
export class FlowModule {}
