export const email = {
  type: 'flow',
  ns: 'mail',
  name: 'service',
  nodes: [
    {
      id: 'Log',
      title: 'Log',
      ns: 'console',
      name: 'log',
    },
    {
      id: 'Server',
      title: 'Server',
      ns: 'emailjs',
      name: 'server',
    },
    {
      id: 'Email',
      title: 'Email',
      ns: 'emailjs',
      name: 'send',
    },
  ],
  links: [
    {
      source: {
        id: 'Server',
        port: 'server',
      },
      target: {
        id: 'Email',
        port: 'server',
      },
      metadata: {
        title: 'Server server -> server Email',
      },
    },
    {
      source: {
        id: 'Email',
        port: 'error',
      },
      target: {
        id: 'Log',
        port: 'msg',
      },
      metadata: {
        title: 'Email error -> msg Log',
      },
    },
    {
      source: {
        id: 'Email',
        port: 'out',
      },
      target: {
        id: 'Log',
        port: 'msg',
      },
      metadata: {
        title: 'Email out -> msg Log',
      },
    },
  ],
  title: 'Email',
  description: 'Sends an email',
  providers: {
    '@': {
      url: 'https://api.chix.io/nodes/{ns}/{name}',
    },
  },
}
