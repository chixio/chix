import {HttpStatus} from '@nestjs/common'
import {IErrorMessages} from '@nestling/errors'

export const flowErrors: IErrorMessages = {
  type: 'flow',
  errors: [
    {
      code: 'notFound',
      statusCode: HttpStatus.NOT_FOUND,
      message: 'Unable to find flow.',
    },
    {
      code: 'alreadyExists',
      statusCode: HttpStatus.CONFLICT,
      message: 'Flow already exists.',
    },
    {
      code: 'projectNotFound',
      statusCode: HttpStatus.UNPROCESSABLE_ENTITY,
      message: 'Project {provider}/{name} does not exist.',
    },
  ],
}
