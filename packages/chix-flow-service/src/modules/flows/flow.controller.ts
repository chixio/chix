import {Flow as FlowDefinition, FlowModel, UserModel} from '@chix/common'
import {
  Body,
  Controller,
  Delete,
  Get,
  Head,
  HttpCode,
  HttpStatus,
  Param,
  Post,
  Put,
  Query,
  Res,
} from '@nestjs/common'
import {Context, ContextModel} from '@nestling/context'
import {ErrorMessage} from '@nestling/errors'
import {Response} from 'express'
import {getRepoManager} from '../../git/getRepoManager'
import {Page, Pagination} from '../../paginate'
import {SharedService} from '../../shared/shared.service'
import {User} from '../../user'
import {UserService} from '../../user/user.service'
import {publicOrOwnFilter} from '../shared'
import {FlowService} from './flow.service'

@Controller('flows')
export class FlowController {
  constructor(
    private readonly flowService: FlowService,
    private readonly sharedService: SharedService,
    private readonly userService: UserService
  ) {}
  @Get('')
  @HttpCode(HttpStatus.OK)
  public async find(
    @Context() context: ContextModel,
    @User() user: UserModel,
    @Page() page: Pagination
  ): Promise<FlowDefinition[]> {
    const $or: any = publicOrOwnFilter(user)

    return this.flowService.find(
      context,
      {$or},
      {
        ...page,
      }
    )
  }

  @Get('search')
  @HttpCode(HttpStatus.OK)
  public async search(
    @Context() context: ContextModel,
    @User() user: UserModel,
    @Query('q') q: string,
    @Page() page: Pagination
  ): Promise<FlowDefinition[]> {
    const $or: any = publicOrOwnFilter(user)

    return this.flowService.search(context, q, {...page}, {$or})
  }

  @Get(':provider')
  @HttpCode(HttpStatus.OK)
  public async findByUser(
    @Context() context: ContextModel,
    @User() user: UserModel,
    @Param('provider') providerName: string,
    @Page() page: Pagination
  ): Promise<FlowDefinition[]> {
    if (user && user.name === providerName) {
      return this.flowService.findByUser(context, user, {}, {...page})
    }

    const provider = await this.userService.getUserByName(providerName)

    return this.flowService.findByUser(
      context,
      provider,
      {
        private: {
          $ne: true,
        },
      },
      {
        ...page,
      }
    )
  }

  @Head(':provider/:ns')
  public async userNamespaceExists(
    @User() user: UserModel,
    @Param('provider') providerName: string,
    @Param('ns') ns: string,
    @Res() response: Response
  ) {
    let exists

    if (user && user.name === providerName) {
      exists = await this.flowService.exists(user, {ns})
    } else {
      const provider = await this.userService.getUserByName(providerName)

      exists = await this.flowService.exists(provider, {
        ns,
        private: {
          $ne: true,
        },
      })
    }

    if (exists) {
      return response.status(HttpStatus.OK).send()
    }

    return response.status(HttpStatus.NOT_FOUND).send()
  }

  @Get(':provider/:ns')
  @HttpCode(HttpStatus.OK)
  public async findByUserNamespace(
    @Context() context: ContextModel,
    @User() user: UserModel,
    @Page() page: Pagination,
    @Param('provider') providerName: string,
    @Param('ns') ns: string
  ): Promise<FlowDefinition[]> {
    if (user && user.name === providerName) {
      return this.flowService.findByUserNamespace(
        context,
        user,
        ns,
        {},
        {...page}
      )
    }

    const provider = await this.userService.getUserByName(providerName)

    return this.flowService.findByUserNamespace(
      context,
      provider,
      ns,
      {
        private: {
          $ne: true,
        },
      },
      {...page}
    )
  }

  @Head(':provider/:ns/:name')
  public async userNamespaceAndNameExists(
    @User() user: UserModel,
    @Param('provider') providerName: string,
    @Param('ns') ns: string,
    @Param('name') name: string,
    @Res() response: Response
  ) {
    let exists

    if (user && user.name === providerName) {
      exists = await this.flowService.exists(user, {ns, name})
    } else {
      const provider = await this.userService.getUserByName(providerName)

      exists = await this.flowService.exists(provider, {
        ns,
        name,
        private: {
          $ne: true,
        },
      })
    }

    if (exists) {
      return response.status(HttpStatus.OK).send()
    }

    return response.status(HttpStatus.NOT_FOUND).send()
  }

  @Get(':provider/:ns/:name')
  @HttpCode(HttpStatus.OK)
  public async getByUserNamespaceAndName(
    @User() user: UserModel,
    @Param('provider') providerName: string,
    @Param('ns') ns: string,
    @Param('name') name: string
  ): Promise<FlowDefinition | null> {
    if (user && user.name === providerName) {
      return this.flowService.getByUserNamespaceAndName(user, ns, name)
    }

    const provider = await this.userService.getUserByName(providerName)

    return this.flowService.findByUserNamespaceAndName(provider, ns, name, {
      private: {
        $ne: true,
      },
    })
  }

  @Post(':provider')
  @HttpCode(HttpStatus.CREATED)
  public async create(
    @User({required: true}) user: UserModel,
    @Context() context: ContextModel,
    @Param('provider') provider: string,
    @Body() flow: FlowModel
  ) {
    const projectExists = await this.sharedService.projectExists(user, {
      name: flow.ns,
    })

    if (projectExists) {
      if (user && user.name === provider) {
        const repoManager = getRepoManager(context, provider)

        await repoManager.openRepository(flow.ns)

        let result

        if (Array.isArray(flow)) {
          result = await this.flowService.createMany(user, flow, {
            repoManager,
          })
        } else {
          result = await this.flowService.create(user, flow, {
            repoManager,
          })
        }

        await repoManager.closeRepository()

        return result
      }

      throw new ErrorMessage('user:invalid')
    }

    throw new ErrorMessage('flow:projectNotFound', {provider, name: flow.ns})
  }

  @Put(':provider')
  @HttpCode(HttpStatus.OK)
  public async update(
    @User() user: UserModel,
    @Context() context: ContextModel,
    @Param('provider') provider: string,
    @Body() flow: FlowModel
  ) {
    const projectExists = await this.sharedService.projectExists(user, {
      name: flow.ns,
    })

    if (projectExists) {
      if (user && user.name === provider) {
        const repoManager = getRepoManager(context, provider)

        await repoManager.openRepository(flow.ns)

        let result

        if (Array.isArray(flow)) {
          result = await this.flowService.updateMany(user, flow, {
            repoManager,
          })
        } else {
          result = await this.flowService.update(user, flow, {
            repoManager,
          })
        }

        await repoManager.closeRepository()

        return result
      }

      throw new ErrorMessage('user:invalid')
    }

    throw new ErrorMessage('flow:projectNotFound', {provider, name: flow.ns})
  }

  @Delete(':provider/:ns/:name')
  @HttpCode(HttpStatus.NO_CONTENT)
  public async removeByUserNamespaceAndName(
    @User() user: UserModel,
    @Context() context: ContextModel,
    @Param('provider') provider: string,
    @Param('ns') ns: string,
    @Param('name') name: string
  ) {
    if (user && user.name === provider) {
      const repoManager = getRepoManager(context, provider)

      await repoManager.openRepository(ns)

      await this.flowService.removeByUserNamespaceAndName(user, ns, name, {
        repoManager,
      })

      await repoManager.closeRepository()
    } else {
      throw new ErrorMessage('user:invalid')
    }
  }
}
