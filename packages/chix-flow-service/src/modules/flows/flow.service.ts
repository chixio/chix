import {Collection} from 'mongodb'
import * as uuid from 'uuid'

import {Flow as FlowDefinition, FlowModel, UserModel} from '@chix/common'

import {Injectable, Logger} from '@nestjs/common'

import {ContextModel} from '@nestling/context'
import {ErrorMessage} from '@nestling/errors'
import {ValidatorService} from '@nestling/validator'

import {MongoDbAdapter} from '../../adapter/mongo.adapter'
import {Pagination} from '../../paginate'
import {UserService} from '../../user/user.service'
import {RepoManagerContext} from '../nodes'
import {ensureUniqueIds} from './utils/ensureUniqueIds'

export interface WithProviderId {
  providerId?: number
}

@Injectable()
export class FlowService {
  constructor(
    private adapter: MongoDbAdapter,
    private validatorService: ValidatorService,
    private userService: UserService
  ) {
    this.adapter.addDocFilterFor('flows', this.UserFilter.bind(this))
  }

  private log = new Logger(FlowService.name)

  /**
   * Ensures the text search index exists.
   *
   * Used to do text search during the search operation.
   *
   * @returns {Promise<void>}
   */
  public async ensureIndex() {
    const collection: Collection = await this.adapter.db.collection('flows')

    try {
      await collection.createIndex(
        {
          ns: 'text',
          name: 'text',
          description: 'text',
        },
        {
          name: 'text_search_index',
        }
      )

      this.log.log('Created text_search_index.')
    } catch (e) {}
  }

  public async getByNamespaceAndName(
    ns: string,
    name: string
  ): Promise<FlowDefinition> {
    const flow = await this.adapter.findOne<FlowDefinition>('flows', {
      ns,
      name,
    })

    if (!flow) {
      throw new ErrorMessage('flow:notFound')
    }

    if (flow._id) {
      delete flow._id
    }

    return flow
  }

  public async getByUserNamespaceAndName(
    user: UserModel,
    ns: string,
    name: string
  ): Promise<FlowModel | null> {
    const flow = await this.adapter.findOne<FlowModel>('flows', {
      providerId: user.id,
      ns,
      name,
    })

    if (flow && flow._id) {
      delete flow._id
    }

    return flow
  }

  public async getById(id: string): Promise<FlowModel> {
    const flow = await this.adapter.findOne<FlowModel>('flows', {
      id,
    })

    if (!flow) {
      throw new ErrorMessage('flow:notFound')
    }

    if (flow._id) {
      delete flow._id
    }

    return flow
  }

  public async getByUserAndId(user: UserModel, id: string): Promise<FlowModel> {
    const flow = await this.adapter.findOne<FlowModel>('flows', {
      providerId: user.id,
      id,
    })

    if (!flow) {
      throw new ErrorMessage('flow:notFound')
    }

    if (flow._id) {
      delete flow._id
    }

    return flow
  }

  public async find(
    context: ContextModel,
    where = {},
    options?: Pagination
  ): Promise<FlowModel[]> {
    return this.adapter.find<FlowModel>('flows', where, options, context)
  }

  public async search(
    context: ContextModel,
    term: string,
    options?: Pagination,
    where: any = {}
  ): Promise<FlowModel[]> {
    return this.adapter.search<FlowModel>(
      context,
      'flows',
      term,
      options,
      where
    )
  }

  public async findByNamespace(
    context: ContextModel,
    ns: string,
    options?: Pagination
  ): Promise<FlowModel[]> {
    return this.adapter.find<FlowModel>(
      'flows',
      {
        ns,
      },
      options,
      context
    )
  }

  public async findByUser(
    context: ContextModel,
    user: UserModel,
    where: any = {},
    options?: Pagination
  ): Promise<FlowModel[]> {
    return this.adapter.find<FlowModel>(
      'flows',
      {
        providerId: user.id,
        ...where,
      },
      options,
      context
    )
  }

  public async findByUserNamespace(
    context: ContextModel,
    user: UserModel,
    ns: string,
    where: any = {},
    options?: Pagination
  ): Promise<FlowModel[]> {
    return this.adapter.find<FlowModel>(
      'flows',
      {
        providerId: user.id,
        ns,
        ...where,
      },
      options,
      context
    )
  }

  public async findByNamespaceAndName(
    ns: string,
    name: string,
    where: any = {}
  ): Promise<FlowModel | null> {
    const flow = this.adapter.findOne<FlowModel>('flows', {
      ns,
      name,
      ...where,
    })

    if (!flow) {
      throw new ErrorMessage('flow:notFound')
    }

    return flow
  }

  public async findByUserNamespaceAndName(
    user: UserModel,
    ns: string,
    name: string,
    where: any = {}
  ): Promise<FlowModel | null> {
    const flow = this.adapter.findOne<FlowModel>('flows', {
      providerId: user.id,
      ns,
      name,
      ...where,
    })

    if (!flow) {
      throw new ErrorMessage('flow:notFound')
    }

    return flow
  }

  public async create(
    user: UserModel,
    flow: FlowDefinition,
    {repoManager}: RepoManagerContext
  ): Promise<FlowModel> {
    this.log.log(`Creating Flow ${flow.title}`)

    await this.validatorService.validate('flow', flow)

    const found = await this.getByUserNamespaceAndName(user, flow.ns, flow.name)

    if (found) {
      throw new ErrorMessage('flow:alreadyExists')
    }

    if ((flow as any)._isCopy) {
      flow.id = uuid.v4()
      ensureUniqueIds(flow, this.log)
    } else if ((flow as any)._isNew || !flow.id) {
      flow.id = flow.id || uuid.v4()
    }

    delete (flow as any)._isNew
    delete (flow as any)._isCopy

    await repoManager.openRepository(flow.ns)

    await repoManager.putFlow(flow)

    const result = await this.adapter.insert<FlowModel>('flows', {
      providerId: user.id,
      ...flow,
    })
    this.log.log(`Created flow: ${result.title}`)

    if (flow._id) {
      delete flow._id
    }
    if (result._id) {
      delete result._id
    }

    return result
  }

  public async createMany(
    user: UserModel,
    flows: FlowDefinition[],
    context: RepoManagerContext
  ): Promise<FlowModel[]> {
    const promises = flows.map((flow: FlowDefinition) =>
      this.create(user, flow, context)
    )

    return Promise.all<FlowModel>(promises)
  }

  public async update(
    user: UserModel,
    flow: FlowModel,
    {repoManager}: RepoManagerContext
  ): Promise<FlowModel> {
    this.log.log(`saving Flow ${flow.ns}:${flow.name}`)

    await this.validatorService.validate('flow', flow)

    let found

    await repoManager.openRepository(flow.ns)

    if (flow.id) {
      found = await this.getByUserAndId(user, flow.id)

      if (!found) {
        throw new ErrorMessage('flow:notFound')
      }

      if (flow.name !== found.name) {
        this.log.log(
          `Removing flow ${found.ns}:${found.name} as it will be renamed.`
        )
        await repoManager.removeFlow(found.name)
      }
    } else {
      found = await this.getByUserNamespaceAndName(user, flow.ns, flow.name)

      if (!found) {
        flow.providerId = user.id
      }
    }

    await repoManager.putFlow({
      ...flow,
      providerId: undefined,
    } as FlowDefinition)

    let result

    if (found) {
      result = await this.adapter.update<FlowModel>('flows', {
        ...flow,
        id: found.id,
      })
    } else {
      result = await this.adapter.update<FlowModel>('flows', flow)
    }

    this.log.log(`Saved flow ${flow.title}`)

    return result
  }

  public async renameNs(
    user: UserModel,
    oldNs: string,
    newNs: string,
    context: RepoManagerContext
  ): Promise<void> {
    this.log.log(`renaming flow namespace ${oldNs} -> ${newNs}`)

    const col = this.adapter.db.collection('flows')

    await col.updateMany(
      {
        ns: oldNs,
        providerId: user.id,
      },
      {
        $set: {
          ns: newNs,
        },
      }
    )

    const flows = (await col
      .find({ns: newNs})
      .toArray()) as unknown as FlowModel[]

    if (flows.length) {
      await this.updateMany(user, flows, context)
    }
  }

  public async updateMany(
    user: UserModel,
    flows: FlowModel[],
    context: RepoManagerContext
  ): Promise<FlowModel[]> {
    const promises = flows.map((flow) => this.update(user, flow, context))

    return Promise.all<FlowModel>(promises)
  }

  public async remove(
    id: string,
    {repoManager}: RepoManagerContext
  ): Promise<boolean> {
    this.log.log('Removing Flow')

    const flow = await this.adapter.findById<FlowModel>('flows', id)

    if (flow) {
      await repoManager.removeFlow(flow.name)

      return this.adapter.remove('flows', id)
    }

    return false
  }

  public async exists(user: UserModel, where: any) {
    this.log.log(`Test if exists: ${Object.keys(where)}`)

    return this.adapter.exists('flows', {
      providerId: user.id,
      ...where,
    })
  }

  public async removeByNamespaceAndName(
    ns: string,
    name: string,
    {repoManager}: RepoManagerContext
  ): Promise<boolean> {
    this.log.log(`Removing Flow by namespace and name: ${ns}:${name}`)

    const flow = await this.adapter.findOne<FlowDefinition>('flows', {ns, name})

    if (flow && flow.id) {
      await repoManager.openRepository(flow.ns)
      await repoManager.removeFlow(flow.name)

      return this.adapter.remove('flows', flow.id)
    }

    return false
  }

  public async removeByUserNamespace(
    user: UserModel,
    ns: string,
    {repoManager}: RepoManagerContext
  ): Promise<boolean> {
    this.log.log(`Removing Flows by namespace: ${ns}`)

    const flows = await this.adapter.find<FlowModel>('flows', {
      providerId: user.id,
      ns,
    })

    if (flows.length) {
      const names = flows.map((flow) => flow.name)

      await repoManager.openRepository(ns)
      await repoManager.removeFlows(names)

      return this.adapter.removeAll('flows', {
        ns,
        providerId: user.id,
      })
    }

    return false
  }

  public async removeByUserNamespaceAndName(
    user: UserModel,
    ns: string,
    name: string,
    {repoManager}: RepoManagerContext
  ): Promise<boolean> {
    this.log.log(`Removing Flow by namespace and name: ${ns}:${name}`)

    const flow = await this.adapter.findOne<FlowDefinition>('flows', {
      providerId: user.id,
      ns,
      name,
    })

    if (flow && flow.id) {
      await repoManager.removeFlow(flow.name)

      return this.adapter.remove('flows', flow.id)
    }

    return false
  }

  public async flush() {
    return this.adapter.flush('flows')
  }

  public async close() {
    return this.adapter.close()
  }

  private async UserFilter<T extends WithProviderId>(document: T): Promise<T> {
    if (document.providerId) {
      const user = await this.userService.getUser(document.providerId)

      delete document.providerId

      return {
        ...document,
        // TODO: make sure this does not become ambiguous
        provider: {
          ...user,
        },
      }
    }

    return document
  }
}
