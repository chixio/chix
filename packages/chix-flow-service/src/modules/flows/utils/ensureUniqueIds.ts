import {Logger} from '@nestjs/common'
import {Flow as FlowDefinition} from '@chix/common'
import * as uuid from 'uuid'

export function ensureUniqueIds(flow: FlowDefinition, Log: Logger) {
  const idChange: {[key: string]: string} = {}

  for (const node of flow.nodes) {
    Log.log('Node oldId', node.id)
    const oldId = node.id

    idChange[oldId] = node.id = uuid.v4()

    Log.log('Node newId', node.id)
  }

  // link.id's are not really used yet.
  for (const link of flow.links) {
    Log.log('Link oldId', link.id)

    link.id = uuid.v4()

    Log.log('Link newId', link.id)

    // rewrite source & targets
    if (link.target) {
      link.target.id = idChange[link.target.id]
    }

    if (link.source) {
      link.source.id = idChange[link.source.id]
    }
  }
}
