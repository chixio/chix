import {RuntimeDefinition, RuntimeModel, UserModel} from '@chix/common'
import {
  Body,
  Controller,
  Delete,
  Get,
  Head,
  HttpCode,
  HttpStatus,
  OnModuleInit,
  Param,
  Post,
  Put,
  Query,
  Res,
} from '@nestjs/common'
import {Context, ContextModel} from '@nestling/context'
import {ErrorMessage} from '@nestling/errors'
import {Response} from 'express'
import {Page, Pagination} from '../../paginate'
import {User} from '../../user'
import {UserService} from '../../user/user.service'
import {publicOrOwnFilter} from '../shared'
import {RuntimeService} from './runtime.service'

@Controller('runtimes')
export class RuntimeController implements OnModuleInit {
  constructor(
    private readonly runtimeService: RuntimeService,
    private readonly userService: UserService
  ) {}

  public onModuleInit() {
    console.log('RuntimeController, trying to ensure index')
    return this.runtimeService.ensureIndex()
  }

  /**
   * Returns *all* runtimes.
   */
  @Get('')
  @HttpCode(HttpStatus.OK)
  public async find(
    @Context() context: ContextModel,
    @User() user: UserModel,
    @Page() page: Pagination
  ): Promise<RuntimeDefinition[]> {
    const $or: any = publicOrOwnFilter(user)

    return this.runtimeService.find(context, {$or}, {...page})
  }

  /**
   * Search for runtimes.
   */
  @Get('search')
  @HttpCode(HttpStatus.OK)
  public async search(
    @Context() context: ContextModel,
    @User() user: UserModel,
    @Query('q') q: string,
    @Page() page: Pagination
  ): Promise<RuntimeDefinition[]> {
    const $or: any = publicOrOwnFilter(user)

    return this.runtimeService.search(context, q, {...page}, {$or})
  }

  /**
   * Get a list of all runtimes for this user.
   *
   * (Flowhub compat)
   */
  @Get()
  @HttpCode(HttpStatus.OK)
  public async list(
    @Context() context: ContextModel,
    @User({required: true}) user: UserModel
  ): Promise<RuntimeDefinition[]> {
    return this.runtimeService.findByUser(context, user)
  }

  /**
   * Get a list of all runtimes for this user.
   *
   * In case these are the user's own runtimes it will list also include private.
   */
  /*
  @Get(':provider')
  @HttpCode(HttpStatus.OK)
  async findByProvider (
    @Context() context: ContextModel,
    @User() user: UserModel,
    @Param('provider') providerName: string,
    @Page() page: Pagination
  ): Promise<RuntimeDefinition[]> {
    if (user && user.name === providerName) {
      return this.runtimeService.findByUser(
        context,
        user,
        {},
        { ...page }
      )
    }

    const provider = await this.userService.getUserByName(providerName)

    return this.runtimeService.findByUser(
      context,
      provider,
      {
        private: {
          $ne: true
        }
      },
      {
        ...page
      }
    )
  }
  */

  @Head(':provider/:name')
  @HttpCode(HttpStatus.OK)
  public async providerRuntimeExists(
    @User() user: UserModel,
    @Param('provider') providerName: string,
    @Param('name') name: string,
    @Res() response: Response
  ) {
    let exists: boolean

    if (user && user.name === providerName) {
      exists = await this.runtimeService.exists(user, {name})
    } else {
      const provider = await this.userService.getUserByName(providerName)

      exists = await this.runtimeService.exists(provider, {
        name,
        private: {
          $ne: true,
        },
      })
    }

    if (exists) {
      return response.status(HttpStatus.OK)
    }

    return response.status(HttpStatus.NOT_FOUND)
  }
  @Post(':id')
  @HttpCode(HttpStatus.OK)
  public async ping(
    @User() user: UserModel,
    @Param('id') id: string,
    @Res() response: Response
  ) {
    if (user) {
      const exists = await this.runtimeService.exists(user, {id})

      if (exists) {
        return response.status(HttpStatus.OK)
      }
    }

    return response.status(HttpStatus.NOT_FOUND)
  }

  /**
   * Retrieve runtime by id.
   */
  @Get(':id')
  @HttpCode(HttpStatus.OK)
  public async getById(
    @Param('id') id: string,
    @User() user: UserModel
  ): Promise<RuntimeDefinition> {
    if (user) {
      return this.runtimeService.getByUserAndId(user, id)
    }

    throw new ErrorMessage('user:invalid')
  }

  /**
   * Retrieve runtime by username and runtime name.
   */
  @Get(':provider/:name')
  @HttpCode(HttpStatus.OK)
  public async getByProviderAndName(
    @Param('provider') providerName: string,
    @User() user: UserModel,
    @Param('name') name: string
  ): Promise<RuntimeDefinition | null> {
    if (user && user.name === providerName) {
      return this.runtimeService.getByUserAndLabel(user, name)
    }

    const provider = await this.userService.getUserByName(providerName)

    return this.runtimeService.getByUserAndLabel(provider, name, {
      private: {
        $ne: true,
      },
    })
  }

  /**
   * Create a new runtime
   */
  @Post(':provider')
  @HttpCode(HttpStatus.CREATED)
  public async create(
    @User({required: true}) user: UserModel,
    @Param('provider') provider: string,
    @Body() runtime: RuntimeModel
  ) {
    if (user && user.name === provider) {
      return this.runtimeService.create(user, runtime)
    }

    throw new ErrorMessage('user:invalid')
  }

  /**
   * Register given runtime for this user.
   *
   * (FlowHub Compat)
   */
  @Put()
  @HttpCode(HttpStatus.OK)
  public async register(
    @User({required: true}) user: UserModel,
    @Body() runtime: RuntimeModel
  ) {
    if (user) {
      if (Array.isArray(runtime)) {
        return this.runtimeService.createMany(user, runtime)
      }
      return this.runtimeService.create(user, runtime)
    }

    throw new ErrorMessage('user:invalid')
  }

  /**
   * Update given runtime for this user.
   */
  @Put(':provider')
  @HttpCode(HttpStatus.OK)
  public async update(
    @User({required: true}) user: UserModel,
    @Param('provider') provider: string,
    @Body() runtime: RuntimeModel
  ) {
    if (user && user.name === provider) {
      if (Array.isArray(runtime)) {
        return this.runtimeService.updateMany(user, runtime)
      }
      return this.runtimeService.update(user, runtime)
    }

    throw new ErrorMessage('user:invalid')
  }
  /**
   * Remove runtime by user and id.
   */
  @Delete(':id')
  @HttpCode(HttpStatus.NO_CONTENT)
  public async removeById(
    @User({required: true}) user: UserModel,
    @Param('id') id: string
  ) {
    if (user) {
      await this.runtimeService.removeByUserAndId(user, id)
    } else {
      throw new ErrorMessage('user:invalid')
    }
  }

  /**
   * Remove runtime by user and label.
   */
  @Delete(':provider/:name')
  @HttpCode(HttpStatus.NO_CONTENT)
  public async remove(
    @User({required: true}) user: UserModel,
    @Param('provider') provider: string,
    @Param('name') name: string
  ) {
    if (user && user.name === provider) {
      await this.runtimeService.removeByUserAndLabel(user, name)
    } else {
      throw new ErrorMessage('user:invalid')
    }
  }
}
