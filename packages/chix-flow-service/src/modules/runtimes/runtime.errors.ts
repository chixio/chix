import {HttpStatus} from '@nestjs/common'
import {IErrorMessages} from '@nestling/errors'

export const runtimeErrors: IErrorMessages = {
  type: 'runtime',
  errors: [
    {
      code: 'notFound',
      statusCode: HttpStatus.NOT_FOUND,
      message: 'Unable to find the runtime.',
    },
    {
      code: 'alreadyExists',
      statusCode: HttpStatus.CONFLICT,
      message: 'Runtime already exists.',
    },
  ],
}
