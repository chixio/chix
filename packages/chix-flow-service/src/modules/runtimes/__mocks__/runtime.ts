import {RuntimeDefinition} from '@chix/common'

export const runtime: RuntimeDefinition = {
  id: 'fc864109-1880-46bc-9f0c-ebaa454a1af5',
  address: 'wss://localhost:3569',
  protocol: 'websocket',
  type: 'chix',
  label: 'Default Runtime',
  description: 'This is the default runtime',
  registered: new Date(),
  seen: new Date(),
  user: 'rhalff',
  secret: '4571',
}
