import {Module} from '@nestjs/common'
import {ContextModule} from '@nestling/context'
import {ErrorMessage} from '@nestling/errors'
import {DatabaseAdapterModule} from '../../adapter'
import {SharedModule} from '../../shared/shared.module'
import {UserModule} from '../../user/user.module'
import {RuntimeController} from './runtime.controller'
import {runtimeErrors} from './runtime.errors'
import {RuntimeService} from './runtime.service'

ErrorMessage.addErrorMessages(runtimeErrors)

@Module({
  imports: [SharedModule, DatabaseAdapterModule, UserModule, ContextModule],
  controllers: [RuntimeController],
  providers: [RuntimeService],
  exports: [RuntimeService],
})
export class RuntimeModule {}
