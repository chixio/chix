import {RuntimeDefinition, RuntimeModel, UserModel} from '@chix/common'
import {Injectable, Logger} from '@nestjs/common'
import {ContextModel} from '@nestling/context'
import {ErrorMessage} from '@nestling/errors'
import {ValidatorService} from '@nestling/validator'
import {Collection} from 'mongodb'
import {MongoDbAdapter} from '../../adapter/mongo.adapter'
import {Pagination} from '../../paginate'
import {UserService} from '../../user/user.service'
import {WithProviderId} from '../flows'

@Injectable()
export class RuntimeService {
  constructor(
    private adapter: MongoDbAdapter,
    private validatorService: ValidatorService,
    private userService: UserService
  ) {
    this.adapter.addDocFilterFor('runtimes', this.UserFilter.bind(this))
  }

  private log = new Logger(RuntimeService.name)

  /**
   * Ensures the text search index exists.
   *
   * Used to do text search during the search operation.
   *
   * @returns {Promise<void>}
   */
  public async ensureIndex() {
    const collection: Collection = await this.adapter.db.collection('runtimes')

    try {
      await collection.createIndex(
        {
          label: 'text',
          description: 'text',
        },
        {
          name: 'text_search_index',
        }
      )

      this.log.log('Created text_search_index.')
    } catch (e) {}
  }

  public async getByUserAndLabel(
    user: UserModel,
    label: string,
    where: any = {}
  ): Promise<RuntimeModel | null> {
    return this.adapter.findOne<RuntimeModel>('runtimes', {
      providerId: user.id,
      label,
      ...where,
    })
  }

  public async getById(id: string): Promise<RuntimeModel | null> {
    return this.adapter.findOne<RuntimeModel>('runtimes', {id})
  }

  public async getByUserAndId(
    user: UserModel,
    id: string
  ): Promise<RuntimeModel> {
    const runtime = await this.adapter.findOne<RuntimeModel>('runtimes', {
      providerId: user.id,
      id,
    })

    if (!runtime) {
      throw new ErrorMessage('runtime:notFound')
    }

    if (runtime._id) {
      delete runtime._id
    }

    return runtime
  }

  public async find(
    context: ContextModel,
    where = {},
    options?: Pagination
  ): Promise<RuntimeModel[]> {
    return this.adapter.find<RuntimeModel>('runtimes', where, options, context)
  }

  public async search(
    context: ContextModel,
    term: string,
    options?: Pagination,
    where: any = {}
  ): Promise<RuntimeModel[]> {
    return this.adapter.search<RuntimeModel>(
      context,
      'runtimes',
      term,
      options,
      where
    )
  }

  public async findByUser(
    context: ContextModel,
    user: UserModel,
    where = {},
    options?: Pagination
  ): Promise<RuntimeModel[]> {
    return this.adapter.find<RuntimeModel>(
      'runtimes',
      {
        providerId: user.id,
        ...where,
      },
      options,
      context
    )
  }

  public async create(
    user: UserModel,
    runtime: RuntimeDefinition
  ): Promise<RuntimeModel> {
    await this.validatorService.validate('runtime', runtime)
    this.log.log(`Creating Runtime ${runtime.label}`)

    const found = await this.getByUserAndLabel(user, runtime.label)

    if (found) {
      throw new ErrorMessage('runtime:alreadyExists')
    }

    const result = await this.adapter.insert<RuntimeModel>('runtimes', {
      ...runtime,
      registered: new Date(),
      seen: new Date(),
      providerId: user.id,
    })
    this.log.log(`Created runtime: ${result.label}`)

    if (result._id) {
      delete result._id
    }

    return result
  }

  public async createMany(
    user: UserModel,
    runtimes: RuntimeDefinition[]
  ): Promise<RuntimeModel[]> {
    const runtimeModels: RuntimeModel[] = []

    for (const runtime of runtimes) {
      runtimeModels.push(await this.create(user, runtime))
    }

    return runtimeModels
  }

  public async update(
    user: UserModel,
    runtime: RuntimeModel
  ): Promise<RuntimeModel> {
    this.log.log(`Update Runtime ${runtime.label}`)

    await this.validatorService.validate('runtime', runtime)

    let found

    if (runtime.id) {
      found = await this.getByUserAndId(user, runtime.id)

      if (!found) {
        throw new ErrorMessage('runtime:notFound')
      }
    } else {
      found = await this.getByUserAndLabel(user, runtime.label)
    }

    let result
    if (found) {
      result = await this.adapter.update<RuntimeModel>('runtimes', {
        ...runtime,
        seen: new Date(),
        id: found.id,
      })
    } else {
      result = await this.adapter.update<RuntimeModel>('runtimes', {
        ...runtime,
        seen: new Date(),
      })
    }
    this.log.log(`Update runtime ${runtime.label}`)

    return result
  }

  public async updateMany(
    user: UserModel,
    runtimes: RuntimeModel[]
  ): Promise<RuntimeModel[]> {
    const runtimeModels: RuntimeModel[] = []

    for (const runtime of runtimes) {
      runtimeModels.push(await this.update(user, runtime))
    }

    return runtimeModels
  }

  public async exists(user: UserModel, where: any = {}) {
    const _where: any = {
      providerId: user.id,
      ...where,
    }
    this.log.log(`Test if exists: ${JSON.stringify(_where)}`)

    return this.adapter.exists('runtimes', _where)
  }

  public async removeByUserAndLabel(
    user: UserModel,
    label: string
  ): Promise<boolean> {
    this.log.log(`Removing Runtime: ${user.name}/${label}`)

    const runtime = await this.adapter.findOne<RuntimeModel>('runtimes', {
      providerId: user.id,
      label,
    })

    if (runtime) {
      return this.adapter.remove('runtimes', runtime.id)
    }

    return false
  }

  public async removeByUserAndId(
    user: UserModel,
    id: string
  ): Promise<boolean> {
    this.log.log(`Removing Runtime: ${user.name}/${id}`)

    const runtime = await this.adapter.findOne<RuntimeModel>('runtimes', {
      providerId: user.id,
      id,
    })

    if (runtime) {
      return this.adapter.remove('runtimes', runtime.id)
    }

    return false
  }

  public async flush(where: any = {}) {
    const runtimes = await this.adapter.find<RuntimeModel>('runtimes', {
      ...where,
    })

    this.log.log(`Removing ${runtimes.length} runtimes`)

    for (const runtime of runtimes) {
      let user
      try {
        user = await this.userService.getUser((runtime as any).provider.id)
      } catch (error) {
        console.log(runtime)
        throw Error(
          `Owner (${(runtime as any).provider.id}) of runtime ${
            runtime.label
          } not found!`
        )
      }

      await this.removeByUserAndLabel(user, runtime.label)
    }

    return this.adapter.flush('runtimes')
  }

  public async close() {
    return this.adapter.close()
  }

  private async UserFilter<T extends WithProviderId>(document: T): Promise<T> {
    if (document.providerId) {
      const user = await this.userService.getUser(document.providerId)

      delete document.providerId

      return {
        ...(document as any),
        provider: {
          ...user,
        },
      }
    }

    return document
  }
}
