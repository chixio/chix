import {Flow as FlowDefinition, Node, NodeDefinition} from '@chix/common'

export function getIdentifier(node: Node | NodeDefinition | FlowDefinition) {
  return `${node.ns}:${node.name}`
}
