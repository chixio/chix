import {NodeDefinition, NodeModel, UserModel} from '@chix/common'
import {
  Body,
  Controller,
  Delete,
  Get,
  Head,
  HttpCode,
  HttpStatus,
  OnModuleInit,
  Param,
  Post,
  Put,
  Query,
  Res,
} from '@nestjs/common'
import {MessagePattern} from '@nestjs/microservices'
import {Context, ContextModel} from '@nestling/context'
import {ErrorMessage} from '@nestling/errors'
import {Response} from 'express'
import {getRepoManager} from '../../git/getRepoManager'
import {Page, Pagination} from '../../paginate'
import {SharedService} from '../../shared/shared.service'
import {User} from '../../user'
import {UserService} from '../../user/user.service'
import {publicOrOwnFilter} from '../shared'
import {
  GitStatusCreatedMessage,
  GitStatusDeletedMessage,
  GitStatusMessage,
  GitStatusModifiedMessage,
  GitStatusRenamedMessage,
  GitStatusForcePushedMessage,
} from './node.messages'
import {NodeService} from './node.service'

@Controller('nodes')
export class NodeController implements OnModuleInit {
  constructor(
    private readonly nodeService: NodeService,
    private readonly userService: UserService,
    private readonly sharedService: SharedService
  ) {}

  public async onModuleInit() {
    await this.nodeService.ensureIndex()
  }
  @Get('')
  @HttpCode(HttpStatus.OK)
  public async find(
    @Context() context: ContextModel,
    @User() user: UserModel,
    @Page() page: Pagination
  ): Promise<NodeDefinition[]> {
    const $or = publicOrOwnFilter(user)

    return this.nodeService.find(context, {$or}, {...page})
  }

  @Get('search')
  @HttpCode(HttpStatus.OK)
  public async search(
    @Context() context: ContextModel,
    @User() user: UserModel,
    @Query('q') q: string,
    @Page() page: Pagination
  ): Promise<NodeDefinition[]> {
    const $or = publicOrOwnFilter(user)

    return this.nodeService.search(context, q, {...page}, {$or})
  }

  // TODO: clashes a bit with /search
  @Get(':provider')
  @HttpCode(HttpStatus.OK)
  public async findByUser(
    @Context() context: ContextModel,
    @User() user: UserModel,
    @Param('provider') providerName: string,
    @Page() page: Pagination
  ): Promise<NodeDefinition[]> {
    if (user && user.name === providerName) {
      return this.nodeService.findByUser(context, user, {}, {...page})
    }

    const provider = await this.userService.getUserByName(providerName)

    return this.nodeService.findByUser(
      context,
      provider,
      {
        private: {
          $ne: true,
        },
      },
      {
        ...page,
      }
    )
  }

  @Head(':provider/:ns')
  @HttpCode(HttpStatus.OK)
  public async userNamespaceExists(
    @User() user: UserModel,
    @Param('provider') providerName: string,
    @Param('ns') ns: string,
    @Res() response: Response
  ) {
    let exists

    if (user && user.name === providerName) {
      exists = await this.nodeService.exists(user, {ns})
    } else {
      const provider = await this.userService.getUserByName(providerName)

      exists = await this.nodeService.exists(provider, {
        ns,
        private: {
          $ne: true,
        },
      })
    }

    if (exists) {
      return response.status(HttpStatus.OK)
    }

    return response.status(HttpStatus.NOT_FOUND)
  }

  @Get(':provider/:ns')
  @HttpCode(HttpStatus.OK)
  public async findByUserNamespace(
    @Context() context: ContextModel,
    @User() user: UserModel,
    @Param('provider') providerName: string,
    @Param('ns') ns: string,
    @Page() page: Pagination
  ): Promise<NodeDefinition[]> {
    if (user && user.name === providerName) {
      return this.nodeService.findByUserNamespace(
        context,
        user,
        ns,
        {},
        {
          ...page,
        }
      )
    }

    const provider = await this.userService.getUserByName(providerName)

    return this.nodeService.findByUserNamespace(
      context,
      provider,
      ns,
      {
        private: {
          $ne: true,
        },
      },
      {...page}
    )
  }

  @Head(':provider/:ns/:name')
  public async userNamespaceAndNameExists(
    @User() user: UserModel,
    @Param('provider') providerName: string,
    @Param('ns') ns: string,
    @Param('name') name: string,
    @Res() response: Response
  ) {
    let exists

    if (user && user.name === providerName) {
      exists = this.nodeService.exists(user, {ns, name})
    } else {
      const provider = await this.userService.getUserByName(providerName)

      exists = await this.nodeService.exists(provider, {
        ns,
        name,
        private: {
          $ne: true,
        },
      })
    }

    if (exists) {
      return response.status(HttpStatus.OK)
    }

    return response.status(HttpStatus.NOT_FOUND)
  }

  @Get(':provider/:ns/:name')
  @HttpCode(HttpStatus.OK)
  public async findByUserNamespaceAndName(
    @User() user: UserModel,
    @Param('provider') providerName: string,
    @Param('ns') ns: string,
    @Param('name') name: string
  ): Promise<NodeDefinition | null> {
    if (user && user.name === providerName) {
      return this.nodeService.findByUserNamespaceAndName(user, ns, name)
    }

    const provider = await this.userService.getUserByName(providerName)

    return this.nodeService.findByUserNamespaceAndName(provider, ns, name, {
      private: {
        $ne: true,
      },
    })
  }

  /// CRUD OPERATIONS

  @MessagePattern(GitStatusCreatedMessage.subject)
  public async gogsGitStatusCreated(message: GitStatusMessage) {
    return this.nodeService.handleGitStatusCreated(message)
  }

  @MessagePattern(GitStatusDeletedMessage.subject)
  public async gogsGitStatusDeleted(message: GitStatusMessage) {
    return this.nodeService.handleGitStatusDeleted(message)
  }

  @MessagePattern(GitStatusModifiedMessage.subject)
  public async gogsGitStatusModified(message: GitStatusMessage) {
    return this.nodeService.handleGitStatusModified(message)
  }

  @MessagePattern(GitStatusRenamedMessage.subject)
  public async gogsGitStatusRenamed(message: GitStatusMessage) {
    return this.nodeService.handleGitStatusRenamed(message)
  }

  @MessagePattern(GitStatusForcePushedMessage.subject)
  public async gogsGitStatusForcePushed(messages: GitStatusMessage[]) {
    return this.nodeService.handleGitStatusForcePushed(messages)
  }

  @Post(':provider')
  @HttpCode(HttpStatus.CREATED)
  public async create(
    @User({required: true}) user: UserModel,
    @Context() context: ContextModel,
    @Param('provider') provider: string,
    @Body() node: NodeModel
  ) {
    const projectExists = await this.sharedService.projectExists(user, {
      name: node.ns,
    })

    if (projectExists) {
      if (user && user.name === provider) {
        const repoManager = getRepoManager(context, provider)

        await repoManager.openRepository(node.ns)

        let result

        if (Array.isArray(node)) {
          result = await this.nodeService.createMany(user, node, {
            repoManager,
          })
        } else {
          result = await this.nodeService.create(user, node, {
            repoManager,
          })
        }

        await repoManager.closeRepository()

        return result
      }

      throw new ErrorMessage('user:invalid')
    }

    throw new ErrorMessage('node:projectNotFound', {provider, name: node.ns})
  }

  @Put(':provider')
  @HttpCode(HttpStatus.OK)
  public async update(
    @User() user: UserModel,
    @Context() context: ContextModel,
    @Param('provider') provider: string,
    @Body() node: NodeModel
  ) {
    const projectExists = await this.sharedService.projectExists(user, {
      name: node.ns,
    })

    if (projectExists) {
      if (user && user.name === provider) {
        const repoManager = getRepoManager(context, provider)

        await repoManager.openRepository(node.ns)

        let result

        if (Array.isArray(node)) {
          result = await this.nodeService.updateMany(user, node, {
            repoManager,
          })
        } else {
          result = await this.nodeService.update(user, node, {
            repoManager,
          })
        }

        await repoManager.closeRepository()

        return result
      }

      throw new ErrorMessage('user:invalid')
    }

    throw new ErrorMessage('node:projectNotFound', {provider, name: node.ns})
  }

  @Delete(':provider/:ns/:name')
  @HttpCode(HttpStatus.NO_CONTENT)
  public async removeByUserNamespaceAndName(
    @User() user: UserModel,
    @Context() context: ContextModel,
    @Param('provider') provider: string,
    @Param('ns') ns: string,
    @Param('name') name: string
  ) {
    if (user && user.name === provider) {
      const repoManager = getRepoManager(context, provider)

      await repoManager.openRepository(ns)

      await this.nodeService.removeByUserNamespaceAndName(user, ns, name, {
        repoManager,
      })

      await repoManager.closeRepository()
    } else {
      throw new ErrorMessage('user:invalid')
    }
  }
}
