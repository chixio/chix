export const node = {
  name: 'log',
  ns: 'console',
  description: 'Console logger',
  async: true,
  phrases: {
    active: 'Logging to console',
  },
  ports: {
    input: {
      msg: {
        type: 'any',
        title: 'Log message',
        description: 'Logs a message to the console',
        async: true,
        required: true,
      },
    },
    output: {
      out: {
        type: 'any',
        title: 'Log message',
      },
    },
  },
  fn:
    "on.input.msg = function() {\n  console.log($.msg);\n  output( { out: $.get('msg') });\n}\n",
}
