// tslint:disable:max-classes-per-file
export interface RepoInfo {
  repoUsername: string
  repoUserId: number
  repoName: string
  oldCommitId: string
  newCommitId: string
  refFullName: string
  pusherID: number
  pusherName: string
  oldPath: string
  path: string
  baseName: string
  extension: string
}

export interface GitStatusMessage {
  type: string
  info: RepoInfo
  contents: string
  repoUserId: number // added
}

export class GitStatusCreatedMessage {
  public static subject = 'gogs.gitStatus.created'
  constructor(public payload: GitStatusMessage) {}
}

export class GitStatusRenamedMessage {
  public static subject = 'gogs.gitStatus.renamed'
  constructor(public payload: GitStatusMessage) {}
}

export class GitStatusModifiedMessage {
  public static subject = 'gogs.gitStatus.modified'
  constructor(public payload: GitStatusMessage) {}
}

export class GitStatusDeletedMessage {
  public static subject = 'gogs.gitStatus.deleted'
  constructor(public payload: GitStatusMessage) {}
}

export class GitStatusForcePushedMessage {
  public static subject = 'gogs.gitStatus.forcePushed'
  constructor(public payload: GitStatusMessage[]) {}
}
