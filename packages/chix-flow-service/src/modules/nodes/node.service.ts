import {
  Flow as FlowDefinition,
  NodeDefinition,
  NodeModel,
  UserModel,
} from '@chix/common'
import {Injectable, Logger} from '@nestjs/common'
import {ContextModel} from '@nestling/context'
import {ErrorMessage} from '@nestling/errors'
import {ValidatorService} from '@nestling/validator'
import * as yaml from 'js-yaml'
import {Collection} from 'mongodb'
import {MongoDbAdapter} from '../../adapter/mongo.adapter'
import {RepoManager} from '../../git/RepoManager'
import {Pagination} from '../../paginate'
import {UserService} from '../../user/user.service'
import {WithProviderId} from '../flows'
import {GitStatusMessage} from './node.messages'
import {getIdentifier} from './utils'

export interface GitStatusResponse {
  status: string
}

export interface Scope {
  user?: string
}

export interface RepoManagerContext {
  repoManager: RepoManager
}

@Injectable()
export class NodeService {
  constructor(
    private adapter: MongoDbAdapter,
    private validatorService: ValidatorService,
    private userService: UserService
  ) {
    this.adapter.addDocFilterFor('nodes', this.UserFilter.bind(this))
  }

  private log = new Logger(NodeService.name)

  /**
   * Ensures the text search index exists.
   *
   * Used to do text search during the search operation.
   *
   * @returns {Promise<void>}
   */
  public async ensureIndex() {
    const collection: Collection = await this.adapter.db.collection('nodes')

    try {
      await collection.createIndex(
        {
          ns: 'text',
          name: 'text',
          description: 'text',
        },
        {
          name: 'text_search_index',
        }
      )

      this.log.log('Created text_search_index.')
    } catch (e) {}
  }

  public async getByNamespaceAndName(
    ns: string,
    name: string,
    scope: Scope = {}
  ): Promise<NodeModel | null> {
    return this.adapter.findOne<NodeModel>('nodes', {
      ns,
      name,
      ...scope,
    })
  }

  public async getByUserNamespaceAndName(
    user: UserModel,
    ns: string,
    name: string,
    scope: Scope = {}
  ): Promise<NodeModel | null> {
    return this.adapter.findOne<NodeModel>('nodes', {
      providerId: user.id,
      ns,
      name,
      ...scope,
    })
  }

  public async getById(id: string): Promise<NodeModel | null> {
    return this.adapter.findOne<NodeModel>('nodes', {id})
  }

  public async getByUserAndId(user: UserModel, id: string): Promise<NodeModel> {
    const node = await this.adapter.findOne<NodeModel>('nodes', {
      providerId: user.id,
      id,
    })

    if (!node) {
      throw new ErrorMessage('node:notFound')
    }

    if (node._id) {
      delete node._id
    }

    return node
  }

  public async find(
    context: ContextModel,
    where = {},
    options?: Pagination
  ): Promise<NodeModel[]> {
    return this.adapter.find<NodeModel>('nodes', where, options, context)
  }

  public async search(
    context: ContextModel,
    term: string,
    options?: Pagination,
    where: any = {}
  ): Promise<NodeModel[]> {
    return this.adapter.search<NodeModel>(
      context,
      'nodes',
      term,
      options,
      where
    )
  }

  public async findByNamespace(
    context: ContextModel,
    ns: string,
    options?: Pagination
  ): Promise<NodeModel[]> {
    return this.adapter.find<NodeModel>(
      'nodes',
      {
        ns,
      },
      options,
      context
    )
  }

  public async findByUser(
    context: ContextModel,
    user: UserModel,
    where: any,
    options?: Pagination
  ): Promise<NodeModel[]> {
    return this.adapter.find<NodeModel>(
      'nodes',
      {
        providerId: user.id,
        ...where,
      },
      options,
      context
    )
  }

  public async findByUserNamespace(
    context: ContextModel,
    user: UserModel,
    ns: string,
    where: any = {},
    options?: Pagination
  ): Promise<NodeModel[]> {
    return this.adapter.find<NodeModel>(
      'nodes',
      {
        providerId: user.id,
        ns,
        ...where,
      },
      options,
      context
    )
  }

  public async findByNamespaceAndName(
    ns: string,
    name: string,
    where: any = {}
  ): Promise<NodeModel | null> {
    return this.adapter.findOne<NodeModel>('nodes', {
      ns,
      name,
      ...where,
    })
  }

  public async findByUserNamespaceAndName(
    user: UserModel,
    ns: string,
    name: string,
    where: any = {}
  ): Promise<NodeModel | null> {
    return this.adapter.findOne<NodeModel>('nodes', {
      providerId: user.id,
      ns,
      name,
      ...where,
    })
  }

  ///////////
  // CRUD
  ///////////

  public async create(
    user: UserModel,
    node: NodeDefinition,
    {repoManager}: RepoManagerContext
  ): Promise<NodeModel> {
    await this.validatorService.validate('node', node)

    const found = await this.getByUserNamespaceAndName(user, node.ns, node.name)

    if (found) {
      throw new ErrorMessage('node:alreadyExists', {
        ns: node.ns,
        name: node.name,
      })
    }

    this.log.log(`Creating Node ${getIdentifier(node)}`)

    await repoManager.openRepository(node.ns)

    // this put will commit the node.
    // gogs will reach us back through nats
    // that call will insert the node
    // after this step the node itself should be present.
    await this.complexCall(() => repoManager.putNode(node))

    const result = await this.getByUserNamespaceAndName(
      user,
      node.ns,
      node.name
    )

    if (!result) {
      throw new ErrorMessage('node:notFound')
    }

    this.log.log(`Created node: ${getIdentifier(result)}`)

    if (node._id) {
      delete node._id
    }
    if (result._id) {
      delete result._id
    }

    return result
  }

  public async createMany(
    user: UserModel,
    nodes: NodeDefinition[],
    context: RepoManagerContext
  ): Promise<NodeModel[]> {
    const promises = nodes.map((node: NodeDefinition) =>
      this.create(user, node, context)
    )

    return Promise.all<NodeModel>(promises)
  }

  public async update(
    user: UserModel,
    node: NodeModel,
    {repoManager}: RepoManagerContext
  ): Promise<NodeModel> {
    this.log.log(`saving Node ${node.ns}:${node.name}`)

    await this.validatorService.validate('node', node)

    let found: NodeModel | null

    await repoManager.openRepository(node.ns)

    if (node.id) {
      found = await this.getByUserAndId(user, node.id)

      if (!found) {
        throw new ErrorMessage('node:notFound')
      }

      const foundName = found.name

      if (node.name !== foundName) {
        this.log.log(
          `Removing node ${found.ns}:${foundName} as it will be renamed.`
        )
        await this.complexCall(() => repoManager.removeNode(foundName))
      }
    } else {
      found = await this.getByUserNamespaceAndName(user, node.ns, node.name)

      if (!found) {
        node.providerId = user.id
      }
    }

    const bareNode = Object.keys(node).reduce((_node, key) => {
      if (key !== 'providerId') {
        _node[key] = (node as any)[key]
      }

      return _node
    }, {} as any)

    // this will cause the updated node to be committed
    // this commit will trigger an update message from gogs.
    // after the node is added to the database this promise will successfully return
    await this.complexCall(() => repoManager.putNode(bareNode))

    // find back the updated node and return it
    const result = await this.getByUserNamespaceAndName(
      user,
      node.ns,
      node.name
    )

    if (!result) {
      throw new ErrorMessage('node:notFound')
    }

    /*
    if (found) {
      result = await this.adapter.update<NodeModel>('nodes', {
        ...node,
        id: found.id,
      })
    } else {
      result = await this.adapter.update<NodeModel>('nodes', node)
    }
    */

    this.log.log(`Saved node ${getIdentifier(node)}`)

    return result
  }

  public async renameNs(
    user: UserModel,
    oldNs: string,
    newNs: string,
    context: RepoManagerContext
  ): Promise<void> {
    this.log.log(`renaming node namespace ${oldNs} -> ${newNs}`)

    const col = this.adapter.db.collection('nodes')

    await col.updateMany(
      {
        ns: oldNs,
        providerId: user.id,
      },
      {
        $set: {ns: newNs},
      }
    )

    const result = await col.find({ns: newNs}).toArray()
    const nodes = result as unknown as NodeModel[]

    if (nodes.length) {
      this.log.log(`Updating ${nodes.length} renamed nodes.`)
      await this.updateMany(user, nodes, context)
    } else {
      this.log.log(`renameNs: found no ${newNs} nodes to update.`)
    }
  }

  public async updateMany(
    user: UserModel,
    nodes: NodeModel[],
    context: RepoManagerContext
  ): Promise<NodeModel[]> {
    const promises = nodes.map((node) => this.update(user, node, context))

    return Promise.all<NodeModel>(promises)
  }

  public async remove(id: string, {repoManager}: RepoManagerContext) {
    this.log.log(`Removing Node: ${id}`)

    const node = await this.adapter.findById<NodeModel>('nodes', id)

    if (node) {
      // should test whether it's removed
      // after this call gogs should have requested a removal
      // and it should have succeeded after this call
      await this.complexCall(() => repoManager.removeNode(node.name))

      // return this.adapter.remove('nodes', id)
    }

    return false
  }

  public async exists(user: UserModel, where: any) {
    this.log.log(`Test if exists: ${Object.keys(where)}`)

    return this.adapter.exists('nodes', {
      providerId: user.id,
      ...where,
    })
  }

  public async removeByNamespaceAndName(
    ns: string,
    name: string,
    {repoManager}: RepoManagerContext
  ): Promise<boolean> {
    this.log.log(`Removing Node by namespace and name: ${ns}:${name}`)

    const node = await this.adapter.findOne<NodeModel>('nodes', {
      ns,
      name,
    })

    if (node) {
      await repoManager.openRepository(node.ns)

      await this.complexCall(() => repoManager.removeNode(node.name))

      await repoManager.closeRepository()

      // return this.adapter.remove('nodes', node.id)
      return true
    }

    return false
  }

  public async removeByUserNamespace(
    user: UserModel,
    ns: string,
    {repoManager}: RepoManagerContext
  ): Promise<boolean> {
    this.log.log(`Removing Nodes by namespace: ${ns}`)

    const nodes = await this.adapter.find<NodeModel>('nodes', {
      providerId: user.id,
      ns,
    })

    if (nodes.length) {
      const names = nodes.map((node) => node.name)

      try {
        await repoManager.openRepository(ns)
        await this.complexCall(() => repoManager.removeNodes(names))
      } catch (error) {
        this.log.warn(
          `Failed to remove (some) nodes from repository continuing anyway.`
        )
      } finally {
        await repoManager.closeRepository()
      }

      return this.adapter.removeAll('nodes', {
        ns,
        providerId: user.id,
      })
    }

    return false
  }

  public async removeByUserNamespaceAndName(
    user: UserModel,
    ns: string,
    name: string,
    {repoManager}: RepoManagerContext
  ): Promise<boolean> {
    this.log.log(`Removing Node by namespace and name: ${ns}:${name}`)

    const node = await this.adapter.findOne<NodeModel>('nodes', {
      providerId: user.id,
      ns,
      name,
    })

    if (node) {
      // removes a node and commits it to gogs
      await this.complexCall(() => repoManager.removeNode(node.name))

      // return this.adapter.remove('nodes', node.id)
    }

    return false
  }

  public isNodeDefinitionMessage({
    info: {baseName},
  }: GitStatusMessage): boolean {
    return baseName === 'node.json' || baseName === 'node.yml'
  }

  /**
   * Runs after a force push to the repository
   *
   * The post receive hook in gogs will make the request to update our db.
   *
   * And will only end successfully based on our reply.
   *
   * The message itself contains a list of create messages.
   *
   * Before starting the creation the current nodes for the repository will be removed from the database.
   */
  public async handleGitStatusForcePushed(
    messages: GitStatusMessage[]
  ): Promise<GitStatusResponse> {
    this.log.log(`handleGitStatusForcePushed start`)
    if (messages.length) {
      // TODO: make a better message format so this picking is not necessary
      const firstMessage = messages[0]

      const {
        info: {repoUserId, repoName},
      } = firstMessage

      this.log.log(`handleGitStatusForcePushed removeAll ${repoName} nodes`)
      await this.adapter.removeAll('nodes', {
        ns: repoName,
        providerId: repoUserId,
      })

      for (const message of messages) {
        await this.handleGitStatusCreated(message)
      }
    }
    this.log.log(`handleGitStatusForcePushed success`)

    return {status: 'OK'}
  }

  /**
   * Runs after a new node has been committed to gogs
   *
   * The post receive hook in gogs will make the request to update our db.
   *
   * And will only end successfully based on our reply.
   */
  public async handleGitStatusCreated(
    message: GitStatusMessage
  ): Promise<GitStatusResponse> {
    const node = this.parseFileContents(message) as NodeDefinition

    this.log.log(`handleGitStatusCreated ${node.ns}:${node.name}`)

    await this.adapter.insert<NodeModel>('nodes', {
      providerId: message.repoUserId,
      ...node,
    })

    this.log.log(`handleGitStatusCreated success`)

    return {status: 'OK'}
  }

  /**
   * Handles git status deleted
   *
   * The post receive hook in gogs will make the request to remove the node from our db.
   *
   * And will only end successfully based on our reply.
   */
  public async handleGitStatusDeleted(
    message: GitStatusMessage
  ): Promise<GitStatusResponse> {
    const {name} = this.getNodeNameAndTypeFromPath(message.info.path)
    const ns = message.info.repoName

    this.log.log(`handleGitStatusDeleted ${ns}:${name}`)

    await this.adapter.removeAll('nodes', {
      providerId: message.info.repoUserId,
      ns,
      name,
    })

    this.log.log(`handleGitStatusDeleted success`)

    return {status: 'OK'}
  }

  /**
   * Handles git status modified
   *
   * The post receive hook in gogs will make the request to modify the node within our db.
   *
   * And will only end successfully based on our reply.
   *
   * Note: This status message will only occur for direct commits as the api itself
   * will always do a remove followed by a create during a rename action.
   */
  public async handleGitStatusModified(
    message: GitStatusMessage
  ): Promise<GitStatusResponse> {
    this.log.log(`handleGitStatusModified`)
    const node = this.parseFileContents(message) as NodeDefinition

    this.log.log(`handleGitStatusModified ${node.ns}:${node.name}`)

    const user: UserModel = {
      id: message.info.repoUserId,
      name: message.info.repoUsername,
    }

    const nodeModel = await this.findByUserNamespaceAndName(
      user,
      node.ns,
      node.name
    )

    if (!nodeModel) {
      return {status: 'error'}
    }

    await this.adapter.update<NodeModel>('nodes', {
      ...nodeModel,
      ...node,
    })

    this.log.log(`handleGitStatusModified success`)

    return {status: 'OK'}
  }

  /**
   * Handles git status renamed
   *
   * The post receive hook in gogs will make the request to rename the node within our db.
   *
   * And will only end successfully based on our reply.
   */
  public async handleGitStatusRenamed(
    message: GitStatusMessage
  ): Promise<GitStatusResponse> {
    const {name: oldName, type: oldType} = this.getNodeNameAndTypeFromPath(
      message.info.oldPath
    )
    const {name: newName, type: newType} = this.getNodeNameAndTypeFromPath(
      message.info.path
    )

    this.log.log(`handleGitStatusRenamed ${oldName} => ${newName}`)

    if (oldType !== 'nodes' && newType !== 'nodes') {
      // noop
      return {status: 'OK'}
    }

    if (newType === 'nodes') {
      if (oldType !== 'nodes') {
        // add
        return this.handleGitStatusCreated(message)
      }
      // update
      const nodeModel = await this.findByUserNamespaceAndName(
        {
          id: message.info.repoUserId,
          name: message.info.repoUsername,
        },
        message.info.repoName,
        oldName
      )

      if (!nodeModel) {
        return {status: 'error'}
      }

      await this.adapter.update<NodeModel>('nodes', {
        ...nodeModel,
        name: newName,
      })

      this.log.log(`HandleGitStatusRenamed renamed ${oldName} => ${newName}`)

      return {status: 'OK'}
    } else {
      // remove
      return this.handleGitStatusDeleted(message)
    }
  }

  public async flush() {
    return this.adapter.flush('nodes')
  }

  public async close() {
    return this.adapter.close()
  }

  /**
   *  Determines the node's name based on the path convention used in git. e.g.
   *
   *  nodes/api/node.yml - node name is `api`
   *  nodes/sub/api/node.yml - node name is `sub/api`
   */
  private getNodeNameAndTypeFromPath(path: string) {
    const [type, ...parts] = path.split('/')

    if (parts.length < 2) {
      throw new ErrorMessage('node:notFound')
    }

    const name = parts.slice(0, -1).join('/')

    return {
      type,
      name,
    }
  }

  /**
   * This method exist to mark the complexity of the call being made.
   *
   * The executed methods rely on a request response trough nats.
   *
   * - commit is made to gogs
   * - gogs sends a git status message through nats
   * - this server handles this message and updates the database accordingly
   * - this server replies with a successful response
   * - the commit in gogs succeeds
   * - and thus the complex call resolves successfully.
   */
  private async complexCall(fn: () => Promise<void>) {
    this.log.log(`complexCall`)
    return fn()
  }

  // TODO: currently at the nodes/ endpoint a flow is never a yaml file
  // Perhaps make the format in which it is saved optional
  private parseFileContents(
    message: GitStatusMessage
  ): FlowDefinition | NodeDefinition | Error {
    const {
      contents,
      info: {extension},
    } = message

    let node
    switch (extension) {
      case '.yml':
        node = yaml.load(contents)
        break
      case '.json':
        node = JSON.parse(contents)
        break
      default:
        return Error('failed to determine file type')
    }

    return node
  }

  private async UserFilter<T extends WithProviderId>(document: T): Promise<T> {
    if (document.providerId) {
      const user = await this.userService.getUser(document.providerId)

      delete document.providerId

      return {
        ...(document as any),
        // TODO: make sure this does not become ambiguous
        provider: {
          ...user,
        },
      }
    }

    return document
  }
}
