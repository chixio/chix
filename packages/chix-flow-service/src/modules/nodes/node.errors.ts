import {HttpStatus} from '@nestjs/common'
import {IErrorMessages} from '@nestling/errors'

export const nodeErrors: IErrorMessages = {
  type: 'node',
  errors: [
    {
      code: 'notFound',
      statusCode: HttpStatus.NOT_FOUND,
      message: 'Unable to find the node.',
    },
    {
      code: 'alreadyExists',
      statusCode: HttpStatus.CONFLICT,
      message: 'Node {ns}:{name} already exists.',
    },
    {
      code: 'projectNotFound',
      statusCode: HttpStatus.UNPROCESSABLE_ENTITY,
      message: 'Project {provider}/{name} does not exist.',
    },
  ],
}
