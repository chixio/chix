import {Module} from '@nestjs/common'
import {ContextModule} from '@nestling/context'
import {ErrorMessage} from '@nestling/errors'
import {LoggerModule} from '@nestling/logger'
import {ValidatorModule} from '@nestling/validator'
import {DatabaseAdapterModule} from '../../adapter'
import {NatsModule} from '../../nats'
import {SharedModule} from '../../shared/shared.module'
import {UserModule} from '../../user/user.module'
import {NodeController} from './node.controller'
import {nodeErrors} from './node.errors'
import {NodeService} from './node.service'

ErrorMessage.addErrorMessages(nodeErrors)

@Module({
  imports: [
    SharedModule,
    NatsModule,
    LoggerModule,
    ValidatorModule,
    DatabaseAdapterModule,
    UserModule,
    ContextModule,
  ],
  controllers: [NodeController],
  providers: [NodeService],
  exports: [NodeService],
})
export class NodeModule {}
