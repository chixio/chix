import {UserModel} from '@chix/common'

export function publicOrOwnFilter(user: UserModel, or: any[] = []) {
  or.push({
    private: {$ne: true},
  })

  if (user) {
    or.push({
      providerId: user.id,
    })
  }

  return or
}
