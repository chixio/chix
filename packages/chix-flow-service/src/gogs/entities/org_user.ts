import {Column, Entity, Index, PrimaryGeneratedColumn} from 'typeorm'

@Entity('org_user', {schema: 'chix_gogs'})
@Index('UQE_org_user_s', ['uid', 'orgId'], {unique: true})
@Index('IDX_org_user_uid', ['uid'])
@Index('IDX_org_user_org_id', ['orgId'])
export class OrgUser {
  @PrimaryGeneratedColumn({
    type: 'bigint',
    name: 'id',
  })
  public id: string

  @Column({
    type: 'bigint',
    nullable: true,
    name: 'uid',
  })
  public uid: string | null

  @Column({
    type: 'bigint',
    nullable: true,
    name: 'org_id',
  })
  public orgId: string | null

  @Column({
    type: 'tinyint',
    nullable: true,
    width: 1,
    name: 'is_public',
  })
  public ispublic: boolean | null

  @Column({
    type: 'tinyint',
    nullable: true,
    width: 1,
    name: 'is_owner',
  })
  public isOwner: boolean | null

  @Column({
    type: 'int',
    nullable: true,
    name: 'num_teams',
  })
  public numTeams: number | null
}
