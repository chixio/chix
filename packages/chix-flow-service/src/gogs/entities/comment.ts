import {Column, Entity, Index, PrimaryGeneratedColumn} from 'typeorm'

@Entity('comment', {schema: 'chix_gogs'})
@Index('IDX_comment_issue_id', ['issueId'])
export class Comment {
  @PrimaryGeneratedColumn({
    type: 'bigint',
    name: 'id',
  })
  public id: string

  @Column({
    type: 'int',
    nullable: true,
    name: 'type',
  })
  public type: number | null

  @Column({
    type: 'bigint',
    nullable: true,
    name: 'poster_id',
  })
  public posterId: string | null

  @Column({
    type: 'bigint',
    nullable: true,
    name: 'issue_id',
  })
  public issueId: string | null

  @Column({
    type: 'bigint',
    nullable: true,
    name: 'commit_id',
  })
  public commitId: string | null

  @Column({
    type: 'bigint',
    nullable: true,
    name: 'line',
  })
  public line: string | null

  @Column({
    type: 'text',
    nullable: true,
    name: 'content',
  })
  public content: string | null

  @Column({
    type: 'bigint',
    nullable: true,
    name: 'created_unix',
  })
  public created: string | null

  @Column({
    type: 'bigint',
    nullable: true,
    name: 'updated_unix',
  })
  public updated: string | null

  @Column({
    type: 'varchar',
    nullable: true,
    length: 40,
    name: 'commit_sha',
  })
  public commitSha: string | null
}
