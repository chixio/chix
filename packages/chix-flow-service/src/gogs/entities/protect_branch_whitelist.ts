import {Column, Entity, Index, PrimaryGeneratedColumn} from 'typeorm'

@Entity('protect_branch_whitelist', {schema: 'chix_gogs'})
@Index(
  'UQE_protect_branch_whitelist_protect_branch_whitelist',
  ['repoId', 'name', 'userId'],
  {unique: true}
)
export class ProtectBranchWhitelist {
  @PrimaryGeneratedColumn({
    type: 'bigint',
    name: 'id',
  })
  public id: string

  @Column({
    type: 'bigint',
    nullable: true,
    name: 'protect_branch_id',
  })
  public protectBranchId: string | null

  @Column({
    type: 'bigint',
    nullable: true,
    name: 'repo_id',
  })
  public repoId: string | null

  @Column({
    type: 'varchar',
    nullable: true,
    length: 255,
    name: 'name',
  })
  public name: string | null

  @Column({
    type: 'bigint',
    nullable: true,
    name: 'user_id',
  })
  public userId: string | null
}
