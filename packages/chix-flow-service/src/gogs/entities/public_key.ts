import {Column, Entity, Index, PrimaryGeneratedColumn} from 'typeorm'

@Entity('public_key', {schema: 'chix_gogs'})
@Index('IDX_public_key_owner_id', ['ownerId'])
export class PublicKey {
  @PrimaryGeneratedColumn({
    type: 'bigint',
    name: 'id',
  })
  public id: string

  @Column({
    type: 'bigint',
    nullable: false,
    name: 'owner_id',
  })
  public ownerId: string

  @Column({
    type: 'varchar',
    nullable: false,
    length: 255,
    name: 'name',
  })
  public name: string

  @Column({
    type: 'varchar',
    nullable: false,
    length: 255,
    name: 'fingerprint',
  })
  public fingerprint: string

  @Column({
    type: 'text',
    nullable: false,
    name: 'content',
  })
  public content: string

  @Column({
    type: 'int',
    nullable: false,
    default: '2',
    name: 'mode',
  })
  public mode: number

  @Column({
    type: 'int',
    nullable: false,
    default: '1',
    name: 'type',
  })
  public type: number

  @Column({
    type: 'bigint',
    nullable: true,
    name: 'created_unix',
  })
  public created: string | null

  @Column({
    type: 'bigint',
    nullable: true,
    name: 'updated_unix',
  })
  public updated: string | null
}
