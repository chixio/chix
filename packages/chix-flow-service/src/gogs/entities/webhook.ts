import {Column, Entity, PrimaryGeneratedColumn} from 'typeorm'

@Entity('webhook', {schema: 'chix_gogs'})
export class Webhook {
  @PrimaryGeneratedColumn({
    type: 'bigint',
    name: 'id',
  })
  public id: string

  @Column({
    type: 'bigint',
    nullable: true,
    name: 'repo_id',
  })
  public repoId: string | null

  @Column({
    type: 'bigint',
    nullable: true,
    name: 'org_id',
  })
  public orgId: string | null

  @Column({
    type: 'text',
    nullable: true,
    name: 'url',
  })
  public url: string | null

  @Column({
    type: 'int',
    nullable: true,
    name: 'content_type',
  })
  public contentType: number | null

  @Column({
    type: 'text',
    nullable: true,
    name: 'secret',
  })
  public secret: string | null

  @Column({
    type: 'text',
    nullable: true,
    name: 'events',
  })
  public events: string | null

  @Column({
    type: 'tinyint',
    nullable: true,
    width: 1,
    name: 'is_ssl',
  })
  public isSsl: boolean | null

  @Column({
    type: 'tinyint',
    nullable: true,
    width: 1,
    name: 'is_active',
  })
  public isActive: boolean | null

  @Column({
    type: 'int',
    nullable: true,
    name: 'hook_task_type',
  })
  public hookTaskType: number | null

  @Column({
    type: 'text',
    nullable: true,
    name: 'meta',
  })
  public meta: string | null

  @Column({
    type: 'int',
    nullable: true,
    name: 'last_status',
  })
  public lastStatus: number | null

  @Column({
    type: 'bigint',
    nullable: true,
    name: 'created_unix',
  })
  public created: string | null

  @Column({
    type: 'bigint',
    nullable: true,
    name: 'updated_unix',
  })
  public updated: string | null
}
