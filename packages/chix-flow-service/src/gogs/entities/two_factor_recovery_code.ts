import {Column, Entity, PrimaryGeneratedColumn} from 'typeorm'

@Entity('two_factor_recovery_code', {schema: 'chix_gogs'})
export class TwoFactorRecoveryCode {
  @PrimaryGeneratedColumn({
    type: 'bigint',
    name: 'id',
  })
  public id: string

  @Column({
    type: 'bigint',
    nullable: true,
    name: 'user_id',
  })
  public userId: string | null

  @Column({
    type: 'varchar',
    nullable: true,
    length: 11,
    name: 'code',
  })
  public code: string | null

  @Column({
    type: 'tinyint',
    nullable: true,
    width: 1,
    name: 'is_used',
  })
  public isUsed: boolean | null
}
