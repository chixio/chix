import {Column, Entity, Index, PrimaryGeneratedColumn} from 'typeorm'

@Entity('hook_task', {schema: 'chix_gogs'})
@Index('IDX_hook_task_repo_id', ['repoId'])
export class HookTask {
  @PrimaryGeneratedColumn({
    type: 'bigint',
    name: 'id',
  })
  public id: string

  @Column({
    type: 'bigint',
    nullable: true,
    name: 'repo_id',
  })
  public repoId: string | null

  @Column({
    type: 'bigint',
    nullable: true,
    name: 'hook_id',
  })
  public hookId: string | null

  @Column({
    type: 'varchar',
    nullable: true,
    length: 255,
    name: 'uuid',
  })
  public uuid: string | null

  @Column({
    type: 'int',
    nullable: true,
    name: 'type',
  })
  public type: number | null

  @Column({
    type: 'text',
    nullable: true,
    name: 'url',
  })
  public url: string | null

  @Column({
    type: 'text',
    nullable: true,
    name: 'signature',
  })
  public signature: string | null

  @Column({
    type: 'text',
    nullable: true,
    name: 'payload_content',
  })
  public payloadContent: string | null

  @Column({
    type: 'int',
    nullable: true,
    name: 'content_type',
  })
  public contentType: number | null

  @Column({
    type: 'varchar',
    nullable: true,
    length: 255,
    name: 'event_type',
  })
  public eventType: string | null

  @Column({
    type: 'tinyint',
    nullable: true,
    width: 1,
    name: 'is_ssl',
  })
  public isSsl: boolean | null

  @Column({
    type: 'tinyint',
    nullable: true,
    width: 1,
    name: 'is_delivered',
  })
  public isDelivered: boolean | null

  @Column({
    type: 'bigint',
    nullable: true,
    name: 'delivered',
  })
  public delivered: string | null

  @Column({
    type: 'tinyint',
    nullable: true,
    width: 1,
    name: 'is_succeed',
  })
  public isSucceed: boolean | null

  @Column({
    type: 'text',
    nullable: true,
    name: 'request_content',
  })
  public requestContent: string | null

  @Column({
    type: 'text',
    nullable: true,
    name: 'response_content',
  })
  public responseContent: string | null
}
