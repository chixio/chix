import {Column, Entity, Index, PrimaryGeneratedColumn} from 'typeorm'

@Entity('deploy_key', {schema: 'chix_gogs'})
@Index('UQE_deploy_key_s', ['keyId', 'repoId'], {unique: true})
@Index('IDX_deploy_key_key_id', ['keyId'])
@Index('IDX_deploy_key_repo_id', ['repoId'])
export class DeployKey {
  @PrimaryGeneratedColumn({
    type: 'bigint',
    name: 'id',
  })
  public id: string

  @Column({
    type: 'bigint',
    nullable: true,
    name: 'key_id',
  })
  public keyId: string | null

  @Column({
    type: 'bigint',
    nullable: true,
    name: 'repo_id',
  })
  public repoId: string | null

  @Column({
    type: 'varchar',
    nullable: true,
    length: 255,
    name: 'name',
  })
  public name: string | null

  @Column({
    type: 'varchar',
    nullable: true,
    length: 255,
    name: 'fingerprint',
  })
  public fingerprint: string | null

  @Column({
    type: 'bigint',
    nullable: true,
    name: 'created_unix',
  })
  public created: string | null

  @Column({
    type: 'bigint',
    nullable: true,
    name: 'updated_unix',
  })
  public updated: string | null
}
