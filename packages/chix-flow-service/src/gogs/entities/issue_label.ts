import {Column, Entity, Index, PrimaryGeneratedColumn} from 'typeorm'

@Entity('issue_label', {schema: 'chix_gogs'})
@Index('UQE_issue_label_s', ['issueId', 'labelId'], {unique: true})
export class IssueLabel {
  @PrimaryGeneratedColumn({
    type: 'bigint',
    name: 'id',
  })
  public id: string

  @Column({
    type: 'bigint',
    nullable: true,
    name: 'issue_id',
  })
  public issueId: string | null

  @Column({
    type: 'bigint',
    nullable: true,
    name: 'label_id',
  })
  public labelId: string | null
}
