import {Column, Entity, PrimaryGeneratedColumn} from 'typeorm'

@Entity('release', {schema: 'chix_gogs'})
export class Release {
  @PrimaryGeneratedColumn({
    type: 'bigint',
    name: 'id',
  })
  public id: string

  @Column({
    type: 'bigint',
    nullable: true,
    name: 'repo_id',
  })
  public repoId: string | null

  @Column({
    type: 'bigint',
    nullable: true,
    name: 'publisher_id',
  })
  public publisherId: string | null

  @Column({
    type: 'varchar',
    nullable: true,
    length: 255,
    name: 'tag_name',
  })
  public tagName: string | null

  @Column({
    type: 'varchar',
    nullable: true,
    length: 255,
    name: 'lower_tag_name',
  })
  public lowerTagName: string | null

  @Column({
    type: 'varchar',
    nullable: true,
    length: 255,
    name: 'target',
  })
  public target: string | null

  @Column({
    type: 'varchar',
    nullable: true,
    length: 255,
    name: 'title',
  })
  public title: string | null

  @Column('varchar', {
    nullable: true,
    length: 40,
    name: 'sha1',
  })
  public sha1: string | null

  @Column({
    type: 'bigint',
    nullable: true,
    name: 'num_commits',
  })
  public numCommits: string | null

  @Column({
    type: 'text',
    nullable: true,
    name: 'note',
  })
  public note: string | null

  @Column({
    type: 'tinyint',
    nullable: false,
    width: 1,
    default: '0',
    name: 'is_draft',
  })
  public isDraft: boolean

  @Column({
    type: 'tinyint',
    nullable: true,
    width: 1,
    name: 'is_prerelease',
  })
  public isPrerelease: boolean | null

  @Column({
    type: 'bigint',
    nullable: true,
    name: 'created_unix',
  })
  public created: string | null
}
