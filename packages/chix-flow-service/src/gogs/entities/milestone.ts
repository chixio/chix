import {Column, Entity, Index, PrimaryGeneratedColumn} from 'typeorm'

@Entity('milestone', {schema: 'chix_gogs'})
@Index('IDX_milestone_repo_id', ['repoId'])
export class Milestone {
  @PrimaryGeneratedColumn({
    type: 'bigint',
    name: 'id',
  })
  public id: string

  @Column({
    type: 'bigint',
    nullable: true,
    name: 'repo_id',
  })
  public repoId: string | null

  @Column({
    type: 'varchar',
    nullable: true,
    length: 255,
    name: 'name',
  })
  public name: string | null

  @Column({
    type: 'text',
    nullable: true,
    name: 'content',
  })
  public content: string | null

  @Column({
    type: 'tinyint',
    nullable: true,
    width: 1,
    name: 'is_closed',
  })
  public Cislosed: boolean | null

  @Column({
    type: 'int',
    nullable: true,
    name: 'num_issues',
  })
  public numIssues: number | null

  @Column({
    type: 'int',
    nullable: true,
    name: 'num_closed_issues',
  })
  public numClosedIssues: number | null

  @Column({
    type: 'int',
    nullable: true,
    name: 'completeness',
  })
  public completeness: number | null

  @Column({
    type: 'bigint',
    nullable: true,
    name: 'deadline_unix',
  })
  public deadlineUnix: string | null

  @Column({
    type: 'bigint',
    nullable: true,
    name: 'closed_date_unix',
  })
  public closedDateUnix: string | null
}
