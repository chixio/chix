import {Column, Entity, Index, PrimaryGeneratedColumn} from 'typeorm'

@Entity('action', {schema: 'chix_gogs'})
@Index('IDX_action_repo_id', ['repoId'])
export class Action {
  @PrimaryGeneratedColumn({
    type: 'bigint',
    name: 'id',
  })
  public id: string

  @Column({
    type: 'bigint',
    nullable: true,
    name: 'user_id',
  })
  public userId: string | null

  @Column({
    type: 'int',
    nullable: true,
    name: 'op_type',
  })
  public opType: number | null

  @Column({
    type: 'bigint',
    nullable: true,
    name: 'act_user_id',
  })
  public actUserId: string | null

  @Column({
    type: 'varchar',
    nullable: true,
    length: 255,
    name: 'act_user_name',
  })
  public actUserName: string | null

  @Column({
    type: 'bigint',
    nullable: true,
    name: 'repo_id',
  })
  public repoId: string | null

  @Column({
    type: 'varchar',
    nullable: true,
    length: 255,
    name: 'repo_user_name',
  })
  public repoUserName: string | null

  @Column({
    type: 'varchar',
    nullable: true,
    length: 255,
    name: 'repo_name',
  })
  public repoName: string | null

  @Column({
    type: 'varchar',
    nullable: true,
    length: 255,
    name: 'ref_name',
  })
  public refName: string | null

  @Column({
    type: 'tinyint',
    nullable: false,
    width: 1,
    default: '0',
    name: 'is_private',
  })
  public isPrivate: boolean

  @Column({
    type: 'text',
    nullable: true,
    name: 'content',
  })
  public content: string | null

  @Column({
    type: 'bigint',
    nullable: true,
    name: 'created_unix',
  })
  public created: string | null
}
