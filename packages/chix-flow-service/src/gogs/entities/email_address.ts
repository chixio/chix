import {Column, Entity, Index, PrimaryGeneratedColumn} from 'typeorm'

@Entity('email_address', {schema: 'chix_gogs'})
@Index('UQE_email_address_email', ['email'], {unique: true})
@Index('IDX_email_address_uid', ['uid'])
export class EmailAddress {
  @PrimaryGeneratedColumn({
    type: 'bigint',
    name: 'id',
  })
  public id: string

  @Column({
    type: 'bigint',
    nullable: false,
    name: 'uid',
  })
  public uid: string

  @Column({
    type: 'varchar',
    nullable: false,
    unique: true,
    length: 255,
    name: 'email',
  })
  public email: string

  @Column({
    type: 'tinyint',
    nullable: true,
    width: 1,
    name: 'is_activated',
  })
  public isActivated: boolean | null
}
