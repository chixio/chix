import {Column, Entity, Index, PrimaryGeneratedColumn} from 'typeorm'

@Entity('issue_user', {schema: 'chix_gogs'})
@Index('IDX_issue_user_uid', ['uid'])
@Index('IDX_issue_user_repo_id', ['repoId'])
export class IssueUser {
  @PrimaryGeneratedColumn({
    type: 'bigint',
    name: 'id',
  })
  public id: string

  @Column({
    type: 'bigint',
    nullable: true,
    name: 'uid',
  })
  public uid: string | null

  @Column({
    type: 'bigint',
    nullable: true,
    name: 'issue_id',
  })
  public issueId: string | null

  @Column({
    type: 'bigint',
    nullable: true,
    name: 'repo_id',
  })
  public repoId: string | null

  @Column({
    type: 'bigint',
    nullable: true,
    name: 'milestone_id',
  })
  public milestoneId: string | null

  @Column({
    type: 'tinyint',
    nullable: true,
    width: 1,
    name: 'is_read',
  })
  public isRead: boolean | null

  @Column({
    type: 'tinyint',
    nullable: true,
    width: 1,
    name: 'is_assigned',
  })
  public isAssigned: boolean | null

  @Column({
    type: 'tinyint',
    nullable: true,
    width: 1,
    name: 'is_mentioned',
  })
  public isMentioned: boolean | null

  @Column({
    type: 'tinyint',
    nullable: true,
    width: 1,
    name: 'is_poster',
  })
  public isPoster: boolean | null

  @Column({
    type: 'tinyint',
    nullable: true,
    width: 1,
    name: 'is_closed',
  })
  public isClosed: boolean | null
}
