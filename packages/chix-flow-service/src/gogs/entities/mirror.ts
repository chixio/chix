import {Column, Entity, PrimaryGeneratedColumn} from 'typeorm'

@Entity('mirror', {schema: 'chix_gogs'})
export class Mirror {
  @PrimaryGeneratedColumn({
    type: 'bigint',
    name: 'id',
  })
  public id: string

  @Column({
    type: 'bigint',
    nullable: true,
    name: 'repo_id',
  })
  public repoId: string | null

  @Column({
    type: 'int',
    nullable: true,
    name: 'interval',
  })
  public interval: number | null

  @Column({
    type: 'tinyint',
    nullable: false,
    width: 1,
    default: '1',
    name: 'enable_prune',
  })
  public enablePrune: boolean

  @Column({
    type: 'bigint',
    nullable: true,
    name: 'updated_unix',
  })
  public updated: string | null

  @Column({
    type: 'bigint',
    nullable: true,
    name: 'next_update_unix',
  })
  public nextUpdateUnix: string | null
}
