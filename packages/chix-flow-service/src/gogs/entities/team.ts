import {Column, Entity, Index, PrimaryGeneratedColumn} from 'typeorm'

@Entity('team', {schema: 'chix_gogs'})
@Index('IDX_team_org_id', ['orgId'])
export class Team {
  @PrimaryGeneratedColumn({
    type: 'bigint',
    name: 'id',
  })
  public id: string

  @Column({
    type: 'bigint',
    nullable: true,
    name: 'org_id',
  })
  public orgId: string | null

  @Column({
    type: 'varchar',
    nullable: true,
    length: 255,
    name: 'lower_name',
  })
  public lowerName: string | null

  @Column({
    type: 'varchar',
    nullable: true,
    length: 255,
    name: 'name',
  })
  public name: string | null

  @Column({
    type: 'varchar',
    nullable: true,
    length: 255,
    name: 'description',
  })
  public description: string | null

  @Column({
    type: 'int',
    nullable: true,
    name: 'authorize',
  })
  public authorize: number | null

  @Column({
    type: 'int',
    nullable: true,
    name: 'num_repos',
  })
  public numRepos: number | null

  @Column({
    type: 'int',
    nullable: true,
    name: 'num_members',
  })
  public numMembers: number | null
}
