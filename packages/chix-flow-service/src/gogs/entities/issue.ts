import {Column, Entity, Index, PrimaryGeneratedColumn} from 'typeorm'

@Entity('issue', {schema: 'chix_gogs'})
@Index('UQE_issue_repo_index', ['repoId', 'index'], {unique: true})
@Index('IDX_issue_repo_id', ['repoId'])
export class Issue {
  @PrimaryGeneratedColumn({
    type: 'bigint',
    name: 'id',
  })
  public id: string

  @Column({
    type: 'bigint',
    nullable: true,
    name: 'repo_id',
  })
  public repoId: string | null

  @Column({
    type: 'bigint',
    nullable: true,
    name: 'index',
  })
  public index: string | null

  @Column({
    type: 'bigint',
    nullable: true,
    name: 'poster_id',
  })
  public posterId: string | null

  @Column({
    type: 'varchar',
    nullable: true,
    length: 255,
    name: 'name',
  })
  public name: string | null

  @Column({
    type: 'text',
    nullable: true,
    name: 'content',
  })
  public content: string | null

  @Column({
    type: 'bigint',
    nullable: true,
    name: 'milestone_id',
  })
  public milestoneId: string | null

  @Column({
    type: 'int',
    nullable: true,
    name: 'priority',
  })
  public priority: number | null

  @Column({
    type: 'bigint',
    nullable: true,
    name: 'assignee_id',
  })
  public assigneeId: string | null

  @Column({
    type: 'tinyint',
    nullable: true,
    width: 1,
    name: 'is_closed',
  })
  public isClosed: boolean | null

  @Column({
    type: 'tinyint',
    nullable: true,
    width: 1,
    name: 'is_pull',
  })
  public isPull: boolean | null

  @Column({
    type: 'int',
    nullable: true,
    name: 'num_comments',
  })
  public numComments: number | null

  @Column({
    type: 'bigint',
    nullable: true,
    name: 'deadline_unix',
  })
  public deadlineUnix: string | null

  @Column({
    type: 'bigint',
    nullable: true,
    name: 'created_unix',
  })
  public created: string | null

  @Column({
    type: 'bigint',
    nullable: true,
    name: 'updated_unix',
  })
  public updated: string | null
}
