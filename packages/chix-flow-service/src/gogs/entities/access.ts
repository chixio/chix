import {Column, Entity, Index, PrimaryGeneratedColumn} from 'typeorm'

@Entity('access', {schema: 'chix_gogs'})
@Index('UQE_access_s', ['userId', 'repoId'], {unique: true})
export class Access {
  @PrimaryGeneratedColumn({
    type: 'bigint',
    name: 'id',
  })
  public id: string

  @Column({
    type: 'bigint',
    nullable: true,
    name: 'user_id',
  })
  public userId: string | null

  @Column({
    type: 'bigint',
    nullable: true,
    name: 'repo_id',
  })
  public repoId: string | null

  @Column({
    type: 'int',
    nullable: true,
    name: 'mode',
  })
  public mode: number | null
}
