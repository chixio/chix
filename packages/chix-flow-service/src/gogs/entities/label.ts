import {Column, Entity, Index, PrimaryGeneratedColumn} from 'typeorm'

@Entity('label', {schema: 'chix_gogs'})
@Index('IDX_label_repo_id', ['repoId'])
export class Label {
  @PrimaryGeneratedColumn({
    type: 'bigint',
    name: 'id',
  })
  public id: string

  @Column({
    type: 'bigint',
    nullable: true,
    name: 'repo_id',
  })
  public repoId: string | null

  @Column({
    type: 'varchar',
    nullable: true,
    length: 255,
    name: 'name',
  })
  public name: string | null

  @Column({
    type: 'varchar',
    nullable: true,
    length: 7,
    name: 'color',
  })
  public color: string | null

  @Column({
    type: 'int',
    nullable: true,
    name: 'num_issues',
  })
  public numIssues: number | null

  @Column({
    type: 'int',
    nullable: true,
    name: 'num_closed_issues',
  })
  public numClosedIssues: number | null
}
