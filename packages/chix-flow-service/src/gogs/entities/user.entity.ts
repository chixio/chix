import {Column, Entity, Index, PrimaryGeneratedColumn} from 'typeorm'

export type UserType = number

export const USER_TYPE_INDIVIDUAL = 0
export const USER_TYPE_ORGANIZATION = 1

@Entity('user', {schema: 'chix_gogs'})
@Index('UQE_user_name', ['name'], {unique: true})
@Index('UQE_user_lower_name', ['lowerName'], {unique: true})
export class User {
  @PrimaryGeneratedColumn({
    type: 'bigint',
    name: 'id',
  })
  public id: number

  @Column({
    type: 'varchar',
    nullable: false,
    unique: true,
    length: 255,
    name: 'lower_name',
  })
  public lowerName: string

  @Column({
    type: 'varchar',
    nullable: false,
    unique: true,
    length: 255,
    name: 'name',
  })
  public name: string

  @Column({
    type: 'varchar',
    nullable: true,
    length: 255,
    name: 'full_name',
  })
  public fullName: string | null

  @Column({
    type: 'varchar',
    nullable: false,
    length: 255,
    name: 'email',
  })
  public email: string

  @Column({
    type: 'varchar',
    nullable: false,
    length: 255,
    name: 'passwd',
  })
  public passwd: string

  @Column({
    type: 'int',
    nullable: true,
    name: 'login_type',
  })
  public loginType: number | null

  @Column({
    type: 'bigint',
    nullable: false,
    default: 0,
    name: 'login_source',
  })
  public loginSource: number

  @Column({
    type: 'varchar',
    nullable: true,
    length: 255,
    name: 'login_name',
  })
  public loginName: string | null

  @Column({
    type: 'int',
    nullable: true,
    name: 'type',
  })
  public type: number | null

  @Column({
    type: 'varchar',
    nullable: true,
    length: 255,
    name: 'location',
  })
  public location: string | null

  @Column({
    type: 'varchar',
    nullable: true,
    length: 255,
    name: 'website',
  })
  public website: string | null

  @Column({
    type: 'varchar',
    nullable: true,
    length: 10,
    name: 'rands',
  })
  public rands: string | null

  @Column({
    type: 'varchar',
    nullable: true,
    length: 10,
    name: 'salt',
  })
  public salt: string | null

  @Column({
    type: 'bigint',
    nullable: true,
    name: 'created_unix',
  })
  public created: number | null

  @Column({
    type: 'bigint',
    nullable: true,
    name: 'updated_unix',
  })
  public updated: number | null

  @Column({
    type: 'tinyint',
    nullable: true,
    width: 1,
    name: 'last_repo_visibility',
  })
  public lastRepoVisibility: boolean | null

  @Column({
    type: 'int',
    nullable: false,
    default: '-1',
    name: 'max_repo_creation',
  })
  public maxRepoCreation: number

  @Column({
    type: 'tinyint',
    nullable: true,
    width: 1,
    name: 'is_active',
  })
  public isActive: boolean | null

  @Column({
    type: 'tinyint',
    nullable: true,
    width: 1,
    name: 'is_admin',
  })
  public isAdmin: boolean | null

  @Column({
    type: 'tinyint',
    nullable: true,
    width: 1,
    name: 'allow_git_hook',
  })
  public allowGitHook: boolean | null

  @Column({
    type: 'tinyint',
    nullable: true,
    width: 1,
    name: 'allow_import_local',
  })
  public allowImportLocal: boolean | null

  @Column({
    type: 'tinyint',
    nullable: true,
    width: 1,
    name: 'prohibit_login',
  })
  public prohibitLogin: boolean | null

  @Column({
    type: 'varchar',
    nullable: false,
    length: 2048,
    name: 'avatar',
  })
  public avatar: string

  @Column({
    type: 'varchar',
    nullable: false,
    length: 255,
    name: 'avatar_email',
  })
  public avatarEmail: string

  @Column({
    type: 'int',
    nullable: true,
    width: 1,
    name: 'use_custom_avatar',
  })
  public useCustomAvatar: boolean | null

  @Column({
    type: 'int',
    nullable: true,
    name: 'num_followers',
  })
  public numFollowers: number | null

  @Column({
    type: 'int',
    nullable: false,
    default: '0',
    name: 'num_following',
  })
  public numFollowing: number

  @Column({
    type: 'int',
    nullable: true,
    name: 'num_stars',
  })
  public numStars: number | null

  @Column({
    type: 'int',
    nullable: true,
    name: 'num_repos',
  })
  public numRepos: number | null

  @Column({
    type: 'varchar',
    nullable: true,
    length: 255,
    name: 'description',
  })
  public description: string | null

  @Column({
    type: 'int',
    nullable: true,
    name: 'num_teams',
  })
  public numTeams: number | null

  @Column({
    type: 'int',
    nullable: true,
    name: 'num_members',
  })
  public numMembers: number | null
}
