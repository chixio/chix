import {Column, Entity, Index, PrimaryGeneratedColumn} from 'typeorm'

@Entity('upload', {schema: 'chix_gogs'})
@Index('UQE_upload_uuid', ['uuid'], {unique: true})
export class Upload {
  @PrimaryGeneratedColumn({
    type: 'bigint',
    name: 'id',
  })
  public id: string

  @Column({
    type: 'varchar',
    nullable: true,
    unique: true,
    length: 40,
    name: 'uuid',
  })
  public uuid: string | null

  @Column({
    type: 'varchar',
    nullable: true,
    length: 255,
    name: 'name',
  })
  public name: string | null
}
