import {Column, Entity, Index, PrimaryGeneratedColumn} from 'typeorm'

@Entity('repository', {schema: 'chix_gogs'})
@Index('UQE_repository_s', ['ownerId', 'lowerName'], {unique: true})
@Index('IDX_repository_lower_name', ['lowerName'])
@Index('IDX_repository_name', ['name'])
export class Repository {
  @PrimaryGeneratedColumn({
    type: 'bigint',
    name: 'id',
  })
  public id: string

  @Column({
    type: 'bigint',
    nullable: true,
    name: 'owner_id',
  })
  public ownerId: string | null

  @Column({
    type: 'varchar',
    nullable: false,
    length: 255,
    name: 'lower_name',
  })
  public lowerName: string

  @Column({
    type: 'varchar',
    nullable: false,
    length: 255,
    name: 'name',
  })
  public name: string

  @Column({
    type: 'varchar',
    nullable: true,
    length: 512,
    name: 'description',
  })
  public description: string | null

  @Column({
    type: 'varchar',
    nullable: true,
    length: 255,
    name: 'website',
  })
  public website: string | null

  @Column({
    type: 'varchar',
    nullable: true,
    length: 255,
    name: 'default_branch',
  })
  public defaultBranch: string | null

  @Column({
    type: 'bigint',
    nullable: false,
    default: '0',
    name: 'size',
  })
  public size: string

  @Column({
    type: 'tinyint',
    nullable: true,
    width: 1,
    name: 'use_custom_avatar',
  })
  public useCustomAvatar: boolean | null

  @Column({
    type: 'int',
    nullable: true,
    name: 'num_watches',
  })
  public numWatches: number | null

  @Column({
    type: 'int',
    nullable: true,
    name: 'num_stars',
  })
  public numStars: number | null

  @Column({
    type: 'int',
    nullable: true,
    name: 'num_forks',
  })
  public numForks: number | null

  @Column({
    type: 'int',
    nullable: true,
    name: 'num_issues',
  })
  public numIssues: number | null

  @Column({
    type: 'int',
    nullable: true,
    name: 'num_closed_issues',
  })
  public numClosedIssues: number | null

  @Column({
    type: 'int',
    nullable: true,
    name: 'num_pulls',
  })
  public numPulls: number | null

  @Column({
    type: 'int',
    nullable: true,
    name: 'num_closed_pulls',
  })
  public numClosedPulls: number | null

  @Column({
    type: 'int',
    nullable: false,
    default: '0',
    name: 'num_milestones',
  })
  public numMilestones: number

  @Column({
    type: 'int',
    nullable: false,
    default: '0',
    name: 'num_closed_milestones',
  })
  public numClosedMilestones: number

  @Column({
    type: 'tinyint',
    nullable: true,
    width: 1,
    name: 'is_private',
  })
  public isPrivate: boolean | null

  @Column({
    type: 'tinyint',
    nullable: true,
    width: 1,
    name: 'is_bare',
  })
  public isBare: boolean | null

  @Column({
    type: 'tinyint',
    nullable: true,
    width: 1,
    name: 'is_mirror',
  })
  public isMirror: boolean | null

  @Column({
    type: 'tinyint',
    nullable: false,
    width: 1,
    default: '1',
    name: 'enable_wiki',
  })
  public enableWiki: boolean

  @Column({
    type: 'tinyint',
    nullable: true,
    width: 1,
    name: 'allow_public_wiki',
  })
  public allowPublicWiki: boolean | null

  @Column({
    type: 'tinyint',
    nullable: true,
    width: 1,
    name: 'enable_external_wiki',
  })
  public enableExternalWiki: boolean | null

  @Column({
    type: 'varchar',
    nullable: true,
    length: 255,
    name: 'external_wiki_url',
  })
  public externalWikiUrl: string | null

  @Column({
    type: 'tinyint',
    nullable: false,
    width: 1,
    default: '1',
    name: 'enable_issues',
  })
  public enableIssues: boolean

  @Column({
    type: 'tinyint',
    nullable: true,
    width: 1,
    name: 'allow_public_issues',
  })
  public allowPublicIssues: boolean | null

  @Column({
    type: 'tinyint',
    nullable: true,
    width: 1,
    name: 'enable_external_tracker',
  })
  public enableExternalTracker: boolean | null

  @Column({
    type: 'varchar',
    nullable: true,
    length: 255,
    name: 'external_tracker_url',
  })
  public externalTrackerUrl: string | null

  @Column({
    type: 'varchar',
    nullable: true,
    length: 255,
    name: 'external_tracker_format',
  })
  public externalTrackerFormat: string | null

  @Column({
    type: 'varchar',
    nullable: true,
    length: 255,
    name: 'external_tracker_style',
  })
  public externalTrackerStyle: string | null

  @Column({
    type: 'tinyint',
    nullable: false,
    width: 1,
    default: '1',
    name: 'enable_pulls',
  })
  public enablePulls: boolean

  @Column({
    type: 'tinyint',
    nullable: false,
    width: 1,
    default: '0',
    name: 'pulls_ignore_whitespace',
  })
  public pullsIgnoreWhitespace: boolean

  @Column({
    type: 'tinyint',
    nullable: false,
    width: 1,
    default: '0',
    name: 'pulls_allow_rebase',
  })
  public pullsAllowRebase: boolean

  @Column({
    type: 'tinyint',
    nullable: false,
    width: 1,
    default: '0',
    name: 'is_fork',
  })
  public isFork: boolean

  @Column({
    type: 'bigint',
    nullable: true,
    name: 'fork_id',
  })
  public forkId: string | null

  @Column({
    type: 'bigint',
    nullable: true,
    name: 'created_unix',
  })
  public created: string | null

  @Column({
    type: 'bigint',
    nullable: true,
    name: 'updated_unix',
  })
  public updated: string | null
}
