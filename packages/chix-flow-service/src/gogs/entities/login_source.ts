import {Column, Entity, Index, PrimaryGeneratedColumn} from 'typeorm'

@Entity('login_source', {schema: 'chix_gogs'})
@Index('UQE_login_source_name', ['name'], {unique: true})
export class LoginSource {
  @PrimaryGeneratedColumn({
    type: 'bigint',
    name: 'id',
  })
  public id: string

  @Column({
    type: 'int',
    nullable: true,
    name: 'type',
  })
  public type: number | null

  @Column({
    type: 'varchar',
    nullable: true,
    unique: true,
    length: 255,
    name: 'name',
  })
  public name: string | null

  @Column({
    type: 'tinyint',
    nullable: false,
    width: 1,
    default: '0',
    name: 'is_actived',
  })
  public isActived: boolean

  @Column({
    type: 'text',
    nullable: true,
    name: 'cfg',
  })
  public cfg: string | null

  @Column({
    type: 'bigint',
    nullable: true,
    name: 'created_unix',
  })
  public created: string | null

  @Column({
    type: 'bigint',
    nullable: true,
    name: 'updated_unix',
  })
  public updated: string | null
}
