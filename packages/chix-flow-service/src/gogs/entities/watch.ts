import {Column, Entity, Index, PrimaryGeneratedColumn} from 'typeorm'

@Entity('watch', {schema: 'chix_gogs'})
@Index('UQE_watch_watch', ['userId', 'repoId'], {unique: true})
export class Watch {
  @PrimaryGeneratedColumn({
    type: 'bigint',
    name: 'id',
  })
  public id: string

  @Column({
    type: 'bigint',
    nullable: true,
    name: 'user_id',
  })
  public userId: string | null

  @Column({
    type: 'bigint',
    nullable: true,
    name: 'repo_id',
  })
  public repoId: string | null
}
