import {Column, Entity, Index, PrimaryGeneratedColumn} from 'typeorm'

@Entity('star', {schema: 'chix_gogs'})
@Index('UQE_star_s', ['uid', 'repoId'], {unique: true})
export class Star {
  @PrimaryGeneratedColumn({
    type: 'bigint',
    name: 'id',
  })
  public id: string

  @Column({
    type: 'bigint',
    nullable: true,
    name: 'uid',
  })
  public uid: string | null

  @Column({
    type: 'bigint',
    nullable: true,
    name: 'repo_id',
  })
  public repoId: string | null
}
