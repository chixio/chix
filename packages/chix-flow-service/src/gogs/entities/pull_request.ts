import {Column, Entity, Index, PrimaryGeneratedColumn} from 'typeorm'

@Entity('pull_request', {schema: 'chix_gogs'})
@Index('IDX_pull_request_issue_id', ['issueId'])
export class PullRequest {
  @PrimaryGeneratedColumn({
    type: 'bigint',
    name: 'id',
  })
  public id: string

  @Column({
    type: 'int',
    nullable: true,
    name: 'type',
  })
  public type: number | null

  @Column({
    type: 'int',
    nullable: true,
    name: 'status',
  })
  public status: number | null

  @Column({
    type: 'bigint',
    nullable: true,
    name: 'issue_id',
  })
  public issueId: string | null

  @Column({
    type: 'bigint',
    nullable: true,
    name: 'index',
  })
  public index: string | null

  @Column({
    type: 'bigint',
    nullable: true,
    name: 'head_repo_id',
  })
  public headRepoId: string | null

  @Column({
    type: 'bigint',
    nullable: true,
    name: 'base_repo_id',
  })
  public baseRepoId: string | null

  @Column({
    type: 'varchar',
    nullable: true,
    length: 255,
    name: 'head_user_name',
  })
  public headUserName: string | null

  @Column({
    type: 'varchar',
    nullable: true,
    length: 255,
    name: 'head_branch',
  })
  public headBranch: string | null

  @Column({
    type: 'varchar',
    nullable: true,
    length: 255,
    name: 'base_branch',
  })
  public baseBranch: string | null

  @Column({
    type: 'varchar',
    nullable: true,
    length: 40,
    name: 'merge_base',
  })
  public mergeBase: string | null

  @Column({
    type: 'tinyint',
    nullable: true,
    width: 1,
    name: 'has_merged',
  })
  public hasMerged: boolean | null

  @Column({
    type: 'varchar',
    nullable: true,
    length: 40,
    name: 'merged_commit_id',
  })
  public mergedCommitId: string | null

  @Column({
    type: 'bigint',
    nullable: true,
    name: 'merger_id',
  })
  public mergerId: string | null

  @Column({
    type: 'bigint',
    nullable: true,
    name: 'merged_unix',
  })
  public mergedUnix: string | null
}
