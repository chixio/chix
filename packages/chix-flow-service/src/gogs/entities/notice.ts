import {Column, Entity, PrimaryGeneratedColumn} from 'typeorm'

@Entity('notice', {schema: 'chix_gogs'})
export class Notice {
  @PrimaryGeneratedColumn({
    type: 'bigint',
    name: 'id',
  })
  public id: string

  @Column({
    type: 'int',
    nullable: true,
    name: 'type',
  })
  public type: number | null

  @Column({
    type: 'text',
    nullable: true,
    name: 'description',
  })
  public description: string | null

  @Column({
    type: 'bigint',
    nullable: true,
    name: 'created_unix',
  })
  public created: string | null
}
