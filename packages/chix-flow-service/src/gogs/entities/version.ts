import {Column, Entity, PrimaryGeneratedColumn} from 'typeorm'

@Entity('version', {schema: 'chix_gogs'})
export class Version {
  @PrimaryGeneratedColumn({
    type: 'bigint',
    name: 'id',
  })
  public id: string

  @Column({
    type: 'bigint',
    nullable: true,
    name: 'version',
  })
  public version: string | null
}
