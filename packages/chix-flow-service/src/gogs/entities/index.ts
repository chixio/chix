import {Access} from './access'
import {AccessToken} from './access_token'
import {Action} from './action'
import {Attachment} from './attachment'
import {Collaboration} from './collaboration'
import {Comment} from './comment'
import {DeployKey} from './deploy_key'
import {EmailAddress} from './email_address'
import {Follow} from './follow'
import {HookTask} from './hook_task'
import {Issue} from './issue'
import {IssueLabel} from './issue_label'
import {IssueUser} from './issue_user'
import {Label} from './label'
import {LoginSource} from './login_source'
import {Milestone} from './milestone'
import {Mirror} from './mirror'
import {Notice} from './notice'
import {OrgUser} from './org_user'
import {ProtectBranch} from './protect_branch'
import {ProtectBranchWhitelist} from './protect_branch_whitelist'
import {PublicKey} from './public_key'
import {PullRequest} from './pull_request'
import {Release} from './release'
import {Repository} from './repository'
import {Star} from './star'
import {Team} from './team'
import {TeamRepo} from './team_repo'
import {TeamUser} from './team_user'
import {TwoFactor} from './two_factor'
import {TwoFactorRecoveryCode} from './two_factor_recovery_code'
import {Upload} from './upload'
import {User} from './user.entity'
import {Version} from './version'
import {Watch} from './watch'
import {Webhook} from './webhook'

export const entities = [
  Access,
  AccessToken,
  Action,
  Attachment,
  Collaboration,
  Comment,
  DeployKey,
  EmailAddress,
  Follow,
  HookTask,
  Issue,
  IssueLabel,
  IssueUser,
  Label,
  LoginSource,
  Milestone,
  Mirror,
  Notice,
  OrgUser,
  ProtectBranch,
  ProtectBranchWhitelist,
  PublicKey,
  PullRequest,
  Release,
  Repository,
  Star,
  Team,
  TeamRepo,
  TeamUser,
  TwoFactor,
  TwoFactorRecoveryCode,
  Upload,
  User,
  Version,
  Watch,
  Webhook,
]
