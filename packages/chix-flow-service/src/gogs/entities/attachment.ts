import {Column, Entity, Index, PrimaryGeneratedColumn} from 'typeorm'

@Entity('attachment', {schema: 'chix_gogs'})
@Index('UQE_attachment_uuid', ['uuid'], {unique: true})
@Index('IDX_attachment_issue_id', ['issueId'])
@Index('IDX_attachment_release_id', ['releaseId'])
export class Attachment {
  @PrimaryGeneratedColumn({
    type: 'bigint',
    name: 'id',
  })
  public id: string

  @Column({
    type: 'varchar',
    nullable: true,
    unique: true,
    length: 40,
    name: 'uuid',
  })
  public uuid: string | null

  @Column({
    type: 'bigint',
    nullable: true,
    name: 'issue_id',
  })
  public issueId: string | null

  @Column({
    type: 'bigint',
    nullable: true,
    name: 'comment_id',
  })
  public commentId: string | null

  @Column({
    type: 'bigint',
    nullable: true,
    name: 'release_id',
  })
  public releaseId: string | null

  @Column({
    type: 'varchar',
    nullable: true,
    length: 255,
    name: 'name',
  })
  public name: string | null

  @Column({
    type: 'bigint',
    nullable: true,
    name: 'created_unix',
  })
  public created: string | null
}
