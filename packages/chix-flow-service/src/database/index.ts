import {Module} from '@nestjs/common'
import {mongoProvider} from '@nestling/mongodb'

export const databaseProviders = [mongoProvider]

@Module({
  exports: [...databaseProviders],
  providers: [...databaseProviders],
})
export class DatabaseModule {}
