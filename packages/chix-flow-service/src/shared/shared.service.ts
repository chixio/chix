import {UserModel} from '@chix/common'
import {Injectable, Logger} from '@nestjs/common'
import {MongoDbAdapter} from '../adapter/mongo.adapter'

@Injectable()
export class SharedService {
  constructor(private adapter: MongoDbAdapter) {}

  private log = new Logger(SharedService.name)

  public async projectExists(user: UserModel, where: any = {}) {
    const _where: any = {
      providerId: user.id,
      ...where,
    }

    this.log.log(`Test if exists: ${JSON.stringify(_where)}`)

    return this.adapter.exists('projects', _where)
  }
}
