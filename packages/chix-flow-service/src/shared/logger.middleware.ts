import {Injectable, Logger, NestMiddleware} from '@nestjs/common'
import {setContext} from '@nestling/context'
import {Request, Response} from 'express'

@Injectable()
export class LoggerMiddleware implements NestMiddleware {
  private logger = new Logger(LoggerMiddleware.name)
  public use() {
    return (request: Request, _response: Response, next: () => void) => {
      this.logger.log(request)

      setContext('logger', this.logger, request)

      if (next) {
        next()
      }
    }
  }
}
