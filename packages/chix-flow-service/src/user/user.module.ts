import {Module} from '@nestjs/common'
import {ErrorMessage} from '@nestling/errors'
import {MemcachedModule} from '../memcached'
import {UserService} from './user.service'
/*
import { GrpcStrategy } from './strategies/grpc.strategy'
import { MongoStrategy } from './strategies/mongo.strategy'
import { ConfigService } from '@nestling/config'
import { LogService } from '@nestling/logger/dist/LogService'
*/

import {DatabaseModule} from '../database'
import {GogsDbStrategy} from './strategies/gogs-db.strategy'
import {userErrors} from './user.errors'

ErrorMessage.addErrorMessages(userErrors)

/*
const strategyFactory = {
  provide: 'UserStrategy',
  useFactory: (
    config: ConfigService,
    log: LogService,
    memcachedService: MemcachedService,
    mongoDbToken,
    typeOrmConnectionToken
  ) => {
    const { user: { strategy } } = config as any

    if (strategy === 'GrpcStrategy') {
      return new GrpcStrategy(log, config, memcachedService)
    }

    if (strategy === 'MongoStrategy') {
      return new MongoStrategy(log, config, mongoDbToken)
    }

    if (strategy === 'GogsDbStrategy') {
      return new GogsDbStrategy(log, typeOrmConnectionToken)
    }

    throw Error(`Unknown strategy ${strategy}`)
  },
  inject: [
    ConfigService,
    LogService,
    MemcachedService,
    'MongoDbToken',
    'TypeOrmConnectionToken'
  ]
}
*/

@Module({
  imports: [MemcachedModule, DatabaseModule],
  exports: [UserService, GogsDbStrategy],
  providers: [
    GogsDbStrategy,
    UserService,
    // strategyFactory,
    {
      provide: 'UserStrategy',
      useClass: GogsDbStrategy,
    },
  ],
})
export class UserModule {}
