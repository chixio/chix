import {Inject, Injectable, Logger, OnModuleInit} from '@nestjs/common'

import {ErrorMessage} from '@nestling/errors'

import {UserId, UserModel} from '@chix/common'

import {StartableInterface} from '../interfaces'
import {UserStrategyInterface} from './strategies/user.strategy.interface'

@Injectable()
export class UserService implements OnModuleInit {
  constructor(
    @Inject('UserStrategy')
    private strategy: UserStrategyInterface & StartableInterface
  ) {
    // todo, see why onModuleInit doesn't run on specific tests. (does run as application)
    // this.strategy.start()
  }

  private log = new Logger(UserService.name)

  public async onModuleInit() {
    await this.strategy.start()
  }

  public async preload(): Promise<void> {
    this.log.log('preloading user information')

    await this.strategy.preload()
  }

  public async getUserByName(name: string): Promise<UserModel> {
    const user = await this.strategy.getUserByName(name)

    if (user) {
      return user
    }

    throw new ErrorMessage('user:notfound')
  }

  public async getUser(id: UserId): Promise<UserModel> {
    const user = await this.strategy.getUser(id)

    if (user) {
      return user
    }

    throw new ErrorMessage('user:notfound')
  }
}
