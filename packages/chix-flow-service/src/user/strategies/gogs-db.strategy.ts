import {getRepository, Repository} from 'typeorm'

import {Injectable, Logger} from '@nestjs/common'
import {ErrorMessage} from '@nestling/errors'

import {UserModel} from '@chix/common'
import {User} from '../../gogs/entities/user.entity'
import {StartableInterface} from '../../interfaces'
import {UserStrategyInterface} from './user.strategy.interface'

@Injectable()
export class GogsDbStrategy
  extends UserStrategyInterface
  implements StartableInterface
{
  public userRepository: Repository<User>
  private log = new Logger(GogsDbStrategy.name)

  public async start() {
    this.log.log('GogsDbStrategy Start.')

    this.userRepository = getRepository(User)
  }

  public async stop() {
    /*
    if (this.connection) {
      await this.connection.close()
    }
    */
  }

  public async preload(): Promise<void> {
    this.log.log('preloading user information')

    await this.cacheUserModels()
  }

  public async getUserByName(name: string): Promise<UserModel> {
    let user: UserModel = this.usersByName[name]

    if (!user) {
      const result = await this.userRepository.findOneBy({name})

      if (result) {
        user = {
          id: result.id,
          name: result.name,
        }
      }
    }

    if (user) {
      this.log.log('Found user information.')
      this.log.log(user)

      return user
    }

    throw new ErrorMessage('user:notfound')
  }

  public async getUser(id: number): Promise<UserModel> {
    let user = this.usersById[id]

    if (!user) {
      const result = await this.userRepository.findOneBy({id})

      if (result) {
        user = {
          id: result.id,
          name: result.name,
        }
      }
    }

    if (user) {
      this.log.log('Found user information.')
      this.log.log(user)

      return user
    }

    throw new ErrorMessage('user:notfound')
  }

  private async cacheUserModels() {
    const users = await this.userRepository.find()

    this.usersById = users.reduce((_users: {[id: number]: UserModel}, user) => {
      _users[user.id] = {
        id: user.id,
        name: user.name,
      }

      return _users
    }, {})

    this.usersByName = users.reduce(
      (_users: {[id: string]: UserModel}, user) => {
        _users[user.name] = {
          id: user.id,
          name: user.name,
        }

        return _users
      },
      {}
    )
  }
}
