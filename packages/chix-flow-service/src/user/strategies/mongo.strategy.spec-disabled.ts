import {Test} from '@nestjs/testing'
import {ConfigModule} from '@nestling/config'
import {ErrorMessage} from '@nestling/errors'
import {LoggerModule} from '@nestling/logger'
// tslint:disable-next-line:no-implicit-dependencies
import {expect} from 'chai'
import {Db} from 'mongodb'
import {DatabaseModule} from '../../database'
import {environment} from '../../environment/environment'
import {userErrors} from '../user.errors'
import {MongoStrategy} from './mongo.strategy'

ErrorMessage.addErrorMessages(userErrors)

const createTestUser = async (db: Db) => {
  await db.collection('users').insertOne({
    id: 1,
    username: 'testuser',
  })
}

const removeTestUser = async (db: Db) => {
  await db.collection('users').deleteOne({id: 1})
}

describe('Mongo Strategy', () => {
  let mongoStrategy: MongoStrategy

  before(async () => {
    const module = await Test.createTestingModule({
      imports: [
        ConfigModule.forRoot(environment),
        LoggerModule.forRoot({
          name: 'chix-test',
        }),
        DatabaseModule,
      ],
      providers: [MongoStrategy],
    }).compile()

    mongoStrategy = module.get<MongoStrategy>(MongoStrategy)
  })

  after(async () => {
    await removeTestUser(mongoStrategy.db)
    await mongoStrategy.mongo.close()
  })

  it('should start', async () => {
    await mongoStrategy.start()

    await createTestUser(mongoStrategy.db)
  })

  it('Can preload', async () => {
    await mongoStrategy.preload()
  })

  it('Can get user', async () => {
    const user = await mongoStrategy.getUser(1)

    expect(user.id).to.equal('test-user')
    expect(user.name).to.equal('testuser')
  })

  it('should stop', async () => {
    await mongoStrategy.stop()
  })
})
