import {UserId, UserModel} from '@chix/common'
import {Injectable, Logger} from '@nestjs/common'
import {ConfigService} from '@nestling/config'
import {ErrorMessage} from '@nestling/errors'
import * as caller from 'grpc-caller'
import {resolve} from 'path'
import {StartableInterface} from '../../interfaces'
import {MemcachedService} from '../../memcached'
import {UserStrategyInterface} from './user.strategy.interface'

@Injectable()
export class GrpcStrategy
  extends UserStrategyInterface
  implements StartableInterface
{
  private grpcUserService: any

  constructor(
    private config: ConfigService,
    private memcachedService: MemcachedService
  ) {
    super()
  }

  private log = new Logger(GrpcStrategy.name)

  public async start(): Promise<void> {
    await this.memcachedService.start()

    const {options} = (this.config as any).user

    this.grpcUserService = caller(
      `${options.host}:${options.port}`,
      resolve(__dirname, '../user.proto'),
      'UserService'
    )

    return
  }

  public async stop(): Promise<void> {
    return
  }

  public async preload(): Promise<void> {
    this.log.log('preloading user information')

    const users: UserModel[] = await this.grpcUserService.Preload({})

    // fill memcached.
    await Promise.all(
      users
        .map((user) => this.memcachedService.add(user.id.toString(), user))
        .concat(users.map((user) => this.memcachedService.add(user.name, user)))
    )
    this.log.log('cached all current user information')
  }

  public async getUserByName(name: string): Promise<UserModel> {
    this.log.log(`Retrieving user information for ${name}.`)

    let user = await this.memcachedService.get(name)

    if (!user) {
      user = await this.grpcUserService.GetId({name})
    }

    if (user) {
      this.log.log(user)

      return user
    }

    throw new ErrorMessage('user:notfound')
  }

  public async getUser(id: UserId): Promise<UserModel> {
    this.log.log('Retrieving user information by id.')

    let user = await this.memcachedService.get(id.toString())

    if (!user) {
      user = await this.grpcUserService.GetUsername({id})
    }

    if (user) {
      this.log.log(user)

      return user
    }

    throw new ErrorMessage('user:notfound')
  }
}
