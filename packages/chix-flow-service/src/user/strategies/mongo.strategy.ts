import {Inject, Injectable, Logger} from '@nestjs/common'
import {ConfigService} from '@nestling/config'
import {ErrorMessage} from '@nestling/errors'

import {Db, MongoClient} from 'mongodb'

import {UserId, UserModel} from '@chix/common'
import {StartableInterface} from '../../interfaces'
import {UserStrategyInterface} from './user.strategy.interface'

export interface UserType {
  _id?: string
  id: number
  username: string
}

@Injectable()
export class MongoStrategy
  extends UserStrategyInterface
  implements StartableInterface
{
  public db: Db
  constructor(
    private config: ConfigService,
    @Inject('MongoDbToken') public readonly mongo: MongoClient
  ) {
    super()
  }

  private log = new Logger(MongoStrategy.name)

  public async start() {
    this.log.log('MongoDB Adapter, selected database')
    this.db = await this.mongo.db((this.config as any).user.options.database)

    // TODO: watch change stream, and reload this.users

    /* Implement later. Only works on replica sets.
    const changeStream = this.db.collection('users').watch()

    changeStream.next(function(err, next) {
      if (err) return console.log(err)

      console.log('NEW CHANGE', next)
      this.cacheUserModels()
    })
    */

    return
  }

  public async stop() {
    return
  }

  public async preload(): Promise<void> {
    this.log.log('preloading user information')

    await this.cacheUserModels()
  }

  public async getUserByName(name: string): Promise<UserModel> {
    let user = this.usersByName[name]

    if (!user) {
      const result = await this.db
        .collection('users')
        .findOne<UserType>({username: name})

      if (result) {
        user = {
          id: result.id,
          name: result.username,
        }
      }
    }

    if (user) {
      this.log.log('Found user information.')
      this.log.log(user)

      return user
    }

    throw new ErrorMessage('user:notfound')
  }

  public async getUser(id: UserId): Promise<UserModel> {
    let user = this.usersById[id]

    if (!user) {
      const result = await this.db.collection('users').findOne<UserType>({id})

      if (result) {
        user = {
          id: result.id,
          name: result.username,
        }
      }
    }

    if (user) {
      this.log.log('Found user information.')
      this.log.log(user)

      return user
    }

    throw new ErrorMessage('user:notfound')
  }

  private async cacheUserModels() {
    const collection = this.db.collection('users')

    const users = await collection
      .find({}, {
        _id: 0,
        id: 1,
        username: 1,
      } as any)
      .toArray()

    this.usersById = users.reduce((_users: {[id: number]: UserModel}, user) => {
      _users[user.id] = {
        id: user.id,
        name: user.username,
      }

      return _users
    }, {})

    this.usersByName = users.reduce(
      (_users: {[id: string]: UserModel}, user) => {
        _users[user.username] = {
          id: user.id,
          name: user.username,
        }

        return _users
      },
      {}
    )
  }
}
