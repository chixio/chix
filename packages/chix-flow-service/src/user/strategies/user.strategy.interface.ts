import {UserModel} from '@chix/common'

export type CachedUserModelsById = {
  [id: number]: UserModel
}

export type CachedUserModelsByName = {
  [id: string]: UserModel
}

export abstract class UserStrategyInterface {
  public usersByName: CachedUserModelsByName = {}
  public usersById: CachedUserModelsById = {}
  public abstract preload(): Promise<void>
  public abstract getUser(id: number): Promise<UserModel>
  public abstract getUserByName(name: string): Promise<UserModel>
}
