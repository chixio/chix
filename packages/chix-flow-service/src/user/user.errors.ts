import {IErrorMessages} from '@nestling/errors'

export const userErrors: IErrorMessages = {
  type: 'user',
  errors: [
    {
      code: 'invalid',
      statusCode: 403,
      message: 'Invalid user.',
    },
    {
      code: 'notfound',
      statusCode: 403,
      message: 'User not found.',
    },
  ],
}
