export function ucfirst(str: string) {
  return str[0].toUpperCase().concat(str.slice(1))
}
