export interface JWTModel {
  sub: string
  username: string
  roles?: string[]
}
