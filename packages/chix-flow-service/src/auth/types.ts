export type JWTConfig = {
  jwt: {
    contextKey?: string
    secret: string
    header?: string
  }
}
