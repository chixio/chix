import {CanActivate, ExecutionContext, Injectable, Logger} from '@nestjs/common'
import {ConfigService} from '@nestling/config'
import {setContext} from '@nestling/context'
import * as jwt from 'jsonwebtoken'

import {DataSource} from 'typeorm'
// import * as uuid from 'uuid'
import {AccessToken} from '../gogs/entities/access_token'
import {JWTConfig} from './types'

@Injectable()
export class AuthGuard implements CanActivate {
  constructor(private config: ConfigService, private dataSource: DataSource) {}

  private logger = new Logger(AuthGuard.name)

  public async canActivate(
    executionContext: ExecutionContext
  ): Promise<boolean> {
    const {
      jwt: {contextKey = 'jwt', secret, header = 'authorization'},
    }: JWTConfig = this.config as any

    const httpContext = executionContext.switchToHttp()
    const request = httpContext.getRequest()
    const {headers} = request

    if (typeof headers[header] === 'string') {
      const parts = headers[header].split(' ')

      if (parts[0] === 'Bearer') {
        const token = parts[1]
        const decoded: any = jwt.verify(token, secret)

        setContext(contextKey, decoded, request)
        setContext('Authorization', headers[header], request)
        setContext(
          'logger',
          this.logger,
          // this.logger.child({reqId: uuid.v4(), uid: decoded.sub}),
          request
        )

        return true
      }

      if (parts[0] === 'token') {
        const accessTokenRepository = this.dataSource.getRepository(AccessToken)

        const accessToken = await accessTokenRepository.findOneBy({
          sha1: parts[0],
        })

        if (accessToken) {
          setContext('token', accessToken, request)
          setContext('Authorization', headers[header], request)

          setContext(
            'logger',
            this.logger,
            /*
            this.logger.child({
              reqId: uuid.v4(),
              uid: accessToken.uid || 'nouid',
            }),
             */
            request
          )

          return true
        }
      }
    }

    // authorization is done by User(required: true) decorator
    return true
    // throw new ErrorMessage('auth:unauthorized')
  }
}
