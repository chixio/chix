// tslint:disable-next-line:no-implicit-dependencies
import {expect} from 'chai'
import {objEnv} from 'objenv'
import {getRepository} from 'typeorm'
import {environment} from '../environment/environment.test'
import {User} from '../gogs/entities/user.entity'
import {generateFakeAccessToken} from '../support'
import {connectDb} from '../support/typeorm/connectDb'
import {GogsClient} from './GogsClient'

const config: any = objEnv(environment)

describe('GogsClient', () => {
  let client: GogsClient
  let testUser: User

  before(async () => {
    await connectDb()

    const userRepository = getRepository(User)

    const result = await userRepository.findOne({
      where: {
        name: 'test-user',
      },
    })

    if (result) {
      testUser = result
      const jwt = await generateFakeAccessToken(
        testUser.id,
        testUser.name,
        config.jwt.secret
      )

      // ok this can be user token OR jwt.
      client = new GogsClient(config.gogs.api.url)
      client.setAuthorization(`Bearer ${jwt}`)
      // client.setAuthorization('token', config.test.user.token)
    } else {
      throw Error(`Unable to find test-user`)
    }
  })

  it('Client can tell if repo does not exists', async () => {
    const result = await client.hasRepo(testUser.name, 'test-repo')

    expect(result).to.eql(false)
  })

  it('User can create repo', async () => {
    await client.createRepo('test-repo', 'My First Chix Repo', true)
  })

  it('Can tell if repo does exists', async () => {
    expect(await client.hasRepo(testUser.name, 'test-repo')).to.eql(true)
  })

  it('User can remove repo', async () => {
    await client.deleteRepo(testUser.name, 'test-repo')
  })

  it('Can tell if repo does not exists', async () => {
    expect(await client.hasRepo(testUser.name, 'test-repo')).to.eql(false)
  })
})
