import {Logger} from '@nestjs/common'
import {ContextService} from '@nestling/context'
import assert from 'assert'
import {objEnv} from 'objenv'
import {environment} from '../environment/environment'
import {RepoManager} from './RepoManager'

export function getRepoManager(context: ContextService, user: string) {
  const config: any = objEnv(environment)

  const authorization = context.get('Authorization')

  assert(authorization, 'Authorization header not found')

  const {host, port, user: gitUser} = config.gogs.git

  const gitUrl = `ssh://${gitUser}@${host}:${port}`
	  
  const logger = new Logger('getRepoManager')

  logger.log('gitUrl', gitUrl)

  return new RepoManager({
    apiUrl: `${config.gogs.api.url}`,
    cloneDir: config.gogs.git.cloneDir,
    gitUrl,
    token: config.gogs.api.token,
    user,
    authorization,
  })
}
