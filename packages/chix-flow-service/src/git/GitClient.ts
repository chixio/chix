import {Flow as FlowDefinition, NodeDefinition} from '@chix/common'
import {ChixParser} from '@chix/fbpx'
import {convert as convertToFbpx} from '@chix/flow-tofbpx'
import {Injectable, Logger} from '@nestjs/common'
import * as fs from 'fs-extra'
import {glob} from 'glob'
import * as yaml from 'js-yaml'
import * as path from 'path'
// tslint:disable-next-line:no-submodule-imports
import * as git from 'simple-git'

import {ucfirst} from '../util'

export interface GitClientOptions {
  user: string
  gitUrl: string
  cloneDir?: string
}

/**
 * Used by autoclean to remove any files which do not match the expected patterns.
 *
 * TODO: enforce this structure with a githook, rejecting any files which do not match the expected fileSystemStructure.
 *
 * @type {RegExp[]}
 */
const fileStructure: RegExp[] = [
  /^nodes\/.*\/node\.(js|ts|json)/,
  /^flows\/.*\.(fbp|yml|json)/,
  /^twigs\/.*\.(fbp|yml|json)/,
  /^package.json/,
  /^x.json/,
]
@Injectable()
export class GitClient {
  private static getStorageFormat(type: string): 'yml' | 'fbp' {
    if (type === 'node') {
      return 'yml'
    }

    if (type === 'flow' || type === 'twig') {
      return 'fbp'
    }

    throw Error(`Unknown type: ${type}`)
  }

  private static toYaml(nodeDefinition: NodeDefinition | FlowDefinition) {
    return yaml.dump(nodeDefinition, {
      indent: 2,
      skipInvalid: false,
      flowLevel: Infinity,
      // schema: (yaml as any).DEFAULT_SAFE_SCHEMA
    })
  }

  private static async writeFile(
    type: string,
    file: string,
    nodeDefinition: NodeDefinition | FlowDefinition
  ): Promise<string> {
    let fileName: string
    switch (GitClient.getStorageFormat(type)) {
      // case 'json':
      //   await fs.writeJSON(`${file}.json`, nodeDefinition)
      //   break
      case 'yml':
        fileName = `${file}.yml`
        await fs.writeFile(fileName, GitClient.toYaml(nodeDefinition))
        break
      case 'fbp':
        fileName = `${file}.fbp`
        await fs.writeFile(
          fileName,
          convertToFbpx(nodeDefinition as FlowDefinition)
        )
        break
      default:
        throw Error('Unknown storage format.')
    }

    return fileName
  }

  private static fromYaml(contents: string) {
    return yaml.load(contents)
  }
  get targetDir() {
    return `${this.cloneDir}/${this.user}/${this.repo}`
  }

  get nodesDir() {
    return `${this.targetDir}/nodes`
  }

  get flowsDir() {
    return `${this.targetDir}/flows`
  }

  get twigsDir() {
    return `${this.targetDir}/twigs`
  }
  private cloneDir: string
  private user: string
  private repo: string | null = null
  private isCloned: boolean = false
  private gitUrl: string
  private client: any
  private logger = new Logger(GitClient.name)

  constructor(options: GitClientOptions) {
    this.user = options.user
    this.gitUrl = options.gitUrl
    this.cloneDir = options.cloneDir || path.resolve(__dirname, '../../tmp')
  }

  /**
   * Strictly removes all files not belonging to the predefined repository structure
   */
  public async autoclean() {
    await this.openRepository()

    process.chdir(this.targetDir)

    if (process.cwd() !== this.targetDir) {
      throw Error('Refusing to continue')
    }

    const files = await glob(`**/*.*`)

    const filesToBeRemoved = files.filter((file: string) => {
      return !fileStructure.find((regexp) => {
        return regexp.test(file)
      })
    })

    if (filesToBeRemoved.length) {
      await Promise.all(filesToBeRemoved.map((file: string) => fs.remove(file)))

      const client = git(this.targetDir).silent(false)

      await client.add(filesToBeRemoved)

      await client.commit(`[Node][autoclean]`, filesToBeRemoved)

      await client.push('origin', 'master')
    }

    process.chdir(this.cloneDir)
  }

  public async cleanCloneDir() {
    this.logger.log('Removing cloneDir %s', this.cloneDir)

    await fs.remove(this.cloneDir)
  }

  public async openRepository(repo?: string) {
    if (repo) {
      if (this.repo && repo !== this.repo) {
        throw Error(
          `Cannot open multiple repositories. still open: ${this.repo}, to be opened: ${repo}`
        )
      }

      this.repo = repo
    }

    if (!this.repo) {
      throw Error('No repository opened.')
    }

    if (!this.isCloned) {
      const remote = `${this.gitUrl}/${this.user}/${this.repo}`
      this.client = git().silent(false)

      await this.closeRepository()

      const cloneData = await this.client.clone(remote, this.targetDir, [
        // '--depth 1'
      ])

      this.logger.log(cloneData)

      this.client = git(this.targetDir).silent(false)

      this.isCloned = true

      this.logger.log('Opened repository %s', this.repo)
    } else {
      this.logger.log('Re-opened repository %s', this.repo)
    }
  }

  public async closeRepository() {
    if (this.isCloned) {
      const status = await this.client.status()

      if (status.staged.length) {
        this.logger.log(`Pushing ${status.staged.length} files to ${this.repo}`)

        await this.client.push('origin', 'master')
      } else {
        this.logger.log(`No staged files found for ${this.repo}`)

        try {
          await this.client.push('origin', 'master')
          this.logger.log(`Initial push was successful for ${this.repo} `)
        } catch (_error) {
          this.logger.error(_error)
        }
      }

      await this.unmountRepository()

      this.logger.log('Closed repository %s', this.repo)

      this.repo = null
    }
  }

  public async unmountRepository() {
    const exists = await fs.pathExists(this.targetDir)

    if (exists) {
      this.logger.log(`Removing ${this.targetDir}`)
      await fs.remove(this.targetDir)
    }

    this.isCloned = false
  }

  /////////////
  // Nodes
  /////////////
  public async getNode(name: string): Promise<NodeDefinition> {
    await this.openRepository()

    const nodeDir = `${this.nodesDir}/${name}`
    const nodeFile = `${nodeDir}/node`

    return this.readFile('node', nodeFile)
  }

  public async putNode(nodeDefinition: NodeDefinition) {
    return this._putNode('node', nodeDefinition)
  }

  public async putNodes(nodeDefinitions: NodeDefinition[]) {
    return this._putNodes('node', nodeDefinitions)
  }

  public async removeNode(name: string) {
    return this._removeNode('node', name)
  }

  public async removeNodes(names: string[]) {
    return this._removeNodes('node', names)
  }

  public async renameNode(oldName: string, newName: string) {
    return this._renameNode('node', oldName, newName)
  }

  /////////////
  // Flows
  /////////////
  public async getFlow(name: string): Promise<FlowDefinition> {
    await this.openRepository()

    const nodeDir = `${this.flowsDir}/${name}`
    const nodeFile = `${nodeDir}/flow`

    return this.readFile('flow', nodeFile)
  }

  public async putFlow(nodeDefinition: FlowDefinition) {
    return this._putNode('flow', nodeDefinition)
  }

  public async putFlows(nodeDefinitions: FlowDefinition[]) {
    return this._putNodes('flow', nodeDefinitions)
  }

  public async removeFlow(name: string) {
    return this._removeNode('flow', name)
  }

  public async removeFlows(names: string[]) {
    return this._removeNodes('flow', names)
  }

  public async renameFlow(oldName: string, newName: string) {
    return this._renameNode('flow', oldName, newName)
  }

  /////////////
  // Twigs
  /////////////
  public async getTwig(name: string): Promise<FlowDefinition> {
    await this.openRepository()

    const nodeDir = `${this.twigsDir}/${name}`
    const nodeFile = `${nodeDir}/twig`

    return this.readFile('twig', nodeFile)
  }

  public async putTwig(nodeDefinition: FlowDefinition) {
    return this._putNode('twig', nodeDefinition)
  }

  public async putTwigs(nodeDefinitions: FlowDefinition[]) {
    return this._putNodes('twig', nodeDefinitions)
  }

  public async removeTwig(name: string) {
    return this._removeNode('twig', name)
  }

  public async removeTwigs(names: string[]) {
    return this._removeNodes('twig', names)
  }

  public async renameTwig(oldName: string, newName: string) {
    return this._renameNode('twig', oldName, newName)
  }

  // in a repo ns is always considered to be the repo name.
  // Flow *can* include nodes from other namespaces but the noeDefinitions themselves
  // *must* be namespaced to the repo name
  // the name can have slashes in them, which will cause grouping e.g. header/button
  public async _removeNode(type: string, name: string) {
    await this.openRepository()

    const parts = name.split('/')

    const nodeDir = path.resolve(`${this.targetDir}/${type}s`, ...parts)

    const exists = await fs.pathExists(nodeDir)

    if (exists) {
      await fs.remove(nodeDir)

      await this.client.add([nodeDir])

      await this.client.commit(`[${ucfirst(type)}][remove] ${name}`, [nodeDir])
    }
  }

  public async _removeNodes(type: string, names: string[]) {
    await this.openRepository()

    await Promise.all(names.map((name) => this._removeNode(type, name)))
  }

  public async _renameNode(type: string, oldName: string, newName: string) {
    await this.openRepository()

    this.logger.log(`Rename ${type}: ${oldName} > ${newName}`)

    const oldParts = oldName.split('/')
    const newParts = newName.split('/')

    const oldNodeDir = path.resolve(`${this.targetDir}/${type}s`, ...oldParts)
    const newNodeDir = path.resolve(`${this.targetDir}/${type}s`, ...newParts)

    this.logger.log(`Moving ${oldNodeDir} > ${newNodeDir}`)

    const oldNodeExists = await fs.pathExists(oldNodeDir)

    if (oldNodeExists) {
      await this.client.mv(oldNodeDir, newNodeDir)

      await this.client.commit(
        `[${ucfirst(type)}][move] ${oldName} -> ${newName}`,
        [oldNodeDir, newNodeDir]
      )
    }
  }

  public async tag(version: string) {
    await this.openRepository()

    await this.client.addTag(version)
    await this.client.pushTags('origin')
  }

  ///////////////////////////////////
  // Private Generic Node Methods
  ///////////////////////////////////
  private async _putNode(
    type: string,
    nodeDefinition: NodeDefinition | FlowDefinition
  ) {
    await this.openRepository()

    this.logger.log(`Put ${type} ${nodeDefinition.ns}:${nodeDefinition.name}`)

    const nodeDir = `${this.targetDir}/${type}s/${nodeDefinition.name}`
    const nodeFile = `${nodeDir}/${type}`

    await fs.mkdirp(nodeDir)

    const fileName = await GitClient.writeFile(type, nodeFile, nodeDefinition)

    await this.client.raw(['add', `${fileName}`])

    const identifier = `${nodeDefinition.ns}:${nodeDefinition.name}`

    const commitMessage = `[${ucfirst(type)}][add] ${identifier}`

    this.logger.log(commitMessage)

    const commitSummary = await this.client.commit(commitMessage, [
      `${fileName}`,
    ])

    await this.client.push('origin', 'master')

    this.logger.log(commitSummary)
  }

  private async _putNodes(
    type: string,
    nodeDefinitions: (NodeDefinition | FlowDefinition)[]
  ) {
    await this.openRepository()

    await Promise.all(
      nodeDefinitions.map((nodeDefinition) =>
        this._putNode(type, nodeDefinition)
      )
    )
  }

  ////////////////////////////////////////////////////////

  private async readFile(type: string, file: string) {
    let contents

    switch (GitClient.getStorageFormat(type)) {
      // case 'json':
      //   contents = await fs.readJSON(`${file}.json`)
      //   break
      case 'yml':
        contents = GitClient.fromYaml(
          fs.readFile(`${file}.yml`).toString()
        )
        break
      case 'fbp':
        contents = ChixParser({
          defaultProvider: `https://api.chix.io/v1/nodes/${this.user}/{ns}/{name}`,
        }).parse(await fs.readFile(`${file}.fbp`))
        break
      default:
        throw Error('Unknown storage format.')
    }

    return contents
  }
}
