import {Injectable} from '@nestjs/common'
import * as request from 'superagent'

export interface Repo {
  name: string
  description?: string
  private?: boolean
}

export interface UserPayload {
  source_id: number
  login_name: string
  full_name: string
  email: string
  password: string
  website: string
  location: string
  active: boolean
  admin: boolean
  allow_git_hook: boolean
  allow_import_local: boolean
}

/**
 * GogsClient
 *
 * Expects to be instantiated using the users token.
 */
@Injectable()
export class GogsClient {
  private url = 'https://repos.chix.io/api/v1'
  private authorization: string = ''
  private agent: request.SuperAgentStatic & request.Request

  constructor(url: string) {
    this.url = url

    this.agent = (request as any).agent()
  }

  public async createPublicKey(title: string, key: string) {
    return this.makeRequest()
      .post(`${this.url}/users/keys`)
      .send({
        title,
        key,
      })
      .then(({body}) => body)
  }

  public async listPublicKeys(username?: string) {
    if (username) {
      return this.makeRequest()
        .get(`${this.url}/users/${username}/keys`)
        .then(({body}) => body)
    }

    return this.makeRequest()
      .get(`${this.url}/user/keys`)
      .then(({body}) => body)
  }

  public async deletePublicKey(id: string) {
    return this.makeRequest()
      .delete(`${this.url}/user/keys/${id}`)
      .then(({body}) => body)
  }

  /**
   * Gets the nodeDefinition.
   *
   * name can be a path, but normally just a name.
   *
   * Specify ref to get a different version of the file (if it exists)
   *
   * @param {string} owner
   * @param {string} repo
   * @param {string} name
   * @param {string} ref
   * @returns {Promise<PromiseLike<{body: any}> | Promise<{body: any}>>}
   */
  public async getNodeDefinition(
    owner: string,
    repo: string,
    name: string,
    ref: string = 'master'
  ) {
    return this.makeRequest()
      .get(
        `${this.url}/repos/${owner}/${repo}/raw/${ref}/nodes/${name}/node.json`
      )
      .then(({body}) => body)
  }

  public async hasNodeDefinition(
    owner: string,
    repo: string,
    name: string,
    ref: string = 'master'
  ): Promise<boolean> {
    return this.makeRequest()
      .get(
        `${this.url}/repos/${owner}/${repo}/raw/${ref}/nodes/${name}/node.json`
      )
      .ok((res) => res.status === 404 || res.status === 200)
      .then((res) => res.status === 200)
  }

  public async getTwig(
    owner: string,
    repo: string,
    name: string,
    ref: string = 'master'
  ) {
    return this.makeRequest()
      .get(`${this.url}/repos/${owner}/${repo}/raw/${ref}/twigs/${name}.json`)
      .then(({body}) => body)
  }

  public async hasTwig(
    owner: string,
    repo: string,
    name: string,
    ref: string = 'master'
  ): Promise<boolean> {
    return this.makeRequest()
      .get(`${this.url}/repos/${owner}/${repo}/raw/${ref}/twigs/${name}.json`)
      .ok((res) => res.status === 404 || res.status === 200)
      .then((res) => res.status === 200)
  }

  public async getFlow(
    owner: string,
    repo: string,
    name: string,
    ref: string = 'master'
  ) {
    return this.makeRequest()
      .get(`${this.url}/repos/${owner}/${repo}/raw/${ref}/flows/${name}.fbp`)
      .then(({body}) => body)
  }

  public async hasFlow(
    owner: string,
    repo: string,
    name: string,
    ref: string = 'master'
  ): Promise<boolean> {
    return this.makeRequest()
      .get(`${this.url}/repos/${owner}/${repo}/raw/${ref}/flows/${name}.fbp`)
      .ok((res) => res.status === 404 || res.status === 200)
      .then((res) => res.status === 200)
  }

  public async getRepo(owner: string, repo: string) {
    return this.makeRequest()
      .get(`${this.url}/repos/${owner}/${repo}`)
      .then(({body}) => body)
  }

  public async hasRepo(owner: string, repo: string): Promise<boolean> {
    return this.makeRequest()
      .head(`${this.url}/repos/${owner}/${repo}`)
      .ok((res) => res.status === 404 || res.status === 200)
      .then((res) => res.status === 200)
  }

  public async clearRepos(owner: string): Promise<boolean> {
    const repos = await this.getRepos()

    await Promise.all(repos.map((repo) => this.deleteRepo(owner, repo.name)))

    return true
  }

  public async getRepos(): Promise<Repo[]> {
    return this.makeRequest()
      .get(`${this.url}/user/repos`)
      .then(({body}) => body)
  }

  public async createRepo(
    name: string,
    description?: string,
    priv: boolean = true
  ) {
    return this.makeRequest()
      .post(`${this.url}/user/repos`)
      .send({
        name,
        description,
        private: priv,
      })
      .then(({body}) => body)
  }

  // also generate the typescript interfaces for the gogs api payload
  public async renameRepo(owner: string, oldName: string, newName: string) {
    return this.makeRequest()
      .put(`${this.url}/repos/${owner}/${oldName}/rename`)
      .send({newName})
      .then(({body}) => body)
  }

  public async deleteRepo(owner: string, repo: string) {
    return this.makeRequest()
      .delete(`${this.url}/repos/${owner}/${repo}`)
      .then(({body}) => body)
  }

  public setAuthorization(authorization: string) {
    this.authorization = authorization
  }

  private makeRequest() {
    return this.agent.set('Authorization', this.authorization)
  }
}
