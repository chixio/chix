import {GitClient} from './GitClient'
;(async () => {
  const nodeDefinition: any = {
    title: 'First Node',
    ns: 'test-repo',
    name: 'myNode',
    description: 'Test Node',
    ports: {
      input: {
        in: {
          type: 'string',
        },
      },
      output: {
        out: {
          type: 'string',
        },
      },
    },
    updatedAt: Date.now(),
  }

  const someFlow: any = {
    title: 'My Flow',
    ns: 'test',
    name: 'my-flow',
    nodes: [
      {
        id: 'whatever2',
        ns: 'test-repo',
        name: 'myNode',
      },
      {
        id: 'whatever',
        ns: 'test-repo',
        name: 'otherNode/has/a/slash',
      },
    ],
    links: [
      {
        source: {
          id: 'whatever2',
          port: 'out',
        },
        target: {
          id: 'whatever',
          port: 'in',
        },
      },
    ],
  }

  const nestedDefinition: any = {
    title: 'Nested Node',
    ns: 'test-repo', // err this should not be possible. project must match git repo
    name: 'otherNode/has/a/slash',
    description: 'Test for deeply nested',
    ports: {
      input: {
        in: {
          type: 'string',
        },
      },
      output: {
        out: {
          type: 'string',
        },
      },
    },
    updatedAt: Date.now(),
  }

  const client = new GitClient({
    user: 'rhalff',
    gitUrl: '',
    cloneDir: '',
    logger: console.log.bind(console) as any,
  })

  console.log('Open Repository', await client.openRepository())

  await client.autoclean()

  console.log('Put Node', await client.putNode(nodeDefinition))
  // console.log('Remove Node', await client.removeNode(nodeDefinition.name))

  console.log(
    'Put Nodes',
    await client.putNodes([nodeDefinition, nestedDefinition])
  )
  console.log('Put Flow', await client.putFlows([someFlow]))
  // console.log('Rename Node', await client.renameNode(nodeDefinition.name, 'brandNewName'))

  console.log('Tag', await client.tag('v' + nodeDefinition.updatedAt))
  // console.log('Remove nodes', await client.removeNodes(['brandNewName']))
  console.log('Close Repository', await client.closeRepository())
})()
