import {Injectable} from '@nestjs/common'
import * as request from 'superagent'

export interface Repo {
  name: string
  description?: string
  private?: boolean
}

export interface UserPayload {
  source_id: number
  login_name: string
  full_name: string
  email: string
  password: string
  website: string
  location: string
  active: boolean
  admin: boolean
  allow_git_hook: boolean
  allow_import_local: boolean
}

export interface CreateRepoPayload {
  name: string
  description: string
  private: boolean
  auto_init: boolean
  gitignores: string
  license: string
  readme: string
}

/**
 * GogsAdminClient
 *
 * Expects to be instantiated using the admin token.
 */
@Injectable()
export class GogsAdminClient {
  private url = 'https://repos.chix.io/api/v1'
  private readonly token: string

  constructor(url: string, adminToken: string) {
    this.token = adminToken
    this.url = url
  }

  public async createUser(
    username: string,
    email: string,
    password: string,
    sendNotify: boolean = false
  ) {
    return this.makeRequest()
      .post(`${this.url}/admin/users`)
      .send({
        username,
        email,
        password,
        send_notify: sendNotify,
      })
      .then(({body}) => body)
  }

  public async editUser(username: string, updates: Partial<UserPayload>) {
    return this.makeRequest()
      .patch(`${this.url}/admin/users/${username}`)
      .send(updates)
      .then(({body}) => body)
  }

  public async deleteUser(username: string) {
    return this.makeRequest()
      .delete(`${this.url}/admin/users/${username}`)
      .then(({body}) => body)
  }

  public async createPublicKey(username: string, title: string, key: string) {
    return this.makeRequest()
      .post(`${this.url}/admin/users/${username}/keys`)
      .send({
        title,
        key,
      })
      .then(({body}) => body)
  }

  /**
   * Creates a new repository.
   *
   * TODO:repo does not seem to be initialized, probably a gogs issue.
   *
   * @param username
   * @param name
   * @param description
   * @param priv
   */
  public async createRepo(
    username: string,
    name: string,
    description?: string,
    priv: boolean = true
  ) {
    return this.makeRequest()
      .post(`${this.url}/admin/users/${username}/repos`)
      .send({
        name,
        description,
        private: priv,
        auto_init: true,
        gitignores: 'Node',
        license: 'MIT License',
        readme: 'Default',
      })
      .then(({body}) => body)
  }

  private makeRequest() {
    return request.agent().set('Authorization', `token ${this.token}`)
  }
}
