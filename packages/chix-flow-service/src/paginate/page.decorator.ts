import {createParamDecorator} from '@nestjs/common'

export interface Pagination {
  page?: number
  perPage?: number
}

export interface PaginationOptions {
  perPage: number
  perPageLimit: number
}

export const Page = createParamDecorator(
  (
    options: PaginationOptions = {
      perPage: 10,
      perPageLimit: 100,
    },
    request
  ): Pagination => {
    const {page, per_page} = request.query

    if (page) {
      const perPage = per_page ? parseInt(per_page, 10) : options.perPage

      return {
        page: parseInt(page, 10),
        perPage:
          perPage > options.perPageLimit ? options.perPageLimit : perPage,
      }
    }

    return {}
  }
)
