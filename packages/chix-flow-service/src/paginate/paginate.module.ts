import {Global, Module} from '@nestjs/common'
import {APP_INTERCEPTOR} from '@nestjs/core'
import {ContextModule} from '@nestling/context'
import {LinksInterceptor} from './links.interceptor'

const linksInterceptor = {
  provide: APP_INTERCEPTOR,
  useClass: LinksInterceptor,
}

const providers = [ContextModule, linksInterceptor]

@Global()
@Module({
  providers,
})
export class PaginateModule {}
