import * as qs from 'qs'
import {PaginationContext} from '../adapter/mongo.adapter'

export interface QueryParams {
  [name: string]: any
}

export function createPaginationLinks(
  pagination: PaginationContext,
  path: string,
  params: QueryParams
): string[] {
  const {page, perPage, total} = pagination

  if (total > 0 && page && perPage > 0) {
    const serverUrl = `https://api.chix.io`

    const links: string[] = []

    const commonParams = {
      ...params,
      per_page: perPage,
    }

    links.push(
      `<${serverUrl}/${path}?${qs.stringify({
        ...commonParams,
        page: 1,
      })}>; rel="first"`
    )

    if (page > 1) {
      links.push(
        `<${serverUrl}/${path}?${qs.stringify({
          ...commonParams,
          page: page - 1,
        })}>; rel="prev"`
      )
    }

    if ((page + 1) * perPage < total) {
      links.push(
        `<${serverUrl}/${path}?${qs.stringify({
          ...commonParams,
          page: page + 1,
        })}>; rel="next"`
      )
    }

    links.push(
      `<${serverUrl}/${path}?${qs.stringify({
        ...commonParams,
        page: Math.ceil(total / perPage),
      })}>; rel="last"`
    )

    return links
  }

  return []
}
