// tslint:disable-next-line:no-implicit-dependencies
import {expect} from 'chai'
import {PaginationContext} from '../adapter/mongo.adapter'
import {createPaginationLinks, QueryParams} from './createPaginationLinks'

describe('CreatePaginationLinks', () => {
  it('Paginates links', async () => {
    const pagination: PaginationContext = {
      page: 1,
      perPage: 10,
      total: 100,
    }

    const path = 'some-path'
    const params: QueryParams = {
      one: 1,
      two: 2,
    }

    const links = createPaginationLinks(pagination, path, params)

    expect(links).to.deep.equal([
      '<https://api.chix.io/some-path?one=1&two=2&per_page=10&page=1>; rel="first"',
      '<https://api.chix.io/some-path?one=1&two=2&per_page=10&page=2>; rel="next"',
      '<https://api.chix.io/some-path?one=1&two=2&per_page=10&page=10>; rel="last"',
    ])
  })
})
