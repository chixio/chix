import {
  CallHandler,
  ExecutionContext,
  Injectable,
  NestInterceptor,
} from '@nestjs/common'
import {getContext} from '@nestling/context'
import {Observable} from 'rxjs'
// tslint:disable-next-line:no-submodule-imports
import {tap} from 'rxjs/operators'
import {PaginationContext} from '../adapter/mongo.adapter'
import {createPaginationLinks} from './createPaginationLinks'

@Injectable()
export class LinksInterceptor implements NestInterceptor {
  public intercept(
    context: ExecutionContext,
    next: CallHandler
  ): Observable<any> {
    return next.handle().pipe(
      tap(() => {
        const httpContext = context.switchToHttp()

        const request = httpContext.getRequest()
        const response = httpContext.getResponse()

        const pagination = getContext<PaginationContext>('pagination', request)

        if (pagination) {
          const paginationLinks = createPaginationLinks(
            pagination,
            request.path,
            request.params
          )

          if (paginationLinks.length) {
            response.setHeader('Link', paginationLinks.join(', '))
          }
        }
      })
    )
  }
}
