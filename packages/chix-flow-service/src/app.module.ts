import {Module} from '@nestjs/common'
import {APP_FILTER, APP_GUARD} from '@nestjs/core'
import {TypeOrmModule} from '@nestjs/typeorm'
import {objEnv} from 'objenv'
import {environment} from './environment/environment'
import {User} from './gogs/entities/user.entity'
import {FlowModule} from './modules/flows'
import {NodeModule} from './modules/nodes'
import {ProjectModule} from './modules/projects'
import {RuntimeModule} from './modules/runtimes'
import {SharedModule} from './shared/shared.module'

import {ErrorMessage, HttpExceptionFilter} from '@nestling/errors'
// tslint:disable-next-line:no-submodule-imports
import {jsonWebTokenErrorHandler} from '@nestling/errors/dist/handlers/jsonWebTokenError'
// tslint:disable-next-line:no-submodule-imports
import {validationErrorHandler} from '@nestling/errors/dist/handlers/validationError'
import {authErrors, AuthGuard} from './auth'
import {PaginateModule} from './paginate'

HttpExceptionFilter.addExceptionHandler(validationErrorHandler)
HttpExceptionFilter.addExceptionHandler(jsonWebTokenErrorHandler)
ErrorMessage.addErrorMessages(authErrors)

const {mysql} = objEnv(environment) as any
const {host, port, username, password, database, logging} = mysql

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'mysql',
      host,
      port,
      username,
      password,
      database,
      entities: [User],
      bigNumberStrings: false,
      logging,
    }),
    SharedModule,
    PaginateModule,
    FlowModule,
    NodeModule,
    ProjectModule,
    RuntimeModule,
  ],
  providers: [
    {
      provide: APP_GUARD,
      useClass: AuthGuard,
    },
    {
      provide: APP_FILTER,
      useClass: HttpExceptionFilter,
    },
  ],
})
export class ApplicationModule {}
