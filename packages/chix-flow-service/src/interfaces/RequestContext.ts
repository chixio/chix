import {ILogger} from '@chix/common'
import {PaginationContext} from '../adapter/mongo.adapter'
import {JWTModel} from '../jwt.model'

/**
 * Represents a fully setup context.
 *
 * Context is build by the auth guard.
 *
 * Its construction is dynamic yet this can be used to type the context in controllers.
 */
export interface RequestContext {
  Authorization?: string
  jwt?: JWTModel
  token?: string
  logger: ILogger
  pagination?: PaginationContext
}
