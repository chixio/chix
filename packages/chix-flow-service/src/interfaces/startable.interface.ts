export interface StartableInterface {
  start(): Promise<void>
  stop(): Promise<void>
}
