import {NestFactory} from '@nestjs/core'
import {Transport} from '@nestjs/microservices'
import {DocumentBuilder, SwaggerModule} from '@nestjs/swagger'
import {ConfigService} from '@nestling/config'
import {whitelist} from '@nestling/cors'
import * as bodyParser from 'body-parser'
import {DataSource} from 'typeorm'
import {ApplicationModule} from './app.module'
import {environment} from './environment/environment'
import {User} from './gogs/entities/user.entity'
import {ensureTestUser} from './support'

async function bootstrap() {
  const app = await NestFactory.create(ApplicationModule)

  const config = app.get<ConfigService>(ConfigService)
  const dataSource = app.get(DataSource)

  if (process.env.ENV === 'test') {
    const userRepository = dataSource.getRepository(User)
    const testUser = await ensureTestUser(userRepository)

    console.log(`Test Mode, test user is:`, testUser)
  }

  const options = new DocumentBuilder()
    .setTitle('Chiχ Flow API')
    // .setDescription('')
    .setVersion('1.0')
    .addTag('Chiχ', 'flow')
    .build()

  const document = SwaggerModule.createDocument(app, options)

  SwaggerModule.setup('openapi', app, document)

  app.use(
    bodyParser.json({
      limit: '100mb',
    })
  )

  app.enableCors(whitelist(environment.whitelist))

  const natsHost = (config as any).nats.host
  const natsPort = (config as any).nats.port

  app.connectMicroservice({
    transport: Transport.NATS,
    options: {
      url: `nats://${natsHost}:${natsPort}`,
      // queue: 'my-queue',
      // user: process.env.NATS_USER,
      // pass: process.env.NATS_PASS,
    },
  })
  await app.startAllMicroservices()
  await app.listen(environment.port || 2303)
}
bootstrap().catch((error) => {
  console.error(error)
})
