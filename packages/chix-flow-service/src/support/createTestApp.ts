import {Logger, INestApplication} from '@nestjs/common'
import {Test} from '@nestjs/testing'
import {ConfigModule, ConfigService} from '@nestling/config'
import {ContextService} from '@nestling/context'
// tslint:disable-next-line:no-implicit-dependencies no-duplicate-imports
import {Express} from 'express'
import * as request from 'superagent'
// tslint:disable-next-line:no-duplicate-imports
import {Request} from 'superagent'
import {DataSource} from 'typeorm'
import {DatabaseAdapterModule} from '../adapter'
import {MongoDbAdapter} from '../adapter/mongo.adapter'
import {ApplicationModule} from '../app.module'
import {getRepoManager} from '../git/getRepoManager'
import {RepoManager} from '../git/RepoManager'
import {User} from '../gogs/entities/user.entity'
import {GogsDbStrategy} from '../user/strategies/gogs-db.strategy'
import {UserModule} from '../user/user.module'
import {generateFakeAccessToken} from './generateFakeAccessToken'
import {ensureTestUser} from './typeorm'
import {Transport} from '@nestjs/microservices'

export type TestUser = User & {
  accessToken: string
  authorizationHeader: string
}

export interface TestContext {
  app: INestApplication
  config: any
  server: Express
  user: TestUser
  user2: TestUser
  agent: Request
  agent2: Request
  repoManager: RepoManager
  mongoAdapter: MongoDbAdapter
  close: () => Promise<void>
}

export async function createTestApp(collection: string): Promise<TestContext> {
  const module = await Test.createTestingModule({
    imports: [ApplicationModule],
  }).compile()

  const app = module.createNestApplication()

  const logger = new Logger('createTestApp')
  const dataSource = app.get(DataSource)

  const userRepository = dataSource.getRepository(User)
  await ensureTestUser(userRepository)

  await app.init()

  logger.log('Test app initialized')

  const gogsDbStrategy = await app.select(UserModule).get(GogsDbStrategy)

  await gogsDbStrategy.start()

  logger.log('Started gogs db strategy')

  logger.log('Flushing mongo db')
  const mongoAdapter = await app
    .select(DatabaseAdapterModule)
    .get(MongoDbAdapter)

  await mongoAdapter.flush(collection)

  const config = (await app.select(ConfigModule).get(ConfigService)) as any

  const context = await app.get(ContextService)

  context.set('logger', logger)

  logger.log('Test context initialized.')

  // let's do this with gogsClient btw.
  const testUser = (await userRepository.findOneBy({
    name: 'test-user',
  })) as TestUser
  const testUser2 = (await userRepository.findOneBy({
    name: 'test-user2',
  })) as TestUser

  // These can now be the real tokens, not the jwt ones.
  testUser.accessToken = await generateFakeAccessToken(
    testUser.id,
    testUser.name,
    config.jwt.secret
  )
  testUser.authorizationHeader = `Bearer ${testUser.accessToken}`

  testUser2.accessToken = await generateFakeAccessToken(
    testUser2.id,
    testUser2.name,
    config.jwt.secret
  )
  testUser2.authorizationHeader = `Bearer ${testUser2.accessToken}`

  const agent = (request.agent() as any).set(
    'Authorization',
    `Bearer ${testUser.accessToken}`
  )

  const agent2 = (request.agent() as any).set(
    'Authorization',
    `Bearer ${testUser2.accessToken}`
  )

  logger.log('Test users initialized.')

  context.set('Authorization', testUser.authorizationHeader)

  const repoManager = getRepoManager(context, testUser.name)

  logger.log(`Clearing repositories for user ${testUser.name}`)
  await repoManager.cleanCloneDir()

  await repoManager.clearRepos(testUser.name)

  logger.log('Create testApp done.')

  const close = async () => {
    await mongoAdapter.close()
  }

  const natsHost = (config as any).nats.host
  const natsPort = (config as any).nats.port

  app.connectMicroservice({
    transport: Transport.NATS,
    options: {
      url: `nats://${natsHost}:${natsPort}`,
      // queue: 'my-queue',
      // user: process.env.NATS_USER,
      // pass: process.env.NATS_PASS,
    },
  })
  await app.startAllMicroservices()

  return {
    app,
    config,
    server: app.getHttpServer(),
    user: testUser,
    user2: testUser2,
    agent,
    agent2,
    repoManager,
    mongoAdapter,
    close,
  }
}
