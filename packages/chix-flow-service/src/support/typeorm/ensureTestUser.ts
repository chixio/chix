import {Logger} from '@nestjs/common'
import {Repository} from 'typeorm'
import {User} from '../../gogs/entities/user.entity'

const USER_TEST_NAME = 'test-user'
const ADMIN_TEST_NAME = 'test-admin'

export const ensureTestUser = async (userRepository: Repository<User>) => {
  let found = userRepository.findOneBy({name: USER_TEST_NAME})

  if (!found) {
    new Logger('ensureTestUser').log('Test user not found, create one first')

    process.exit()
  }

  found = userRepository.findOneBy({name: ADMIN_TEST_NAME})

  if (!found) {
    console.log('Test admin user not found, create one first')

    process.exit()
  }

  return found
}
