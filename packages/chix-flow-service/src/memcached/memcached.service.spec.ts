import {Test} from '@nestjs/testing'
import {ConfigModule} from '@nestling/config'
import {LoggerModule} from '@nestling/logger'
// tslint:disable-next-line:no-implicit-dependencies
import {expect} from 'chai'
import {MemcachedService} from './memcached.service'

describe('Memcached Service', () => {
  let memcachedService: MemcachedService

  before(async () => {
    const module = await Test.createTestingModule({
      imports: [
        ConfigModule.forRoot({
          memcached: {
            servers: 'localhost:11211',
          },
        }),
        LoggerModule.forRoot({
          name: 'chix-test',
        }),
      ],
      providers: [MemcachedService],
    }).compile()

    memcachedService = module.get<MemcachedService>(MemcachedService)
  })

  after(async () => {
    await memcachedService.flush()
  })

  it('should start', async () => {
    await memcachedService.start()
    // jest.spyOn(memcachedService, 'findAll').mockImplementation(() => result)

    // expect(await memcachedController.findAll()).toBe(result)
  })

  it('Can add item to memcached', async () => {
    await memcachedService.add('hello', 'world')
  })

  it('Can read item from memcached', async () => {
    const result = await memcachedService.get('hello')

    expect(result).to.equal('world')
  })

  it('Can set item multiple times', async () => {
    await memcachedService.set('hello', 'world')
    await memcachedService.set('hello', 'world1')
    await memcachedService.set('hello', 'world2')
    await memcachedService.set('hello', 'world3')

    const result = await memcachedService.get('hello')

    expect(result).to.equal('world3')
  })

  it('Can remove item from memcached', async () => {
    await memcachedService.del('hello')
  })

  it('should stop', async () => {
    await memcachedService.stop()
  })
})
