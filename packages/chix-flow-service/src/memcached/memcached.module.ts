import {Module} from '@nestjs/common'
import {ConfigModule} from '@nestling/config'
import {LoggerModule} from '@nestling/logger'
import {MemcachedService} from './memcached.service'

@Module({
  imports: [ConfigModule, LoggerModule],
  exports: [MemcachedService],
  providers: [MemcachedService],
})
export class MemcachedModule {}
