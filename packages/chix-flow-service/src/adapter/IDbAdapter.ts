import {ContextModel} from '@nestling/context'
import {Filter} from 'mongodb'
import {QueryOptions} from './mongo.adapter'

export interface WithId {
  id: string
}

export interface IDbAdapter {
  find: <T>(
    collection: string,
    by: Filter<T>,
    options: QueryOptions,
    context?: ContextModel
  ) => Promise<T[]>
  findOne: <T>(collection: string, by: Filter<T>) => Promise<T | null>
  insert: <T>(collection: string, item: T) => Promise<T>
  findById: <T>(collection: string, id: string) => Promise<T | null>
  update: <T extends WithId>(collection: string, item: T) => Promise<T>
}
