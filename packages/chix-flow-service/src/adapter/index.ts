import {Global, Module} from '@nestjs/common'
import {ConfigModule} from '@nestling/config'
import {DatabaseModule} from '../database'
import {UserModule} from '../user/user.module'
import {MongoDbAdapter} from './mongo.adapter'

export const adapters = [MongoDbAdapter]

@Global()
@Module({
  imports: [ConfigModule, UserModule, DatabaseModule],
  exports: [...adapters],
  providers: [...adapters],
})
export class DatabaseAdapterModule {}
