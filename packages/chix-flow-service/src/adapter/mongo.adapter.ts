import {Inject, Injectable, OnModuleInit} from '@nestjs/common'
import {IDbAdapter, WithId} from './IDbAdapter'

import {ConfigService} from '@nestling/config'
import {ContextModel} from '@nestling/context'

import {Collection, Db, Filter, FindCursor, MongoClient} from 'mongodb'
import * as uuid from 'uuid'

export type PaginationContext = {
  page: number
  perPage: number
  total: number
}

export type QueryOptions = {
  page?: number
  perPage?: number
}

export type DocFilters = {
  [collection: string]: DocFilter[]
}

export type DocFilter = <T>(document: T) => Promise<T>

@Injectable()
export class MongoDbAdapter implements IDbAdapter, OnModuleInit {
  public db: Db

  private filters: DocFilters = {}

  constructor(
    @Inject('MongoDbToken') private readonly mongo: MongoClient,
    private config: ConfigService
  ) {
    this.db = this.mongo.db((this.config as any).mongo.database)
  }

  public onModuleInit() {
    this.db = this.mongo.db((this.config as any).mongo.database)
  }

  public async paginate(
    query: FindCursor,
    context: ContextModel,
    page: number | undefined,
    perPage: number | undefined
  ): Promise<FindCursor> {
    const total = await query.clone().count()

    context.set('pagination', {
      total,
      page,
      perPage,
    })

    if (page === undefined) {
      // return this.transformMany<T>(query)
      return query
    }

    const limit = perPage || 10

    return query.skip(limit * (page - 1)).limit(limit)
  }

  // public addDocFilterFor(collection: string, filter: DocFilter) {
  public addDocFilterFor(collection: string, filter: any) {
    const ns = `${this.db.databaseName}.${collection}`

    if (!this.filters[ns]) {
      this.filters[ns] = []
    }

    this.filters[ns].push(filter)
  }

  public async find<T>(
    collection: string,
    by: any = {},
    options: QueryOptions = {},
    context?: ContextModel
  ): Promise<T[]> {
    const cursor = this.db.collection(collection).find(by, {
      projection: {
        // providerId: 0,
        _id: 0,
      },
    })

    if (options.page && options.perPage) {
      if (!context) {
        throw Error('Paging requires context to be set')
      }
      return this.transformMany<T>(
        await this.paginate(cursor, context, options.page, options.perPage)
      )
    }

    return this.transformMany<T>(cursor)
  }

  /**
   * Search collection
   *
   * Note: expects a search index to be defined on the collection.
   */
  public async search<T>(
    context: ContextModel,
    collection: string,
    $search: string,
    options: QueryOptions = {},
    where: any = {}
  ): Promise<T[]> {
    if (!$search) {
      return []
    }

    const cursor = await this.paginate(
      await this.db.collection(collection).find(
        {$text: {$search}},
        {
          score: {$meta: 'textScore'},
          ...where,
        }
      ),
      context,
      options.page,
      options.perPage
    )

    return this.transformMany<T>(cursor)
    // .sort({ score: { $meta: 'textScore' } }).toArray()
  }
  public async findOne<T>(
    collection: string,
    by: Filter<T>
  ): Promise<T | null> {
    const doc = await this.db.collection(collection).findOne(by as any, {
      projection: {
        // providerId: 0,
        _id: 0,
      },
    })

    return this.transform(
      doc as unknown as T,
      `${this.db.databaseName}.${collection}`
    )
  }

  public async count<T>(collection: string, by: Filter<T>): Promise<number> {
    return this.db.collection(collection).find(by as any).count()
  }

  public async exists<T>(collection: string, by: Filter<T>): Promise<boolean> {
    const count = await this.count(collection, by)

    return count > 0
  }

  public async remove(collection: string, id: string): Promise<boolean> {
    const result = await this.db.collection(collection).deleteOne({id})

    return result.deletedCount >= 1
  }

  public async removeAll(collection: string, where: object): Promise<boolean> {
    const result = await this.db.collection(collection).deleteOne({
      ...where,
    })

    return result.deletedCount >= 1
  }

  public async insert<T>(collection: string, item: Partial<T>): Promise<T> {
    const col: Collection = await this.db.collection(collection)

    const id = (item as any).id ? (item as any).id : uuid.v4()

    const doc = {id, ...(item as object)}

    const {result}: any = await col.insertOne(doc)

    if (result.ok) {
      const res = await this.findById(collection, id)

      return res as unknown as T
    }

    throw Error(`Failed to insert item into ${collection}`)
  }

  public async findById<T>(collection: string, id: string): Promise<T | null> {
    const col: Collection = await this.db.collection(collection)

    const document = (await col.findOne(
      {id},
      {
        projection: {
          // providerId: 0,
          _id: 0,
        },
      }
    )) as unknown as T

    if (document != null) {
      return this.transform(document, `${this.db.databaseName}.${collection}`)
    }

    return null
  }

  public async update<T extends WithId>(
    collection: string,
    item: T
  ): Promise<T> {
    const col: Collection = await this.db.collection(collection)

    let _item: T | null

    if (item.id) {
      _item = await col.findOne<T>({id: item.id})
    }

    const excludeFromUpdate: string[] = ['id', '_id']

    let hasChanges = false

    const $set = Object.keys(item).reduce((updates, key: string) => {
      if (
        !excludeFromUpdate.includes(key) &&
        (!_item || (_item as any)[key] !== (item as any)[key])
      ) {
        hasChanges = true
        updates[key] = (item as any)[key]
      }

      return updates
    }, {} as any)

    const id = item.id || uuid.v4() // possible upsert

    if (hasChanges) {
      await col.updateOne(
        {
          id,
        },
        {
          $set,
        },
        {
          upsert: true,
        }
      )
    }

    const result = await this.findById(collection, id)

    return result as unknown as T
  }

  public async flush(collection: string): Promise<number> {
    const result = await this.db.collection(collection).deleteMany({})

    return result.deletedCount
  }

  public async close() {
    return this.mongo.close()
  }

  private transformMany<T>(cursor: FindCursor): Promise<T[]> {
    const ns = (cursor as any).ns

    if (!ns) {
      throw Error('Could not determine ns')
    }

    return new Promise((resolve, _reject) => {
      const docPromises: Promise<T>[] = []

      cursor.on('data', (doc: T) => {
        // here can be the async map function.
        // first let the tests just pass.
        docPromises.push(this.docFilter<T>(doc, ns))
      })

      cursor.on('end', () => {
        resolve(Promise.all(docPromises))
      })
    })
  }

  private async transform<T>(doc: T, ns: string): Promise<T> {
    if (doc === null) {
      return doc
    }

    return this.docFilter<T>(doc, ns)
  }

  private async docFilter<T>(document: T, ns: string): Promise<T> {
    if (this.filters[ns] && this.filters[ns].length) {
      let doc = document

      for (const filter of this.filters[ns]) {
        doc = await filter(doc)
      }

      return doc
    }

    return document
  }
}
