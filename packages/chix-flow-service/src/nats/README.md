# Nats Microservice

This is the Nats Microservice

Running the Nats microservice is manditory in a production environment.

It will listen to a nats instance for changes on the gogs git repositories

Gogs itself is sending out post receive repository updates to nats.

This nats microservice will listen to those changes and update the mongodb nodes collection accordingly.

The structure of the message is as follows:

```typescript
export interface RepoInfo {
  repoUsername: string
  repoName: string
  oldCommitId: string
  newCommitId: string
  refFullName: string
  pusherID: number 
  pusherName: string
  path: string
}

export interface Payload {
  repoInfo: RepoInfo
  contents: string 
}

export interface Message {
  type: 'NodeUpdate',
  payload: Payload
}
```

