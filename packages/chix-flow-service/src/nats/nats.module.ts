import {Global, Module} from '@nestjs/common'
import {ClientProxyFactory, Transport} from '@nestjs/microservices'
import {ConfigService} from '@nestling/config'
import {SharedModule} from '../shared/shared.module'

const natsFactory = {
  provide: 'NATS_CONNECTION',
  useFactory: (config: ConfigService) => {
    const natsHost = (config as any).nats.host
    const natsPort = (config as any).nats.port

    const options: any = {
      transport: Transport.NATS,
      options: {
        url: `nats://${natsHost}:${natsPort}`,
        // user:
        // pass:
      },
    }
    return ClientProxyFactory.create(options)
  },
  inject: [ConfigService],
}

@Global()
@Module({
  imports: [SharedModule],
  providers: [natsFactory],
  exports: [natsFactory],
})
export class NatsModule {}
