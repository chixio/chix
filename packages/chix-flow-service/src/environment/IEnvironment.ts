export interface EnvAdmin {
  user: string
  password: string
}

export interface EnvClient {
  id: string
}

export interface EnvLogLevel {
  level: string
}

export interface EnvMongo {
  database: string
  uri: string
}

// Will not work now anymore
export interface EnvMongoUserStrategy {
  strategy: 'MongoStrategy'
  options: {
    database: string
  }
}

export interface EnvGrpcUserStrategy {
  strategy: 'GrpcStrategy'
  options: {
    host: string
    port: number
  }
}

export interface EnvGogsDbUserStrategy {
  strategy: 'GogsDbStrategy'
}

export interface EnvJwt {
  secret: string
}

export interface EnvMemcached {
  servers: string[]
}

export interface EnvMySql {
  host: string
  port?: number
  username: string
  password: string
  database: string
  logging?: boolean
}

export interface EnvNats {
  host: string
  port: number
}

export interface EnvGogsGit {
  host: string
  port: string
  user: string
}

export interface EnvGogsApi {
  token: string
  url: string
}

export interface EnvGogs {
  cloneDir: string
  git: EnvGogsGit
  api: EnvGogsApi
}

export interface IEnvironment {
  admin: EnvAdmin
  port: number
  client: EnvClient
  log: EnvLogLevel
  mongo: EnvMongo
  user: EnvGogsDbUserStrategy | EnvGrpcUserStrategy | EnvMongoUserStrategy
  jwt: EnvJwt
  whitelist: string[]
  memcached: EnvMemcached
  mysql: EnvMySql
  nats: EnvNats
  gogs: EnvGogs
}

export interface EnvTestUser {
  user: string
  password: string
  token: string
}

export type ITestEnvironment = IEnvironment & {
  test: {[user: string]: EnvTestUser}
}
