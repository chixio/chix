import {IEnvironment} from './IEnvironment'
import {whitelist} from './whitelist'

export const environment: IEnvironment = {
  admin: {
    user: 'john',
    password: 'doe',
  },
  port: 2303,
  client: {
    id: 'chix',
  },
  log: {
    level: 'info',
  },
  mongo: {
    database: 'chix',
    uri: 'mongodb://localhost',
  },
  user: {
    /* Will not work now anymore
    strategy: 'MongoStrategy',
    options: {
      database: 'chix-user'
    }
    */
    /*
    strategy: 'GrpcStrategy',
    options: {
      host: '0.0.0.0',
      port: '50051'
    }
    */
    strategy: 'GogsDbStrategy',
  },
  jwt: {
    secret: 'change_me',
  },
  whitelist,
  memcached: {
    servers: ['localhost:11211'],
  },
  mysql: {
    host: 'robberthalff.com',
    port: 3306,
    username: 'root',
    password: '',
    database: 'test',
  },
  nats: {
    host: `localhost`,
    port: 4222,
  },
  gogs: {
    cloneDir: '/tmp/data',
    git: {
      host: 'localhost',
      port: '3022',
      user: 'git',
    },
    api: {
      token: '',
      url: 'https://repos.chix.io/api/v1',
    },
  },
}
