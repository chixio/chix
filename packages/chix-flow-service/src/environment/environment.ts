import {environment as prodEnvironment} from './environment.prod'
import {environment as testEnvironment} from './environment.test'

export const environment =
  process.env.ENV === 'test' ? testEnvironment : prodEnvironment
