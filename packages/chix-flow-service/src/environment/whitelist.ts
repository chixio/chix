let whiteList = ['http://localhost:4200', 'http://authumn']

if (process.env.WHITELIST) {
  whiteList = process.env.WHITELIST.split(',').map((str) => str.trim())
}

export const whitelist = whiteList
