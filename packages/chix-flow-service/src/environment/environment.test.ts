import {ITestEnvironment} from './IEnvironment'
import {whitelist} from './whitelist'

const MONGO_PORT = 30017
const MYSQL_PORT = 6306
const MEMCACHED_PORT = 14211

export const environment: ITestEnvironment = {
  admin: {
    user: 'john',
    password: 'doe',
  },
  port: 2303,
  client: {
    id: 'chix',
  },
  log: {
    level: 'fatal',
  },
  mongo: {
    database: 'chix',
    uri: `mongodb://localhost:${MONGO_PORT}`,
  },
  test: {
    admin: {
      user: 'test-admin',
      password: 'test',
      token: '0fe41775fee6e9c116f1bdfb1ad0dd9c37658cd6',
    },
    user: {
      user: 'test-user',
      password: 'test',
      token: 'f090cc386024ece5f45b0be9c8cd3a9674ce2944',
    },
    user2: {
      user: 'test-user2',
      password: 'test',
      token: 'caf3cabf61e3e3754e9fe442a074b184c6e4916a',
    },
  },
  user: {
    /* Will not work now anymore
    strategy: 'MongoStrategy',
    options: {
      database: 'chix-user'
    }
    */
    /*
    strategy: 'GrpcStrategy',
    options: {
      host: '0.0.0.0',
      port: '50051'
    }
    */
    strategy: 'GogsDbStrategy',
  },
  jwt: {
    secret: 'change_me',
  },
  whitelist,
  memcached: {
    servers: [`localhost:${MEMCACHED_PORT}`],
  },
  mysql: {
    host: '127.0.0.1',
    port: MYSQL_PORT,
    username: 'foo',
    password: 'bar',
    database: 'chix_gogs',
    logging: false,
  },
  nats: {
    host: `nats://localhost`,
    port: 4222,
  },
  gogs: {
    cloneDir: './tmp/cloneDir',
    git: {
      host: 'localhost',
      port: '3022',
      user: 'git',
    },
    api: {
      token: 'c4df05e15d87562177f41af9954c0ae4ec79e675',
      url: 'http://localhost:6001/api/v1',
    },
  },
}
