import {EventEmitter} from 'events'
import {Scope} from './scope'
import {Constructor} from './types'
import {compare} from './util'

export type ScopeString = string

export type ParserToken = {
  name: string
  scope: ScopeString
  value: any
  // scopeChar: string
  scopeChar: number
  start: number
  end: number
}

export type Level = {
  [name: string]: Scope
}

export function tokenEndingContainsEol(
  tokenEndings: string[],
  eols: string[],
  c: string
) {
  for (const tokenEnding of tokenEndings) {
    if (eols.indexOf(tokenEnding) >= 0 && tokenEnding === c) {
      return true
    }
  }

  return false
}

/**
 *
 * DysLexer
 *
 * @constructor
 * @param {object} rootScope - The root scope
 */
export class DysLexer extends EventEmitter {
  public sync: boolean = false
  public chars: string = ''
  public current: number = 0
  // also do not count \r as character, it has 2 chars
  // for eol.
  // TODO: should override comma as custom,
  // not as default over here
  public eol: string[] = ['\n', '\r', ',']

  // public scopeChar: string | undefined = undefined
  public scopeChar: number | undefined

  public lineNumber: number = 0

  // default token endings
  // a scope can overwrite this temporarily
  public tokenEnding: string[] = ['\n', '\t', ' ']

  // Tokens
  public lastToken: ParserToken | undefined

  // token cache
  public tokens: ParserToken[] = []

  // char at which the current token started
  public tokenStart: number | undefined = 0

  public token: string = ''

  // the last character that triggered a scope change.
  public scoper: string | undefined

  // char at which the current scope started
  public scopeStart: number = 0

  // first level matchers
  public level: Level = {}

  // only rebuild for logging
  public line: string = ''

  // keep track of where we are,
  // with possibility to go back
  // a bit wild but works.
  public track: string[] = []

  public scope: ScopeString

  public rootScope: ScopeString

  // will remember all tokens in sync mode.
  private _tokens: ParserToken[][] = []

  constructor(rootScope: ScopeString) {
    super()

    this.scope = this.rootScope = rootScope
  }

  /**
   *
   * Reset
   *
   */
  public reset() {
    this.chars = ''
    this.current = 0

    this.scopeChar = undefined

    // Tokens
    this.lastToken = undefined

    // will only be remembered per line/segment
    this.tokens = []

    // will remember all tokens in sync mode.
    this._tokens = []

    this.token = ''

    // the last character that triggered a scope change.
    this.scoper = undefined

    // line is rebuild for logging
    this.line = ''

    this.lineNumber = 0

    this.scope = this.rootScope

    this.track = []
  }

  /**
   *
   * Add Scope
   *
   * Adds a new scope to the lexer.
   *
   * @param {Scope} $Scope
   */
  public addScope($Scope: Constructor<Scope>) {
    this.level[($Scope as any).name] = new $Scope(this)

    return this.level[($Scope as any).name]
  }

  /**
   *
   * Goes back to the scope we entered from.
   *
   * @param {string} c - Current character
   */
  public back(c?: string) {
    if (this.track.length) {
      this.toScope(this.track.pop() as string, c, false)
    } else {
      throw Error('Already at rootScope cannot go back')
    }
  }

  /**
   * Switch to another scope
   *
   * @param {string} scope - Scope to switch to
   * @param {string} c     - Current character
   * @param {string} track - Whether to track this scope switch
   */
  public toScope(scope: string, c?: string, track = true) {
    // Still useful in debug mode
    // console.log('------ ' + scope + '------')
    this.emit('scopeSwitch', {
      from: this.scope,
      to: scope,
    })

    // I think this should *always* track
    if (track) {
      this.track.push(this.scope)
    }

    if (!this.level[scope]) {
      throw Error('Unknown scope: ' + scope)
    }

    // Not used...
    this.level[scope].setParent(this.level[this.scope])

    /** check structure of the scope we left */
    this.checkStructure(this.scope)

    /** register the current scope */
    this.scope = scope

    /** inform what char did the scope */
    this.scoper = c

    /** char at which this was scoped */
    this.scopeChar = this.current

    /** reset token list */
    this.level[this.scope].tokens = []

    /** notify new scope we have entered. */
    this.level[this.scope].onEnter()
  }

  public next(_index?: number) {
    /** take the current character */
    const c = this.chars.charAt(this.current)

    /** rebuild the line for debug purposes */
    this.line += c

    // debug
    // console.log(this.scope, ':', c)

    const scope = this.level[this.scope]

    if (!scope) {
      throw Error(`Cannot find ${this.scope}`)
    }

    if (scope.rules[c]) {
      // this can cause weird behavior if the rule itself
      // is redirecting the scope, which can happens.
      // it means within this function this.scope is switched...

      scope.rules[c](c)
    } else if (scope.rules['**']) {
      // '**' will run whenever there is not already a match
      scope.rules['**'](c)
    }
    // The above character matching could have switched the scope
    // it's actually the only job the character matching has...
    // but that character matching has a purpose.
    // however because we have switched scopes at _this_ exact point
    // the character will be re-considered by the other scope
    // that's the code following below.

    /** if the scope has set it's own tokenEndings.. */
    if (scope.tokenEnding !== undefined) {
      if (!this.tokenStart) {
        this.tokenStart = this.current
      }
      this.token += c

      if (
        scope.tokenEnding.indexOf(c) >= 0 &&
        // trick to catch the first ' which is not escaped
        // it's the first
        // if the token is saved in the other scope this one will never happen.
        // right hand is set with tokenEnding where left hand is set with the character matching (not good).
        // switching should be done from within the character matching
        this.token.length > 1 &&
        !this.isEscaped()
      ) {
        this.fireToken()
      }
    } else if (
      this.tokenEnding.indexOf(c) === -1 &&
      this.eol.indexOf(c) === -1
    ) {
      /** default behavior, build the token */
      if (!this.tokenStart) {
        this.tokenStart = this.current
      }
      this.token += c
    } else if (
      /** don't act if the scope took control */
      scope.tokenEnding === undefined &&
      this.tokenEnding.indexOf(c) >= 0
    ) {
      this.fireToken()
    }

    // So the same should happen here as onToken, but we got to watch
    // out that we do not have duplicate tokenEnding chars and eol chars.
    if (
      // note, much responsibility for the scope.
      // should take care of it's own ending.
      this.eol.indexOf(c) >= 0 &&
      (scope.tokenEnding === undefined ||
        tokenEndingContainsEol(scope.tokenEnding, this.eol, c))
    ) {
      // fire last token
      this.fireToken()

      this.finishLine()
    }

    this.current++
  }

  // TODO: some pass a token but it's not read..
  public fireToken(_token?: string) {
    if (this.token) {
      if (this.level[this.scope].tokensExpected === undefined) {
        throw new Error('tokensExpected is not set for ' + this.scope)
      }

      /** validate expected token count */
      if (
        (this.level as any)[this.scope].tokensExpected <
        this.level[this.scope].tokens.length
      ) {
        throw new Error(
          [
            'Unexpected token count for',
            this.scope,
            'expected',
            this.level[this.scope].tokensExpected,
            'instead of',
            this.level[this.scope].tokens.length,
            'tokens\n\n\t',
            JSON.stringify(this.level[this.scope].tokens, null, 2),
            '\n',
          ].join(' ')
        )
      }

      /** remember the current scope */
      const realScope = this.scope

      /** switch the current scope to root scope */
      this.scope = this.rootScope

      /**
       * If the scope has set it's own token ending
       * do not run the rootScope 'scanner'
       */
      if (
        this.level[realScope].tokenEnding !== undefined ||
        !this.level[this.rootScope].onToken(this.token) // runs only if the above didn't match, which is important.
      ) {
        this.scope = realScope
        if (this.scope !== this.rootScope) {
          this.level[this.scope].onToken(this.token)
        }
      }

      /** reset token */
      this.token = ''

      /** reset token start */
      this.tokenStart = undefined
    }
  }

  public isEscaped() {
    // leave room to escape the choice.
    if (this.level[this.scope].escape) {
      if (this.chars.charAt(this.current - 1) === '\\') {
        return true
      }
    }

    return false
  }

  // TODO: should check whether the token was handled
  // or at least make sure that when we fire a token
  // it is in sync with what we got presented back.
  // This way we can detect unhandled tokens.
  public present(name: string, data?: any) {
    /** validate the token */
    if (this.level[this.scope].validate) {
      let reg = (this.level[this.scope].validate as any)[name]

      if (typeof reg === 'string') {
        reg = new RegExp(reg)
      } // ...

      if (reg && !reg.test(data)) {
        const previousScope = this.track.length
          ? this.track[this.track.length - 1]
          : ''

        throw new Error(
          [
            'Invalid',
            name,
            'token does not match',
            reg.toString(),
            '\n\n\t',
            data,
            '\n',
            '\n',
            this.drawError(),
            '\nCurrent scope:',
            this.scope,
            previousScope ? `\n\nPrevious Scope: ${previousScope}` : '',
            '\n\nDetected tokens on this line:',
            JSON.stringify(this.tokens, null, 2),
          ].join(' ')
        )
      }
    }

    /**
     * Note: this relies on the scopes emitting the tokens back.
     * It does not contain each and every token emitted by this matcher.
     */
    this.lastToken = {
      name,
      scope: this.scope,
      value: data,
      scopeChar: this.scopeChar,
      start: this.tokenStart,
      end: this.current,
    } as ParserToken

    /** remember token */
    this.tokens.push(this.lastToken)

    this.emit('token', this.lastToken)

    /** push token for this scope */
    this.level[this.scope].tokens.push(this.lastToken)

    /** if we've reached the token count, go back a level (or to root maybe) */
    if (
      this.level[this.scope].tokensExpected ===
      this.level[this.scope].tokens.length
    ) {
      // ok this works but is a bit ugly...
      // this.toScope(this.rootScope)
      this.back()
    }
  }

  public start(str: string, sync?: boolean) {
    const gen = this.run(str, sync)

    while (!gen.next().done) {
      gen.next()
    }

    // FIX ME: should have been done earlier. See comments.spec.ts
    //         problem is probably the no new line at the end, this works though.
    if (this.tokens.length) {
      this._tokens.push(this.tokens)
      this.tokens = []
    }

    this.emit('end')

    return sync ? this._tokens : null
  }

  /**
   * Has been made into a generator to allow for stepping
   *
   * @param str
   * @param {boolean} sync
   */
  public *run(str: string, sync?: boolean): IterableIterator<string> {
    // will remember all _tokens
    this.sync = !!sync

    this.chars = str + '\n' // TODO: temp fix

    if (!this.rootScope) {
      throw new Error('Initial Root Scope required')
    }

    /** notify all scopes we've started */
    for (const scope in this.level) {
      if (this.level.hasOwnProperty(scope)) {
        this.level[scope].onLineStart()
      }
    }

    for (let i = 0; i < this.chars.length; i++) {
      this.next(i)

      yield this.chars[i]
    }
  }

  public readChunk(str: Buffer) {
    this.reset()
    this.start(str.toString())
  }

  /**
   * Checks whether the scope returns with an expected structure.
   */
  public checkStructure(scope: string) {
    if (!this.level.hasOwnProperty(scope)) {
      throw Error(`No such scope: ${scope}`)
    }
    const s = this.level[scope]

    // output to structure
    const tokens = s.tokens.map((t) => t.name)

    if (tokens.length) {
      // Check if it matches any of the structures.
      if (s.structure.length) {
        let match = false
        for (const str in s.structure) {
          if (compare(tokens, s.structure[str])) {
            match = true
            break
          }
        }
        if (!match) {
          throw new Error(
            [
              'Structure mismatch for',
              scope,
              JSON.stringify(tokens),
              'did not match any of',
              JSON.stringify(s.structure),
              '\n\n\t',
              JSON.stringify(s.tokens),
            ].join(' ')
          )
        }

        this.emit('structure', {
          scope,
          tokens,
        })
      }
    }
  }

  public drawError() {
    // for debugging only, contains the full line being processed
    const remainder = this.chars.slice(this.current)

    const eol = this.eol.find((_eol) => remainder.indexOf(_eol) >= 0)
    const eolIndex = eol ? remainder.indexOf(eol) : Infinity

    const current = this.current + 1

    const currentLine = this.chars
      .slice(current, current + eolIndex - 1)
      .replace(/\n/g, '\\n')

    const lineNumberPrefix = `${this.lineNumber + 1}:`
    const pointer = '--^'

    // let newLineCount = 0

    const line = this.line.replace(/\n/g, () => {
      // newLineCount++

      return '\\n'
    })

    const fullLine = `${line}${currentLine}`

    const repeatCount = line.length + lineNumberPrefix.length - pointer.length // + newLineCount

    return (
      `${lineNumberPrefix}${fullLine}\n` +
      ' '.repeat(repeatCount > 0 ? repeatCount : 0) +
      pointer
    )
  }

  private finishLine() {
    for (const s in this.level) {
      if (this.level.hasOwnProperty(s)) {
        this.level[s].onLineEnd()

        // do not emit lines without tokens. (empty ones)
        if (this.tokens.length) {
          this.emit('lineTokens', this.tokens, this.line, this.lineNumber)

          if (this.sync) {
            // array of tokens per line
            this._tokens.push(this.tokens)
          }

          this.lineNumber++
        }

        this.tokens = []

        this.line = ''

        // reset generic stuff from the scope.
        this.level[s].onLineStart()
      }
    }
  }
}
