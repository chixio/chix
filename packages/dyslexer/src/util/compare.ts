export function compare(array1: any[], array2: any[]): boolean {
  const length = array1.length

  if (!array2) {
    return false
  }

  if (array1.length !== array2.length) {
    return false
  }

  for (let index = 0; index < length; index++) {
    if (array1[index] instanceof Array && array2[index] instanceof Array) {
      if (!array1[index].compare(array2[index])) {
        return false
      }
    } else if (array1[index] !== array2[index]) {
      return false
    }
  }

  return true
}
