import {DysLexer, ParserToken} from './dyslexer'

export type Rules = {
  [char: string]: (c?: string) => void
}

export class Scope {
  public lexer: DysLexer

  public validate:
    | {
        [key: string]: RegExp
      }
    | undefined

  public alias:
    | {
        [name: string]: string
      }
    | undefined

  // default token endings
  // a scope can overwrite this.
  // [] now means the scope doesn't have any token endings.
  // it will handle it by itself.
  public tokenEnding: string[] | undefined

  /**
   * If this is set the parser will throw an error
   * when there are more tokens within the current scope.
   */
  public tokensExpected: number | undefined = undefined

  /**
   * tokens processed by this scope on the current line
   * automatically reset on each line start
   */
  public tokens: ParserToken[] = []

  /** check for escaped token endings */
  public escape = false

  /**
   * Basic structure checking
   * if empty no structure checking is done.
   * If it is set the tokens _must_ match one of structures.
   */
  public structure = []

  // this could be made generic for switchers.
  // switchers are only valid until the next char.
  public rules: Rules = {}

  private _parent: Scope | undefined

  constructor(lexer: DysLexer) {
    this.lexer = lexer
    this.setup()
    this.loadRules()
  }

  /**
   * executed during each line end
   * regardless whether this scope is active.
   */
  public onLineEnd() {}

  /**
   * executed during each line start
   * regardless whether this scope is active.
   */
  public onLineStart() {}

  /**
   * Executes each time a scope is entered
   */
  public onEnter() {}

  /**
   * Executes each time a token is found
   */
  public onToken(_token: string): boolean | void {
    throw Error('onToken() method not implemented')
  }

  /**
   * Executed once during initialization
   */
  public setup() {}

  /**
   * Get the parent scope of this scope
   * the scope we entered from
   */
  public parent() {
    return this._parent
  }

  public setParent(parent: Scope) {
    this._parent = parent
  }

  /**
   * For convenience it's possible to create an alias
   * For a scanner function within the scope.
   */
  public loadRules() {
    if (this.alias) {
      for (const alias in this.alias) {
        // create an alias, the function is reused.
        // so the char coming in can now be of multiple types.
        // e.g. both ' or ", or
        //      both - and > to detect ->
        if (this.alias.hasOwnProperty(alias)) {
          this.rules[alias] = this.rules[this.alias[alias]]
        }
      }
    }
  }
}
