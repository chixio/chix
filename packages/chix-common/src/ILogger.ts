export interface ILogger {
  debug: (key: string, ...args: any[]) => void
  info: (key: string, ...args: any[]) => void
  error: (key: string, ...args: any[]) => void
  warn: (key: string, ...args: any[]) => void
  addLevel?: any
}
