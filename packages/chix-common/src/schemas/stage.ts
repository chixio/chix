export const StageSchema = {
  type: 'object',
  title: 'Chiχ Stage',
  properties: {
    id: {
      type: 'string',
      required: false,
    },
    env: {
      type: 'string',
      required: false,
    },
    title: {
      type: 'string',
      required: true,
    },
    description: {
      type: 'string',
      required: true,
    },
    actors: {
      type: 'array',
      title: 'Actors',
      required: true,
      items: {
        type: 'object',
        title: 'Actor',
        properties: {
          id: {
            type: 'string',
            required: true,
          },
          ns: {
            type: 'string',
            required: true,
          },
          name: {
            type: 'string',
            required: true,
          },
          version: {
            type: 'string',
            required: false,
          },
          context: {
            type: 'object',
            required: false,
          },
        },
      },
    },
    links: {
      type: 'array',
      title: 'Links',
      required: true,
      items: {
        type: 'object',
        title: 'Link',
        properties: {
          id: {
            type: 'string',
            required: false,
          },
          source: {
            type: 'string',
            required: true,
          },
          target: {
            type: 'string',
            required: true,
          },
          out: {
            type: 'string',
            required: false,
          },
          in: {
            type: 'string',
            required: false,
          },
          settings: {
            persist: {
              type: 'boolean',
              required: false,
            },
            cyclic: {
              type: 'boolean',
              required: false,
            },
          },
        },
      },
    },
  },
}
