export const IIPSchema = {
  type: 'object',
  title: 'Chiχ IIP',
  properties: {
    id: {
      type: 'string',
      required: false,
    },
    target: {
      type: 'object',
      required: true,
      properties: {
        id: {
          type: 'string',
        },
        port: {
          type: 'string',
        },
        index: {
          type: ['string', 'number'],
        },
        settings: {
          type: 'object',
          properties: {
            persist: {
              type: 'boolean',
            },
            sync: {
              type: 'string',
            },
            cyclic: {
              type: 'boolean',
            },
          },
        },
      },
    },
    data: {
      type: 'any',
      required: true,
    },
  },
  additionalProperties: false,
}
