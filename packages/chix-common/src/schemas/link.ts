export const LinkSchema = {
  type: 'object',
  title: 'Chiχ Link',
  properties: {
    id: {
      type: 'string',
      required: false,
    },
    source: {
      type: 'object',
      required: true,
      properties: {
        id: {
          type: 'string',
        },
        port: {
          type: 'string',
        },
        index: {
          type: ['string', 'number'],
        },
        settings: {
          type: 'object',
        },
      },
    },
    target: {
      type: 'object',
      required: true,
      properties: {
        id: {
          type: 'string',
        },
        port: {
          type: 'string',
        },
        index: {
          type: ['string', 'number'],
        },
        settings: {
          type: 'object',
          properties: {
            persist: {
              type: 'boolean',
            },
            sync: {
              type: 'string',
            },
            cyclic: {
              type: 'boolean',
            },
          },
        },
      },
    },
    settings: {
      type: 'object',
      properties: {
        dispose: {
          type: 'boolean',
        },
      },
    },
    metadata: {
      type: 'object',
    },
  },
  additionalProperties: false,
}
