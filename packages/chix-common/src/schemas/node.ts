export const NodeSchema = {
  type: 'object',
  title: 'Chiχ Nodes',
  properties: {
    title: {
      type: 'string',
      required: false,
    },
    description: {
      type: 'string',
      required: false,
    },
    _id: {
      type: 'string',
    },
    id: {
      type: 'string',
    },
    pid: {
      type: 'string',
      required: false,
    },
    private: {
      type: 'boolean',
      required: false,
    },
    name: {
      type: 'string',
      required: true,
    },
    ns: {
      type: 'string',
      required: true,
    },
    state: {
      type: 'any',
    },
    on: {
      type: 'any',
    },
    phrases: {
      type: 'object',
    },
    env: {
      type: 'string',
      enum: ['server', 'browser', 'polymer', 'phonegap'],
    },
    async: {
      type: 'boolean',
      required: false,
    },
    dependencies: {
      type: 'object',
      required: false,
    },
    provider: {
      required: false,
      type: 'string',
    },
    providers: {
      required: false,
      type: 'object',
    },
    expose: {
      type: 'array',
      required: false,
    },
    fn: {
      type: ['string', 'function'],
      required: false,
    },
    ports: {
      type: 'object',
      required: true,
      properties: {
        input: {
          type: 'object',
        },
        output: {
          type: 'object',
        },
        event: {
          type: 'object',
        },
      },
    },
    type: {
      enum: [
        'node',
        'flow',
        'provider',
        'data',
        'PolymerNode',
        'ReactNode',
        'StateNode',
      ],
      required: false,
    },
  },
  additionalProperties: false,
}
