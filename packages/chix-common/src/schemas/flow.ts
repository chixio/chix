export const FlowSchema = {
  type: 'object',
  title: 'Chiχ Map',
  collectionName: 'flows',
  properties: {
    _id: {
      type: 'string',
    },
    id: {
      type: 'string',
      required: false,
    },
    pid: {
      type: 'string',
      required: false,
    },
    private: {
      type: 'boolean',
      required: false,
    },
    main: {
      type: 'boolean',
      required: false,
    },
    type: {
      type: 'string',
      required: true,
    },
    env: {
      type: 'string',
      required: false,
    },
    ns: {
      type: 'string',
      required: false,
    },
    name: {
      type: 'string',
      required: false,
    },
    title: {
      type: 'string',
      required: false,
    },
    description: {
      type: 'string',
    },
    provider: {
      type: ['string', 'object'],
    },
    providers: {
      type: 'object',
    },
    keywords: {
      type: 'array',
    },
    nodeDefinitions: {
      type: 'object',
    },
    ports: {
      type: 'object',
      properties: {
        input: {
          type: 'object',
        },
        output: {
          type: 'object',
        },
      },
    },
    nodes: {
      type: 'array',
      title: 'Nodes',
      required: true,
      items: {
        type: 'object',
        title: 'Node',
        properties: {
          id: {
            type: 'string',
            required: true,
          },
          pid: {
            type: 'string',
            required: false,
          },
          ns: {
            type: 'string',
            required: true,
          },
          name: {
            type: 'string',
            required: true,
          },
          version: {
            type: 'string',
            required: false,
          },
          context: {
            type: 'object',
            required: false,
          },
        },
      },
    },
    links: {
      type: 'array',
      title: 'Links',
      required: false,
      items: {
        type: 'object',
        title: 'Link',
        properties: {
          id: {
            type: 'string',
            required: false,
          },
          source: {
            type: 'object',
            required: true,
            properties: {
              id: {
                type: 'string',
                required: true,
              },
              port: {
                type: 'string',
                required: true,
              },
              index: {
                type: ['string', 'number'],
                required: false,
              },
            },
          },
          target: {
            type: 'object',
            required: true,
            properties: {
              id: {
                type: 'string',
                required: true,
              },
              port: {
                type: 'string',
                required: true,
              },
              index: {
                type: ['string', 'number'],
                required: false,
              },
            },
          },
          settings: {
            persist: {
              type: 'boolean',
              required: false,
            },
            cyclic: {
              type: 'boolean',
              required: false,
            },
          },
        },
      },
    },
  },
  additionalProperties: false,
}
