import {ProjectDefinition} from './project'
import {UserId} from './user-model'

export interface ProjectModel extends ProjectDefinition {
  _id?: string
  id: string
  providerId: UserId
}
