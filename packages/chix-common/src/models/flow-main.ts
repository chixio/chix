import {Env} from './env'
import {Link} from './link'
import {Node} from './node'
import {NodeDefinitions} from './node-definitions'
import {Providers} from './providers'

export interface MainFlow {
  _id?: string
  id?: string
  type: 'flow'
  title?: string
  description?: string
  env?: Env
  private?: boolean
  provider?: string
  providers?: Providers
  nodeDefinitions?: NodeDefinitions
  nodes: Node[]
  links: Link[]
  keywords?: string
}
