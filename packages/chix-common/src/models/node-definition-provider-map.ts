import {NodeDefinitions} from './node-definitions'

export interface NodeDefinitionProviderMap {
  [provider: string]: NodeDefinitions
}
