import {InputPortsDefinition} from './input-ports-definition'
import {OutputPortsDefinition} from './output-ports-definition'

export type Ports = {
  input: InputPortsDefinition
  output: OutputPortsDefinition
}
