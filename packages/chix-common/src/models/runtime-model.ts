import {RuntimeDefinition} from './runtime-definition'
import {UserId, UserModel} from './user-model'

export interface RuntimeModel extends RuntimeDefinition {
  _id?: string
  id: string
  providerId?: UserId
  provider: UserModel
}
