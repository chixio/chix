export type Action = {
  actionName?: string // parser does not seem to generate this.
  nodes: string[]
  title: string
  description: string
}
