import {Port} from './port'

export interface InputPortsDefinition {
  [name: string]: Port
}
