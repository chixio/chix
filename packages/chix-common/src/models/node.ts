import {Context} from './context'
import {Metadata} from './metadata'

export type Node = {
  id: string
  pid?: string
  ns: string
  name: string
  description?: string
  title?: string
  version?: string
  provider?: string
  context?: Context
  metadata?: Metadata
}
