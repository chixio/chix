import {NodeDefinition} from './node-definition'
import {UserId} from './user-model'

export interface NodeModel extends NodeDefinition {
  _id?: string
  id: string
  providerId: UserId
}
