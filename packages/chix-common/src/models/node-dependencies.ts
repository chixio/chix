import {NpmDependencies} from './npm-dependencies'

export type NodeDependencyTypes = NpmDependencies

export type NodeDependencies = {
  npm?: NpmDependencies
}
