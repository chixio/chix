import {Port} from './port'
import {PortFunction} from './port-function'

export interface AsyncPort extends Port {
  async?: boolean
  fn: string | PortFunction
}
