import {Actions} from './actions'
import {ExternalPorts} from './external-ports'
import {Link} from './link'
import {Node} from './node'
import {BaseNodeDefinition} from './node-definition-base'
import {NodeDefinitions} from './node-definitions'

export interface Flow extends BaseNodeDefinition {
  type: 'flow' | string
  nodeDefinitions?: NodeDefinitions
  main?: boolean
  private?: boolean
  ports?: {
    input?: ExternalPorts
    output?: ExternalPorts
  }
  nodes: Node[]
  links: Link[]
  actions?: Actions
}

/*
Note:
  Everything will work with BaseNodeDefinition instead of NodeDefinition.
  which is almost correct.
  Once dependencies are being checked it's about a NodeDefinition
  So make that distinction.
  A BaseNodeDefinition should be castable to a NodeDefinition.
  Which means it should work.
*/
