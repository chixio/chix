import {NodeDefinition} from './node-definition'

export type NodeDefinitions = {
  // [key: string]: NodeDefinition | {}
  [ns: string]: {
    [name: string]: NodeDefinition
  }
}
