import {JSONSchema4, JSONSchema4Type, JSONSchema4Version} from 'json-schema'

export interface Port {
  $ref?: string
  $schema?: JSONSchema4Version
  name?: string
  title?: string
  description?: string
  default?: JSONSchema4Type
  multipleOf?: number
  maximum?: number
  exclusiveMaximum?: boolean
  minimum?: number
  exclusiveMinimum?: boolean
  maxLength?: number
  minLength?: number
  pattern?: string
  additionalItems?: boolean | JSONSchema4
  items?: JSONSchema4 | JSONSchema4[]
  maxItems?: number
  minItems?: number
  uniqueItems?: boolean
  maxProperties?: number
  minProperties?: number
  // type?: JSONSchema4TypeName | JSONSchema4TypeName[]
  type: string
  context?: any
  options?: any
  required?: boolean
  // enum?: string[]
  fn?: any

  additionalProperties?: boolean | JSONSchema4
  definitions?: {
    [k: string]: JSONSchema4
  }
  properties?: {
    [k: string]: JSONSchema4
  }
  patternProperties?: {
    [k: string]: JSONSchema4
  }
  dependencies?: {
    [k: string]: JSONSchema4 | string[]
  }
  enum?: JSONSchema4Type[]
  allOf?: JSONSchema4[]
  anyOf?: JSONSchema4[]
  oneOf?: JSONSchema4[]
  not?: JSONSchema4
  extends?: string | string[]
  [k: string]: any
  format?: string
}
