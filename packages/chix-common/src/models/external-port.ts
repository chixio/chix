export type ExternalPort = {
  title?: string
  type?: string // TODO: made it optional, should not be required?
  nodeId: string
  name: string
  index?: number
  default?: any
  context?: any
  required?: boolean
  setting?: {[key: string]: any}
}
