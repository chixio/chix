import {Flow as FlowDefinition} from './flow'
import {NodeDefinition} from './node-definition'

export type Process = (FlowDefinition | NodeDefinition) & {pid: string}
