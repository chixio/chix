export type ExternalStartPort = {
  name: ':start'
  type: 'any'
  required: false
}
