import {ExternalPorts} from './external-ports'

export type ExternalPortsDefinition = {
  input: ExternalPorts
  output: ExternalPorts
}
