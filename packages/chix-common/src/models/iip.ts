import {Connector} from './connector'
import {Metadata} from './metadata'

// id is optional
export interface IIPSourceConnector {
  id?: string
  port: string
  setting?: {[key: string]: any}
  action?: string
}

export interface IIP {
  source?: IIPSourceConnector
  target: Connector
  data?: any
  metadata?: Metadata
}
