export type NodeType =
  | 'node'
  | 'flow'
  | 'provider'
  | 'data'
  | 'PolymerNode'
  | 'ReactNode'
  | 'StateNode'
