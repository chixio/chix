import {Port} from './port'
export interface OutputPortsDefinition {
  [name: string]: Port
}
