import {Port} from './port'

export interface EventPortsDefinition {
  [name: string]: Port
}
