export type NpmDependencies = {
  [dependency: string]: string
}
