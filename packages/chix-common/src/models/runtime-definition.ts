export interface RuntimeDefinition {
  id: string
  address: string
  protocol: 'websocket' | 'webrtc' | 'iframe' | 'opener'
  type:
    | 'chix'
    | 'chix-nodejs'
    | 'chix-browser'
    | 'noflo'
    | 'noflo-nodejs'
    | 'noflo-browser'
  label: string
  description: string
  registered: Date
  seen: Date
  user: string
  secret: string
}
