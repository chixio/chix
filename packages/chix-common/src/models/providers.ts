export interface BaseProvider {
  name?: string // TODO: check if this should be optional
}

export type Provider = FSProvider | RemoteProvider

export interface RemoteProvider extends BaseProvider {
  url: string
}

export interface FSProvider extends BaseProvider {
  path: string
}

export interface Providers {
  [key: string]: Provider
}
