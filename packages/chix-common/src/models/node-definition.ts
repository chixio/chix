import {EventPortsDefinition} from './event-ports-definition'
import {InputPortsDefinition} from './input-ports-definition'
import {BaseNodeDefinition} from './node-definition-base'
import {NodeDependencies} from './node-dependencies'
import {NodeFunction} from './node-function'
import {OutputPortsDefinition} from './output-ports-definition'

export interface NodeDefinition extends BaseNodeDefinition {
  type?: string
  state?: any
  phrases?: {
    [key: string]: any
  }
  async?: boolean
  dependencies?: NodeDependencies
  expose?: string[]
  fn?: string | NodeFunction
  ports: {
    input?: InputPortsDefinition
    output?: OutputPortsDefinition
    event?: EventPortsDefinition // not implemented
  }
}
