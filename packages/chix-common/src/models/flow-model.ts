import {Flow as FlowDefinition} from './flow'
import {UserId} from './user-model'

export interface FlowModel extends FlowDefinition {
  _id?: string
  id: string
  providerId: UserId
}
