export interface SourceConnectorSettings {
  collect?: boolean
  index?: string | number
  pointer?: boolean
}

export interface TargetConnectorSettings {
  cyclic?: boolean
  index?: string | number
  mask?: string
  persist?: boolean
  sync?: string
}

export interface Connector {
  id: string
  port: string
  // index?: string | number // TODO: may have moved to setting.index.
  setting?: SourceConnectorSettings & TargetConnectorSettings
  action?: string
}
