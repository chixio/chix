import {Flow as FlowDefinition} from './flow'
import {NodeDefinition} from './node-definition'
import {NpmDependencies} from './npm-dependencies'

export interface ProjectDefinition {
  id?: string
  name: string
  author?: string
  description?: string
  version?: string
  licence?: string
  homepage?: string
  private?: boolean
  repository?: {
    type: string
    url: string
  }
  keywords?: string[]
  dependencies?: NpmDependencies
  nodes?: (NodeDefinition | FlowDefinition)[]
}
