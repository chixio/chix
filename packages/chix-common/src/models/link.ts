import {Connector} from './connector'
import {Metadata} from './metadata'

export type Link = {
  ioid?: string
  id?: string
  source?: Connector
  target?: Connector
  metadata?: Metadata
  data?: any
}
