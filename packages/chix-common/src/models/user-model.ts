// switch this might the userId be converted to string later on.
export type UserId = number

export type UserModel = {
  id: UserId
  name: string
}
