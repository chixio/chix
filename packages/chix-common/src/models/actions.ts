import {Action} from './action'

export type Actions = {
  [actionName: string]: Action
}
