import {Env} from './env'
import {Metadata} from './metadata'
import {Providers} from './providers'

export interface BaseNodeDefinition {
  _id?: string
  id?: string
  pid?: string
  ns: string
  name: string
  title?: string
  description?: string
  env?: Env
  private?: boolean
  metadata?: Metadata
  provider?: any // not used..
  providers?: Providers
  keywords?: string[]
  nodeTimeout?: number
  inputTimeout?: number
}
