import {ExternalPort} from './external-port'

export type ExternalPorts = {
  [key: string]: ExternalPort
}
