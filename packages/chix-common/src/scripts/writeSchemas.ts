import {writeFileSync} from 'fs'
import {join} from 'path'
import {
  FlowSchema,
  IIPSchema,
  LinkSchema,
  NodeSchema,
  ProjectSchema,
} from '../schemas'

const writeFile = (json: any, file: string) => {
  writeFileSync(
    join(__dirname, '../../schemas', file),
    JSON.stringify(json, null, 2),
    'utf-8'
  )
}

writeFile(IIPSchema, 'iip.json')
writeFile(LinkSchema, 'link.json')
writeFile(FlowSchema, 'flow.json')
writeFile(NodeSchema, 'node.json')
writeFile(ProjectSchema, 'project.json')
