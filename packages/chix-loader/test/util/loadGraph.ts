import * as fs from 'fs'
import * as yaml from 'js-yaml'
import * as path from 'path'

export function loadGraph(name: string) {
  return yaml.load(
    fs.readFileSync(
      path.resolve(__dirname, '../fixtures/', `${name}.yaml`),
      'utf8'
    )
  )
}
