import {expect} from 'chai'
import {Loader, parseDependencies} from '@chix/loader'

describe('Loader:', () => {
  let loader: Loader

  before(() => {
    loader = new Loader()
    loader.addNodeDefinitions('@', [
      {
        ns: 'my',
        name: 'component',
        ports: {
          input: {
            in: {
              type: 'string',
            },
          },
          output: {
            out: {
              type: 'string',
            },
          },
        },
      },
      {
        ns: 'my',
        name: 'component2',
        ports: {input: {in: {type: 'string'}}},
      },
    ])
  })

  it('Should be able to get *all* nodeDefinitions', () => {
    const definitions = loader.getNodeDefinitions()

    expect(definitions).of.have.property('@')
    expect(definitions['@']).of.have.property('my')
    expect(definitions['@']).of.have.property('my')
    expect(definitions['@'].my).of.have.property('component')
    expect(definitions['@'].my).of.have.property('component2')
  })

  it('Should be able to get nodeDefinitions for provider `@`', () => {
    const definitions = loader.getNodeDefinitions('@')

    expect(definitions).of.have.property('my')
    expect(definitions).of.have.property('my')
    expect(definitions.my).of.have.property('component')
    expect(definitions.my).of.have.property('component2')
  })

  it('Should be able to tell it has a nodeDefinition from provider `@`', () => {
    expect(loader.hasNodeDefinition('@', 'my', 'component')).of.eql(true)
    expect(loader.hasNodeDefinition('@', 'my', 'component2')).of.eql(true)
  })

  it('Should be able to get a nodeDefinition from provider `@`', () => {
    let nodeDefinition

    nodeDefinition = loader.getNodeDefinitionFrom('@', 'my', 'component')
    expect(nodeDefinition).of.have.property('ns')
    expect(nodeDefinition).of.have.property('name')
    expect(nodeDefinition.ns).of.eql('my')
    expect(nodeDefinition.name).of.eql('component')

    nodeDefinition = loader.getNodeDefinitionFrom('@', 'my', 'component2')
    expect(nodeDefinition).of.have.property('ns')
    expect(nodeDefinition).of.have.property('name')
    expect(nodeDefinition.ns).of.eql('my')
    expect(nodeDefinition.name).of.eql('component2')
  })

  it('Should be able to parse dependencies', () => {
    const def1 = {
      npm: {
        'my-mod': '1.x.x',
        'my-mod2': '2.1.0',
      },
    }

    const def2 = {
      npm: {
        'my-mod3': '1.1.x',
        'my-mod4': '3.1.0',
      },
    }

    const dependencies = {}

    const expected = {
      npm: {
        'my-mod': '1.x.x',
        'my-mod2': '2.1.0',
        'my-mod3': '1.1.x',
        'my-mod4': '3.1.0',
      },
    }

    parseDependencies(dependencies, def1)
    parseDependencies(dependencies, def2)

    expect(dependencies).of.eql(expected)
  })

  it('Should be able to parse namespace', () => {
    expect(() => loader.parseNamespace('@test')).to.throw()

    let result = loader.parseNamespace('@test/net')

    expect(result.org).to.eql('test')
    expect(result.ns).to.eql('net')
    expect(result.segments).to.eql([])

    result = loader.parseNamespace('@test/net/some/path/segments')

    expect(result.org).to.eql('test')
    expect(result.ns).to.eql('net')
    expect(result.segments).to.eql(['some', 'path', 'segments'])

    result = loader.parseNamespace('test')

    expect(result.org).to.eql(undefined)
    expect(result.ns).to.eql('test')
    expect(result.segments).to.eql([])

    result = loader.parseNamespace('test/net')

    expect(result.org).to.eql(undefined)
    expect(result.ns).to.eql('test')
    expect(result.segments).to.eql(['net'])
  })

  it('Should be able to construct providerUrl', () => {
    expect(
      loader.parseProviderUrl(
        'https://api.chix.io/v1/nodes/{org}/{ns}/{name}',
        '@rhalff/math',
        'add'
      )
    ).to.eql('https://api.chix.io/v1/nodes/rhalff/math/add')

    //  omit org in uri
    expect(
      loader.parseProviderUrl(
        'https://api.chix.io/v1/nodes/{ns}/{name}',
        '@rhalff/math',
        'add'
      )
    ).to.eql('https://api.chix.io/v1/nodes/math/add')

    expect(
      loader.parseProviderUrl(
        'https://api.chix.io/v1/nodes/{ns}/{name}',
        'math',
        'add'
      )
    ).to.eql('https://api.chix.io/v1/nodes/math/add')

    expect(
      loader.parseProviderUrl(
        'https://api.chix.io/v1/nodes/{org}{?ns,name}&path={/segments*}',
        '@rhalff/math',
        'add'
      )
    ).to.eql('https://api.chix.io/v1/nodes/rhalff?ns=math&name=add&path=')

    expect(
      loader.parseProviderUrl(
        'https://api.chix.io/v1/nodes/{org}{?ns,name}&path={/segments*}',
        '@rhalff/math/sub/path',
        'add'
      )
    ).to.eql(
      'https://api.chix.io/v1/nodes/rhalff?ns=math&name=add&path=/sub/path'
    )
  })
})
