import {NodeDependencies} from '@chix/common'
import {Installer} from '@chix/install'
import {DependencyLoader} from './DependencyLoader'

export interface RequireDependencyLoaderOptions {
  installPath?: string
}

/**
 *  The require dependency loader loads a dependency using
 *  one of the available require methods.
 *
 *  On the server it will install requirements through npm.
 *
 *  On the client it will load browserified bundles and append
 *  them to the head of the document.
 */
export class RequireDependencyLoader extends DependencyLoader {
  public installer: Installer
  public installPath: string
  constructor(options?: RequireDependencyLoaderOptions) {
    super()
    this.installPath = (options && options.installPath) || process.cwd()
    this.installer = new Installer({prefix: this.installPath})
  }

  public async loadDependency(packagePath: string, requestedVersion: string) {
    if (!this.dependencyMap.has(packagePath)) {
      this.dependencyMap.set(packagePath, {
        instance: this.installer.require(packagePath),
        version: requestedVersion,
      })
    }
  }

  public async load(dependencies: NodeDependencies) {
    await this.installer.load(dependencies)

    if (dependencies.npm) {
      for (const [packagePath, version] of Object.entries(dependencies.npm)) {
        await this.loadDependency(packagePath, version)
      }
    }
  }
}
