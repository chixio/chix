import {NodeDefinition, NodeDependencyTypes} from '@chix/common'
import {Dependencies, DependencyTypes} from '../types'
import {parseDependencies} from '../util'

export class DependencyManager {
  private dependencies: Dependencies = {}

  public parseDependencies(nodeDefinition: NodeDefinition): void {
    if (nodeDefinition.dependencies) {
      this.dependencies = parseDependencies(
        this.dependencies,
        nodeDefinition.dependencies
      )
    }
  }

  public setDependencies(dependencies: Dependencies): void {
    if (this.hasDependencies()) {
      throw Error(
        `Dependencies already set, only use this method to prepopulate the dependencies.`
      )
    }

    this.dependencies = dependencies
  }

  /**
   * Get dependencies for the type given.
   *
   * If no type is given will return all dependencies.
   *
   * @param {DependencyTypes} type
   * @public
   */
  public getDependencies(
    type?: DependencyTypes
  ): Dependencies | NodeDependencyTypes {
    if (type) {
      if (this.dependencies.hasOwnProperty(type)) {
        return this.dependencies[type] as NodeDependencyTypes
      }

      return {}
    }

    return this.dependencies
  }

  /**
   * Checks whether there are any dependencies.
   *
   * If no type is given it will tell whether there are *any* dependencies
   *
   * @param {DependencyTypes} type
   * @public
   */
  public hasDependencies(type?: DependencyTypes): boolean {
    if (type) {
      if (
        this.dependencies.hasOwnProperty(type) &&
        typeof this.dependencies[type] === 'object'
      ) {
        return Boolean(Object.keys(this.dependencies[type] as any).length)
      }
    } else {
      for (const _type in this.dependencies) {
        if (this.dependencies.hasOwnProperty(_type)) {
          if (this.hasDependencies(_type as DependencyTypes)) {
            return true
          }
        }
      }
    }

    return false
  }
}
