import {NodeDependencies} from '@chix/common'
import {DependencyLoader} from './DependencyLoader'

/**
 *  Mock dependency loader
 */
export class MockDependencyLoader extends DependencyLoader {
  public async load(_dependencies: NodeDependencies): Promise<void> {}

  public require(_dependencyPath: string) {
    return {}
  }

  public async loadDependency(
    _packagePath: string,
    _requestedVersion: string
  ) {}

  public hasDependency(_packageName: string) {
    return true
  }
}
