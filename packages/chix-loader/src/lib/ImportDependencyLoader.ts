import {NodeDependencies} from '@chix/common'
import {DependencyLoader} from './DependencyLoader'

const removeXsReg = /\.X/gi

/**
 *  The import dependency loader loads a dependency using dynamic imports.
 */

export class ImportDependencyLoader extends DependencyLoader {
  public async loadDependency(packagePath: string, requestedVersion: string) {
    let version = requestedVersion
    if (!this.dependencyMap.has(packagePath)) {
      const pinnedVersion = this.getPinnedVersion(packagePath)

      version = this.normalizeVersion(
        pinnedVersion ? pinnedVersion : requestedVersion
      )

      const {name, component, path, rest} = this.getPathSegments(packagePath)

      if (!this.dependencyMap.has(path)) {
        this.dependencyMap.set(path, {
          instance: await import(
            /*webpackIgnore: true*/ `https://dev.jspm.io/npm:${name}@${version}${rest}`
          ),
          version,
        })
      }

      if (component) {
        const {instance} = this.dependencyMap.get(path) as any

        if (instance.default && instance.default.hasOwnProperty(component)) {
          this.dependencyMap.set(packagePath, {
            instance: instance.default[component],
            version,
          })
        } else {
          console.error(
            `Module ${path} does not have an export named ${component}`
          )
        }
      }
    }

    const dependency = this.dependencyMap.get(packagePath)

    if (dependency && dependency.version !== requestedVersion) {
      console.warn(
        `Requested package ${packagePath} with ${version}, but providing already loaded version ${dependency.version}`
      )
    }
  }
  public async load(dependencies: NodeDependencies): Promise<void> {
    if (dependencies.npm) {
      for (const [packagePath, requestedVersion] of Object.entries(
        dependencies.npm
      )) {
        await this.loadDependency(packagePath, requestedVersion)
      }
    }
  }

  /**
   * Some versions are specified as 3.x.x, these will be truncated to just 3
   *
   * @param version
   */
  private normalizeVersion(version: string) {
    return version.replace(removeXsReg, '')
  }
}
