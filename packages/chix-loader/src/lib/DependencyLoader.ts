import {NodeDependencies} from '@chix/common'

export interface DependencyMapEntry {
  instance: any
  version: string
}

export interface PinnedDependency {
  name: string
  version: string
}

function _underscored(str: string) {
  return str
    .replace(/([a-z\d])([A-Z]+)/g, '$1_$2')
    .replace(/[.-\s]+/g, '_')
    .toLowerCase()
}

export abstract class DependencyLoader {
  protected dependencyMap: Map<string, DependencyMapEntry> = new Map()
  protected pinnedDependencies: PinnedDependency[] = []

  /**
   * Pins a dependency to a certain version
   *
   * If the packagePath contains a slash it will match any package starting with that string.
   *
   * No slash means it should be an exact package name match
   *
   * @param packagePath
   * @param version
   */
  public pinVersion(packagePath: string, version: string) {
    const pinnedVersion = this.getPinnedVersion(packagePath)

    if (pinnedVersion) {
      throw Error(`Dependency already pinned at ${pinnedVersion}`)
    }

    this.pinnedDependencies.push({
      name: packagePath,
      version,
    })
  }
  public getPinnedVersion(packagePath: string) {
    const pinned = this.pinnedDependencies.find((dep) => {
      const exactMatch = dep.name === packagePath.replace(/\/$/, '')
      const startsWith =
        packagePath.indexOf('/') >= 0 && packagePath.startsWith(dep.name)
      return exactMatch || startsWith
    })

    return pinned ? pinned.version : null
  }
  /**
   * Requires a loaded dependency
   *
   * @param dependencyPath
   */
  public require(dependencyPath: string) {
    const dependency = this.dependencyMap.get(dependencyPath)
    if (dependency) {
      return dependency.instance.default
        ? dependency.instance.default
        : dependency.instance
    }

    throw Error(`Dependency ${dependencyPath} was not found load it first`)
  }

  /**
   * Checks whether a dependency has been loaded.
   *
   * @param packageName
   */
  public hasDependency(packageName: string) {
    return this.dependencyMap.has(packageName)
  }

  /**
   * Parses the package path of the dependency in the form:
   *
   * my-package/lib/file#Type
   * {name}/{rest}#{component?}
   *
   */
  public getPathSegments(packagePath: string) {
    // my-package#Class
    const [path, component] = packagePath.split('#')

    const parts = path.split('/')
    const name = parts[0]
    let rest = ''
    if (parts.length > 1) {
      rest = `/${parts.slice(1).join('/')}`
    }
    const varName = component ? component : _underscored(parts.pop() as string)

    return {
      name,
      component,
      path,
      rest,
      varName,
    }
  }

  public abstract load(dependencies: NodeDependencies): Promise<void>
  public abstract loadDependency(
    packagePath: string,
    requestedVersion: string
  ): Promise<void>
}
