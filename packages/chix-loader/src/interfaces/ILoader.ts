import {
  Flow as FlowDefinition,
  Node,
  NodeDefinition,
  NodeDefinitionProviderMap,
  NodeDefinitions,
} from '@chix/common'
import {EventEmitter} from 'events'
import {Dependencies, DependencyTypes, Workload} from '../types'

export interface LoaderResponse {
  dependencies?: Dependencies
  nodeDefinitions: NodeDefinitionProviderMap
  providerLocation?: string
}

export interface ILoader extends EventEmitter {
  load(
    flow: FlowDefinition | FlowDefinition[],
    update?: boolean
  ): Promise<LoaderResponse>
  getWorkLoad(
    flows: FlowDefinition[],
    defaultProvider: string,
    update: boolean
  ): Workload
  addNodeDefinitions(
    identifier: string,
    nodeDefs: NodeDefinitions | Array<FlowDefinition | NodeDefinition>
  ): void
  addNodeDefinition(
    identifier: string,
    nodeDef: FlowDefinition | NodeDefinition
  ): void
  hasNodeDefinition(providerUrl: string, ns: string, name: string): boolean
  getNodeDefinition(
    node: Node,
    flow: FlowDefinition
  ): Promise<FlowDefinition | NodeDefinition>
  getNodeDefinitionFrom(
    provider: string,
    ns: string,
    name: string
  ): FlowDefinition | NodeDefinition
  saveNodeDefinition(
    _providerLocation: string,
    _nodeDefinition: FlowDefinition | NodeDefinition
  ): void
  getNodeDefinitions(
    provider?: string
  ): NodeDefinitions | NodeDefinitionProviderMap
  setNodeDefinitions(nodeDefinitions: NodeDefinitionProviderMap): void
  hasDependencies(type?: DependencyTypes): boolean
  getDependencies(
    type?: DependencyTypes
  ):
    | {
        [dependency: string]: string
      }
    | {
        npm?:
          | {
              [dependency: string]: string
            }
          | undefined
      }
  setDependencies(dependencies: Dependencies): void
}
