import {forOf} from '@fbpx/lib'
import * as events from 'events'
import _ from 'lodash'
import * as parser from 'uri-template'

import {
  BaseNodeDefinition,
  ExternalPort,
  Flow as FlowDefinition,
  FSProvider,
  Node,
  NodeDefinition,
  NodeDefinitionProviderMap,
  NodeDefinitions,
  NodeDependencyTypes,
  RemoteProvider,
} from '@chix/common'

import {ILoader, LoaderResponse} from './interfaces'

import {DependencyManager} from './lib/DependencyManager'

import {
  Dependencies,
  DependencyTypes,
  FSWorkload,
  LoadDefinitionResult,
  LoadNodeResult,
  ProviderDef,
  RemoteWorkload,
  WillLoad,
  Workload,
} from './types'

import {
  findNodeWithinGraph,
  isFSProvider,
  isProviderHandle,
  isRemoteProvider,
  isUri,
  loadDependencies,
} from './util'

export class Loader extends events.EventEmitter implements ILoader {
  public name = 'Loader'
  // npm loader returns a different source
  public getNodeSource = Loader.prototype.getNodeDefinitionFrom

  protected dependencyManager: DependencyManager = new DependencyManager()
  /**
   *
   * Format:
   *
   * Keeps track of all known nodeDefinitions.
   *
   * {
   *  'http://....': { // url in this case is the identifier
   *
   *    fs: {
   *       readFile:  <definition>
   *       writeFile: <definition>
   *    }
   *
   *  }
   *
   * }
   */
  private nodeDefinitions: NodeDefinitionProviderMap = {}

  /**
   * This is the main method all child classes should implement.
   *
   * @param {FlowDefinition[]} _flows
   * @param {boolean} _update
   * @returns {Promise<LoaderResponse>}
   */
  public async load(
    _flows: FlowDefinition | FlowDefinition[],
    _update?: boolean
  ): Promise<LoaderResponse> {
    // Checks if everything is preloaded
    // if it's not preloaded then throw an error
    // Loader like this is used e.g. in chix-runtime for tests
    const flows = Array.isArray(_flows) ? _flows : [_flows]

    const workload = this.getWorkLoad(flows, '@', false)

    if (workload.length) {
      throw Error('Default loader expects all nodeDefinitions to be preloaded')
    }

    return {
      dependencies: this.getDependencies(),
      nodeDefinitions: this.getNodeDefinitions() as NodeDefinitionProviderMap,
    }
  }

  public async loadNodeDefinitionFrom(
    provider: string,
    ns: string,
    name: string
  ): Promise<LoadDefinitionResult> {
    const loadNodeResult = await this.loadNode({
      ns,
      name,
      url: this.parseProviderUrl(provider, ns, name),
      providerLocation: provider,
    })

    const nodeDefinition = loadNodeResult.nodeDef
    // res.providerLocation

    // provide a way to getDependencies for only this nodeDefinition.
    // parseDependencies will just update *all* dependencies
    // so in case if the nodeDefinition is a flow, it's just a method to traverse
    // all dependencies and return that.
    if ((nodeDefinition as NodeDefinition).dependencies) {
      this.dependencyManager.parseDependencies(nodeDefinition as NodeDefinition)
    }

    this.setNodeDefinition(provider, nodeDefinition)

    if (nodeDefinition.type === 'flow') {
      const flow = nodeDefinition as FlowDefinition

      // Note: returns the full state, which in this case can be ignored.
      await this.load([flow])

      return {
        nodeDefinition,
        dependencies: loadDependencies(
          nodeDefinition,
          this.getNodeDefinitions() as NodeDefinitionProviderMap
        ),
      }
    }

    return {
      nodeDefinition,
      dependencies: loadDependencies(
        nodeDefinition,
        // this.nodeDefinitionManager.getNodeDefinitions() as NodeDefinitionProviderMap
        this.getNodeDefinitions() as NodeDefinitionProviderMap
      ),
    }
  }

  /**
   * Loader itself only expects preloaded nodes.
   * There is no actual load going on.
   *
   * Remote loader *does* implement loading.
   *
   * @param {ProviderDef} providerDef
   * @returns {Promise<{nodeDef: NodeDefinition | {}}>}
   */
  public async loadNode(providerDef: ProviderDef): Promise<LoadNodeResult> {
    // const { provider, ns, name } = providerDef
    const {providerLocation, ns, name} = providerDef

    // const nodeDef = this.nodeDefinitionManager.getNodeDefinitionFrom(
    const nodeDef = this.getNodeDefinitionFrom(
      // provider,
      providerLocation,
      ns,
      name
    ) as NodeDefinition

    if (nodeDef) {
      return {
        providerLocation,
        nodeDef,
      }
    }

    throw Error(`Could not load node ${ns}:${name} from ${providerLocation}`)
  }

  /**
   * loop the nodes, detect what provider they expect
   * if they do not have a provider we will use the default,
   * if the flow has set it's own default we will use that.
   * the default within a flow is the one without namespace
   */
  public getWorkLoad(
    flows: FlowDefinition[],
    defaultProvider: string,
    update: boolean
  ): Workload {
    const willLoad: WillLoad = []
    const workload: Workload = []

    flows.forEach((flow: FlowDefinition) => {
      // Todo for subflows this runs every time.
      // this.nodeDefinitionManager.initProviderMap(
      this.initProviderMap(flow, defaultProvider)

      for (const node of flow.nodes) {
        let providerLocation
        let remote

        // select the name e.g. x
        const provider = node.provider ? node.provider : '@'

        if (!isProviderHandle(provider)) {
          // it's already an url or path
          providerLocation = provider
          remote = true
        } else {
          if (!flow.providers || !flow.providers.hasOwnProperty(provider)) {
            throw Error(
              [
                'Node',
                node.ns + ':' + node.name,
                'refers to unknown provider',
                provider,
                '\n\tplease specify it in the providers section',
              ].join(' ')
            )
          }

          // Within the flow detect what url x is about
          if (flow.providers[provider].hasOwnProperty('path')) {
            // hacked in, remote also takes care of local for now.
            // just refactor this later on.
            providerLocation = (flow.providers[provider] as FSProvider).path
            remote = false
          } else if (flow.providers[provider].hasOwnProperty('url')) {
            providerLocation = (flow.providers[provider] as RemoteProvider).url
            remote = true
          } else {
            throw Error(`Do not know how to handle: ${provider}`)
          }
        }

        // if (!this.nodeDefinitionManager.hasNodeDefinition(
        if (
          !this.hasNodeDefinition(providerLocation, node.ns, node.name) &&
          !update
        ) {
          const location = this.parseProviderUrl(
            providerLocation,
            node.ns,
            node.name
          )

          // Check if it carries the info for our node also.
          if (willLoad.indexOf(location) === -1) {
            willLoad.push(location)

            if (remote) {
              workload.push({
                url: location,
                ns: node.ns,
                name: node.name,
                providerLocation,
              } as RemoteWorkload)
            } else {
              workload.push({
                path: location,
                ns: node.ns,
                name: node.name,
                providerLocation,
              } as FSWorkload)
            }
          }
        }
      }
    })

    return workload
  }

  public saveNodeDefinition(
    _providerLocation: string,
    _nodeDefinition: NodeDefinition
  ) {
    throw new Error(
      [this.constructor.name, 'must implement a save method'].join(' ')
    )
  }

  /**
   * Retrieves the dependency list for a node definition.
   *
   * If the node definition is a flow all required node definitions are expected to already
   * have been loaded. As these are used to traverse the tree in order to collect all dependencies.
   */
  public getDependenciesFor(node: NodeDefinition | FlowDefinition) {
    return loadDependencies(
      node,
      this.getNodeDefinitions() as NodeDefinitionProviderMap
    )
  }

  public hasDependencies(type?: DependencyTypes): boolean {
    return this.dependencyManager.hasDependencies(type)
  }

  public getDependencies(
    type?: DependencyTypes
  ): Dependencies | NodeDependencyTypes {
    return this.dependencyManager.getDependencies(type)
  }

  public setDependencies(dependencies: Dependencies) {
    return this.dependencyManager.setDependencies(dependencies)
  }

  public initProviderMap(flow: FlowDefinition, defaultProvider: string) {
    if (!flow.providers) {
      flow.providers = {}
    }

    if (!flow.providers['@']) {
      flow.providers['@'] = {url: defaultProvider} as RemoteProvider
    }

    for (const key in flow.providers) {
      if (flow.providers.hasOwnProperty(key)) {
        const provider = flow.providers[key]

        if (isRemoteProvider(provider)) {
          if (
            !this.nodeDefinitions.hasOwnProperty(
              (provider as RemoteProvider).url
            )
          ) {
            this.nodeDefinitions[(provider as RemoteProvider).url] = {}
          }
        } else if (isFSProvider(provider)) {
          if (
            !this.nodeDefinitions.hasOwnProperty((provider as FSProvider).path)
          ) {
            this.nodeDefinitions[(provider as FSProvider).path] = {}
          }
        }
      }
    }
  }

  /**
   *
   * Add node definitions
   *
   * Used to `statically` add nodeDefinitions.
   *
   * @param {String} identifier
   * @param {Object} nodeDefs
   * @api public
   */
  public addNodeDefinitions(
    identifier: string,
    nodeDefs: NodeDefinitions | (FlowDefinition | NodeDefinition)[]
  ) {
    if (Array.isArray(nodeDefs)) {
      for (const nodeDef of nodeDefs) {
        this.addNodeDefinition(identifier, nodeDef)
      }
    } else {
      forOf((_ns: string, _name: string, nodeDefinition: NodeDefinition) => {
        this.addNodeDefinition(identifier, nodeDefinition)
      }, nodeDefs)
    }
  }

  /**
   *
   * Add a node definition
   *
   * @param {String} identifier
   * @param {Object} nodeDef
   * @api public
   */
  public addNodeDefinition(
    identifier: string,
    nodeDef: NodeDefinition | FlowDefinition
  ) {
    if (!nodeDef.hasOwnProperty('ns')) {
      throw new Error(
        ['Node definition for', identifier, 'lacks an ns property'].join(' ')
      )
    }

    if (!nodeDef.hasOwnProperty('name')) {
      throw new Error(
        ['Node definition for', identifier, 'lacks an name property'].join(' ')
      )
    }

    // store the provider url along with the nodeDefinition
    // Needed for fetching back from storage.
    nodeDef.provider = identifier

    if (nodeDef.type === 'flow') {
      if (!nodeDef.providers || Object.keys(nodeDef.providers).length === 0) {
        nodeDef.providers = {
          '@': {
            url: identifier,
          },
        }
      }
    } else {
      this.dependencyManager.parseDependencies(nodeDef as NodeDefinition)
    }

    this.setNodeDefinition(identifier, nodeDef)
  }

  public setNodeDefinition(
    provider: string,
    nodeDefinition: NodeDefinition | FlowDefinition
  ) {
    const {ns, name} = nodeDefinition

    _.setWith(
      this.nodeDefinitions,
      [provider, ns, name],
      nodeDefinition,
      Object
    )
  }

  public setNodeDefinitions(nodeDefinitions: NodeDefinitionProviderMap) {
    if (this.hasNodeDefinitions()) {
      throw Error(
        'Node Definitions already set, only use this method to pre-populate the nodeDefinitions'
      )
    }
    this.nodeDefinitions = nodeDefinitions
  }

  /**
   *
   * Check whether we have the definition
   *
   * @param {String} providerUrl
   * @param {String} ns
   * @param {String} name
   * @api public
   */
  public hasNodeDefinition(providerUrl: string, ns: string, name: string) {
    return (
      this.nodeDefinitions.hasOwnProperty(providerUrl) &&
      this.nodeDefinitions[providerUrl].hasOwnProperty(ns) &&
      this.nodeDefinitions[providerUrl][ns].hasOwnProperty(name)
    )
  }

  public hasNodeDefinitions(provider?: string) {
    if (provider) {
      return Boolean(this.nodeDefinitions[provider])
    }
    return Object.keys(this.nodeDefinitions).length > 0
  }

  /**
   * Loads the NodeDefinition for a node.
   *
   * In order to do so each node must know the provider url it was loaded from.
   *
   * This is normally not stored directly into flows, but the flow is saved with the namespaces in use.
   *
   * TODO: this really should just be (provider, name, ns, version) else use the load method.
   */
  public async getNodeDefinition(
    node: Node,
    flow: FlowDefinition
  ): Promise<FlowDefinition | NodeDefinition> {
    const definition = await this.loadNodeDefinition(node, flow)

    // merge the schema of the internal node.
    if (definition.type === 'flow') {
      await this.mergeSchema(definition as FlowDefinition) // in place
    }

    return definition
  }

  /**
   * Resolves the provider url in-place
   *
   * @param {Node} node
   * @param {FlowDefinition} flow
   * @memberof NodeDefinitionManager
   */
  public resolveProvider(node: Node, flow: FlowDefinition): string {
    let location: string
    let provider: FSProvider | RemoteProvider

    if (node.provider && isUri(node.provider)) {
      // it's already an url
      location = node.provider
    } else if (!node.provider && (!flow || !flow.providers)) {
      // for direct additions (only @ is possible)
      location = '@'
    } else {
      if (!flow) {
        throw Error(
          'loadNodeDefinition needs a map or a node with a full provider url'
        )
      }

      if (!flow.providers) {
        throw Error('loadNodeDefinition expects flow to container .providers')
      }

      provider = node.provider
        ? // 'x': ..
          flow.providers[node.provider]
        : flow.providers['@']

      // fix: find provider by path or url, has to do with provider
      // already resolved (sub.sub.graphs)
      if (!provider) {
        for (const key of Object.keys(flow.providers)) {
          if (
            _.get(flow, ['providers', key, 'url']) === node.provider ||
            _.get(flow, ['providers', key, 'path']) === node.provider
          ) {
            provider = flow.providers[key]
          }
        }
        if (!provider) {
          throw Error('unable to find provider')
        }
      }

      if ((provider as FSProvider).hasOwnProperty('path')) {
        location = (provider as FSProvider).path
      } else if ((provider as RemoteProvider).hasOwnProperty('url')) {
        location = (provider as RemoteProvider).url
      } else {
        throw new Error('Do not know how to handle provider')
      }

      // do not overwrite provider anymore
      // node.provider = location
    }

    return location
  }

  public async loadNodeDefinition(
    node: Node,
    flow: FlowDefinition
  ): Promise<FlowDefinition | NodeDefinition> {
    const location = this.resolveProvider(node, flow)

    if (!this.hasNodeDefinition(location, node.ns, node.name)) {
      await this.load(flow)
    }

    return this.nodeDefinitions[location][node.ns][node.name] as NodeDefinition
  }

  public getNodeDefinitionFrom(
    provider: string,
    ns: string,
    name: string
  ): FlowDefinition | NodeDefinition {
    if (this.hasNodeDefinition(provider, ns, name)) {
      return this.nodeDefinitions[provider][ns][name]
    }

    throw Error(`Unable to find node definition for ${ns}:${name}`)
  }

  public getNodeDefinitionForNode(nodeId: string, flow: FlowDefinition) {
    const node = flow.nodes.find((item) => item.id === nodeId)

    if (!node) {
      throw Error('Unable to find node')
    }

    return this.getNodeDefinitionFrom(
      this.resolveProvider(node, flow),
      node.ns,
      node.name
    )
  }

  /**
   * Get Nodedefinitions
   *
   * Optionally with a provider url so it returns only the node definitions
   * at that provider.
   *
   * @param {String} provider (Optional)
   */
  public getNodeDefinitions(
    provider?: string
  ): NodeDefinitionProviderMap | NodeDefinitions {
    if (provider) {
      return this.nodeDefinitions[provider]
    } else {
      return this.nodeDefinitions
    }
  }

  /**
   * Test whether all nodes used by this flow have been resolved
   *
   * @param {FlowDefinition} flow
   * @returns {boolean}
   */
  public isResolved(flow: FlowDefinition): boolean {
    const seen: string[] = []

    for (const node of flow.nodes) {
      if (!this.hasNodeDefinition('@', node.ns, node.name)) {
        return false
      }

      const nodeDefinition = this.getNodeDefinitionFrom(
        '@',
        node.ns,
        node.name
      ) as BaseNodeDefinition

      const identifier = `${nodeDefinition.ns}:${nodeDefinition.name}`

      if (
        (nodeDefinition as FlowDefinition).type === 'flow' &&
        seen.indexOf(identifier) === -1
      ) {
        seen.push(identifier)

        return this.isResolved(nodeDefinition as FlowDefinition)
      }
    }

    return true
  }

  public getUnresolved(flow: FlowDefinition): BaseNodeDefinition[] {
    // tslint:disable-next-line
    const self = this

    const unresolved: BaseNodeDefinition[] = []
    const seen: string[] = []

    function resolveDefinitions(flowDefinition: FlowDefinition) {
      for (const node of flowDefinition.nodes) {
        if (!self.hasNodeDefinition('@', node.ns, node.name)) {
          unresolved.push(node)
        } else {
          const nodeDefinition = self.getNodeDefinitionFrom(
            '@',
            node.ns,
            node.name
          ) as BaseNodeDefinition

          const identifier = `${nodeDefinition.ns}:${nodeDefinition.name}`

          if (
            (nodeDefinition as FlowDefinition).type === 'flow' &&
            seen.indexOf(identifier) === -1
          ) {
            seen.push(identifier)

            resolveDefinitions(nodeDefinition as FlowDefinition)
          }
        }
      }
    }

    resolveDefinitions(flow)

    return unresolved
  }

  // this logic is also chix-ui
  // so take that logic.
  public async mergeSchema(graph: FlowDefinition): Promise<any[]> {
    return Promise.all(
      forOf(
        async (
          type: 'input' | 'output',
          port: string,
          externalPort: ExternalPort
        ): Promise<boolean> => {
          if (externalPort.hasOwnProperty('nodeId')) {
            // TODO: should go recursive if internal node is a flow, in able to get the correct type.
            const internalDef = (await this.getNodeDefinition(
              findNodeWithinGraph(externalPort.nodeId, graph),
              graph
            )) as FlowDefinition

            if (!_.has(internalDef, ['ports', type, externalPort.name])) {
              throw Error(`External Port points to non-existent internal port`)
            }

            const copy: ExternalPort = JSON.parse(
              JSON.stringify(
                _.get(internalDef, ['ports', type, externalPort.name])
              )
            )
            copy.title = externalPort.title
            copy.name = externalPort.name
            copy.nodeId = externalPort.nodeId

            _.setWith(graph, ['ports', type, port], copy, Object)

            return true
          }

          // not pointing to an internal node. :start etc.
          return false
        },
        graph.ports
      )
    )
  }

  /**
   * Parses the namespace into org (if specified), ns and possibly trailing path segments
   *
   * @param namespace
   */
  public parseNamespace(namespace: string) {
    let parts
    if (namespace.startsWith('@')) {
      parts = namespace.match(/@([\w-]+)\/(\w+)\/?(.+)?/)

      if (parts) {
        const [, org, ns, segments] = parts

        return {
          org,
          ns,
          segments: segments ? segments.split('/') : [],
        }
      }
    } else {
      parts = namespace.match(/([\w-]+)\/?(.+)?/)

      if (parts) {
        const [, ns, segments] = parts

        return {
          ns,
          segments: segments ? segments.split('/') : [],
        }
      }
    }

    throw Error('failed to parse namespace')
  }

  public parseProviderUrl(
    uriTemplate: string,
    namespace: string,
    name: string
  ) {
    const {org, ns, segments} = this.parseNamespace(namespace)
    const tpl = parser.parse(uriTemplate)

    return tpl.expand({
      org,
      ns,
      name,
      segments,
    })
  }
}
