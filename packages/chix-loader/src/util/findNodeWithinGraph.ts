import {Flow, Node} from '@chix/common'

export function findNodeWithinGraph(nodeId: string, graph: Flow): Node {
  for (const node of graph.nodes) {
    if (nodeId === node.id) {
      return node
    }
  }

  throw Error('Could not find internal node within graph')
}
