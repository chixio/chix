import {FSProvider, RemoteProvider} from '@chix/common'

export function isFSProvider(provider: FSProvider | RemoteProvider): boolean {
  return (provider as FSProvider).path !== undefined
}
