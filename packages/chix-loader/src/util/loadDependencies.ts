import {
  BaseNodeDefinition,
  Flow,
  NodeDefinition,
  NodeDefinitionProviderMap,
} from '@chix/common'
import {Dependencies} from '../types'
import {collectDependencies} from './collectDependencies'
import {parseDependencies} from './parseDependencies'

/**
 * Get the dependencies for a certain provider.
 *
 * @param definition
 * @param nodeDefinitions
 * @returns {Dependencies}
 */
export function loadDependencies(
  definition: BaseNodeDefinition,
  nodeDefinitions: NodeDefinitionProviderMap
): Dependencies {
  let dependencies: Dependencies = {}

  if ((definition as Flow).type === 'flow') {
    dependencies = collectDependencies(
      definition as Flow,
      nodeDefinitions,
      dependencies
    )

    return dependencies
  }

  if ((definition as NodeDefinition).dependencies) {
    dependencies = parseDependencies(
      dependencies,
      (definition as NodeDefinition).dependencies as Dependencies
    )
  }

  return dependencies
}
