export function isProviderHandle(provider: string): boolean {
  return /^(\w|@)+$/.test(provider)
}
