import {NodeDependencies} from '@chix/common'
import {forOf} from '@fbpx/lib'
import {Dependencies} from '../types'

export function parseDependencies(
  dependencies: Dependencies,
  nodeDependencies: NodeDependencies
) {
  if (nodeDependencies) {
    forOf((type: string, dependency: string, value: string) => {
      if (type === 'npm') {
        if (value !== 'builtin') {
          /*
          requireString = r + '@' + nodeDef.dependencies.npm[r]
          if (requires.indexOf(requireString) === -1) {
            requires.push(requireString)
          }
          */
          if (!dependencies.npm) {
            dependencies.npm = {}
          }

          // TODO: check for duplicates and pick the latest one.
          // dependencies.npm[dependency] = nodeDef.dependencies.npm[dependency]
          dependencies.npm[dependency] = value
        }
      } else {
        console.warn(`Unsupported package manager: ${type}`)
      }
    }, nodeDependencies)
  }

  return dependencies
}
