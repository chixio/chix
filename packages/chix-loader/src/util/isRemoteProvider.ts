import {FSProvider, RemoteProvider} from '@chix/common'

export function isRemoteProvider(
  provider: FSProvider | RemoteProvider
): boolean {
  return (provider as RemoteProvider).url !== undefined
}
