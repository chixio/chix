export function isUri(value: string): boolean {
  return value.indexOf('://') >= 0
}
