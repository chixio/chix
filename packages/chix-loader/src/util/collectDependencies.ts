import {
  BaseNodeDefinition,
  Flow,
  FSProvider,
  NodeDefinition,
  NodeDefinitionProviderMap,
  NodeDependencies,
  RemoteProvider,
} from '@chix/common'
import _ from 'lodash'
import {isUri} from './isUri'
import {parseDependencies} from './parseDependencies'

/**
 * Recursively returns the requires.
 *
 * Assumes the map, node is already loaded.
 *
 * Note: Will break if node.provider is an url already.
 *
 * @private
 */
export function collectDependencies(
  flow: Flow,
  nodeDefinitions: NodeDefinitionProviderMap,
  dependencies: NodeDependencies = {}
) {
  flow.nodes.forEach(node => {
    let provider

    if (node.provider && isUri(node.provider)) {
      provider = node.provider
    } else {
      const providerHandle = node.provider || '@'

      if (flow.providers && flow.providers[providerHandle]) {
        provider =
          (flow.providers[providerHandle] as RemoteProvider).url ||
          (flow.providers[providerHandle] as FSProvider).path
      } else {
        throw Error('Flow does not contain a providers section.')
      }
    }

    const nodeDefinition: BaseNodeDefinition = _.get(nodeDefinitions, [
      provider,
      node.ns,
      node.name,
    ])

    if ((nodeDefinition as Flow).type === 'flow') {
      dependencies = collectDependencies(
        nodeDefinition as Flow,
        nodeDefinitions,
        dependencies
      )
    } else {
      if ((nodeDefinition as NodeDefinition).dependencies) {
        dependencies = parseDependencies(
          dependencies,
          (nodeDefinition as NodeDefinition).dependencies as NodeDependencies
        )
      }
    }
  })

  return dependencies
}
