Chiχ
====

Contains two utilities to prepare [nodule source files](https://github.com/nodule) to compiled versions.

`chix-write` will create an `x.json` and can be sent to a remote api.

`chix-compile` creates the .js files which can be installed locally through npm.

