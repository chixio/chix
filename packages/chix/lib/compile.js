"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const fs = __importStar(require("fs"));
const path = __importStar(require("path"));
const commander_1 = require("commander");
const glob_1 = require("glob");
const browserify_1 = __importDefault(require("browserify"));
const common_1 = require("@chix/common");
const jsonGate = __importStar(require("json-gate"));
const util_1 = require("./util");
const nodeSchema = jsonGate.createSchema(common_1.NodeSchema);
const mapSchema = jsonGate.createSchema(common_1.FlowSchema);
const b = (0, browserify_1.default)();
const dirname = process.cwd();
let globPattern = '/nodes/**/node.js*';
commander_1.program
    .usage('[options]')
    .option('-g, --glob', 'globbing pattern default: ' + globPattern)
    .option('-d, --dry', 'dry run')
    .parse(process.argv);
const options = commander_1.program.opts();
const write = !options.dry;
globPattern = options.glob || globPattern;
let c;
(async () => {
    const pkg = await (0, util_1.getPackageJson)();
    const x = {
        name: pkg.chix.name,
        description: pkg.description,
        version: pkg.version,
        licences: pkg.licenses,
        repository: pkg.repository,
        dependencies: pkg.dependencies,
        nodes: [],
    };
    if (pkg.chix.env) {
        x.env = pkg.chix.env;
    }
    b.bundle((error, _src) => {
        if (error) {
            throw error;
        }
        const fns = {};
        let files;
        files = (0, glob_1.globSync)(dirname + globPattern);
        let parts;
        let module;
        let ext;
        for (const item of files) {
            parts = item.split('/');
            module = parts[1];
            ext = path.extname(item);
            c = fs.readFileSync(item).toString();
            let node = {};
            if (ext === '.js') {
                fns[module] = c;
            }
            else if (ext === '.json') {
                console.log(item);
                const json = JSON.parse(c);
                if (!json.name) {
                    throw Error('name is missing in node.json file' + module);
                }
                node = { ...node, ...json };
                if (x.env && !node.env) {
                    node.env = x.env;
                }
                node.ns = node.ns ? node.ns : x.name;
                node.fn = fns[module];
                nodeSchema.validate(node);
                x.nodes.push(node);
            }
        }
        files = (0, glob_1.globSync)(dirname + '/twigs/*.fbp');
        for (const item of files) {
            const parser = (0, util_1.getParser)();
            console.log('processing %s', item);
            const twig = parser.parse(fs.readFileSync(item).toString());
            const name = path.basename(item, '.fbp');
            twig.ns = x.name;
            twig.name = name;
            mapSchema.validate(twig);
            x.nodes.push(twig);
        }
        const contents = JSON.stringify(x, null, 2);
        if (write) {
            console.log('writing x.json');
            fs.writeFileSync(dirname + '/x.json', contents);
        }
        else {
            console.log(contents);
        }
    });
})();
//# sourceMappingURL=compile.js.map