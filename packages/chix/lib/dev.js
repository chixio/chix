"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const commander_1 = require("commander");
const fs = __importStar(require("fs"));
const child_process_1 = require("child_process");
const util_1 = require("./util");
const cwd = process.cwd();
(async () => {
    const pkg = await (0, util_1.getPackageJson)();
    let type = 'patch';
    const getVersion = (version) => {
        type = version === 'major' || version === 'minor' ? v : type;
    };
    commander_1.program
        .usage('[options] <file>')
        .option('-b, --bump', 'major|minor|patch', getVersion)
        .parse(process.argv);
    const opts = commander_1.program.opts();
    const v = opts.version.match(/(\d)+/g);
    const new_version = {
        major: [1 + parseInt(v[0], 10), 0, 0],
        minor: [v[0], 1 + parseInt(v[1], 10), 0],
        patch: [v[0], v[1], 1 + parseInt(v[2], 10)],
    };
    pkg.version = new_version[type].join('.');
    fs.writeFileSync(cwd + '/package.json', JSON.stringify(pkg, null, '  '));
    const cmds = [];
    if (pkg.version) {
        const versionString = 'v' + pkg.version;
        cmds.push('git checkout master');
        cmds.push('chix-compile');
        cmds.push('git add package.json');
        cmds.push('git add x.json');
        cmds.push('git commit -m "update version to ' + versionString + '"');
        cmds.push('git tag -a ' + versionString + ' -m "new release: ' + versionString + '"');
        cmds.push('git push origin ' + versionString);
        cmds.push('git push origin master');
    }
    const run = cmds.join(';');
    (0, child_process_1.exec)(run, (error, stdout, stderr) => {
        console.log('stdout: ' + stdout);
        console.log('stderr: ' + stderr);
        if (error !== null) {
            console.log('exec error: ' + error);
        }
    });
})();
//# sourceMappingURL=dev.js.map