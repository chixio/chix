"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@chix/common");
const flow_1 = require("@chix/flow");
const camelize = __importStar(require("camelize"));
const fs = __importStar(require("fs"));
const glob_1 = require("glob");
const js_beautify_1 = require("js-beautify");
const jsonGate = __importStar(require("json-gate"));
const mockBrowser = __importStar(require("mock-browser"));
const path = __importStar(require("path"));
const tosource_1 = __importDefault(require("tosource"));
const util_1 = require("./util");
const mapSchema = jsonGate.createSchema(common_1.FlowSchema);
const pkgName = (0, util_1.getPackageName)();
function replaceRequire(str, local) {
    const what = !local ? "require('$1')" : "require('./$1')";
    return str.replace(/"##([\w-_]+)##"/gm, what);
}
const MockBrowser = mockBrowser.mocks.MockBrowser;
const mock = new MockBrowser()(global);
mock.document =
    mock.getDocument()(global).window =
        MockBrowser.createWindow()(global).location =
            mock.getLocation()(global).navigator =
                mock.getNavigator()(global).history =
                    mock.getHistory()(global).localStorage =
                        mock.getLocalStorage()(global).sessionStorage =
                            mock.getSessionStorage()(global).Element =
                                window.Element(global).Mottle =
                                    {
                                        consume: {},
                                    };
const files = (0, glob_1.globSync)('./nodes/**/node.json');
const nodeDefinitions = [];
files.forEach((file) => {
    const nodeDefinition = JSON.parse(fs.readFileSync(file, 'utf8'));
    if (nodeDefinition.dependencies && nodeDefinition.dependencies.npm) {
        Object.keys(nodeDefinition.dependencies.npm).forEach((key) => {
            if (key !== 'builtin') {
                nodeDefinition.dependencies.npm[key] = '##' + key + '##';
            }
        });
    }
    nodeDefinitions.push(nodeDefinition);
    fs.readFile(file.replace('.json', '.js'), 'utf8', (error, contents) => {
        if (error) {
            throw error;
        }
        nodeDefinition.fn = contents;
        const src = (0, flow_1.compile)(nodeDefinition);
        let res = 'module.exports = ' + (0, tosource_1.default)(src);
        res = replaceRequire(res);
        const fileName = [
            process.cwd(),
            '/',
            path.dirname(file).split('/').pop(),
            '.js',
        ].join('');
        fs.writeFile(fileName, (0, js_beautify_1.js_beautify)(res, { indent_size: 2 }), () => {
            console.log('wrote ' + fileName);
        });
    });
});
const twigFiles = (0, glob_1.globSync)('./twigs/*.fbp');
for (const item of twigFiles) {
    const parser = (0, util_1.getParser)();
    console.log('processing %s', item);
    const twig = parser.parse(fs.readFileSync(item).toString());
    const name = path.basename(item, '.fbp');
    twig.ns = pkgName;
    twig.name = name;
    mapSchema.validate(twig);
    const fileName = name + '.js';
    const jsBody = ['module.exports = ', JSON.stringify(twig, null, 2)].join('');
    fs.writeFileSync(fileName, jsBody);
    console.log('wrote %s', fileName);
    nodeDefinitions.push(twig);
}
const idx = {};
nodeDefinitions.forEach((nodeDefinition) => {
    idx[nodeDefinition.name] = `##${nodeDefinition.name}##`;
});
if (Object.keys(idx).length) {
    const res = camelize(idx);
    const src = [
        'module.exports = ',
        replaceRequire(JSON.stringify(res, null, 2), true).replace(/"/g, ''),
    ].join('');
    fs.writeFile('index.js', src, () => {
        console.log('wrote index.js');
    });
}
//# sourceMappingURL=write.js.map