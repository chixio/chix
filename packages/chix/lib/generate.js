"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const fs = __importStar(require("fs"));
const _s = __importStar(require("underscore.string"));
const commander_1 = require("commander");
const dirname = process.cwd();
function output_option(val) {
    return val.split(':');
}
commander_1.program
    .usage('[options] <file>')
    .option('-d, --dry', 'dry run')
    .option('-o, --out <out>:<type>', 'output port', output_option)
    .parse(process.argv);
const opts = commander_1.program.opts();
if (!commander_1.program.args.length) {
    console.log('you should provide a filename');
    process.exit();
}
const file = dirname + '/' + commander_1.program.args[0];
if (!fs.existsSync(file)) {
    console.log('file:', file, 'does not exist');
    process.exit();
}
const write = !opts.dry;
let output_port = 'results';
let output_type = 'object';
if (opts.out.length) {
    output_port = opts.out[0];
    if (opts.out[1]) {
        output_type = opts.out[1];
    }
}
const defaults = {
    fd: 'object',
    buffer: 'object',
    options: 'object',
    family: 'integer',
    len: 'integer',
    uid: 'integer',
    gid: 'integer',
    mode: 'integer',
    offset: 'integer',
    length: 'integer',
    position: 'integer',
};
fs.readFile(file, (err, res) => {
    if (err) {
        throw Error('Error cannot read file ' + file);
    }
    const lines = res.toString().split('\n');
    const nodes = {};
    for (const line of lines) {
        if (line !== '') {
            if (!write) {
                console.log('line:', line);
            }
            const m = line.match(/^(\w+).(\w+)\((.*)\)/);
            if (!write) {
                console.log('match:', m);
            }
            const args = m
                .pop()
                .replace(/[\[\]]/g, '')
                .replace(/\s+/g, '')
                .split(',');
            if (!write) {
                console.log('args:', args);
            }
            const cb = args.pop().replace(/\s+/, '');
            if (!write) {
                console.log('last arg', cb, '\n');
            }
            let isAsync = true;
            if (cb !== 'callback') {
                args.push(cb);
                isAsync = false;
            }
            const module = m[1];
            const method = m[2];
            const input_ports = {};
            for (const item of args) {
                const type = defaults[item] ? defaults[item] : 'string';
                input_ports[item] = { type };
            }
            const description = _s.humanize(module) + ' ' + _s.humanize(method).toLowerCase();
            const json = {
                name: module + '.' + method,
                description,
                ports: {
                    input: input_ports,
                    output: {},
                },
            };
            json.ports.output[output_port] = { type: output_type };
            const argsString = 'input.' + args.join(', input.');
            nodes[method] = { json };
            if (isAsync) {
                nodes[method].fn =
                    'output([' + module + '.' + method + ', ' + argsString + '])';
            }
            else {
                nodes[method].fn =
                    'output({ ' +
                        output_port +
                        ': ' +
                        module +
                        '.' +
                        method +
                        '(' +
                        argsString +
                        ') })';
            }
        }
    }
    if (write) {
        for (const method in nodes) {
            const node = nodes[method];
            if (!fs.existsSync(dirname + '/' + method)) {
                fs.mkdirSync(dirname + '/' + method);
            }
            let f = dirname + '/' + method + '/node.js';
            fs.writeFileSync(f, node.fn);
            console.log('Written:', f);
            f = dirname + '/' + method + '/node.json';
            const json = JSON.stringify(node.json, null, 2);
            fs.writeFileSync(f, json);
            console.log('Written:', f);
        }
    }
    else {
        console.log('nodes', nodes);
    }
});
//# sourceMappingURL=generate.js.map