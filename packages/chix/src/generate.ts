import * as fs from 'fs'
import * as _s from 'underscore.string'
import {program} from 'commander'

const dirname = process.cwd()

function output_option(val) {
  return val.split(':')
}

program
  .usage('[options] <file>')
  .option('-d, --dry', 'dry run')
  .option('-o, --out <out>:<type>', 'output port', output_option)
  .parse(process.argv)

const opts = program.opts()

if (!program.args.length) {
  console.log('you should provide a filename')
  process.exit()
}

const file = dirname + '/' + program.args[0]

if (!fs.existsSync(file)) {
  console.log('file:', file, 'does not exist')
  process.exit()
}

const write = !opts.dry

let output_port = 'results'
let output_type = 'object'

if (opts.out.length) {
  output_port = opts.out[0]
  if (opts.out[1]) {
    output_type = opts.out[1]
  }
}

const defaults = {
  fd: 'object',
  buffer: 'object',
  options: 'object',
  family: 'integer',
  len: 'integer',
  uid: 'integer',
  gid: 'integer',
  mode: 'integer',
  offset: 'integer',
  length: 'integer',
  position: 'integer',
}

fs.readFile(file, (err, res) => {
  if (err) {
    throw Error('Error cannot read file ' + file)
  }

  const lines = res.toString().split('\n')

  const nodes = {}

  for (const line of lines) {
    if (line !== '') {
      if (!write) {
        console.log('line:', line)
      }

      const m = line.match(/^(\w+).(\w+)\((.*)\)/)

      if (!write) {
        console.log('match:', m)
      }

      const args = m
        .pop()
        .replace(/[\[\]]/g, '')
        .replace(/\s+/g, '')
        .split(',')

      if (!write) {
        console.log('args:', args)
      }

      const cb = args.pop().replace(/\s+/, '')
      if (!write) {
        console.log('last arg', cb, '\n')
      }

      let isAsync = true
      if (cb !== 'callback') {
        // synchronous function, format will be {} instead of array
        args.push(cb) // put it back
        isAsync = false
      }

      const module = m[1]
      const method = m[2]

      const input_ports = {}
      for (const item of args) {
        const type = defaults[item] ? defaults[item] : 'string'
        input_ports[item] = {type}
      }

      const description =
        _s.humanize(module) + ' ' + _s.humanize(method).toLowerCase()

      const json = {
        name: module + '.' + method,
        description,
        ports: {
          input: input_ports,
          output: {},
        },
      }

      json.ports.output[output_port] = {type: output_type}

      const argsString = 'input.' + args.join(', input.')

      nodes[method] = {json}
      if (isAsync) {
        nodes[method].fn =
          'output([' + module + '.' + method + ', ' + argsString + '])'
      } else {
        // output({ results: path.normalize(input.path) })
        nodes[method].fn =
          'output({ ' +
          output_port +
          ': ' +
          module +
          '.' +
          method +
          '(' +
          argsString +
          ') })'
      }
    }
  }

  if (write) {
    // tslint:disable-next-line:forin
    for (const method in nodes) {
      const node = nodes[method]

      if (!fs.existsSync(dirname + '/' + method)) {
        fs.mkdirSync(dirname + '/' + method)
      }

      // create node.js
      let f = dirname + '/' + method + '/node.js'
      fs.writeFileSync(f, node.fn)
      console.log('Written:', f)

      // create node.json
      f = dirname + '/' + method + '/node.json'
      const json = JSON.stringify(node.json, null, 2)
      fs.writeFileSync(f, json)
      console.log('Written:', f)
    }
  } else {
    console.log('nodes', nodes)
  }
})
