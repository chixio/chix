import {NodeDefinition, FlowSchema} from '@chix/common'
import {compile} from '@chix/flow'
import * as camelize from 'camelize'
import * as fs from 'fs'
import {globSync} from 'glob'
import {js_beautify as beautify} from 'js-beautify'
import * as jsonGate from 'json-gate'
import * as mockBrowser from 'mock-browser'
import * as path from 'path'
import toSource from 'tosource'
import {getPackageName, getParser} from './util'

const mapSchema = jsonGate.createSchema(FlowSchema)

const pkgName = getPackageName()

function replaceRequire(str: string, local?: boolean) {
  const what = !local ? "require('$1')" : "require('./$1')"

  return str.replace(/"##([\w-_]+)##"/gm, what)
}

// try to trick some requires
const MockBrowser = mockBrowser.mocks.MockBrowser

const mock: any = new MockBrowser()(global as any)
mock.document =
  mock.getDocument()(global as any).window =
  MockBrowser.createWindow()(global as any).location =
  mock.getLocation()(global as any).navigator =
  mock.getNavigator()(global as any).history =
  mock.getHistory()(global as any).localStorage =
  mock.getLocalStorage()(global as any).sessionStorage =
  mock.getSessionStorage()(global as any).Element =
  (window as any).Element(global as any).Mottle =
    {
      consume: {},
    }

const files = globSync('./nodes/**/node.json')

const nodeDefinitions: NodeDefinition[] = []

files.forEach((file) => {
  const nodeDefinition = JSON.parse(fs.readFileSync(file, 'utf8'))

  // do real requires
  if (nodeDefinition.dependencies && nodeDefinition.dependencies.npm) {
    Object.keys(nodeDefinition.dependencies.npm).forEach((key) => {
      if (key !== 'builtin') {
        nodeDefinition.dependencies.npm[key] = '##' + key + '##'
      }
    })
  }

  nodeDefinitions.push(nodeDefinition)

  fs.readFile(
    file.replace('.json', '.js'),
    'utf8',
    // tslint:disable-next-line:no-shadowed-variable
    (error: null | Error, contents) => {
      if (error) {
        throw error
      }

      nodeDefinition.fn = contents

      const src = compile(nodeDefinition)

      let res = 'module.exports = ' + toSource(src)

      // unquote the requires
      res = replaceRequire(res)

      const fileName = [
        process.cwd(),
        '/',
        path.dirname(file).split('/').pop(),
        '.js',
      ].join('')

      fs.writeFile(fileName, beautify(res, {indent_size: 2}), () => {
        console.log('wrote ' + fileName)
      })
    }
  )
})

const twigFiles = globSync('./twigs/*.fbp')

// nodes within this twig are allowed to incorporate other
// namespaces, but his twig belongs to this namespace
for (const item of twigFiles) {
  const parser = getParser()
  console.log('processing %s', item)
  const twig = parser.parse(fs.readFileSync(item).toString())
  const name = path.basename(item, '.fbp')
  twig.ns = pkgName
  twig.name = name

  mapSchema.validate(twig)

  const fileName = name + '.js'
  const jsBody = ['module.exports = ', JSON.stringify(twig, null, 2)].join('')
  fs.writeFileSync(fileName, jsBody)
  console.log('wrote %s', fileName)
  nodeDefinitions.push(twig)
}

// note: ns: 'my/sub', should translate to sub/subdir
const idx: {[name: string]: string} = {}

// write index
nodeDefinitions.forEach((nodeDefinition) => {
  // const parts = nodeDefinition.ns.split('/')
  // const ns = parts[0]
  // if (parts.length > 1) {
  //   recursive also, later.
  // }
  idx[nodeDefinition.name] = `##${nodeDefinition.name}##`
})

// note: requiring code must expect camelized properties..
if (Object.keys(idx).length) {
  const res = camelize(idx)
  const src = [
    'module.exports = ',
    replaceRequire(JSON.stringify(res, null, 2), true).replace(/"/g, ''),
  ].join('')

  fs.writeFile('index.js', src, () => {
    console.log('wrote index.js')
  })
}
