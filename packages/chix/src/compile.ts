import * as fs from 'fs'
import * as path from 'path'
import {program} from 'commander'
import {globSync} from 'glob'
import browserify from 'browserify'
import {NodeSchema, FlowSchema} from '@chix/common'
import * as jsonGate from 'json-gate'
import {getParser, getPackageJson} from './util'

const nodeSchema = jsonGate.createSchema(NodeSchema)
const mapSchema = jsonGate.createSchema(FlowSchema)
const b = browserify()
const dirname = process.cwd()

let globPattern = '/nodes/**/node.js*'

program
  .usage('[options]')
  .option('-g, --glob', 'globbing pattern default: ' + globPattern)
  .option('-d, --dry', 'dry run')
  .parse(process.argv)

const options = program.opts()

const write = !options.dry

globPattern = options.glob || globPattern

let c
;(async () => {
  const pkg = await getPackageJson()

  const x: any = {
    name: pkg.chix.name,
    description: pkg.description,
    version: pkg.version,
    licences: pkg.licenses,
    repository: pkg.repository,
    dependencies: pkg.dependencies,
    nodes: [],
  }

  if (pkg.chix.env) {
    x.env = pkg.chix.env
  }

  b.bundle((error, _src) => {
    if (error) {
      throw error
    }
    const fns = {}
    let files: string[]
    files = globSync(dirname + globPattern)
    let parts
    let module
    let ext
    for (const item of files) {
      parts = item.split('/')
      module = parts[1]
      ext = path.extname(item)

      c = fs.readFileSync(item).toString()

      let node: any = {}
      if (ext === '.js') {
        fns[module] = c // warning: relies on .js before .json
      } else if (ext === '.json') {
        console.log(item)
        const json = JSON.parse(c)

        if (!json.name) {
          throw Error('name is missing in node.json file' + module)
        }

        node = {...node, ...json}

        // If the package specifies an environment
        // also add this to each node definition within this package
        // yet leave an option to override it.
        if (x.env && !node.env) {
          node.env = x.env
        }

        node.ns = node.ns ? node.ns : x.name // add namespace if not specified by the node itself
        node.fn = fns[module]

        nodeSchema.validate(node)

        x.nodes.push(node)
      }
    }

    // add twigs
    // concept of twigs was different, but unusable anyway.
    // tslint:disable-next-line:no-shadowed-variable
    files = globSync(dirname + '/twigs/*.fbp')
    // nodes within this twig are allowed to incorporate other
    // namespaces, but his twig belongs to this namespace
    for (const item of files) {
      const parser = getParser()
      console.log('processing %s', item)
      const twig = parser.parse(fs.readFileSync(item).toString())
      const name = path.basename(item, '.fbp')
      twig.ns = x.name
      twig.name = name

      mapSchema.validate(twig)

      x.nodes.push(twig)
    }

    const contents = JSON.stringify(x, null, 2)

    if (write) {
      console.log('writing x.json')
      fs.writeFileSync(dirname + '/x.json', contents)
    } else {
      console.log(contents)
    }
  })
})()
