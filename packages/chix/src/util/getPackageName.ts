import {getPackageJson} from './getPackageJson'

export async function getPackageName() {
  const pkg = await getPackageJson()

  return pkg.chix.name
}
