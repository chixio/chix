import {ChixRenderer, FBPParser} from '@chix/fbpx'

const useUUID = false

export function getParser() {
  const parser = new FBPParser()
  const renderer = new ChixRenderer()

  renderer.skipIDs = !useUUID
  parser.addRenderer(renderer)

  return parser
}
