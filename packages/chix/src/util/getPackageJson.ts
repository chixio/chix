import * as fs from 'fs-extra'
import * as path from 'path'

export type ChixPackage = {
  description: string
  version: string
  licenses: string[]
  repository: string
  dependencies: {[key: string]: string}
  nodes: string[]
  chix: {
    env: string
    name: string
  }
}

export async function getPackageJson() {
  const pkg: ChixPackage = JSON.parse(
    await fs.readFile(path.join(process.cwd(), './package.json'), 'utf-8')
  )

  if (!pkg.hasOwnProperty('chix')) {
    throw Error('package.json does not contain a chix section')
  }

  if (!pkg.chix.hasOwnProperty('name')) {
    throw Error('Cannot determine chix package name')
  }

  return pkg as unknown as ChixPackage
}
