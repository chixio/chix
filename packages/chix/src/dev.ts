import {program} from 'commander'
import * as fs from 'fs'
import {exec} from 'child_process'
import {getPackageJson} from './util'

const cwd = process.cwd()

;(async () => {
  const pkg = await getPackageJson()

  let type = 'patch'

  const getVersion = (version: string) => {
    type = version === 'major' || version === 'minor' ? v : type
  }

  program
    .usage('[options] <file>')
    .option('-b, --bump', 'major|minor|patch', getVersion)
    .parse(process.argv)

  const opts = program.opts()

  const v = opts.version.match(/(\d)+/g)

  // major,minor,patch
  const new_version = {
    major: [1 + parseInt(v[0], 10), 0, 0],
    minor: [v[0], 1 + parseInt(v[1], 10), 0],
    patch: [v[0], v[1], 1 + parseInt(v[2], 10)],
  }

  pkg.version = new_version[type].join('.')

  fs.writeFileSync(cwd + '/package.json', JSON.stringify(pkg, null, '  '))

  const cmds = []
  if (pkg.version) {
    const versionString = 'v' + pkg.version
    cmds.push('git checkout master')
    cmds.push('chix-compile')
    cmds.push('git add package.json')
    cmds.push('git add x.json')
    cmds.push('git commit -m "update version to ' + versionString + '"')
    cmds.push(
      'git tag -a ' + versionString + ' -m "new release: ' + versionString + '"'
    )
    cmds.push('git push origin ' + versionString)
    cmds.push('git push origin master')
  }

  const run = cmds.join(';')

  exec(run, (error, stdout, stderr) => {
    console.log('stdout: ' + stdout)
    console.log('stderr: ' + stderr)
    if (error !== null) {
      console.log('exec error: ' + error)
    }
  })
})()
