import * as events from 'events'

export interface Args {
  [key: string]: any
}

export type Returns = string[]

/**
 *
 * IO Box
 *
 * @param {String} name
 */
export class IOBox extends events.EventEmitter {
  // to ensure correct order
  public _keys: string[] = []
  public name = 'UNNAMED'
  public args: Args = {}
  public returns: Returns = []
  public fn: any
  public code: string | null = null;
  [x: string]: any

  constructor(name?: string, args?: Args, returns?: Returns) {
    super()

    if (name) {
      this.name = name
    }

    this.setup(args, returns)
  }

  public create(name?: string, args?: Args, returns?: Returns) {
    return new IOBox(name, args, returns)
  }

  /**
   *
   * Setup
   *
   * @param {Array} args
   * @param {Array} returns
   */
  public setup(args: Args = [], returns: Returns = []) {
    let i: number
    // set up the empty input arguments object
    for (i = 0; i < args.length; i++) {
      this.addArg(args[i], undefined)
    }

    for (i = 0; i < returns.length; i++) {
      this.addReturn(returns[i])
    }
  }

  /**
   *
   * Used to access the properties at the top level,
   * but still be able to get all relevant arguments
   * at once using this.args
   *
   * @param {String} key
   * @param {any} initial
   */
  public addArg(key: string, initial: any) {
    Object.defineProperty(this, key, {
      set(this: IOBox, val) {
        this.args[key] = val
      },
      get(this: IOBox) {
        return this.args[key]
      },
    })

    this._keys.push(key)

    Object.assign(this, {[key]: initial}) // can be undefined
  }

  public addReturn(r: string) {
    if (this.returns.indexOf(r) === -1) {
      if (this._keys.indexOf(r) !== -1) {
        this.returns.push(r)
      } else {
        throw Error(
          ['Output `', r, '` is not one of', this._keys.join(', ')].join(' ')
        )
      }
    }
  }

  /**
   *
   * Sets a property of the sandbox.
   * Because the keys determine what arguments will
   * be generated for the function, it is important
   * we keep some kind of control over what is set.
   *
   * @param {String} key
   * @param {any} value
   *
   */
  public set(key: string, value: any) {
    if (this.args.hasOwnProperty(key)) {
      this.args[key] = value
    } else {
      throw new Error(['Will not set unknown property', key].join(' '))
    }
  }

  /**
   *
   * Compiles and returns the generated function.
   *
   * @param {String} func
   * @param {Boolean} asObject
   * @return {String}
   */
  public compile(func?: string, asObject?: boolean): any {
    if (!this.code) {
      if (typeof func !== 'string') {
        throw Error('Function parameter expected')
      }
      this.generate(func ? func.trim() : func, asObject)
    }

    // tslint:disable-next-line:function-constructor
    this.fn = new Function(this.code as string)()

    return this.fn
  }

  /**
   *
   * Fill with a precompiled function
   *
   * Return type in this case is determined by compiled function
   *
   * @param {any} fn
   * @return {any}
   */
  public fill(fn: any): any {
    // argument signature check?

    this.fn = fn

    return this.fn
  }

  /**
   *
   * Wraps the function in yet another function
   *
   * This way it's possible to get the original return.
   *
   * @param {String} fn
   * @return {String}
   */
  public _returnWrap(fn: string): string {
    return ['function() {', fn, '}.call(this)'].join('\n')
  }

  /**
   *
   * Clear generated code
   */
  public clear() {
    this.code = null
  }

  /**
   *
   * Generates the function.
   *
   * This can be used directly
   *
   * @param {String} fn
   * @param {Boolean} asObject
   * @return {String}
   */
  public generate(fn: string, asObject: boolean = false) {
    this.code = [
      'return function ',
      this.name,
      '(',
      this._keys.join(','),
      ') {\n',
      'var r = ', // r goes to 'return'
      this._returnWrap(fn),
      '; return ',
      asObject ? this._asObject() : this._asArray(),
      '; }',
    ].join('')

    return this.code
  }

  /**
   *
   * Return output as array.
   *
   * @return {String}
   */
  public _asArray(): string {
    return `[${this.returns.join(',')},r]`
  }

  /**
   *
   * Return output as object.
   *
   * @return {String}
   */
  public _asObject(): string {
    const _return: string[] = []

    for (const prop of this.returns) {
      _return.push(`${prop}:${prop}`)
    }

    _return.push('return:' + 'r')

    return `{${_return.join(',')}}`
  }

  /**
   *
   * Renders the function to string.
   *
   * @return {String}
   */
  public toString(): string {
    if (this.fn) {
      return this.fn.toString()
    }

    return ''
  }

  /**
   *
   * Runs the generated function
   *
   * @param {any} bind   Context to bind to the function
   * @return {any}
   */
  public run(bind?: any): any {
    const v: any[] = []

    for (const k in this.args) {
      if (this.args.hasOwnProperty(k)) {
        v[this._keys.indexOf(k)] = this.args[k]
      } else {
        throw new Error('unknown input ' + k)
      }
    }

    if (!this.fn) {
      throw Error(`${this.name}(${this._keys}): fn body is not set.`)
    }

    // returns the output, format depends on the `compile` step
    return this.fn.apply(bind, v)
  }
}
