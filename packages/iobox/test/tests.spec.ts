import 'should'

/* global it, describe, before */

import * as events from 'events'
import {IOBox} from '@chix/iobox'

describe('IOBox', () => {
  let io: IOBox

  before(() => {
    io = new IOBox('my_box')
  })

  it('Should be able to instantiate', () => {
    io.should.be.an.instanceOf(IOBox)
    io.should.be.an.instanceOf(events.EventEmitter)
  })

  it('Should have the function name set', () => {
    io.name.should.eql('my_box')
  })

  describe('When setup', () => {
    it('Should throw if return is not one of input', () => {
      ;(() => {
        io.setup(['input', 'output'], ['noway'])
      }).should.throw(/is not one of input/)
    })

    it('Should know inputs', () => {
      io = new IOBox('my_box') // reset state
      io.setup(['input', 'output'], ['output'])
      io.args.should.eql({
        input: undefined,
        output: undefined,
      })
    })

    it('Should know returns', () => {
      io.returns.should.eql(['output'])
    })
  })

  describe('When generated', () => {
    it('should return a function string', () => {
      io.generate('output = input;').should.eql(
        [
          'return function my_box(input,output) {',
          'var r = function() {',
          'output = input;',
          '}.call(this); return [output,r]; }',
        ].join('\n')
      )
    })

    it('if an arg is added, should reflect the changes', () => {
      io.addArg('peach', true)
      io.generate('output = input;').should.eql(
        [
          'return function my_box(input,output,peach) {',
          'var r = function() {',
          'output = input;',
          '}.call(this); return [output,r]; }',
        ].join('\n')
      )
    })

    it('if a return is added, should reflect the changes', () => {
      io.addReturn('peach')

      const ret = io.generate('output = input;')

      ret.should.eql(
        [
          'return function my_box(input,output,peach) {',
          'var r = function() {',
          'output = input;',
          '}.call(this); return [output,peach,r]; }',
        ].join('\n')
      )

      ret.should.eql(io.code)
    })

    it('Should return an object, if is isObject is true', () => {
      io.generate('output = input;', true).should.eql(
        [
          'return function my_box(input,output,peach) {',
          'var r = function() {',
          'output = input;',
          '}.call(this); return {output:output,peach:peach,return:r}; }',
        ].join('\n')
      )
    })

    it('Should be able to fill argument', () => {
      ;(io.args.input === undefined).should.eql(true)
      io.set('input', 'Hey')
      io.args.input.should.eql('Hey')
    })

    it('Should not be able to fill a non-existent argument', () => {
      ;(() => {
        io.set('nope', 'Hey')
      }).should.throw(/will not set/i)
    })

    it('Input arguments should be available as methods', () => {
      io.input.should.be.an.instanceOf(String)
      io.input.should.eql('Hey')
      io.peach.should.eql(true)
    })
  })

  describe('Compile', () => {
    it('Should return a function instance', () => {
      const fn = io.compile()
      fn.should.be.an.instanceOf(Function)
    })
  })

  describe('Run', () => {
    it('Should be able to run the function', () => {
      const out = io.run()
      io.input.should.eql('Hey')
      io.peach.should.eql(true)
      // out come specific to this particular tested function body
      out.should.have.property('output')
      out.should.have.property('peach')
      out.should.have.property('return')
      out.output.should.eql(io.input)
      out.peach.should.eql(io.peach)
    })
    /*
    // not implemented
    it('Should be able to clear input', function() {

    });
*/
  })
})
