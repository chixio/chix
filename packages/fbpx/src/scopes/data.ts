import {Scope} from 'dyslexer'
import {replaceSingleQuotes} from './util'

/**
 *
 * This is the data scope it is entered
 * when a quoted string is encountered.
 *
 * It will quit the scope when we reach
 * the same quote type (' or "), unless
 * that quote was escaped.
 *
 */
export class DataScope extends Scope {
  public onEnter(): void {
    // We need to hint what is our tokenEnding
    // also causes space to no longer be the token ending.
    this.tokenEnding = [this.lexer.scoper]

    // whether the lexer should check for
    // an escape \ one char back.
    this.escape = true
  }

  public onLineStart(): void {
    this.tokensExpected = 1
  }

  public onToken(token: string): void {
    if (this.lexer.scoper === "'") {
      token = JSON.parse(replaceSingleQuotes(token))
    } else if (this.lexer.scoper === '"') {
      token = JSON.parse(token)
    }

    this.lexer.present('DATA', token)
  }
}
