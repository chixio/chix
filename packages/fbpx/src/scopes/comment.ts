import {Scope} from 'dyslexer'

export class CommentScope extends Scope {
  public tokenEnding = ['\n']

  public structure = [['COMMENT']]

  public validate = {
    COMMENT: /.*/,
  }

  public onLineStart(): void {
    // setting this is important
    // mandatory actually.
    this.tokensExpected = 1
  }

  public onToken(token: string): void {
    this.lexer.present('COMMENT', token)
  }
}
