import {Scope} from 'dyslexer'

/**
 *
 * This is the JSON scope it is entered
 * when JSON is encountered.
 *
 * It will quit the scope when we reach
 * either } or ] depending on what started the scope
 *
 * An attempt has been made to support nesting also.
 *
 */
export class JSONScope extends Scope {
  public structure = [['DATA']]

  private nesting = 0

  public onEnter(): void {
    // As far as root is concerned
    // we have no token ending
    this.tokenEnding = []

    // custom for this scope
    this.nesting = 0

    // whether the lexer should check for
    // an escape \ one char back.
    // Ok the only problem will arise if there is []
    // or {}  inside data strings. but whatever
    // This is not meant to be that advanced.
    // this.escape = true;
  }

  public setup(): void {
    this.rules = {
      '{': () => {
        if (this.lexer.scoper === '{') {
          this.nesting++
        }
      },

      '[': () => {
        if (this.lexer.scoper === '[') {
          this.nesting++
        }
      },

      '}': () => {
        if (this.lexer.scoper === '{') {
          if (this.nesting) {
            this.nesting--
          } else {
            this.tokenEnding = ['}']
          }
        }
      },

      ']': () => {
        if (this.lexer.scoper === '[') {
          if (this.nesting) {
            this.nesting--
          } else {
            this.tokenEnding = [']']
          }
        }
      },
    }
  }

  public onLineStart(): void {
    this.tokensExpected = 1
  }

  public onToken(token: string): void {
    this.lexer.present('DATA', JSON.parse(token))
  }
}
