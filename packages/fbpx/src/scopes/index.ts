import {Scope} from 'dyslexer'

export class IndexScope extends Scope {
  public structure = [['TARGET_INDEX']]

  public validate = {}

  public onEnter(): void {}

  public onLineStart(): void {
    this.tokensExpected = 1
  }

  public onToken() {
    this.lexer.present('TARGET_INDEX')
  }
}
