import {Scope} from 'dyslexer'

/**
 *
 * This is the JSON scope it is entered
 * when JSON is encountered.
 *
 * It will quit the scope when we reach
 * either } or ] depending on what started the scope
 *
 * An attempt has been made to support nesting also.
 *
 */

// All we need is simply neh, don't need this at all.
// just need to jump back.
export class ActionScope extends Scope {
  public structure = [['ACTION_START'], ['ACTION_END']]

  public onLineStart(): void {
    this.tokensExpected = 1
  }

  public onToken(token: string): void {
    // ok, this is skipped by LeftHandScope.
    // bit magical ah well.
    if (token === '{') {
      this.lexer.present('ACTION_START', token)
      this.lexer.toScope('LeftHandScope')
    }
  }
}
