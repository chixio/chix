import {Scope} from 'dyslexer'

/**
 *
 * This is the Mask scope it is entered
 * when a mask is encountered.
 *
 * This scope is entered after -( was detected in leftHandScope
 *
 * It will quit the scope when we reach )>
 */
export class MaskScope extends Scope {
  public structure = [['MASK']]

  public onEnter(): void {
    // No token ending until > is encountered
    this.tokenEnding = []
  }

  public setup(): void {
    this.rules = {
      '>': () => {
        this.tokenEnding = [' ']
      },
    }
  }

  public onLineStart(): void {
    this.tokensExpected = 1
  }

  public onToken(token: string): void {
    const match = token.match(/\(([\s\S]*)\)/m)

    if (match[1] === undefined) {
      throw Error('Unable to detect mask')
    } else {
      token = match[1]
    }

    let value

    try {
      value = JSON.parse(token)
    } catch (e) {
      value = token
    }

    this.lexer.present('MASK', value)

    // is right hand correct?
    this.lexer.toScope('RightHandScope')
  }
}
