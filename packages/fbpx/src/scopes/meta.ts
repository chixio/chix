import {Scope} from 'dyslexer'

/**
 * This is the data scope it is entered
 * when a quoted string is encountered.
 *
 * It will quit the scope when we reach
 * the same quote type (' or "), unless
 * that quote was escaped.
 */
export class MetaScope extends Scope {
  public structure = [['META_DATA']]

  public validate = {
    META: /^\w+:/,
    META_DATA: /.*/,
  }

  public onEnter(): void {}

  public onLineStart(): void {
    this.tokensExpected = 1
    this.tokenEnding = ['\n']
  }

  public onToken(token: string): void {
    if (this.tokens.length === 0) {
      // todo: the trailing \n should not be in the value, but it still is.
      // the space is ok.
      this.lexer.present('META_DATA', token.trim())
    }
  }
}
