import {Scope} from 'dyslexer'

export class RightHandScope extends Scope {
  public structure = [
    ['IN_PORT', 'PROCESS'],
    ['TARGET_INDEX', 'IN_PORT', 'PROCESS'],
    ['IN_PORT', 'PROCESS', 'OUT_PORT'],
    ['IN_PORT', 'PROCESS', 'OUT_PORT', 'SOURCE_INDEX'],
    ['TARGET_INDEX', 'IN_PORT', 'PROCESS', 'OUT_PORT'],
    ['TARGET_INDEX', 'IN_PORT', 'PROCESS', 'OUT_PORT', 'SOURCE_INDEX'],
  ]

  public validate = {
    SOURCE_INDEX: /^\[[A-z0-9]+\]$/,
    TARGET_INDEX: /^\[[A-z0-9]+\]$/,
    IN_PORT: /[A-z0-9_:]+/,
    PROCESS: /^[A-z0-9_:]+(\([A-z0-9:\.\-\/]*\))?$/,
  }

  private count = 0

  public onEnter(): void {
    // do our own count
    this.count = 0
  }

  public onLineStart(): void {
    // this.tokensExpected = 3;
    this.tokensExpected = 5
  }

  public setup(): void {
    this.rules = {
      // our lexers
      '#': (char) => {
        this.lexer.toScope('CommentScope', char)
      },

      '(': (char) => {
        if (this.lexer.token === '-') {
          this.lexer.toScope('MaskScope', char)
        }
      },
    }
  }

  public onToken(token: string): void {
    if (this.validate.TARGET_INDEX.test(token)) {
      if (this.count === 0) {
        // always the first
        this.lexer.present('TARGET_INDEX', token)
      } else {
        this.lexer.present('SOURCE_INDEX', token) // always last
        this.lexer.toScope('RootScope')
      }
    } else if (this.count === 0) {
      // must be the in port
      this.lexer.present('IN_PORT', token)
      this.count++
    } else if (this.count === 1) {
      // Must be the process
      this.lexer.present('PROCESS', token)
      this.count++
    } else if (this.count === 2) {
      this.lexer.present('OUT_PORT', token)
      this.count++
    }
  }
}
