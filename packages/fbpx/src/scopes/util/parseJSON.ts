export function parseJSON(token: string) {
  let json
  try {
    json = JSON.parse(token)
  } catch (error) {
    const match = error.toString().match(/\d+/)

    if (match) {
      const pos = parseInt(match[0], 10)
      const leftPos = pos > 10 ? pos - 10 : 0
      const length = pos + 100

      console.log(token.slice(leftPos, length).replace(/\n/g, ''))
      console.log(new Array(9).join('-') + '^')
    }

    throw error
  }

  return json
}
