export function replaceSingleQuotes(token: string): string {
  return token[0] === "'" && token[token.length - 1] === "'"
    ? JSON.stringify(`${token.slice(1, token.length)}`)
    : token
}
