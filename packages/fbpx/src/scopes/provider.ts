import {Scope} from 'dyslexer'

/**
 *
 * This is the data scope it is entered
 * when a quoted string is encountered.
 *
 * It will quit the scope when we reach
 * the same quote type (' or "), unless
 * that quote was escaped.
 *
 */
export class ProviderScope extends Scope {
  public structure = [
    ['PROVIDER', 'PROVIDER_URL'],
    ['PROVIDER', 'PROVIDER_URL', 'AS', 'PROVIDER_NS'],
  ]

  public validate = {
    PROVIDER_NS: /^[A-z_]+$/,
    AS: /^as$/i,
    PROVIDER_URL: /[A-z0-9:\-\/{}\.#]+/,
  }

  public onEnter(): void {}

  public onLineStart(): void {
    /* 4 is the max */
    this.tokensExpected = 4
  }

  public onToken(token: string): void {
    if (this.tokens.length === 0) {
      this.lexer.present('PROVIDER', token)
    } else if (this.tokens.length === 1) {
      this.lexer.present('PROVIDER_URL', token)
    } else if (this.tokens.length === 2) {
      this.lexer.present('AS', token)
    } else if (this.tokens.length === 3) {
      this.lexer.present('PROVIDER_NS', token)
    }
  }
}
