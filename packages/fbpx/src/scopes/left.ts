import {Scope} from 'dyslexer'

export class LeftHandScope extends Scope {
  // aliases, will run the same function
  public alias = {
    "'": '"',
  }

  // Used to validate the tokens.
  public validate = {
    SOURCE_INDEX: /^\[.+\]$/,
    OUT_PORT: /^[A-z0-9_:\*]+$/,
    EXPORT: /^EXPORT=[A-z]+/,
    META: /^[A-z_]+:/,
    ACTION: /^::[A-z0-9_]+$/,
    PROCESS: /^[A-z0-9_:]+(\([A-z0-9:\.\-\/]*\))?$/,
    // DATA: /.*/
    // PROVIDER: /:[A-z0-9:\-\/{}\.#]+$/
    PROVIDER: /^provider/,
  }

  public structure = [
    ['JSON_START'],
    ['ACTION'],
    ['FUNCTION_START'],
    ['EXPORT_IN'],
    ['EXPORT_OUT'],
    ['DATA_INCLUDE'],
    ['DATA'],
    ['META'],
    ['EXPORT'],
    ['PROCESS', 'OUT_PORT'],
    ['PROCESS', 'OUT_PORT', 'SOURCE_INDEX'],
    // allow process only, defines a node without connecting anything yet
    ['PROCESS'],
  ]

  public onLineStart(): void {
    this.tokensExpected = 3
  }

  public setup(): void {
    // Used by the lexer to validate token combinations.
    // COMMENT is also possible, but it's not brought back from comment scope..

    this.rules = {
      '#': () => {
        this.lexer.toScope('CommentScope')
      },

      '/': () => {
        if (!this.lexer.tokens.length && this.lexer.token === '') {
          this.lexer.toScope('FBPDocScope')
        }
      },

      '[': (char: string) => {
        // only if we are at the beginning
        // and we are the first char
        if (!this.lexer.tokens.length && this.lexer.token === '') {
          this.lexer.toScope('JSONScope', char)
        }
      },

      '{': (char: string) => {
        // only if we are at the beginning
        // and we are the first char
        if (!this.lexer.tokens.length && this.lexer.token === '') {
          this.lexer.toScope('JSONScope', char)
        }
      },

      '(': (char: string) => {
        // only if we are at the beginning
        // and we are the first char
        if (!this.lexer.tokens.length && this.lexer.token === '') {
          this.lexer.toScope('FunctionScope', char)
        } else if (this.lexer.token === '-') {
          this.lexer.toScope('MaskScope', char)
        }
      },

      // quotes means data, if so we only expect one token
      '"': (char: string) => {
        // We only expect one token now.
        this.tokensExpected = 1
        this.lexer.toScope('DataScope', char)
      },

      // FIXME: ALIAS seems broke? ' is not detected?, below should not be necessary
      "'": (char: string) => {
        // We only expect one token now.
        this.tokensExpected = 1
        // this.lexer.toScope('DataScope', '"')
        this.lexer.toScope('DataScope', char)
      },
    }
  }

  public onToken(token: string): void {
    // TODO: I don't think the scope switching should be the job of this scope..
    if (token === '}') {
      // JSON type } is handled during JSONScope

      // jikes.. :-)
      this.lexer.toScope('ActionScope')
      this.lexer.present('ACTION_END', token)
      this.lexer.toScope('LeftHandScope')
    } else if (this.tokens.length === 1) {
      // must be the out port
      this.lexer.present('OUT_PORT', token)
      // Removed this! I think it keeps working
      // because with every -> root automatically
      // takes the scope.
      // this.lexer.toScope('RootScope');
    } else if (this.tokensExpected === 1 && this.tokens.length === 0) {
      // throw new Error('Error: not used.')
      this.lexer.present('DATA', token)
      this.lexer.toScope('RootScope')
    } else if (this.tokens.length === 0) {
      if (/^->/.test(token)) {
        this.lexer.present('EXPORT_IN', token)
        this.lexer.toScope('RightHandScope')
      } else if (/^<-/.test(token)) {
        this.lexer.present('EXPORT_OUT', token)
        this.lexer.toScope('RightHandScope')
      } else if (/^EXPORT=/.test(token)) {
        this.lexer.present('EXPORT', token)
        /* Already done by scanner
        } else if (/^[{\[]/.test(token)) {
          this.lexer.present('JSON_START', token);
          this.lexer.toScope('JSONScope', token); // scoper
        */
      } else if (/^\^/.test(token)) {
        this.lexer.present('DATA_INCLUDE', true)
        this.lexer.toScope('RootScope')
      } else if (/^true$/.test(token)) {
        this.lexer.present('DATA', true)
        this.lexer.toScope('RootScope')
      } else if (/^false$/.test(token)) {
        this.lexer.present('DATA', false)
        this.lexer.toScope('RootScope')
        // numeric data
      } else if (/^[0-9\-]*\.[0-9]+$/.test(token)) {
        // todo: check if we receive "'some_string'"
        // it should be with quotes, so we can detect a string.
        this.lexer.present('DATA', parseFloat(token))
        this.lexer.toScope('RootScope')
      } else if (/^[0-9\-]+$/.test(token)) {
        // todo: check if we receive "'some_string'"
        // it should be with quotes, so we can detect a string.
        this.lexer.present('DATA', parseInt(token, 10))
        this.lexer.toScope('RootScope')
      } else if (/^\w+::/.test(token)) {
        // before meta data.
        this.lexer.present('PROCESS', token)
      } else if (/^\w+:/.test(token)) {
        this.lexer.present('META', token)
        this.lexer.toScope('MetaScope')
      } else if (/^provider/.test(token)) {
        this.lexer.toScope('ProviderScope')
        this.lexer.present('PROVIDER', token)
      } else if (this.validate.ACTION.test(token)) {
        this.lexer.present('ACTION', token)
        this.lexer.toScope('ActionScope')
      } else {
        this.lexer.present('PROCESS', token)
      }
    } else if (this.validate.SOURCE_INDEX.test(token)) {
      this.lexer.present('SOURCE_INDEX', token)
    }
  }
}
