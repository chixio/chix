import {Scope} from 'dyslexer'

/**
 *
 * This is the Function scope it is entered
 * when a () block is encountered.
 *
 * It will quit the scope when we reach a closing )
 *
 */
export class FunctionScope extends Scope {
  public structure = [['DATA']]

  private nesting = 0

  public onEnter(): void {
    // As far as root is concerned
    // we have no token ending
    this.tokenEnding = []

    // custom for this scope
    this.nesting = 0
  }

  // TODO nesting will probably break in some weird
  // escaped situations. or like adding comments
  // like /* hi ( */
  public setup(): void {
    this.rules = {
      '(': () => {
        this.nesting++
      },

      ')': () => {
        if (this.nesting) {
          this.nesting--
        } else {
          this.tokenEnding = [')']
        }
      },
    }
  }

  public onLineStart(): void {
    this.tokensExpected = 1
  }

  public onToken(token: string): void {
    token = token.slice(1, token.length - 1).trim()

    // render can take care of making it a real function or not.
    this.lexer.present('DATA', token)
  }
}
