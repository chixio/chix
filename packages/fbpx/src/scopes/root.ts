import {Scope} from 'dyslexer'

export class RootScope extends Scope {
  public validate = {
    ARROW: /->|→/,
    PAIRED_ARROW: /=>|⇉/,
    INCLUDE_ARROW: /~>/,
    COLLECT_ARROW: />=/,
  }

  public onLineStart(): void {
    // Note: forces left hand scope on new line
    this.tokensExpected = 1
    this.lexer.toScope('LeftHandScope')
  }

  public onToken(token: string): boolean | void {
    if (this.validate.ARROW.test(token)) {
      // first on line means EXPORT_IN
      if (this.lexer.tokens.length === 0) {
        this.lexer.present('EXPORT_IN', token)
      } else {
        this.lexer.present('ARROW', token)
      }

      this.lexer.toScope('RightHandScope')

      return true
    } else if (this.validate.PAIRED_ARROW.test(token)) {
      this.lexer.present('PAIRED_ARROW', token)
      this.lexer.toScope('RightHandScope')
      return true
    } else if (this.validate.COLLECT_ARROW.test(token)) {
      this.lexer.present('COLLECT_ARROW', token)
      this.lexer.toScope('RightHandScope')
      return true
    } else if (this.validate.INCLUDE_ARROW.test(token)) {
      this.lexer.present('INCLUDE_ARROW', token)
      this.lexer.toScope('RightHandScope')
      return true
    } else {
      // enabled running on each token again.
      // throw new Error('Unhandled token: ' + token);
    }
  }
}
