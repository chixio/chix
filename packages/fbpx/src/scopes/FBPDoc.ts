import {Scope} from 'dyslexer'

export class FBPDocScope extends Scope {
  public tokenEnding = []

  // Used to validate the tokens.
  public validate = {
    FBPDOC: /\/\*([\S\s]*?)\*\//gm,
  }

  public structure = [['FBPDOC']]

  // escape char could be *anything* but *
  // so maybe make the escape char a regexp

  public onLineStart(): void {
    // setting this is important
    // mandatory actually.
    this.tokensExpected = 1
  }

  public setup(): void {
    this.rules = {
      '/': () => {
        const prevChar = this.lexer.chars[this.lexer.current - 1]

        if (prevChar === '*') {
          this.tokenEnding = ['/']
        }
      },
    }
  }

  public onToken(token: string): void {
    this.lexer.present('FBPDOC', token)
  }
}
