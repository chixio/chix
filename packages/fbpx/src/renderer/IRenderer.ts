import {IIP, Provider} from '@chix/common'
import {ComponentDefinition, FBPComponent, FBPExport, FBPLink} from '../types'

export interface IRenderer {
  addContext(process: string, inPort: string, context: any): void
  addExport(c: FBPExport, ioType: string): void
  addLink(link: FBPLink): void
  addMeta(name: string, value: any): void
  addNode(componentDefinition: ComponentDefinition): void
  addProvider(provider: Provider): void
  fbpdoc(params: any): void
  getFlow(): any
  parseComponentString(component: string, node: FBPComponent): FBPComponent
  reset(): void
  touchNode(processName: string): void
  toJSON(): string
  getIIPs(action?: string): IIP[]
  // include(data: any): void
}
