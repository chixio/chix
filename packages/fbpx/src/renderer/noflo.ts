import {IIP} from '@chix/common'

import {Connection, FBPComponent, FBPLink, NoFloGraph} from '../types'
import {IRenderer} from './IRenderer'

export class NoFloRenderer implements IRenderer {
  public flow: NoFloGraph = {
    connections: [],
    processes: {},
    exports: [],
  }

  public create(): NoFloRenderer {
    return new NoFloRenderer()
  }

  public reset(): void {
    this.flow = {
      connections: [],
      processes: {},
      exports: [],
    }
  }

  /**
   * Called from the fbpParser
   */
  public parseComponentString(
    component: string,
    node: FBPComponent
  ): FBPComponent {
    const m = component.split(':')

    if (m.length === 1) {
      node.component = component
    } else {
      node.component = m[0]
      node.metadata = m.slice(1) // is this correct?
    }

    return node
  }

  public touchNode(/* processName */): void {}

  public addNode(c): void {
    this.flow.processes[c.process] = {
      component: c.component,
    }

    if (c.metadata) {
      this.flow.processes[c.process].metadata = {
        routes: c.metadata,
      }
    }
  }

  public addContext(/* process, in_port, context */): void {}

  public addProvider(/* provider */): void {
    /*
    if (!this.flow.providers)  this.flow.providers = [];
    this.flow.providers.push(provider);
    */
  }

  public addMeta(/* name, value */): void {
    // this.flow[name] = value;
  }

  public addExport($export): void {
    this.flow.exports.push({
      private: [$export.process, $export.port].join('.').toLowerCase(),
      public: $export.name.toLowerCase(),
    })
  }

  public addLink(link: FBPLink) {
    const connection: Connection = {} as Connection

    connection.tgt = {
      port: link.in.toLowerCase(),
      process: link.target,
    }

    if (undefined !== link.targetIndex) {
      connection.tgt.index = parseInt(link.targetIndex, 10)
    }

    if (link.source) {
      connection.src = {
        port: link.out.toLowerCase(),
        process: link.source,
      }

      if (undefined !== link.sourceIndex) {
        connection.src.index = parseInt(link.sourceIndex, 10)
      }
    }

    if (typeof link.data !== 'undefined') {
      connection.data = link.data
    }

    this.flow.connections.push(connection)
  }

  public fbpdoc(/* params */) {}

  public getFlow() {
    return JSON.parse(JSON.stringify(this.flow))
  }

  public toJSON() {
    return JSON.stringify(this.flow, null, 2)
  }

  public getIIPs(_action?: string): IIP[] {
    throw Error('Method not implemented.')
  }
}
