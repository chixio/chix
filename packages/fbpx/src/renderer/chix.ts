import {Flow, IIP, Link, Node, Provider, RemoteProvider} from '@chix/common'
import * as uuid from 'uuid'
import {
  ChixRenderOptions,
  ComponentDefinition,
  FBPComponent,
  FBPExport,
  FBPLink,
} from '../types'
import {IRenderer} from './IRenderer'

export type Action = {
  title: string
  nodes: Node[]
}

export type Exposed = {
  ports: ExposedPorts
}

export type ExposedPorts = {
  input: {
    [port: string]: {
      title: string
      expose: boolean
    }
  }
}

export type ExportsType = {
  [process: string]: ExposedPorts
}

const defaultProvider = 'https://api.chix.io/v1/nodes/{ns}/{name}'

export class ChixRenderer implements IRenderer {
  public static create(options: ChixRenderOptions) {
    return new ChixRenderer(options)
  }
  public action: string
  public mask = undefined
  public skipIDs = false
  public defaultProvider: RemoteProvider | false = false
  public flow: Flow
  public nodes: {[id: string]: Node} = {}
  public idx: string[] = []
  public iips = []
  public knownNodes = []
  public nameToUuid: {[uuid: string]: any} = {}

  private exports: ExportsType

  constructor(options?: ChixRenderOptions) {
    options = options || {}

    // do not set uuids
    if (options.skipIDs) {
      this.skipIDs = options.skipIDs
    }

    if (options.defaultProvider === false) {
      this.defaultProvider = false
    } else if (options.defaultProvider) {
      this.setDefaultProvider(options.defaultProvider)
    } else {
      this.setDefaultProvider(defaultProvider)
    }

    this.reset()
  }

  public actionStart(name: string) {
    // e.g. ::Delete
    this.action = name.replace('::', '')

    if (!this.flow.actions) {
      this.flow.actions = {}
    }

    // TODO: Not sure what action looked liked.
    ;(this.flow.actions[this.action] as any) = {
      nodes: [] as Node[],
      title: this.action,
    }
  }

  public actionEnd() {
    // e.g. ::Delete
    this.action = undefined
  }

  public setDefaultProvider(url: string) {
    this.defaultProvider = {
      url,
    }
  }

  /**
   *
   * Called from the fbpParser
   *
   */
  public parseComponentString(
    component: string,
    node: FBPComponent
  ): FBPComponent {
    // check for meta data
    const m = component.split(':')

    // Now: NS:ns/name or ns/name
    if (m.length === 1) {
      node.component = component
    } else if (m.length === 2) {
      node.NS = m[0]
      node.component = m[1]
    } else {
      throw Error('Failed to parse component string.')
    }

    return node
  }

  public reset() {
    this.flow = {} as Flow
    if (!this.skipIDs) {
      this.flow.id = uuid.v4()
    }
    this.flow.type = 'flow'
    this.flow.nodes = []
    this.flow.links = []

    // simplify node finding used by input.
    this.nodes = {}

    // simplify index finding.
    this.idx = []

    this.iips = []

    // this.exports = undefined;

    this.knownNodes = []

    this.nameToUuid = {}
  }

  public addNode(componentDefinition: ComponentDefinition): void {
    let ns
    let name

    const m = componentDefinition.component.split('/')

    if (m.length !== 2) {
      name = componentDefinition.component
    } else {
      ns = m[0]
      name = m[1]
    }

    const id = this.getUUID(componentDefinition.process)

    // TODO: Exposed type is weird and probably not totally correct overhere
    const node: Node = {
      id,
      title: componentDefinition.process,
      ns,
      name,
    }

    // Big namespace, the provider url
    if (componentDefinition.NS) {
      node.provider = componentDefinition.NS
    }

    // fix this also, must use uuid.
    // const lprocess = c.process.toLowerCase();
    const proc = this.getUUID(componentDefinition.process)

    if (this.exports && this.exports[proc]) {
      ;(node as Node & Exposed).ports = this.exports[proc]
    }

    /*
    if (metadata) {
      this.flow.processes[c.process].metadata = {
        routes: metadata
      };
    }
  */

    this.nodes[node.id] = node

    if (this.skipIDs) {
      node.id = componentDefinition.process
    }

    this.flow.nodes.push(node)

    this.idx.push(id)
  }

  // a node is mentioned
  public touchNode(processName: string) {
    const id = this.getUUID(processName)

    // register it
    if (this.action) {
      const aNodes = this.flow.actions[this.action].nodes
      if (aNodes.indexOf(id) === -1) {
        // ok problem if the node is just referenced.
        aNodes.push(id)
      }
    }
  }

  public addContext(process: string, inPort: string, context: any): void {
    const id = this.getUUID(process)
    const node = this.flow.nodes[this.idx.indexOf(id)]
    if (!node) {
      throw Error(['Node', process, 'is not defined'].join(' '))
    }
    if (!node.context) {
      node.context = {}
    }

    node.context[inPort] = context
  }

  public addLink(link: FBPLink): void {
    const newLink: Link = this.skipIDs ? {id: ''} : {id: uuid.v4()}
    newLink.metadata = {}

    const targetId = this.getUUID(link.target)

    // in[MyProcess]
    const sync = link.in.match(/\[(.*)\]/)

    if (sync) {
      link.in = link.in.replace(sync[0], '')
    }

    newLink.target = {id: targetId, port: link.in, setting: {}}

    if (link.targetIndex !== undefined) {
      newLink.target.setting.index = link.targetIndex
    }

    // TODO: is this ever set?
    if (link.sync !== undefined) {
      newLink.target.setting.sync = link.sync
    }

    if (link.targetAction) {
      // hm, put action within setting or not? neh;
      newLink.target.action = link.targetAction
    }

    if (sync) {
      // MyProcess
      newLink.target.setting.sync = this.getUUID(sync[1])
    }

    // TODO: link.settings is no more..
    if (link.settings) {
      if (link.settings.cyclic) {
        newLink.target.setting.cyclic = true
      }
      if (link.settings.persist) {
        newLink.target.setting.persist = true
      }
    }

    if (this.mask !== undefined) {
      newLink.target.setting.mask = this.mask
      this.mask = undefined // reset
    }

    let pointer = false

    // Ok, right now I do not save data within the link.
    if (!link.source) {
      // iip.
      link.source = this.flow.title || ''
      link.out = ':iip'
      newLink.source = {id: this.flow.id, port: link.out}
    } else {
      if (link.out.charAt(0) === '*') {
        link.out = link.out.slice(1)
        pointer = true
      }
      newLink.source = {id: this.getUUID(link.source), port: link.out}
    }

    newLink.source.setting = {}

    if (link.sourceAction) {
      // hm, put action within setting or not? neh;
      newLink.source.action = link.sourceAction
    }

    if (pointer) {
      newLink.source.setting.pointer = true
    }

    if (link.settings) {
      if (link.settings.collect) {
        newLink.source.setting.collect = true
      }
    }

    if (this.skipIDs) {
      delete newLink.id
    }

    if (link.sourceIndex !== undefined) {
      newLink.source.setting.index = link.sourceIndex
    }

    // Not really important this..
    newLink.metadata.title = [
      link.source + (link.sourceAction ? '::' + link.sourceAction : ''),
      link.out,
      '->',
      link.in,
      link.target + (link.targetAction ? '::' + link.targetAction : ''),
    ].join(' ')

    if (Object.keys(newLink.source.setting).length === 0) {
      delete newLink.source.setting
    }

    if (Object.keys(newLink.target.setting).length === 0) {
      delete newLink.target.setting
    }

    if (newLink.source.port === ':iip') {
      // note: this is different from specifying an action within target
      // or source.
      //
      // within target it means target the action of the node I'm pointing to.
      //
      // This however indicates the iip must run during this action.
      // TODO: above not valid anymore
      if (this.action) {
        ;(newLink as any).action = this.action
      }

      newLink.data = link.data

      this.iips.push(newLink)
    } else {
      this.flow.links.push(newLink)
    }
  }

  public addProvider(provider: Provider): void {
    if (!this.flow.providers) {
      this.flow.providers = {}
    }
    const key = provider.name || '@'
    this.flow.providers[key] = provider
  }

  public addMeta(name: string, value: any): void {
    if (this.action) {
      this.flow.actions[this.action][name] = value
    } else {
      if (name === 'package') {
        this.flow.ns = value
      } else {
        this.flow[name] = value
      }
    }
  }

  public addExport(c: FBPExport, ioType: string): void {
    const lprocess = this.getUUID(c.process)

    // ok, this only handles exposed input ports.
    // port def will be empty, but this doesn't matter.
    // the real information is in the node.
    // we could use it to override title though.
    if (!this.flow.ports) {
      this.flow.ports = {}
    }

    if (!this.flow.ports.hasOwnProperty(ioType)) {
      this.flow.ports[ioType] = {}
    }

    if (!c.port) {
      if (c.name) {
        // -> :start Proc (: is used to defined title normally)
        c.port = c.name
        delete c.name
      } else {
        throw new Error(['No port defined to expose for', c.name].join(' '))
      }
    }

    const portName = this.determineExternalPortName(c.port, c.name, ioType)

    this.flow.ports[ioType][portName] = {
      nodeId: lprocess,
      // title: c.process + ' ' + c.port, // default title
      title: portName.charAt(0).toUpperCase() + portName.slice(1),
      name: c.port,
    }

    const {input: inputPorts, output: outputPorts} = this.flow.ports

    // this _is_ used with exported ports.
    // because the external port will not require you to set an index
    // the internal port however needs to pass on this information.
    if (ioType === 'input' && c.hasOwnProperty('targetIndex')) {
      const inputPort = inputPorts[portName]

      // should be within setting.index
      if (!inputPort.setting) {
        inputPort.setting = {}
      }

      inputPort.setting.index = c.targetIndex
    } else if (ioType === 'output' && c.hasOwnProperty('sourceIndex')) {
      const outputPort = outputPorts[portName]

      if (!outputPort.setting) {
        outputPort.setting = {}
      }

      outputPort.setting.index = c.sourceIndex
    }

    if (c.setting) {
      if (!inputPorts[portName].setting) {
        inputPorts[portName].setting = {}
      }
      for (const k in c.setting) {
        if (c.setting.hasOwnProperty(k)) {
          inputPorts[portName].setting[k] = c.setting[k]
        }
      }
    }

    // BELOW I WANT TO DROP, expose will be different.
    // see above
    // Let's assume exports are defined before node declaration
    if (!this.exports) {
      this.exports = {}
    }

    if (!this.exports[lprocess]) {
      this.exports[lprocess] = {
        input: {},
      }
    }

    this.exports[lprocess].input[c.port] = {
      // not sure what to do with the alias,
      // also could override name here.
      title: c.name,
      expose: true,
    }
  }

  /**
   *
   * Get the IIPs
   *
   * The idea is not to include them in the graph definition.
   *
   * But allow them to be sent to the created graph.
   *
   */
  public getIIPs(action?: string): IIP[] {
    if (action) {
      return this.iips.filter((el) => el.action === action)
    }

    return JSON.parse(JSON.stringify(this.iips))
  }

  /**
   *
   * Get the Input
   *
   * Returns an object containing all data input.
   *
   * It is not used for processing, but it can
   * be used for display purposes.
   *
   */
  public getInput() {
    const results = []

    for (const nodeId in this.iips) {
      if (this.iips.hasOwnProperty(nodeId)) {
        const iip = this.iips[nodeId]
        const result = iip.target

        if (this.nodes[iip.target.id]) {
          // todo: weird construction
          result.process = this.nodes[iip.target.id].title

          if (iip.data === undefined) {
            throw Error('IIP data may not be `undefined`')
          }

          result.data = iip.data
          result.target = iip.target
          // todo: is now target.settings && source.settings.
          result.settings = iip.settings
          if (iip.action) {
            // dunno, could be in settings also.
            result.action = iip.action
          }
          results.push(result)
        } else {
          throw new Error('IIP refers to unknown node')
        }
      }
    }

    // Fill context
    for (const nodeId in this.nodes) {
      if (this.nodes[nodeId].context) {
        for (const port in this.nodes[nodeId].context) {
          if (this.nodes[nodeId].hasOwnProperty(port)) {
            results.push({
              process: this.nodes[nodeId].title,
              port,
              data: this.nodes[nodeId].context[port],
              context: true,
            })
          }
        }
      }
    }

    return results
  }

  public getFlow(): Flow {
    this.ensureDefaultProvider()

    this.checkNodes()

    return JSON.parse(JSON.stringify(this.flow))
  }

  public toJSON(): string {
    // TODO: add logic here to detected unconnected nodes.

    if (this.skipIDs) {
      delete this.flow.id
    }

    this.checkNodes()

    return JSON.stringify(this.flow, null, 2)
  }

  // Because we generate ID's on the fly and before
  // we know process names, (because links can come before definition)
  // We have to check whether all links point to existing nodes.
  public checkNodes() {
    for (const name in this.nameToUuid) {
      if (
        this.nameToUuid.hasOwnProperty(name) &&
        !this.nodes.hasOwnProperty(this.nameToUuid[name])
      ) {
        console.log(this.nodes)
        throw new Error(['Node', name, 'is not defined'].join(' '))
      }
    }
  }

  public getUUID(name: string) {
    if (this.skipIDs) {
      return name
    }

    if (!this.nameToUuid.hasOwnProperty(name)) {
      this.nameToUuid[name] = uuid.v4()
    }

    return this.nameToUuid[name]
  }

  /**
   *
   *  Adds a mask, this mask is always about the next
   * link added, the next link added *must* do something
   * with this value
   *
   */
  public addMask(mask) {
    this.mask = mask
  }

  public getName($uuid: string) {
    if (this.skipIDs) {
      return $uuid
    }

    let name

    for (const key in this.nameToUuid) {
      if (this.nameToUuid.hasOwnProperty(key)) {
        name = key
        const id = this.nameToUuid[key]
        if (id === $uuid) {
          break
        }
      }
    }

    if (!name) {
      throw new Error('Cannot find name for uuid: ' + $uuid)
    }

    return name
  }

  /**
   *
   * Makes sure the external portname is unique.
   *
   * TODO: just throw an error when we get an externalName provided and
   * it proofs not to be unique, this is an .fbp syntax error
   *
   */
  public determineExternalPortName(
    internalName: string,
    externalName: string,
    ioType: string
  ) {
    let portName = externalName ? externalName : internalName
    const origName = portName

    // ok here is the case of internal (c.port) and external name
    // for the iips I also need this. for now I will not support
    // custom naming of the port, I could though, ah well
    // let's see, so that this needs is internalPort, externalPort
    if (this.flow.ports[ioType].hasOwnProperty(portName)) {
      let nr = 2
      while (this.flow.ports[ioType].hasOwnProperty(portName)) {
        portName = origName + nr++
      }
    }

    return portName
  }

  public fbpdoc(params: any): void {
    console.log(params)
  }

  /**
   *
   * Ensures a default provider is set, but only if nodes are used
   * without a namespace and only set one if there was not yet a
   * default set, within the fbp file.
   *
   */
  public ensureDefaultProvider() {
    if (this.defaultProvider !== false) {
      if (!this.flow.providers) {
        this.flow.providers = {}
      }
      if (!this.flow.providers.hasOwnProperty('@')) {
        for (const node of this.flow.nodes) {
          if (!node.hasOwnProperty('provider')) {
            // means there is at least one node depending
            // on a default namespace so create one.
            this.flow.providers['@'] = this.defaultProvider
            break
          }
        }
      }
    }
  }
}
