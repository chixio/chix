export function debugTokens(tokens: any[], line: string) {
  // check if line matches joined tokens
  const a = tokens.map((t: any) => t.value)

  const sline = line
    .replace(/\n/, '')
    .replace(/[\s]+/g, ' ')
    .trim()

  const stokens = a
    .join(' ')
    .replace(/\s+$/, '')
    .replace(/[\s]+/g, ' ')

  if (sline !== stokens) {
    throw new Error(`\n\nMismatch:\n>${sline}<\n>${stokens}<`)
  }
}
