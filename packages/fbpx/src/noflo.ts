import {FBPParser} from './fbp'
import {NoFloRenderer} from './renderer'

/**
 * FBP parser for chix.
 *
 * @example Parse an fbp string
 *
 *   const parser = require('fbpx/chix')();
 *   parser.parse(fbp_string);
 */
export function NoFloParser() {
  const parser = new FBPParser()

  parser.addRenderer(new NoFloRenderer())

  return parser
}
