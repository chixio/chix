export class NoopResolver {
  public resolve(_data): any {
    throw Error(
      'No include resolver registered. Add one of the available Resolvers (FSResolver)'
    )
  }
}
