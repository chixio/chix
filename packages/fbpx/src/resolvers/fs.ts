import * as fs from 'fs'
import * as path from 'path'
import {IResolver} from './IResolver'

export class FSResolver implements IResolver {
  private _includePath: string = process.cwd()

  constructor(includePath?: string) {
    if (includePath) {
      this.setIncludePath(includePath)
    }
  }

  public resolve(data: string): any {
    const res = data.split('!')
    const file = res[0]
    const type = res[1]

    const body = fs
      .readFileSync(path.resolve(this._includePath, file))
      .toString()

    if (undefined !== type) {
      if (type === '' || type === 'json') {
        return JSON.parse(body)
      }

      return body
    }

    return body
  }

  public setIncludePath($path: string) {
    this._includePath = path.resolve($path)
  }
}
