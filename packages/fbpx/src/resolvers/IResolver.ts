export interface IResolver {
  resolve(data: string): any
}
