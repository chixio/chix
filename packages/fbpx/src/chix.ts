import {ChixRenderer} from './renderer'
import {ChixRenderOptions} from './types'

/**
 * FBP parser for chix.
 *
 * @example Parse an fbp string
 *
 *   var parser = require('fbpx/chix')();
 *   parser.parse(fbp_string);
 */
export function ChixParser(options?: ChixRenderOptions) {
  const fbp: any = require('./fbp')

  const parser = new fbp.FBPParser()

  parser.addRenderer(new ChixRenderer(options))

  return parser
}
