import {FSProvider, IIP, Provider, RemoteProvider} from '@chix/common'
import {DysLexer, ParserToken} from 'dyslexer'
import {ChixRenderer, IRenderer} from './renderer'
import {IResolver} from './resolvers/IResolver'
import {NoopResolver} from './resolvers/noop'
import {ActionScope} from './scopes/Action'
import {CommentScope} from './scopes/comment'
import {DataScope} from './scopes/data'
import {FBPDocScope} from './scopes/FBPDoc'
import {FunctionScope} from './scopes/Function'
import {JSONScope} from './scopes/JSON'
import {LeftHandScope} from './scopes/left'
import {MaskScope} from './scopes/mask'
import {MetaScope} from './scopes/meta'
import {ProviderScope} from './scopes/provider'
import {RightHandScope} from './scopes/right'
import {RootScope} from './scopes/root'
import {FBPComponent, FBPLink, FBPProcess} from './types'
import {debugTokens} from './util'

// remove outer quotes
const quoteReg = new RegExp('^[\'"]|[\'"]$', 'gm')

/**
 *
 * The FBPParser parses an FBP language file or string and
 * returns a JSON object representing the graph.
 *
 * Currently it supports generating json compatible with noflo & chix.
 *
 *
 * @example Example usage:
 *
 * var renderer = require('fbpx/lib/renderer/chix');
 * var parser = require('fbpx')();
 *
 * parser.addRenderer(renderer);
 * parser.parse(fbp_string);
 *
 * @returns {FBPParser}
 */

export class FBPParser {
  public debug = false
  public autoReset = true
  public lexer: DysLexer
  public currentLink: FBPLink = {} as FBPLink
  public data = undefined
  public include_path!: string
  public renderer!: IRenderer
  public provider: Provider = {} as Provider
  public _nodes = []

  private includeResolver: IResolver = new NoopResolver()
  constructor() {
    this.lexerSetup()
  }

  /**
   *
   * Reset this Parser instance
   *
   * @private
   */
  public reset() {
    this.currentLink = {} as FBPLink
    this.data = undefined
    this._nodes = []
    this.lexer.reset()
  }

  /**
   *
   * Setup the lexer
   *
   * @private
   */
  public lexerSetup() {
    this.lexer = new DysLexer('RootScope')
    this.lexer.addScope(LeftHandScope)
    this.lexer.addScope(CommentScope)
    this.lexer.addScope(RootScope)
    this.lexer.addScope(DataScope)
    this.lexer.addScope(JSONScope)
    this.lexer.addScope(FunctionScope)
    this.lexer.addScope(FBPDocScope)
    this.lexer.addScope(MetaScope)
    this.lexer.addScope(ActionScope)
    this.lexer.addScope(ProviderScope)
    this.lexer.addScope(RightHandScope)
    this.lexer.addScope(MaskScope)

    // not done by default.
    // TODO: pass this test always..
    // Requires opening and close tokens also to be fired.
    // now they are swallowed, causing a mismatch.
    // e.g () and {} do not appear as tokens.
    if (this.debug === true) {
      this.lexer.on('lineTokens', debugTokens)
    }
  }

  /**
   *
   * Parse Process
   *
   * @param {type} proc
   * @param {type} in_port
   * @param {type} context
   * @returns {parseProcess.p}
   */
  public parseProcess(
    proc: string,
    in_port?: string,
    context?: any
  ): FBPProcess {
    // first filter out action ::my_action

    // TODO: simplify to not use regexp (saver)
    let c: FBPComponent = {} as FBPComponent
    const ret = proc.match(/^([\w_:]+)\(?([\w-\/:]+)?\)?/)

    if (!ret || ret.length < 3) {
      console.error(ret)
      console.error(this.lexer.drawError())
      throw Error(`Failed to parse process: ${proc} in_port: ${in_port}`)
    }

    const _process: FBPProcess = {
      name: ret[1],
    }

    const component = ret[2]

    // test whether processName contains an ::action
    const sp = _process.name.split('::')

    if (sp.length > 1) {
      _process.name = sp[0]
      _process.action = sp[1]
    }

    if (component && component !== '') {
      c = this.renderer.parseComponentString(component, c)
      c.process = _process.name

      if (this._nodes.indexOf(_process.name) === -1) {
        // register node
        // this.renderer.addNode(c, metadata);
        this.renderer.addNode(c)
        this._nodes.push(c.process)
      }
    }

    // maybe also notify if node is refered to.
    this.renderer.touchNode(_process.name)

    // context provided
    if (in_port) {
      this.renderer.addContext(_process.name, in_port, context)
    }

    return _process
  }

  public addRenderer(renderer: IRenderer): void {
    this.renderer = renderer
  }

  public getRenderer(): IRenderer {
    if (this.renderer) {
      return this.renderer
    }

    throw Error('Renderer is not set.')
  }

  public handleTokens(tokens: ParserToken[]) {
    let in_port
    let out_port
    let process
    let token
    let i
    let ex
    let spl
    let collect
    let meta_name
    let cyclic
    let persist
    let context
    let targetIndex = null
    let sourceIndex = null
    let export_in
    let export_out

    this.currentLink = {} as FBPLink

    this.provider = {} as Provider

    for (i = 0; i < tokens.length; i++) {
      token = tokens[i]

      switch (token.name) {
        case 'DATA':
          // TODO: not sure whether to do this replacement here
          // or just send the quoted data.
          this.data =
            typeof token.value === 'string'
              ? token.value.replace(quoteReg, '')
              : token.value
          break
        case 'ACTION':
          ;(this.renderer as ChixRenderer).actionStart(token.value)
          break

        // case 'ACTION_START': not used.
        case 'ACTION_END':
          // ends the current action
          ;(this.renderer as ChixRenderer).actionEnd()
          break

        case 'FBPDOC':
          // NOP
          break

        case 'IN_PORT':
          if (token.value[0] === '^') {
            persist = true
            token.value = token.value.replace(/^\^/, '')
          }

          if (token.value[0] === '@') {
            // maybe use this for context instead
            token.value = token.value.replace(/^@/, '')
            context = true
          }

          in_port = token.value

          break

        case 'OUT_PORT':
          out_port = token.value
          break

        case 'TARGET_INDEX':
          // can now be string or number
          // if is numeric do parseInt else not.
          targetIndex = token.value.replace(/[\[\]]/g, '')
          if (/^\d+$/.test(targetIndex)) {
            targetIndex = parseInt(targetIndex, 10)
          }

          break

        case 'SOURCE_INDEX':
          // can now be string or number
          // if is numeric do parseInt else not.
          sourceIndex = token.value.replace(/[\[\]]/g, '')
          if (/^\d+$/.test(sourceIndex)) {
            sourceIndex = parseInt(sourceIndex, 10)
          }

          break

        case 'AS':
          break

        case 'PROVIDER_URL':
          if (token.value.indexOf('://') === -1) {
            ;(this.provider as FSProvider).path = token.value
          } else {
            // else consider it a local file path.
            ;(this.provider as RemoteProvider).url = token.value
          }
          break

        case 'PROVIDER_NS':
          this.provider.name = token.value.toLowerCase()
          break

        case 'META':
          meta_name = token.value.replace(/:$/, '')
          break

        case 'MASK':
          ;(this.renderer as ChixRenderer).addMask(token.value)
          break

        case 'META_DATA':
          this.renderer.addMeta(meta_name, token.value)
          break

        case 'PROCESS':
          this.currentLink = {} as FBPLink

          if (export_in) {
            process = this.parseProcess(token.value)

            ex = {
              port: in_port,
              process: process.name,
            }

            // NOT sure, maybe this needs action also..
            // neh.. It would mean exporting a port from
            // a subgraph which has actions defined.
            // any just add it.
            if (process.action) {
              ex.action = process.action
            }

            // check for in:title
            // Must be the last one, :start:Start should also work
            // An edge case, I do not really like the disambigity but
            // ah well..
            /*
              ":start:Start".split(':')  ["", "start", "Start"]
              ":start".split(':')        ["", "start"]
              "start".split(':')         ["start"]
              "normal".split(':')        ["normal"]
              "normal:Normal".split(':') ["normal", "Normal"]
            */
            spl = in_port.split(':')
            if (spl.length === 2) {
              ex.port = spl[0]
              ex.name = spl[1] // alternate portname
            } else if (spl.length === 3) {
              ex.port = ':' + spl[1]
              ex.name = spl[2] // alternate portname
            } else if (spl.length > 3) {
              throw new Error('Cannot determine port name')
            }

            // this _is_ used in case of exported ports
            if (targetIndex !== null) {
              ex.targetIndex = targetIndex
            }
            targetIndex = null

            if (sourceIndex !== null) {
              ex.sourceIndex = sourceIndex
            }
            sourceIndex = null

            if (persist) {
              if (!ex.setting) {
                ex.setting = {}
              }
              ex.setting.persist = true
              persist = undefined
            }

            this.renderer.addExport(ex, 'input')

            export_in = false
            in_port = undefined
          } else if (export_out) {
            process = this.parseProcess(token.value)

            ex = {
              port: in_port,
              process: process.name,
            }

            // check for in:title
            spl = in_port.split(':')
            if (spl.length > 1) {
              ex.name = spl[1] // alternate portname
              ex.port = spl[0]
            }

            // this _is_ used in case of exported ports
            if (targetIndex !== null) {
              ex.targetIndex = targetIndex
            }
            targetIndex = null

            if (sourceIndex !== null) {
              ex.sourceIndex = sourceIndex
            }
            sourceIndex = null

            // addExport
            // name is title..
            // this also uses inport, because the format is:
            //
            // <- outport Process
            this.renderer.addExport(ex, 'output')

            in_port = undefined
            export_out = false
          } else if (context) {
            // this data is now the context
            process = this.parseProcess(token.value, in_port, this.data)

            this.data = undefined
            context = false
          } else if (in_port) {
            this.currentLink.in = in_port

            if (process) {
              this.currentLink.source = process.name
              if (process.action) {
                this.currentLink.sourceAction = process.action
              }
            }

            process = this.parseProcess(token.value)
            this.currentLink.target = process.name
            if (process.action) {
              this.currentLink.targetAction = process.action
            }

            if (out_port) {
              this.currentLink.out = out_port
            }

            if (cyclic) {
              if (!this.currentLink.settings) {
                this.currentLink.settings = {}
              }
              this.currentLink.settings.cyclic = true
            }

            // TODO: a collect cannot be exported (doesn't really matter now)
            if (collect) {
              if (!this.currentLink.settings) {
                this.currentLink.settings = {}
              }
              this.currentLink.settings.collect = true
            }

            if (persist) {
              if (!this.currentLink.settings) {
                this.currentLink.settings = {}
              }
              this.currentLink.settings.persist = true
            }

            if (this.data !== undefined) {
              this.currentLink.data = this.data
              this.data = undefined
            }

            if (targetIndex !== null) {
              // can now be string or number
              // if is numeric do parseInt else not.
              this.currentLink.targetIndex = targetIndex
            }
            targetIndex = null

            if (sourceIndex !== null) {
              // can now be string or number
              // if is numeric do parseInt else not.
              this.currentLink.sourceIndex = sourceIndex
            }
            sourceIndex = null

            this.renderer.addLink(this.currentLink)

            this.currentLink = {} as FBPLink

            cyclic = false
            persist = false
            collect = false
          } else {
            // first, just register the process
            process = this.parseProcess(token.value)
          }

          break

        case 'COMMENT':
          // ignore comments
          break

        case 'EXPORT':
          const match = token.value.match(/^\s*EXPORT=(.*)/)

          const e = match[1].split(':')
          const pp = e[0].split('.')

          const exportDefinition = {
            name: e[1], // a different name for the port
            port: pp[1].toLowerCase(),
            process: pp[0],
          }

          this.renderer.addExport(exportDefinition, 'input')

          break

        // will change the way how right hand scope
        // is handled, a bit like DATA is handled.
        case 'EXPORT_IN':
          export_in = true
          break
        case 'EXPORT_OUT':
          export_out = true
          break
        case 'COLLECT_ARROW':
          collect = true
          break
        case 'INCLUDE_ARROW':
          // we already have data now, so overwrite that with
          // whatever we fetch localy or remote
          if (!this.data) {
            throw new Error('Cannot include without data section')
          }
          this.data = this.include(this.data)
          break
        // case 'ARROW':
        case 'PAIRED_ARROW':
          if (token.value === '=>' || token.value === '⇉') {
            cyclic = true
          }
          break
      }
    }

    if (Object.keys(this.provider).length) {
      this.renderer.addProvider(this.provider)
    }
  }

  // TODO: can be expanded to support many include types.
  public addIncludeResolver(resolver: IResolver) {
    this.includeResolver = resolver
  }

  /**
   *
   * Data Include
   *
   * Used to process `~>`
   *
   * Will treat the input as a file and tries to include it's contents
   *
   * @example Data include
   * 'my_json_file' ~> in MyProcess
   *
   * @param {Object} data
   * @returns {String}
   */
  public include(data: string): any {
    return this.includeResolver.resolve(data)
  }

  /**
   *
   * Get IIPs
   *
   * Returns the collected IIPs
   *
   * To store initial data within the JSON itself context should be used.
   * @returns {Object}
   */
  public getIIPs(): IIP[] {
    return this.renderer.getIIPs()
  }

  /**
   * Parses an FBP string.
   * @param {String} str An fbp language string
   * @param {string} include_path Hint for data includes
   * @returns {Object}
   */
  public parse(str: string, include_path?: string) {
    if (include_path) {
      this.include_path = include_path
    }

    if (this.renderer) {
      if (this.autoReset) {
        this.reset()
        this.renderer.reset()
      }

      const tokens = this.lexer.start(str, true)

      for (const token of tokens) {
        this.handleTokens(token)
      }

      return this.renderer.getFlow()
    }

    throw new Error('FBPParser needs a renderer')
  }

  public parseFile(_file: string) {
    throw Error(
      'Method deprecated, use one of the available include resolvers.'
    )
  }
}
