import * as fs from 'fs'
import * as path from 'path'
import {FBPParser as FBPParserBase} from './fbp'

export class FBPParser extends FBPParserBase {
  /**
   *
   * Parse File
   *
   * @param {String} file
   * @returns {Object}
   *
   */
  public parseFile(file: string) {
    // this is used to resolve data includes
    this.include_path = path.dirname(file)

    return this.parse(fs.readFileSync(file, 'utf-8'))
  }
}
