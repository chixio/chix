export type FBPProcess = {
  name: string
  action?: string
}

export type FBPComponent = {
  component: string
  NS: string
  process: string
  metadata?: {
    [key: string]: any
  }
}

export type FBPExport = {
  name: string
  port: string
  process: string
  setting?: {[key: string]: any}
  sourceIndex?: string
  targetIndex?: string
}

export type FBPLink = {
  source: string
  sourceAction?: string
  sourceIndex?: string

  target: string
  targetIndex?: string
  targetAction?: string

  in: string
  out: string

  settings?: {
    cyclic?: boolean
    collect?: boolean
    persist?: boolean
  }

  // TODO: is this ever set?
  sync?: any

  data?: any
}
