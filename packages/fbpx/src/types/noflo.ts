export type Process = {
  component: string
  metadata?: {[key: string]: any}
}

export type Processes = {
  [process: string]: Process
}

export type Connection = {
  data?: any
  src: {
    index?: number
    process: string
    port: string
  }
  tgt: {
    process: string
    port: string
    index?: number
  }
  metadata?: {
    route?: number
    schema?: string
  }
  secure?: boolean
}

export type Export = {
  private: string
  public: string
}

export type Group = {
  name: string
  nodes: string[]
  metadata?: {[key: string]: any}
}

export type Properties = {
  name?: string
  environment: {type: string; content?: string} & {[key: string]: any}
  description?: string
  icon?: string
}

export type ExternalPorts = {
  process: string
  port: string
  metadata?: {[key: string]: any}
}

export type NoFloGraph = {
  caseSensitive?: boolean
  inports?: ExternalPorts
  outports?: ExternalPorts
  properties?: Properties
  connections: Connection[]
  processes: Processes
  exports?: Export[]
  groups?: Group[]
}
