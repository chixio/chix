export type ChixRenderOptions = {
  skipIDs?: boolean
  defaultProvider?: false | string
}

export type ComponentDefinition = {
  component: string
  process: string
  NS: string
}
