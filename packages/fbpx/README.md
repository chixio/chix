FBP flow definition language parser for Chiχ
====

The language parser provides two renderers:
  - NoFloRenderer:
    creates a noflo compatible graph structure
  - ChixRenderer:
    creates a chix flow definition 

Usage:
```typescript
import {ChixRenderer, FBPParser, FSResolver} from '@chix/fbpx'

const parser = new FBPParser()

// noflo
// renderer = new NoFloRenderer()

const renderer = new ChixRenderer()
renderer.skipIDs = false 
renderer.defaultProvider = 'https://api.chix.io/v1/{provider}/{ns}/{name}' 

// how to resolve includes
parser.addIncludeResolver(new FSResolver())

// register the render with the parser 
parser.addRenderer(renderer)

const flowDefinition = parser.parse(contents, includePath)

console.log(JSON.stringify(flowDefinition, null, 2)
```

### Include Resolver

Fbpx can reference file contents to be included using a resolver:
```fbp
'./my_include_file.md' ~> @in MyProcess
```
The `~>` indicates the input should be treated as a path and it's
contents is set as input.
The resulting flow definition will have the contents of the file embedded.

### Listening for line tokens

It's possible to listen to the parse tokens while they are being read.

To listen to the tokens per line use something like:
```javascript
  parser.lexer.on(
    'lineTokens',
    (tokens: LexerToken[], line: string, nr: number) => {
      // Do something with the tokens
      // env.tokenTable += tokenTable(tokens, line, nr)
    }
  )
```
