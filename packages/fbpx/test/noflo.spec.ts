// tslint:disable only-arrow-functions object-literal-sort-keys no-implicit-dependencies
import * as chai from 'chai'
import {NoFloRenderer, FBPParser as Parser, NoFloGraph} from '@chix/fbpx'

const createParser = () => {
  const parser = new Parser()
  const renderer = new NoFloRenderer()

  parser.addRenderer(renderer)

  return parser
}

describe('FBP parser', () => {
  it('should provide a parse method', () => {
    const parser = createParser()
    chai.expect(parser.parse).to.be.a('function')
  })
  describe('with simple FBP string', () => {
    let graphData: NoFloGraph
    const fbpData = "'somefile' -> SOURCE Read(ReadFile)\n"
    it('should produce a graph JSON object', () => {
      const parser = createParser()
      graphData = parser.parse(fbpData)
      chai.expect(graphData).to.be.an('object')
    })
    describe('the generated graph', () => {
      it('should contain one node', () => {
        chai.expect(graphData.processes).to.eql({
          Read: {
            component: 'ReadFile',
          },
        })
      })
      it('should contain an IIP', () => {
        chai.expect(graphData.connections).to.be.an('array')
        chai.expect(graphData.connections.length).to.equal(1)
      })
    })
  })
  describe('with three-statement FBP string', () => {
    let graphData: NoFloGraph
    const fbpData =
      "'somefile.txt' -> SOURCE Read(ReadFile) OUT -> IN Display(Output)"
    it('should produce a graph JSON object', () => {
      const parser = createParser()
      graphData = parser.parse(fbpData)
      chai.expect(graphData).to.be.an('object')
    })
    describe('the generated graph', () => {
      it('should contain two nodes', () => {
        chai.expect(graphData.processes).to.eql({
          Display: {
            component: 'Output',
          },
          Read: {
            component: 'ReadFile',
          },
        })
      })
      it('should contain an edge and an IIP', () => {
        chai.expect(graphData.connections).to.be.an('array')
        chai.expect(graphData.connections.length).to.equal(2)
      })
      it('should contain no exports', () => {
        chai.expect(graphData.exports).to.be.an('array')
        chai.expect(graphData.exports).to.have.lengthOf(0)
      })
    })
  })
  describe('with a more complex FBP string', () => {
    let graphData: NoFloGraph
    const fbpData =
      "'8003' -> LISTEN WebServer(HTTP/Server) REQUEST -> IN Profiler(HTTP/Profiler) OUT -> IN Authentication(HTTP/BasicAuth)\nAuthentication() OUT -> IN GreetUser(HelloController) OUT -> IN WriteResponse(HTTP/WriteResponse) OUT -> IN Send(HTTP/SendResponse)\n'hello.jade' -> SOURCE ReadTemplate(ReadFile) OUT -> TEMPLATE Render(Template)\nGreetUser() DATA -> OPTIONS Render() OUT -> STRING WriteResponse()"
    it('should produce a graph JSON object', () => {
      const parser = createParser()
      graphData = parser.parse(fbpData)
      chai.expect(graphData).to.be.an('object')
    })
    describe('the generated graph', () => {
      it('should contain eight nodes', () => {
        chai.expect(graphData.processes).to.be.an('object')
        chai
          .expect(graphData.processes)
          .to.have.keys([
            'WebServer',
            'Profiler',
            'Authentication',
            'GreetUser',
            'WriteResponse',
            'Send',
            'ReadTemplate',
            'Render',
          ])
      })
      it('should contain ten edges and IIPs', () => {
        chai.expect(graphData.connections).to.be.an('array')
        chai.expect(graphData.connections.length).to.equal(10)
      })
      it('should contain no exports', () => {
        chai.expect(graphData.exports).to.be.an('array')
        chai.expect(graphData.exports).to.have.lengthOf(0)
      })
    })
  })
  describe('with FBP string containing an IIP with whitespace', () => {
    let graphData: NoFloGraph
    const fbpData = "'foo Bar BAZ' -> IN Display(Output)"
    it('should produce a graph JSON object', () => {
      const parser = createParser()
      graphData = parser.parse(fbpData)
      chai.expect(graphData).to.be.an('object')
    })
    describe('the generated graph', () => {
      it('should contain a node', () => {
        chai.expect(graphData.processes).to.eql({
          Display: {
            component: 'Output',
          },
        })
      })
      it('should contain an IIP', () => {
        chai.expect(graphData.connections).to.be.an('array')
        chai.expect(graphData.connections.length).to.equal(1)
        chai.expect(graphData.connections[0].data).to.equal('foo Bar BAZ')
      })
      it('should contain no exports', () => {
        chai.expect(graphData.exports).to.be.an('array')
        chai.expect(graphData.exports).to.have.lengthOf(0)
      })
    })
  })
  describe('with FBP string containing an empty IIP string', () => {
    let graphData: NoFloGraph
    const fbpData = "'' -> IN Display(Output)"
    it('should produce a graph JSON object', () => {
      const parser = createParser()
      graphData = parser.parse(fbpData)
      chai.expect(graphData).to.be.an('object')
    })
    describe('the generated graph', () => {
      it('should contain a node', () => {
        chai.expect(graphData.processes).to.eql({
          Display: {
            component: 'Output',
          },
        })
      })
      it('should contain an IIP', () => {
        chai.expect(graphData.connections).to.be.an('array')
        chai.expect(graphData.connections.length).to.equal(1)
        chai.expect(graphData.connections[0].data).to.equal('')
      })
      it('should contain no exports', () => {
        chai.expect(graphData.exports).to.be.an('array')
        chai.expect(graphData.exports).to.have.lengthOf(0)
      })
    })
  })
  describe('with FBP string containing comments', () => {
    let graphData: NoFloGraph
    const fbpData =
      "# Do stuff\n'foo bar' -> IN Display(Output) # Here we show the string"
    it('should produce a graph JSON object', () => {
      const parser = createParser()
      graphData = parser.parse(fbpData)
      chai.expect(graphData).to.be.an('object')
    })
    describe('the generated graph', () => {
      it('should contain a node', () => {
        chai.expect(graphData.processes).to.eql({
          Display: {
            component: 'Output',
          },
        })
      })
      it('should contain an IIP', () => {
        chai.expect(graphData.connections).to.be.an('array')
        chai.expect(graphData.connections.length).to.equal(1)
        chai.expect(graphData.connections[0].data).to.equal('foo bar')
      })
      it('should contain no exports', () => {
        chai.expect(graphData.exports).to.be.an('array')
        chai.expect(graphData.exports).to.have.lengthOf(0)
      })
    })
  })
  describe('with FBP string containing URL as IIP', () => {
    let graphData: NoFloGraph
    const fbpData =
      "'http://localhost:5984/default' -> URL Conn(couchdb/OpenDatabase)"
    it('should produce a graph JSON object', () => {
      const parser = createParser()
      graphData = parser.parse(fbpData)
      chai.expect(graphData).to.be.an('object')
    })
    describe('the generated graph', () => {
      it('should contain a node', () => {
        chai.expect(graphData.processes).to.eql({
          Conn: {
            component: 'couchdb/OpenDatabase',
          },
        })
      })
      it('should contain an IIP', () => {
        chai.expect(graphData.connections).to.be.an('array')
        chai.expect(graphData.connections.length).to.equal(1)
        chai
          .expect(graphData.connections[0].data)
          .to.equal('http://localhost:5984/default')
      })
      it('should contain no exports', () => {
        chai.expect(graphData.exports).to.be.an('array')
        chai.expect(graphData.exports).to.have.lengthOf(0)
      })
    })
  })
  describe('with FBP string containing RegExp as IIP', () => {
    let graphData: NoFloGraph
    const fbpData =
      "'_id=(d+.d+.d*)=http://iks-project.eu/%deliverable/$1' -> REGEXP MapDeliverableUri(MapPropertyValue)\n'path=/_(?!(includes|layouts)' -> REGEXP MapDeliverableUri(MapPropertyValue)\n'@type=deliverable' -> PROPERTY SetDeliverableProps(SetProperty)\n'#foo' -> SELECTOR Get(dom/GetElement)\n'Hi, {{ name }}' -> TEMPLATE Get"
    it('should produce a graph JSON object', () => {
      const parser = createParser()
      graphData = parser.parse(fbpData)
      chai.expect(graphData).to.be.an('object')
    })
    describe('the generated graph', () => {
      it('should contain two nodes', () => {
        chai.expect(graphData.processes).to.eql({
          MapDeliverableUri: {
            component: 'MapPropertyValue',
          },
          SetDeliverableProps: {
            component: 'SetProperty',
          },
          Get: {
            component: 'dom/GetElement',
          },
        })
      })
      it('should contain IIPs', () => {
        chai.expect(graphData.connections).to.be.an('array')
        chai.expect(graphData.connections.length).to.equal(5)
        chai.expect(graphData.connections[0].data).to.be.a('string')
      })
      it('should contain no exports', () => {
        chai.expect(graphData.exports).to.be.an('array')
        chai.expect(graphData.exports).to.have.lengthOf(0)
      })
    })
  })
  describe('with FBP string with EXPORTs', () => {
    let graphData: NoFloGraph
    const fbpData =
      'EXPORT=READ.IN:FILENAME\nRead(ReadFile) OUT -> IN Display(Output) '
    it('should produce a graph JSON object', () => {
      const parser = createParser()
      graphData = parser.parse(fbpData)
      chai.expect(graphData).to.be.an('object')
    })
    describe('the generated graph', () => {
      it('should contain two nodes', () => {
        chai.expect(graphData.processes).to.eql({
          Display: {
            component: 'Output',
          },
          Read: {
            component: 'ReadFile',
          },
        })
      })
      it('should contain a single connection', () => {
        chai.expect(graphData.connections).to.be.an('array')
        chai.expect(graphData.connections.length).to.equal(1)

        chai.expect(graphData.connections[0]).to.eql({
          src: {
            port: 'out',
            process: 'Read',
          },
          tgt: {
            port: 'in',
            process: 'Display',
          },
        })
      })
      it('should contain an export', () => {
        chai.expect(graphData.exports).to.be.an('array')
        chai.expect(graphData.exports).to.have.lengthOf(1)
        chai.expect((graphData.exports as any)[0]).to.eql({
          private: 'read.in',
          public: 'filename',
        })
      })
    })
  })
  describe('with FBP string containing node metadata', () => {
    let graphData: NoFloGraph
    const fbpData =
      'Read(ReadFile) OUT -> IN Display(Output:foo)\n\n# And we drop the rest\nDisplay() OUT -> IN Drop(Drop:foo)'
    it('should produce a graph JSON object', () => {
      const parser = createParser()
      graphData = parser.parse(fbpData)
      chai.expect(graphData).to.be.an('object')
    })
    describe('the generated graph', () => {
      it('should contain nodes with named routes', () => {
        chai.expect(graphData.processes).to.eql({
          Display: {
            component: 'Output',
            metadata: {
              routes: ['foo'],
            },
          },
          Drop: {
            component: 'Drop',
            metadata: {
              routes: ['foo'],
            },
          },
          Read: {
            component: 'ReadFile',
          },
        })
      })
      it('should contain two edges', () => {
        chai.expect(graphData.connections).to.be.an('array')
        chai.expect(graphData.connections.length).to.equal(2)
      })
      it('should contain no exports', () => {
        chai.expect(graphData.exports).to.be.an('array')
        chai.expect(graphData.exports).to.have.lengthOf(0)
      })
    })
  })
  describe('with an invalid FBP string', () => {
    const fbpData = "'foo' -> Display(Output)"
    it('should fail with an Exception', () => {
      chai
        .expect(() => {
          const parser = createParser()
          parser.parse(fbpData)
        })
        .to.throw(Error)
    })
  })
  describe('with a component that contains dashes in name', () => {
    let graphData: NoFloGraph
    const fbpData = "'somefile' -> SOURCE Read(my-cool-component/ReadFile)"
    it('should produce a graph JSON object', () => {
      const parser = createParser()
      graphData = parser.parse(fbpData)
      chai.expect(graphData).to.be.an('object')
    })
    describe('the generated graph', () => {
      it('should contain one node', () => {
        chai.expect(graphData.processes).to.eql({
          Read: {
            component: 'my-cool-component/ReadFile',
          },
        })
      })
      it('should contain an IIP', () => {
        chai.expect(graphData.connections).to.be.an('array')
        chai.expect(graphData.connections.length).to.equal(1)
      })
    })
  })
  describe('with commas to separate statements', () => {
    let graphData: NoFloGraph
    const fbpData =
      "'Hello' -> IN Foo(Component), 'World' -> IN Bar(OtherComponent), Foo OUT -> DATA Bar"
    it('should produce a graph JSON object', () => {
      const parser = createParser()
      graphData = parser.parse(fbpData)
      chai.expect(graphData).to.be.an('object')
    })
    describe('the generated graph', () => {
      it('should contain two nodes', () => {
        chai.expect(graphData.processes).to.eql({
          Bar: {
            component: 'OtherComponent',
          },
          Foo: {
            component: 'Component',
          },
        })
      })
      it('should contain two IIPs and one edge', () => {
        chai.expect(graphData.connections).to.eql([
          {
            data: 'Hello',
            tgt: {
              port: 'in',
              process: 'Foo',
            },
          },
          {
            data: 'World',
            tgt: {
              port: 'in',
              process: 'Bar',
            },
          },
          {
            src: {
              port: 'out',
              process: 'Foo',
            },
            tgt: {
              port: 'data',
              process: 'Bar',
            },
          },
        ])
      })
    })
  })

  describe('with underscores and numbers in ports, nodes, and components', () => {
    let graphData: NoFloGraph
    const fbpData = "'Hello 09' -> IN_2 Foo_Node_42(Component_15)"
    it('should produce a graph JSON object', () => {
      const parser = createParser()
      graphData = parser.parse(fbpData)
      chai.expect(graphData).to.be.an('object')
    })
    describe('the generated graph', () => {
      it('should contain one node', () => {
        chai.expect(graphData.processes).to.eql({
          Foo_Node_42: {
            component: 'Component_15',
          },
        })
      })
      it('should contain an IIP', () => {
        chai.expect(graphData.connections).to.be.an('array')
        chai.expect(graphData.connections.length).to.equal(1)
        chai.expect(graphData.connections[0]).to.eql({
          data: 'Hello 09',
          tgt: {
            port: 'in_2',
            process: 'Foo_Node_42',
          },
        })
      })
    })
  })

  describe('with array index on ports', () => {
    let graphData: NoFloGraph
    const fbpData =
      "'Hello 09' -> [0] IN_2 Foo_Node_42(Component_15) OUT [9] -> [1] IN Some(Comp)"
    it('should produce a graph JSON object', () => {
      const parser = createParser()
      graphData = parser.parse(fbpData)
      chai.expect(graphData).to.be.an('object')
    })
    describe('the generated graph', () => {
      it('should contain two nodes', () => {
        chai.expect(Object.keys(graphData.processes).length).to.eql(2)
      })
      it('the nodes should have an input and output index', () => {
        chai.expect(graphData.connections).to.eql([
          {
            data: 'Hello 09',
            tgt: {
              index: 0,
              port: 'in_2',
              process: 'Foo_Node_42',
            },
          },
          {
            src: {
              index: 9,
              port: 'out', // converted to lowercase correct?
              process: 'Foo_Node_42',
            },
            tgt: {
              index: 1,
              port: 'in',
              process: 'Some',
            },
          },
        ])
      })
    })
  })
})
