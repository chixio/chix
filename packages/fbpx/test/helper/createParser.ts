import {FBPParser, ChixRenderer} from '@chix/fbpx'

export function createParser() {
  const parser = new FBPParser()
  const renderer = new ChixRenderer()

  renderer.skipIDs = true
  parser.addRenderer(renderer)

  return parser
}
