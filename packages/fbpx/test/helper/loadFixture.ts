import * as fs from 'fs'
import * as path from 'path'

export function loadFixture(fileName: string) {
  return fs.readFileSync(
    path.resolve(__dirname, '../fixtures', fileName),
    'utf-8'
  )
}
