import {Flow as FlowDefinition} from '@chix/common'
import {expect} from 'chai'
import {FBPParser} from '@chix/fbpx'
import {createParser} from '../helper'

describe('With a graph containing pointers and sync ports', () => {
  const parser: FBPParser = createParser()
  const fbpData = 'MyProcess(test) *out -> in[MyProcess] Consume(consume)\n'

  describe('the generated graph', () => {
    const flow: FlowDefinition = parser.parse(fbpData)

    it('should contain two nodes', () => {
      expect(flow.nodes[0]).to.eql({
        id: 'MyProcess',
        name: 'test',
        title: 'MyProcess',
      })
      expect(flow.nodes[1]).to.eql({
        id: 'Consume',
        name: 'consume',
        title: 'Consume',
      })
    })

    it('should contain a pointer and a sync port', () => {
      expect(flow.links).to.eql([
        {
          metadata: {
            title: 'MyProcess out -> in Consume',
          },
          source: {
            id: 'MyProcess',
            port: 'out',
            setting: {
              pointer: true,
            },
          },
          target: {
            id: 'Consume',
            port: 'in',
            setting: {
              sync: 'MyProcess',
            },
          },
        },
      ])
    })
  })
})
