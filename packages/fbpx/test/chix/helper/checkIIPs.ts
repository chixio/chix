import {IIP as IIPDefinition} from '@chix/common'
import {checkIIP} from './checkIIP'

export function checkIIPs(real: IIPDefinition[], expected: IIPDefinition[]) {
  real.forEach((result: IIPDefinition, index: number) => {
    checkIIP(result, expected[index])
  })
}
