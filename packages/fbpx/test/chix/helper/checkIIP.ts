// Helper to check an iip.
// source iip id is always unique so we only check whether it was set.
import {IIP as IIPDefinition} from '@chix/common'
import {expect} from 'chai'

export function checkIIP(real: IIPDefinition, expected: IIPDefinition) {
  expect(expected).to.have.property('source')
  expect(expected).to.have.property('target')
  expect(expected).to.have.property('data')

  expect(real).to.have.property('target')
  expect(real).to.have.property('data')

  // check source structure
  expect(real.source).to.have.property('port')

  // check target structure
  expect(real.target).to.have.property('id')
  expect(real.target).to.have.property('port')
  expect(real.target).to.eql(expected.target)

  // check data
  expect(real.data).to.eql(expected.data)

  // copy id and compare both together
  // expected.source.id = real.source.id;

  // do not care that much about metadata.title at the moment..
  expected.metadata = real.metadata

  expect(real).to.eql(expected)
}
