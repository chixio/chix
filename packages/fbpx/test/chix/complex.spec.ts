import {Flow as FlowDefinition} from '@chix/common'
import {expect} from 'chai'
import {FBPParser} from '@chix/fbpx'
import {createParser} from '../helper'
import {checkIIPs} from './helper'

describe('Complex', () => {
  describe('with a more complex FBP string', () => {
    const fbpData = [
      "'8003' -> LISTEN WebServer(HTTP/Server) REQUEST -> IN Profiler(HTTP/Profiler) OUT -> IN Authentication(HTTP/BasicAuth)",
      'Authentication() OUT -> IN GreetUser(HelloController) OUT -> IN WriteResponse(HTTP/WriteResponse) OUT -> IN Send(HTTP/SendResponse)',
      "'hello.jade' -> SOURCE ReadTemplate(ReadFile) OUT -> TEMPLATE Render(Template)",
      'GreetUser() DATA -> OPTIONS Render() OUT -> STRING WriteResponse()',
    ].join('\n')

    const parser: FBPParser = createParser()
    const flow: FlowDefinition = parser.parse(fbpData)

    it('should produce a graph JSON object', () => {
      expect(flow).to.be.an('object')
    })

    describe('the generated graph', () => {
      it('should contain eight nodes', () => {
        expect(flow.nodes).to.be.an('array')
        const keys = flow.nodes.map((node: any) => node.id)
        expect(keys).to.eql([
          'WebServer',
          'Profiler',
          'Authentication',
          'GreetUser',
          'WriteResponse',
          'Send',
          'ReadTemplate',
          'Render',
        ])
      })

      it('should contain eight edges', () => {
        expect(flow.links).to.be.an('array')
        expect(flow.links.length).to.equal(8)
      })

      it('should contain two IIPs', () => {
        expect(parser.getRenderer().getIIPs()).to.be.an('array')
        checkIIPs(parser.getRenderer().getIIPs(), [
          {
            data: '8003',
            source: {
              port: ':iip',
            },
            target: {
              id: 'WebServer',
              port: 'LISTEN',
            },
          },
          {
            data: 'hello.jade',
            source: {
              port: ':iip',
            },
            target: {
              id: 'ReadTemplate',
              port: 'SOURCE',
            },
          },
        ])
      })
    })
  })

  describe('with FBP string containing EXPORTs, persistent ports, alternative names and index information', () => {
    const fbpData =
      '-> ^IN Read(ReadFile)\n-> [prop] ^IN2:Input2 Read\n Read OUT -> IN Display(Output)\n <- out Display'

    const parser: FBPParser = createParser()
    const flow: FlowDefinition = parser.parse(fbpData)

    it('should produce a graph JSON object', () => {
      expect(flow).to.be.an('object')
    })

    describe('the generated graph', () => {
      it('should contain two nodes', () => {
        expect(flow.nodes).to.eql([
          {
            id: 'Read',
            name: 'ReadFile',
            title: 'Read',
          },
          {
            id: 'Display',
            name: 'Output',
            title: 'Display',
          },
        ])
      })

      it('should contain external ports', () => {
        expect(flow.ports).to.eql({
          input: {
            IN: {
              name: 'IN',
              nodeId: 'Read',
              setting: {
                persist: true,
              },
              title: 'IN',
            },
            Input2: {
              name: 'IN2',
              nodeId: 'Read',
              setting: {
                index: 'prop',
                persist: true,
              },
              title: 'Input2',
            },
          },
          output: {
            out: {
              name: 'out',
              nodeId: 'Display',
              title: 'Out',
            },
          },
        })
      })

      it('should contain a single connection', () => {
        expect(flow.links).to.be.an('array')
        expect(flow.links.length).to.equal(1)

        const res = {
          metadata: {
            title: 'Read OUT -> IN Display',
          },
          source: {
            id: 'Read',
            port: 'OUT',
          },
          target: {
            id: 'Display',
            port: 'IN',
          },
        }

        expect(flow.links[0]).to.eql(res)
      })
    })
  })
})
