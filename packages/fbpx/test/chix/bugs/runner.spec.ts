import {Flow as FlowDefinition, IIP} from '@chix/common'
import {expect} from 'chai'
import * as fs from 'fs'
import * as glob from 'glob'
import * as yaml from 'js-yaml'
import * as _ from 'lodash'
import * as path from 'path'
import {FBPParser} from '@chix/fbpx'
import {createParser} from '../../helper'

export type TestFile = {
  file: string
  contents: {
    title: string
    input: string
    output?: FlowDefinition
    expect: {
      [path: string]: any
    }
    iips?: IIP[]
  }
}

describe('Test Runner', () => {
  describe('Run tests', () => {
    // For Debugging, allows to pass --only=bug1 to test only one bug.yml file
    const onlyIndex = process.argv.findIndex((arg) => arg === '--only')
    let only

    if (onlyIndex >= 0) {
      only = process.argv[onlyIndex + 1]
    }

    const tests = glob
      .sync(path.resolve(__dirname, './data/*.yml'))
      .map((file) => {
        return {
          file,
          contents: yaml.load(fs.readFileSync(file, 'utf-8')),
        }
      }) as TestFile[]

    for (const test of tests) {
      if (
        (!only || only === path.basename(test.file, '.yml')) &&
        test.contents
      ) {
        it(`${path.basename(test.file)}:${test.contents.title}`, () => {
          const parser: FBPParser = createParser()

          let flow: FlowDefinition
          try {
            flow = parser.parse(test.contents.input)
          } catch (error) {
            console.error(parser.lexer.drawError())
            throw error
          }

          if (test.contents.expect) {
            _.keys(test.contents.expect).forEach((_path) => {
              const value = _.get(flow, _path)

              if (value === undefined) {
                console.log(JSON.stringify(flow, null, 2))

                expect(value).to.not.eql(
                  undefined,
                  `Unable to find path ${_path}`
                )
              }
              expect(value).to.eql(test.contents.expect[_path])
            })
          } else if (test.contents.output) {
            expect(test.contents.output).to.eql(flow)
          } else {
            throw Error(
              `Data fixture ${test.file} does not contain any expectation.`
            )
          }

          if (test.contents.iips) {
            expect(parser.getIIPs()).to.eql(test.contents.iips)
          }
        })
      }
    }
  })
})
