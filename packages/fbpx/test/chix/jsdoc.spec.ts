// FBPDOC is not parsed yet, but at least allow them
import {Flow as FlowDefinition} from '@chix/common'
import {expect} from 'chai'
import {FBPParser} from '@chix/fbpx'
import {createParser} from '../helper'

describe('With a graph containing jsdoc style comments', () => {
  const parser: FBPParser = createParser()
  const fbpData =
    '/* Some Comment\n @proc {MyProc} My Proc */\nMyProc(my/proc)\n'

  describe('the generated graph', () => {
    const flow: FlowDefinition = parser.parse(fbpData)

    it('should contain one node', () => {
      expect(flow.nodes[0]).to.eql({
        id: 'MyProc',
        // title: 'My Proc', FBP DOC
        name: 'proc',
        ns: 'my',
        title: 'MyProc',
      })
    })
  })
})
