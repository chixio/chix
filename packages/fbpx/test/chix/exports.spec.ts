import {Flow as FlowDefinition} from '@chix/common'
import {expect} from 'chai'
import {FBPParser} from '@chix/fbpx'
import {createParser} from '../helper'

describe('with FBP string with EXPORTs', () => {
  const fbpData = [
    '-> IN Read(ReadFile)',
    'Read OUT -> IN Display(Output)',
    '<- out Display',
  ].join('\n')

  const parser: FBPParser = createParser()
  const flow: FlowDefinition = parser.parse(fbpData)

  it('should produce a graph JSON object', () => {
    expect(flow).to.be.an('object')
  })

  describe('the generated graph', () => {
    it('should contain two nodes', () => {
      expect(flow.nodes).to.eql([
        {
          id: 'Read',
          name: 'ReadFile',
          title: 'Read',
        },
        {
          id: 'Display',
          name: 'Output',
          title: 'Display',
        },
      ])
    })

    it('should contain external ports', () => {
      expect(flow.ports).to.eql({
        input: {
          IN: {
            name: 'IN',
            nodeId: 'Read',
            title: 'IN',
          },
        },
        output: {
          out: {
            name: 'out',
            nodeId: 'Display',
            title: 'Out',
          },
        },
      })
    })

    it('should contain a single connection', () => {
      expect(flow.links).to.be.an('array')
      expect(flow.links.length).to.equal(1)

      const res = {
        metadata: {
          title: 'Read OUT -> IN Display',
        },
        source: {
          id: 'Read',
          port: 'OUT',
        },
        target: {
          id: 'Display',
          port: 'IN',
        },
      }

      expect(flow.links[0]).to.eql(res)
    })
  })
})
