import {Actions, Flow as FlowDefinition} from '@chix/common'
import {expect} from 'chai'
import {FBPParser} from '@chix/fbpx'
import {createParser} from '../helper'
import {checkIIPs} from './helper'

describe('Actions', () => {
  describe('With a graph containing actions', () => {
    let flow: FlowDefinition

    const parser: FBPParser = createParser()
    const fbpData = `
Proc1(proc)

::Delete {
  title: Delete
  description: Delete a repository
  
  Proc1
  Proc2(proc)
  
  "hi" -> in Proc1
  "hey" -> in Proc2
}
`
    describe('the generated graph', () => {
      flow = parser.parse(fbpData)

      it('should contain two nodes', () => {
        expect(flow.nodes[0]).to.eql({
          id: 'Proc1',
          name: 'proc',
          title: 'Proc1',
        })
        expect(flow.nodes[1]).to.eql({
          id: 'Proc2',
          name: 'proc',
          title: 'Proc2',
        })
      })

      it('should contain one action', () => {
        const actions = flow.actions as Actions

        expect(actions).to.be.an('object')
        expect(Object.keys(actions).length).to.eql(1)
        expect(actions.Delete.nodes.length).to.eql(2)
        expect(actions.Delete.title).to.eql('Delete')
        expect(actions.Delete.description).to.eql('Delete a repository')
      })

      it.skip('should contain two IIPs', () => {
        const iips = parser.getIIPs()
        expect(iips).to.be.an('array')
        checkIIPs(iips, [
          {
            data: 'hi',
            source: {
              port: ':iip',
            },
            target: {
              id: 'Proc1',
              port: 'in',
              action: 'Delete',
            },
          },
          {
            data: 'hey',
            source: {
              port: ':iip',
            },
            target: {
              id: 'Proc2',
              port: 'in',
              action: 'Delete',
            },
          },
        ])
      })
    })
  })

  describe('With a graph sourcing actions', () => {
    let flow: FlowDefinition

    const parser: FBPParser = createParser()

    // ok this also indicate source should store the action also.
    // so an action also has it's own unique outports.
    const fbpData =
      'Proc(proc), Proc2(proc)\nProc2 out -> in Proc::my_action\nProc::my_action out -> msg Log(log)\n'

    describe('the generated graph', () => {
      flow = parser.parse(fbpData)

      it('should contain two nodes', () => {
        expect(flow.nodes).to.eql([
          {
            id: 'Proc',
            name: 'proc',
            title: 'Proc',
          },
          {
            id: 'Proc2',
            name: 'proc',
            title: 'Proc2',
          },
          {
            id: 'Log',
            name: 'log',
            title: 'Log',
          },
        ])
      })

      it('should contain connections with actions', () => {
        expect(flow.links).to.eql([
          {
            metadata: {
              title: 'Proc2 out -> in Proc::my_action',
            },
            source: {
              id: 'Proc2',
              port: 'out',
            },
            target: {
              action: 'my_action',
              id: 'Proc',
              port: 'in',
            },
          },
          {
            metadata: {
              title: 'Proc::my_action out -> msg Log',
            },
            source: {
              action: 'my_action',
              id: 'Proc',
              port: 'out',
            },
            target: {
              id: 'Log',
              port: 'msg',
            },
          },
        ])
      })
    })
  })

  describe('With a graph targeting actions', () => {
    const parser: FBPParser = createParser()

    // ok this also indicate source should store the action also.
    // so an action also has it's own unique outports.
    const fbpData =
      '"bla" -> in Proc::my_action(proc) out -> in Proc2::other_action(proc2)\n'

    describe('the generated graph', () => {
      const flow: FlowDefinition = parser.parse(fbpData)

      it('should contain one node', () => {
        expect(flow.nodes[0]).to.eql({
          id: 'Proc',
          name: 'proc',
          title: 'Proc',
        })
      })

      it('should contain an IIP', () => {
        const iips = parser.getIIPs()
        expect(iips).to.be.an('array')
        checkIIPs(iips, [
          {
            data: 'bla',
            source: {
              port: ':iip',
            },
            target: {
              action: 'my_action',
              id: 'Proc',
              port: 'in',
            },
          },
        ])
      })

      it('should contain connections with actions', () => {
        expect(flow.links).to.eql([
          {
            metadata: {
              title: 'Proc::my_action out -> in Proc2::other_action',
            },
            source: {
              action: 'my_action',
              id: 'Proc',
              port: 'out',
            },
            target: {
              action: 'other_action',
              id: 'Proc2',
              port: 'in',
            },
          },
        ])
      })
    })
  })

  describe('With another graph targeting actions', () => {
    const parser: FBPParser = createParser()

    // ok this also indicate source should store the action also.
    // so an action also has it's own unique outports.
    const fbpData =
      "provider ./{name}/{ns}.fbp as bla\n Test(bla:testing/test)\n Lower1(string/toLowerCase), Lower2(string/toLowerCase)\n 'file1' -> in Lower1 out -> file Test::Delete \n 'file2' -> in Lower2 out -> file Test::Create"

    describe('the generated graph', () => {
      const flow: FlowDefinition = parser.parse(fbpData)

      it('should contain three nodes', () => {
        expect(flow.nodes).to.eql([
          {
            id: 'Test',
            name: 'test',
            ns: 'testing',
            provider: 'bla',
            title: 'Test',
          },
          {
            id: 'Lower1',
            name: 'toLowerCase',
            ns: 'string',
            title: 'Lower1',
          },
          {
            id: 'Lower2',
            name: 'toLowerCase',
            ns: 'string',
            title: 'Lower2',
          },
        ])
      })

      it('should contain two IIPs', () => {
        const iips = parser.getIIPs()
        expect(iips).to.be.an('array')
        checkIIPs(iips, [
          {
            data: 'file1',
            source: {
              port: ':iip',
            },
            target: {
              id: 'Lower1',
              port: 'in',
            },
          },
          {
            data: 'file2',
            source: {
              port: ':iip',
            },
            target: {
              id: 'Lower2',
              port: 'in',
            },
          },
        ])
      })

      it('should contain two connections with actions', () => {
        expect(flow.links).to.eql([
          {
            metadata: {
              title: 'Lower1 out -> file Test::Delete',
            },
            source: {
              id: 'Lower1',
              port: 'out',
            },
            target: {
              action: 'Delete',
              id: 'Test',
              port: 'file',
            },
          },
          {
            metadata: {
              title: 'Lower2 out -> file Test::Create',
            },
            source: {
              id: 'Lower2',
              port: 'out',
            },
            target: {
              action: 'Create',
              id: 'Test',
              port: 'file',
            },
          },
        ])
      })
    })
  })
})
