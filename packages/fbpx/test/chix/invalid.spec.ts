import {expect} from 'chai'
import {FBPParser} from '@chix/fbpx'
import {createParser} from '../helper'

describe('with an invalid FBP string', () => {
  const fbpData = "'foo' -> Display(Output)"

  it('should fail with an Exception', () => {
    expect(() => {
      const parser: FBPParser = createParser()
      parser.parse(fbpData)
    }).to.throw(Error)
  })
})
