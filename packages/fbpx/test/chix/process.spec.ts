import {Flow as FlowDefinition} from '@chix/common'
import {expect} from 'chai'
import {FBPParser} from '@chix/fbpx'
import {createParser} from '../helper'

describe('Process', () => {
  describe('allow a process to be defined without connecting it', () => {
    let flow: FlowDefinition
    const fbpData = 'SomeNode(some/node)'
    it('should produce a graph JSON object', () => {
      const parser: FBPParser = createParser()
      flow = parser.parse(fbpData)
      expect(flow).to.be.an('object')
    })
    describe('the generated graph', () => {
      it('should contain one node', () => {
        expect(flow.nodes).to.eql([
          {
            id: 'SomeNode',
            name: 'node',
            ns: 'some',
            title: 'SomeNode',
          },
        ])
      })
      it('should not contain a connection', () => {
        expect(flow.links).to.be.an('array')
        expect(flow.links.length).to.equal(0)
      })
    })
  })
})
