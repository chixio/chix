// tslint:disable only-arrow-functions object-literal-sort-keys no-implicit-dependencies
import {Flow as FlowDefinition} from '@chix/common'
import * as chai from 'chai'
import {createParser} from '../helper'

describe('With a graph containing masks', () => {
  let flow: FlowDefinition

  const parser = createParser()
  const fbpData =
    '\nMyProc(my/proc), Proc2(my/proc)\nMyProc out -({\n"a": "my.prop"})> in Proc2'

  describe('the generated graph', () => {
    flow = parser.parse(fbpData)

    it('should contain two nodes', () => {
      chai.expect(flow.nodes).to.eql([
        {
          id: 'MyProc',
          name: 'proc',
          ns: 'my',
          title: 'MyProc',
        },
        {
          id: 'Proc2',
          name: 'proc',
          ns: 'my',
          title: 'Proc2',
        },
      ])
    })

    it('should contain a link with mask', () => {
      chai.expect(flow.links[0]).to.eql({
        metadata: {
          title: 'MyProc out -> in Proc2',
        },
        source: {
          id: 'MyProc',
          port: 'out',
        },
        target: {
          id: 'Proc2',
          port: 'in',
          setting: {
            mask: {
              a: 'my.prop',
            },
          },
        },
      })
    })
  })
})
describe('With a graph containing a pointer path', () => {
  let flow: FlowDefinition

  const parser = createParser()
  const fbpData =
    '\nMyProc(my/proc), Proc2(my/proc)\n{"title": "test"} -> in MyProc out -(/title)> in Proc2'

  describe('the generated graph', () => {
    flow = parser.parse(fbpData)

    it('should contain two nodes', () => {
      chai.expect(flow.nodes).to.eql([
        {
          id: 'MyProc',
          title: 'MyProc',
          ns: 'my',
          name: 'proc',
        },
        {
          id: 'Proc2',
          title: 'Proc2',
          ns: 'my',
          name: 'proc',
        },
      ])
    })

    it('should contain a link with mask', () => {
      chai.expect(flow.links[0]).to.eql({
        metadata: {
          title: 'MyProc out -> in Proc2',
        },
        source: {
          id: 'MyProc',
          port: 'out',
        },
        target: {
          id: 'Proc2',
          port: 'in',
          setting: {
            mask: '/title',
          },
        },
      })
    })
  })
})

describe('With a slightly different graph containing masks', () => {
  let flow: FlowDefinition

  const parser = createParser()
  const fbpData =
    '\nMyProc(my/proc)\n Proc2(my/proc)\n {"title": "my title"} -> in MyProc out -({\n  "a": "my.prop"\n})> in Proc2'

  describe('the generated graph', () => {
    flow = parser.parse(fbpData)

    it('should contain two nodes', () => {
      chai.expect(flow.nodes).to.eql([
        {
          id: 'MyProc',
          title: 'MyProc',
          ns: 'my',
          name: 'proc',
        },
        {
          id: 'Proc2',
          title: 'Proc2',
          ns: 'my',
          name: 'proc',
        },
      ])
    })

    it('should contain a link with mask', () => {
      chai.expect(flow.links[0]).to.eql({
        metadata: {
          title: 'MyProc out -> in Proc2',
        },
        source: {
          id: 'MyProc',
          port: 'out',
        },
        target: {
          id: 'Proc2',
          port: 'in',
          setting: {
            mask: {
              a: 'my.prop',
            },
          },
        },
      })
    })
  })
})
