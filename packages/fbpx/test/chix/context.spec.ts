import {Flow as FlowDefinition} from '@chix/common'
import {expect} from 'chai'
import {FBPParser} from '@chix/fbpx'
import {createParser} from '../helper'

describe('Context', () => {
  describe('can define context', () => {
    let flow: FlowDefinition
    const fbpData = "'data' -> @IN SomeNode(some/node)"
    it('should produce a graph JSON object', () => {
      const parser: FBPParser = createParser()
      flow = parser.parse(fbpData)
      expect(flow).to.be.an('object')
    })
    describe('the generated graph', () => {
      it('node should have context', () => {
        expect(flow.nodes).to.eql([
          {
            context: {
              IN: 'data',
            },
            id: 'SomeNode',
            name: 'node',
            ns: 'some',
            title: 'SomeNode',
          },
        ])
      })
    })
    describe('the generated graph has context', () => {
      it('should not contain any connections', () => {
        expect(flow.links).to.eql([])
      })
    })
  })

  describe('can a process and then define context', () => {
    const fbpData = "SomeNode(some/node)\n 'data' -> @IN SomeNode()"
    let flow: FlowDefinition
    it('should produce a graph JSON object', () => {
      const parser: FBPParser = createParser()
      flow = parser.parse(fbpData)
      expect(flow).to.be.an('object')
    })
    describe('the generated graph', () => {
      it('node should have context', () => {
        expect(flow.nodes).to.eql([
          {
            context: {
              IN: 'data',
            },
            id: 'SomeNode',
            name: 'node',
            ns: 'some',
            title: 'SomeNode',
          },
        ])
      })
    })
    describe('the generated graph has context', () => {
      it('should not contain any connections', () => {
        expect(flow.links).to.eql([])
      })
    })
  })
})
