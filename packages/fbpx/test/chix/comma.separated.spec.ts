import {Flow as FlowDefinition} from '@chix/common'
import {expect} from 'chai'
import {FBPParser} from '@chix/fbpx'
import {createParser} from '../helper'
import {checkIIPs} from './helper'

describe('with commas to separate statements', () => {
  const fbpData =
    "'Hello' -> IN Foo(Component), 'World' -> IN Bar(OtherComponent), Foo OUT -> DATA Bar"
  let flow: FlowDefinition
  let parser: FBPParser
  it('should produce a graph JSON object', () => {
    parser = createParser()
    flow = parser.parse(fbpData)
    expect(flow).to.be.an('object')
  })
  describe('the generated graph', () => {
    it('should contain two nodes', () => {
      expect(flow.nodes).to.eql([
        {
          id: 'Foo',
          name: 'Component',
          title: 'Foo',
        },
        {
          id: 'Bar',
          name: 'OtherComponent',
          title: 'Bar',
        },
      ])
    })

    it('should contain one edge', () => {
      expect(flow.links).to.eql([
        {
          metadata: {
            title: 'Foo OUT -> DATA Bar',
          },
          source: {
            id: 'Foo',
            port: 'OUT',
          },
          target: {
            id: 'Bar',
            port: 'DATA',
          },
        },
      ])
    })

    it('should contain two IIPs', () => {
      checkIIPs(parser.getIIPs(), [
        {
          data: 'Hello',
          source: {
            port: ':iip',
          },
          target: {
            id: 'Foo',
            port: 'IN',
          },
        },
        {
          data: 'World',
          source: {
            port: ':iip',
          },
          target: {
            id: 'Bar',
            port: 'IN',
          },
        },
      ])
    })
  })
})
