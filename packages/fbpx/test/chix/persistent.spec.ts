import {Flow as FlowDefinition} from '@chix/common'
import {expect} from 'chai'
import {FBPParser} from '@chix/fbpx'
import {createParser} from '../helper'

describe('ConnectionSettings', () => {
  describe('a connection can define itself as persistent', () => {
    let flow: FlowDefinition
    const fbpData = 'Test(my/test) out -> ^IN SomeNode(some/node)'

    it('should produce a graph JSON object', () => {
      const parser: FBPParser = createParser()
      flow = parser.parse(fbpData)
      expect(flow).to.be.an('object')
    })

    describe('the generated graph', () => {
      it('should contain a persistent connection', () => {
        expect(flow.links).to.eql([
          {
            metadata: {
              title: 'Test out -> IN SomeNode',
            },
            source: {
              id: 'Test',
              port: 'out',
            },
            target: {
              id: 'SomeNode',
              port: 'IN',
              setting: {
                persist: true,
              },
            },
          },
        ])
      })
    })
  })

  describe('a connection can define itself as persistent and cyclic', () => {
    let flow: FlowDefinition
    const fbpData = 'Test(my/test) out => ^IN SomeNode(some/node)'

    it('should produce a graph JSON object', () => {
      const parser: FBPParser = createParser()
      flow = parser.parse(fbpData)

      expect(flow).to.be.an('object')
    })

    describe('the generated graph', () => {
      it('should contain a persistent cyclic connection', () => {
        expect(flow.links).to.eql([
          {
            metadata: {
              title: 'Test out -> IN SomeNode',
            },
            source: {
              id: 'Test',
              port: 'out',
            },
            target: {
              id: 'SomeNode',
              port: 'IN',
              setting: {
                cyclic: true,
                persist: true,
              },
            },
          },
        ])
      })
    })
  })

  describe('a connection can define itself as cyclic and a connection can collect', () => {
    let flow: FlowDefinition
    const fbpData =
      'Test(my/test) out => IN SomeNode(some/node) out >= in Collect(collect/it)'

    it('should produce a graph JSON object', () => {
      const parser: FBPParser = createParser()

      flow = parser.parse(fbpData)
      expect(flow).to.be.an('object')
    })

    describe('the generated graph', () => {
      it('should contain a cyclic and a collect connection', () => {
        expect(flow.links).to.eql([
          {
            metadata: {
              title: 'Test out -> IN SomeNode',
            },
            source: {
              id: 'Test',
              port: 'out',
            },
            target: {
              id: 'SomeNode',
              port: 'IN',
              setting: {
                cyclic: true,
              },
            },
          },
          {
            metadata: {
              title: 'SomeNode out -> in Collect',
            },
            source: {
              id: 'SomeNode',
              port: 'out',
              setting: {
                collect: true,
              },
            },
            target: {
              id: 'Collect',
              port: 'in',
            },
          },
        ])
      })
    })
  })
})
