import {Flow as FlowDefinition} from '@chix/common'
import {expect} from 'chai'
import {FBPParser} from '@chix/fbpx'
import {createParser} from '../helper'
import {checkIIPs} from './helper'

describe('with underscores and numbers in ports, nodes, and components', () => {
  let flow: FlowDefinition
  let parser: FBPParser
  const fbpData = "'Hello 09' -> IN_2 Foo_Node_42(Component_15)"
  it('should produce a graph JSON object', () => {
    parser = createParser()
    flow = parser.parse(fbpData)
    expect(flow).to.be.an('object')
  })
  describe('the generated graph', () => {
    it('should contain one node', () => {
      expect(flow.nodes).to.eql([
        {
          id: 'Foo_Node_42',
          title: 'Foo_Node_42',
          name: 'Component_15',
        },
      ])
    })
    it('should contain an IIP', () => {
      expect(flow.links).to.be.an('array')
      expect(flow.links.length).to.equal(0)
      checkIIPs((parser.getRenderer() as any).getIIPs(), [
        {
          data: 'Hello 09',
          source: {
            port: ':iip',
          },
          target: {
            id: 'Foo_Node_42',
            port: 'IN_2',
          },
        },
      ])
    })
  })
})
