import {Flow as FlowDefinition} from '@chix/common'
import {expect} from 'chai'
import {FBPParser} from '@chix/fbpx'
import {createParser} from '../../helper'
import {checkIIPs} from '../helper'

describe('JSON Array', () => {
  describe('With a graph containing JSON Arrays as input', () => {
    const parser: FBPParser = createParser()
    const fbpData =
      '[ 1\n,\n2,\n\n3, { "bla": "di"\n} ] -> in Consume(consume)\n'

    describe('the generated graph', () => {
      const flow: FlowDefinition = parser.parse(fbpData)

      it('should contain one node', () => {
        expect(flow.nodes[0]).to.eql({
          id: 'Consume',
          name: 'consume',
          title: 'Consume',
        })
      })

      it('should contain an IIP', () => {
        const iips = parser.getIIPs()
        expect(iips).to.be.an('array')
        checkIIPs(iips, [
          {
            data: [1, 2, 3, {bla: 'di'}],
            source: {
              port: ':iip',
            },
            target: {
              id: 'Consume',
              port: 'in',
            },
          },
        ])
      })
    })
  })
})
