import {Flow as FlowDefinition} from '@chix/common'
import {expect} from 'chai'
import {FBPParser} from '@chix/fbpx'
import {createParser} from '../../helper'
import {checkIIPs} from '../helper'

describe('with three-statement FBP string', () => {
  const parser: FBPParser = createParser()
  const fbpData =
    "'somefile.txt' -> SOURCE Read(ReadFile) OUT -> IN Display(Output)"
  const flow: FlowDefinition = parser.parse(fbpData)

  it('should produce a graph JSON object', () => {
    expect(flow).to.be.an('object')
  })

  describe('the generated graph', () => {
    it('should contain two nodes and data', () => {
      expect(flow.nodes).to.eql([
        {
          id: 'Read',
          name: 'ReadFile',
          title: 'Read',
        },
        {
          id: 'Display',
          name: 'Output',
          title: 'Display',
        },
      ])
    })

    it('should contain an edge', () => {
      expect(flow.links).to.be.an('array')
      expect(flow.links.length).to.equal(1)
    })

    it('should contain an IIP', () => {
      expect(flow.links).to.be.an('array')

      checkIIPs(parser.getIIPs(), [
        {
          data: 'somefile.txt',
          source: {
            port: ':iip',
          },
          target: {
            id: 'Read',
            port: 'SOURCE',
          },
        },
      ])
    })
  })
})
