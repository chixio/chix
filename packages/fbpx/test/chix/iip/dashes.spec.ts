import {Flow as FlowDefinition} from '@chix/common'
import {expect} from 'chai'
import {FBPParser} from '@chix/fbpx'
import {createParser} from '../../helper'
import {checkIIPs} from '../helper'

describe('with a component that contains dashes in name', () => {
  const fbpData = "'somefile' -> SOURCE Read(my-cool-component/ReadFile)"

  const parser: FBPParser = createParser()
  const flow: FlowDefinition = parser.parse(fbpData)

  it('should produce a graph JSON object', () => {
    expect(flow).to.be.an('object')
  })

  describe('the generated graph', () => {
    it('should contain one node', () => {
      expect(flow.nodes).to.eql([
        {
          id: 'Read',
          name: 'ReadFile',
          ns: 'my-cool-component',
          title: 'Read',
        },
      ])
    })

    it('should contain an IIP', () => {
      expect(parser.getIIPs()).to.be.an('array')
      checkIIPs(parser.getRenderer().getIIPs(), [
        {
          data: 'somefile',
          source: {
            port: ':iip',
          },
          target: {
            id: 'Read',
            port: 'SOURCE',
          },
        },
      ])
    })
  })
})
