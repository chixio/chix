import {Flow as FlowDefinition} from '@chix/common'
import {expect} from 'chai'
import {FBPParser} from '@chix/fbpx'
import {createParser} from '../../helper'
import {checkIIPs} from '../helper'

describe('with numeric, float and boolean ips', () => {
  let flow: FlowDefinition
  let parser: FBPParser

  const fbpData = ['0.0091 -> in Foo()', '-0.0023 -> in Bar()'].join(',')

  it('should produce a graph JSON object', () => {
    parser = createParser()
    flow = parser.parse(fbpData)
    expect(flow).to.be.an('object')
  })

  describe('the generated graph', () => {
    it('should contain an IIP', () => {
      expect(flow.links).to.be.an('array')
      expect(flow.links.length).to.equal(0)
      checkIIPs((parser.getRenderer() as any).getIIPs(), [
        {
          data: 0.0091,
          source: {
            port: ':iip',
          },
          target: {
            id: 'Foo',
            port: 'in',
          },
        },
        {
          data: -0.0023,
          source: {
            port: ':iip',
          },
          target: {
            id: 'Bar',
            port: 'in',
          },
        },
      ])
    })
  })
})
