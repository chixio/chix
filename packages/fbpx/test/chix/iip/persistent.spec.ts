import {Flow as FlowDefinition, IIP as IIPDefinition} from '@chix/common'
import {expect} from 'chai'
import {FBPParser} from '@chix/fbpx'
import {createParser} from '../../helper'

describe('an IIP can define itself as persistent', () => {
  let fbpData: string
  let flow: FlowDefinition
  let iips: IIPDefinition[]

  const parser: FBPParser = createParser()

  fbpData = "'somefile' -> ^SOURCE Read(ReadFile)\n"

  it('should produce a graph JSON object', () => {
    flow = parser.parse(fbpData)
    iips = parser.getRenderer().getIIPs()

    expect(flow).to.be.an('object')
  })

  describe('the generated graph', () => {
    it('should contain one node', () => {
      expect(flow.nodes[0]).to.eql({
        id: 'Read',
        title: 'Read',
        name: 'ReadFile',
      })
    })

    it('should contain an IIP', () => {
      expect(iips).to.be.an('array')

      // ok this seems weird, but no we can send
      // multiple IIPs and even specify which one
      // should persist and also cycle.
      // Imagine being able to send an array to a port
      // and also a single entry.
      // Like a globed array of files + just some
      // single files, the globbed array will define
      // itself as cyclic.
      expect(iips).to.eql([
        {
          source: {
            port: ':iip',
          },
          target: {
            id: 'Read',
            port: 'SOURCE',
            setting: {
              persist: true,
            },
          },
          metadata: {
            title: ' :iip -> SOURCE Read',
          },
          data: 'somefile',
        },
      ])
    })
  })
})
