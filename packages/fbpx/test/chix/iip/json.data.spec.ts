import {Flow as FlowDefinition} from '@chix/common'
import {expect} from 'chai'
import {FBPParser} from '@chix/fbpx'
import {createParser} from '../../helper'

describe('JSON Data', () => {
  describe('With a graph containing JSON Data', () => {
    const parser: FBPParser = createParser()
    const fbpData = [
      '"Length of 2"  ->   @msg AssertLength(assert/equal)',
      '2              -> expect AssertLength',
      '{',
      '"name": "test",',
      '"title": "Test"',
      '} -> in Keys(object/keys) out -> in Length(array/length)',
      'Length out -> in AssertLength',
    ].join('\n')

    describe('the generated graph', () => {
      const flow: FlowDefinition = parser.parse(fbpData)

      it('should contain three nodes', () => {
        expect(flow.nodes[0]).to.eql({
          id: 'AssertLength',
          title: 'AssertLength',
          ns: 'assert',
          name: 'equal',
          context: {
            msg: 'Length of 2',
          },
        })

        expect(flow.nodes[1]).to.eql({
          id: 'Keys',
          name: 'keys',
          ns: 'object',
          title: 'Keys',
        })

        expect(flow.nodes[2]).to.eql({
          id: 'Length',
          name: 'length',
          ns: 'array',
          title: 'Length',
        })
      })

      it('should contain correct links', () => {
        expect(flow.links).to.eql([
          {
            metadata: {title: 'Keys out -> in Length'},
            source: {id: 'Keys', port: 'out'},
            target: {id: 'Length', port: 'in'},
          },
          {
            metadata: {title: 'Length out -> in AssertLength'},
            source: {id: 'Length', port: 'out'},
            target: {id: 'AssertLength', port: 'in'},
          },
        ])
      })
    })
  })
})
