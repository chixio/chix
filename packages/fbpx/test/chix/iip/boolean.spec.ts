import {Flow as FlowDefinition} from '@chix/common'
import {expect} from 'chai'
import {FBPParser} from '@chix/fbpx'
import {createParser} from '../../helper'
import {checkIIPs} from '../helper'

describe('with numeric, float and boolean ips', () => {
  let flow: FlowDefinition
  let parser: FBPParser
  const fbpData = ['true -> IN Bar(Bar)', 'false -> IN Foo()'].join(',')

  it('should produce a graph JSON object', () => {
    parser = createParser()
    flow = parser.parse(fbpData)
    expect(flow).to.be.an('object')
  })

  describe('the generated graph', () => {
    it('should contain an IIP', () => {
      expect(flow.links).to.be.an('array')
      expect(flow.links.length).to.equal(0)
      checkIIPs((parser.getRenderer() as any).getIIPs(), [
        {
          data: true,
          source: {
            port: ':iip',
          },
          target: {
            id: 'Bar',
            port: 'IN',
          },
        },
        {
          data: false,
          source: {
            port: ':iip',
          },
          target: {
            id: 'Foo',
            port: 'IN',
          },
        },
      ])
    })
  })
})
