import {Flow as FlowDefinition} from '@chix/common'
import {expect} from 'chai'
import {FBPParser} from '@chix/fbpx'
import {createParser} from '../../helper'
import {checkIIPs} from '../helper'

describe('String', () => {
  describe('Sending iips to different nodes using the same component', () => {
    const parser: FBPParser = createParser()
    const fbpData =
      "'somefile' -> SOURCE Read(ReadFile), 'file2' -> SOURCE Read2(ReadFile)\n"

    describe('the generated graph', () => {
      const flow: FlowDefinition = parser.parse(fbpData)

      it('should contain two nodes', () => {
        expect(flow.nodes[0]).to.eql({
          id: 'Read',
          name: 'ReadFile',
          title: 'Read',
        })

        expect(flow.nodes[1]).to.eql({
          id: 'Read2',
          name: 'ReadFile',
          title: 'Read2',
        })
      })

      it('should contain two IIPs', () => {
        const iips = parser.getIIPs()

        expect(iips).to.be.an('array')

        const _expect = [
          {
            data: 'somefile',
            source: {
              port: ':iip',
            },
            target: {
              id: 'Read',
              port: 'SOURCE',
            },
          },
          {
            data: 'file2',
            source: {
              port: ':iip',
            },
            target: {
              id: 'Read2',
              port: 'SOURCE',
            },
          },
        ]

        checkIIPs(iips, _expect)
      })
    })
  })

  describe('with FBP string containing an IIP with whitespace', () => {
    const fbpData = "'foo Bar BAZ' -> IN Display(Output)"
    const parser: FBPParser = createParser()
    const flow: FlowDefinition = parser.parse(fbpData)

    it('should produce a graph JSON object', () => {
      expect(flow).to.be.an('object')
    })

    describe('the generated graph', () => {
      it('should contain a node', () => {
        expect(flow.nodes).to.eql([
          {id: 'Display', title: 'Display', name: 'Output'},
        ])
      })

      it('should contain an IIP', () => {
        expect(flow.links).to.be.an('array')
        expect(flow.links.length).to.equal(0)
        checkIIPs(parser.getRenderer().getIIPs(), [
          {
            data: 'foo Bar BAZ',
            source: {
              port: ':iip',
            },
            target: {
              id: 'Display',
              port: 'IN',
            },
          },
        ])
      })
    })
  })

  describe('with FBP string containing an empty IIP string', () => {
    const fbpData = "'' -> IN Display(Output)"
    const parser: FBPParser = createParser()
    const flow: FlowDefinition = parser.parse(fbpData)

    it('should produce a graph JSON object', () => {
      expect(flow).to.be.an('object')
    })

    describe('the generated graph', () => {
      it('should contain a node', () => {
        expect(flow.nodes).to.eql([
          {
            id: 'Display',
            name: 'Output',
            title: 'Display',
          },
        ])
      })

      it('should contain an IIP', () => {
        expect(flow.links).to.be.an('array')
        expect(flow.links.length).to.equal(0)
        checkIIPs(parser.getRenderer().getIIPs(), [
          {
            data: '',
            source: {
              port: ':iip',
            },
            target: {
              id: 'Display',
              port: 'IN',
            },
          },
        ])
      })
    })
  })

  describe('with FBP string containing URL as IIP', () => {
    const fbpData =
      "'http://localhost:5984/default' -> URL Conn(couchdb/OpenDatabase)"
    const parser: FBPParser = createParser()
    const flow: FlowDefinition = parser.parse(fbpData)

    it('should produce a graph JSON object', () => {
      expect(flow).to.be.an('object')
    })

    describe('the generated graph', () => {
      it('should contain a node', () => {
        expect(flow.nodes).to.eql([
          {
            id: 'Conn',
            name: 'OpenDatabase',
            ns: 'couchdb',
            title: 'Conn',
          },
        ])
      })

      it('should contain an IIP', () => {
        expect(flow.links).to.be.an('array')
        expect(flow.links.length).to.equal(0)
        checkIIPs(parser.getRenderer().getIIPs(), [
          {
            data: 'http://localhost:5984/default',
            source: {
              port: ':iip',
            },
            target: {
              id: 'Conn',
              port: 'URL',
            },
          },
        ])
      })
    })
  })

  describe('with FBP string containing RegExp as IIP', () => {
    const fbpData = [
      "'_id=(d+.d+.d*)=http://iks-project.eu/%deliverable/$1' -> REGEXP MapDeliverableUri(MapPropertyValue)",
      "'path=/_(?!(includes|layouts)' -> REGEXP MapDeliverableUri(MapPropertyValue)",
      "'@type=deliverable' -> PROPERTY SetDeliverableProps(SetProperty)",
      "'#foo' -> SELECTOR Get(dom/GetElement)",
      "'Hi, {{ name }}' -> TEMPLATE Get",
    ].join('\n')

    const parser: FBPParser = createParser()
    const flow: FlowDefinition = parser.parse(fbpData)

    it('should produce a graph JSON object', () => {
      expect(flow).to.be.an('object')
    })

    describe('the generated graph', () => {
      it('should contain two nodes', () => {
        expect(flow.nodes).to.eql([
          {
            id: 'MapDeliverableUri',
            name: 'MapPropertyValue',
            title: 'MapDeliverableUri',
          },
          {
            id: 'SetDeliverableProps',
            name: 'SetProperty',
            title: 'SetDeliverableProps',
          },
          {
            id: 'Get',
            name: 'GetElement',
            ns: 'dom',
            title: 'Get',
          },
        ])
      })

      it('should contain IIPs', () => {
        expect(flow.links).to.be.an('array')
        expect(flow.links.length).to.equal(0)
        checkIIPs(parser.getRenderer().getIIPs(), [
          {
            data: '_id=(d+.d+.d*)=http://iks-project.eu/%deliverable/$1',
            source: {
              port: ':iip',
            },
            target: {
              id: 'MapDeliverableUri',
              port: 'REGEXP',
            },
          },
          {
            data: 'path=/_(?!(includes|layouts)',
            source: {
              port: ':iip',
            },
            target: {
              id: 'MapDeliverableUri',
              port: 'REGEXP',
            },
          },
          {
            data: '@type=deliverable',
            source: {
              port: ':iip',
            },
            target: {
              id: 'SetDeliverableProps',
              port: 'PROPERTY',
            },
          },
          {
            data: '#foo',
            source: {
              port: ':iip',
            },
            target: {
              id: 'Get',
              port: 'SELECTOR',
            },
          },
          {
            data: 'Hi, {{ name }}',
            source: {
              port: ':iip',
            },
            target: {
              id: 'Get',
              port: 'TEMPLATE',
            },
          },
        ])
      })
    })
  })
})
