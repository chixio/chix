import {Flow as FlowDefinition} from '@chix/common'
import {expect} from 'chai'
import {FBPParser} from '@chix/fbpx'
import {createParser} from '../../helper'
import {checkIIPs} from '../helper'

describe('with simple FBP string', () => {
  const parser: FBPParser = createParser()
  const fbpData = "'somefile' -> SOURCE Read(ReadFile)\n'"

  it('should provide a parse method', () => {
    expect(parser.parse).to.be.a('function')
  })

  describe('the generated graph', () => {
    const flow: FlowDefinition = parser.parse(fbpData)

    it('should contain one node', () => {
      expect(flow.nodes[0]).to.eql({
        id: 'Read',
        name: 'ReadFile',
        title: 'Read',
      })
    })

    it('should contain an IIP', () => {
      const iips = parser.getIIPs()

      expect(iips).to.be.an('array')

      const _expect = [
        {
          data: 'somefile',
          source: {
            port: ':iip',
          },
          target: {
            id: 'Read',
            port: 'SOURCE',
          },
        },
      ]

      checkIIPs(iips, _expect)
    })
  })
})
