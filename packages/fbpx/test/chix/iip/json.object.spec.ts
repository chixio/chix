import {Flow as FlowDefinition} from '@chix/common'
import {expect} from 'chai'
import {FBPParser} from '@chix/fbpx'
import {createParser} from '../../helper'
import {checkIIPs} from '../helper'

describe('JSON Objects', () => {
  describe('With a graph containing JSON Objects as input', () => {
    let flow: FlowDefinition

    const parser: FBPParser = createParser()
    const fbpData = '{ "hi": "huh", "me": [1,2,3] } -> in Consume(consume)\n'

    describe('the generated graph', () => {
      flow = parser.parse(fbpData)

      it('should contain one node', () => {
        expect(flow.nodes[0]).to.eql({
          id: 'Consume',
          name: 'consume',
          title: 'Consume',
        })
      })

      it('should contain an IIP', () => {
        const iips = parser.getIIPs()
        expect(iips).to.be.an('array')
        checkIIPs(iips, [
          {
            data: {
              hi: 'huh',
              me: [1, 2, 3],
            },
            source: {
              port: ':iip',
            },
            target: {
              id: 'Consume',
              port: 'in',
            },
          },
        ])
      })
    })
  })
})
