import {Flow as FlowDefinition} from '@chix/common'
import {expect} from 'chai'
import {FBPParser} from '@chix/fbpx'
import {createParser} from '../helper'
import {checkIIPs} from './helper'

describe('with array index on ports', () => {
  describe('the generated graph', () => {
    let flow: FlowDefinition
    let parser: FBPParser
    before(() => {
      parser = createParser()
      flow = parser.parse(
        "'Hello 09' -> [0] IN_2 Foo_Node_42(Component_15) OUT [9] -> [1] IN Some(Comp)"
      )
    })
    it('should contain two nodes', () => {
      expect(flow.nodes.length).to.eql(2)
    })
    it('the nodes should have an input and output index', () => {
      expect(flow.links).to.eql([
        {
          metadata: {
            title: 'Foo_Node_42 OUT -> IN Some',
          },
          source: {
            id: 'Foo_Node_42',
            port: 'OUT',
            setting: {
              index: 9,
            },
          },
          target: {
            id: 'Some',
            port: 'IN',
            setting: {
              index: 1,
            },
          },
        },
      ])
    })
    it('should contain an IIP with index', () => {
      expect(flow.links).to.be.an('array')
      checkIIPs((parser.getRenderer() as any).getIIPs(), [
        {
          data: 'Hello 09',
          source: {
            port: ':iip',
          },
          target: {
            id: 'Foo_Node_42',
            port: 'IN_2',
            setting: {
              index: 0,
            },
          },
        },
      ])
    })
  })

  describe('both data and connections can define an index', () => {
    const parser: FBPParser = createParser()
    const flow: FlowDefinition = parser.parse(
      "'./graphs' -> [0] in Join(path/join)\nGlob(fs/glob) match -> [1] in Join"
    )

    it('should produce a graph JSON object', () => {
      expect(flow).to.be.an('object')
    })

    it('should contain an IIP with index information', () => {
      checkIIPs(parser.getIIPs(), [
        {
          data: './graphs',
          source: {
            port: ':iip',
          },
          target: {
            id: 'Join',
            port: 'in',
            setting: {
              index: 0,
            },
          },
        },
      ])
    })

    it('should contain 2 nodes', () => {
      expect(flow.nodes).to.eql([
        {
          id: 'Join',
          name: 'join',
          ns: 'path',
          title: 'Join',
        },
        {
          id: 'Glob',
          name: 'glob',
          ns: 'fs',
          title: 'Glob',
        },
      ])
    })

    it('should contain a link with index information', () => {
      expect(flow.links).to.eql([
        {
          metadata: {
            title: 'Glob match -> in Join',
          },
          source: {
            id: 'Glob',
            port: 'match',
          },
          target: {
            id: 'Join',
            port: 'in',
            setting: {
              index: 1,
            },
          },
        },
      ])
    })
  })

  describe('both data and connections can define an object index', () => {
    const parser: FBPParser = createParser()
    const flow: FlowDefinition = parser.parse(
      "'./graphs' -> [title] in Join(path/join)\nGlob(fs/glob) match [/test] -> [prop] in Join"
    )

    it('should produce a graph JSON object', () => {
      expect(flow).to.be.an('object')
    })

    it('should contain an IIP with index information', () => {
      checkIIPs(parser.getIIPs(), [
        {
          data: './graphs',
          source: {
            port: ':iip',
          },
          target: {
            id: 'Join',
            port: 'in',
            setting: {
              index: 'title',
            },
          },
        },
      ])
    })

    it('should contain 2 nodes', () => {
      expect(flow.nodes).to.eql([
        {
          id: 'Join',
          name: 'join',
          ns: 'path',
          title: 'Join',
        },
        {
          id: 'Glob',
          name: 'glob',
          ns: 'fs',
          title: 'Glob',
        },
      ])
    })

    it('should contain a link with index information', () => {
      expect(flow.links).to.eql([
        {
          metadata: {
            title: 'Glob match -> in Join',
          },
          source: {
            id: 'Glob',
            port: 'match',
            setting: {
              index: '/test',
            },
          },
          target: {
            id: 'Join',
            port: 'in',
            setting: {
              index: 'prop',
            },
          },
        },
      ])
    })
  })
})
