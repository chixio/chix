import {Flow as FlowDefinition} from '@chix/common'
import {expect} from 'chai'
import {FBPParser} from '@chix/fbpx'
import {createParser} from '../helper'

describe('a connection can define itself as being cyclic', () => {
  let flow: FlowDefinition
  const fbpData = 'Test(my/test) out => IN SomeNode(some/node)'

  it('should produce a graph JSON object', () => {
    const parser: FBPParser = createParser()
    flow = parser.parse(fbpData)
    expect(flow).to.be.an('object')
  })

  describe('the generated graph', () => {
    it('should contain a cyclic connection', () => {
      expect(flow.links).to.eql([
        {
          metadata: {
            title: 'Test out -> IN SomeNode',
          },
          source: {
            id: 'Test',
            port: 'out',
          },
          target: {
            id: 'SomeNode',
            port: 'IN',
            setting: {
              cyclic: true,
            },
          },
        },
      ])
    })
  })
})
