import {Flow as FlowDefinition} from '@chix/common'
import {expect} from 'chai'
import {FBPParser} from '@chix/fbpx'
import {createParser} from '../helper'
import {checkIIPs} from './helper'

describe('with FBP string containing comments', () => {
  const fbpData =
    "# Do stuff\n'foo bar' -> IN Display(Output) # Here we show the string"
  const parser: FBPParser = createParser()
  const flow: FlowDefinition = parser.parse(fbpData)

  it('should produce a graph JSON object', () => {
    expect(flow).to.be.an('object')
  })

  describe('the generated graph', () => {
    it('should contain a node', () => {
      expect(flow.nodes).to.eql([
        {
          id: 'Display',
          name: 'Output',
          title: 'Display',
        },
      ])
    })

    it('should contain an IIP', () => {
      expect(flow.links).to.be.an('array')
      expect(flow.links.length).to.equal(0)
      checkIIPs(parser.getRenderer().getIIPs(), [
        {
          data: 'foo bar',
          source: {
            port: ':iip',
          },
          target: {
            id: 'Display',
            port: 'IN',
          },
        },
      ])
    })
  })
})
