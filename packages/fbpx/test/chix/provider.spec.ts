// tslint:disable only-arrow-functions object-literal-sort-keys no-implicit-dependencies
import {expect} from 'chai'
import {ChixParser as Parser} from '@chix/fbpx'
import {loadFixture} from '../helper'

const DEFAULT_PROVIDER = 'https://api.chix.io/nodes/{ns}/{name}'

describe('Loading a graph', () => {
  const fbp_namespaced = loadFixture('namespaced.fbp')
  const fbp_graph = loadFixture('graph.fbp')

  it('By default has a provider set', () => {
    const parser = Parser({
      skipIDs: true,
    })

    const json = parser.parse(fbp_graph)
    expect(json).to.eql({
      title: 'My Graph',
      name: 'myGraph',
      ns: 'test',
      type: 'flow',
      nodes: [
        {
          id: 'Log',
          name: 'log',
          ns: 'console',
          title: 'Log',
        },
        {
          id: 'Repeat',
          ns: 'test',
          name: 'repeat',
          title: 'Repeat',
        },
      ],
      links: [
        {
          metadata: {
            title: 'Repeat out -> msg Log',
          },
          source: {
            id: 'Repeat',
            port: 'out',
          },
          target: {
            id: 'Log',
            port: 'msg',
          },
        },
      ],
      providers: {
        '@': {
          url: DEFAULT_PROVIDER,
        },
      },
    })
  })

  it('Custom default provider if one is set', () => {
    const parser = Parser({
      skipIDs: true,
      defaultProvider: 'https://localhost/nodes/{ns}/{name}',
    })

    const json = parser.parse(fbp_graph)
    expect(json).to.eql({
      title: 'My Graph',
      ns: 'test',
      name: 'myGraph',
      type: 'flow',
      nodes: [
        {
          id: 'Log',
          name: 'log',
          ns: 'console',
          title: 'Log',
        },
        {
          id: 'Repeat',
          name: 'repeat',
          ns: 'test',
          title: 'Repeat',
        },
      ],
      links: [
        {
          metadata: {
            title: 'Repeat out -> msg Log',
          },
          source: {
            id: 'Repeat',
            port: 'out',
          },
          target: {
            id: 'Log',
            port: 'msg',
          },
        },
      ],
      providers: {
        '@': {
          url: 'https://localhost/nodes/{ns}/{name}',
        },
      },
    })
  })

  it('No provider if defaultProvider is false', () => {
    const parser = Parser({
      skipIDs: true,
      defaultProvider: false,
    })

    const json = parser.parse(fbp_graph)
    expect(json).to.eql({
      name: 'myGraph',
      nodes: [
        {
          id: 'Log',
          name: 'log',
          ns: 'console',
          title: 'Log',
        },
        {
          id: 'Repeat',
          title: 'Repeat',
          ns: 'test',
          name: 'repeat',
        },
      ],
      links: [
        {
          metadata: {
            title: 'Repeat out -> msg Log',
          },
          source: {
            id: 'Repeat',
            port: 'out',
          },
          target: {
            id: 'Log',
            port: 'msg',
          },
        },
      ],
      ns: 'test',
      title: 'My Graph',
      type: 'flow',
    })
  })

  it('Understands provider namespacing', () => {
    const parser = Parser({
      skipIDs: true,
    })

    const json = parser.parse(fbp_namespaced)
    expect(json).to.eql({
      title: 'My Graph',
      ns: 'test',
      name: 'myGraph',
      type: 'flow',
      nodes: [
        {
          id: 'Log',
          title: 'Log',
          ns: 'console',
          name: 'log',
        },
        {
          id: 'Repeat',
          title: 'Repeat',
          ns: 'test',
          name: 'repeat',
          provider: 'x',
        },
      ],
      links: [
        {
          metadata: {
            title: 'Repeat out -> msg Log',
          },
          source: {
            id: 'Repeat',
            port: 'out',
          },
          target: {
            id: 'Log',
            port: 'msg',
          },
        },
      ],
      providers: {
        '@': {
          url: DEFAULT_PROVIDER,
        },
        x: {
          name: 'x',
          url: 'https://localhost/{ns}/{name}',
        },
      },
    })
  })
})
