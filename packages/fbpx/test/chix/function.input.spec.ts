import {Flow as FlowDefinition} from '@chix/common'
import {expect} from 'chai'
import {FBPParser} from '@chix/fbpx'
import {createParser} from '../helper'
import {checkIIPs} from './helper'

describe('With a graph containing a Function as input', () => {
  let flow: FlowDefinition

  const parser: FBPParser = createParser()
  const fbpData =
    '( console.log("test") ) -> in Consume(consume)\n( console.log("default") ) -> @in Consume(consume)'

  describe('the generated graph', () => {
    flow = parser.parse(fbpData)

    it('should contain one node', () => {
      expect(flow.nodes[0]).to.eql({
        context: {
          in: 'console.log("default")',
        },
        id: 'Consume',
        name: 'consume',
        title: 'Consume',
      })
    })

    it('should contain an IIP', () => {
      const iips = parser.getIIPs()
      expect(iips).to.be.an('array')
      // should actually already be a function.
      checkIIPs(iips, [
        {
          data: 'console.log("test")',
          source: {
            port: ':iip',
          },
          target: {
            id: 'Consume',
            port: 'in',
          },
        },
      ])
    })
  })
})
