// tslint:disable only-arrow-functions object-literal-sort-keys no-implicit-dependencies
import * as chai from 'chai'
import {FSResolver} from '@chix/fbpx'
import {createParser, loadFixture} from '../helper'

describe('With a graph containing an include', () => {
  const parser = createParser()

  describe('the generated graph', () => {
    it('should be able to include the the file as string', () => {
      const fbpData = loadFixture('include.fbp')
      parser.addIncludeResolver(new FSResolver())
      const graphData = parser.parse(fbpData)

      chai.expect(graphData.nodes[0]).to.eql({
        id: 'Repeat',
        name: 'repeat',
        ns: 'test',
        title: 'Repeat',
        context: {
          in: '{ "test": "ing" }',
        },
      })
    })

    it('should be able to include the the file as json', () => {
      const fbpData = loadFixture('include!.fbp')
      const graphData = parser.parse(fbpData)

      chai.expect(graphData.nodes[0]).to.eql({
        id: 'Repeat',
        name: 'repeat',
        ns: 'test',
        title: 'Repeat',
        context: {
          in: {
            test: 'ing',
          },
        },
      })
    })
  })
})
