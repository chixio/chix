A description of what happens during token parsing:

Run will first iterate each scope in this.level
and run onLineStart for all scopes.

Scopes can then declare how many tokens are expected for this line.

OnLineStart is like a setup run whenever a new line is processed.

The rootScope will actually jump to LeftHandScope always during this phase.

(Wrong: The jumping is lazy however, so all onLineStarts will first be finished.) it actually goes toScope(RootScope) and starts processing..

...
ToScope:
The scope is tracked.
The Parent of the scope is set.
The structure is of the scope we left is checked
In case there are no tokens yet we skip (this case)
The current scope is set (LeftHandScope)
The scoper char is set (In this case undefined)
The token list for this scope is reset.
We run onEnter() for this scope

This then has effectivly set the Dyslexer's scope to LeftHandScope.
---
Iteration continues.
And online start is called for the remaining scopes.

--
Next the chars of the string we are parsing are simply iterated.
Going from one char to the next.

On each char the dyslexer.next(char) function is called.

That's what run is doing in it's entirety.

Thus, during setup. The RootScope switched onLineStart to LeftHandscope.
During this switch LeftHandScope is made the active Scope and RootScope
was set as it's parent.

Line starts, Always in left handscope.

....

next(charIndex)
Seems like the index is not actually used because we use this.current
which is the same..

this.line is rebuild, so we always know the line segment up to char we are processing.

we take the current scope and make it local to the function.

Next, for this scope we check the rules.
the rules are matching the current char being processed.

As the current char is ' we have a match and we go from leftHandscope
to datascope, lefthandscope will become our parent scope.
It's the lefthand scope's ' rule which decides to switch.

Datascope.

On enter, datascope decides our token ending is the token we entered with.
Which in this case is '

We also set escape to true, this indicates the lexer should check for the \ escape character.

We then continue within the dyslexer in our next() function.
We execute the logic in case the scope has set a tokenEnding.
In this case it did, the tokenEnding was '.

Bit weird scope.tokenEnding is not set to ' but the lexer.tokenEnding is..







