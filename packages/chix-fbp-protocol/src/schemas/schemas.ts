import {fbpProtocolSchemas} from './fbp-protocol'
import {fbpxProtocolSchemas} from './fbpx-protocol'

export const schemas = {
  ...fbpProtocolSchemas,
  graph: {
    ...fbpProtocolSchemas,
    input: {
      ...fbpProtocolSchemas.graph.input,
      ...fbpxProtocolSchemas.graph.input,
    },
    output: {
      ...fbpProtocolSchemas.graph.output,
      ...fbpxProtocolSchemas.graph.output,
    },
  },
  process: {
    ...fbpxProtocolSchemas.process,
  },
  runtime: {
    ...fbpProtocolSchemas.runtime,
    input: {
      ...fbpProtocolSchemas.runtime.input,
      ...fbpxProtocolSchemas.runtime.input,
    },
    output: {
      ...fbpProtocolSchemas.runtime.output,
      ...fbpxProtocolSchemas.runtime.output,
    },
  },
}
