import {FlowSchema, IIPSchema, NodeSchema} from '@chix/common'

export const fbpxProtocolSchemas = {
  graph: {
    input: {
      create: {
        title: 'Create',
        description: 'Create a graph.',
        type: 'object',
        properties: {
          graph: {
            type: 'string',
          },
          iips: {
            type: 'array',
            items: IIPSchema,
          },
          start: {
            type: 'boolean',
            default: false,
          },
          subscribe: {
            type: 'boolean',
            default: true,
          },
          flow: FlowSchema,
        },
      },
      getnode: {
        title: 'Get Node',
        description: 'Retrieve node',
        type: 'object',
        properties: {
          graph: {
            type: 'string',
          },
          node: {
            type: 'string',
          },
        },
      },
      subscribe: {
        title: 'Subscribe',
        description: 'Subscribe to a graph.',
        type: 'object',
        properties: {
          graph: {
            title: 'Graph',
            type: 'string',
            description: 'graph to subscribe to',
            required: true,
          },
        },
      },
      unsubscribe: {
        title: 'Unsubscribe',
        description: 'Unsubscribe to a graph.',
        type: 'object',
        properties: {
          graph: {
            title: 'Graph',
            type: 'string',
            description: 'graph to unsubscribe from',
            required: true,
          },
        },
      },
    },
    output: {
      create: {
        title: 'Create',
        description: 'Created a graph.',
        type: 'object',
        properties: {
          start: {
            type: 'boolean',
            default: false,
          },
          subscribe: {
            type: 'boolean',
            default: true,
          },
          graph: {
            type: 'string',
          },
          flow: FlowSchema,
        },
      },
      getnode: {
        title: 'Get Node',
        description: 'Returns node process definition',
        type: 'object',
        properties: {
          graph: {
            type: 'string',
          },
          node: {
            anyOf: [FlowSchema, NodeSchema],
          },
        },
      },
      subscribe: {
        title: 'Subscribe',
        description: 'Subscribe to a graph.',
        type: 'object',
        properties: {
          graph: {
            title: 'Graph',
            type: 'string',
            description: 'graph to subscribe to',
            required: true,
          },
        },
      },
      unsubscribe: {
        title: 'Unsubscribe',
        description: 'Unsubscribe to a graph.',
        type: 'object',
        properties: {
          graph: {
            title: 'Graph',
            type: 'string',
            description: 'graph to unsubscribe from',
            required: true,
          },
        },
      },
    },
  },
  process: {
    title: 'Process protocol',
    description:
      'Protocol for starting and stopping processes, and finding out about their state.',
    output: {
      stopped: {
        title: 'Stopped',
        description: 'Inform that a given process has stopped.',
        type: 'object',
        properties: {
          time: {
            title: 'Time',
            type: 'string',
            format: 'date-time',
            description: 'time when the process was stopped',
          },
          uptime: {
            title: 'Uptime',
            type: 'string',
            format: 'date-time',
            description: 'time the process was running, in seconds',
          },
          graph: {
            title: 'Graph',
            type: 'string',
            description: 'graph containing this process',
          },
        },
      },
      started: {
        title: 'Started',
        description: 'Inform that a given process has been started.',
        type: 'object',
        properties: {
          time: {
            title: 'Time',
            type: 'string',
            format: 'date-time',
            description: 'time when the process was started',
          },
          graph: {
            title: 'Graph',
            type: 'string',
            description: 'graph containing this process',
          },
        },
      },
      status: {
        title: 'Status',
        description: 'Process status message.',
        type: 'object',
        properties: {
          reqId: {
            type: 'string',
            description: 'id of the request',
          },
          status: {
            title: 'Status',
            type: 'string',
            description: 'Current status of the process',
          },
          graph: {
            title: 'Graph',
            type: 'string',
            description: 'graph containing this process',
          },
          process: {
            title: 'Process Information',
            type: 'object',
            description: 'Captured state of the process',
          },
        },
      },
      getprocess: {
        title: 'Get Process',
        description: 'Get Process message.',
        type: 'object',
        properties: {
          reqId: {
            type: 'string',
            description: 'id of the request',
          },
          pid: {
            type: 'string',
            description: 'pid of the process',
            required: true,
          },
          process: {
            type: 'object',
            description: 'the process',
          },
        },
      },
      getprocessmap: {
        title: 'Get Process map',
        description: 'Process map message.',
        type: 'object',
        properties: {
          reqId: {
            type: 'string',
            description: 'id of the request',
          },
          pid: {
            type: 'string',
            description: 'pid of the process',
          },
          processes: {
            type: 'object',
            required: true,
          },
        },
      },
      error: {
        description: 'Error during process protocol',
        properties: {
          message: {
            type: 'any',
            description: 'contents of the error message',
          },
        },
      },
    },
    input: {
      error: {
        description: 'Error during process protocol',
        properties: {
          message: {
            type: 'any',
            description: 'contents of the error message',
          },
        },
      },
      getstatus: {
        description: 'Request Process Status',
        properties: {
          reqId: {
            type: 'string',
            description: 'id of the request',
          },
          pid: {
            type: 'string',
            description: 'pid of the process',
          },
          graph: {
            type: 'string',
            description: 'graph the action targets',
          },
        },
      },
      getprocess: {
        description: 'Request Process',
        properties: {
          reqId: {
            type: 'string',
            description: 'id of the request',
          },
          pid: {
            type: 'string',
            description: 'pid of the process',
            required: true,
          },
        },
      },
      getprocessmap: {
        description: 'Request Process Map',
        properties: {
          reqId: {
            type: 'string',
            description: 'id of the request',
          },
          pid: {
            type: 'string',
            description: 'pid of the process',
            required: true,
          },
        },
      },
      start: {
        description: 'Start process.',
        properties: {
          pid: {
            type: 'string',
            description: 'pid of the process',
          },
          graph: {
            type: 'string',
            description: 'graph the action targets',
          },
        },
      },
      stop: {
        description: 'Stop process.',
        properties: {
          pid: {
            type: 'string',
            description: 'pid of the process',
          },
          graph: {
            type: 'string',
            description: 'graph the action targets',
          },
        },
      },
      hold: {
        description: 'Hold process.',
        properties: {
          pid: {
            type: 'string',
            description: 'pid of the process',
          },
          graph: {
            type: 'string',
            description: 'graph the action targets',
          },
        },
      },
      release: {
        description: 'Release process.',
        properties: {
          pid: {
            type: 'string',
            description: 'pid of the process',
          },
          graph: {
            type: 'string',
            description: 'graph the action targets',
          },
        },
      },
    },
  },
  runtime: {
    input: {
      list: {
        title: 'List Graphs',
        description: 'Request a list of the currently running main graphs.',
      },
    },
    output: {
      list: {
        title: 'List Graphs',
        description: 'List the currently running main graphs.',
        type: 'array',
      },
    },
  },
}
