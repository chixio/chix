export type ProtocolSchema = {
  output: {
    [command: string]: {
      properties?: {
        [field: string]: {
          type: string
          required?: boolean
        }
      }
    }
  }
}

export type RelativePathResult = {
  up: number
  path: string[]
}

export type InterfaceProperty = {
  name: string
  type: string
  isExported?: boolean
  description?: string
  hasQuestionToken?: boolean
}

export type InterfaceDeclaration = {
  properties: InterfaceProperty[]
} & TypeDeclaration

export type TypeDeclaration = {
  name: string
  type: string
  isExported?: boolean
  description?: string
  hasQuestionToken?: boolean
}
