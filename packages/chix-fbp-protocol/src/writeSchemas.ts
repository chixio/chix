import * as fs from 'fs'
import * as path from 'path'
import {loadSchemas} from './utils'

const schemaPattern =
  path.join(__dirname, './fbp-protocol/schema/yaml/') + '*.yml'

const schemas = loadSchemas(schemaPattern)

fs.writeFileSync(
  path.join(__dirname, './schemas.ts'),
  `
// generated by writeSchemas.ts
export const schemas = ${JSON.stringify(schemas, null, 2)}
`
)
