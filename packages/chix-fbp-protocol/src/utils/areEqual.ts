export function areEqual(one: any, two: any): boolean {
  return JSON.stringify(one) === JSON.stringify(two)
}
