import {charToUpperCase} from './charToUpperCase'

export function camelCaseSplit(str: string, splits: string[] = []): string {
  let newStr = str
  if (str.indexOf('_') >= 0) {
    newStr = str.replace(/_(\w)/, (substring) => substring[1].toUpperCase())
  }
  for (const split of splits) {
    if (newStr.indexOf(split) === 0) {
      return charToUpperCase(newStr, [0, split.length])
    }
  }

  return charToUpperCase(newStr, [0])
}
