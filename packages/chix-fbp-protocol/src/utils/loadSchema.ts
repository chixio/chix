import * as fs from 'fs'
import * as yaml from 'js-yaml'
import * as path from 'path'

export const loadSchema = (file: string, ext: string = '.yml') => {
  return yaml.load(
    fs.readFileSync(
      path.resolve(
        /*
        __dirname,
        '../../src/schema/noflo',
        */
        `${file}${ext}`
      ),
      'utf-8'
    )
  )
}
