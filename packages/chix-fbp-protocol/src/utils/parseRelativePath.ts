import {RelativePathResult} from '../types'

/**
 * Parses a relative path.
 *
 * Returns the levels up and the left over path.
 *
 * @param {string} path_
 * @returns
 * @memberof Interfacer
 */
export function parseRelativePath(path_: string): RelativePathResult {
  const parts = path_.split('/')

  let up = 0
  const path = []

  for (const part of parts) {
    if (part === '..') {
      up++
    } else {
      path.push(part)
    }
  }

  return {
    up,
    path,
  }
}
