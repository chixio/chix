export function charToUpperCase(str: string, pos: number[]) {
  return Array.from(str).reduce(
    (newString: string, char: string, index: number) => {
      if (pos.indexOf(index) >= 0) {
        return `${newString}${char.toUpperCase()}`
      }

      return newString + char
    },
    ''
  )
}
