export function isAbsolutePath(path: string): boolean {
  return path[0] === '/'
}
