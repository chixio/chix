import * as fs from 'fs'
import * as glob from 'glob'
import * as yaml from 'js-yaml'
import * as path from 'path'

export function loadSchemas(schemaPattern: string) {
  return glob.sync(schemaPattern).reduce((schemas, file) => {
    const name = path.basename(file, '.yml')

    schemas[name] = yaml.load(fs.readFileSync(file, 'utf8'))

    return schemas
  }, {})
}
