export function isRelativePath(path: string): boolean {
  return path[0] === '.'
}
