import {expect} from 'chai'
import {charToUpperCase} from '../charToUpperCase'

describe('charToUpperCase', () => {
  it('changes char to uppercase', () => {
    expect(charToUpperCase('astring', [1])).to.be.equal('aString')
  })

  it('changes multiple chars to uppercase', () => {
    expect(charToUpperCase('uppercase', [0, 5])).to.be.equal('UpperCase')
  })
})
