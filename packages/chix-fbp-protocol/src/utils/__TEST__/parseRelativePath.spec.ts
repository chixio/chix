import {expect} from 'chai'
import {parseRelativePath} from '../parseRelativePath'

describe('parseRelativePath', () => {
  it('finds levels up and remainder path', () => {
    expect(parseRelativePath('../../../shared')).to.be.deep.equal({
      up: 3,
      path: ['shared'],
    })
  })
})
