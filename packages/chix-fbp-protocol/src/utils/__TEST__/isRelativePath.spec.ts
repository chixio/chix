import {expect} from 'chai'
import {isRelativePath} from '../isRelativePath'

describe('isRelativePath', () => {
  it('detects relative path', () => {
    expect(isRelativePath('../shared')).to.be.equal(true)
  })
})
