import {expect} from 'chai'
import {areEqual} from '../areEqual'

describe('areEqual', () => {
  it('should be equal', () => {
    expect(areEqual({a: 1}, {a: 1})).to.be.equal(true)
  })
})
