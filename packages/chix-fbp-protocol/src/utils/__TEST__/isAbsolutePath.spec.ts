import {expect} from 'chai'
import {isAbsolutePath} from '../isAbsolutePath'

describe('isAbsolutePath', () => {
  it('detects absolute path', () => {
    expect(isAbsolutePath('/root')).to.be.equal(true)
  })
})
