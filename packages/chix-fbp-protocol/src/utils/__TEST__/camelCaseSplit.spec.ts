import {expect} from 'chai'
import {camelCaseSplit} from '../camelCaseSplit'

describe('camelCaseSplit', () => {
  it('should convert underscored to camelcase', () => {
    expect(camelCaseSplit('under_score')).to.be.equal('UnderScore')
  })

  it('should split by word', () => {
    expect(camelCaseSplit('concatenatedword', ['concatenated'])).to.be.equal(
      'ConcatenatedWord'
    )
  })
})
