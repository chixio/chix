// tslint:disable no-unused-expression no-unused-variable
import glob from 'glob'
import traverse from 'traverse'
import {RelativePathResult} from '../types'
import {
  areEqual,
  camelCaseSplit,
  isAbsolutePath,
  isRelativePath,
  loadSchema,
  parseRelativePath,
} from '../utils'

export type Imports = {
  [file: string]: string[][]
}

export type RefResolverOptions = {
  relative?: boolean
  absolute?: boolean
}

/**
 * Resolving will just be done based on this particular use-case
 *
 * References:
 *
 * $ref: '/shared/message' - Refers to the root of the dir
 * $ref: '../input/addnode' - Refers internally and possibly one dir up
 *
 * @export
 * @class Interfacer
 */
export class RefResolver {
  public baseDir: string = ''
  public schemas: Map<string, any> = new Map()
  public $refs: Map<string, [string[]]> = new Map()
  public relative: boolean = true
  public absolute: boolean = true
  public imports: Map<string, Imports> = new Map()

  constructor(options: RefResolverOptions = {}) {
    Object.assign(this, options)
  }

  public loadSchema(baseDir: string, pattern: string = '**/*.yml') {
    this.baseDir = baseDir

    process.chdir(baseDir)

    const files = glob.sync(pattern)

    files.forEach((file) => {
      const schema = loadSchema(file, '')

      this.registerSchema(file, schema)

      const [baseName] = file.split('.')

      this.loadRefs(baseName)
    })
  }

  /**
   * Use this to process per schema, instead of loadSchema
   *
   * @param {string} fileName
   * @param {*} schema
   * @memberof RefResolver
   */
  public processSchema(fileName: string, schema: any) {
    this.registerSchema(fileName, schema)

    const [baseName] = fileName.split('.')

    this.loadRefs(baseName)
  }

  /**
   * Returns all detected refs.
   *
   * This can be used to generate their interfaces first.
   *
   * Use refToInterfaceName to replace the type with the InterfaceName
   * whenever a $ref is encountered.
   *
   * @returns
   * @memberof RefResolver
   */
  public getRefs() {
    return this.$refs
  }

  /**
   * Should be able to translate a $ref string to an interface
   *
   * Given the file it's contained in.
   *
   * Should also just ignore the file and return the interface name
   * from a different file if that's the case.
   *
   * Idea here is that we do not have to worry about refs at all.
   *
   * Ref just means we already have an interface provided for that ref.
   *
   * This also solves any additional complexity or recursion.
   *
   * And I think the existing setup can now just be used.
   *
   * Only thing that has to be added is this ref check.
   *
   * Note that the interface convention is <CamelCase: FileName><CamelCase: path>
   *
   * e.g. SharedMessage, SharedPortDefinition, GraphInputClear, GraphInputChangeNode
   * Although in the Fbp Protocol case input || output can be ignored thus:
   *
   *  GraphClear, GraphChangeNode could also work.
   *
   * This is because the message format seems to be 'through'
   *
   * The result will be pretty concise now I think, and will need no manual adjustments.
   */
  public refToInterfaceName(
    $ref: string,
    fileName?: string,
    prefixFilename: boolean = false
  ) {
    if (isAbsolutePath($ref)) {
      const [, file, ...path] = $ref.split('/')

      const parts = prefixFilename ? [file].concat(path) : path

      return parts.map((part) => camelCaseSplit(part)).join('')
    } else if (isRelativePath($ref) && fileName) {
      // only handles this certain type of relative path.
      const path = $ref.replace('../', '').split('/')

      if (
        this.$refs.has(fileName) &&
        this.$refs.get(fileName).find((val) => val && areEqual(val, path))
      ) {
        const parts = prefixFilename ? [fileName].concat(path) : path

        return parts.map((part) => camelCaseSplit(part)).join('')
      }
      throw Error('Ref not registered.')
    }

    throw Error('Failed to retrieve InterfaceName.')
    /*
    return [file].concat(path).map((part) => camelCaseSplit(part))
    */
  }

  private registerSchema(fullName: string, schema: any) {
    const file = fullName.replace('.yml', '')

    this.schemas.set(file, schema)
  }

  private initRef(name): void {
    if (!this.$refs.has(name)) {
      this.$refs.set(name, [] as any)
    }
  }

  private initImport(schemaName, name): void {
    if (!this.imports.has(schemaName)) {
      this.imports.set(schemaName, {})
    }

    if (!this.imports.get(schemaName)[name]) {
      const imports = this.imports.get(schemaName)

      imports[name] = []

      this.imports.set(schemaName, imports)
    }
  }

  private addRef(name: string, path: string[]): void {
    const fileRefs = this.$refs.get(name)

    if (!fileRefs.find((ref) => areEqual(ref, path))) {
      fileRefs.push(path)

      this.$refs.set(name, fileRefs)
    }
  }

  private addImport(schemaName: string, name: string, path: string[]): void {
    const file = this.imports.get(schemaName)
    const imports = file[name]

    if (!imports.find((import_) => areEqual(import_, path))) {
      imports.push(path)

      this.imports.set(schemaName, {
        ...file,
        [name]: imports,
      })
    }
  }

  private loadRefs(schemaName: string) {
    const schema = this.schemas.get(schemaName)

    const self = this as RefResolver

    traverse(schema).forEach(function (
      this: traverse.TraverseContext,
      obj: any
    ) {
      const {$ref} = obj

      if ($ref) {
        // just resolve relative to absolute including containing file.
        if (isRelativePath($ref)) {
          if (!self.relative) {
            return
          }
          const {up, path} = parseRelativePath($ref) as RelativePathResult

          let ancestor = this.parent as any
          for (let i = 0; i <= up - 1; i++) {
            if (ancestor.parent) {
              ancestor = ancestor.parent
            } else {
              throw Error('No ancestor')
            }
          }

          if (ancestor.isRoot) {
            self.initRef(schemaName)

            self.addRef(schemaName, path)
          } else {
            throw Error('Relative path to non-root not supported')
          }
        } else if (isAbsolutePath($ref)) {
          if (!self.absolute) {
            return
          }

          // process absolute reference. absolute in this case always seems to point to a file.
          // <file>/<path>
          // but is not always the case, anyway the second case is never used.
          const [, file, ...parts] = $ref.split('/')

          if (parts.length > 0) {
            self.initRef(file)
            self.initImport(schemaName, file)
            self.addRef(file, parts)
            self.addImport(schemaName, file, parts)
          } else {
            throw Error(`Failed to process $ref {$ref}`)
          }
        } else {
          throw Error(`Failed to process $ref ${$ref}`)
        }
      }
    })
  }
}
