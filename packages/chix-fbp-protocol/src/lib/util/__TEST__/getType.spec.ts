import {expect} from 'chai'
import {getType} from '../getType'

describe('getType', () => {
  it('defaults to object', () => {
    expect(getType({})).to.equal('object')
  })

  it('converts enum', () => {
    expect(
      getType({
        enum: ['veni', 'vidi', 'vici'],
      })
    ).to.equal(`'veni' | 'vidi' | 'vici'`)
  })

  it('converts multiple types', () => {
    expect(
      getType({
        type: ['number', 'string'],
      })
    ).to.equal(`number | string`)
  })

  it('passes through valid types', () => {
    ;['string', 'number', 'boolean', 'any', 'null'].forEach(type => {
      expect(
        getType({
          type,
        })
      ).to.equal(type)
    })
  })

  it('converts integer to number', () => {
    expect(
      getType({
        type: 'integer',
      })
    ).to.equal('number')
  })

  it('converts array to any[]', () => {
    expect(
      getType({
        type: 'array',
      })
    ).to.equal('any[]')
  })

  it('will merge number and integer for multiple types', () => {
    expect(
      getType({
        type: ['integer', 'number'],
      })
    ).to.equal('number')
  })
})
