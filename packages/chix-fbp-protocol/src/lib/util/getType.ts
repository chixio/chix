export function filterType(type) {
  if (type === 'integer') {
    return 'number'
  }

  if (type === 'array') {
    return 'any[]'
  }

  return type
}

export function filterTypes(types: string[]) {
  if (types.indexOf('integer') >= 0) {
    if (types.indexOf('number') >= 0) {
      return types.filter((value) => value !== 'integer').map(filterType)
    }

    return types.map(filterType)
  }

  return types
}

export function getType(obj: any): string {
  if (obj.enum) {
    return `'${obj.enum.join("' | '")}'`
  }

  if (obj.type) {
    if (Array.isArray(obj.type)) {
      return filterTypes(obj.type).join(' | ')
    }

    return filterType(obj.type)
  }

  return 'object'
}
