import deepEqual from 'fast-deep-equal'
import {resolve} from 'path'
import {InterfaceDeclaration, InterfaceProperty, ProtocolSchema} from '../types'
import {camelCaseSplit, isAbsolutePath, loadSchema} from '../utils'
import {RefResolver} from './RefResolver'
import {getType} from './util'

/*
 * const refResolver = new RefResolver()
 *
 * refResolver.loadSchema('./src/fbp-protocol/schema/yaml')
 *
 * console.log(
 *   refResolver.refToInterfaceName('/shared/message'),
 *   refResolver.refToInterfaceName('/shared/port_definition'),
 *   refResolver.refToInterfaceName('../input/edges', 'network')
 * )
 */
export class InterfaceSchema {
  public interfaces: InterfaceDeclaration[] = []
  public types: InterfaceDeclaration[] = []

  public _refResolver: RefResolver = new RefResolver({
    relative: false,
  })

  public suffix: string = 'Payload'

  public processSchema(
    sourceFile: any,
    protocol: string,
    kind?: string,
    splits?: any[]
  ) {
    const schema = loadSchema(
      resolve(__dirname, '../fbp-protocol/schema/yaml', protocol)
    )

    this._refResolver.processSchema(protocol, schema)

    const interfaces: InterfaceDeclaration[] = []
    const types: InterfaceDeclaration[] = []

    if (kind) {
      this.loadInterfacesFrom(
        kind,
        interfaces,
        types,
        schema as ProtocolSchema,
        splits
      )
    } else {
      this.loadInterfaces(interfaces, types, schema as ProtocolSchema, splits)
    }

    // TODO: can cause duplicates.
    // this's are only for reference btw.
    this.interfaces = [...this.interfaces, ...interfaces]

    this.types = [...this.types, ...types]

    // this._refResolver.getRefs()

    types.forEach((type: any) => {
      sourceFile.addTypeAlias(type)
    })

    interfaces.forEach((interface_: any) => {
      sourceFile.addInterface(interface_)
    })
  }

  public processInterfaces(
    prop: string,
    schema: any,
    interfaces: InterfaceDeclaration[],
    types: InterfaceDeclaration[],
    splits: string[]
  ) {
    const name = camelCaseSplit(prop, splits)

    const required: string[] = schema.required || []

    const declaration = {
      name: `${name}${this.suffix}`,
      type: getType(schema),
      isExported: true,
      hasQuestionToken: required.indexOf(prop) === -1,
      properties: [],
    } as InterfaceDeclaration

    const {properties, allOf} = schema

    if (properties) {
      this.processProperties(
        declaration,
        properties,
        interfaces,
        [name],
        required
      )

      interfaces.push(declaration)
    } else if (allOf) {
      // will be a union, first just process merge the properties part.
      // which is also very specific to these schemas. should be able to support more types later.
      allOf.forEach((item: any) => {
        if (item.properties) {
          this.processProperties(
            declaration,
            item.properties,
            interfaces,
            [name],
            required
          )

          interfaces.push(declaration)
        }
        // else process ref.
      })
    } else {
      const existingType = types.find(
        (typeDeclaration: InterfaceDeclaration) => {
          return deepEqual(typeDeclaration, declaration)
        }
      )

      if (!existingType) {
        types.push(declaration)
      } else {
        console.log('exists', existingType)
      }
    }
  }

  public loadInterfaces(
    interfaces: InterfaceDeclaration[],
    types: InterfaceDeclaration[],
    schema: ProtocolSchema,
    splits: string[] = []
  ) {
    Object.keys(schema).forEach((key) => {
      this.processInterfaces(key, schema[key], interfaces, types, splits)
    })
  }

  // TODO: entry into most files are non standard `shared` is correct.
  public loadInterfacesFrom(
    type: string,
    interfaces: InterfaceDeclaration[],
    types: InterfaceDeclaration[],
    schema: ProtocolSchema,
    splits: string[] = []
  ) {
    const commands = (schema as any)[type.toLowerCase()]

    Object.keys(commands).forEach((command: string) => {
      const _schema = commands[command] as any
      if (
        _schema.allOf &&
        _schema.allOf[1] &&
        _schema.allOf[1].properties &&
        _schema.allOf[1].properties.payload
      ) {
        // only interested in creating interfaces for the actual payload
        this.processInterfaces(
          command,
          _schema.allOf[1].properties.payload,
          interfaces,
          types,
          splits
        )
      }
    })
  }

  public processProperties(
    interfaceDeclaration: InterfaceDeclaration,
    properties: any,
    interfaces: any[],
    path: string[] = [],
    required
  ) {
    Object.keys(properties).map((prop: string) => {
      const property = properties[prop]

      const name = camelCaseSplit(prop)

      if (property.type === 'array') {
        interfaceDeclaration.properties.push(
          this.processArray(
            prop,
            property,
            interfaces,
            path.concat(name)
          ) as InterfaceProperty
        )
      } else if (property.type === 'object' && property.properties) {
        const newInterfaceName = this.createInterfaceName(name, path)

        const newObjectInterface = {
          name: newInterfaceName,
          type: 'object',
          isExported: true,
          hasQuestionToken: required.indexOf(prop) === -1,
          docs: property.description,
          properties: [],
        } as InterfaceDeclaration

        this.processProperties(
          newObjectInterface,
          property.properties,
          interfaces,
          path.concat(name),
          property.required || []
        )

        const existingInterface = interfaces.find((interface_) => {
          return newObjectInterface.name === interface_.name
        })

        if (!existingInterface) {
          interfaces.push(newObjectInterface)
        } else {
          interfaces.forEach((interface_) => {
            if (
              newObjectInterface.name === interface_.name &&
              !deepEqual(newObjectInterface, interface_)
            ) {
              console.log(newObjectInterface)
              console.log(interface_)
              throw Error(
                'Interface already exists, but structure is not the same.'
              )
            }
          })

          console.error(
            `Interface ${newInterfaceName} already exists. skipping...`
          )
        }

        interfaceDeclaration.properties.push({
          name: prop,
          type: newInterfaceName,
          hasQuestionToken: required.indexOf(prop) === -1,
          description: property.description,
        })
      } else {
        let type

        if (property.$ref) {
          if (isAbsolutePath(property.$ref)) {
            // TODO make this configurable per file
            const prefixFileName = false

            type = this._refResolver.refToInterfaceName(
              property.$ref,
              undefined,
              prefixFileName
            )
          }
        } else {
          type = getType(property)
        }

        interfaceDeclaration.properties.push({
          name: prop,
          hasQuestionToken: required.indexOf(prop) === -1,
          type,
          description: property.description,
        })
      }
    })
  }

  public processArray(
    prop: string,
    property: any,
    interfaces: any[],
    path: string[] = []
  ) {
    // the array type, the type becomes an interface itself
    // in case the item type is object else just string for now.
    if (
      property.items &&
      property.items.type === 'object' &&
      property.items.properties
    ) {
      const properties = property.items.properties
      // new interface has to be created.
      const interfaceName = camelCaseSplit(prop)

      // our type
      const interfaceDeclaration = {
        name: prop,
        type: `${interfaceName}${this.suffix}[]`,
        // isExported: true
      } as any

      const newInterfaceDeclaration = {
        name: `${interfaceName}${this.suffix}`,
        type: 'object',
        isExported: true,
        properties: [],
      }

      const required = []

      this.processProperties(
        newInterfaceDeclaration,
        properties,
        interfaces,
        path.concat(prop),
        required
      )

      interfaces.push(newInterfaceDeclaration)

      return interfaceDeclaration
    } else {
      return {
        name: prop,
        type: 'string[]',
      }
    }
  }

  public createInterfaceName(name: string, path: string[]) {
    return false
      ? `${name}${this.suffix}`
      : `${path.concat(name).join('')}${this.suffix}`
  }

  public addImportsTo(schemaName: string, sourceFile) {
    // need to determine from refs, which files are using imports
    const importsForFile = this._refResolver.imports.get(schemaName)

    if (importsForFile) {
      Object.keys(importsForFile).forEach((file) => {
        const imports = importsForFile[file]

        const namedImports = imports.map((import_) => {
          return import_.map((part) => camelCaseSplit(part)).join('')
        })

        sourceFile.addImportDeclaration({
          // should get a list of used refs for *this* file
          namedImports,
          moduleSpecifier: './shared',
        })
      })
    }
  }
}
