import {expect} from 'chai'
import * as path from 'path'
import {RefResolver} from '../RefResolver'

const baseDir = path.resolve(__dirname, '../../fbpprotocol/schema/yaml')

describe.skip('RefResolver', () => {
  const refResolver = new RefResolver()

  describe('loadSchema', () => {
    it('loads yaml schema', () => {
      refResolver.loadSchema(baseDir)
    })

    it('schemas are registered', () => {
      expect(refResolver.schemas.size).to.equal(6)
    })

    it('captured all references', () => {
      expect(refResolver.$refs.get('shared')).to.deep.equal([
        ['message'],
        ['port_definition'],
        ['metadata_node'],
        ['port'],
        ['metadata_edge'],
        ['iip'],
        ['metadata_group'],
        ['network_event'],
        ['capabilities'],
        ['graph_id'],
        ['secret'],
      ])
    })
  })
})
