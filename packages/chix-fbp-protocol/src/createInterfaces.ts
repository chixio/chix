import {mkdirpSync} from 'mkdirp'
import * as path from 'path'
import * as rimraf from 'rimraf'
import Project from 'ts-simple-ast'
import {InterfaceSchema} from './lib'

export async function createInterfaces() {
  const project = new Project()

  const is = new InterfaceSchema()

  const dir = path.resolve(__dirname, './interfaces')

  rimraf.sync(dir)

  mkdirpSync(dir)
  process.chdir(dir)

  // const processPayloadInterfaces = project.createSourceFile(`./process.ts`)
  const graphPayloadInterfaces = project.createSourceFile(`./graph.ts`)
  const sharedInterfaces = project.createSourceFile(`./shared.ts`)
  const networkPayloadInterfaces = project.createSourceFile(`./network.ts`)
  const runtimePayloadInterfaces = project.createSourceFile(`./runtime.ts`)
  const componentPayloadInterfaces = project.createSourceFile(`./component.ts`)
  const tracePayloadInterfaces = project.createSourceFile(`./trace.ts`)

  is.processSchema(graphPayloadInterfaces, 'graph', 'Input', [
    'add',
    'remove',
    'rename',
    'change',
  ])
  is.processSchema(graphPayloadInterfaces, 'graph', 'Output', [
    'add',
    'remove',
    'rename',
    'change',
  ])

  is.processSchema(networkPayloadInterfaces, 'network', 'Input', [
    'process',
    'begin',
    'end',
    'get',
  ])
  is.processSchema(networkPayloadInterfaces, 'network', 'Output', [
    'process',
    'begin',
    'end',
  ])

  is.processSchema(runtimePayloadInterfaces, 'runtime', 'Input', [
    'get',
    'packet',
  ])
  is.processSchema(runtimePayloadInterfaces, 'runtime', 'Output', ['packet'])

  is.processSchema(componentPayloadInterfaces, 'component', 'Input', ['get'])
  is.processSchema(componentPayloadInterfaces, 'component', 'Output')

  is.processSchema(tracePayloadInterfaces, 'trace', 'Input')
  is.processSchema(tracePayloadInterfaces, 'trace', 'Output')

  // TODO: process is chix specific..
  // is.processSchema(processPayloadInterfaces, 'process', 'Input')
  // is.processSchema(processPayloadInterfaces, 'process', Output')

  is.suffix = ''
  is.processSchema(sharedInterfaces, 'shared')

  is.addImportsTo('graph', graphPayloadInterfaces)
  is.addImportsTo('network', networkPayloadInterfaces)
  is.addImportsTo('runtime', runtimePayloadInterfaces)
  is.addImportsTo('component', componentPayloadInterfaces)

  await project.save()
}

(async () => {
  await createInterfaces()
})()
