import * as path from 'path'

import {camelCaseSplit, loadSchemas} from './utils'

const capitalize = (str: string) => {
  return str[0].toUpperCase() + str.slice(1)
}

const splits = [
  'add',
  'begin',
  'change',
  'components',
  'end',
  'get',
  'packet',
  'process',
  'remove',
  'rename',
]

export type ActionMap = {
  input: {
    [key: string]: {
      [key: string]: string
    }
  }
  output: {
    [key: string]: {
      [key: string]: string
    }
  }
}

export function createActionsMap(actionsMap) {
  let postfix
  const actionsKeys = Object.keys(actionsMap)
  return actionsKeys.reduce((text, protocol, index) => {
    const item = actionsMap[protocol]

    text += `  ${protocol}: {\n`

    const itemKeys = Object.keys(item)
    text += itemKeys.reduce((text2, command, index2) => {
      postfix = index2 < itemKeys.length - 1 ? ',' : ''
      const type = item[command]
      text2 += `    ${command}: ${type}${postfix}\n`

      return text2
    }, '')

    postfix = index < actionsKeys.length - 1 ? ',\n' : ''
    text += `  }${postfix}`

    return text
  }, '')
}

export function createNgxsActions() {
  const schemaPattern =
    path.join(__dirname, './fbp-protocol/schema/yaml/') + '*.yml'

  const schemas = loadSchemas(schemaPattern)

  const actionClasses = []
  const actionTypes = []
  const actions = {input: [], output: []}
  const actionsMap: ActionMap = {input: {}, output: {}}

  Object.entries(schemas).forEach(([protocol, item]) => {
    if (protocol !== 'shared') {
      actionTypes.push(`  // ${protocol.toUpperCase()}`)
      actionClasses.push(`// ${protocol.toUpperCase()}`)
      ;['input', 'output'].forEach(type => {
        Object.entries(item[type]).forEach(([command, props]) => {
          // const className = `${capitalize(protocol)}${capitalize(command)}Action`
          const Type = capitalize(type)
          const Protocol = capitalize(protocol)
          const camelCaseCommand = camelCaseSplit(command, splits)
          const payloadType = `${Protocol}.${Type}.${capitalize(
            camelCaseCommand
          )}Payload`

          const prefix = type === 'input' ? 'FbpSend' : 'FbpRecv'
          const typeProperty = `${prefix}${capitalize(protocol)}${capitalize(
            camelCaseCommand
          )}`
          const actionName = `${typeProperty}Action`

          const klazz = `
/**
 * ${(props as any).description}
 */            
export class ${actionName} {
  public static readonly type = FbpProtocolActionTypes.${typeProperty}
  public readonly protocol = '${protocol}'
  public readonly command = '${command}'
  constructor(
    public payload: ${payloadType},
    public runtimeId: string
  ) {}
}
  `

          actionTypes.push(
            `  public static readonly ${typeProperty} = '[FbpProtocol] ${typeProperty}'`
          )
          actionClasses.push(klazz)

          actions[type].push(actionName)

          if (!actionsMap[type].hasOwnProperty(protocol)) {
            actionsMap[type][protocol] = {}
          }

          actionsMap[type][protocol][command] = actionName
        })

        actionTypes.push('')
      })
    }
  })

  const contents = `
// tslint:disable max-classes-per-file
import {
  Component,
  Graph,
  Network,
  Runtime,
  Trace
} from '@chix/fbp-protocol'

export class FbpProtocolActionTypes {
${actionTypes.join('\n')}
}

${actionClasses.join('\n')}

export type FbpActions = ${actions.output.join('\n  | ')}
  | ${actions.input.join('\n  | ')}

export const FbpRecvActions = [
  ${actions.output.join(',\n  ')}
]

export const FbpRecvActionMap = {
  ${createActionsMap(actionsMap.output)}
}

export const FbpSendActions = [
  ${actions.input.join(',\n  ')}
]

export const FbpSendActionMap = {
  ${createActionsMap(actionsMap.input)}
}
`
  console.log(contents)
}

createNgxsActions()
