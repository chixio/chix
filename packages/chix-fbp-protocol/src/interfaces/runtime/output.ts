import {Flow as FlowDefinition} from '@chix/common'
import {ExtendedCapabilities, PortDefinition} from '../shared'
export type {PacketPayload} from './input'

export interface ErrorPayload {
  message: string
}

export interface PortsPayload {
  graph: string
  inPorts: PortDefinition
  outPorts: PortDefinition
}

export interface RuntimePayload {
  id?: string
  label?: string
  version: string
  allCapabilities?: ExtendedCapabilities
  capabilities: ExtendedCapabilities
  graph?: string
  graphs?: FlowDefinition[] // fbpx property
  type: string
  namespace?: string
  repository?: string
  repositoryVersion?: string
}

export type ListPayload = FlowDefinition[]

export interface PacketSentPayload {
  port: string
  event: 'connect' | 'begingroup' | 'data' | 'endgroup' | 'disconnect'
  type?: string
  schema?: string
  graph: string
  payload?: any
}
