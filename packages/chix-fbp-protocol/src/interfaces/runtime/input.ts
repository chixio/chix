export interface GetRuntimePayload {
  secret?: string
}

export interface PacketPayload {
  port: string
  event: 'connect' | 'begingroup' | 'data' | 'endgroup' | 'disconnect'
  type?: string
  schema?: string
  graph: string
  payload?: any
  secret?: string
}
