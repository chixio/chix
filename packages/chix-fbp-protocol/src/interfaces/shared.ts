import {JSONSchema4Type} from 'json-schema'

export type Secret = string
export type GraphId = string

export interface PortDefinition {
  id: string
  type: string
  schema?: string
  required?: boolean
  addressable?: boolean
  description?: string
  // note supports more than string[]
  values?: JSONSchema4Type[]
  default?: any
}

export type StandardCapabilityTypes =
  | 'protocol:network'
  | 'network:persist'
  | 'network:status'
  | 'network:data'
  | 'network:control'
  | 'protocol:component'
  | 'component:getsource'
  | 'component:setsource'
  | 'protocol:runtime'
  | 'protocol:graph'
  | 'graph:readonly'
  | 'protocol:trace'

export type ExtendedCapabilityTypes =
  | 'protocol:process'
  | 'process:status'
  | StandardCapabilityTypes

export type Capabilities = StandardCapabilityTypes[]

export type ExtendedCapabilities = ExtendedCapabilityTypes[]

export interface Message {
  protocol: string
  command: string
  payload: {
    graph?: string
  }
}

export interface MetadataNode {
  x?: number
  y?: number
}

export interface MetadataEdge {
  route?: number
  schema?: string
  secure?: boolean
}

export interface MetadataGroup {
  description?: string
}

export interface Port {
  node: string
  port: string
  index?: string | number
}

export interface Iip {
  data: any // object | any[] | string | number | boolean | null
}

export interface NetworkEvent {
  id: string
  src?: Port
  tgt: Port
  graph: string
  subgraph?: string[] // made optional
}

// Extra

export interface Edge {
  src: Port
  tgt: Port
}
