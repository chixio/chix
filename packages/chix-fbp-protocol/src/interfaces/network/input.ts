import {Port} from '../shared'

export interface StartPayload {
  graph: string
}

export interface GetStatusPayload {
  graph: string
}

export interface StopPayload {
  graph: string
}

export interface PersistPayload {}

export interface DebugPayload {
  enable: boolean
  graph: string
}

export interface Edges {
  src?: Port
  tgt?: Port
}

export interface EdgesPayload {
  graph: string
  edges: Edges[]
}
