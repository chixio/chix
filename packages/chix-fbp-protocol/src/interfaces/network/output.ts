import {NetworkEvent} from '../shared'
export type {EdgesPayload} from './input'

export interface StoppedPayload {
  time?: string
  uptime?: number
  graph: string
  running?: boolean
  started?: boolean
  debug?: boolean
}

export interface StartedPayload {
  time?: string
  graph: string
  started?: boolean
  running?: boolean
  debug?: boolean
}

export interface StatusPayload {
  graph: string
  uptime?: number
  started?: boolean
  running: boolean
  debug?: boolean
}

export interface OutputPayload {
  message: string
  type?: 'message' | 'previewurl'
  url?: string
}

export interface ErrorPayload {
  message: string
  stack?: string
  graph?: string
}

export interface ProcessErrorPayload {
  id: string
  error: string
  graph: string
}

export interface IconPayload {
  id: string
  icon: string
  graph: string
}
export interface ConnectPayload extends NetworkEvent {}

export interface BeginGroupPayload extends NetworkEvent {
  group: string
}

// export interface DataPayload extends NetworkEvent {
export interface DataPayload {
  data: any
  type?: string
  schema?: string
  // additional properties
  id?: string
  src?: any
  tgt?: any
  graph?: string
  subgraph?: string
}

export interface EndGroupPayload extends NetworkEvent {
  group: string
}

export interface DisconnectPayload extends NetworkEvent {}
