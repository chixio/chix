export interface GetStatusPayload {
  reqId: string
  pid: string
  graph: string
}

export interface GetProcessPayload {
  reqId?: string
  pid: string
}

export interface GetProcessMapPayload {
  reqId?: string
  pid: string
}

export interface StoppedPayload {
  time: string
  uptime: string
  graph: string
}

export interface StartedPayload {
  time: string
  graph: string
}

export interface StatusPayload {
  status: string
  graph: string
  process: object
}

export interface ErrorPayload {
  message: any
}

export interface StartPayload {
  pid: string
  graph: string
}

export interface StopPayload {
  pid: string
  graph: string
}

export interface HoldPayload {
  pid: string
  graph: string
}

export interface ReleasePayload {
  pid: string
  graph: string
}
