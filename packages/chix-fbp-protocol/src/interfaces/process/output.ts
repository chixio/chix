import {Process} from '@chix/common'

export interface ProcessMap {
  [pid: string]: Process
}

export interface StoppedPayload {
  time: string
  uptime: string
  graph: string
}

export interface StartedPayload {
  time: string
  graph: string
}

export interface StatusPayload {
  reqId?: string
  status: string
  graph: string
  process: Process
}

export interface GetProcessPayload {
  reqId?: string
  pid: string
  process: Process
}

export interface GetProcessMapPayload {
  reqId?: string
  pid: string
  processes: ProcessMap
}

export interface ErrorPayload {
  message: any
}

export interface StartPayload {
  pid: string
  graph: string
}

export interface StopPayload {
  pid: string
  graph: string
}

export interface HoldPayload {
  pid: string
  graph: string
}

export interface ReleasePayload {
  pid: string
  graph: string
}
