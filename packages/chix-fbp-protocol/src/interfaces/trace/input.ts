import {GraphId} from '../shared'
export type {StartPayload, StopPayload, ClearPayload} from './messages'

export interface DumpPayload {
  graph: GraphId
  type: 'flowtrace.json'
}
