import {GraphId} from '../shared'

export interface StartPayload {
  graph: GraphId
  buffersize?: number
}

export interface StopPayload {
  graph: GraphId
}

export interface ClearPayload {
  graph: GraphId
}
