export interface ListPayload {}

export interface GetSourcePayload {
  name: string
  // custom
  metadata?: {
    provider?: string
  }
}

export interface SourcePayload {
  name: string
  language: string
  library?: string
  code: string
  tests?: string
}
