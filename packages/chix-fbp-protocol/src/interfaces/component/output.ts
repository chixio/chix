import {PortDefinition} from '../shared'

export interface ErrorPayload {
  message: string
}

export interface ComponentPayload {
  name: string
  description?: string
  icon?: string
  subgraph: boolean
  inPorts: PortDefinition
  outPorts: PortDefinition
}

export type ComponentsReadyPayload = number

export interface SourcePayload {
  name: string
  language: string
  library?: string
  code: string
  tests?: string
}
