import {Flow as FlowDefinition, IIP as IIPDefinition} from '@chix/common'
import {Iip, MetadataEdge, MetadataGroup, MetadataNode, Port} from '../shared'

export interface ErrorPayload {
  message: string
}

export interface ClearPayload {
  id: string
  name?: string
  library?: string
  main?: boolean
  icon?: string
  description?: string
}

export interface AddNodePayload {
  id: string
  component: string
  metadata?: MetadataNode
  graph: string
}

export interface RemoveNodePayload {
  id: string
  graph: string
}

export interface RenameNodePayload {
  from: string
  to: string
  graph: string
}

export interface ChangeNodePayload {
  id: string
  metadata: MetadataNode
  graph: string
}

export interface AddEdgePayload {
  src: Port
  tgt: Port
  metadata?: MetadataEdge
  graph: string
}

export interface RemoveEdgePayload {
  graph: string
  src: Port
  tgt: Port
}

export interface ChangeEdgePayload {
  graph: string
  metadata?: MetadataEdge
  src: Port
  tgt: Port
}

export interface AddInitialPayload {
  graph: string
  metadata?: MetadataEdge
  src: Iip
  tgt: Port
}

export interface RemoveInitialPayload {
  src?: Iip
  tgt: Port
  graph: string
}

export interface AddInportPayload {
  public: string
  node: string
  port: string
  metadata?: MetadataNode
  graph: string
}

export interface RemoveInportPayload {
  public: string
  graph: string
}

export interface RenameInportPayload {
  from: string
  to: string
  graph: string
}

export interface AddOutportPayload {
  public: string
  node: string
  port: string
  metadata?: MetadataNode
  graph: string
}

export interface RemoveOutportPayload {
  public: string
  graph?: string
}

export interface RenameOutportPayload {
  from: string
  to: string
  graph: string
}

export interface AddGroupPayload {
  name: string
  nodes: string[]
  metadata?: MetadataGroup
  graph: string
}

export interface RemoveGroupPayload {
  name: string
  graph: string
}

export interface RenameGroupPayload {
  from: string
  to: string
  graph: string
}

export interface ChangeGroupPayload {
  name: string
  metadata: MetadataGroup
  graph: string
}

// Fbpx

export interface CreatePayload {
  start?: boolean
  flow: FlowDefinition
  graph?: string
  iips?: IIPDefinition[]
  install?: boolean
  subscribe?: boolean
}
export interface SubscribePayload {
  graph: string
}

export interface UnsubscribePayload {
  graph: string
}
