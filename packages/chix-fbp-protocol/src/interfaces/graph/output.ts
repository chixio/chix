import {Flow as FlowDefinition, NodeDefinition} from '@chix/common'

export type {
  ErrorPayload,
  ClearPayload,
  AddNodePayload,
  RemoveNodePayload,
  RenameNodePayload,
  ChangeNodePayload,
  AddEdgePayload,
  RemoveEdgePayload,
  ChangeEdgePayload,
  AddInitialPayload,
  RemoveInitialPayload,
  AddInportPayload,
  RemoveInportPayload,
  RenameInportPayload,
  AddOutportPayload,
  RemoveOutportPayload,
  RenameOutportPayload,
  AddGroupPayload,
  RemoveGroupPayload,
  RenameGroupPayload,
  ChangeGroupPayload,
  CreatePayload,
  SubscribePayload,
  UnsubscribePayload,
} from './messages'

// FBPX
export interface GetNodePayload {
  graph: string
  node: FlowDefinition | NodeDefinition
}
