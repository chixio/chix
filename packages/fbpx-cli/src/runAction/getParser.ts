import {ChixRenderer, FBPParser, FSResolver, NoFloRenderer} from '@chix/fbpx'
import {Env} from '../types'
import {LexerToken, tokenTable} from './tokenTable'

export function getParser(env: Env, defaultProvider: string) {
  let renderer
  const parser = new FBPParser()

  // Renderer Setup
  if (env.noflo) {
    renderer = new NoFloRenderer()
  } else {
    renderer = new ChixRenderer()
    renderer.skipIDs = !env.uuid
    renderer.setDefaultProvider(defaultProvider)
  }

  parser.addIncludeResolver(new FSResolver())
  parser.addRenderer(renderer)

  // TODO: this should be done differently, runs always now
  // should not be a listener just get the tokens.
  // if no stream is used it is synchronous anyway..
  parser.lexer.on(
    'lineTokens',
    (tokens: LexerToken[], line: string, nr: number) => {
      env.tokenTable += tokenTable(tokens, line, nr)
    }
  )

  return parser
}
