import * as Logger from 'npmlog'
import {Env} from '../types'

export function fail(env: Env, error: Error) {
  if (env.watch) {
    Logger.error(error)
  } else {
    throw error
  }
}
