import {
  Action,
  BrowserifyAction,
  CertAction,
  CompileAction,
  ConfigAction,
  ConvertAction,
  DeployAction,
  DepsAction,
  FlowhubAction,
  GraphAction,
  InputAction,
  InstallAction,
  ListAction,
  LoginAction,
  RunAction,
  RuntimeAction,
  ServeAction,
  TokenAction,
  // update
  WatchAction,
} from '../action'
import {Env} from '../types'
import {fail} from './fail'
import {getParser} from './getParser'

const __O_O__ = {
  browserify: BrowserifyAction,
  convert: ConvertAction,
  compile: CompileAction,
  cert: CertAction,
  config: ConfigAction,
  deploy: DeployAction,
  deps: DepsAction,
  flowhub: FlowhubAction,
  graph: GraphAction,
  input: InputAction,
  install: InstallAction,
  list: ListAction,
  login: LoginAction,
  run: RunAction,
  runtime: RuntimeAction,
  serve: ServeAction,
  tokens: TokenAction,
  // update,
  watch: WatchAction,
}

export async function runAction(env: Env, contents?: string) {
  let flow

  env.logger.level = env.parent.verbose ? 'verbose' : 'info'

  const action = new __O_O__[env._name]() as Action
  action.env = env // this === program
  action.logger = env.logger
  action.fail = (err: Error) => fail(env, err)
  action.parser = env.parser = getParser(env, await action.getDefaultProvider())

  if (contents) {
    try {
      // maybe it's json
      flow = JSON.parse(contents)
      env.reverse = true
    } catch (e) {
      // else we expect .fbp
      try {
        flow = env.parser.parse(contents, env.includePath)
      } catch (e: any) {
        // return in case of watch
        return fail(env, e)
      }
    }
  }

  action.execute(flow).catch((error) => {
    console.error(error.message)
  })
}
