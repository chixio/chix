import {Env} from '../types'
import {loadFile} from './loadFile'
import {loadStdin} from './loadStdin'

export async function loadFlow(env: Env) {
  const fileName = env.args.length ? env.args[0] : null
  if (fileName) {
    await loadFile(fileName, env)
  } else {
    loadStdin(env)
  }
}
