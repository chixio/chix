import {runAction} from '../runAction'
import {Env} from '../types'
import {logSetup} from './logSetup'
import {setFile} from './setFile'

export async function basicAction(file?: string) {
  logSetup(this)
  if (typeof file === 'string') {
    setFile(this, file)
  }
  await runAction(this as Env)
}
