import * as path from 'path'
import {Env} from '../types'

export function setFile(env: Env, file: string) {
  // bit weird determination if we have a file..
  env.parent.fileName = file
  //  typeof env.parent.args[0] === 'string' ? env.parent.args[0] : ''

  env.includePath = path.dirname(env.parent.fileName)
}
