import Logger from 'npmlog'
import {Env} from '../types'

export function logSetup(env: Env) {
  Logger.addLevel(
    'debug',
    3000,
    {
      fg: 'grey',
      bg: 'black',
    },
    'DEBUG'
  )

  if (env.parent.verbose) {
    Logger.level = 'verbose'
  } else if (env.parent.log) {
    Logger.level = env.parent.log
  } else {
    Logger.level = 'error'
  }

  env.logger = Logger
}
