/// <reference types="../types/fb-flo"/>
/// <reference types="../types/npmlog"/>
import * as flo from 'fb-flo'
import {readFile, readFileSync, exists, lstat} from 'fs-extra'
import * as Logger from 'npmlog'
import * as path from 'path'

import {runAction} from '../runAction'
import {Env} from '../types'
import {watch} from '../watch'

export async function loadFile(fileName: string, env: Env) {
  const file = path.resolve(process.cwd(), fileName)

  if (!(await exists(file))) {
    Logger.error('Invoke', 'File %s not found', file)
    process.exit(1)
  }

  if ((await lstat(file)).isDirectory()) {
    Logger.error('Invoke', 'File %s is a directory', file)
    process.exit(1)
  }

  // run action
  const contents = await readFile(file)
  await runAction(env, contents.toString('utf-8'))

  // setup watchers
  if (env.watch) {
    if (env._name === 'browserify') {
      if (!env.output) {
        Logger.error('Watcher', 'no output file specified, use --output <file>')
        process.exit(1)
      }

      if (env.browser) {
        Logger.info('FBFlo', 'started')

        // enable auto reload of generated bundle
        flo(
          path.dirname(env.output as string),
          {
            port: env.port || 8888,
            host: env.host || 'localhost',
            verbose: env.verbose,
            glob: ['*.js'],
          },
          function resolver(filepath: string, resolve: (options: any) => {}) {
            resolve({
              resourceURL: filepath,
              contents: readFileSync(env.output as string),
              update(_window: Window /*, _resourceURL */) {
                // do something smarter later
                _window.location.reload()
              },
            })
          }
        )
      }
    }

    watch(env, file)
  }
}
