import {loadFlow} from './loadFlow'
import {logSetup} from './logSetup'
import {setFile} from './setFile'

export async function flowAction(file?: string) {
  logSetup(this)
  if (typeof file === 'string') {
    setFile(this, file)
  }
  await loadFlow(this)
}
