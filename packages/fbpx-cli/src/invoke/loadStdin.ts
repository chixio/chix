import {runAction} from '../runAction'
import {Env} from '../types'

export function loadStdin(env: Env) {
  let fbpStr = ''

  process.stdin.setEncoding('utf8')
  process.stdin.on('readable', () => {
    const chunk = process.stdin.read()

    if (chunk !== null) {
      fbpStr += chunk
    }
  })

  process.stdin.on('end', async () => {
    await runAction(env, fbpStr)
  })
}
