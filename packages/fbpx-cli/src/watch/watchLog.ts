import * as Logger from 'npmlog'

export function watchLog(files: any, env: any) {
  const keys = Object.keys(files)

  keys.forEach(_path => {
    for (const file of files[_path]) {
      const p = file.replace(process.cwd() + '/', '')

      if (p.charAt(p.length - 1) !== '/') {
        // dir
        if (p === env.parent.fileName) {
          Logger.info('WatchSource', p)
        } else {
          Logger.info('WatchTrigger', p)
        }
      }
    }
  })
}
