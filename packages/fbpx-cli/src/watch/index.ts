/// <reference types="../types/gaze"/>
import * as fs from 'fs'
import {Gaze} from 'gaze'
import * as Logger from 'npmlog'
import * as path from 'path'
import {runAction} from '../runAction'
import {watchLog} from './watchLog'

export function watch(env: any, file: string) {
  // watches anything within and below the .fbp being edited.
  // e.g. watch graphs/my.fbp will watch within graphs/**.*
  const dir = path.dirname(env.parent.fileName) + '/'
  const prefix = dir ? dir + '/' : './'

  const patterns = [prefix + '**/*.+(fbp|html|hb|tpl|json)']

  const gaze = new Gaze(patterns)

  gaze.on('all', (/* event, filepath */) => {
    fs.readFile(file, async (error, contents) => {
      if (error) {
        throw error
      }
      await runAction(env, contents.toString('utf8'))
    })
  })

  gaze.watched((error: Error, files: any) => {
    if (error) {
      throw error
    }
    watchLog(files, env)
  })

  gaze.on('error', (error: Error) => {
    // Handle error here
    Logger.error(error)
  })
}
