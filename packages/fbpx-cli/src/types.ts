export interface Env {
  logger: any
  includePath: string
  tokenTable: any

  _name:
    | 'browserify'
    | 'convert'
    | 'deps'
    | 'graph'
    | 'input'
    | 'install'
    | 'run'
    | 'tokens'
    | 'watch'
    | 'list'
  target: 'phonegap' | 'init' | 'require' | 'react' | 'runtime'

  compile?: string
  purge?: boolean
  nocache?: boolean
  saveDev?: boolean
  save?: boolean
  interactive?: boolean
  step?: boolean
  action?: string
  template?: string

  dry?: boolean
  glob?: string

  // deploy
  srcDir?: string
  baseDir?: string
  deployTo?: string
  test?: string
  deploy?: boolean

  out?: string[]

  watch?: boolean
  noflo?: boolean
  uuid?: boolean
  reverse?: boolean
  parser?: any
  debug?: boolean
  standalone?: boolean
  output?: string
  browser?: boolean
  json?: boolean
  yaml?: boolean
  port?: number
  host?: string
  verbose?: boolean
  parent: {
    fileName: string
    args: any[]
    rawArgs: any[]
    log?: string
    verbose?: boolean
  }

  // serve
  flowhub?: boolean
  no_ids?: boolean
  secure?: boolean
  type?: string
  interface?: string
  loader?: 'remote' | 'npm'
  proxy?: boolean
  proxyServer?: string
  schemas?: any
  catchExceptions?: boolean
  role?: 'client' | 'server'
  secret?: string
  bail?: boolean
  args: any[]
  compat?: boolean
  start?: boolean
  graph?: string

  clear?: boolean
  list?: boolean
  register?: string
  del?: string
  login?: boolean

  show?: boolean
  create?: string

  raw?: boolean

  // input
  iips?: boolean
}
