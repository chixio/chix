export function truncate(str: string, length: number) {
  if (length < 1) {
    return ''
  }

  return str.length >= length ? str.slice(0, length - 1) + '…' : str
}
