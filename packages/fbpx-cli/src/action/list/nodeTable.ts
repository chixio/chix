import {NodeDefinition, Port} from '@chix/common'
import {forOf} from '@fbpx/lib'
import CliTable from 'cli-table'
import {truncate} from '../../util'

export function printPorts(ports: {[name: string]: Port}) {
  return forOf((name: string, port: Port) => {
    return `<${port.type}>${name}`
  }, ports).join(', ')
}

/**
 * Displays a node definition
 *
 * @param {type} node
 * @returns {Array}
 */
export function toNodeTable(node: NodeDefinition) {
  const title = `${node.ns}/${node.name}`

  const table = new CliTable({
    // head: ['Data', 'Port', 'Process']
  }) as any

  table.push([
    node.title || node.name,
    node.description ? truncate(JSON.stringify(node.description), 40) : '',
  ])

  if (node.ports) {
    if (node.ports.input) {
      table.push(['Input Ports', printPorts(node.ports.input)])
    }

    if (node.ports.output) {
      table.push(['Output Ports', printPorts(node.ports.output)])
    }
  }

  return ['\n ' + title + '\n\n', table.toString(), '\n'].join('')
}
