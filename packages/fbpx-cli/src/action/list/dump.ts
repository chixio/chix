import {NodeDefinition} from '@chix/common'
import * as YAML from 'js-yaml'
import {Env} from '../../types'
import {toNodeTable} from './nodeTable'

export function dump(node: NodeDefinition, env: Env) {
  if (env.json) {
    console.log(JSON.stringify(node, null, 2))
  } else if (env.yaml) {
    console.log(YAML.dump(node))
  } else {
    console.log(toNodeTable(node))
  }
}
