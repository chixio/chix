import {NodeDefinition} from '@chix/common'
import * as request from 'superagent'
import {Action} from '../action'
import {dump} from './dump'
import {toNodeTable} from './nodeTable'

const endpoint = 'https://api.chix.io/v1/'

export class ListAction extends Action {
  public async execute(): Promise<void> {
    let single
    const args = this.env.parent.rawArgs
    const provider = args[3]
    const ns = args[4]
    const name = args[5]
    const segments = ['nodes']

    const config = await this.getConfig()

    if (provider) {
      segments.push(provider)
    }

    if (ns) {
      segments.push(ns)
    }

    if (name) {
      single = true
      segments.push(name)
    }

    let response

    const url = segments.join('/')

    try {
      response = await request
        .get(endpoint + url)
        .set('Authorization', `Bearer ${config.get('access_token')}`)
    } catch (error: any) {
      if (error.body) {
        console.error(error.body)
      } else {
        console.error(error)
      }

      return
    }

    if (response.status === 200) {
      const json = response.body

      if (single) {
        if ((this.env as any).source) {
          console.log(json.fn)
        } else {
          dump(json, this.env as any)
        }
      } else {
        json.forEach((nodeDefinition: NodeDefinition) => {
          console.log(toNodeTable(nodeDefinition))
        })
      }
    } else {
      console.log(`${response.status}: Failed to retrieve list`)
    }
  }
}
