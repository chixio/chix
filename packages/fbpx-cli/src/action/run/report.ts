import CliTable from 'cli-table'
import {forOf} from '@fbpx/lib'
import {NodeProcess} from './onExit'

export function report(process: NodeProcess) {
  if (process.type === 'node') {
    const table = new CliTable({
      head: [
        'Identifier',
        'ns',
        'name',
        'status',
        'filled',
        'RunCount',
        'Output count',
      ],
    }) as any

    table.push([
      process.identifier,
      process.ns,
      process.name,
      process.status,
      process.filled,
      process.runCount,
      process.outputCount,
    ])

    console.log('Node:')
    console.log(table.toString())

    const portTable = new CliTable({
      head: [
        'name',
        'async',
        'state',
        'indexed',
        'default',
        'context',
        'persist',
        'fills',
        'reads',
        'RunCount',
        'connections',
      ],
    }) as any

    console.log('Ports:')
    forOf((name: string, port: any) => {
      try {
        const row = [
          name,
          port.isAsync(),
          port.isOpen() ? 'open' : 'closed',
          port.indexed,
          /*
                    port['default'] ? JSON.stringify(port['default'].read(this)) : '',
                    port.context ? JSON.stringify(port.context.read(this)) : '',
                    */
          port.default ? JSON.stringify(port.default.read()) : '',
          port.context ? JSON.stringify(port.context.read()) : '',
          port.hasPersist(),
          port.fills,
          port.reads,
          port.runCount,
          port.getConnections().length,
        ]

        portTable.push(row)
      } catch (e) {
        console.log('Error', e, port.default)
      }
    }, process.ports.input)

    console.log(portTable.toString())
  }

  if (!process.ok) {
    console.log('process', process.identifier, 'is not ok')
  }
}
