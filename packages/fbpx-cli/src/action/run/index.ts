/// <reference types="../../types/prompt" />
import {Flow as FlowDefinition} from '@chix/common'
import {PromptContextProvider} from '@chix/context-provider-prompt'
import {Flow, ProcessManager} from '@chix/flow'
import {FSLoader} from '@chix/loader-fs'
import {Actor as ActorMonitor} from '@chix/monitor-npmlog'
import 'chalk'
import * as prettyjson from 'prettyjson'
import * as prompt from 'prompt'
import {Action} from '../action'

import {onExit} from './onExit'

export class RunAction extends Action {
  public async execute(flow: FlowDefinition): Promise<void> {
    const loader = await this.getLoader()

    const iips = this.parser.renderer.getIIPs()

    await loader.load(flow)

    this.ensureNpmDependencies(loader)

    /*
    if (loader.hasDependencies('bower')) {
      var bp = new BowerProject()
      bp._analyse().spread(function (json, tree, flattened) {
        var installed = Object.keys(flattened)
        Object.keys(deps).forEach(function (mod) {
          if (installed.indexOf(mod) === -1) {
            self.logger.error([
              'Bower dependency ' + mod + ' is not installed',
              'Please, install the requirements using `fbpx install ' +
              self.env.fileName + '`'
            ].join('\n'))
            process.exit(1)
          }
        })

        self.startActor(loader, flow, iips)
      })
    } else {
    */

    await this.startActor(loader, flow, iips)
  }

  public async startActor(loader: FSLoader, flow: FlowDefinition, iips: any) {
    // TODO: enable the display of phrases
    // const actor = Actor.create('main:flow') // new Actor()
    if (!flow.ns) {
      flow.ns = 'main'
    }

    if (!flow.name) {
      flow.name = 'flow'
    }

    const actor = await Flow.create('main:flow', flow, loader)

    if (this.env.interactive) {
      actor.setContextProvider(new PromptContextProvider())
    }

    if (this.env.debug) {
      const pm = new ProcessManager({
        onExit: onExit as any, // TODO: fix me.
      })

      actor.setProcessManager(pm)

      actor.processManager.on('report', (report: any) => {
        console.log(prettyjson.render(report, {}))
      })
    }

    actor.processManager.on('error', (error: Error) => {
      console.log(prettyjson.render(error, {}))
    })

    // actor.setLoader(loader)
    // await actor.addMap(flow)

    ActorMonitor(this.logger, actor)

    if (this.env.output) {
      actor.on('output', (data: any) => {
        console.log('there is output', data)
        this.logger.info(data)
      })
    }

    if (this.env.step) {
      actor.ioHandler.hold()
    }

    if (typeof this.env.action === 'string') {
      const sub = actor.action(this.env.action)

      iips = iips.filter((iip: any) => iip.action === this.env.action)

      sub.sendIIPs(iips)

      sub.push()
    } else {
      if (!this.env.interactive) {
        actor.sendIIPs(iips)
        actor.push()
      } else {
        // context is asked
        actor.push()

        // send remaining iips
        actor.sendIIPs(iips)
      }
    }

    // there should be a ready event of the main graph
    if (this.env.step) {
      stepping()
    }

    function stepping() {
      prompt.start()

      const io = actor.ioHandler

      function getCMD() {
        prompt.get(['cmd'], (error: Error, result: any) => {
          if (error) {
            throw error
          }

          io._heldItems.forEach((item: any[], index: number) => {
            const ln = item[0]
            const dat = item[1].read()

            const template = '%s: %s:%s -> %s:%s %s' as any

            const coloredString = index === 0 ? template.green : template.gray

            // source is actor which makes it fail.
            // actor does not know it's own id.
            console.log(
              coloredString,
              index,
              actor.getNode(ln.source.id).identifier,
              ln.source.port,
              actor.getNode(ln.target.id).identifier,
              ln.target.port,
              dat
            )
          })
          if (result.cmd === 'n') {
            io.step()
            getCMD()
          }
        })
      }

      getCMD()
    }
  }
}
