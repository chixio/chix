import {report} from './report'

export type NodeProcess = {
  type: 'node' | 'flow'
  identifier: string
  ns: string
  name: string
  status: string
  filled: boolean
  runCount: number
  outputCount: number
  ports: any // Instance of chix-flow Port..
  ok: boolean
}

// TODO: was default export!
export function onExit(processes: NodeProcess[] /*, pm */) {
  processes.forEach((process: NodeProcess) => {
    // setTimeout(() => report(process), 0)
    // TODO: setTimeout/nextTick is not possible, just give a warning when the report is too big.
    report(process)
  })
}
