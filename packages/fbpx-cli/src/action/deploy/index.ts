import * as fs from 'fs-extra'
import * as path from 'path'
import * as request from 'superagent'
import {Action} from '../action'
import {XCache} from '../compile/types'

export class DeployAction extends Action {
  public async execute(): Promise<void> {
    const config = await this.getConfig()

    const xCache: XCache = JSON.parse(
      await fs.readFile(path.join(process.cwd(), 'x.json'), 'utf-8')
    )

    const username = await this.getUsername()

    let response

    const {name, description, version, repository, nodes} = xCache

    try {
      const projectUrl = `https://api.chix.io/v1/projects/${username}`

      response = await request
        .put(projectUrl)
        .send({
          name,
          description,
          version,
          repository,
          nodes,
        })
        .set('Authorization', `Bearer ${config.get('access_token')}`)
    } catch (error: any) {
      this.logger.error(error.toString(), error)

      return
    }

    if (response.status === 200) {
      this.logger.info(`Project ${name} updated successfully.`)
    } else {
      this.logger.info(`${response.status}: Failed to update project.`)
    }
  }
}
