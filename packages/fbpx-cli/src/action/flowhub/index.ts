import {Config} from '@chix/config'

import inquirer from 'inquirer'

import {Action} from '../action'

export class FlowhubAction extends Action {
  public async execute(): Promise<void> {
    if (this.env.login) {
      await this.login()
    }
  }

  private async login() {
    const config = await this.getConfig()

    console.log(
      `Note: the retrieved api key will be written to ${Config.globalConfigFile}.`
    )

    const credentials: any = await inquirer.prompt([
      {
        type: 'string',
        message: 'Flowhub User Id:',
        name: 'uid',
      },
      {
        type: 'string',
        message: 'Github Personal Access Token:',
        name: 'token',
      },
    ])

    config.set('flowhub.uid', credentials.uid)
    config.set('github.token', credentials.token)

    config.write()

    this.logger.info('Flowhub credentials have been stored.')
  }
}
