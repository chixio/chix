import Table from 'cli-table'
import {truncate} from '../../util'

export interface Runtime {
  id: string
  address: string
  protocol?: string
  type: string
  label: string
  description: string
  registered: string
  seen: string
  user: string
  secret: string | null
}

export function runtimeTable(runtimes: Runtime[]): string {
  const show = [
    'id',
    'address',
    // 'protocol',
    'type',
    'label',
    // 'description',
    'registered',
    'seen',
    // 'user',
    'secret',
  ]
  const table = new Table({
    head: show,
  })

  runtimes.forEach((runtime) => {
    table.push(
      Object.keys(runtime).reduce((row: string[], key: string) => {
        if (show.includes(key)) {
          const value = (runtime as any)[key]

          row.push(
            typeof value === 'string' && key !== 'id'
              ? truncate(value, 23)
              : `${value}`
          )
        }

        return row
      }, [] as string[])
    )
  })

  return [table.toString(), '\n'].join('')
}
