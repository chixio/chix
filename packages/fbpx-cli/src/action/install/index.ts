import {Flow} from '@chix/common'
import {Installer} from '@chix/install'
import {Action} from '../action'

export class InstallAction extends Action {
  public async execute(flow: Flow): Promise<void> {
    const loader = await this.getLoader()

    await loader.load(flow)

    const installer = new Installer({
      saveDev: this.env.saveDev,
      save: this.env.save,
    })

    await installer.load(loader.getDependencies())

    console.log('Requirements are installed...')
  }
}
