import {Action} from '../action'

export class WatchAction extends Action {
  public async execute(): Promise<void> {
    const pattern =
      typeof this.env.watch === 'string' ? this.env.watch : '**/*.fbp'

    this.logger.info('watching ' + pattern)
    this.watcher(pattern)
  }

  public watcher(pattern: string) {
    const Gaze = require('gaze').Gaze

    const gaze = new Gaze(pattern)

    gaze.on('error', (error: Error) => {
      console.error(error)
    })

    gaze.on('all', (event: string, filepath: string) => {
      this.parser.autoReset = true
      this.parser.renderer.reset()
      this.env.purge = true
      this.env.nocache = true

      if (event === 'changed') {
        // changed/added/deleted
        console.log('reloading: ' + filepath)
        // const  flow = this.parser.parseFile(filepath);
        // Runner(options, flow, fileName, renderer.getIIPs());
      }
    })
  }
}
