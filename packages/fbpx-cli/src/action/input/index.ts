import {Flow} from '@chix/common'
import {Action} from '../action'
import {toInputTable} from './toInputTable'

export class InputAction extends Action {
  public async execute(flow: Flow): Promise<void> {
    if (this.env.iips) {
      // only output iips
      const iips = this.parser.renderer.getIIPs()

      console.log(JSON.stringify(iips, null, 2))
    } else {
      const input = this.parser.renderer.getInput()

      console.log(toInputTable(flow, input))
    }
  }
}
