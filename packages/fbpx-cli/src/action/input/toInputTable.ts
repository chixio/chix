/// <reference types="../../types/dots"/>
import {Flow} from '@chix/common'
import CliTable from 'cli-table'
import {truncate} from '../../util'

/**
 * Displays the input for a graph for easy
 * viewing within the command line.
 *
 * Both IIPs and Context is shown.
 *
 * @param {type} flow
 * @param {type} input
 * @returns {Array}
 */
export function toInputTable(flow: Flow, input: any) {
  let prefix

  const title = 'Input Data' + (flow.title ? ' for ' + flow.title : '')

  const table = new CliTable({
    head: ['Data', 'Port', 'Process', 'Setting'],
  }) as any

  input.forEach((iip: any) => {
    prefix = ''
    prefix += iip.context ? '@' : ''

    if (iip.target && iip.target.hasOwnProperty('index')) {
      prefix += '[' + iip.target.index + '] '
    } else {
      prefix += ''
    }

    if (iip.target && iip.target.hasOwnProperty('index')) {
      prefix += '[' + iip.target.index + '] '
    } else {
      prefix += ''
    }

    prefix += iip.settings && iip.settings.persist ? '^' : ''

    table.push([
      truncate(JSON.stringify(iip.data), 40),
      prefix + iip.port,
      iip.process,
      iip.setting ? JSON.stringify(iip.setting) : '',
    ])
  })

  return ['\n ' + title + '\n\n', table.toString(), '\n'].join('')
}
