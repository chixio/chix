import {Action} from '../action'

import {CertManager} from '@chix/cert'
import {Config} from '@chix/config'

export class CertAction extends Action {
  public async execute(): Promise<void> {
    if (this.env.list) {
      await this.list()
    } else if (this.env.create) {
      await this.create()
    } else if (this.env.del) {
      await this.del()
    }
  }

  private async list() {
    const certificates = await this.certManager.list()

    console.log(certificates)
  }

  private async del() {
    const hostname = this.env.del as string

    if (!this.certManager.hostCertsExists(hostname)) {
      this.logger.info(`Certificate for ${hostname} does not exist`)

      process.exit()
    }

    await this.certManager.removeHostCertificate(hostname)

    this.logger.info(`Certificate for ${hostname} has been removed.`)
  }

  private async create() {
    const hostname = this.env.create as string

    if (this.certManager.hostCertsExists(hostname)) {
      this.logger.info(
        `Certificate already exists for ${hostname}, remove it first using \`fbpx cert --del <hostname>\`.`
      )
    }

    await this.certManager.getCertificate(hostname)

    this.logger.info(`Certificate for ${hostname} has been created.`)
  }

  private get certManager() {
    const certDir = `${Config.globalConfigDir}/certs`

    return new CertManager({certDir})
  }
}
