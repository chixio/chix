import {Flow} from '@chix/common'
import {Config} from '@chix/config'
import {Loader} from '@chix/loader'
import {FSLoader} from '@chix/loader-fs'
import {Loader as LoaderMonitor} from '@chix/monitor-npmlog'
import inquirer from 'inquirer'
/*
import * as Logger from 'npmlog'
import * as FBPX from '@chix/fbpx'
*/
import jwtDecode from 'jwt-decode'
import * as resolve from 'resolve'
import {Env} from '../types'

export abstract class Action {
  public logger: any // Logger
  public env!: Env
  public parser: any // FBPX
  public fail: any

  public abstract execute(flow: Flow): Promise<void>

  public ensureNpmDependencies(loader: Loader | FSLoader): void {
    if (loader.hasDependencies('npm')) {
      // ensure all requirements can be loaded
      const dependencies = loader.getDependencies('npm')

      // TODO: the `my-mod/variant` is ignored
      Object.keys(dependencies).forEach((mod) => {
        const result = this.ensureNpmDependency(mod)

        if (result instanceof Error) {
          this.logger.error(
            [
              result.message,
              'Please, install the requirements using `fbpx install ' +
                this.env.parent.fileName +
                '`',
            ].join('\n')
          )

          process.exit(1)
        }
      })
    }
  }

  public async getUsername(): Promise<string> {
    const config = await this.getConfig()

    // @ts-ignore
    const {username} = jwtDecode(config.get('access_token'))

    return username
  }

  public async getConfig(): Promise<Config<Env>> {
    if (!Config.hasGlobalConfigFile()) {
      console.log('It seems no global configuration file for chix exists.')
      console.log(`Do you want to create one at ${Config.globalConfigFile}?`)

      const confirmation = (await inquirer.prompt([
        {
          type: 'confirm',
          name: 'ok',
          message: 'Yes/No',
          default: false,
        },
      ])) as {ok: boolean}

      if (confirmation && confirmation.ok) {
        Config.createGlobalConfigFile()
      } else {
        process.exit(0)
      }
    }

    return new Config<Env>()
  }

  public ensureNpmDependency(mod: string): string | Error {
    try {
      // returns resolved path or throws otherwise
      return resolve.sync(mod, {
        basedir: process.cwd(),
      })
    } catch (error: any) {
      return error
    }
  }

  public async getDefaultProvider() {
    const config = await this.getConfig()
    return (
      config.get('defaultProvider') ||
      'https://api.chix.io/v1/nodes/{provider}/{ns}/{name}'
    )
  }

  public async getLoader(): Promise<FSLoader> {
    const config = await this.getConfig()
    const defaultProvider = await this.getDefaultProvider()
    const loader = new FSLoader({
      purge: this.env.purge,
      cache: !this.env.nocache && !this.env.compile,
      defaultProvider,
    })

    const accessToken = config.get('access_token')

    if (accessToken) {
      loader.setAuthorizationHeader(`Bearer ${accessToken}`)
    }

    LoaderMonitor(this.logger, loader)

    return loader
  }
}
