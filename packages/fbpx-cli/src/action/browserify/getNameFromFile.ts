import * as path from 'path'

export function getNameFromFile(filename: string): string {
  return path.basename(filename).split('.')[0]
}
