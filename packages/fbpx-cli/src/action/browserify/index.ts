import {Flow} from '@chix/common'
import {Compiler} from '../../compiler'
import {Action} from '../action'
import {getNameFromFile} from './getNameFromFile'

export class BrowserifyAction extends Action {
  public async execute(flow: Flow): Promise<void> {
    const loader = await this.getLoader()

    const moduleName =
      this.env.compile || flow.name || getNameFromFile(this.env.parent.fileName)

    // var iips = this.parser.renderer.getIIPs(program.action);
    const iips = this.parser.getIIPs()

    this.env.target = this.env.target || 'init'

    try {
      await loader.load(flow)

      if (this.ensureNpmDependency('@chix/flow') instanceof Error) {
        this.logger.error(
          [
            'fbpx browserify requires @chix/flow to be installed.',
            '\tRun npm install @chix/flow --save-dev to do so',
          ].join('\n')
        )

        process.exit(1)
      }

      this.ensureNpmDependencies(loader)

      const compiler = new Compiler(this.env)

      compiler.compile(moduleName, loader, flow, iips)
    } catch (error) {
      return this.fail(error)
    }
  }
}
