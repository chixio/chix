import {Flow} from '@chix/common'
import {Loader as LoaderMonitor} from '@chix/monitor-npmlog'
import * as YAML from 'js-yaml'
import {Action} from '../action'

export class DepsAction extends Action {
  public async execute(flow: Flow): Promise<void> {
    const loader = await this.getLoader()

    // Load the standard loader monitor
    LoaderMonitor(this.logger, loader)

    try {
      const {dependencies} = await loader.load(flow)

      console.log(YAML.dump(dependencies))
    } catch (error) {
      console.error(error)
    }
  }
}
