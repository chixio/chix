import {Flow} from '@chix/common'
import {ToFBPX} from '@chix/flow-tofbpx'
import * as YAML from 'js-yaml'
import {Action} from '../action'

const toFbpx = new ToFBPX()

export class ConvertAction extends Action {
  public async execute(flow: Flow): Promise<void> {
    // TODO: bare still relevant?
    let src

    // reverse is a bit of a hack?
    if (this.env.reverse) {
      // input was json
      // TODO: input was yaml
      src = toFbpx.convert(flow)
    } else {
      if (this.env.yaml) {
        src = YAML.dump(flow)
      } else {
        src = JSON.stringify(flow, null, 2)
      }
    }

    process.stdout.write(src)
  }
}
