import {Flow} from '@chix/common'
import {Config} from '@chix/config'
import inquirer from 'inquirer'
import * as request from 'superagent'
import {Action} from '../action'

export class LoginAction extends Action {
  public async execute(_flow: Flow): Promise<void> {
    const config = await this.getConfig()

    const apiUrl = process.env.CHIX_API_URL || 'https://api.chix.io/v1'
    const loginUrl = `${apiUrl}/auth/login`

    console.log(
      `Note: the retrieved api key will be written to ${Config.globalConfigFile}, your credentials will not be stored.`
    )

    const credentials = await inquirer.prompt([
      {
        type: 'string',
        message: 'Username:',
        name: 'username',
      },
      {
        type: 'password',
        message: 'Password:',
        name: 'password',
        mask: '*',
        // validate: requireLetterAndNumber
      },
    ])

    request
      .post(loginUrl)
      .type('form')
      .send(credentials)
      .end((error, response) => {
        if (error) {
          if (error.response) {
            console.log(error.response.body)
          } else {
            console.error('Error:', error.message)
          }
        } else {
          config.set('access_token', response.body.access_token)

          config.write()
        }
      })
  }
}
