import * as http from 'http'
import * as https from 'https'

import {Runtime, RuntimeManager} from '@chix/flowhub'

import {
  WebSocketProxyServerTransport,
  WebSocketProxyTransport,
  WebSocketTransport,
  WebSocketTransportOptions,
} from '@chix/transport'

import {chixServerRuntime, ChixServerRuntimeOptions} from '@chix/runtime'

import {schemas} from '@chix/fbp-protocol'

import {GraphInterface} from './graphInterface'

import {CertManager} from '@chix/cert'
import {Config} from '@chix/config'

import {Action} from '../action'

export class ServeAction extends Action {
  private baseDir = process.cwd()

  public async execute(): Promise<void> {
    if (this.env.flowhub) {
      this.env.no_ids = true
    }

    this.env.port = this.env.port || 3569
    this.env.host = this.env.host || '127.0.0.1'
    this.env.secure = this.env.secure || false

    this.env.type = 'chix-nodejs'

    const server = await this.createServer()

    let callback

    if (this.env.proxy) {
      callback = this.proxy(server)
    } else if (this.env.proxyServer) {
      callback = this.proxyServer(server)
    } else {
      callback = await this.createChixRuntimeServer(server)
    }

    server.listen(this.env.port, callback)
  }

  private get wsProtocolString() {
    return this.env.secure ? 'wss://' : 'ws://'
  }

  private async createHttpsServerOptions(): Promise<https.ServerOptions> {
    if (!Config.hasGlobalConfigFile) {
      console.log('Global config file does not exists. Run fbpx login first.')

      process.exit()
    }

    const options = {
      certDir: `${Config.globalConfigDir}/certs`,
      defaultCertAttrs: {
        countryName: 'NL',
        organizationName: 'ChixCertManager',
        ST: 'FL',
        OU: 'ChixCertManager SSL',
      },
    }
    const certManager = new CertManager(options)

    if (!certManager.rootCAFileExists) {
      await certManager.generateRootCA('FBPX')
    }

    if (!this.env.host) {
      throw Error('--host parameter required')
    }

    if (!certManager.hostCertsExists(this.env.host)) {
      console.log(`No certificate exists for ${this.env.host}, generating...`)
    }

    return certManager.getCertificate(this.env.host)
  }

  private async createFlowHubManager() {
    const config = await this.getConfig()

    const api = 'https://api.flowhub.io'

    const token = config.get('github.token')

    if (!token) {
      console.error(
        'Registration to flowhub requires a github token. Run `fbpx flowhub --login` first.'
      )

      process.exit()
    }

    const uid = config.get('flowhub.uid')

    if (!uid) {
      console.error(
        'Registration to flowhub requires a uid to be set. Run `fbpx flowhub --login` first'
      )
    }

    return new RuntimeManager({
      api,
      uid,
      token,
    })
  }

  private async createServer(): Promise<http.Server | https.Server> {
    const graphInterfaceOptions = {
      interface: this.env.interface,
    }

    const serveGraphInterface = GraphInterface(
      graphInterfaceOptions,
      this.logger
    )

    if (this.env.secure) {
      return https.createServer(
        await this.createHttpsServerOptions(),
        serveGraphInterface
      )
    } else {
      return http.createServer(serveGraphInterface)
    }
  }

  private proxy(server: http.Server | https.Server) {
    // proxy doesn't run any graph itself
    WebSocketProxyTransport.create(server, {
      logger: this.logger,
    })

    return () => {
      const listenMsg = `proxy server listening at ${this.wsProtocolString}${this.env.host}:${this.env.port}/proxy`

      this.logger.info(this.env.type, listenMsg)
    }
  }

  private proxyServer(server: http.Server | https.Server) {
    // proxy doesn't run any graph itself
    WebSocketProxyServerTransport.create(server, {
      logger: this.logger,
      url: this.env.proxyServer,
    })

    return () => {
      const listenMsg = `proxy server listening at ${this.wsProtocolString}${this.env.host}:${this.env.port}`

      this.logger.info(this.env.type, listenMsg)
    }
  }

  private async createChixRuntimeServer(server: http.Server | https.Server) {
    const loader = this.env.loader || 'remote'

    this.env.logger = this.logger

    let secret = ''
    let runtime: Runtime
    let flowhubManager: RuntimeManager

    if (this.env.flowhub) {
      flowhubManager = await this.createFlowHubManager()

      runtime = (await flowhubManager.getRuntimeBy({
        address: this.websocketUrl,
      })) as Runtime

      if (!runtime) {
        this.logger.error(
          `Could not find runtime for address ${this.websocketUrl}. Use \`fbpx flowhub --list\` to get a list of registered runtimes.`
        )

        process.exit()
      }

      secret = runtime.secret || ''
    }

    const websocketTransportOptions: WebSocketTransportOptions = {
      catchExceptions: this.env.catchExceptions,
      type: 'chix', // or chix-nodejs ?
      schemas,
      logger: this.logger,
      // role: this.env.role || 'client',
      role: this.env.role || 'server', // not sure why the default was client?
      secret,
      bail: this.env.bail,
    }

    const transport = new WebSocketTransport(websocketTransportOptions, server)

    let token

    if (loader === 'remote') {
      const config = new Config()
      token = config.get('access_token')

      if (!token) {
        this.logger.error(
          'Remote loading requires an access token, run `fbpx login` first'
        )
      }

      this.logger.info(
        this.env.type,
        'Using remote providers to load components.'
      )
    } else {
      this.logger.info(
        this.env.type,
        `Using ${this.baseDir} for component loading.`
      )
    }

    const file = this.env.parent.fileName
    /*
      this.env.parent.args.length && typeof this.env.parent.args[0] === 'string'
        ? this.env.parent.args[0]
        : ''
   */

    if (file) {
      this.logger.info(this.env.type, `Running server with graph ${file}.`)
    } else {
      this.logger.info(this.env.type, `Running server without initial graph.`)
    }

    const chixRuntimeOptions: ChixServerRuntimeOptions = {
      type: 'chix',
      // capabilities?:
      file,
      loader,
      compat: this.env.compat,
      purge: this.env.purge,
      nocache: this.env.nocache,
      start: this.env.start,
      token,
      transport,
      logger: this.logger,
      install: true,
    }

    await chixServerRuntime(chixRuntimeOptions)

    return async () => {
      const interval = 10 * 60 * 1000

      const listenMsg = ` runtime listening at ${this.websocketUrl}`

      this.logger.info(this.env.type, listenMsg)

      if (this.env.flowhub) {
        this.logger.info(
          `found flowhub runtime for ${this.websocketUrl}, pinging periodically`
        )

        // Ping Flowhub periodically to let the user know that the runtime is still available
        setInterval(async () => {
          await flowhubManager.ping(runtime.id)
        }, interval)
      }
    }
  }

  private get websocketUrl() {
    return (
      (this.env.secure ? 'wss://' : 'ws://') +
      `${this.env.host}:${this.env.port}`
    )
  }
}
