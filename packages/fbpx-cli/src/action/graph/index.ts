import {Flow} from '@chix/common'
import {ToDot} from '@chix/flow-todot'
import {Action} from '../action'

export class GraphAction extends Action {
  public async execute(flow: Flow): Promise<void> {
    const template = this.env.template || 'compact'

    console.log(ToDot(flow, this.parser.renderer.getInput(), template))
  }
}
