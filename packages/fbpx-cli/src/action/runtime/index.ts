import {Runtime, RuntimeManager} from '@chix/flowhub'

import inquirer from 'inquirer'
import * as uuid from 'uuid'

import {runtimeTable} from './runtimeTable'

import {Action} from '../action'

export class RuntimeAction extends Action {
  public async execute(): Promise<void> {
    if (this.env.list) {
      await this.list()
    } else if (this.env.del) {
      await this.del()
    } else if (this.env.register) {
      await this.register()
    } else if (this.env.clear) {
      await this.clearAll()
    }
  }

  private async getRuntimeManager(): Promise<RuntimeManager> {
    const config: any = await this.getConfig()

    if (this.env.flowhub) {
      if (!config.has('flowhub.uid') || !config.has('github.token')) {
        this.logger.error(
          'Flowhub not configured, run `fbpx flowhub --login` first.'
        )

        process.exit()
      }

      return new RuntimeManager({
        uid: config.get('flowhub.uid'),
        token: config.get('github.token'),
        api: 'https://api.flowhub.io/runtimes',
      })
    }

    return new RuntimeManager({
      token: config.get('access_token'),
      api: 'https://api.chix.io/v1/runtimes',
    })
  }

  private async register() {
    const runtime: any = await inquirer.prompt([
      {
        type: 'string',
        message: 'Runtime Type:',
        name: 'type',
        default: 'chix-nodejs',
      },
      {
        type: 'string',
        message: 'Websocket host:',
        name: 'host',
        default: 'localhost',
      },
      {
        type: 'string',
        message: 'Websocket port:',
        name: 'port',
        default: 80,
      },
      {
        type: 'string',
        message: 'Use secure websockets?:',
        name: 'secure',
        default: false,
      },
      {
        type: 'string',
        message: 'Label:',
        name: 'label',
      },
      {
        type: 'string',
        message: 'Description:',
        name: 'description',
        default: '',
      },
      {
        type: 'string',
        message: 'Secret:',
        name: 'secret',
        default: null,
      },
    ])

    const runtimeManager = await this.getRuntimeManager()

    await runtimeManager.register({
      id: this.env.flowhub ? uuid.v4() : undefined,
      address: runtime.secure
        ? `wss://${runtime.host}:${runtime.port}`
        : `ws://${runtime.host}:${runtime.port}`,
      // protocol: runtime.protocol,
      type: runtime.type,
      label: runtime.label,
      description: runtime.description,
      secret: runtime.secret,
    })
  }

  private async list() {
    const runtimeManager = await this.getRuntimeManager()

    const result = await runtimeManager.list()

    if (this.env.raw) {
      console.log(JSON.stringify(result, null, 2))
    } else {
      console.log(runtimeTable(result))
    }
  }

  private async clearAll() {
    const runtimeManager = await this.getRuntimeManager()

    await runtimeManager.clearAll()

    this.logger.info('Cleared all registered runtimes.')
  }

  private async del() {
    const labelOrId = this.env.del as string

    const runtimeManager = await this.getRuntimeManager()

    const runtimes = await runtimeManager.list()

    const found = runtimes.find(
      (runtime: Runtime) =>
        runtime.label === labelOrId || runtime.id === labelOrId
    )

    if (found) {
      await runtimeManager.del(found.id)

      this.logger.info(`Removed runtime ${found.label}`)
    }
  }
}
