import * as path from 'path'

export function getPackageName() {
  const pkg = require(path.join(process.cwd(), './package.json'))

  if (!pkg.hasOwnProperty('chix')) {
    throw Error('package.json does not contain a chix section')
  }

  if (!pkg.chix.hasOwnProperty('name')) {
    throw Error('Cannot determine chix package name')
  }

  return pkg.chix.name
}
