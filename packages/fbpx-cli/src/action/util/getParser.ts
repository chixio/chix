import {ChixRenderer, FBPParser, FSResolver} from '@chix/fbpx'

const useUUID = false

export function getParser(provider: string) {
  const parser = new FBPParser()
  const renderer = new ChixRenderer()

  renderer.setDefaultProvider(provider)

  renderer.skipIDs = !useUUID

  parser.addIncludeResolver(new FSResolver())
  parser.addRenderer(renderer)

  return parser
}
