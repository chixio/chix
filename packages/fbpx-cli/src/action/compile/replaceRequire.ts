export function replaceRequire(str: string, local?: boolean) {
  const what = !local ? "require('$1')" : "require('./$1')"

  return str.replace(/"##([\w-_]+)##"/gm, what)
}
