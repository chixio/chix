/// <reference types="../../types/mock-browser" />
import {mocks} from 'mock-browser'

const {MockBrowser} = mocks
const mock = new MockBrowser()

export const mockBrowser = () => {
  const Global: any = global

  Global.document = mock.getDocument()
  Global.window = MockBrowser.createWindow()
  Global.location = mock.getLocation()
  Global.navigator = mock.getNavigator()
  Global.history = mock.getHistory()
  Global.localStorage = mock.getLocalStorage()
  Global.sessionStorage = mock.getSessionStorage()
  Global.Element = (window as any).Element
  Global.Mottle = {
    consume: {},
  }
}
