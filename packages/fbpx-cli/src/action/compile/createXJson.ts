import * as fs from 'fs'
import {glob} from 'glob'
import * as path from 'path'
import * as util from 'util'
import {createXConfig} from './createXConfig'
import {processFiles} from './processFiles'
import {processTwigs} from './processTwigs'

const writeFile = util.promisify(fs.writeFile)
const readFile = util.promisify(fs.readFile)

export async function createXJson(
  dirname: string,
  pattern: string,
  provider: string,
  write?: boolean
) {
  // bundle() is only to check if all dependencies have been installed..
  const pkg = JSON.parse(
    await readFile(path.join(process.cwd(), '/package.json'), 'utf-8')
  )

  const x = createXConfig(pkg)

  let files

  files = await glob(`${dirname}${pattern}`)

  await processFiles(x)(files)

  files = await glob(`${dirname}/twigs/*.fbp`)

  await processTwigs(x, provider)(files)

  const contents = JSON.stringify(x, null, 2)

  if (write) {
    console.log('writing x.json')
    await writeFile(dirname + '/x.json', contents)
  } else {
    console.log(contents)
  }
}
