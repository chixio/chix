import {XCache, XPackage} from './types'

export function createXConfig(pkg: XPackage): XCache {
  const x: XCache = {
    name: pkg.chix.name,
    description: pkg.description,
    version: pkg.version,
    licences: pkg.licenses,
    repository: pkg.repository,
    dependencies: pkg.dependencies,
    nodes: [],
  }

  if (pkg.chix.env) {
    x.env = pkg.chix.env
  }

  return x
}
