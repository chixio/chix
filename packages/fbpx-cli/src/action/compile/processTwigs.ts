import {FlowSchema} from '@chix/common'
import * as fs from 'fs'
import * as jsonGate from 'json-gate'
import * as path from 'path'
import * as util from 'util'
import {getParser} from '../util'
import {XCache} from './types'

const flowSchema = jsonGate.createSchema(FlowSchema)
const readFile = util.promisify(fs.readFile)

export function processTwigs(x: XCache, provider: string) {
  return async (files: string[]) => {
    // nodes within this twig are allowed to incorporate other
    // namespaces, but his twig belongs to this namespace
    for (const file of files) {
      const parser = getParser(provider)
      console.log('processing %s', file)

      const contents = await readFile(file, 'utf-8')

      const twig = parser.parse(contents)

      const name = path.basename(file, '.fbp')

      twig.ns = x.name
      twig.name = name

      flowSchema.validate(twig)

      x.nodes.push(twig)
    }

    return x
  }
}
