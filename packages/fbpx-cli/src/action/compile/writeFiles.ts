/// <reference types="../../types/camelize" />
import {FlowSchema, NodeDefinition} from '@chix/common'
import {glob} from 'glob'
import * as path from 'path'
import * as util from 'util'
import {mockBrowser} from './mock'
import {processNodeDefinition} from './processNodeDefinition'
import {replaceRequire} from './replaceRequire'

import * as camelize from 'camelize'
import * as fs from 'fs'
import * as jsonGate from 'json-gate'
import {getPackageName, getParser} from '../util'

const flowSchema = jsonGate.createSchema(FlowSchema)

const readFile = util.promisify(fs.readFile)
const writeFile = util.promisify(fs.writeFile)

export async function writeFiles(provider: string): Promise<void> {
  mockBrowser()

  const pkgName = getPackageName()

  const files = await glob('./nodes/**/node.json')

  const nodeDefinitions: NodeDefinition[] = await Promise.all(
    files.map(processNodeDefinition)
  )

  const twigFiles = await glob('./twigs/*.fbp')

  // nodes within this twig are allowed to incorporate other
  // namespaces, but his twig belongs to this namespace
  for (const file of twigFiles) {
    console.log('processing %s', file)

    const twig = {
      ...getParser(provider).parse(await readFile(file, 'utf-8')),
      ns: pkgName,
      name: path.basename(file, '.fbp'),
    }

    flowSchema.validate(twig)

    const fileName = `${twig.name}.js`
    const twigSource = JSON.stringify(twig, null, 2)
    const jsBody = `module.exports = ${twigSource}`

    await writeFile(fileName, jsBody)

    console.log('wrote %s', fileName)

    nodeDefinitions.push(twig)
  }

  // note: ns: 'my/sub', should translate to sub/subdir
  const idx: {[key: string]: string} = {}

  // write index
  nodeDefinitions.forEach(({name}) => {
    // const parts = nodeDefinition.ns.split('/')
    // const ns = parts[0]
    // if (parts.length > 1) {
    //   recursive also, later.
    // }
    idx[name] = `##${name}##`
  })

  // note: requiring code must expect camelized properties..
  if (Object.keys(idx).length) {
    const res = camelize(idx)
    const src = [
      'module.exports = ',
      replaceRequire(JSON.stringify(res, null, 2), true).replace(/"/g, ''),
    ].join('')

    await writeFile('index.js', src)

    console.log('wrote index.js')
  }
}
