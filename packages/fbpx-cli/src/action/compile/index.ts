import {Flow as FlowDefinition} from '@chix/common'
import {Action} from '../action'
import {createXJson} from './createXJson'
import {writeFiles} from './writeFiles'

// import * as util from 'util'
// import * as Browserify from 'browserify'

// const browserify = new Browserify()
// const bundle = util.promisify(browserify.bundle)

/* options
  --glob', 'globbing pattern default: ' + gpattern)
  --dry', 'dry run')
*/
export class CompileAction extends Action {
  public async execute(_flow: FlowDefinition): Promise<void> {
    // flow
    // this.env
    // this.parser
    const dirname = process.cwd()
    const write = !this.env.dry
    const pattern = this.env.glob || '/nodes/**/node.*'

    const username = await this.getUsername()
    const provider = `https://api.chix.io/v1/nodes/${username}/{ns}/{name}`

    // await bundle()
    await writeFiles(provider)

    await createXJson(dirname, pattern, provider, write)
  }
}
