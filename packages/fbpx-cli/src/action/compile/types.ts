import {NodeDefinition, NpmDependencies} from '@chix/common'

export type XCache = {
  dependencies: NpmDependencies
  description: string
  env?: string
  version: string
  licences: any
  name: string
  nodes: NodeDefinition[]
  repository: any
}

export type XPackage = {
  chix: {
    name: string
    env?: string
  }
  description: string
  version: string
  licenses: any
  repository: any
  dependencies: NpmDependencies
  nodes: NodeDefinition[]
}
