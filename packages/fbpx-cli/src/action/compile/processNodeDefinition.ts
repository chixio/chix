/// <reference types="../../types/js-beautify" />
/// <reference types="../../types/tosource" />
import {compile} from '@chix/flow'
import * as fs from 'fs'
import * as path from 'path'
import * as util from 'util'
import {replaceRequire} from './replaceRequire'

import {js_beautify} from 'js-beautify'
import * as toSource from 'tosource'

const readFile = util.promisify(fs.readFile)
const writeFile = util.promisify(fs.writeFile)

export const processNodeDefinition = async (file: string) => {
  const nodeDefinition = JSON.parse(await readFile(file, 'utf8'))

  // do real requires
  if (nodeDefinition.dependencies && nodeDefinition.dependencies.npm) {
    Object.keys(nodeDefinition.dependencies.npm).forEach(key => {
      if (key !== 'builtin') {
        nodeDefinition.dependencies.npm[key] = '##' + key + '##'
      }
    })
  }

  // nodeDefinitions.push(nodeDefinition)

  // some node types do not have an node.js file e.g. ReactNode
  try {
    nodeDefinition.fn = await readFile(file.replace('.json', '.js'), 'utf8')
  } catch (_) {}

  const src = nodeDefinition.fn ? compile(nodeDefinition) : nodeDefinition

  let res = 'module.exports = ' + toSource(src)

  // unquote the requires
  res = replaceRequire(res)

  const fileName = [
    process.cwd(),
    '/',
    path
      .dirname(file)
      .split('/')
      .pop(),
    '.js',
  ].join('')

  await writeFile(fileName, js_beautify(res, {indent_size: 2}))

  console.log('wrote ' + fileName)

  return nodeDefinition
}
