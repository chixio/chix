import {Env, NodeDefinition, NodeSchema} from '@chix/common'
import * as fs from 'fs'
import * as jsonGate from 'json-gate'
import * as path from 'path'
import * as util from 'util'
import * as yaml from 'js-yaml'
import {XCache} from './types'

const nodeSchema = jsonGate.createSchema(NodeSchema)
const readFile = util.promisify(fs.readFile)

export function processFiles(x: XCache) {
  return async (files: string[]) => {
    const fns: {[name: string]: string} = {}

    for (const file of files) {
      const parts = file.split('/')
      const module = parts[1]
      const ext = path.extname(file)

      const contents = await readFile(file, 'utf-8')

      let node: NodeDefinition = {} as NodeDefinition

      if (ext === '.js') {
        fns[module] = contents // warning: relies on .js before .json
      } else if (ext === '.json' || ext === '.yml') {
        console.log(file)

        const json = ext === '.yml' ? yaml.load(contents) : JSON.parse(contents)

        if (!json.name) {
          throw Error('name is missing in node.json file' + module)
        }

        node = {
          ...node,
          ...json,
        }

        // If the package specifies an environment
        // also add this to each node definition within this package
        // yet leave an option to override it.
        if (x.env && !node.env) {
          node.env = x.env as Env
        }

        node.ns = node.ns ? node.ns : x.name // add namespace if not specified by the node itself

        if (fns[module]) {
          node.fn = fns[module]

          nodeSchema.validate(node)
        }

        x.nodes.push(node)
      }
    }

    return x
  }
}
