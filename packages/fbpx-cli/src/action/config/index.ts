import * as yaml from 'js-yaml'

import {Action} from '../action'

export class ConfigAction extends Action {
  public async execute(): Promise<void> {
    if (this.env.show) {
      await this.show()
    } else {
      if (this.env.args.length) {
        const args = this.env.args.slice()
        const subAction = args.shift()
        const config = await this.getConfig()
        if (subAction === 'set') {
          const [name, value] = args
          config.set(name, value)
          await config.write()
        } else if (subAction === 'get') {
          const [name] = args
          console.log(config.get(name))
        }
      }
    }
  }

  private async show() {
    const config = await this.getConfig()

    console.log(yaml.dump(config.config))
  }
}
