import {Flow as FlowDefinition, NodeDefinition} from '@chix/common'
import {Action} from '../action'

import * as fs from 'fs'
import * as _s from 'underscore.string'

import * as util from 'util'
import {Defaults, InputPorts, Nodes} from './types'
import {writeNodes} from './writeNodes'

const dirname = process.cwd()

const readFile = util.promisify(fs.readFile)
const exists = util.promisify(fs.exists)

export function output_option(val: string) {
  return val.split(':')
}

export class GenerateAction extends Action {
  public async execute(_flow: FlowDefinition): Promise<void> {
    /*
      .option('-d, --dry', 'dry run')
      .option('-o, --out <out>:<type>', 'output port', output_option)
      .parse(process.argv)
    */

    const fileName = this.env.parent.fileName

    if (!fileName) {
      console.log('you should provide a filename')
      process.exit()
    }

    const file = `${dirname}/${fileName}`

    if (!(await exists(file))) {
      console.log('file:', file, 'does not exist')
      process.exit()
    }

    const write = !this.env.dry

    let output_port = 'results'
    let output_type = 'object'

    if (!this.env.out) {
      throw Error('--out is not set.')
    }

    if (this.env.out.length) {
      output_port = this.env.out[0]
      if (this.env.out[1]) {
        output_type = this.env.out[1]
      }
    }

    const defaults: Defaults = {
      fd: 'object',
      buffer: 'object',
      options: 'object',
      family: 'integer',
      len: 'integer',
      uid: 'integer',
      gid: 'integer',
      mode: 'integer',
      offset: 'integer',
      length: 'integer',
      position: 'integer',
    }

    try {
      const res = await readFile(file)

      const lines = res.toString().split('\n')

      const nodes: Nodes = {}

      for (const line of lines) {
        if (line !== '') {
          if (!write) {
            console.log('line:', line)
          }

          const m: any = line.match(/^(\w+).(\w+)\((.*)\)/) as RegExpMatchArray

          if (!write) {
            console.log('match:', m)
          }

          if (!m || Array.isArray(m)) {
            throw Error('Unable to match line.')
          }

          let args = m
            .pop()
            .replace(/[\[\]]/g, '')
            .replace(/\s+/g, '')
            .split(',')

          if (!write) {
            console.log('args:', args)
          }

          const cb = args.pop().replace(/\s+/, '')

          if (!write) {
            console.log('last arg', cb, '\n')
          }

          let isAsync = true

          if (cb !== 'callback') {
            // synchronous function, format will be {} instead of array
            args.push(cb) // put it back
            isAsync = false
          }

          const module = m[1]
          const method = m[2]

          const input_ports: InputPorts = {}

          for (const arg of args) {
            const type = defaults[arg] ? defaults[arg] : 'string'

            input_ports[arg] = {type}
          }

          const description =
            _s.humanize(module) + ' ' + _s.humanize(method).toLowerCase()

          const json: NodeDefinition = {
            ns: '',
            name: module + '.' + method,
            description,
            ports: {
              input: input_ports,
              output: {
                [output_port]: {
                  type: output_type,
                },
              },
            },
          }

          args = 'input.' + args.join(', input.')

          nodes[method] = {
            json,
            fn: isAsync
              ? 'output([' + module + '.' + method + ', ' + args + '])'
              : // look like: output({ results: path.normalize(input.path) })
                'output({ ' +
                output_port +
                ': ' +
                module +
                '.' +
                method +
                '(' +
                args +
                ') })',
          }
        }
      }

      if (write) {
        await writeNodes(nodes)
      } else {
        console.log('nodes', nodes)
      }
    } catch (_error) {
      throw Error('Error cannot read file ' + file)
    }
  }
}
