export type Nodes = {
  [method: string]: {
    json: any
    fn: string
  }
}

export type InputPorts = {
  [type: string]: {
    type: string
  }
}

export type Defaults = {
  [key: string]: string
}
