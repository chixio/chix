import * as fs from 'fs'
import * as util from 'util'
import {Nodes} from './types'

const dirname = process.cwd()

const writeFile = util.promisify(fs.writeFile)
const exists = util.promisify(fs.exists)
const mkdir = util.promisify(fs.mkdir)

export async function writeNodes(nodes: Nodes) {
  for (const method in nodes) {
    if (nodes.hasOwnProperty(method)) {
      const node = nodes[method]
      const exist = await exists(dirname + '/' + method)

      if (!exist) {
        await mkdir(dirname + '/' + method)
      }

      // create node.js
      let f = dirname + '/' + method + '/node.js'

      await writeFile(f, node.fn)

      console.log('Written:', f)

      // create node.json
      f = dirname + '/' + method + '/node.json'

      const json = JSON.stringify(node.json, null, 2)

      await writeFile(f, json)

      console.log('Written:', f)
    }
  }
}
