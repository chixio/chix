/// <reference types="./types/npmlog"/>
import {Flow} from '@chix/common'
import {Loader} from '@chix/loader'
import {FSLoader} from '@chix/loader-fs'
import browserify from 'browserify'
import * as fs from 'fs'
import * as hb from 'handlebars'
import * as Logger from 'npmlog'
import * as path from 'path'
import through from 'through2'
import {Env} from './types'

export type TargetDef = {
  entry: boolean
}

export class Compiler {
  public env: Env
  public targets = {
    phonegap: {
      entry: true,
    },
    init: {
      entry: true,
    },
    require: {
      entry: false,
    },
    react: {
      entry: true,
    },
    runtime: {
      entry: true,
    },
  }

  public target: TargetDef
  public template: string

  /**
   * Compiles a graph into a browserified bundle.
   *
   * @param {Object} env
   * @returns {Compiler}
   */
  constructor(env: Env) {
    this.env = env

    Logger.level = 'info'

    const targetName = env.target || 'init'

    this.target = this.targets[targetName]

    if (Object.keys(this.targets).indexOf(targetName) === -1) {
      throw Error('Unknown target: ' + targetName)
    }

    this.template = targetName + '.js.tpl'
  }

  /**
   * Compile the graph as module
   *
   * @param {type} moduleName
   * @param {type} loader
   * @param {type} map
   * @param {type} iips
   */
  public compile(
    moduleName: string,
    loader: Loader | FSLoader,
    map: Flow,
    iips: any
  ) {
    if (!moduleName) {
      throw Error('module name required')
    }

    const tpl = hb.compile(
      fs
        .readFileSync(
          path.resolve(__dirname, 'templates', this.template),
          'utf8'
        )
        .toString()
    )

    const body = tpl({
      // TODO: disabled, templates still use them though, so fix that
      //  nodes: JSON.stringify(loader.nodes),
      nodeDefinitions: JSON.stringify(loader.getNodeDefinitions()),
      dependencies: JSON.stringify(loader.getDependencies()),
      defaultProvider: this.env.parser.renderer.defaultProvider.url,
      map: JSON.stringify(map),
      iips: JSON.stringify(iips),
      debug: this.env.debug,
    })

    /*
      if (this.env.debug) {
        loader.dependencies.npm['chix-monitor-npmlog'] = 'latest';
      }
    */

    const compile = browserify()

    if (loader.getDependencies('npm')) {
      Object.keys(loader.hasDependencies('npm')).forEach((pkg) => {
        const name = pkg.split('@')[0]

        compile.require(name, {
          // basedir: '',
          expose: name,
        })
      })
    }

    // TODO: test the expose!
    compile.require('@chix/flow', {
      // basedir: '',
      expose: 'chix-flow',
    })

    if (this.env.debug) {
      // TODO: test the expose!
      // compile.require('chix-monitor-npmlog', {
      compile.require('@chix/monitor-npmlog', {
        // basedir: '',
        expose: 'chix-monitor-npmlog',
      })
      compile.require('debug', {
        expose: 'debug',
      })
    }

    // TODO: this hack is what makes it impossible to upgrade browserify, so fix and upgrade
    const stream = through() as any

    stream.push(body)
    stream.push(null)

    stream.id = moduleName

    compile
      .require(stream, {
        entry: this.target.entry,
      })
      .bundle((err, src) => {
        if (err) {
          throw Error(err)
        }

        let _src = src.toString('utf-8')

        if (this.env.standalone) {
          _src += ";module.exports = require('" + moduleName + "');"
        }

        if (this.env.output) {
          fs.writeFile(this.env.output, _src, (error) => {
            if (error) {
              throw error
            } else {
              Logger.info('Browserify', 'wrote %s', this.env.output)
            }
          })
        } else {
          process.stdout.write(src)
        }
      })
  }
}
