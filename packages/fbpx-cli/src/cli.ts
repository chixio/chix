process.env.NODE_PATH = [
  process.cwd() + '/node_modules',
  process.env.NODE_PATH,
].join(':')

import {Command} from 'commander'

import {basicAction} from './invoke/basicAction'
import {flowAction} from './invoke/flowAction'

// tslint:disable-next-line:no-var-requires
const pkg = require('../package')
const program = new Command()

program
  .version(pkg.version, '-V, --version')
  .usage('[options] [file ...]')
  .option('-v, --verbose', 'verbose output')
  .option('-C, --nocache', 'do not read/write cache')
  .option('-P, --purge', 'purge cache file')

// run
program
  .command('run [file]')
  .description('Run graph')
  .alias('r')
  .option('-i, --interactive', 'prompt for input data')
  .option('-d, --debug', 'show debug data')
  .option('-s, --step', 'step through node execution')
  .option('-v, --verbose', 'be verbose')
  .option('-a, --action [name]', 'run an specific action')
  .option('-o, --output', 'log all output port data')
  .option('-w, --watch', 'Watch file path for changes')
  .action(flowAction)

program
  .command('graph [file]')
  .alias('g')
  .option(
    '-t, --template [name]',
    'dot template to use (compact|simple)',
    'compact'
  )
  .description('Show Graph')
  .action(flowAction)

program
  .command('input')
  .description('Show input data')
  .option('-i, --iips', 'Only show IIPs (as JSON)')
  .option('-w, --watch', 'Watch file path for changes')
  .action(flowAction)

program
  .command('login')
  .description('Login to api.chix.io to set token')
  .action(basicAction)

program
  .command('deps')
  .description('Show dependencies')
  .option('-w, --watch', 'Watch file path for changes')
  .action(flowAction)

program
  .command('deploy')
  .description('Deploy nodes to api.')
  .action(basicAction)

program
  .command('tokens')
  .description('Debug lexer tokens')
  .option('-w, --watch', 'Watch file path for changes')
  .action(flowAction)

program
  .command('list')
  .description('List available nodes')
  .alias('info')
  .option('-j, --json', 'Output as json')
  .option('-y, --yaml', 'Output as yaml')
  .option('-s, --source', 'show source')
  .action(basicAction)

program
  .command('compile')
  .description('Compile nodes')
  .option('--dry', 'Dry run.')
  .option('--glob', 'Globbing pattern')
  .action(basicAction)

/*
program
  .command('update')
  .alias('u')
  .description('Update node definitions & requirements')
  .action(flowAction);
// watch
program
  .command('watch')
  .alias('w')
  .description('watch and reload fbp on changes')
  .option('-a, --action [name]', 'run an specific action')
  .option('-o, --output', 'log all output port data')
  .option('-p, --pattern [pattern]', 'watch and reload fbp on changes')
  .action(flowAction);
*/

program
  .command('convert')
  .alias('c')
  .description('Convert file')
  .option('-j, --json', 'JSON Format')
  .option('-y, --yaml', 'YAML Format')
  .option('-b, --bare', 'Do not validate')
  .option('-u, --uuid', 'generate uuids')
  .option('-n, --noflo', 'show JSON (noflo)')
  .action(flowAction)

program
  .command('install')
  .alias('i')
  .option('-s, --save', 'save dependencies in package.json')
  .option('-d, --save-dev', 'save as dev dependencies in package.json')
  .description('Load node definitions & install requirements')
  .action(flowAction)

// compile
program
  .command('browserify [file]')
  .alias('brsf')
  .description('Create a browserified bundle')
  .option('-c, --compile [modulename]', 'compile bundle to stdout')
  .option('-s, --standalone', 'Browserify Standalone')
  .option(
    '-w, --watch',
    [
      'Watch file path for changes,',
      'must be used together with the -o option',
    ].join('\n')
  )
  .option('-b, --browser', 'Use fb-flo extension to reload generated bundle')
  .option('-o, --output [file]', 'Output file')
  .option('-t, --target [name]', 'target to bundle for')
  .option('-d, --debug', 'compile with debug mode')
  .action(flowAction)

program
  .command('flowhub')
  .option('-l, --login', 'login to flowhub.')
  .action(basicAction)

program
  .command('runtime')
  .option('-r, --register', 'register runtime.')
  .option('-c, --clear', 'clear all registered runtimes.')
  .option('-l, --list', 'list all registered runtimes.')
  .option('-d, --del <labelOrId>', 'remove a runtime by label or id.')
  .option('-j, --raw', 'print raw json output instead of tables.')
  .option('-f, --flowhub', 'target flowhub service.')
  .action(basicAction)

program
  .command('cert')
  .option('-c, --create <hostname>', 'Create a SSL certificate for host.')
  .option('-d, --del <hostname>', 'Remove SSL certificate for host.')
  .option('-l, --list', 'List all SSL certificates.')
  .action(basicAction)

program
  .command('config')
  .option('-s, --show', 'Show configuration.')
  .action(basicAction)

program
  .command('serve [file]')
  .usage('[options] <file>')
  .option('-t, --type [name]', 'Runtime type chix, noflo-nodejs', 'chix-nodejs')
  .option('-c, --cache', 'cache components')
  .option('-x, --compat', 'stay compatible with noflo format')
  .option('-p, --proxy', 'expose /proxy to proxy requests to e.g. a browser')
  .option('-r, --proxy-server [runtime-url]', 'proxy nodejs runtime')
  .option('-s, --secure', 'secure wss or ws')
  .option('-n, --start', 'start graph immediately (now)')
  .option('-u, --uppercase', 'force ports uppercase')
  .option('-h, --host [name]', 'hostname')
  .option('-p, --port [name]', 'port')
  .option('-I, --no-ids', 'do not use uuids')
  .option('-b, --bail', 'bail on errors')
  .option('-i, --interface [name]', 'choose a webserver interface')
  .option('-l, --loader [loader]', 'loader `remote` or `npm`', 'npm')
  .option('-L, --loglevel [level]', 'Log level')
  // perhaps --flowhub default, --flowhub example, to pick runtime by their name/label
  .option('-f, --flowhub', 'ping flowhub')
  .option('-v, --verbose', 'verbose output')
  .allowUnknownOption() // allow node debugger options
  .action(basicAction)

// general options
program.option('-l, --log [level]', 'log [level]')

if (!module.parent) {
  program.parse(process.argv)
}

export default program
