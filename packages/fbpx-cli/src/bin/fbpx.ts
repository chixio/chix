import childProcess from 'child_process'
import cli from '../cli'

const {argv} = process

function debug(args: any) {
  return args.reduce(
    (result: any, arg: any) => {
      if (arg.indexOf('--inspect') >= 0) {
        result.inspectArgs.push(arg)
      } else {
        result.args.push(arg)
      }

      return result
    },
    {
      args: [],
      inspectArgs: [],
    }
  )
}

if (argv.indexOf('--inspect') >= 0) {
  const args = debug(argv.slice(2))

  const cmd = `node ${args.inspectArgs.join(' ')} ${
    argv[1]
  }.js ${args.args.join(' ')}`

  childProcess.spawnSync(cmd, {
    shell: true,
    stdio: 'inherit',
  })
} else {
  cli.parse(process.argv)
}
