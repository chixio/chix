var Loader = function() {
  // will be replaced with the json.
  this.dependencies = {{{dependencies}}};
  this.nodes = {{{nodes}}};
  this.nodeDefinitions = {{{nodeDefinitions}}};
};

Loader.prototype.hasNodeDefinition = function(nodeId) {
  return !!this.nodes[nodeId];
};

Loader.prototype.getNodeDefinition = function(node) {
  var provider = this.nodes[node.id].provider;

  if (!this.nodeDefinitions[provider]) {
    throw new Error('Node Provider not found for ' + node.name);
  }

  return this.nodeDefinitions[provider][node.ns][node.name];
};

var Flow = require('@chix/flow').Flow;
var loader = new Loader();
var map = {{{map}}};
var actor = Flow.create(map, loader);
actor.run();

{{#if iips}}
  actor.sendIIPs({{iips}});
{{/if}}

module.exports = actor.expose.bind(actor);
