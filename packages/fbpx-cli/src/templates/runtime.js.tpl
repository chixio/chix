var util = require('util');
var Loader = require('@chix/loader').Loader;

var StaticLoader = function() {

  Loader.apply(this, arguments);

  // will be replaced with the json.
  this.dependencies = {{{dependencies}}};
  //this.nodes = {{{nodes}}};
  this.nodeDefinitions = {{{nodeDefinitions}}};

 this.defaultProvider = '{{defaultProvider}}';

 // nifty: link @ to default provider
 this.nodeDefinitions['@'] = this.nodeDefinitions[this.defaultProvider];

};
/*
StaticLoader.prototype.hasNodeDefinition = function(nodeId) {

  return !!this.nodes[nodeId];

};

StaticLoader.prototype.getNodeDefinition = function(node, map) {

  if (!this.nodeDefinitions[node.provider]) {

    // needed for subgraphs
    if (map.providers && map.providers[node.provider]) {
      node.provider = map.providers[node.provider].path ||
        map.providers[node.provider].url;
    } else if (map.providers && map.providers['@']) {
      node.provider = map.providers['@'].path ||
        map.providers['@'].url;
    } else {
      throw new Error('Node Provider not found for ' + node.name);
    }
  }

  return this.nodeDefinitions[node.provider][node.ns][node.name];

};
*/

util.inherits(StaticLoader, Loader);

var Flow = require('@chix/flow').Flow;
var Runtime = require('@chix/runtime').Runtime;
var Transport = require('@chix/transport').WindowTransport;
var loader = new StaticLoader();
var log = undefined;

// which transport...
// supposed to work both ways if role is defined
// default role *is* server
// this is not about websockets..
//
var transport = new Transport({
  // logger: log // do not use logger for now, could be npmlog.
  protocol: 'chix-runtime'
});

var map = {{{map}}};

window.runtime = Runtime(transport, log, loader, {logger: log});

var start = true;
{{#debug}}
  start = false;
{{/debug}}

function onDeviceReady() {
  window.runtime.gh.initMap(map, start, {{{iips}}}, true);
};

if (navigator.userAgent.match(/(iPhone|iPod|iPad|Android|BlackBerry|IEMobile)/)) {
  document.addEventListener("deviceready", onDeviceReady, false);
} else {
  document.addEventListener("DOMContentLoaded" , onDeviceReady); //this is the browser
}
