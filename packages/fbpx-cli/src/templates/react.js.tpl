var Loader = function() {
  // will be replaced with the json.
  this.dependencies = {{{dependencies}}};
  //this.nodes = {{{nodes}}};
  this.nodeDefinitions = {{{nodeDefinitions}}};
};

Loader.prototype.hasNodeDefinition = function(nodeId) {
  return !!this.nodes[nodeId];
};

Loader.prototype.getNodeDefinition = function(node, map) {
  if (!this.nodeDefinitions[node.provider]) {
    // needed for subgraphs
    if (map.providers && map.providers[node.provider]) {
      node.provider = map.providers[node.provider].path ||
        map.providers[node.provider].url;
    } else if (map.providers && map.providers['@']) {
      node.provider = map.providers['@'].path ||
        map.providers['@'].url;
    } else {
      throw new Error('Node Provider not found for ' + node.name);
    }
  }

  return this.nodeDefinitions[node.provider][node.ns][node.name];
};

var Flow = require('@chix/flow').Flow;
var Installer = require('@chix/install').Installer;
var ReactNode = require('@chix/node-react').ReactNode;
var loader = new Loader();

var map = {{{map}}};

debugger;
Flow.registerNodeType(ReactNode);

const installer = new Installer()

installer.load(loader.dependencies).then((result) => {
  console.log(result);
  return Flow.create('main', map, loader).then((actor) => {
     window.Actor = actor;

     console.log('Requirements are installed...')

{{#debug}}
     var monitor = require('@chix/monitor-npmlog').Actor;
     monitor(console, actor);
     localStorage.debug = 'chix:*'
{{/debug}}

     actor.push();
     actor.sendIIPs({{{iips}}});
  });
})

