var Loader = function() {

  // will be replaced with the json.
  this.dependencies = {{{dependencies}}};
  //this.nodes = {{{nodes}}};
  this.nodeDefinitions = {{{nodeDefinitions}}};

};

Loader.prototype.hasNodeDefinition = function(nodeId) {

  return !!this.nodes[nodeId];

};

Loader.prototype.getNodeDefinition = function(node) {

  var provider = this.nodes[node.id].provider;

  if (!this.nodeDefinitions[provider]) {
    throw new Error('Node Provider not found for ' + node.name);
  }

  return this.nodeDefinitions[provider][node.ns][node.name];

};

var Flow = require('chix-flow').Flow;
var loader = new Loader();

var map = {{{map}}};

var actor = Flow.create(map, loader);

function onDeviceReady() {

actor.sendIIPs({{{iips}}});
actor.push();

};

(function(document) {
  'use strict';

  document.addEventListener('polymer-ready', function() {
    onDeviceReady();
  });

// wrap document so it plays nice with other libraries
// http://www.polymer-project.org/platform/shadow-dom.html#wrappers
})(wrap(document));

// for entry it doesn't really matter what is the module.
// as long as this module is loaded.
module.exports = actor;
