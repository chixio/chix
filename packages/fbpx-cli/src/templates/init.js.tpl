var Loader = function() {
  this.dependencies = {{{dependencies}}};
  this.nodeDefinitions = {{{nodeDefinitions}}};
};

Loader.prototype.hasNodeDefinition = function(nodeId) {
  return !!this.nodes[nodeId];
};

Loader.prototype.getNodeDefinition = function(node, map) {
  if (!this.nodeDefinitions[node.provider]) {
    // needed for subgraphs
    if (map.providers && map.providers[node.provider]) {
      node.provider = map.providers[node.provider].path ||
        map.providers[node.provider].url;
    } else if (map.providers && map.providers['@']) {
      node.provider = map.providers['@'].path ||
        map.providers['@'].url;
    } else {
      throw new Error('Node Provider not found for ' + node.name);
    }
  }

  return this.nodeDefinitions[node.provider][node.ns][node.name];
};

var Flow = require('@chix/flow').Flow;
var loader = new Loader();

var map = {{{map}}};

var actor;
window.Actor = actor = Flow.create('main', map, loader);

{{#debug}}
var monitor = require('@chix/monitor-npmlog').Actor;
monitor(console, actor);
{{/debug}}

function onDeviceReady() {
  actor.push();
  actor.sendIIPs({{{iips}}});
};

if (navigator.userAgent.match(/(iPhone|iPod|iPad|Android|BlackBerry|IEMobile)/)) {
  document.addEventListener("deviceready", onDeviceReady, false);
} else {
  document.addEventListener("DOMContentLoaded" , onDeviceReady); //this is the browser
}

// for entry it doesn't really matter what is the module.
// as long as this module is loaded.
module.exports = actor;
