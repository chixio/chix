var Loader = function() {

  // will be replaced with the json.
  this.dependencies = {{{dependencies}}};
  this.nodes = {{{nodes}}};
  this.nodeDefinitions = {{{nodeDefinitions}}};

};

Loader.prototype.hasNodeDefinition = function(nodeId) {

  return !!this.nodes[nodeId];

};

Loader.prototype.getNodeDefinition = function(node) {

  var provider = this.nodes[node.id].provider;

  if (!this.nodeDefinitions[provider]) {
    throw new Error('Node Provider not found for ' + node.name);
  }

  return this.nodeDefinitions[provider][node.ns][node.name];

};

var Actor = require('chix-flow').Actor,
   loader = new Loader();

var actor = new Actor();

function onDeviceReady() {
  actor.addLoader(loader);
  actor.addMap({{{map}}}).then(() => {
    actor.run();
    actor.sendIIPs({{iips}});
    actor.push();
  })
};

if (navigator.userAgent.match(/(iPhone|iPod|iPad|Android|BlackBerry|IEMobile)/)) {
  document.addEventListener("deviceready", onDeviceReady, false);
} else {
  onDeviceReady(); //this is the browser
}

// for entry it doesn't really matter what is the module.
// as long as this module is loaded.
module.exports = actor;
