[![Build Status](https://travis-ci.org/psichi/fbpx-cli.png?branch=master)](https://travis-ci.org/psichi/fbpx-cli)

# fbpx command line interface

fbpx is the command line interface for Chiχ Flow

Usage:

```
  Usage: fbpx [options] <file ...>

  Commands:

    run|r [options] <filename>        Run graph
    graph|g [options] <filename>      Show Graph
    input                             Show input data
    deps                              Show dependencies
    tokens                            Debug lexer tokens
    convert|c [options]               Convert file
    install|i                         Load node definitions & install requirements
    browserify|brsf [options] <file>  Create a browserified bundle

  Options:

    -h, --help         output usage information
    -V, --version      output the version number
    -v, --verbose      verbose output
    -C, --nocache      do not read/write cache
    -P, --purge        purge cache file
    -l, --log [level]  log [level]

```

Install graph dependencies:

 `fbpx install graphs/clock.fbp`

Run server side graph:

 `fbpx install graphs/server_side_graph.fbp`

Browserify graph bundle + watch:

 `fbpx browserify graphs/clock.fbp --watch -o example/bundle.js`

Browserify graph bundle + watch + debug:

 `fbpx browserify graphs/clock.fbp --debug --watch -o example/bundle.js`


For vim & watch you might have to do `:set nowb` otherwise during a save
the file will not be found.
