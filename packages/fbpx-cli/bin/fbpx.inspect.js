#!/usr/bin/env node

const childProcess = require('child_process')

const { argv } = process

function debug(args) {
  return args.reduce((result, arg) => {
    if (arg.indexOf('--inspect') >= 0) {
      result.inspectArgs.push(arg)
    } else {
      result.args.push(arg)
    }

    return result
  }, {
    args: [],
    inspectArgs: []
  })
}

if (argv.indexOf('--inspect') >= 0) {
  const args = debug(argv.slice(2))

  const cmd = `node ${args.inspectArgs.join(' ')} ${argv[1]}.js ${args.args.join(' ')}`

  childProcess.spawnSync(cmd, {
      shell: true,
      stdio: 'inherit'
    }
  )
} else {
  require('../lib/cli')
    .default
    .parse(process.argv)
}
