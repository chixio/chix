# Chiχ Cert

Manages certificate generation for development.

Mainly used to support chix-runtime and provide an easy way to manage certificates for local development.

Code is adapted from: https://github.com/ottomao/node-easy-cert