import {CertManager} from './CertManager'

const options = {
  certDir: './tmp',
  defaultCertAttrs: {
    countryName: 'NL',
    organizationName: 'ChixCertManager',
    ST: 'FL',
    OU: 'ChixCertManager SSL',
  },
}

const crtMgr = new CertManager(options)

if (!crtMgr.rootCAFileExists) {
  crtMgr
    .generateRootCA('theNameYouLike')
    .then(() => {
      console.log('Generated Root Certificate')
    })
    .catch((error) => {
      console.error(error)
    })
}

crtMgr.getCertificate('localhost').then((cert) => {
  console.log(cert)
})

crtMgr.getCertificate('192.168.1.1').then((cert) => {
  console.log(cert)
})
