import {exec} from 'child_process'
import * as fs from 'fs-extra'
import {glob} from 'glob'
import * as https from 'https'
import * as path from 'path'

import {AttributeConfig, CertGenerator} from './CertGenerator'

import {
  deleteFolderContentsRecursive,
  getPort,
  // ifWinRootCATrusted,
} from './util'

export interface KeyCertPair {
  key: string
  cert: string
}

export type HostCertificate = KeyCertPair & {hostname: string}

export interface CertManagerOptions {
  certDir: string
  defaultCertAttrs?: AttributeConfig
}

export class CertManager {
  private certDir: string
  private rootCAcrtFilePath: string
  private rootCAkeyFilePath: string
  private cacheRootCACrtFileContent: string | undefined
  private cacheRootCAKeyFileContent: string | undefined
  private rootCAExists: boolean = false
  private certGenerator = new CertGenerator()

  constructor(options: CertManagerOptions) {
    this.certDir = path.resolve(options.certDir)

    if (options.defaultCertAttrs) {
      this.certGenerator.setDefaultAttrs(options.defaultCertAttrs)
    }

    this.rootCAcrtFilePath = path.join(this.certDir, 'rootCA.crt')
    this.rootCAkeyFilePath = path.join(this.certDir, 'rootCA.key')

    if (!fs.existsSync(this.certDir)) {
      fs.ensureDirSync(this.certDir)
    }
  }

  public async getCertificate(hostname: string): Promise<KeyCertPair> {
    if (!this._checkRootCA()) {
      throw Error(
        'please generate root CA before getting certificate for sub-domains'
      )
    }

    const keyFile = this.hostKeyFile(hostname)
    const crtFile = this.hostCertFile(hostname)

    if (!this.cacheRootCACrtFileContent || !this.cacheRootCAKeyFileContent) {
      this.cacheRootCACrtFileContent = await fs.readFile(
        this.rootCAcrtFilePath,
        'utf8'
      )
      this.cacheRootCAKeyFileContent = await fs.readFile(
        this.rootCAkeyFilePath,
        'utf8'
      )
    }

    if (!this.hostCertsExists(hostname)) {
      const result = this.certGenerator.generateCertsForHostname(hostname, {
        cert: this.cacheRootCACrtFileContent,
        key: this.cacheRootCAKeyFileContent,
      })

      await Promise.all([
        fs.writeFile(keyFile, result.privateKey),
        fs.writeFile(crtFile, result.certificate),
      ])

      return {
        key: result.privateKey,
        cert: result.certificate,
      }
    }

    return {
      key: fs.readFileSync(keyFile, 'utf8'),
      cert: fs.readFileSync(crtFile, 'utf8'),
    }
  }

  public clearCerts() {
    return deleteFolderContentsRecursive(this.certDir)
  }

  public hostCertFile(hostname: string) {
    return path.join(this.certDir, `${hostname}.crt`)
  }

  public hostKeyFile(hostname: string) {
    return path.join(this.certDir, `${hostname}.key`)
  }

  public async list(): Promise<HostCertificate[]> {
    const files = await glob(`${this.certDir}/*.crt`)

    return files.reduce((hostFiles, file) => {
      const hostname = path.basename(file, '.crt')

      if (hostname !== 'rootCA') {
        hostFiles.push({
          hostname,
          key: fs.readFileSync(this.hostKeyFile(hostname), 'utf8'),
          cert: fs.readFileSync(this.hostCertFile(hostname), 'utf8'),
        })
      }

      return hostFiles
    }, [] as HostCertificate[])
  }

  public hostCertsExists(hostname: string): boolean {
    const keyFile = this.hostKeyFile(hostname)
    const crtFile = this.hostCertFile(hostname)

    return fs.existsSync(keyFile) && fs.existsSync(crtFile)
  }

  public async removeHostCertificate(hostname: string): Promise<boolean> {
    const keyFile = this.hostKeyFile(hostname)
    const crtFile = this.hostCertFile(hostname)

    const keyExists = await fs.pathExists(keyFile)
    const crtExists = await fs.pathExists(crtFile)

    let removed = false

    if (keyExists) {
      await fs.remove(keyFile)

      removed = true
    }

    if (crtExists) {
      await fs.remove(crtFile)

      removed = true
    }

    return removed
  }

  public rootCAFileExists() {
    return (
      fs.existsSync(this.rootCAcrtFilePath) &&
      fs.existsSync(this.rootCAkeyFilePath)
    )
  }

  public async generateRootCA(
    commonName: string,
    overwrite: boolean = false
  ): Promise<void> {
    if (this.rootCAFileExists()) {
      if (overwrite) {
        await this._generateRootCA(commonName)
      } else {
        throw Error('The rootCA exists already and overwrite is false.')
      }
    } else {
      await this._generateRootCA(commonName)
    }
  }

  public async ifRootCATrusted(): Promise<boolean> {
    if (!this.rootCAFileExists()) {
      throw Error('RootCa does not exist.')
    }

    if (/^win/.test(process.platform)) {
      /* disabled
      try {
        return ifWinRootCATrusted()
      } catch (e) {
        return false
      }
       */
      return false
    } else {
      const HTTPS_RESPONSE = 'HTTPS Server is ON'

      const {key, cert} = await this.getCertificate('local.anyproxy.io')

      const port = await getPort()

      const server = https
        .createServer(
          {
            ca: fs.readFileSync(this.rootCAcrtFilePath),
            key,
            cert,
          },
          (_req, res) => {
            res.end(HTTPS_RESPONSE)
          }
        )
        .listen(port)

      return new Promise<boolean>((resolve) => {
        // do not use node.http to test the cert. Ref: https://github.com/nodejs/node/issues/4175
        const testCmd = `curl https://local.anyproxy.io:${port}`

        exec(testCmd, {timeout: 1000}, (error, stdout) => {
          server.close()

          if (error) {
            resolve(false)
          }
          if (stdout && stdout.indexOf(HTTPS_RESPONSE) >= 0) {
            resolve(true)
          } else {
            resolve(false)
          }
        })
      })
    }
  }

  private async _generateRootCA(commonName: string): Promise<KeyCertPair> {
    // clear old certs
    this.clearCerts()

    const result = this.certGenerator.generateRootCA(commonName)

    await Promise.all([
      fs.writeFile(this.rootCAkeyFilePath, result.privateKey),
      fs.writeFile(this.rootCAcrtFilePath, result.certificate),
    ])

    // PLEASE TRUST the rootCA.crt in ' + certDir
    return {
      key: this.rootCAkeyFilePath,
      cert: this.rootCAcrtFilePath,
    }
  }

  private _checkRootCA() {
    if (this.rootCAExists) {
      return true
    }

    if (!this.rootCAFileExists()) {
      /*
      console.log(color.red('can not find rootCA.crt or rootCA.key'))
      console.log(color.red('you may generate one'))
      */
      return false
    } else {
      this.rootCAExists = true

      return true
    }
  }
}
