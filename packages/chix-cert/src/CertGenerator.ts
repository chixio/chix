import * as forge from 'node-forge'
import {getSANExtension} from './util'

export interface RootCAConfig {
  cert: string
  key: string
}

export interface CAConfig {
  privateKey: string
  publicKey: string
  certificate: string
}

export type NamedAttribute = {name: string; value: string}
export type ShortNamedAttribute = {shortName: string; value: string}
export type CertBaseAttributes = (NamedAttribute | ShortNamedAttribute)[]

export interface AttributeConfig {
  [key: string]: string
}

export interface CertAndKeys {
  cert: forge.pki.Certificate
  keys: forge.pki.KeyPair
}

export type CertAttributes = CertBaseAttributes & {
  name: 'commonName'
  value: string
}

const Attrs: string[] = ['countryName', 'organizationName']
const shortAttrs: string[] = ['ST', 'OU']

export class CertGenerator {
  public defaultAttrs: CertBaseAttributes = [
    {name: 'countryName', value: 'NL'},
    {name: 'organizationName', value: 'ChixCert'},
    {shortName: 'ST', value: 'FL'},
    {shortName: 'OU', value: 'ChixCert SSL'},
  ]

  public getCertAndKeys(serialNumber?: string): CertAndKeys {
    const keys = forge.pki.rsa.generateKeyPair(1024)
    const cert = forge.pki.createCertificate()

    cert.publicKey = keys.publicKey
    cert.serialNumber = serialNumber || Math.floor(Math.random() * 100000) + ''
    cert.validity.notBefore = new Date()
    cert.validity.notBefore.setFullYear(
      cert.validity.notBefore.getFullYear() - 10
    ) // 10 years
    cert.validity.notAfter = new Date()
    cert.validity.notAfter.setFullYear(
      cert.validity.notAfter.getFullYear() + 10
    ) // 10 years

    return {
      cert,
      keys,
    }
  }

  public generateRootCA(commonName: string): CAConfig {
    const {keys, cert} = this.getCertAndKeys()

    commonName = commonName || 'CertManager'

    const attrs = [
      ...this.defaultAttrs,
      {
        name: 'commonName',
        value: commonName,
      },
    ] as CertAttributes

    cert.setSubject(attrs)
    cert.setIssuer(attrs)
    cert.setExtensions([
      {name: 'basicConstraints', cA: true},
      // { name: 'keyUsage', keyCertSign: true, digitalSignature: true, nonRepudiation: true, keyEncipherment: true, dataEncipherment: true },
      // { name: 'extKeyUsage', serverAuth: true, clientAuth: true, codeSigning: true, emailProtection: true, timeStamping: true },
      // { name: 'nsCertType', client: true, server: true, email: true, objsign: true, sslCA: true, emailCA: true, objCA: true },
      // { name: 'subjectKeyIdentifier' }
    ])

    cert.sign(keys.privateKey, forge.md.sha256.create())

    return {
      privateKey: forge.pki.privateKeyToPem(keys.privateKey),
      publicKey: forge.pki.publicKeyToPem(keys.publicKey),
      certificate: forge.pki.certificateToPem(cert),
    }
  }

  public generateCertsForHostname(
    domain: string,
    rootCAConfig: RootCAConfig
  ): CAConfig {
    // generate a serialNumber for domain
    const md = forge.md.md5.create()
    md.update(domain)

    const keysAndCert = this.getCertAndKeys(md.digest().toHex())
    const keys = keysAndCert.keys
    const cert = keysAndCert.cert

    const caCert = forge.pki.certificateFromPem(rootCAConfig.cert)
    const caKey = forge.pki.privateKeyFromPem(rootCAConfig.key)

    // issuer from CA
    cert.setIssuer(caCert.subject.attributes)

    const attrs = [
      ...this.defaultAttrs,
      {
        name: 'commonName',
        value: domain,
      },
    ] as CertAttributes

    const extensions = [
      {name: 'basicConstraints', cA: false},
      getSANExtension(domain),
    ]

    cert.setSubject(attrs)
    cert.setExtensions(extensions)

    cert.sign(caKey, forge.md.sha256.create())

    return {
      privateKey: forge.pki.privateKeyToPem(keys.privateKey),
      publicKey: forge.pki.publicKeyToPem(keys.publicKey),
      certificate: forge.pki.certificateToPem(cert),
    }
  }

  public setDefaultAttrs(attrs: AttributeConfig): void {
    this.defaultAttrs = Object.keys(attrs).map((key) => {
      if (Attrs.includes(key)) {
        return {
          name: key,
          value: attrs[key],
        }
      }

      if (shortAttrs.includes(key)) {
        return {
          shortName: key,
          value: attrs[key],
        }
      }

      throw Error(`Unsupported attribute ${key}`)
    })
  }
}
