import {PowerShell, InvocationResult} from 'node-powershell'

const anyProxyCertReg = /CN=AnyProxy,\sOU=AnyProxy\sSSL\sProxy/

/**
 * detect whether root CA is trusted
 */
export function ifWinRootCATrusted(): Promise<boolean> {
  const ps = new PowerShell({
    executableOptions: {
      '-ExecutionPolicy': 'Bypass',
      '-NoProfile': true,
    },
    debug: true,
  })

  return new Promise<boolean>((resolve) => {
    ps.invoke(PowerShell.command`Get-ChildItem path cert:\\CurrentUser\\Root`)
      .then((output: InvocationResult) => {
        const isCATrusted = anyProxyCertReg.test(output.raw)

        ps.dispose()

        resolve(isCATrusted)
      })
      .catch((error: Error) => {
        console.error(error)

        ps.dispose()

        resolve(false)
      })
  })
}
