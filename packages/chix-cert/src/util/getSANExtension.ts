import {isIpDomain} from './isIpDomain'

export function getSANExtension(domain = '') {
  if (isIpDomain(domain)) {
    return {
      name: 'subjectAltName',
      altNames: [
        {
          type: 7,
          ip: domain,
        },
      ],
    }
  } else {
    return {
      name: 'subjectAltName',
      altNames: [
        {
          type: 2,
          value: domain,
        },
      ],
    }
  }
}
