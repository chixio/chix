import * as fs from 'fs-extra'
import * as path from 'path'

export function deleteFolderContentsRecursive(dirPath: string) {
  if (!dirPath.trim() || dirPath === '/') {
    throw new Error('can_not_delete_this_dir')
  }

  if (fs.existsSync(dirPath)) {
    fs.readdirSync(dirPath).forEach((file, _index) => {
      const curPath = path.join(dirPath, file)

      if (fs.lstatSync(curPath).isDirectory()) {
        deleteFolderContentsRecursive(curPath)
      } else {
        // delete all files
        fs.unlinkSync(curPath)
      }
    })
  }
}
