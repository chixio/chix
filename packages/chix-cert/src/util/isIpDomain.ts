const ipReg = /^\d+?\.\d+?\.\d+?\.\d+?$/

export const isIpDomain = (domain = '') => ipReg.test(domain)
