import * as os from 'os'
import * as path from 'path'

export function darwin(name: string) {
  if (process.env.HOME) {
    return path.join(process.env.HOME, 'Library', 'Application Support', name)
  }

  throw Error('HOME directory is not set.')
}

export function linux(name: string) {
  if (process.env.XDG_CONFIG_HOME) {
    return path.join(process.env.XDG_CONFIG_HOME, name)
  }

  if (process.env.HOME) {
    return path.join(process.env.HOME, '.config', name)
  }

  throw Error('HOME directory is not set.')
}

export function win32(name: string) {
  if (process.env.LOCALAPPDATA) {
    return path.join(process.env.LOCALAPPDATA, name)
  }

  if (process.env.USERPROFILE) {
    return path.join(
      process.env.USERPROFILE,
      'Local Settings',
      'Application Data',
      name
    )
  }

  throw Error('Neither LOCALAPPDATA or USERPROFILE are set.')
}

export function configPath(name?: string) {
  if (typeof name !== 'string') {
    throw new TypeError('`name` must be string')
  }

  switch (os.platform()) {
    case 'darwin':
      return darwin(name)
    case 'linux':
      return linux(name)
    case 'win32':
      return win32(name)
  }

  throw new Error('Platform not supported')
}
