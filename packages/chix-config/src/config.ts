import {
  ensureDirSync,
  pathExistsSync,
  readFileSync,
  writeFileSync,
} from 'fs-extra'
import {get as _get, has as _has, PropertyPath, set as _set} from 'lodash'
import {join} from 'path'
import {configPath} from './util'

export type ConfigOptions = {
  configFile: string
}

export type ChixConfig<T> = {
  version: string
  env?: T
  access_token?: string
}

const version = '1.0.0'

export class Config<Configuration> {
  static get globalConfigDir(): string {
    return configPath('chix')
  }

  static get globalConfigFile(): string {
    return join(Config.globalConfigDir, 'config.json')
  }

  public static hasGlobalConfigFile(): boolean {
    return pathExistsSync(Config.globalConfigFile)
  }

  public static createGlobalConfigFile() {
    ensureDirSync(this.globalConfigDir)

    if (!Config.hasGlobalConfigFile()) {
      Config.write(this.globalConfigFile, {version})
    }
  }

  public static writeGlobalConfig<Configuration>(
    config: ChixConfig<Configuration>
  ): void {
    Config.write(Config.globalConfigFile, {
      ...config,
      version,
    })
  }

  public static write<Configuration>(
    path: string,
    config: ChixConfig<Configuration>
  ): void {
    writeFileSync(path, JSON.stringify(config, null, 2))
  }

  public path: string
  public config: ChixConfig<Configuration> = {
    version,
  }

  constructor(options?: ConfigOptions) {
    this.path =
      options && options.configFile
        ? options.configFile
        : Config.globalConfigFile

    this.config = this.readConfig()

    if (!this.config.version) {
      throw Error('Unable to determine config version.')
    }
  }

  public set(path: PropertyPath, value: any): void {
    _set(this.config, path, value)
  }

  public get(path: PropertyPath): any {
    return _get(this.config, path)
  }

  public has(path: PropertyPath): boolean {
    return _has(this.config, path)
  }

  public write() {
    Config.write(this.path, this.config)
  }

  private readConfig(): ChixConfig<Configuration> {
    if (!Config.hasGlobalConfigFile()) {
      Config.createGlobalConfigFile()
    }
    return JSON.parse(readFileSync(this.path, 'utf-8'))
  }
}
