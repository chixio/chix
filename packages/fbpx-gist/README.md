fbpx-gist
=========

Loads an .fbp from gist and parses it into JSON Format.

Usage:

```javascript

var ChixRenderer = require('fbpx-chix');
var fbpxGist     = require('fbpx-gist');

var renderer = new ChixRenderer();

// assume non-namespaced nodes can be found overhere.
renderer.setDefaultProvider('https://serve-chix.rhcloud.com/nodes/{ns}/{name}');

var fg = new FbpxGist(renderer);

fg.load('9081237', function(err, flow) {

  // Running a flow might be more useful than this...
  console.log(flow);

});


```
