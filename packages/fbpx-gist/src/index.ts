import {FBPParser, IRenderer} from '@chix/fbpx'
import * as request from 'superagent'

const fbpx = new FBPParser()

export class GistLoader {
  public renderer: IRenderer

  constructor(renderer: IRenderer) {
    if (!renderer) {
      throw Error('GistLoader needs a renderer')
    }

    this.renderer = renderer
  }

  public load(gistId: string, cb: (error: Error | null, result?: any) => void) {
    const url = `https://api.github.com/gists/${gistId}`

    request.get(url, (err: Error | null, res) => {
      if (err) {
        cb(err)
      } else {
        if (res.clientError) {
          cb(res.error ? res.error : Error('Unknown error'))
        } else {
          const obj = JSON.parse(res.text)
          // obj.comments
          // obj.created_at
          // obj.updated_at
          // obj.description
          // obj.user..
          let content
          let fileName

          // only interested in the first 'file'
          // not sure what it looks like if multiple files.
          for (const file in obj.files) {
            if (obj.files.hasOwnProperty(file)) {
              fileName = obj.files[file].filename

              content = obj.files[fileName].content
              break
            }
          }

          let flow

          try {
            flow = JSON.parse(content)

            cb(null, flow)
          } catch (e) {
            fbpx.addRenderer(this.renderer)

            flow = fbpx.parse(content)
            flow.iips = this.renderer.getIIPs()
            flow.title = fileName
            flow.description = obj.description

            cb(null, flow)
          }
        }
      }
    })
  }
}
