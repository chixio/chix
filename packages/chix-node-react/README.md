# chix-node-react
React Node Type

The React Node type will handle nodes defined as `"type": "ReactNode"`

To install use:

`npm i chix-node-react --save-dev`

The fbpx tool will automatically prompt to install this module
whenever a graph makes uses of a node using react.

The type can be registered with the Actor using:

```
var Actor = require('chix-flow/lib/actor')
var ReactNode = require('chix-node-react')

Actor.registerNodeType(ReactNode)

var actor = new Actor()
...
```

Test:

Run `npm test`
