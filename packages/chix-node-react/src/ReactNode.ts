import {
  NodeDefinition,
  Port as PortDefinition,
  Ports as PortsDefinition,
} from '@chix/common'
// TODO: xNode was BaseNode (stripped down version)
import {InputPort, NodeEvents, Packet, xNode} from '@chix/flow'
import {forOf} from '@fbpx/lib'
import Debug from 'debug'
import {EventEmitter} from 'events'

const debug = Debug('chix:ReactNode')

// temporary hack, material ui defines types as `node`
// Should have been Node, as uppercase is automatically interpreted as an instance.
// for now rewrite all those to be of type `any`
// Is done in place though, so it's really a hack.
function fixPortTypes(node: NodeDefinition) {
  if (node.ports.input) {
    forOf((_port: string, value: PortDefinition) => {
      if (value.type === 'node') {
        value.type = 'any'
      }
      if (value.type === 'custom') {
        value.type = 'any'
      }
    }, node.ports.input)
  }

  if (node.ports.output) {
    forOf((_port: string, value: PortDefinition) => {
      if (value.type === 'node') {
        value.type = 'any'
      }
      if (value.type === 'custom') {
        value.type = 'any'
      }
    }, node.ports.output)
  }

  return node
}
/**
 * A ReactNode wraps a normal React Component
 *
 * The react component must be defined as a require, within the node definition.
 *
 * Props are used as input ports.
 *
 * Example definition:
 *
 * "require": { "my-comp": "my/repos/someComponent" }
 * "ports": {
 *    "input": {
 *      "label": {
 *        "type": "string
 *      }
 *      "someHandler": {
 *         "type": "boolean"
 *      }
 *    },
 *    "output": {
 *      "component": {} // needed for composition
 *      "someHandler": {
 *         "type": "any"
 *      }
 *    }
 *  }
 *
 * 'My Label'           ->        label MyComp
 * true                 ->  someHandler MyComp
 * MyComp component     ->      element Render
 * MyComp onDismiss [0] ->          msg Console
 *
 * @param {type} id
 * @param {type} node
 * @param {type} identifier
 */
export class ReactNode extends xNode {
  public component: any
  public componentClass: any
  public stateEmitter: EventEmitter
  public react: any

  constructor(id: string, node: NodeDefinition, identifier: string) {
    super(id, fixPortTypes(node), identifier)

    this.id = id

    this.ns = node.ns
    this.name = node.name

    this.type = 'ReactNode'

    this.identifier = identifier

    debug('%s load component', this.identifier)

    this.stateEmitter = new EventEmitter()

    this.status = 'ready'
  }

  public init = async (): Promise<void> => {
    this.createPorts(this.nodeDefinition.ports as PortsDefinition)
    this.componentClass = await this.loadComponent(this.nodeDefinition) // node)
    this.setStatus('created')
    this.react = this.dependencyLoader.require('react')
    this.start()
  }

  public async loadComponent(node: NodeDefinition) {
    if (!node.dependencies || !node.dependencies.npm) {
      throw Error('ReactNode must have a dependency field')
    }

    const dependencies = Object.keys(node.dependencies.npm)

    if (dependencies.length === 1) {
      const key = dependencies[0]

      const instance = this.dependencyLoader.require(key)

      // weird case where dependencyLoader already returns default
      // but there is another nested default.
      return instance.default ? instance.default : instance
    }

    throw Error('A ReactNode must require only one component')
  }

  public start(): void {
    if (['created', 'stopped'].indexOf(this.status) >= 0) {
      this.setStatus('started')
      debug('%s: running on start', this.identifier)

      this.event(NodeEvents.STARTED, {
        node: this.export(),
      })

      this.setStatus('running')
    } else {
      throw Error(
        'Only can start node which is in the `created` or `stopped` state, current status: ' +
          this.status
      )
    }
  }

  public createComponent(initialState: any) {
    const stateEmitter = this.stateEmitter
    const componentClass = this.componentClass

    const React = this.react
    // tslint:disable-next-line: no-this-assignment
    const node = this
    // tslint:disable-next-line:max-classes-per-file
    class ReactNodeComponent extends React.Component {
      public state: any
      public stateListener: any

      constructor(props: any, context: any) {
        super(props, context)

        this.state = initialState

        this.stateListener = this.setState.bind(this)

        stateEmitter.on('state', this.stateListener)
      }

      public componentWillUnmount() {
        stateEmitter.off('state', this.stateListener)
      }

      public render() {
        // TODO: Need a proper executing/idle status.
        node.setStatus('running')
        return React.createElement(componentClass, this.state)
      }
    }

    this.component = React.createElement(ReactNodeComponent)

    const p = new Packet(this.component, 'ReactElement')

    // needed during setup otherwise target is not yet created.
    setTimeout(() => {
      debug('%s: Sending component', this.identifier)
      // this.sendPortOutput('component', p)
      this.getOutputPort('component').write(p)
    })
  }

  public isEventHandler(port: InputPort) {
    return port.type === 'boolean' && this.portExists('output', port.name)
  }

  public allPortsFilled() {
    let allFilled = true

    forOf((_name: string, port: InputPort) => {
      if (!port.isFilled()) {
        allFilled = false
      }
    }, this.ports.input)

    return allFilled
  }

  public setRealType() {}

  public $setContextProperty = (port: string, packet: any) => {
    debug('%s:%s set context', this.identifier, port)
    let data

    data = Packet.isPacket(packet) ? packet.read(this) : packet

    this.stateEmitter.emit('state', {[port]: data})
  }

  public $portIsFilled = (/* port */) => {
    // always ready to receive new props.
    return false
  }

  /**
   *
   * Holds all input until release is called
   *
   * @public
   */
  public hold = () => {
    this.setStatus('hold')
  }

  public shutdown = (callback?: () => void) => {
    this.stateEmitter.removeAllListeners()

    if (callback) {
      callback()
    }
  }

  public hasShutdown = () => {
    return true
  }

  /**
   *
   * Releases the node if it was on hold
   *
   * @public
   */
  public release = () => {
    this.setStatus('ready')

    if (this.__halted) {
      this.__halted = false
      // not much to do
    }
  }

  public onPortFill = (): boolean | undefined => {
    debug('%s:onPortFill running', this.identifier)
    const input: {[key: string]: any} = {}

    if (this.component) {
      // always update state on every portfill
      forOf((_name: string, port: InputPort) => {
        if (port.isFilled()) {
          const data = port.read().read(port)

          if (!this.isEventHandler(port)) {
            // only set handler during initialization
            input[port.name] = data
          }
        }
      }, this.ports.input)

      debug('%s:onPortFill emitting state', this.identifier)

      this.stateEmitter.emit('state', input)
    } else {
      if (this.allPortsFilled()) {
        forOf((name: string, port: InputPort) => {
          const data = port.read().read(port)

          if (this.isEventHandler(port)) {
            if (data === true) {
              input[port.name] = (...args: any[]) => {
                const event = {
                  name: `${this.ns}_${this.name}`.toUpperCase(),
                  arguments: args,
                }

                const eventPacket = new Packet(event, 'object')

                this.getOutputPort(port.name).write(eventPacket)
                // this.sendPortOutput(port.name, eventPacket)
              }
            }
          } else if (name === 'children') {
            // etc. more options.
            // TODO: not sure if propType will ever be set?
            input[port.name] =
              (port as any).propType === 'element'
                ? this.react.createElement('div', null, data)
                : // data.pop()
                  (input[port.name] = data)
          } else {
            input[port.name] = data
          }
        }, this.ports.input)

        debug('%s:onPortFill creating component', this.identifier)

        this.createComponent(input)
      } else {
        debug('%s:onPortFill ports not ready yet', this.identifier)
      }
    }

    return undefined
  }
}
