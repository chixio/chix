/* global document */

import test from 'ava'
import Helper from './helper'
import fs from 'fs'
import path from 'path'
import {Actor} from '@chix/flow'
import ReactNode from '@chix/node-react'

test.cb('Should be able to instantiate', (t) => {
  // Register the ReactNode type
  Actor.registerNodeType(ReactNode)

  const contents = fs.readFileSync(
    path.join(__dirname, '/fixtures/react.fbp'), 'utf8'
  )

  // note: first is uncompiled, second compiled.
  var defs = [
    require('./fixtures/node.js'),
    // installed as devDependency
    require('chix-react/render'),
    require('chix-utils/log')
  ]

  const actor = Helper.loadActorFromFbp(contents, defs)

  actor.processManager.on('error', (event) => {
    console.log(event)
  })

  actor.getNode('Render').getOutputPort('container')
    .on('data', (p) => {
      const container = p.read()
      const button = container.querySelector('button')

      t.is(button.constructor.name, 'HTMLButtonElement')

      t.end()
    })

  actor.sendIIP({
    id: 'Render',
    port: 'container'
  }, document.body)

  actor.sendIIP({
    id: 'Alert',
    port: 'closeLabel'
  }, 'My Label')
})
