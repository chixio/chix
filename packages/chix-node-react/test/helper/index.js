'use strict';

var fs = require('fs');
var CHI = require('chix-chi');
var yaml = require('js-yaml');
var FBPX = require('fbpx/chix');
var Actor = require('chix-flow/lib/actor');
var xNode = require('chix-flow/lib/node');
var xFlow = require('chix-flow/lib/flow');

var helpers = {};

function createActor(flow, defs, xFlow) {
  var actor = xFlow || new Actor();
  if (defs) {
    // fbp renderer sets default provider to remote.
    // FIX ME: should not matter.
    // If I add manually, it should be considered
    // as being loaded from remote.
    delete flow.providers;

    if (Array.isArray(defs)) {
      actor.loader.addNodeDefinitions('@', defs);
    } else {
      actor.loader.addNodeDefinition('@', defs);
    }
  }
  actor.addMap(flow);
  return actor;
}

function createFlow(id, flow) {
  return new xFlow(id, flow);
}

helpers.createNode = function() {
  return new xNode('node1', {
      ns: 'my',
      name: 'component',
      ports: {}
    },
    'node1-1',
    new CHI()
  );
};

helpers.human = function(event) {
  console.warn(
    'NODE IN ERROR STATE, Solve this by listening to the ProcessManager:',
    event.msg
  );
};

helpers.loadNode = function(name) {
  return new xNode(name, helpers.loadYaml(name),
    name + '-0',
    new CHI()
  );
};

helpers.loadYaml = function(name) {
  return yaml.safeLoad(
    fs.readFileSync(__dirname + '/../fixtures/' + name + '.yaml', 'utf8')
  );
};

helpers.loadFBP = function(name) {

  var parser = new FBPX();
  parser.renderer.skipIDs = true;

  return parser.parse(
    fs.readFileSync(__dirname + '/../fixtures/' + name + '.fbp', 'utf8')
  );
};

/* used with brfs in-browser */
helpers.loadActorFromFbp = function(content, defs) {

  var parser = new FBPX();
  parser.renderer.skipIDs = true;

  return createActor(parser.parse(content), defs);
};

/**
 *
 * Initialize an actor with a yaml fixture file.
 *
 * @param {String} name yaml fixture name
 */
helpers.loadActor = function(name, type) {

  var flow;

  if (type === 'fbp') {
    flow = helpers.loadFBP(name);
  } else {
    flow = helpers.loadYaml(name);
  }

  return createActor(flow);

};

helpers.loadFlow = function(id, name, type) {

  var flow;

  if (type === 'fbp') {
    flow = helpers.loadFBP(name);
  } else {
    flow = helpers.loadYaml(name);
  }

  return createFlow(id, flow);

};

module.exports = helpers;
