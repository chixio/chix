/**
 *
 * Note: could be entirely dynamic.
 *
 * Then only use complete JSON form to overrule.
 *
 */
module.exports = {
  ns: 'react' ,
  name: 'alert',
  type: 'ReactNode',
  dependencies: {
    npm: {
      // should support both.
      // 'react-bootstrap/lib/Alert': 'latest',
      alert: require('react-bootstrap/lib/Alert')
    }
  },
  ports: {
    input: {
      bsStyle: {
        type: 'string',
        default: ''
      },
      bsClass: {
        type: 'string',
        default: ''
      },
      closeLabel: {
        type: 'string',
        default: 'Close Me'
      },
      onDismiss: {
        type: 'boolean',
        required: false
      }
    },
    output: {
      component: {
        type: 'ReactElement' // ReactComponent?
      },
      onDismiss: {
        type: 'Action'
      }
    }
  }
};
