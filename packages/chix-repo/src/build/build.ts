import {compileProject, loadPackages} from '../util'

const prefix = 'build'

// Do not build these for now.
export const omitPackages = [
  '@chix/fbpx-gist',
  '@chix/chix',
  // '@chix/runtime',
  // '@chix/group',
  '@chix/chi',
  '@chix/chit',
]

/**
 * This script will build all dependencies.
 */
export async function build(log: any, project?: string, force?: boolean) {
  const packages = loadPackages().filter(
    ({pkg: {name}}) => omitPackages.indexOf(name) === -1
  )

  for (const projectFile of packages) {
    if (project === undefined || projectFile.pkg.name.includes(project)) {
      log.info(prefix, `Building ${projectFile.pkg.name}`)

      if (!compileProject(projectFile.dir, log)) {
        log.error(`Failed to build ${projectFile.pkg.name}`)

        if (!force) {
          process.exit(1)
        }
      }
    }
  }
}
