import {Package} from './loadPackages'

export function getDependencies(
  pkg: Package,
  types = ['dependencies', 'devDependencies']
) {
  return types.reduce((_graph, type) => {
    Object.keys(pkg[type]).forEach((key) => {
      if (key.indexOf('@chix') >= 0) {
        _graph.push(key)
      }
    })

    return _graph
  }, [] as string[])
}
