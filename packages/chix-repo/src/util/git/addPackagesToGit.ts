import {PackageItem} from '../loadPackages'

export function addPackagesToGit(git: any) {
  return async (packageItems: PackageItem[]) => {
    return git.add(
      packageItems.map((packageItem: PackageItem) => packageItem.file)
    )
  }
}
