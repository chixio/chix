import * as npm from 'npm'
import * as path from 'path'

import {compileProject} from '../compileProject'
import {PackageItem} from '../loadPackages'
import {writePackages} from '../writePackages'
import {addPackagesToGit} from './addPackagesToGit'

const prefix = 'commitRelease'

export function commitRelease(git: any, log: any) {
  return async (version: string, packages: PackageItem[]) => {
    const status = await git.status()

    if (status.staged.length) {
      log.info(
        'There are already files added to staging, unstage them first:\n\n -',
        status.staged.join('\n - '),
        '\n'
      )

      return
    }

    log.info(prefix, 'lerna run prepare')

    for (const projectFile of packages) {
      log.info(prefix, `Building ${projectFile.pkg.name}`)

      if (!compileProject(projectFile.dir, log)) {
        log.error(`Failed to build ${projectFile.pkg.name}`)

        process.exit(1)
      }
    }

    log.info(prefix, 'writing package updates.')
    await writePackages(packages)

    log.info(prefix, 'Adding packages to git.')

    await addPackagesToGit(git)(packages)

    log.info(prefix, `Committing release ${version}`)

    // commit release -m '[Release] Bumped version to ${version}'
    await git.commit(`[Release] Bumped version to ${version}`)

    // git push
    log.info(prefix, 'Pushing release to master.')
    await git.push('origin', 'master')

    // git tag ${version}
    log.info(prefix, `Tagging release v${version}.`)
    await git.addTag(`v${version}`)

    // git push tags
    log.info(prefix, 'Pushing tag to origin')
    await git.pushTags('origin')

    // initialize npm
    log.info(prefix, 'Initialize npm')
    await npm.load()

    // now publish each and every package
    log.info(
      prefix,
      `Publishing ${packages.length} new packages for release v${version}`
    )
    const releases = packages.map((packageItem: PackageItem) => {
      return (npm as any).publish(path.dirname(packageItem.file))
    })

    await Promise.all(releases)

    log.info(prefix, 'All packages are published.')
  }
}
