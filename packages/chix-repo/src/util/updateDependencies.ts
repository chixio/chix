import * as logger from 'npmlog'

import {Dependencies, DependencyUpdate, Versions} from './updateDeps'

export function updateDependencies(
  dependencies: Dependencies,
  versions: Versions
): DependencyUpdate {
  let modified = false

  const deps = Object.keys(dependencies).reduce((all, key) => {
    const version = versions[key] ? `^${versions[key]}` : null

    if (version && dependencies[key] !== version) {
      logger.info(
        'updateDependencies',
        `Updating ${key} from ${dependencies[key]} to ${version}`
      )
      modified = true
      all[key] = version
    } else {
      all[key] = dependencies[key]
    }

    return all
  }, {} as Versions)

  return {
    deps,
    modified,
  }
}
