import * as childProcess from 'child_process'

export function spawn(
  command: string,
  options = {}
): childProcess.SpawnSyncReturns<Buffer> {
  const output = childProcess.spawnSync(`${command}`, {
    shell: true,
    stdio: 'inherit',
    ...options,
  })

  if (output.status !== null && output.status !== 0) {
    process.exit(output.status)
  }

  return output
}
