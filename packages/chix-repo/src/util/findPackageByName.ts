import {PackageItem} from './loadPackages'

export function findPackageByName(packages: PackageItem[]) {
  return (packageName: string) => {
    return packages.find((pkg) => pkg.pkg.name === packageName) as PackageItem
  }
}
