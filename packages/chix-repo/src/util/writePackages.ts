import * as fs from 'fs-extra'

import {PackageItem} from './loadPackages'

export async function writePackages(packages: PackageItem[]) {
  const writes = packages.map(async ({file, pkg}) =>
    fs.writeFile(file, JSON.stringify(pkg, null, 2))
  )

  return Promise.all(writes)
}
