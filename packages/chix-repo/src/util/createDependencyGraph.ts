import {getDependencyList} from './getDependencyList'
import {PackageItem} from './loadPackages'

export function createDependencyGraph(packages: PackageItem[]) {
  return packages.reduce((_graph, {pkg}) => {
    getDependencyList(pkg, _graph, ['dependencies'])

    return _graph
  }, [])
}
