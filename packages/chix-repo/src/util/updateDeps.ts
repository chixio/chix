import {PackageItem} from './loadPackages'
import {updateDependencies} from './updateDependencies'

export type Versions = {
  [key: string]: string
}

export type Dependencies = {
  [key: string]: string
}

export type DependencyUpdate = {
  deps: Versions
  modified: boolean
}

export function updateDeps(pkgs: PackageItem[]): PackageItem[] {
  const versions = pkgs.reduce((all, {pkg}) => {
    all[pkg.name] = pkg.version

    return all
  }, {} as Versions)

  pkgs.forEach(({pkg}) => {
    ;['dependencies', 'devDependencies'].forEach((type) => {
      if (pkg[type]) {
        const {deps, modified} = updateDependencies(pkg[type], versions)

        if (modified) {
          pkg[type] = deps
        }
      }
    })
  })

  return pkgs
}
