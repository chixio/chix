import {Package} from './loadPackages'

export type DependencyGraph = string[][]

export function getDependencyList(
  pkg: Package,
  graph: DependencyGraph,
  types = ['dependencies', 'devDependencies']
) {
  return types.reduce((_graph, type) => {
    if (pkg[type]) {
      Object.keys(pkg[type]).forEach((key) => {
        if (key.indexOf('@chix') >= 0) {
          _graph.push([pkg.name, key])
        }
      })
    }

    return _graph
  }, graph)
}
