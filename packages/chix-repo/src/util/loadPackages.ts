import * as fs from 'fs-extra'
import * as glob from 'glob'
import * as path from 'path'
import toposort from 'toposort'

import {createDependencyGraph} from './createDependencyGraph'
import {findPackageByName} from './findPackageByName'

export type Package = {
  name: string
  dependencies: {
    [key: string]: string
  }
  devDependencies: {
    [key: string]: string
  }
  [key: string]: any
}

export type PackageItem = {
  file: string
  dir: string
  pkg: Package
}

const defaultPattern = path.resolve(process.cwd(), './packages/*/package.json')

export function loadPackages(pattern: string = defaultPattern): PackageItem[] {
  const packages = glob.sync(pattern).map((file: string) => ({
    file,
    dir: path.dirname(file),
    pkg: JSON.parse(fs.readFileSync(file, 'utf-8')),
  }))

  const graph = createDependencyGraph(packages)

  const result = toposort(graph).reverse()

  return result.map(findPackageByName(packages))
}
