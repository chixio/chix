import * as path from 'path'
import * as ts from 'typescript'

const prefix = 'compileProject'

/**
 * Compiles a project based on its configuration.
 *
 * If the compile fails the files will not be written and the errors are shown
 *
 * Usage:
 *
 *  compile('packages/chix-common')
 */
export function compileProject(project: string, log: any): boolean {
  const configFileName = path.join(project, 'tsconfig.json')

  const config = ts.getParsedCommandLineOfConfigFile(
    configFileName,
    {project},
    ts.sys as any
  )

  if (!config) {
    return false
  }

  const {fileNames, options, projectReferences} = config
  const host = ts.createCompilerHost(options)

  const programOptions: ts.CreateProgramOptions = {
    rootNames: fileNames,
    options: {
      ...options,
      noEmitOnError: true,
    },
    projectReferences,
    host,
    configFileParsingDiagnostics: ts.getConfigFileParsingDiagnostics(config),
  }

  const program = ts.createProgram(programOptions)

  // Emit and report any errors we ran into.
  const {diagnostics, emitSkipped} = program.emit()

  const allDiagnostics = ts.getPreEmitDiagnostics(program).concat(diagnostics)

  let lastMessageText: string
  allDiagnostics.forEach((diagnostic) => {
    if (diagnostic.file) {
      const {line, character} = diagnostic.file.getLineAndCharacterOfPosition(
        diagnostic.start!
      )
      const message = ts.flattenDiagnosticMessageText(
        diagnostic.messageText,
        '\n'
      )
      const messageText = `${diagnostic.file.fileName} (${
        line + 1
      },${1},${1},${1},${character + 1}): ${message}`

      if (messageText !== lastMessageText) {
        log.error(prefix, messageText)
      }

      lastMessageText = messageText
    } else {
      log.error(
        prefix,
        ts.flattenDiagnosticMessageText(diagnostic.messageText, '\n')
      )
    }
  })

  return !emitSkipped
}
