import {PackageItem} from './loadPackages'

export function applyPackageVersion(
  version: string,
  packageItems: PackageItem[]
) {
  return packageItems.map((packageItem: PackageItem) => {
    return {
      ...packageItem,
      pkg: {
        ...packageItem.pkg,
        version,
      },
    }
  })
}
