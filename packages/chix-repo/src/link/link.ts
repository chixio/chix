import {ensureSymlink, remove} from 'fs-extra'
import * as path from 'path'
import {loadPackages} from '../util'

/**
 * This script will update all dependencies.
 */
const prefix = 'link'

export async function link(log: any, sourceDir: string = process.cwd()) {
  const pattern = path.resolve(sourceDir, './packages/*/package.json')
  const packages = loadPackages(pattern)

  for (const projectFile of packages) {
    const destinationPath = path.resolve(
      process.cwd(),
      `node_modules/${projectFile.pkg.name}`
    )

    log.info(prefix, `Linking ${projectFile.dir} -> ${destinationPath}`)

    await remove(destinationPath)
    await ensureSymlink(projectFile.dir, destinationPath)
  }
}
