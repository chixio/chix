import {Command, flags} from '@oclif/command'
import * as logger from 'npmlog'
import {ReleaseType} from 'semver'

import {releaseAll} from '../release/'

const releaseTypes: ReleaseType[] = ['major', 'minor', 'patch']

export default class Release extends Command {
  public static description = 'Release all packages with a new version'

  public static examples = ['$ chix-repo release --patch']

  public static flags = {
    help: flags.help({char: 'h'}),
    major: flags.boolean({char: 'M', description: 'Release major version.'}),
    minor: flags.boolean({char: 'm', description: 'Release minor version.'}),
    patch: flags.boolean({char: 'p', description: 'Release patch version.'}),
  }

  public async run() {
    const parsed = this.parse(Release)

    const releaseType = releaseTypes.filter(
      (type) => !!(parsed.flags as any)[type]
    )

    if (releaseType.length === 0) {
      this.log('Please specify a release type --major|minor|patch')

      process.exit()
    }

    if (releaseType.length > 1) {
      throw new Error(
        `Multiple release types specified choose one ${releaseType}`
      )
    }

    await releaseAll(logger)(releaseType[0])

    this.log('All packages are released.')
  }
}
