import {Command, flags} from '@oclif/command'
import * as logger from 'npmlog'

import {build} from '../build/'

export default class BuildCommand extends Command {
  public static description = 'Build all project packages'

  public static examples = ['$ chix-repo build']

  public static flags = {
    name: flags.string({
      char: 'n',
      description: 'Package name to be build. e.g. chix-flow.',
      required: false,
    }),
    force: flags.boolean({
      char: 'f',
      description: 'Continue even if there are errors.',
      required: false,
    }),
    help: flags.help({char: 'h'}),
  }

  public async run() {
    const {
      flags: {name},
    } = this.parse(BuildCommand)
    await build(logger, name)

    this.log('All packages have been build.')
  }
}
