import {Command, flags} from '@oclif/command'
import * as logger from 'npmlog'

import {update} from '../update/'

export default class UpdateCommand extends Command {
  public static description = 'Update all package dependencies'

  public static examples = ['$ chix-repo update']

  public static flags = {
    help: flags.help({char: 'h'}),
  }

  public async run() {
    await update(logger)

    this.log('All packages are updated.')
  }
}
