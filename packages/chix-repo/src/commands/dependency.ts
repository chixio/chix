import {Command, flags} from '@oclif/command'

import {loadPackages, PackageItem, writePackages} from '../util/'
import {setVersion} from '../version/'

export default class Dependency extends Command {
  public static description =
    'Use this command to update a dependency version in all package dirs.'

  public static examples = [
    '$ chix-repo dependency --name typescript --version ^2.8.3 ',
  ]

  public static flags = {
    name: flags.string({
      char: 'n',
      description:
        'Dependency name to be updated across repository. e.g. typescript.',
      required: true,
    }),
    version: flags.string({
      char: 'v',
      description: 'Set dependency to this version. e.g. ^2.8.3',
      required: true,
    }),
  }

  public async run() {
    const {
      flags: {name, version},
    } = this.parse(Dependency)
    const packages = loadPackages()

    const affectedPackages = setVersion(name, version, packages)

    await writePackages(affectedPackages)

    const affectedPackageNames = affectedPackages.map(
      (item: PackageItem) => item.pkg.name
    )

    this.log(
      `${name} version for these packages was set to ${version}: ${affectedPackageNames} `
    )
  }
}
