import {Command, flags} from '@oclif/command'
import * as logger from 'npmlog'

import {link} from '../link/'

export default class LinkCommand extends Command {
  public static args = [
    {
      name: 'sourceDir',
      required: false,
      description: 'path to chix repository',
    },
  ]
  public static description = 'Link all project packages'

  public static examples = ['$ chix-repo link']

  public static flags = {
    help: flags.help({char: 'h'}),
  }

  public async run() {
    const {args} = this.parse(LinkCommand)
    await link(logger, args.sourceDir)

    this.log('All packages are linked.')
  }
}
