import {run} from 'npm-check-updates'

import {loadPackages} from '../util'

/**
 * This script will update all dependencies.
 */
const prefix = 'update'

export async function update(log: any) {
  const packages = loadPackages()

  log.info(prefix, packages)

  for (const projectFile of packages) {
    console.log(`Checking ${projectFile.file}`)

    const upgraded = await run({
      packageFile: projectFile.file,
      silent: false,
      // greatest: true,
      peer: true,
      upgrade: true,
      upgradeAll: true,
    })

    if (Object.keys(upgraded).length > 0) {
      log.info(prefix, upgraded)
    }
  }
}
