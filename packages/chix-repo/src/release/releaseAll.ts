import * as path from 'path'
import * as semver from 'semver'
// tslint:disable-next-line:no-submodule-imports
import * as Git from 'simple-git'

import {
  applyPackageVersion,
  commitRelease,
  loadPackages,
  PackageItem,
  updateDeps,
} from '../util'

/**
 * This script will update *all* packages to the next version.
 *
 * Release will be tagged in git and all packages will be published to npm.
 *
 * Each and every package will carry the same release version.
 *
 * `@chix` dependencies are automatically updated to the latest version also.
 */

// Do not publish these for now.
export const omitPackages = [
  '@chix/fbpx-gist',
  '@chix/chix',
  // '@chix/runtime',
  // '@chix/group',
  '@chix/chi',
  '@chix/chit',
]

export type Versioned = {
  name: string
  file: string
  version: string
}

const prefix = 'releaseAll'

export function releaseAll(log: any) {
  return async (release: semver.ReleaseType = 'minor') => {
    const git = Git(path.resolve(__dirname, '../'))

    const status = await git.status()

    const versions = loadPackages().filter(
      ({pkg: {name}}) => omitPackages.indexOf(name) === -1
    )

    const packageDirs = versions.map((packageItem: PackageItem) =>
      path.dirname(packageItem.file)
    )

    log.info(prefix, packageDirs)

    const modifiedPackages = packageDirs.filter(
      (packageDir: string) => status.modified.indexOf(packageDir) >= 0
    )

    if (modifiedPackages.length > 0) {
      // Insufficient check. should check git history, for latest release.
      log.info(
        prefix,
        'The following packages have changes which have not been committed yet, commit them first:'
      )
      log.info(prefix, modifiedPackages)
    }

    const greatestVersion = versions.reduce(
      (greatest: string, {pkg}: PackageItem) => {
        if (semver.gt(pkg.version, greatest)) {
          return pkg.version
        }

        return greatest
      },
      '0.0.0'
    )

    log.info(prefix, `Greatest version is: ${greatestVersion}`)

    const version = semver.inc(greatestVersion, release) as string

    log.info(
      prefix,
      `Bumping version for all @chix packages to version ${version}`
    )
    const bumpedPackages = applyPackageVersion(version, versions)

    log.info(prefix, `Updating all @chix dependencies to version ${version}`)
    const newReleases = updateDeps(bumpedPackages)

    log.info(prefix, 'Committing release.')

    commitRelease(git, log)(version, newReleases).catch((error) => {
      log.error(error)
    })
  }
}
