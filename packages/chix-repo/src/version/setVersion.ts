import {PackageItem} from '../util'

export function setVersion(
  name: string,
  version: string,
  packages: PackageItem[]
) {
  return packages.reduce((updated: PackageItem[], item: PackageItem) => {
    ;['dependencies', 'devDependencies'].forEach((type) => {
      const dependencies = item.pkg[type]

      if (dependencies && dependencies[name]) {
        dependencies[name] = version

        updated.push(item)
      }
    })

    return updated
  }, [])
}
