chix
====

Repository Tool

[![oclif](https://img.shields.io/badge/cli-oclif-brightgreen.svg)](https://oclif.io)
[![Version](https://img.shields.io/npm/v/chix.svg)](https://npmjs.org/package/chix)
[![CircleCI](https://circleci.com/gh/rhalff/chix/tree/master.svg?style=shield)](https://circleci.com/gh/rhalff/chix/tree/master)
[![Appveyor CI](https://ci.appveyor.com/api/projects/status/github/rhalff/chix?branch=master&svg=true)](https://ci.appveyor.com/project/rhalff/chix/branch/master)
[![Codecov](https://codecov.io/gh/rhalff/chix/branch/master/graph/badge.svg)](https://codecov.io/gh/rhalff/chix)
[![Downloads/week](https://img.shields.io/npm/dw/chix.svg)](https://npmjs.org/package/chix)
[![License](https://img.shields.io/npm/l/chix.svg)](https://github.com/rhalff/chix/blob/master/package.json)

<!-- toc -->
# Usage
<!-- usage -->
# Commands
<!-- commands -->
