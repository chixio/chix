'use strict'

const xNode = require('./lib/node/node')
const xFlow = require('./lib/flow/flow')
const xLink = require('./lib/link')
const Actor = require('./lib/actor/actor')
const ProcessManager = require('./lib/process/manager')
const mapSchema = require('./schemas/map.json')
const nodeSchema = require('./schemas/node.json')
const stageSchema = require('./schemas/stage.json')
const Validate = require('./lib/validate')

module.exports = {
  Actor: Actor,
  Flow: xFlow,
  Link: xLink,
  Node: xNode,
  ProcessManager: ProcessManager,
  Schema: {
    Map: mapSchema,
    Node: nodeSchema,
    Stage: stageSchema
  },
  Validate: Validate
}
