import {namify} from './namify'

export function safeName(str: string) {
  str = namify(str)
  str = isNaN(parseInt(str[0], 10)) ? str : '_' + str

  return str
}
