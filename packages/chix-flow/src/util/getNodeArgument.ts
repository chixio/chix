import {Flow as FlowDefinition, NodeDefinition} from '@chix/common'

export type NodeArguments = {
  id: string
  node: NodeDefinition | FlowDefinition
}

export function getNodeArgument(args: any[]): NodeArguments {
  if (!args || !args.length) {
    throw Error('Not enough arguments.')
  }

  const {node, id} = args[0]

  if (!id || typeof id !== 'string') {
    throw Error('id parameter is required.')
  }

  if (!node) {
    throw Error('node parameter is required.')
  }

  if (!node.ns || !node.name) {
    console.log(node)
    throw Error(`Node requires both ns and name.`)
  }

  return {
    id,
    node,
  }
}
