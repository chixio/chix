export const mixin =
  (...mixer: any[]) =>
  <T>(Base: T) => {
    const seen: any[] = []

    return mixer.reverse().reduce((Clazz, mix) => {
      if (seen.indexOf(mix) === -1) {
        seen.push(mix)

        return mix(Clazz)
      }

      return Clazz
    }, Base)
  }
