import {InputPort, OutputPort} from './port'

export interface Ports {
  input?: {
    [name: string]: InputPort
  }
  output?: {
    [name: string]: OutputPort
  }
}
