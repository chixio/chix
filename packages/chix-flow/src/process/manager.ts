import d from 'debug'
import {EventEmitter} from 'events'
import {v4 as uuid} from 'uuid'
import {NodeEvents, PMEvents} from '../events'
import {Flow} from '../flow'
import {xNode} from '../node'
import {InputPort, Port} from '../port'

const debug = d.debug('chix:pm')

const onExit: ProcessManager[] = []

if (typeof process !== 'undefined' && process.on) {
  // old browserify
  process.on('exit', function onExitHandlerProcessManager() {
    onExit.forEach((instance: ProcessManager) =>
      instance.onExit.bind(instance)()
    )
  })
}

const noop = (_processes: (Flow | xNode)[], _that: any) => {}

export type OnExitHandler = (processes: (Flow | xNode)[], that: any) => void

export interface ProcessManagerOptions {
  onExit?: OnExitHandler
}

/**
 *
 * Default Process Manager
 *
 * @constructor
 * @public
 *
 */
export class ProcessManager extends EventEmitter {
  public processes: Map<string, Flow | xNode> = new Map()
  public onExitHandler: OnExitHandler = noop
  private vouchers: string[] = []
  constructor(options: ProcessManagerOptions = {}) {
    super()
    if (options.onExit) {
      this.onExitHandler = options.onExit
    }

    onExit.push(this)
  }

  public onExit() {
    const processes = Array.from(this.processes.values()).reverse()

    this.onExitHandler(processes, this)
  }

  public hasMainGraph(): boolean {
    return this.getMainGraphs().length > 0
  }

  public getMainGraph(): Flow {
    const main = this.getMainGraphs().pop()

    if (!main) {
      throw Error('Could not find main actor')
    }

    return main
  }

  public getMainGraphs(): Flow[] {
    const main: Flow[] = []

    for (const process of this.processes.values()) {
      if (process.type === 'flow' && !process.hasParent()) {
        main.push(process as Flow)
      }
    }

    return main
  }

  /**
   * Per current design links are added prior to the flow itself
   * being registered with the process manager as a result the
   * links are unable to know the pid of the flow at the time they are connected.
   *
   * This registrator hands out a pid so it is known before the flow itself is
   * actually registered as a process.
   */
  public createRegistrator() {
    const pid = uuid()
    this.vouchers.push(pid)
    return {
      pid,
      register: (node: xNode | Flow) => {
        this.register(node)
      },
    }
  }
  public register(node: xNode | Flow): void {
    if (node.pid) {
      if (this.vouchers.includes(node.pid)) {
        this.vouchers = this.vouchers.filter((voucher) => voucher === node.pid)
        this.processes.set(node.pid, node)
      } else {
        throw new Error('Refusing to add node with existing process id')
      }
    } else {
      const pid = uuid()
      node.setPid(pid)
      this.processes.set(pid, node)
    }

    node.on(NodeEvents.START.name, this.onProcessStartHandler)
    node.on(NodeEvents.STOP.name, this.onProcessStopHandler)
    node.on(NodeEvents.STATUS_UPDATE.name, this.onProcessStatusHandler)
    node.on(NodeEvents.ERROR.name, this.processErrorHandler)

    debug('Registered ' + node.identifier)

    this.emit(PMEvents.ADD_PROCESS, node)
  }

  public changePid(from: string, to: string): void {
    const process = this.processes.get(from)
    if (process !== undefined) {
      this.processes.set(to, process)
      this.processes.delete(from)
    } else {
      throw Error('Process id not found')
    }

    this.emit(PMEvents.CHANGE_PID, {
      from,
      to,
    })
  }

  // Why can a process be a string?
  // public getProcess(pid: string): string | Flow | xNode {
  public getProcess(pid: string): Flow | xNode {
    const process = this.processes.get(pid)
    if (process !== undefined) {
      return process
    }
    throw Error('Process id not found')
  }

  public getProcesses(): (Flow | xNode)[] {
    return Array.from(this.processes.values())
  }

  // TODO: improve start, stop, hold, release logic..
  public start(node: xNode | Flow | string) {
    const pid = typeof node === 'object' ? node.pid : node

    if (pid !== null) {
      const process = this.getProcess(pid)

      if (process !== undefined) {
        if (process.type === 'flow') {
          return process.start()
        } else {
          return process.release()
        }
      }
    }

    throw Error('Unable to find process')
  }

  public async stop(node: xNode | Flow | string): Promise<void> {
    const pid = typeof node === 'object' ? node.pid : node
    if (pid !== null) {
      const process = this.getProcess(pid)
      if (process !== undefined) {
        if (process.type === 'flow') {
          return (process as Flow).stop()
        }

        return process.hold()
      }
    }

    throw Error('Unable to find process')
  }

  // TODO: just deleting is not enough.
  // links also contains the pids
  // on remove process those links should also be removed.
  public async unregister(node: xNode | Flow): Promise<xNode | Flow> {
    if (!node.pid) {
      throw new Error('Process id not found')
    }

    const process = this.getProcess(node.pid)

    if (process.type === 'flow') {
      // wait for `subgraph` to be finished
      await this.stop(node)
    } else {
      await node.shutdown()
    }

    node.removeListener(NodeEvents.START.name, this.onProcessStartHandler)
    node.removeListener(NodeEvents.STOP.name, this.onProcessStopHandler)
    node.removeListener(
      NodeEvents.STATUS_UPDATE.name,
      this.onProcessStatusHandler
    )
    node.removeListener(NodeEvents.ERROR.name, this.processErrorHandler)

    this.processes.delete(node.pid)

    // @ts-ignore FIX this. pid is non optional on Flow
    delete node.pid

    this.emit(PMEvents.REMOVE_PROCESS, node)

    debug('Unregistered ' + node.identifier)

    return node
  }

  /**
   *
   * Get Process
   * Either by id or it's pid.
   *
   */
  public get(pid: string): xNode | Flow {
    const process = this.processes.get(pid)

    if (process != null) {
      return process
    }

    throw Error(' Could not determine process.')
  }

  public getById(id: string, actor?: Flow) {
    return this.findBy('id', id, actor)
  }

  public findBy(prop: string, value: any, actor?: Flow) {
    let found
    for (const process of this.processes.values()) {
      const id = process.id

      if (id !== undefined) {
        // TODO: should not be any
        if ((process as any)[prop] === value && (!actor || actor.hasNode(id))) {
          if (found) {
            console.log(this.processes)
            throw Error('conflict: multiple ' + prop + 's matching ' + value)
          }
          found = process
        }
      } else {
        throw Error('Could not determine process id.')
      }
    }
    return found
  }

  /**
   *  Gets all the stalled port for the main graph.
   *
   *  A port is considered stalled when:
   *
   *   - the port itself reports to be empty
   *   - the port is not waiting for input from a node which has not run yet.
   *
   * Example usage:
   *  console.table(runtime.processManager.getStalledPorts())
   */
  public getStalledPorts() {
    const nodes = Array.from(this.getMainGraph().nodes.values()).filter(
      (node) => node.runCount === 0 && node.isStartable
    )

    const ports: any[] = []

    // TODO: remove as any, external ports are treated as InputPorts here.
    for (const node of nodes) {
      const inputPorts = Array.from(
        Object.values(node.ports.input as any)
      ) as InputPort[]
      for (const port of inputPorts) {
        const status = port.isFilled()

        if (status === Port.EMPTY) {
          let waiting
          if (port.hasConnections()) {
            const connections = port.getConnections()
            for (const connection of connections) {
              const pid = connection.source.pid

              if (pid != null) {
                const source = this.get(pid)

                if (source.runCount === 0) {
                  waiting = true
                  break
                }
              } else {
                throw Error('Could not determine pid')
              }
            }
          }

          if (!waiting) {
            const parent = port.parent

            if (parent != null) {
              const {identifier, ns, name} = parent
              ports.push({
                node: identifier,
                ns,
                name,
                status: node.getStatus(),
                port: port.name,
                required: port.required,
                context: port.hasContext(),
                persist: port.hasPersist(),
                default: port.default,
              })
            } else {
              throw Error('Could not determine parent')
            }
          }
        }
      }
    }

    return ports
  }

  public filterByStatus(status: string) {
    return this.filterBy('status', status)
  }

  public filterBy(prop: string, value: any): (xNode | Flow)[] {
    const filtered: (xNode | Flow)[] = []

    for (const process of this.processes.values()) {
      // TODO: should not be any
      if ((process as any)[prop] === value) {
        filtered.push(process)
      }
    }

    return filtered
  }

  /**
   *
   * Process Error Handler.
   *
   * The only errors we receive come from the nodes themselves.
   * It's also guaranteed if we receive an error the process itself
   * Is already within an error state.
   *
   */
  private processErrorHandler = (event: any): void => {
    /* TODO: re-enable
    if (event.node.status !== 'error') {
      console.log('STATUS', event.node.status)
      throw Error('Process is not within error state', event.node.status)
    }
    */

    console.log('PM Error', event)
    this.emit(PMEvents.ERROR, event)
  }

  private onProcessStartHandler = (event: any): void => {
    debug(`Process started ${event.node.identifier}`)
    this.emit(PMEvents.START_PROCESS, event.node)
  }

  private onProcessStopHandler = (event: any): void => {
    debug(`Process stopped ${event.node.identifier}`)
    this.emit(PMEvents.STOP_PROCESS, event.node)
  }
  private onProcessStatusHandler = (event: any): void => {
    debug(`Changed status ${event.node.identifier} to ${event.status}`)
    this.emit(PMEvents.PROCESS_STATUS, event)
  }
}
