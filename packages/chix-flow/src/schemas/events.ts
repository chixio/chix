export const Events = {
  events: {
    error: {
      node: {
        type: 'object',
      },
      msg: {
        type: 'string',
      },
    },
    nodeContext: {
      node: {
        type: 'object',
      },
      message: {
        type: 'string',
      },
    },
    start: {
      node: {
        type: 'object',
      },
    },
    branching: {
      node: {
        type: 'object',
      },
      method: {
        type: 'string',
      },
    },
    statusUpdate: {
      node: {
        type: 'object',
      },
      status: {
        type: 'string',
      },
    },
    output: {
      node: {
        type: 'object',
      },
      port: {
        type: 'string',
      },
      out: {
        type: 'any',
      },
    },
    fillCore: {
      node: {
        type: 'object',
      },
      fn: {
        type: 'string',
      },
      name: {
        type: 'string',
      },
    },
    nodeComplete: {
      node: {
        type: 'object',
      },
      message: {
        type: 'string',
      },
    },
    nodeTimeout: {
      node: {
        type: 'object',
      },
    },
    inputTimeout: {
      node: {
        type: 'object',
      },
      message: {
        type: 'string',
      },
    },
    portReject: {
      node: {
        type: 'object',
      },
      port: {
        type: 'string',
      },
      options: {
        type: 'object',
      },
      data: {
        type: 'object',
      },
    },
    portFill: {
      node: {
        type: 'object',
      },
      message: {
        type: 'string',
      },
    },
    expose: {
      node: {
        type: 'object',
      },
      expose: {
        type: 'string',
      },
    },
    inputValidated: {
      node: {
        type: 'object',
      },
      port: {
        type: 'string',
      },
    },
  },
}
