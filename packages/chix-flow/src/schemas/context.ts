export const Context = {
  type: 'object',
  title: 'Chiχ Context Object',
  description: 'A context object contains all context for a given map',
  properties: {
    key: {
      type: 'string',
      required: true,
    },
    title: {
      type: 'string',
      required: true,
    },
    description: {
      type: 'string',
      required: true,
    },
    context: {
      type: 'object',
      required: false,
    },
    ports: {
      type: 'object',
      required: true,
      properties: {
        input: {
          type: 'object',
        },
        output: {
          type: 'object',
        },
        event: {
          type: 'object',
        },
      },
    },
  },
}
