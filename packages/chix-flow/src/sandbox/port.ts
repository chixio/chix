import {NodeBox} from './node'

/**
 *
 * PortBox
 *
 * @constructor
 * @public
 *
 */
export class PortBox extends NodeBox {
  constructor(name: string) {
    super(name)
    this.name = name || 'PortBox'
  }

  public define() {
    // Define the structure
    this.addArg('data', null)
    this.addArg('source', null) // not sure..
    this.addArg('state', {})
    this.addArg('input', {})
    this.addArg('$', {})
    this.addArg('output', null) // output function should be set manually

    // what to return from the function.
    this.addReturn('state')
  }
}
