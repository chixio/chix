/* global window */
import {NpmDependencies} from '@chix/common'
import {IOBox} from '@chix/iobox'
import {DependencyLoader} from '@chix/loader'

export type GlobalExpose = string[]

/**
 *
 * NodeBox
 *
 * @constructor
 * @public
 *
 */
export class NodeBox extends IOBox {
  public name = 'NodeBox'

  public state: any
  public output: any

  constructor(name: string) {
    super(name)

    if (name) {
      this.name = name
    }

    // FIXME: IOBox extends eventEmitter and already defines on.
    /*
    (this.on as any).input
    (this.on as any).shutdown
    (this.on as any).start
    */

    this.define()
  }

  public define() {
    // Define the structure
    this.addArg('input', {})
    this.addArg('$', {})
    this.addArg('output', {})
    this.addArg('state', {})
    this.addArg('done', null)
    this.addArg('cb', null)
    // this.addArg('console', console)

    this.addArg('on', {
      input: {},
    }) // dynamic construction

    // what to return from the function
    this.addReturn('output')
    this.addReturn('state')
    this.addReturn('on')
  }

  public addDependencies(
    dependencies: NpmDependencies,
    dependencyLoader: DependencyLoader,
    check: boolean = true
  ) {
    const packagePaths = Object.keys(dependencies)
    for (const packagePath of packagePaths) {
      const {varName} = dependencyLoader.getPathSegments(packagePath)

      if (dependencyLoader.hasDependency(packagePath) || !check) {
        this.addArg(varName, dependencyLoader.require(packagePath))

        this.emit('require', {require: packagePath})
      }
    }
  }

  public expose(exposed: GlobalExpose): void {
    // created to allow window to be exposed to a node.
    // only meant to be used for dom nodes.
    const _global: {[key: string]: any} =
      typeof window === 'undefined' ? global : window

    if (exposed) {
      for (const expose of exposed) {
        this.emit('expose', {expose})

        if (expose === 'window') {
          this.addArg('win', window)
        } else if (expose === 'self') {
          this.addArg('self', this)
        } else {
          // Do not re-expose anything already going in
          if (!this.args.hasOwnProperty(expose)) {
            this.addArg(expose, _global[expose])
          }
        }
      }
    }
  }

  public compile(fn?: string) {
    return super.compile(fn, true) // return as object
  }

  /**
   *
   * Runs the sandbox.
   *
   */
  public run(bind?: any): any {
    const res = super.run.apply(this, [bind])

    let ret

    // puts the result back into our args/state
    // TODO: I do not think this is needed?
    for (const k in res) {
      if (k === 'return') {
        ret = res.return
      } else if (res.hasOwnProperty(k)) {
        this.set(k, res[k])
      }
    }

    return ret // original return value
  }
}
