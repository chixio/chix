import {forOf} from '@fbpx/lib'
import {Packet, PacketMetaType} from './packet'

/**
 * Packet container to be used within components.
 *
 * API:
 *
 * const x = new PacketContainer(input)
 * x.in1
 * x.in1 = 'my-value'
 */
const reserved = ['isPacket', 'get', 'read', 'write', 'create', 'clone']

export type PacketContainerParams = {
  [key: string]: Packet
}

export class PacketContainer {
  [key: string]: any
  public static create(params: PacketContainerParams) {
    return new PacketContainer(params)
  }

  public params: PacketContainerParams
  public _meta: PacketMetaType = {}

  constructor(params: PacketContainerParams = {}) {
    this.params = params

    forOf((name: string, packet: Packet) => {
      if (!Packet.isPacket(packet)) {
        throw Error(`Packet expected got ${typeof packet} for param ${name}`)
      }
      // merge meta
      if (reserved.indexOf(name) === -1) {
        Object.defineProperty(this, name, {
          enumerable: true,
          get() {
            return packet.read(packet.owner)
          },
          set(val) {
            packet.write(packet.owner, val)
          },
        })
        // meta
        Packet.metaMerge(this._meta, packet._meta)
      } else {
        throw Error(`Parameter name '${name}' not allowed`)
      }
    }, this.params)

    // merge meta for all packets
    forOf((_name: string, p: Packet) => {
      Object.assign(p._meta, this._meta)
    }, this.params)
  }

  /**
   * Determines whether the value is a Packet
   *
   * @param {Packet} packet - Value to test
   * @returns {*}
   */
  public isPacket(packet: Packet) {
    return Packet.isPacket(packet)
  }

  /**
   *
   * @param {String} name - Param name
   * @returns {*}
   */
  public get(name: string) {
    return this.params[name]
  }

  public clone(name: string, value?: any): Packet {
    this.hasParamOrThrow(name)

    const packet = this.params[name]
    const packetClone = packet.clone(packet.owner)

    if (packet === packetClone) {
      throw Error('Failed to clone packet.')
    }

    if (value !== undefined) {
      packetClone.write(packet.owner, value)
    }

    return packetClone
  }

  public read(name: string) {
    this.hasParamOrThrow(name)

    const packet = this.params[name]

    return packet.read(packet.owner)
  }

  public write(name: string, val: any) {
    this.hasParamOrThrow(name)

    const packet = this.params[name]

    return packet.write(packet.owner, val)
  }

  public hasParam(name: string): boolean {
    return !!this.params[name]
  }

  public hasParamOrThrow(name: string): boolean {
    if (this.hasParam(name)) {
      return true
    }

    throw Error(`${name} does not exist.`)
  }

  public create(value: any, type?: string) {
    const p = Packet.create(value, type)

    Object.assign(p._meta, this._meta)

    return p
  }
}
