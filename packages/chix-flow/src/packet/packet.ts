// TODO: only has to be a pointer packet if it's actually used.
import {forOf} from '@fbpx/lib'
import d from 'debug'
import {JsonPointer} from 'json-ptr'
import {ContainerPacket} from './ContainerPacket'

const debug = d.debug('chix:packet')
/**
 *
 * A Packet wraps the data.
 *
 * The packet is always owned by one owner at a time.
 *
 * In order to read or write a packet, the owner must identify
 * itself first, by sending itself als a reference as first argument
 * to any of the methods.
 *
 * const p = new Packet(data)
 *
 * Packet containing it's own map of source & target.
 * Could be possible, only what will happen on split.
 *
 */
let nr = 10000000000

export type PacketMetaType = {
  [ns: string]: {
    [name: string]: any
  }
}

export type PacketTrailEntry = {
  id: string
  pid: string
}

export type PacketTrail = PacketTrailEntry[]

export type PacketObjectType = {[portName: string]: any}

export type PacketType = string | PacketObjectType

export type TypeTrail = string[]

export class Packet {
  public static create(
    data?: any,
    type?: PacketType,
    n?: number,
    c?: number,
    pp?: string
  ): Packet {
    return new Packet(data, type, n, c, pp)
  }

  public static metaMerge(
    target: PacketMetaType,
    source: PacketMetaType
  ): void {
    forOf((ns: string, name: string, val: any) => {
      if (!target.hasOwnProperty(ns)) {
        target[ns] = {}
      }
      if (!target[ns].hasOwnProperty(name) || target[ns][name] === val) {
        target[ns][name] = val
      } else {
        console.log('tgt:', target, 'src:', source)
        console.log('oldValue: %s, newValue: %s', target[ns][name], val)
        throw Error(`Refusing to overwrite meta property ${ns}:${name}`)
      }
    }, source)
  }

  // guard for two versions of Packet
  // instanceof would normally be sufficient
  public static isPacket(packet: Packet): boolean {
    return (
      packet &&
      typeof packet === 'object' &&
      (packet instanceof Packet ||
        (packet as any).constructor.name === 'Packet')
    )
  }

  public owner?: any
  public trail: PacketTrail = []
  public typeTrail: TypeTrail = []
  public pointerPath = '/.'
  public _meta: PacketMetaType = {}
  public type: PacketType
  public __data: ContainerPacket
  public nr: number
  public c = 0
  public pointer: JsonPointer

  public created_at: Date
  public updated_at: Date | null = null

  // TODO: hacked in used by chix-chi
  public chi: {[gid: string]: string} = {}

  constructor(
    data?: any,
    type?: PacketType,
    n?: number,
    c?: number,
    pp?: string
  ) {
    if (pp) {
      this.pointerPath = pp
    }

    this.type = type || typeof data

    // string must be assigned to? or {".": "string"}
    // which means base is /. instead of ''
    this.__data =
      data instanceof ContainerPacket ? data : new ContainerPacket(data)
    this.nr = n || nr++
    // clone version
    if (c) {
      this.c = c
    }

    this.pointer = JsonPointer.create(this.pointerPath)

    Object.defineProperty(this, 'data', {
      get() {
        throw Error('data property should not be accessed')
      },
      set() {
        throw Error('data property should not be written to')
      },
    })

    this.created_at = new Date()
  }

  /**
   *
   * Pointer is a JSON Pointer.
   *
   * If the pointer does not start with a slash
   * the pointer will be (forward) relative to the current position
   * If the pointer is empty the pointer will be to the root.
   *
   * @param {Object} owner
   * @param {String} pointer JSON Pointer
   */
  public point(owner: any, pointer: string): Packet {
    if (this.isOwner(owner)) {
      if (pointer === undefined || pointer === '') {
        this.pointerPath = '/.'
      } else if (pointer[0] === '/') {
        this.pointerPath = '/.' + pointer
      } else {
        this.pointerPath = this.pointer.pointer + '/' + pointer
      }
      this.pointer = JsonPointer.create(this.pointerPath)
    }

    return this
  }

  public read(owner?: any): any {
    if (!this.hasOwner() || this.isOwner(owner)) {
      return this.pointer.get(this.__data)
    }

    throw Error('Not allowed to read packet')
  }

  public write(owner?: any, data?: any, type?: string): Packet {
    if (this.isOwner(owner)) {
      this.updated_at = new Date()

      this.pointer.set(this.__data, data)
      if (type) {
        this.type = type
      }

      return this
    }

    throw Error('Not allowed to write packet')
  }

  /**
   * Clone the current packet
   *
   * In case of non plain objects it's mostly desired
   * not to clone the data itself, however do create a *new*
   * packet with the other cloned information.
   *
   * To enable this set cloneData to true.
   *
   * @param {Object} owner Owner of the packet
   */
  public clone(owner?: any): Packet {
    if (this.isOwner(owner)) {
      const packet = new Packet(
        null,
        undefined,
        this.nr,
        this.c + 1,
        this.pointerPath
      )

      packet.setOwner(owner)

      if (!this.type) {
        throw Error('Refusing to clone substance of unknown type')
      }
      // TODO: make sure String Object are always lowercase.
      // I think they are..
      if (this.type === 'function' || /[A-Z]/.test(this.type as string)) {
        // do not clone, ownership will throw if things go wrong
        packet.__data = this.__data
      } else {
        try {
          packet.__data = JSON.parse(JSON.stringify(this.__data))
        } catch (e) {
          console.log('ERROR', this.__data, e)
          throw e
        }
      }

      packet.type = this.type
      packet._meta = JSON.parse(JSON.stringify(this._meta))

      return packet
    }

    throw Error('Packet is not owned by this owner, refusing to clone.')
  }

  public setType(owner: any, type: string): Packet {
    if (this.isOwner(owner)) {
      this.typeTrail.push(this.type as string)
      this.type = type

      return this
    }

    // throw in isOwner is disabled for now.
    return this
  }

  public release(owner?: any): Packet {
    if (this.isOwner(owner)) {
      this.trail.push({
        id: owner.id,
        pid: owner.pid,
      })
      this.owner = undefined

      return this
    }

    // throw in isOwner is disabled for now.
    return this
  }

  public setOwner(newOwner: any): Packet {
    if (this.owner === undefined) {
      this.owner = newOwner

      return this
    }

    if (newOwner === this.owner) {
      debug('Warning: Packet already owned by this owner')

      return this
    }

    throw Error('Refusing to overwrite owner')
  }

  public hasOwner(): boolean {
    return this.owner !== undefined
  }

  public isOwner(owner: any): true {
    if (owner === this.owner) {
      return true
    }

    if (!owner) {
      throw Error('Packet.isOwner expects an owner object as parameter')
    } else if (this.owner === undefined) {
      debug('Warning: Packet is unclaimed, claim it first')
      this.setOwner(owner)

      return true
    }

    debug(
      'Warning: Packet is not owned by this instance. REQUESTOR: %s:%s OWNER: %s:%s',
      owner.constructor.name,
      owner.name || owner,
      this.owner.constructor.name,
      this.owner.name || this.owner
    )

    this.release(this.owner)
    this.setOwner(owner)

    return true
  }

  public dump(): string {
    return JSON.stringify(this, null, 2)
  }

  // Are these used?
  public set(prop: string, value: any): Packet {
    ;(this as any)[prop] = value

    return this
  }

  public get(prop: string): any {
    return (this as any)[prop]
  }

  public del(prop: string): Packet {
    delete (this as any)[prop]

    return this
  }

  public has(prop: string): boolean {
    return this.hasOwnProperty(prop)
  }

  public meta(ns: string, key?: string, val?: any): any {
    if (val !== undefined) {
      if (!key) {
        throw Error('key must be set.')
      }
      if (!this._meta[ns]) {
        this._meta[ns] = {}
      }

      this._meta[ns][key] = val

      return this
    }

    if (!this._meta.hasOwnProperty(ns)) {
      throw Error(`No such key ${ns}`)
    }

    if (key) {
      if (!this._meta[ns].hasOwnProperty(key)) {
        throw Error(`No such key [${ns}][${key}]`)
      }

      return this._meta[ns][key]
    }

    return this._meta[ns]
  }

  public hasMeta(ns: string, key: string): boolean {
    if (!this._meta.hasOwnProperty(ns)) {
      return false
    }

    return this._meta[ns].hasOwnProperty(key)
  }

  public removeMeta(ns: string, key: string): void {
    if (this._meta.hasOwnProperty(ns)) {
      if (key) {
        if (this._meta[ns].hasOwnProperty(key)) {
          delete this._meta[ns][key]
        }
      } else {
        delete this._meta[ns]
      }
    }
  }

  public toJSON() {
    return this.pointer.get(this.__data)
  }

  public export() {
    return {
      c: this.c,
      data: this.pointer.get(this.__data),
      meta: this._meta,
      nr: this.nr,
      owner: this.owner ? this.owner.id : null,
      pointerPath: this.pointerPath,
      trail: this.trail,
      type: this.type,
      typeTrail: this.typeTrail,
    }
  }
}
