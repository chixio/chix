import {Flow as FlowDefinition, NodeDefinition} from '@chix/common'
import {xNode} from './node/index'

export type Constructor<T = {}> = new (...args: any[]) => T

export type MixinParams = {
  node: NodeDefinition | FlowDefinition
  [key: string]: any
}
// tslint:disable-next-line
export interface xEvent {
  name: string
  expose?: boolean
}

// tslint:disable-next-line
export interface xEvents {
  [name: string]: xEvent
}

export interface NodeError {
  msg: string
  node: xNode
}
