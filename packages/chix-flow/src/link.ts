import {
  Connector as ConnectorDefinition,
  Link as LinkDefinition,
} from '@chix/common'
import {v4 as uuid} from 'uuid'
import {Connector} from './connector'
import {LinkEvents} from './events'
import {Setting} from './setting'
import {Constructor} from './types'
import {validate} from './validate'

/**
 * xLink
 *
 * Settings:
 *
 *   - ttl
 *   - expire
 *   - dispose: true
 *
 * Just need something to indicate it's an iip.
 *
 * @constructor
 * @public
 */
export class Link extends Setting {
  /**
   *
   * Creates a new connection/link
   *
   * Basically takes a plain link object
   * and creates a proper xLink from it.
   *
   * The internal map holds xLinks, whereas
   * the source map is just plain JSON.
   *
   * Structure wise they are almost the same.
   *
   * @param {LinkDefinition} linkDefinition
   * @param {Constructor<Link>} LinkType type
   * @return {Link} LinkType
   * @public
   */
  public static create(
    linkDefinition: LinkDefinition = {},
    LinkType: Constructor<Link> = Link
  ) {
    if (!linkDefinition.source) {
      linkDefinition.source = {} as ConnectorDefinition
    }
    if (!linkDefinition.target) {
      linkDefinition.target = {} as ConnectorDefinition
    }
    const link = new LinkType(linkDefinition.id, linkDefinition.ioid)

    if (linkDefinition.source || linkDefinition.target) {
      link.build(linkDefinition)
    }

    return link
  }

  public data: any
  public fills = 0
  public writes = 0
  public metadata: {[key: string]: any} = {}

  public id: string
  public ioid?: string

  public source: Connector = new Connector()
  public target: Connector = new Connector()

  public graphId?: string
  public graphPid?: string

  constructor(id = uuid(), ioid = uuid()) {
    super()

    this.id = id
    this.ioid = ioid
  }

  public build(linkDefinition: LinkDefinition) {
    if (!linkDefinition.source) {
      throw Error('Create link expects a source')
    }

    if (!linkDefinition.target) {
      throw Error('Create link expects a target')
    }

    this.source.clearSettings()
    this.target.clearSettings()

    validate.link(linkDefinition)

    this.setSource(
      linkDefinition.source.id,
      linkDefinition.source.port,
      linkDefinition.source.setting,
      linkDefinition.source.action
    )

    if (linkDefinition.metadata) {
      this.setMetadata(linkDefinition.metadata)
    } else {
      this.setMetadata({})
    }

    this.setTarget(
      linkDefinition.target.id,
      linkDefinition.target.port,
      linkDefinition.target.setting,
      linkDefinition.target.action
    )
  }

  /**
   *
   * Set target
   *
   * @param {String} targetId
   * @param {String} port
   * @param {Object} settings
   * @param {String} action
   * @public
   */
  public setTarget(
    targetId: string,
    port: string,
    settings?: any,
    action?: string
  ) {
    if (settings) {
      this.target.configure(settings)
    }
    this.target.wire = this
    this.target.plug(targetId, port, action)
  }

  /**
   *
   * Set Source
   *
   * @param {Object} sourceId
   * @param {String} port
   * @param {Object} settings
   * @param {String} action
   * @public
   */
  public setSource(
    sourceId: string,
    port: string,
    settings?: any,
    action?: string
  ) {
    if (settings) {
      this.source.configure(settings)
    }
    this.source.wire = this
    this.source.plug(sourceId, port, action)
  }

  /**
   *
   * Setting of pids is delayed.
   * I would like them to be available during plug.
   * but whatever.
   *
   */

  public setSourcePid(pid: string) {
    this.source.setPid(pid)
  }

  public setTargetPid(pid: string) {
    this.target.setPid(pid)
  }

  public hasMetadata(): boolean {
    return Object.keys(this.metadata).length > 0
  }

  public setMetadata(metadata: any) {
    this.metadata = metadata
  }

  public setMeta(key: string, val: any) {
    this.metadata[key] = val
  }

  /**
   *
   * Set Title
   *
   * @param {String} title
   * @fires Link#metadata
   * @public
   */
  public setTitle(title: string) {
    this.setMeta('title', title)
    this.emit(LinkEvents.CHANGE, this, 'metadata', this.metadata)
  }

  /**
   *
   * @fires Link#clear
   * @public
   */
  public clear() {
    this.fills = 0
    this.writes = 0
    this.emit(LinkEvents.CLEAR, this)
  }

  /**
   * Update link by passing it a full object.
   *
   * Will only emit one change event.
   *
   * @fires Link#change
   * @public
   */
  public update(linkDefinition: LinkDefinition) {
    this.build(linkDefinition)
    this.emit(LinkEvents.CHANGE, this)
  }

  public toJSON() {
    // TODO: use schema validation for toJSON
    if (!this.hasOwnProperty('source')) {
      console.log(this)
      throw Error('Link should have a source property')
    }
    if (!this.hasOwnProperty('target')) {
      throw Error('Link should have a target property')
    }

    const link: any = {
      id: this.id,
      source: this.source.toJSON(),
      target: this.target.toJSON(),
    }

    if (this.metadata) {
      link.metadata = this.metadata
    }

    if (this.fills) {
      link.fills = this.fills
    }

    if (this.writes) {
      link.writes = this.writes
    }

    if (this.data !== undefined) {
      link.data = JSON.parse(JSON.stringify(this.data))
    }

    return link
  }
}
