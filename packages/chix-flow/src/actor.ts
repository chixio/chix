import {Flow as FlowDefinition} from '@chix/common'
import {Flow} from './flow'

const defaultDefinition = {
  id: 'flow:main',
  links: [],
  name: 'main',
  nodes: [],
  ns: 'flow',
  type: 'flow',
}

export class Actor extends Flow {
  constructor(flowDefinition: FlowDefinition = defaultDefinition) {
    super(
      'flow:main',
      flowDefinition,
      undefined,
      undefined,
      undefined,
      undefined,
      true
    )
  }
}
