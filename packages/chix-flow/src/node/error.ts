import {$Event, $Status} from '../common'
import {NodeEvents} from '../events'
import {Constructor} from '../types'
import {mixin} from '../util'

export type $ErrorExtensionType = $Event & $Status

export interface $Error {
  error: (error: Error | string) => Error
}

export function $Error<T extends Constructor<$ErrorExtensionType>>(
  Base: T
): Constructor<$Error> & T {
  return class Error$ extends Base implements $Error {
    /**
     *
     * Could be used to externally set a node into error state
     *
     * BaseNode.error(node, Error('you are bad')
     *
     * @param {Error| string} error
     * @returns {Error}
     */
    public error(error: Error | string): Error {
      error = error instanceof Error ? error : Error(error as string)

      // Update our own status
      this.setStatus('error')

      // TODO: better to have full (custom) error objects
      const errorObject = {
        // node: node.export(),
        msg: error,
        node: this, // do not export so soon.
      }

      this.event(NodeEvents.ERROR, errorObject)

      return error
    }
  }
}

export namespace $Error {
  export function create(Base: any) {
    return mixin($Event.create, $Status.create)(Base)
  }
}
