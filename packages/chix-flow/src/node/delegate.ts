import {Constructor} from '../types'
import {mixin} from '../util'
import {$CallbackWrapper} from './callbackWrapper'

export type $DelegateExtensionType = $CallbackWrapper

export interface $Delegate {
  _delegate: (output: any[]) => void
}

export function $Delegate<T extends Constructor<$DelegateExtensionType>>(
  Base: T
): Constructor<$Delegate> & T {
  return class Delegate$ extends Base implements $Delegate {
    /**
     *
     * Execute the delegated callback for this node.
     *
     * [fs, 'readFile', '/etc/passwd']
     *
     * will execute:
     *
     * fs['readFile']('/etc/passwd', this.callbackWrapper)
     *
     * @param {Object} output
     * @emits Node#branching
     */
    public _delegate(output: any[]): void {
      const fn = output.splice(0, 1).pop()
      const method = output.splice(0, 1).pop()

      output.push(this._callbackWrapper.bind(this))

      fn[method].apply(fn, output)
    }
  }
}

export namespace $Delegate {
  export function create(Base: any) {
    return mixin($Delegate, $CallbackWrapper.create)(Base)
  }
}
