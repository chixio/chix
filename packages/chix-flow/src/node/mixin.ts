import {
  $Event,
  $ExportCommon,
  $Identity,
  $Meta,
  $Parent,
  $Status,
} from '../common'
import {mixin} from '../util'
import {$CallbackWrapper} from './callbackWrapper'
import {$Context} from './context'
import {$Control} from './control'
import {$Delegate} from './delegate'
import {$Dependency} from './dependency'
import {$Error} from './error'
import {$Export} from './export'
import {$Fill} from './fill'
import {$Init} from './init'
import {$IsStartable} from './isStartable'
import {$Nodebox} from './nodebox'
import {$Output} from './output'
import {$Port} from './port'
import {$Ports} from './ports'
import {$Process} from './process'
import {$Report} from './report'
import {$RunOnce} from './runOnce'
import {$Shutdown} from './shutdown'
import {$Start} from './start'
import {$ToJSON} from './toJSON'

export type NodeMixinType<T> = $CallbackWrapper &
  $Context &
  $Delegate &
  $Dependency &
  $Fill &
  $Init &
  $Nodebox &
  $Output &
  $Port &
  $RunOnce &
  $Shutdown &
  $Start &
  $ToJSON &
  $Report &
  $ExportCommon &
  $Parent<T> &
  $IsStartable &
  $Control &
  $Process &
  $Error &
  $Ports &
  $Event &
  $Status &
  $Meta &
  $Identity

export const NodeMixin = mixin(
  $CallbackWrapper,
  $Context,
  $Delegate,
  $Dependency,
  $Fill,
  $Init,
  $Nodebox,
  $Output,
  $Port,
  $RunOnce,
  $Shutdown,
  $Start,
  $ToJSON,
  $Report,
  $Export,
  $Parent,
  $IsStartable,
  $Control,
  $Process,
  $Error,
  $Ports,
  $Event,
  $Status,
  $Meta,
  $Identity
)
