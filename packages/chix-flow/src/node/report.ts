import {$Identity, $Meta, $Status} from '../common'
import {Constructor} from '../types'
import {mixin} from '../util'
import {$Context} from './context'
import {$Control} from './control'
import {$Export} from './export'
import {$Ports} from './ports'

export type $ReportExtensionType = $Context &
  $Control &
  $Identity &
  $Meta &
  $Ports &
  $Status

export interface $Report {
  report: () => any
}

export function $Report<T extends Constructor<$ReportExtensionType>>(
  Base: T
): Constructor<$Report> & T {
  return class Report$ extends Base implements $Report {
    /**
     *
     * @returns {Object}
     * @public
     */
    public report(): any {
      return {
        context: this.context,
        filled: this.filled,
        id: this.id,
        identifier: this.identifier,
        // input: this.filteredInput(),
        outputCount: this.outputCount,
        ports: this.ports,
        runCount: this.runCount,
        state: this.state,
        status: this.status,
        title: this.title,
        // connections: this._connections.toJSON()
      }
    }
  }
}

export namespace $Report {
  export function create(Base: any) {
    return mixin(
      $Report,
      $Context.create,
      $Control.create,
      $Export.create,
      $Identity.create,
      $Meta.create,
      $Ports.create,
      $Status.create
    )(Base)
  }
}
