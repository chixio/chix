import {$Identity, $Meta} from '../common'
import {Constructor} from '../types'
import {mixin} from '../util'
import {$Process} from './process'

export type $ToJSONExtensionType = $Identity & $Meta & $Process

export interface $ToJSON {
  toJSON: () => any
}

export function $ToJSON<T extends Constructor<$ToJSONExtensionType>>(
  Base: T
): Constructor<$ToJSON> & T {
  return class ToJSON$ extends Base implements $ToJSON {
    /**
     *
     * Returns the node representation to be stored along
     * with the graph.
     *
     * This is not the full definition of the node itself.
     *
     * The full definition can be found using the ns/name pair
     * along with the provider property.
     *
     */
    public toJSON(asProcess: boolean = false): any {
      const props = ['title', 'description', 'metadata', 'provider']
      const json: {[key: string]: any} = {
        id: this.id,
        name: this.name,
        ns: this.ns,
      }
      if (asProcess) {
        json.pid = this.pid
      }
      props.forEach((prop: string) => {
        const value = (this as any)[prop]

        if (value) {
          if (typeof value !== 'object' || Object.keys(value).length > 0) {
            json[prop] = value
          }
        }
      })

      return json
    }
  }
}

export namespace $ToJSON {
  export function create(Base: any) {
    return mixin($ToJSON, $Identity.create, $Meta.create, $Process.create)(Base)
  }
}
