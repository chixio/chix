import {forOf} from '@fbpx/lib'
import {InputPort} from '../port'
import {Constructor} from '../types'
import {mixin} from '../util'
import {$Ports} from './ports'

export type $StartableExtensionType = $Ports

export interface $IsStartable {
  isStartable: () => boolean
}
export function $IsStartable<T extends Constructor<$StartableExtensionType>>(
  Base: T
): Constructor<$IsStartable> & T {
  return class IsStartable$ extends Base implements $IsStartable {
    public isStartable(): boolean {
      return (
        forOf(
          (_name: string, port: InputPort) => port.isFilled() || undefined,
          this.ports.input
        ).length === this.inPorts.length
      )
    }
  }
}

export namespace $IsStartable {
  export function create(Base: any) {
    return mixin($IsStartable, $Ports.create)(Base)
  }
}
