import {
  InputPortsDefinition,
  NodeDefinition,
  NodeDependencies,
} from '@chix/common'
import {$Identity} from '../common'
import {NodeBox} from '../sandbox/node'
import {Constructor} from '../types'
import {getNodeArgument, mixin, safeName} from '../util'
import {$Control} from './control'
import {$Dependency} from './dependency'
import {$Output} from './output'
import {$Port} from './port'

export type $NodeboxExtensionType = $Identity &
  $Control &
  $Dependency &
  $Output &
  $Port

export interface $Nodebox {
  _delay: number
  async: boolean
  nodeDefinition: NodeDefinition
  dependencies: NodeDependencies | undefined
  expose: string[] | undefined
  fn: any
  inputTimeout: number
  interval: number
  nodebox: any /* FIXME! NodeBox | undefined */
  nodeTimeout: number
  setupNodeBox: () => void
}

export function $Nodebox<T extends Constructor<$NodeboxExtensionType>>(
  Base: T
): Constructor<$Nodebox> & T {
  return class Nodebox$ extends Base implements $Nodebox {
    /** delay interval */
    public _delay = 0
    public async: boolean = false
    public nodeDefinition: NodeDefinition
    public dependencies: NodeDependencies | undefined
    public expose: string[] | undefined
    public fn: any
    public interval = 100
    public inputTimeout: number = 3000
    public nodebox: NodeBox | undefined
    public nodeTimeout: number = 3000

    constructor(...args: any[]) {
      super(...args)

      const {node} = getNodeArgument(args) as {node: NodeDefinition}

      if (node.expose) {
        this.expose = node.expose
      }

      if (node.dependencies) {
        this.dependencies = node.dependencies
      }

      if (node.nodeTimeout) {
        this.nodeTimeout = node.nodeTimeout
      }

      if (node.inputTimeout) {
        this.inputTimeout = node.inputTimeout
      }

      if (!this.async) {
        // extended might have set it
        if (node.async) {
          this.async = node.async
        }
      }

      // remember def for .compile()
      this.nodeDefinition = node
    }

    public async setupNodeBox(): Promise<void> {
      const functionName = safeName(`${this.ns}_${this.name}`)
      this.nodebox = new NodeBox(functionName)
      this.nodebox.set('done', this.complete.bind(this))
      this.nodebox.set('cb', this._asyncOutput.bind(this))
      this.nodebox.set('state', this.state)

      if (this.dependencies && this.dependencies.npm) {
        this.nodebox.addDependencies(
          this.dependencies.npm,
          this.dependencyLoader
        )
      }

      if (this.expose) {
        this.nodebox.expose(this.expose)
      }

      this.nodebox.set('output', this.async ? this._asyncOutput.bind(this) : {})

      if (!this.nodeDefinition.ports) {
        throw Error('Node definition does not contain any ports.')
      }

      if (this.nodeDefinition.hasOwnProperty('fn')) {
        // Note: fn: '' is allowed.
        this.fn = this.nodeDefinition.fn
      }

      if (this.nodeDefinition.ports.input) {
        if (this._isPreloaded(this.nodeDefinition.ports.input)) {
          if (this.fn) {
            // this.nodebox.fill(this.fn)
            // not tested..
            this.nodebox.fill(this.fn)
          }

          if (this.async) {
            this.nodebox.state = this.nodeDefinition.state
          }
        } else {
          if (this.fn !== undefined) {
            this.nodebox.compile(this.fn)
          }

          if (this.async) {
            // This collects the port definitions they
            // attach to `on`
            this.nodebox.run()
          }
        }
      }

      this.state = this.nodebox.state
    }

    /**
     *
     * Test whether this is a preloaded node.
     *
     * @private
     */
    public _isPreloaded(inputPorts: InputPortsDefinition): boolean {
      if (typeof this.fn === 'function') {
        return true
      }

      for (const port in inputPorts) {
        if (inputPorts.hasOwnProperty(port)) {
          if (inputPorts[port].fn) {
            return true
          }
        }
      }

      return false
    }
  }
}

export namespace $Nodebox {
  export function create(Base: any) {
    return mixin(
      $Nodebox,
      $Identity.create,
      $Control.create,
      $Dependency.create,
      $Output.create,
      $Port.create
    )(Base)
  }
}
