import {$Identity, $Meta, $Parent, $Status} from '../common'
import {$ExportCommon} from '../common/interfaces'
import {Constructor} from '../types'
import {mixin} from '../util'
import {$Context} from './context'
import {$Control} from './control'
import {$Nodebox} from './nodebox'
import {$Ports} from './ports'
import {$Process} from './process'
import {$Start} from './start'

export type $ExportExtensionType<T> = $Context &
  $Control &
  $Identity &
  $Meta &
  $Nodebox &
  $Parent<T> &
  $Ports &
  $Process &
  $Status &
  $Start

export function $Export<T extends Constructor<$ExportExtensionType<T>>>(
  Base: T
): Constructor<$ExportCommon> & T {
  return class Export$ extends Base implements $ExportCommon {
    /**
     *
     * Do a lightweight export of this node.
     *
     * Used in emit's to give node information
     *
     *
     * @return {Object}
     * @public
     */
    public export(): any {
      return {
        active: this.active,
        context: this.context,
        dependencies: this.dependencies,
        expose: this.expose,
        filled: this.filled,
        id: this.id,
        identifier: this.identifier,
        inPorts: this.inPorts,
        // input: this.filteredInput(),
        inputTimeout: this.inputTimeout,
        metadata: this.metadata,
        name: this.name,
        nodeTimeout: this.nodeTimeout,
        ns: this.ns,
        outPorts: this.outPorts,
        parentId: this.hasParent() ? (this.getParent() as any).id : null,
        pid: this.pid,
        ports: this.ports,
        provider: this.provider,
        runCount: this.runCount,
        state: this.state,
        status: this.status,
        title: this.title,
      }
    }

    /**
     *
     * Filters the input for export.
     *
     * Leaving out everything defined as a function
     *
     * Note: depends on the stage of emit whether this value contains anything
     *
     * @return {Object} Filtered Input
     * @private
     */
    // TODO: Input now only exists within the ports, so fetch it from there.
    public filteredInput(): void /*NodeInput*/ {
      /*
      const input: NodeInput = {}

      for (const port in this.input) {
        if (this.input.hasOwnProperty(port)) {

          if (this.portExists('input', port)) {
            const type = (this.ports as any).input[port].type

            if (type === 'string' ||
              type === 'number' ||
              type === 'enum' ||
              type === 'boolean') {
              input[port] = this.input[port]
            } else {
              // can't think of anything better right now
              input[port] = Object.prototype.toString.call(this.input[port])
            }
          } else {
            // faulty but used during export so we want to know
            input[port] = this.input[port]
          }
        }
      }

      return input
      */
    }
  }
}

export namespace $Export {
  export function create(Base: any) {
    return mixin(
      $Context.create,
      $Control.create,
      $Identity.create,
      $Meta.create,
      $Nodebox.create,
      $Parent.create,
      $Ports.create,
      $Process.create,
      $Status.create,
      $Start.create
    )(Base)
  }
}
