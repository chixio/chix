import d from 'debug'
import {$Identity} from '../common'
import {Connector} from '../connector'
import {PortBox} from '../sandbox/port'
import {Constructor} from '../types'
import {mixin} from '../util'
import {$Dependency} from './dependency'
import {$Nodebox} from './nodebox'
import {$Output} from './output'
import {$Ports} from './ports'

const debug = d.debug('chix:node')

export type $PortExtensionType = $Identity &
  $Dependency &
  $Nodebox &
  $Ports &
  $Output

export interface $Port {
  CHI: any
  _createPortBox: (fn: string, name: string) => PortBox
  $portIsFilled: (port: string) => boolean
  portIsOpen: (name: string) => boolean
  inputPortAvailable: (target: Connector) => boolean
  handlePortSettings: (port: string) => void
}

export function $Port<T extends Constructor<$PortExtensionType>>(
  Base: T
): Constructor<$Port> & T {
  return class Port$ extends Base implements $Port {
    public CHI: any
    /**
     *
     * Executes the async variant
     *
     * state is the only variable which will persist.
     *
     * @param {string} fn - PortBox Function Body
     * @param {string} name - PortBox Function Name
     * @returns {PortBox}
     * @private
     */
    public _createPortBox(fn: string, name: string): PortBox {
      debug('%s: creating portBox `%s`', this.identifier, name)

      const portBox = new PortBox(name)
      portBox.set('state', this.nodebox.state)
      portBox.set('output', this._asyncOutput.bind(this))

      if (this.dependencies && this.dependencies.npm) {
        portBox.addDependencies(this.dependencies.npm, this.dependencyLoader)
      }

      if (this.expose) {
        portBox.expose(this.expose) // , this.CHI)
      }

      if (typeof fn !== 'function') {
        fn = fn.slice(fn.indexOf('{') + 1, fn.lastIndexOf('}'))
        portBox.compile(fn)
      } else {
        portBox.fill(fn)
      }

      return portBox
    }

    public $portIsFilled(port: string): boolean {
      const inputPorts = this.ports.input

      if (inputPorts) {
        const inputPort = (inputPorts as any)[port]

        return inputPort.isFilled()
      }

      return false
    }

    public handlePortSettings(_port: string): void {
      throw Error('Method not implemented...')
      /*
      if (this.ports.input.hasOwnProperty(port)) {
      }
      const inputPorts = this.ports.input

      if (inputPorts) {
        const inputPort = (inputPorts as any)[port]

        inputPort.....
      }
      */
    }

    // TODO: not sure if these are still used, probably should be checked on the ports themselves
    public portIsOpen(_name: string): boolean {
      return true
    }

    public inputPortAvailable(_target: Connector): boolean {
      return true
    }
  }
}

export namespace $Port {
  export function create(Base: any) {
    return mixin(
      $Port,
      $Dependency.create,
      $Identity.create,
      $Nodebox.create,
      $Ports.create,
      $Output.create
    )(Base)
  }
}
