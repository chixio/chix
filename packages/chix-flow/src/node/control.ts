import {forOf} from '@fbpx/lib'
import d from 'debug'
import {$Event, $ExportCommon, $Identity, $Status} from '../common'
import {NodeEvents} from '../events'
import {InputPort} from '../port'
import {Constructor} from '../types'
import {mixin} from '../util'
import {$Export} from './export'
import {$Ports} from './ports'

const debug = d.debug('chix:node:control')

export type $ControlExtensionType = $Event &
  $ExportCommon &
  $Identity &
  $Ports &
  $Status

export interface $Control {
  __halted: boolean
  state: any
  active: boolean
  destroy: () => void
  runCount: number
  outputCount: number
  // start: () => void
  hold: () => void
  release: () => void
  isHalted: () => boolean
  complete: () => void
  reset: () => void
  freeInput: () => void
}

export function $Control<T extends Constructor<$ControlExtensionType>>(
  Base: T
): Constructor<$Control> & T {
  return class Control$ extends Base implements $Control {
    public active = false
    public runCount = 0
    public outputCount = 0

    public state = {}
    public __halted: boolean = false

    public start() {
      throw Error('Node needs to implement start() method')
    }

    /**
     *
     * Holds all input until release is called
     *
     * @public
     */
    public hold(): void {
      this.setStatus('hold')

      this.__halted = true
    }

    /**
     *
     * Releases the node if it was on hold
     *
     * @public
     */
    public release(): void {
      this.setStatus('running')

      if (this.__halted) {
        this.__halted = false
      }
    }

    public isHalted(): boolean {
      return this.__halted
    }

    /**
     *
     * Node completion
     *
     * Sends an empty string to the :complete port.
     * Each node automatically has one of those available.
     *
     * Emits the complete event and frees all input ports.
     *
     * @private
     */
    public complete(): void {
      this.active = false
      /**
       * Complete Event.
       *
       * The node has completed.
       *
       * TODO: a node can set itself as being active
       * active must be taken into account before calling
       * a node complete. As long as a node is active
       * it is not complete.
       *
       * @event Node#complete
       * @type {object}
       * @property {object} node - An export of this node
       */

      // this.setStatus('complete')

      this.event(NodeEvents.COMPLETE, {
        node: this.export(),
      })
    }

    /**
     *
     * Live reset, connections, etc. stay alive.
     *
     */
    public reset(): void {
      debug('%s: reset', this.identifier)

      // clear any input
      this.freeInput()

      // reset any internal state.
      this.state = {}

      this.runCount = 0

      this.status = 'stopped'

      this.start()
    }

    public freeInput(): void {
      forOf((_name: string, port: InputPort) => {
        port.reset()
      }, this.ports.input)
    }

    public destroy(): void {}
  }
}

export namespace $Control {
  export function create(Base: any) {
    return mixin(
      $Event.create,
      $Export.create,
      $Identity.create,
      $Ports.create,
      $Status.create
    )(Base)
  }
}
