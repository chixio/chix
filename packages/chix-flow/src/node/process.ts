import {$Parent} from '../common/parent'
import {Constructor} from '../types'
import {mixin} from '../util'

export interface $Process {
  pid: string | null
  getPid(): string
  setPid(pid: string): void
}

export type $ProcessExtensionType<T> = $Parent<T>

export function $Process<T extends Constructor<$ProcessExtensionType<T>>>(
  Base: T
): Constructor<$Process> & T {
  return class Process$ extends Base implements $Process {
    public pid: string | null = null

    public getPid(): string {
      if (!this.pid) {
        throw Error('Failed to get pid')
      }

      return this.pid
    }

    /**
     *
     * @param {type} pid
     * @public
     */
    public setPid(pid: string): void {
      this.pid = pid
    }
  }
}

export namespace $Process {
  export function create(Base: any) {
    return mixin($Process, $Parent.create)(Base)
  }
}
