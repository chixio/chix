import d from 'debug'
import {$Event, $ExportCommon, $Identity, $Status} from '../common'
import {NodeEvents} from '../events'
import {PortBox} from '../sandbox/port'
import {Constructor} from '../types'
import {mixin} from '../util'
import {$Control} from './control'
import {$Export} from './export'
import {$Nodebox} from './nodebox'
import {$Port} from './port'
import {$Ports} from './ports'
import {$RunOnce} from './runOnce'
import {$Shutdown} from './shutdown'

const debug = d.debug('chix:actor')

export type $StartExtensionType = $Control &
  $Identity &
  $Event &
  $ExportCommon &
  $RunOnce &
  $Nodebox &
  $Port &
  $Ports &
  $Status &
  $Shutdown

export interface $Start {
  active: boolean
  start(): void
  __start(params: any): void
}

// THIS STUFF IS INCORRECT.
// is more of a load method then a run each and every time thing
// actually why not just use the compile() to make it compiled.
export function $Start<T extends Constructor<$StartExtensionType>>(
  Base: T
): Constructor<$Start> & T {
  return class Start$ extends Base implements $Start {
    public interval!: number
    public _delay!: number
    public active: boolean = false
    public state: any
    private _onStart: PortBox

    public start(): void {
      if (['created', 'stopped'].indexOf(this.status) >= 0) {
        this.setStatus('started')
        debug('%s: running on start', this.identifier)
        if (this.nodebox.on.start) {
          // Run onStart functionality first
          const name = `${this.ns}_${this.name}`

          if (!this._onStart) {
            const onStart =
              this.nodebox.on.start === 'function'
                ? this.nodebox.on.start
                : this.nodebox.on.start.toString()

            this._onStart = this._createPortBox(onStart, name)

            this._onStart.run(this)

            this.nodebox.state = this.state = this._onStart.state
          } else {
            this._onStart.run(this)
          }
        }

        this.event(NodeEvents.STARTED, {
          node: this.export(),
        })

        this.setStatus('running')
      } else {
        throw Error(
          'Only can start node which is in the `created` or `stopped` state, current status: ' +
            this.status
        )
      }
    }

    public __start(params: any): void {
      if (this.active) {
        debug('%s: node still active delaying', this.identifier)

        this._delay = this._delay + this.interval

        setTimeout(() => {
          this.__start(params)
        }, 500 + this._delay)
      } else {
        // set active state.
        this.active = true

        // document, is for servers etc, next run must shut the old one down
        if (!this.async) {
          if (this.nodebox.on) {
            if (this.nodebox.on.shutdown) {
              debug('%s: running shutdown', this.identifier)
              this.shutdown()
            }
          }
        }

        this.nodebox.set('$', params)

        // done before compile.
        // this.nodebox.output = this.async ? this._asyncOutput.bind(this) : {}

        this._runOnce()
      }
    }
  }
}

export namespace $Start {
  export function create(Base: any) {
    return mixin(
      $Start,
      $Control.create,
      $Identity.create,
      $Event.create,
      $Export.create,
      $RunOnce.create,
      $Nodebox.create,
      $Port.create,
      $Ports.create,
      $Status.create,
      $Shutdown.create
    )(Base)
  }
}
