import {forOf} from '@fbpx/lib'
import {$PortsCommon} from '../common'
import {Packet} from '../packet'
import {Constructor} from '../types'
import {mixin} from '../util'
import {$Delegate} from './delegate'
import {$Ports, PortGroup} from './ports'

export type FunctionOutput = (...args: any[]) => void
export interface ObjectOutput {
  [port: string]: any
}
export type ArrayOutput = any[]
export type OutputType = FunctionOutput | ArrayOutput | ObjectOutput

export type $OutputExtensionType = $Delegate & $PortsCommon & $Ports

export interface $Output {
  _asyncOutput: (output: ObjectOutput) => void
  _output: (output: OutputType) => void
}

export function $Output<T extends Constructor<$OutputExtensionType>>(
  Base: T
): Constructor<$Output> & T {
  return class Output$ extends Base implements $Output {
    /**
     *
     * This node handles the output of the `blackbox`
     *
     * It is specific to the API of the internal Chix node function.
     *
     * out = { port1: data, port2: data }
     * out = [fs.readFile, arg1, arg2 ]
     *
     * Upon output the input will be freed.
     *
     * @param {Object} output
     * @private
     */
    public _asyncOutput(output: ObjectOutput): void {
      // delegate and object output has
      // synchronous output on _all_ ports
      // however we do not know if we we're called from
      // the function type of output..
      forOf((port: PortGroup, packet: Packet) => {
        this.sendPortOutput(port, packet)
      }, output)
    }

    /**
     *
     * Output
     *
     * Directs the output to the correct handler.
     *
     * If output is a function it is handled by asyncOutput.
     *
     * If it's an array, it means it's the shorthand variant
     *
     * e.g. output = [fs, 'readFile']
     *
     * This will be handled by the delegate() method.
     *
     * Otherwise it is a normal output object containing the output for the ports.
     *
     * e.g. { out1: ...,  out2: ...,  error: ... } etc.
     *
     * TODO: not sure if this should always call complete.
     *
     * @param {Object} output
     * @private
     */
    public _output(output: OutputType): void {
      if (typeof output === 'function') {
        ;(output as FunctionOutput).call(this, this._asyncOutput.bind(this))
      } else if (Array.isArray(output)) {
        this._delegate(output as ArrayOutput)
      } else {
        forOf((port: string, packet: Packet) => {
          this.sendPortOutput(port as any, packet)
        }, output as ObjectOutput)
      }
    }
  }
}

export namespace $Output {
  export function create(Base: any) {
    return mixin($Output, $Delegate.create, $Ports.create)(Base)
  }
}
