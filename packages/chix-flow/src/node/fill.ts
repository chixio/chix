import {forOf} from '@fbpx/lib'
import d from 'debug'
import {$Event, $Identity, $Status} from '../common'
import {NodeEvents} from '../events'
import {PacketContainer} from '../packet'
import {Port} from '../port'
import {AsyncInputPort, InputPort} from '../port/'
import {Constructor} from '../types'
import {mixin} from '../util'
import {$Control} from './control'
import {$Nodebox} from './nodebox'
import {$Ports} from './ports'
import {$Start} from './start'

const debug = d.debug('chix:node')

export type $FillExtensionType = $Control &
  $Event &
  $Nodebox &
  $Identity &
  $Ports &
  $Status &
  $Start

export interface $Fill {
  onPortFill: () => boolean | undefined
  notFilled: string[]
}

export function $Fill<T extends Constructor<$FillExtensionType>>(
  Base: T
): Constructor<$Fill> & T {
  return class Fill$ extends Base implements $Fill {
    public notFilled: string[] = []

    public onPortFill(): boolean | undefined {
      debug('%s onPortFill', this.identifier)

      if (this.status === 'hold') {
        this.__halted = true
        this.event(NodeEvents.PORT_FILL, this)

        return undefined
      }

      if (this.status !== 'running') {
        throw Error(
          'onPortFill: node must be in `running` state, current state: ' +
            this.status
        )
      }

      const inputPorts = this.ports.input

      this.notFilled = []

      const res = forOf((name: string, port: InputPort) => {
        if (port.isSync()) {
          const ret = port.isFilled()

          if (ret) {
            debug(
              '%s.%s: ready using `%s`',
              this.identifier,
              name,
              Port.Message[ret]
            )
            return true
          }

          debug('%s.%s: not ready', this.identifier, name)
          this.notFilled.push(name)

          return undefined
        }

        // async is always ready
        debug(
          '%s.%s: ready using `%s`',
          this.identifier,
          name,
          'async always ready'
        )

        return true
      }, inputPorts).length

      if (res === this.inPorts.length) {
        debug('%s:onPortFill running', this.identifier)

        this.setStatus('running')

        // need an emit here to indicate running
        // can also run shutdown then first.

        if (this.async) {
          const atLeastOneAsyncFilled = forOf(
            (_portName: string, port: InputPort) =>
              (port.isAsync() && port.isFilled()) || undefined,
            inputPorts
          ).length

          if (atLeastOneAsyncFilled) {
            this.event(NodeEvents.EXECUTE, {})

            const input: {[name: string]: any} = {}

            forOf((name: string, port: InputPort) => {
              if (port.isSync()) {
                input[name] = port.read()
              } else if (port.isFilled()) {
                input[name] = port.read()
              }
            }, inputPorts)

            let packetContainer: PacketContainer

            try {
              packetContainer = PacketContainer.create(input)
            } catch (e) {
              console.log(
                '%s: INPUT TRIED TO MERGE',
                this.identifier,
                JSON.stringify(input, null, 2)
              )
              throw Error('stop')
              /*
              this.emit('error', {
                node: this,
                // todo: should be named 'error' and be an error everywhere
                msg: e
              })
              this.event(NodeEvents.PORTFILL, this)
              return false
              */
            }

            forOf((name: string, port: AsyncInputPort | InputPort) => {
              // input[name] because already read() above (isFilled() === false)
              if (!port.isSync() && input[name]) {
                debug('%s.%s: execute %s', this.identifier, name, port.fn.name)

                // tslint:disable-next-line
                ;(port as AsyncInputPort).run(packetContainer, this.state) // this state?

                this.state = this.nodebox.state = port.fn.state
              }
            }, inputPorts)
          } else {
            this.event(NodeEvents.PORT_FILL, this)
            return false
          }
        } else {
          this.event(NodeEvents.EXECUTE, {})

          const input: {[name: string]: any} = {}

          forOf((portName: string, port: InputPort) => {
            input[portName] = port.read()
          }, inputPorts)

          let params

          try {
            params = PacketContainer.create(input)
          } catch (e: any) {
            this.emit('error', {
              msg: e.toString(),
              node: this,
              // todo: should be named 'error' and be an error everywhere
            } as any)
            this.event(NodeEvents.PORT_FILL, this)

            return false
          }
          this.__start(params)
        }

        this.runCount++

        this.event(NodeEvents.EXECUTED, {
          node: this,
          // port: port
        })

        // Cleanup all these events
        this.complete()

        this.event(NodeEvents.PORT_FILL, this)

        return true
      } else {
        debug(
          '%s:onPortFill ports: %s not ready',
          this.identifier,
          this.notFilled
        )

        this.event(NodeEvents.PORT_FILL, this)

        return undefined
      }
    }
  }
}

export namespace $Fill {
  export function create(Base: any) {
    return mixin(
      $Fill,
      $Control.create,
      $Event.create,
      $Nodebox.create,
      $Identity.create,
      $Ports.create,
      $Status.create,
      $Start.create
    )(Base)
  }
}
