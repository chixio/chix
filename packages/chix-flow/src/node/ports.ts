import {forOf} from '@fbpx/lib'
import {
  AsyncPort,
  NodeDefinition,
  Port as PortDefinition,
  Ports as PortsDefinition,
} from '@chix/common'
import d from 'debug'
import {$Event, $Identity, $PortsCommon} from '../common'
import {Connector} from '../connector'
import {NodeEvents, PortEvents} from '../events'
import {Packet} from '../packet'
import {InputPort, OutputPort, portFactory} from '../port'
import {Constructor, xEvent} from '../types'
import {getNodeArgument, mixin} from '../util'
import {$Control} from './control'
import {$Fill} from './fill'
import {$Nodebox} from './nodebox'
import {$Port} from './port'

const debug = d.debug('chix:node')

export interface PortOptions {
  [opt: string]: any
}

export type NodeInputPorts = InputPort[]
export type NodeOutputPorts = {[name: string]: OutputPort}

export type NodePorts = {
  input?: NodeInputPorts
  output?: NodeOutputPorts
}

export type PortGroup = 'event' | 'input' | 'output'

export const createPorts = mixin(
  $Nodebox,
  $Control,
  $Event,
  $Fill,
  $Identity,
  $Port
)

export type $PortsExtensionType = $Control &
  $Nodebox &
  $Event &
  $Fill &
  $Identity &
  $Port

export interface $Ports {
  inPorts: string[]
  outPorts: string[]
  openPorts: string[]
  ports: NodePorts
  async: boolean
  filled: number
  handleLinkSettings: (target: Connector) => void
  _initStartPort: () => void
  addPort: (
    type: PortGroup,
    port: string,
    portDefinition: PortDefinition
  ) => InputPort | OutputPort
  closePort: (name: string) => void
  removePort: (type: PortGroup, port: string) => boolean
  renamePort: (type: PortGroup, from: string, to: string) => boolean
  getPort: (type: PortGroup, name: string) => InputPort | OutputPort
  getInputPort: (name: string) => InputPort
  getOutputPort: (name: string) => OutputPort
  getPortOption: (type: PortGroup, name: string, opt: string) => any
  portExists: (type: PortGroup, port: string) => boolean
  getPortType: (kind: PortGroup, port: string | string[]) => string
  setPortOption: (
    type: PortGroup,
    name: string,
    opt: string,
    value: any
  ) => void
  setPortOptions: (type: PortGroup, options: PortOptions) => void
  createPorts: (portsDefinition: PortsDefinition) => void
  _portsAvailable: () => string
}

export function $Ports<T extends Constructor<$PortsExtensionType>>(
  Base: T
): Constructor<$Ports> & T {
  return class Ports$ extends Base implements $Ports, $PortsCommon {
    public async: boolean = false
    public ports: NodePorts = {}

    constructor(...args: any[]) {
      super(...args)

      const {node} = getNodeArgument(args) as {node: NodeDefinition}

      if (!node.ports) {
        throw Error('NodeDefinition does not declare any ports')
      }
    }

    public get openPorts(): string[] {
      const ports: string[] = []

      forOf((name: string, port: InputPort) => {
        if (port.isOpen()) {
          ports.push(name)
        }
      }, this.ports.input)

      return ports
    }

    /**
     *
     * Fills the port.
     *
     * Does the same as fillPort, however it also checks:
     *
     *   - port availability
     *   - port settings
     *
     * FIXME: fill & fillPort can just be merged probably.
     *
     * @param {Object} target
     * @public
     */
    public handleLinkSettings(target: Connector): void {
      if (!target.port) {
        throw Error('Cannot determine target port.')
      }
      // FIX: hold is not handled anywhere as setting anymore
      if (target.has('hold')) {
        this.hold()
      } else if (target.has('persist')) {
        const index = target.get('index')

        const ports = (this.ports as any).input

        if (ports && ports[target.port]) {
          const _port = ports[target.port]

          // persist can be a boolean, or it becomes an array of indexes to persist.
          if (index) {
            if (!Array.isArray(_port.persist)) {
              _port.persist = []
            }
            ;(_port.persist as (number | string)[]).push(index)
          } else {
            _port.persist = true
          }
        } else {
          throw Error(`Could not find input port ${target.port}`)
        }
      }
    }

    public sendPortOutput(port: string, output: Packet): undefined {
      if (port === 'error' && output.read(this) === null) {
        // do nothing
        return undefined
      }

      if (/error/.test(port)) {
        if (!this.portExists('output', port)) {
          // throw Error(output.read(output.owner))
          this.emit('error', {
            msg: output.read(output.owner),
            node: this,
          } as any)
          return undefined
        }

        const errorPort = this.getPort('output', port)

        if (!errorPort.hasConnections()) {
          this.emit('error', {
            msg: output.read(output.owner),
            node: this,
          } as any)

          return undefined
        }
      }

      if (!Packet.isPacket(output)) {
        console.error(output)
        throw Error(
          `${this.identifier}: Output for port '${port}' must be a Packet`
        )
      }

      const outputPorts = (this.ports as any).output

      if (outputPorts && outputPorts.hasOwnProperty(port)) {
        debug('%s:sendPortOutput port `%s`', this.identifier, port)

        this.event(NodeEvents.OUTPUT, {
          node: this,
          out: output,
          port,
        })

        const _port = outputPorts[port]

        _port.write(output)

        return undefined
      }

      throw Error(`${this.identifier}: no such output port ${port}`)
    }
    /**
     *
     * Wires a source port to one of our ports
     *
     * target is the target object of the connection.
     * which consist of a source and target object.
     *
     * So in this link the target is _our_ port.
     *
     * If a connection is made to the virtual `:start` port
     * it will be created automatically if it does not exist already.
     *
     * The port will be set to the open state and the connection
     * will be registered.
     *
     * A port can have multiple connections.
     *
     * TODO: the idea was to also keep track of
     *       what sources are connected.
     *
     * @private
     */
    public _initStartPort(): void {
      // add it to known ports
      if (!this.portExists('input', ':start')) {
        debug('%s:%s initialized', this.identifier, ':start')

        this.addPort('input', ':start', {
          name: ':start',
          required: false,
          type: 'any',
        })
      }
    }

    public addPort(
      type: PortGroup,
      port: string,
      portDefinition: PortDefinition
    ): InputPort | OutputPort {
      if (this.portExists(type, port)) {
        throw Error(`${this.identifier}: Port ${port} already exists`)
      }

      // TODO: this generic, however options does not exists anymore, it's settings
      /*
      _setup () {
        for (const port in this.ports.input) {
          if (this.ports.input.hasOwnProperty(port)) {
            if (this.ports.input[port].options) {
              for (const opt in this.ports.input[port].options) {
                if (this.ports.input[port].options.hasOwnProperty(opt)) {
                  this.setPortOption(
                    'input',
                    port,
                    opt,
                    this.ports.input[port].options[opt])
                }
              }
            }
          }
        }
      }
      */
      const _portDefinition: PortDefinition = {...portDefinition}

      // If there is a port function defined for this port it means it's async.
      // ASYNC PORT SETUP
      if (this.nodebox && this.nodebox.on.input.hasOwnProperty(port)) {
        _portDefinition.fn = this._createPortBox(
          this.nodebox.on.input[port].toString(),
          ('__' + port + '__').toUpperCase()
        )

        this.async = true
        ;(portDefinition as AsyncPort).async = true
      } else if ((_portDefinition as AsyncPort).fn) {
        // pre-compiled
        _portDefinition.fn = this._createPortBox(
          _portDefinition.fn,
          ('__' + port + '__').toUpperCase()
        )

        this.async = true
        ;(_portDefinition as AsyncPort).async = true
      }

      // was Port.create()
      const _port = portFactory(type, {
        ..._portDefinition,
        name: port,
      })

      if (type === 'input' && (_port as any).setState) {
        ;(_port as any).setState(this.state)
      }

      if (portDefinition.options) {
        forOf((opt: string, val: any) => {
          this.setPortOption('input', port, opt, val)
        }, portDefinition.options)
      }

      _port.setParent(this as any)

      _port.on(PortEvents.FILL, this.onPortFill.bind(this))
      ;(this.ports as any)[type][port] = _port

      return _port
    }

    public removePort(type: PortGroup, port: string): boolean {
      if (this.portExists(type, port)) {
        const _port = this.getPort(type, port)
        // Add shutdown task?
        // io handler would also need to disconnect etc.
        _port.destroy()
        // this.clearContextProperty(port)
        this._deletePort(type, port)

        return true
      }

      throw Error(`${this.identifier}: Port '${port}' does not exist`)
    }

    // TODO: name within the port object does not seem to be updated.
    public renamePort(type: PortGroup, from: string, to: string): boolean {
      if (this.portExists(type, from)) {
        if (this.portExists(type, to)) {
          throw Error(
            `Refusing to rename ${type} port ${from} to ${to}, port with that name already exists`
          )
        }

        const port: any = (this.ports as any)[type]

        port[to] = port[from]

        this._deletePort(type, from)

        return true
      }

      throw Error(`${this.identifier}: Port '${from}' does not exist`)
    }

    public getPort(type: PortGroup, name: string): InputPort | OutputPort {
      if (type === 'input' && name === ':start') {
        this._initStartPort()
      }
      if (this.portExists(type, name)) {
        const ports = (this.ports as any)[type]

        return (ports as any)[name]
      } else {
        throw new Error(`${this.identifier}: Port '${name}' does not exist`)
      }
    }

    public getInputPort(name: string): InputPort {
      return this.getPort('input', name) as InputPort
    }

    public getOutputPort(name: string): OutputPort {
      return this.getPort('output', name) as OutputPort
    }

    public getOutputPorts(): OutputPort[] {
      return this.outPorts.map((name) => this.getOutputPort(name))
    }

    public getInputPorts(): InputPort[] {
      return this.inPorts.map((name) => this.getInputPort(name))
    }

    public getPortOption(type: PortGroup, name: string, opt: string): any {
      return this.getPort(type, name).getOption(opt)
    }

    /*
    portExists (type, port) {
      return (this.ports[type] && this.ports[type].hasOwnProperty(port)) ||
      (type === 'output' && Ports.events.indexOf(port) >= 0)
    }
    */
    public portExists(type: PortGroup, port: string): boolean {
      const ports = (this.ports as any)[type]

      return Boolean(ports && ports.hasOwnProperty(port))
    }

    // Array format is used to get nested property type
    public getPortType(kind: PortGroup, port: string | string[]): string {
      let type

      const ports = (this.ports as any)[kind]

      if (Array.isArray(port)) {
        let obj: any = ports

        if (obj) {
          for (let i = 0; i < port.length; i++) {
            if (i === 0) {
              if (obj[port[i]]) {
                obj = obj[port[i]]
              }
            } else {
              if (obj.properties) {
                obj = obj.properties[port[i]]
              } else {
                throw Error(`${kind} path ${port} does not exists`)
              }
            }
          }

          type = obj.type
        } else {
          throw Error(`No ${kind} ports found`)
        }
      } else {
        if (this.portExists(kind, port)) {
          type = ports[port].type
        } else {
          throw new Error(`${this.identifier}: Port '${port}' does not exist`)
        }
      }

      if (type) {
        return type
      }

      throw Error(
        `${this.identifier}: Unable to determine type for port ${port}`
      )
    }

    /**
     *
     * Sets an input port option.
     *
     * The node schema for instance can specify whether a port is persistent.
     *
     * At the moment a connection can override these values.
     * It's a way of saying I give you this once so take care of it.
     *
     * @param {string} type
     * @param {string} name
     * @param {string} opt
     * @param {any} value
     * @returns {undefined}
     */
    public setPortOption(
      type: PortGroup,
      name: string,
      opt: string,
      value: any
    ): void {
      this.getPort(type, name).setOption(opt, value)
    }

    public setPortOptions(type: PortGroup, options: PortOptions): void {
      forOf(
        (port: string, opt: string, _val: any) =>
          this.setPortOption(type, port, opt, options[opt]),
        options
      )
    }

    /**
     *
     * Receive a ports definition and creates the Port instances
     *
     * @param {PortsDefinition} portsDefinition
     */
    public createPorts(portsDefinition: PortsDefinition): void {
      if (!portsDefinition.output) {
        portsDefinition.output = {}
      }

      if (!portsDefinition.input) {
        portsDefinition.input = {}
      }

      this.ports = {input: {}, output: {}} as NodePorts

      forOf(
        (name: string, portDefinition: PortDefinition) =>
          this.addPort('input', name, portDefinition),
        portsDefinition.input
      )

      forOf(
        (name: string, portDefinition: PortDefinition) =>
          this.addPort('output', name, portDefinition),
        portsDefinition.output
      )

      forOf((_name: string, event: xEvent) => {
        if (event.expose) {
          const port = `:${event.name}`
          this.addPort('output', port, {name: port, type: 'any'})
        }
      }, NodeEvents)
    }

    public _portsAvailable(): string {
      const inputPorts: NodeInputPorts = (this.ports as any).input
      return forOf((_name: string, port: InputPort) => {
        if (
          (!port.hasOwnProperty('required') || port.required) &&
          !port.hasOwnProperty('default')
        ) {
          return port + '*'
        }
        return port
      }, inputPorts).join(', ')
    }

    // TODO: check if this is deprecated
    public closePort(_name: string): void {}

    private _deletePort(type: PortGroup, port: string) {
      const ports = (this.ports as any)[type]

      delete ports[port]
    }

    /** @property {Array} inPorts */
    get inPorts(): string[] {
      const inputPorts = (this.ports as any).input

      return inputPorts ? Object.keys(inputPorts) : []
    }

    /** @property {Array} outPorts */
    get outPorts(): string[] {
      const outputPorts = (this.ports as any).output

      return outputPorts ? Object.keys(outputPorts) : []
    }

    get filled(): number {
      const inputPorts = (this.ports as any).input

      if (inputPorts) {
        return forOf(
          (_name: string, port: InputPort) => port.isFilled() || undefined,
          inputPorts
        ).length
      }

      return 0
    }
  }
}

export namespace $Ports {
  export function create(Base: any) {
    return mixin(
      $Ports,
      $Nodebox.create,
      $Control.create,
      $Event.create,
      $Fill.create,
      $Identity.create,
      $Port.create
    )(Base)
  }
}
