import {$Event} from '../common'
import {NodeEvents} from '../events/node'
import {Constructor} from '../types'
import {mixin} from '../util'
import {$Nodebox} from './nodebox'
import {$Ports} from './ports'

export type $ShutdownExtensionType = $Event & $Nodebox & $Ports

export interface $Shutdown {
  shutdown: (callback?: () => void) => void
  hasShutdown: () => boolean
}

export function $Shutdown<T extends Constructor<$ShutdownExtensionType>>(
  Base: T
): Constructor<$Shutdown> & T {
  return class Shutdown$ extends Base implements $Shutdown {
    /**
     *
     * Runs the shutdown method of the blackbox
     *
     * An asynchronous node can define a shutdown function:
     *
     *   on.shutdown = function() {
     *
     *     // do shutdown stuff
     *
     *   }
     *
     * When a network shuts down, this function will be called.
     * To make sure all nodes shutdown gracefully.
     *
     * e.g. A node starting a http server can use this
     *      method to shutdown the server.
     *
     * @returns {undefined}
     * @public
     */
    public shutdown() {
      if (this.nodebox.on && this.nodebox.on.shutdown) {
        // TODO: nodes now do nothing with the callback, they should..
        // otherwise we will hang
        this.nodebox.on.shutdown()

        // TODO: send the nodebox, or just the node export?
        this.event(NodeEvents.SHUTDOWN, this.nodebox)
      }
    }

    public hasShutdown(): boolean {
      // shutdown is not picked up.
      // console.log(this.nodebox.fn.toString())
      return this.nodebox.on && this.nodebox.on.shutdown
    }
  }
}

export namespace $Shutdown {
  export function create(Base: any) {
    return mixin($Shutdown, $Event.create, $Nodebox.create, $Ports.create)(Base)
  }
}
