import {$Event} from '../common'
import {NodeEvents} from '../events'
import {Packet} from '../packet'
import {Constructor} from '../types'
import {mixin} from '../util'
import {$Output} from './output'
import {$Ports} from './ports'

export type $CallbackWrapperExtensionType = $Event & $Ports & $Output

export interface $CallbackWrapper {
  _callbackWrapper: () => void
}

export function $CallbackWrapper<
  T extends Constructor<$CallbackWrapperExtensionType>
>(Base: T): Constructor<$CallbackWrapper> & T {
  return class CallbackWrapper$ extends Base implements $CallbackWrapper {
    /**
     *
     * Generic Callback wrapper
     *
     * Will collect the arguments and pass them on to the next node
     *
     * So technically the next node is the callback.
     *
     * Parameters are defined on the output as ports.
     *
     * Each callback argument must be defined as output port
     * in the callee's schema
     *
     * e.g.
     *
     *  node style callback:
     *
     *  ports.output: { err: ..., result: ... }
     *
     *  connect style callback:
     *
     *  ports.output: { req: ..., res: ..., next: ... }
     *
     * The order of appearance of arguments must match those of the ports within
     * the json schema.
     *
     * TODO: Within the schema you must define the correct type otherwise output
     * will be refused
     */
    public _callbackWrapper(...args: any[]): void {
      const obj: {[index: string]: any} = {}
      const ports = this.outPorts

      for (let i = 0; i < args.length; i++) {
        if (!ports[i]) {
          const type =
            typeof args[i] === 'object'
              ? args[i].constructor.name
              : typeof args[i]

          this.event(NodeEvents.ERROR, {
            msg: Error(`Unexpected extra port of type ${type}`),
          })
        } else {
          obj[ports[i]] = Packet.create(args[i])
        }
      }

      this._output(obj)
    }
  }
}

export namespace $CallbackWrapper {
  export function create(Base: any) {
    return mixin(
      $CallbackWrapper,
      $Event.create,
      $Ports.create,
      $Output.create
    )(Base)
  }
}
