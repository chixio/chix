import {Context} from '@chix/common'
import {forOf} from '@fbpx/lib'
import d from 'debug'
import {$Event, $Identity} from '../common'
import {NodeEvents} from '../events'
import {Packet} from '../packet'
import {InputPort} from '../port/'
import {Constructor} from '../types'
import {mixin} from '../util'
import {$Ports} from './ports'

const debug = d.debug('chix:node')

export type $ContextExtensionType = $Event & $Identity & $Ports

export interface $Context {
  clearContextProperty: (port: string) => void
  context: Context
  $setContextProperty: (port: string, data: any, trigger: boolean) => void
  addContext: (context: Context) => void
  setContextProperty: (port: string, data: any, trigger?: boolean) => void
}

export function $Context<T extends Constructor<$ContextExtensionType>>(
  Base: T
): Constructor<$Context> & T {
  return class Context$ extends Base implements $Context {
    public setContext = this.setContextProperty

    public $setContextProperty(
      port: string,
      data: any,
      trigger?: boolean
    ): void {
      debug('%s:%s set context', this.identifier, port)
      if (data === undefined) {
        throw Error('Refused to $setContextProperty to undefined')
      }

      let packet

      if (Packet.isPacket(data)) {
        packet = data
      } else {
        packet = Packet.create(data, this.getPortType('input', port)).setOwner(
          this
        )
      }

      this.getInputPort(port).setContext(packet, trigger)
    }

    get context(): Context {
      const context: {[key: string]: any} = {}

      forOf((name: string, port: InputPort) => {
        context[name] = port.context
      }, this.ports.input)

      return context
    }

    public clearContextProperty(port: string): void {
      debug('%s:%s clear context', this.identifier, port)

      const inputPort = this.getInputPort(port)

      const context = inputPort.context?.read(inputPort)

      inputPort.clearContext()

      this.event(NodeEvents.CONTEXT_CLEAR, {
        node: this,
        port,
        context,
      })
    }

    /**
     *
     * Add context.
     *
     * Must be set in one go.
     *
     * @param {Object} context
     * @public
     */
    public addContext(context: Context): void {
      forOf((port: string, val: any) => {
        this.setContextProperty(port, val)
      }, context)
    }

    /**
     *
     * Set context to a port.
     *
     * Can be changed during runtime, but will never trigger
     * a start.
     *
     * Adding the whole context in one go could trigger a start.
     *
     * @param {String} port
     * @param {Mixed} data
     * @param {Boolean} trigger whether to trigger a port fill event
     * @emits BaseNode#contextUpdate
     * @private
     */
    public setContextProperty(
      port: string,
      data: any,
      trigger?: boolean
    ): void {
      if (port === ':start') {
        this._initStartPort()
      }

      if (this.portExists('input', port)) {
        this.$setContextProperty(port, data, trigger)

        this.event(NodeEvents.CONTEXT_UPDATE, {
          data,
          node: this,
          port,
        })
      } else {
        throw Error(`No such input port ${port} for ${this.identifier}`)
      }
    }
  }
}

export namespace $Context {
  export function create(Base: any) {
    return mixin($Context, $Event.create, $Identity.create, $Ports.create)(Base)
  }
}
