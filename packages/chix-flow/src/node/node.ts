import {NodeDefinition} from '@chix/common'
import {DependencyLoader} from '@chix/loader'
import {Constructor, MixinParams} from '../types'
import {validate} from '../validate'
import {BaseNode} from './BaseNode'
import {NodeMixin, NodeMixinType} from './mixin'

// Running within vm is also possible and api should stay
// compatible with that, but disable for now.
// vm = require('vm'),

/**
 * Error Event.
 *
 * @event Node#error
 * @type {object}
 * @property {object} node - An export of this node
 * @property {string} msg - The error message
 */

/**
 * Executed Event.
 *
 * @event Node#executed
 * @type {object}
 * @property {object} node - An export of this node
 */

/**
 * Context Update event.
 *
 * @event Node#contextUpdate
 */

/**
 * Output Event.
 *
 * Fired multiple times on output
 *
 * Once for every output port.
 *
 * @event Node#output
 * @type {object}
 * @property {object} node - An export of this node
 * @property {string} port - The output port
 * @property {string} out - A (reference) to the output
 */

export type NodeType<T> = NodeMixinType<T>

export function NodeMix<T>(Base: T): Constructor<NodeType<T>> & T {
  return NodeMixin(Base)
}

export interface CreateNodeOptions {
  /**
   * The nodeId is mandatory
   */
  nodeId: string
  /**
   * Possible alternative identifier.
   *
   * Default value is ${ns}:${name}
   */
  identifier?: string
  /**
   * Node definition may be omitted if a node is constructed from scratch
   */
  nodeDefinition?: NodeDefinition
  /**
   * Not strictly needed if the nodeDefinition carries no dependencies
   */
  dependencyLoader?: DependencyLoader
}

// tslint:disable-next-line class-name
export class xNode extends NodeMix(BaseNode) {
  /**
   * Node has to be created using the create method.
   */
  public static async create({
    nodeId,
    identifier,
    nodeDefinition,
    dependencyLoader,
  }: CreateNodeOptions) {
    if (nodeDefinition != null) {
      validate.nodeDefinition(nodeDefinition)
    }

    const node = new this(nodeId, nodeDefinition)

    if (identifier) {
      node.identifier = identifier
    }

    if (dependencyLoader) {
      node.setDependencyLoader(dependencyLoader)
    }

    await node.init()

    return node
  }

  public type = 'node'
  public status = 'init'

  // constructor (id: string, node: NodeDefinition, identifier?: string, _CHI?: any) {
  protected constructor(id: string, node?: NodeDefinition, _CHI?: any) {
    super({
      id,
      node: {type: 'node', ...node},
    } as MixinParams)

    this.active = false
  }
}
