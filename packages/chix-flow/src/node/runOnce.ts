import d from 'debug'
import {$Event, $ExportCommon, $Identity} from '../common'
import {NodeEvents} from '../events'
import {Constructor} from '../types'
import {mixin} from '../util'
import {$Export} from './export'
import {$Nodebox} from './nodebox'
import {$Output} from './output'
import {$Ports} from './ports'

const debug = d.debug('chix:node')

export type $RunOnceExtensionType = $Event &
  $ExportCommon &
  $Identity &
  $Nodebox &
  $Output &
  $Ports

export interface $RunOnce {
  _runOnce: () => void
}

export function $RunOnce<T extends Constructor<$RunOnceExtensionType>>(
  Base: T
): Constructor<$RunOnce> & T {
  return class RunOnce$ extends Base implements $RunOnce {
    public active: boolean = false
    public state: any
    /**
     *
     * Runs the node
     *
     * @emits Node#nodeTimeout
     * @emits Node#start
     * @emits Node#executed
     * @private
     */
    public _runOnce(): void {
      const t = setTimeout(() => {
        debug('%s: node timeout', this.identifier)

        /**
         * Timeout Event.
         *
         * @event Node#nodeTimeout
         * @type {object}
         * @property {object} node - An export of this node
         */
        this.event(NodeEvents.TIMEOUT, {
          node: this.export(),
        })
      }, this.nodeTimeout)

      /**
       * Start Event.
       *
       * @event Node#start
       * @type {object}
       * @property {object} node - An export of this node
       */
      this.event(NodeEvents.START, {
        node: this.export(),
      })

      // this.nodebox.runInNewContext(this.sandbox)

      // this.setStatus('running')

      this.nodebox.run()

      this.state = this.nodebox.state

      debug('%s:%s executed', this.identifier, this.nodebox.fn.name)

      clearTimeout(t)

      this.active = false

      this._output(this.nodebox.output)
    }
  }
}

export namespace $RunOnce {
  export function create(Base: any) {
    return mixin(
      $RunOnce,
      $Event.create,
      $Export.create,
      $Identity.create,
      $Nodebox.create,
      $Output.create,
      $Ports.create
    )(Base)
  }
}
