import {DependencyLoader} from '@chix/loader'
import {Constructor} from '../types'
import {mixin} from '../util'

export interface $Dependency {
  dependencyLoader: DependencyLoader
  setDependencyLoader(loader: DependencyLoader): void
  require<I = any>(packagePath: string): I
}

export function $Dependency<T extends Constructor<$Dependency>>(
  Base: T
): Constructor<$Dependency> & T {
  return class Dependency$ extends Base implements $Dependency {
    public dependencyLoader: DependencyLoader

    public setDependencyLoader(loader: DependencyLoader) {
      this.dependencyLoader = loader
    }

    public require<I = any>(packagePath: string): I {
      return this.dependencyLoader.require(packagePath) as I
    }
  }
}

export namespace $Dependency {
  export function create(Base: any) {
    return mixin($Dependency)(Base)
  }
}
