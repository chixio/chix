import {Ports as PortsDefinition} from '@chix/common'
import {$Status} from '../common'
import {Constructor} from '../types'
import {mixin} from '../util'
import {$Dependency} from './dependency'
import {$Nodebox} from './nodebox'
import {$Ports} from './ports'
import {$Start} from './start'

export type $InitExtensionType = $Nodebox &
  $Status &
  $Ports &
  $Start &
  $Dependency

export interface $Init {
  init(): Promise<void>
}

export function $Init<T extends Constructor<$InitExtensionType>>(
  Base: T
): Constructor<$Init> & T {
  return class Init$ extends Base implements $Init {
    public async init(): Promise<void> {
      if (this.nodeDefinition.dependencies) {
        if (this.dependencyLoader) {
          await this.dependencyLoader.load(this.nodeDefinition.dependencies)
        } else {
          throw Error(
            `NodeDefinition ${this.nodeDefinition.ns}:${this.nodeDefinition.name}  contains dependency, but a dependencyLoader has not been provided`
          )
        }
      }

      await this.setupNodeBox()

      this.createPorts(this.nodeDefinition.ports as PortsDefinition)
      this.setStatus('created')
      this.start()
    }
  }
}

export namespace $Init {
  export function create(Base: any) {
    return mixin($Init, $Nodebox.create)(Base)
  }
}
