import * as instanceOf from 'instance-of'
import {isPlainObject} from '../util'

export function validateData(type: string, data: any): boolean {
  if (Array.isArray(type)) {
    for (const _type of type) {
      if (validateData(_type, data)) {
        return true
      }
    }
    return false
  }

  switch (type) {
    case 'string':
      return typeof data === 'string'

    case 'enum':
      console.warn('TODO: validate enum correctly')
      return typeof data === 'string'

    case 'array':
      return Object.prototype.toString.call(data) === '[object Array]'

    case 'integer':
    case 'number':
      return Object.prototype.toString.call(data) === '[object Number]'

    case 'null':
      type = type.charAt(0).toUpperCase() + type.slice(1)
      return Object.prototype.toString.call(data) === '[object ' + type + ']'

    case 'boolean':
    case 'bool':
      return data === true || data === false || data === 0 || data === 1

    case 'any':
      return true
    case 'object':
      if (isPlainObject(data)) {
        return true
      }
      return instanceOf(data, type)
    case 'function':
      return true

    default:
      return instanceOf(data, type)
  }
}
