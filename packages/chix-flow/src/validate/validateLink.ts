import {Link as LinkDefinition, LinkSchema} from '@chix/common'
import * as jsongate from 'json-gate'

const linkSchema = jsongate.createSchema(LinkSchema)

export function validateLink(link: LinkDefinition) {
  linkSchema.validate(link)
}
