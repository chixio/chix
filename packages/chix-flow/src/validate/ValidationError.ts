export class ValidationError {
  public id: string | undefined
  public message: string
  public obj: any
  public name: string

  constructor(message: string, id?: string, obj?: any) {
    this.message = message
    this.id = id
    this.obj = obj
    this.name = 'ValidationError'
  }
}
