import {Flow as FlowDefinition, FlowSchema} from '@chix/common'
import * as jsongate from 'json-gate'

const mapSchema = jsongate.createSchema(FlowSchema)

export function validateFlow(flowDefinition: FlowDefinition) {
  mapSchema.validate(flowDefinition)
  // _checkIds(flow, nodeDefinitions)
}
