import {_checkIds} from './_checkIds'
import {validateData} from './validateData'
import {validateFlow} from './validateFlow'
import {validateLink} from './validateLink'
import {validateNodeDefinition} from './validateNodeDefinition'

export {validateData, validateFlow, validateLink, validateNodeDefinition}

export const validate = {
  data: validateData,
  flow: validateFlow,
  link: validateLink,
  nodeDefinition: validateNodeDefinition,
  nodeDefinitions: _checkIds,
}
