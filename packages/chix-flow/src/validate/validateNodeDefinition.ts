import {NodeDefinition, NodeSchema} from '@chix/common'
import * as jsongate from 'json-gate'

const nodeSchema = jsongate.createSchema(NodeSchema)

/**
 *
 * Validates a nodeDefinition
 *
 */
export function validateNodeDefinition(nodeDefinition: NodeDefinition) {
  nodeSchema.validate(nodeDefinition)
  // _checkIds(flow, nodeDefinitions)
  // make sure the id's are correct
}
