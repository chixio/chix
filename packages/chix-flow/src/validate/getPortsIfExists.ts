import {
  InputPortsDefinition,
  Node,
  NodeDefinition,
  NodeDefinitions,
  OutputPortsDefinition,
} from '@chix/common'
import {PortGroup} from '../node'
import {ValidationError} from './ValidationError'

export function getPortsIfExists(
  type: PortGroup,
  node: Node,
  nodeDefinitions: NodeDefinitions
): InputPortsDefinition | OutputPortsDefinition {
  const nodeDefinition = nodeDefinitions[node.ns][node.name] as NodeDefinition

  if (!nodeDefinition) {
    throw new ValidationError(
      `NodeDefinitions ${node.ns}:${node.name} does not exist.`
    )
  }

  if (!nodeDefinition.ports || Object.keys(nodeDefinition.ports).length === 0) {
    throw new ValidationError(
      `NodeDefinitions ${node.ns}:${node.name} has no ports.`
    )
  }

  const ports = nodeDefinition.ports[type]

  if (!ports || Object.keys(ports).length === 0) {
    throw new ValidationError(
      `NodeDefinitions ${node.ns}:${node.name} has no ${type} ports.`
    )
  }

  return ports
}
