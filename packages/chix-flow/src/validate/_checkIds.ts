import {Flow as FlowDefinition, Node, NodeDefinitions} from '@chix/common'
import {getPortsIfExists} from './getPortsIfExists'
import {ValidationError} from './ValidationError'

/**
 *
 * Check if we are not adding a (rotten) flow. Where there are id's which
 * overlap other ids in other flows.
 *
 * This shouldn't happen but just perform this check always.
 *
 */
export function _checkIds(
  flowDefinition: FlowDefinition,
  nodeDefinitions: NodeDefinitions
): boolean {
  const knownIds: string[] = []
  const nodes: {[nodeId: string]: Node} = {}

  if (flowDefinition.nodes.length > 0 && !nodeDefinitions) {
    throw new Error('Cannot validate without nodeDefinitions')
  }

  // we will not add the flow, we will show a warning and stop adding the
  // flow.
  for (const node of flowDefinition.nodes) {
    // nodeDefinition should be loaded
    if (!node.ns) {
      throw new ValidationError(
        `Cannot find nodeDefinition without name: ${node.name}`
      )
    }
    if (!nodeDefinitions[node.ns]) {
      throw new ValidationError(
        `Cannot find nodeDefinition with namespace: ${node.ns}`
      )
    }
    if (!nodeDefinitions[node.ns][node.name]) {
      throw new ValidationError(
        `Cannot find nodeDefinition for ${node.ns}:${node.name}`
      )
    }

    knownIds.push(node.id)
    nodes[node.id] = node
    // _checkPortDefinitions(nodeDefinitions[node.ns][node.name])
  }

  for (const link of flowDefinition.links) {
    // links should not point to non-existing nodes.
    if (!link.source) {
      throw Error('source not defined')
    }
    if (!link.target) {
      throw Error('target not defined')
    }
    if (knownIds.indexOf(link.source.id) === -1) {
      throw new ValidationError('Source node does not exist ' + link.source.id)
    }
    if (knownIds.indexOf(link.target.id) === -1) {
      throw new ValidationError('Target node does not exist ' + link.target.id)
    }

    const source = nodes[link.source.id]
    const target = nodes[link.target.id]

    const outputPorts = getPortsIfExists('output', source, nodeDefinitions)

    // allow :start
    if (link.source.port[0] !== ':' && !outputPorts[link.source.port]) {
      throw new ValidationError(
        [
          'Process',
          link.source.id,
          'has no output port named',
          link.source.port,
          '\n\n\tOutput ports available:',
          '\n\n\t',
          Object.keys(outputPorts).join(', '),
        ].join(' ')
      )
    }

    const inputPorts = getPortsIfExists('input', target, nodeDefinitions)

    if (link.target.port[0] !== ':' && !inputPorts[link.target.port]) {
      throw new ValidationError(
        [
          'Process',
          link.target.id,
          'has no input port named',
          link.target.port,
          '\n\n\tInput ports available:',
          '\n\n\t',
          Object.keys(inputPorts).join(', '),
        ].join(' ')
      )
    }
  }

  return true
}
