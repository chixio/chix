import {
  Flow as FlowDefinition,
  NodeDefinition,
  RemoteProvider,
} from '@chix/common'
import d from 'debug'
import {$Identity, $Status} from '../common'
import {Link} from '../link'
import {Constructor} from '../types'
import {mixin} from '../util'
import {validate} from '../validate'

import {$Dependency} from './dependency'
import {$Link} from './link'
import {$Loader} from './loader'
import {$Node} from './node'
import {$Process} from './process'

const debug = d.debug('chix:actor')

export type FlowMask = string[]

export type $MapExtensionType<T> = $Identity &
  $Link &
  $Loader &
  $Dependency &
  $Node<T> &
  $Process<T> &
  $Status

export interface $Map<T> {
  addMap: (flowDefinition: FlowDefinition) => Promise<T>
}

export function $Map<T extends Constructor<$MapExtensionType<T>>>(
  Base: T
): Constructor<$Map<T>> & T {
  return class Map$ extends Base implements $Map<T> {
    public view: FlowMask = []

    /**
     * Reads a flowDefinition from JSON
     *
     * Will validate the JSON first then creates the nodes and it's links
     *
     * If the flowDefinition itself contains nodeDefinitions those will be added to
     * the loader.
     *
     * If the flowDefinition itself does not have a provider property set it will
     * be set to '@', which means internal lookup.
     *
     * A default view will be setup unmasking all nodes.
     *
     * If this Actor is the main actor it's status will be set to `created`
     *
     * If this Actor was not yet registered with the Process Manager it will be registered.
     *
     * @param {FlowDefinition} flowDefinition
     * @public
     *
     */
    public async addMap(flowDefinition: FlowDefinition): Promise<T> {
      debug('%s: addMap()', this.identifier)
      try {
        validate.flow(flowDefinition)
      } catch (error) {
        if (flowDefinition.title) {
          throw Error(
            `Flow \`${flowDefinition.title}\`: ${(error as Error).message}`
          )
        } else {
          console.error(flowDefinition)
          throw Error(
            `Flow ${flowDefinition.ns}:${flowDefinition.name}: ${
              (error as Error).message
            }`
          )
        }
      }

      if (flowDefinition.id) {
        // xFlow contains it, direct actors don't per se
        this.id = flowDefinition.id
      }

      // allow a flowDefinition to carry it's own definitions
      if (flowDefinition.nodeDefinitions) {
        this.loader.addNodeDefinitions('@', flowDefinition.nodeDefinitions)
      }

      // register ourselves first

      // THIS IS TOO EARLY STUFF IS REACTING ON ADD_PROCESS AND THEN STARTS.
      // BUT THE NODE IS NOT BUILD YET.
      // SO IF ANYTHING NEED this.pid below, fix that
      /*
      if (!this.pid) {
        // re-run
        this.processManager.register(this as any)
      }
      */

      // add nodes and links one by one so there is more control
      await Promise.all(
        flowDefinition.nodes.map(async (node) => {
          if (!node.id) {
            throw new Error(`Node lacks an id: ${node.ns}:${node.name}`)
          }

          // give the node a default provider.
          if (!node.provider) {
            if (
              flowDefinition.providers &&
              flowDefinition.providers.hasOwnProperty('@')
            ) {
              node.provider = (
                flowDefinition.providers['@'] as RemoteProvider
              ).url
            }
          }

          const nodeDefinition = (await this.loader.getNodeDefinition(
            node,
            flowDefinition
          )) as NodeDefinition

          if (!nodeDefinition) {
            throw new Error(
              `Failed to get node definition for ${node.ns}:${node.name}`
            )
          }

          debug('%s: Creating node %s:%s', this.identifier, node.ns, node.name)
          return this.createNode(node, nodeDefinition)
        })
      )

      let registrator
      if (!this.pid) {
        registrator = this.processManager.createRegistrator()
        this.pid = registrator.pid
      }

      if (flowDefinition.hasOwnProperty('links')) {
        for (const link of flowDefinition.links) {
          this.addLink(Link.create(link))
        }
      }

      for (const node of flowDefinition.nodes) {
        this.view.push(node.id)
      }

      // fix later
      // if (this.identifier === 'flow:main') {
      this.setStatus('created')
      // }

      if (registrator) {
        // re-run
        registrator.register(this as any)
      }

      return this as any
    }
  }
}

export namespace $Map {
  export function create(Base: any) {
    return mixin(
      $Map,
      $Identity.create,
      $Link.create,
      $Loader.create,
      $Node.create,
      $Process.create,
      $Status.create
    )(Base)
  }
}
