import {
  Connector as ConnectorDefinition,
  IIP as IIPDefinition,
} from '@chix/common'
import {forOf} from '@fbpx/lib'
import {$Event, $Identity} from '../common'
import {Connector} from '../connector'
import {FlowEvents} from '../events'
import {IIP} from '../IIP'
import {Link} from '../link'
import {Packet} from '../packet'
import {Constructor} from '../types'
import {mixin} from '../util'
import {$Control} from './control'
import {$Link} from './link'
import {$Node} from './node'
import {$Port} from './port'
import {$Process} from './process'

export type $IIPExtensionType<T> = $Control &
  $Event &
  $Identity &
  $Link &
  $Node<T> &
  $Port &
  $Process<T>

export interface $IIP {
  __sendIIP: (xLink: Link) => Link
  iips: Map<string, IIP>
  clearIIP: (link: Link) => void
  clearIIPs: (target?: {id: string; port: string}) => void
  connectIIP: (link: Link) => Link
  createIIP: (iip: IIPDefinition) => Link
  getIIPs: () => IIP[]
  sendIIP: (target: ConnectorDefinition, data: any) => Link
  sendIIPs: (iips: IIPDefinition[]) => Link[]
  sendIIPsOutstanding: () => void
}

export function $IIP<T extends Constructor<$IIPExtensionType<T>>>(
  Base: T
): Constructor<$IIP> & T {
  return class IIP$ extends Base implements $IIP {
    /**
     *
     * Create an IIP
     *
     * Optionally with `options` for the port:
     *
     * e.g. { persist: true }
     *
     * Optionally with `source` information
     *
     * e.g. { index: 1 } // index for array port
     *
     * @param {IIPDefinition} iip
     * @fires Flow#addIIP
     * @returns {xLink}
     */
    public createIIP(iip: IIPDefinition): IIP {
      if (!this.id) {
        throw Error('id is not set.')
      }
      const xIIP = IIP.create({
        source: {
          id: this.id, // we are the sender
          port: ':iip',
        },
        target: iip.target,
      })

      xIIP.set('dispose', true)

      if (iip.data === undefined) {
        throw Error('IIP data is `undefined`')
      }

      xIIP.data = iip.data

      this.event(FlowEvents.ADD_IIP, xIIP.toJSON())

      return xIIP
    }

    /**
     * Connect an IIP
     *
     * @param {Object|Link} iip
     * @returns {*}
     */
    public connectIIP(iip: IIP): IIP {
      if (iip.target.constructor.name !== 'Connector') {
        const target = new Connector()

        if (!iip.target.id) {
          throw Error('target id is not set.')
        }

        if (!iip.target.port) {
          throw Error('target port is not set.')
        }

        target.plug(iip.target.id, iip.target.port)

        forOf(
          (key: string, setting: any) => target.set(key, setting),
          iip.target.setting
        )

        iip.target = target
      }

      // TODO: iip should contain their own unique id
      if (!this.id) {
        throw Error('Actor must contain an id')
      }

      return this.addLink(iip)
    }

    /**
     *
     * Send IIPs
     *
     * Creates, connects and sends an IIPs
     *
     * @param {Object} iips
     * @public
     */
    public sendIIPs(iips: IIPDefinition[]): IIP[] {
      const links: IIP[] = iips
        .map(this.createIIP.bind(this))
        .map(this.connectIIP.bind(this))
        .map(this.__sendIIP.bind(this))

      return links
    }

    /**
     * Send IIPs present in this.links
     *
     * Used by the runtime, which first adds them
     * next when the graph is started they will be used.
     *
     * @private
     */
    public sendIIPsOutstanding(): void {
      for (const iip of this.getIIPs()) {
        this.__sendIIP(iip)
      }
    }

    /*
     * Send a single IIP to a port.
     *
     * Note: If multiple IIPs have to be send use sendIIPs instead.
     *
     * Source is mainly for testing, but essentially it allows you
     * to imposter a sender as long as you send along the right
     * id and source port name.
     *
     * Source is also used to set an index[] for array ports.
     * However, if you send multiple iips for an array port
     * they should be send as a group using sendIIPs
     *
     * This is because they should be added in reverse order.
     * Otherwise the process will start too early.
     *
     * @param {Connector} target
     * @param {Object} data
     * @public
     */
    public sendIIP(target: ConnectorDefinition, data: any): IIP {
      // TODO: just create a unique id for the IIP, do not use the graph id
      if (!this.id) {
        throw Error('Actor must contain an id')
      }

      if (undefined === data) {
        throw Error('Refused to send IIP without data')
      }

      // const iipDefinition: IIPDefinition = {
      const iipDefinition: any = {
        data,
        source: {
          id: this.id as string, // we are the sender
          // pid: this.pid as string,
          port: ':iip',
        },
        target,
      }

      const iip = this.createIIP(iipDefinition)

      this.connectIIP(iip)

      this.__sendIIP(iip)

      return iip
    }

    public __sendIIP(iip: IIP): IIP {
      if (!iip.target.id) {
        throw Error('target id is not set.')
      }

      if (!iip.target.port) {
        throw Error('target port is not set.')
      }

      const targetNode = this.getNode(iip.target.id)

      if (!targetNode) {
        throw Error('Target node not found.')
      }

      const targetPortType = (targetNode as any).getPortType(
        'input',
        iip.target.port
      )

      const packet = Packet.create(
        // JSON.parse(JSON.stringify(xLink.data)),
        iip.data,
        targetPortType
      )

      iip.source.write(packet)

      // remove data bit.
      // delete xLink.data

      return iip
    }

    /**
     * clearIIP
     *
     * Unplugs, disconnects and removes an IIP
     *
     * When an IIP is connected and the data is send
     * this method removes/unregisters the IIP again.
     *
     * @param {IIP} iip
     * @fires Flow#removeIIP
     */
    public clearIIP(iip: IIP): void {
      for (const link of this.getIIPs()) {
        const oldIIP = link
        // source is always us so do not have to check it.
        // source will have to be checked when iip has it's own id.
        if (
          (oldIIP.source.port === ':iip' ||
            oldIIP.target.port === iip.target.port ||
            oldIIP.target.port === ':start') && // huge uglyness
          oldIIP.target.id === iip.target.id
        ) {
          this.unplugPort('input', oldIIP.target)

          this.ioHandler.disconnect(oldIIP)

          this.links.delete(oldIIP.id)

          // TODO: just rename this to clearIIP
          this.event(FlowEvents.REMOVE_IIP, oldIIP)
        }
      }
    }

    /**
     *
     * Clear IIPs
     *
     * If target is specified, only those iips will be cleared.
     */
    public clearIIPs(target?: {id: string; port: string}): void {
      for (const link of this.getIIPs()) {
        const iip = link

        if (
          !target ||
          (target.id === iip.target.id && target.port === iip.target.port)
        ) {
          this.clearIIP(iip)
        }
      }
    }

    public getIIPs(): IIP[] {
      return Array.from(this.links.values()).filter(
        (link) => link instanceof IIP
      ) as IIP[]
    }

    get iips(): Map<string, IIP> {
      return this.getIIPs().reduce(
        (iips, iip) => iips.set(iip.id, iip),
        new Map()
      )
    }
  }
}

export namespace $IIP {
  export function create(Base: any) {
    return mixin(
      $IIP,
      $Control.create,
      $Event.create,
      $Identity.create,
      $Node.create,
      $Port.create,
      $Process.create
    )(Base)
  }
}
