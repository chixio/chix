import d from 'debug'
import {$Event, $Identity, $Status} from '../common/'
import {Connector} from '../connector'
import {FlowEvents} from '../events'
import {IoHandler} from '../io/handler'
import {xNode} from '../node'
import {Constructor} from '../types'
import {mixin} from '../util'
import {$IIP} from './iip'
import {$Link} from './link'
import {$Node} from './node'
import {$Status as Status} from './status'

const debug = d.debug('chix:actor')

export type $ControlExtensionType<T> = $Event &
  $Identity &
  $IIP &
  $Link &
  $Node<T> &
  $Status

export interface $Control {
  _hold: boolean
  active: boolean
  ioHandler: IoHandler
  ready: boolean
  runCount: number

  clear: () => Promise<void>
  complete: () => void
  destroy: () => void
  getStatus: () => string
  hold: (id?: string) => void
  isHalted: () => boolean
  pause: () => void
  push: () => void
  run: () => void
  reset: () => void
  resume: () => void
  release: (id?: string) => void
  setIoHandler: (ioHandler: IoHandler) => void
  shutdown: (callback?: () => void) => void
  start: (push?: boolean) => void
  stop: (cb?: () => void) => void
}

export function $Control<T extends Constructor<$ControlExtensionType<T>>>(
  Base: T
): Constructor<$Control> & T {
  return class Control$ extends Base implements $Control {
    public _hold: boolean = false
    public active: boolean = false
    public ready: boolean = true
    public runCount = 0

    public ioHandler: IoHandler = new IoHandler()
    /**
     *
     * Add IO Handler.
     *
     * The IO Handler handles all the input and output.
     *
     * @param {IoHandler} ioHandler
     * @public
     *
     */
    public setIoHandler(ioHandler: IoHandler) {
      this.ioHandler = ioHandler

      return this
    }

    /**
     *
     * Holds a Node
     *
     * @param {String} id
     * @public
     */
    public hold(id?: string) {
      if (id) {
        this.getNode(id).hold()

        this.setStatus(Status.HOLD)
      } else {
        this._hold = true
        this.stop()
      }

      return this
    }

    public isHalted(id?: string): boolean {
      if (id) {
        this.getNode(id).isHalted()
      }

      return this._hold
    }

    public complete(): void {
      this.ready = false
      this.active = false
    }

    /**
     *
     * Pushes the Actor
     *
     * Will send :start to all nodes without input
     * and all nodes which have all their input ports
     * filled by context already.
     *
     * @public
     */
    public push() {
      /*
    if (false && this.status !== 'created') {
      throw Error(
        `To push actor must be in created state, current status ${this.status}`
      )
    }
    */
      this.setStatus(Status.RUNNING)
      debug('%s: push()', this.identifier)

      for (const node of this.nodes.values()) {
        if (node.isStartable() && node.id) {
          const iip = new Connector()

          iip.plug(node.id, ':start')

          this.sendIIP(iip as any, '')
        }
      }

      this.setStatus('running')

      return this
    }

    /**
     *
     * Starts the actor
     *
     * @param {Boolean} push
     * @fires Flow#start
     * @public
     */
    public start(push?: boolean) {
      // this.status = Status.RUNNING
      if (['created', 'running', 'stopped'].indexOf(this.status) >= 0) {
        this.setStatus('created')
        debug('%s: start', this.identifier)

        // this.sendIIPsOutstanding()
        // this.clearIIPs()

        for (const node of this.nodes.values()) {
          // skip normal nodes now, because they auto start
          // flows, do need to start here
          if (node.type === 'flow') {
            node.start()
          } else {
            // manually evaluate node
            // if (true || push) {
            ;(node as xNode).onPortFill()
            this.setStatus(Status.RUNNING)
            // }
          }
        }

        if (push !== false) {
          // this.push()
        } else {
          this.setStatus('started')
        }

        this.event(FlowEvents.START, {
          node: this,
        })

        return this
      }

      throw Error(
        `To start actor must be in created state, current status: ${this.status}`
      )
    }

    /**
     *
     * Stops the actor
     *
     * Will set the status to `stopped`
     *
     * Any outstanding IIPs will be cleared.
     *
     * Emits the `stop` event when done.
     *
     * @param {Function} cb - Callback
     * @fires Flow#stop
     * @public
     */
    public stop() {
      this.status = Status.STOPPED

      if (this.ioHandler) {
        this.ioHandler.reset()

        // close ports opened by iips
        this.clearIIPs()

        this.event(FlowEvents.STOP, {
          node: this,
        })

        // hack, remove it all together possibly
        this.ioHandler._shutdown = false
      }
    }

    /**
     * Will pause the actor
     *
     * Will set status to `stopped`(?)
     *
     * And will hold() all nodes.
     *
     * Graphs in their turn will put all their nodes on hold()
     *
     * @returns {Control}
     */
    public pause() {
      this.status = Status.STOPPED

      for (const node of this.nodes.values()) {
        node.hold()
      }

      return this
    }

    /**
     *
     * Resumes the actor
     *
     * Will set it's own status to `running`
     *
     * All nodes which are on hold will resume again.
     *
     * @public
     */
    public resume() {
      this.status = Status.RUNNING

      for (const node of this.nodes.values()) {
        node.release()
      }

      return this
    }

    /**
     *
     * Releases a node if it was on hold
     *
     * @param {String} id - Id of node to release
     * @public
     */
    public release(id?: string): void {
      if (id) {
        return this.getNode(id).release()
      }

      this._hold = false
      this.resume()
    }

    public shutdown(): void {}

    public destroy(): void {
      for (const node of this.nodes.values()) {
        node.destroy()
      }
    }

    /**
     *
     * Resets this instance so it can be re-used
     *
     * Will call reset() on all nodes
     *
     * Graphs in their turn will reset() all their nodes
     *
     * Note: The registered loader is left untouched.
     *
     * @public
     */
    public reset() {
      for (const node of this.nodes.values()) {
        node.reset()
      }

      this.runCount = 0

      if (this.identifier === 'flow:main' && this.ioHandler) {
        this.ioHandler.reset()
      }

      return this
    }

    /**
     * Clears this graph from it's nodes
     */
    public async clear() {
      const total = this.nodes.size

      if (total > 0) {
        for (const node of this.nodes.values()) {
          if (node.id) {
            await this.removeNode(node.id)
            this.removeLinks(node.id)
          }
        }
      }
    }

    /**
     *
     * Run the current flow
     *
     * The flow starts by providing the ports with their context.
     *
     * Nodes which have all their ports filled by context will run immediately.
     *
     * Others will wait until their unfilled ports are filled by connections.
     *
     * If a port is not required and context was given and there are
     * no connections on it, it will not block the node from running.
     *
     * If a map has actions defined, run expects an action name to run.
     *
     * Combinations:
     *
     *   - run()
     *     run flow without callback
     *
     *   - run(callback)
     *     run with callback
     *
     *   - action('actionName').run()
     *     run action without callback
     *
     *   - action('actionName').run(callback)
     *     run action with callback
     *
     * The callback will receive the output of the (last) node(s)
     * Determined by which output ports are exposed.
     *
     * If we pass the exposed output, it can contain output from anywhere.
     *
     * If a callback is defined but there are no exposed output ports.
     * The callback will never fire.
     *
     * @public
     */
    public run(): void {
      throw Error('Run is deprecated.')
    }
  }
}

export namespace $Control {
  export function create(Base: any) {
    return mixin(
      $Control,
      $Event.create,
      $Identity.create,
      $IIP.create,
      $Link.create,
      $Node.create,
      $Status.create
    )(Base)
  }
}
