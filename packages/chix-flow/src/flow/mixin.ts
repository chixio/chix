import {$Event, $Identity, $Meta, $Parent, $Status} from '../common'
import {$ExportCommon} from '../common/interfaces'
import {mixin} from '../util'
import {$Action} from './action'
import {$Context} from './context'
import {$Control} from './control'
import {$Dependency} from './dependency'
import {$FlowError} from './error'
import {$Export} from './export'
import {$IIP} from './iip'
import {$IsStartable} from './isStartable'
import {$Link} from './link'
import {$Loader} from './loader'
import {$Map} from './map'
import {$MetaNode} from './meta'
import {$Node} from './node'
import {$Port} from './port'
import {$Ports} from './ports'
import {$Process} from './process'
import {$Providers} from './providers'
import {$Report} from './report'
import {$Shutdown} from './shutdown'
import {$ToJSON} from './toJSON'

export type FlowType<T> = $Action &
  $Context &
  $FlowError &
  $Event &
  $ExportCommon &
  $Dependency &
  $IsStartable &
  $Meta &
  $MetaNode &
  $Ports &
  $Providers &
  $Shutdown &
  $Status &
  $ToJSON &
  $Identity &
  $Status &
  $Loader &
  $Parent<T> &
  $Port &
  $Meta &
  $Report &
  $Process<T> &
  $Node<T> &
  $Link &
  $IIP &
  $Control &
  $Map<T>

export const FlowMixin = mixin(
  $Identity,
  $Status,
  $Parent,
  $Port,
  $Loader,
  $Dependency,
  $Meta,
  $MetaNode,
  $Report,
  $Process,
  $Node,
  $Link,
  $IIP,
  $Control,
  $Map,
  $Action,
  $Context,
  $FlowError,
  $Event,
  $Export,
  $IsStartable,
  $Meta,
  $Ports,
  $Providers,
  $Shutdown,
  $Status,
  $ToJSON
)
