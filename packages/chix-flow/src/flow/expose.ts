import {IIP} from '@chix/common'
import {forOf} from '@fbpx/lib'
import {$Event, $Identity} from '../common/'
import {FlowEvents} from '../events/flow'
import {ExternalInputPort} from '../port/'
import {Constructor} from '../types'
import {mixin} from '../util'
import {$Control} from './control'
import {Flow} from './flow'
import {$IIP} from './iip'
import {$Ports} from './ports'

export type $ExposeExtensionType = $Control & $Event & $Identity & $IIP & $Ports

export interface $Expose<T> {
  expose: (input: any, output: any) => Flow
}

export function $Expose<T extends Constructor<$ExposeExtensionType>>(
  Base: T
): Constructor<$Expose<T>> & T {
  return class Expose$ extends Base implements $Expose<T> {
    /**
     *
     * Helper function to make the flow an npm module itself.
     *
     * Usage:
     *
     *   const xflow = new xFlow:create(map, loader)
     *   xflow.addMap(map)
     *
     *   module.exports = xflow.expose
     *
     *   ---
     *
     *   import { flow } from 'my-flow'
     *
     *   flow({
     *     in: 'some_data',
     *     in2: 'other_data'
     *   }, {
     *     out: function(data) {
     *       // do something with data..
     *     }
     *   })
     */
    public expose(input: any, output: any): Flow {
      const iips: IIP[] = []

      if (this.hasOwnProperty('ports')) {
        if (this.ports.hasOwnProperty('input')) {
          forOf((name: string, port: ExternalInputPort) => {
            const iip = {
              data: input[name],
              target: {
                id: port.nodeId,
                port: port.name,
              },
            }

            iips.push(iip)

            // Within the exposed ports these should
            // already be set if they must be used.
            // (implement that) they are not properties
            // a caller should set.
            //
            // target.settings,
            // target.action
          }, this.ports.input)
        } else {
          throw Error(
            'The map provided does not have any input ports available'
          )
        }

        if (output) {
          const cb = output

          if (this.ports.hasOwnProperty('output')) {
            // setup callbacks
            this.on(FlowEvents.OUTPUT.name, (data) => {
              if (data.node.id === this.id && cb.hasOwnProperty(data.port)) {
                // TODO: does not take ownership into account
                cb[data.port](data.out)
              }
            })
          } else {
            throw Error(
              'The map provided does not have any output ports available'
            )
          }
        }
      } else {
        throw Error('The map provided does not have any ports available')
      }

      // start it all
      if (iips.length) {
        this.sendIIPs(iips)
      }

      this.push()

      return this as any
    }
  }
}

export namespace $Expose {
  export function create(Base: any) {
    return mixin(
      $Expose,
      $Control.create,
      $Event.create,
      $Identity.create,
      $IIP.create,
      $Ports.create
    )(Base)
  }
}
