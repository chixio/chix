import {$Identity} from '../common/identity'
import {Constructor} from '../types'
import {mixin} from '../util'
import {$Node} from './node'

export interface $Report {
  report: () => any
}

export type $ReportExtensionType<T> = $Identity & $Node<T>

export function $Report<T extends Constructor<$ReportExtensionType<T>>>(
  Base: T
): Constructor<$Report> & T {
  return class Report$ extends Base implements $Report {
    /**
     *
     * JSON Status report about the nodes.
     *
     * Mainly meant to debug after shutdown.
     *
     * Should handle all stuff one can think of
     * why `it` doesn't work.
     *
     */
    public report() {
      // let size
      // const qm = this.ioHandler.queueManager

      const _report: any = {
        flow: this.id,
        nodes: [],
        ok: true,
        queues: [],
      }

      for (const node of this.nodes.values()) {
        if (node.status !== 'complete') {
          _report.ok = false
          _report.nodes.push({
            node: node.report(),
          })
        }
      }

      /*
    for (const link of this.links.values()) {
      if (qm.hasQueue(link.ioid)) {
        size = qm.size(link.ioid)
        _report.ok = false
        _report.queues.push({
          link: link.toJSON(),
          port: link.target.port,
          // super weird, will be undefined if called here.
          // size: qm.size(link.ioid),
          size: size,
          node: this.getNode(link.target.id).report()
        })
      }
    }
    */

      return _report
    }
  }
}

export namespace $Report {
  export function create(Base: any) {
    return mixin($Report, $Identity.create, $Node.create)(Base)
  }
}
