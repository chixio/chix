import {Constructor} from '../types'
import {mixin} from '../util'

export interface $Shutdown {
  hasShutdown: () => boolean
}

export function $Shutdown<T extends Constructor>(
  Base: T
): Constructor<$Shutdown> & T {
  return class Shutdown$ extends Base implements $Shutdown {
    public hasShutdown(): boolean {
      return false
    }
  }
}

export namespace $Shutdown {
  export function create(Base: any) {
    return mixin($Shutdown)(Base)
  }
}
