import {$Parent} from '../common/parent'
import {Process} from '../common/process'
import {xNode} from '../node/node'
import {ProcessManager} from '../process/manager'
import {Constructor} from '../types'
import {mixin} from '../util'
import {Flow} from './flow'
import {$Node} from './node'
import {$ToJSON} from './toJSON'

export type $ProcessExtensionType<T> = $Node<T> & $Parent<T> & $ToJSON

export interface ProcessMap {
  [id: string]: Process
}

export interface $Process<T> {
  pid: string | null
  ppid: string | null
  children: string[]
  processManager: ProcessManager
  getNodeByPid(pid: string): xNode | Flow
  getProcessMap(): ProcessMap
  setPid(pid: string): void
  setProcessManager(processManager: ProcessManager): T
}

export function $Process<T extends Constructor<$ProcessExtensionType<T>>>(
  Base: T
): Constructor<$Process<T>> & T {
  return class Process$ extends Base implements $Process<T> {
    public pid: string | null = null
    public ppid: string | null = null
    public processManager: ProcessManager = new ProcessManager()

    /**
     *
     * Add Process Manager.
     *
     * The Process Manager holds all processes.
     *
     * @param {ProcessManager} processManager
     * @public
     *
     */
    public setProcessManager(processManager: ProcessManager): T {
      this.processManager = processManager

      return this as any
    }

    /**
     *
     * Retrieve a node by it's process id
     *
     * @param {String} pid - Process ID
     * @return {Object} node
     * @public
     */
    public getNodeByPid(pid: string): xNode | Flow {
      const process = Array.from(this.nodes.values()).find(
        (node) => node.pid === pid
      )

      if (!process) {
        throw Error('Process not found')
      }

      return process as xNode | Flow
    }

    /**
     * Returns a list of process ids of the child nodes.
     *
     */
    public get children(): string[] {
      const result: string[] = []
      for (const node of this.nodes.values()) {
        if (node.pid !== null) {
          result.push(node.pid)
        }
      }
      return result
    }

    /**
     * Returns a map containing the process ids of:
     *
     *  - this node
     *  - the parent node (if any)
     *  - all descendent nodes
     */
    public getProcessMap(): ProcessMap {
      if (this.pid === null) {
        throw Error('Pid is null')
      }
      let processes: ProcessMap = {
        [this.pid]: this.toJSON(true),
      }
      for (const child of this.nodes.values()) {
        // Note: don't use `instance of Flow` here it will cause circular
        //       using Flow as a type in this file is fine.
        if ((child as Flow).nodes) {
          processes = {
            ...processes,
            ...(child as Flow).getProcessMap(),
          }
        } else {
          if (child.pid === null) {
            throw Error('child.pid is null')
          }
          processes[child.pid] = child.toJSON(true)
        }
      }

      return processes
    }

    /**
     *
     * Used by the process manager to set our process id
     *
     */
    public setPid(pid: string): void {
      this.pid = pid
    }
  }
}

export namespace $Process {
  export function create(Base: any) {
    return mixin($Process, $Node.create, $ToJSON.create)(Base)
  }
}
