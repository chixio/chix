import {Constructor} from '../types'
import {mixin} from '../util'

export interface $Action {
  actions: any
  actionName: string
  action: (action: string) => any
  isAction: () => boolean
  use: (action: string) => void
}

export function $Action<T extends Constructor>(
  Base: T
): Constructor<$Action> & T {
  return class Action$ extends Base implements $Action {
    public actions: any
    public actionName: string = ''

    public action(action: string): any {
      if (!this.actions.hasOwnProperty(action)) {
        throw Error('this.action should return something with the action map.')
        /*
     const ActionActor = this.action(action)

     // ActionActor.map.ports = this.ports

     // not sure what to do with the id and identifier.
     // I think they should stay the same, for now.
     //
     this.actions[action] = Flow.create(
     // ActionActor, // BROKEN
     map, // action definition should be here
     this.identifier + '::' + action
     )

     // a bit loose this.
     this.actions[action].actionName = action

     //this.actions[action].ports = this.ports
     */
      }

      return this.actions[action]
    }

    public isAction(): boolean {
      return Boolean(this.actionName)
    }

    public use(_action: string): void {}
  }
}

export namespace $Action {
  export function create(Base: any) {
    return mixin($Action)(Base)
  }
}
