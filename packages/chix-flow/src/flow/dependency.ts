import {DependencyLoader} from '@chix/loader'
import {Constructor} from '../types'
import {mixin} from '../util'

export interface $Dependency {
  dependencyLoader: DependencyLoader
  setDependencyLoader: (loader: DependencyLoader) => void
}

export function $Dependency<T extends Constructor>(
  Base: T
): Constructor<$Dependency> & T {
  return class Dependency$ extends Base implements $Dependency {
    public dependencyLoader!: DependencyLoader
    /**
     * Adds the dependency Loader
     *
     * This provides an api to get the required dependencies.
     * @public
     */
    public setDependencyLoader(loader: DependencyLoader): void {
      this.dependencyLoader = loader
    }
  }
}

export namespace $Dependency {
  export function create(Base: any) {
    return mixin($Dependency)(Base)
  }
}
