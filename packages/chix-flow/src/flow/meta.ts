import {$Event, $Identity} from '../common'
import {FlowEvents} from '../events/flow'
import {Constructor} from '../types'
import {mixin} from '../util'
import {$Node} from './node'

export interface $MetaNode {
  setMeta: (nodeId: string, key: string, value: any) => void
}

export type $MetaNodeExtensionType<T> = $Event & $Identity & $Node<T>

export function $MetaNode<T extends Constructor<$MetaNodeExtensionType<T>>>(
  Base: T
): Constructor<$MetaNode> & T {
  return class MetaNode$ extends Base implements $MetaNode {
    /**
     * Will set a meta property for the node specified by nodeId
     *
     * A meta property can contain any sort of value.
     *
     * @param {String} nodeId - The nodeId
     * @param {String} key - The meta key
     * @param {Any} value - The meta value
     * @fires Flow#metadata
     */
    public setMeta(nodeId: string, key: string, value: any): void {
      const node = this.getNode(nodeId)

      node.setMeta(key, value)

      this.event(FlowEvents.METADATA, {
        id: this.id,
        node: node.export(),
      })
    }
  }
}

export namespace $MetaNode {
  export function create(Base: any) {
    return mixin($MetaNode, $Event.create, $Identity.create, $Node.create)(Base)
  }
}
