import {Context} from '@chix/common'
import {forOf} from '@fbpx/lib'
import d from 'debug'
import {$Event, $Identity} from '../common'
import {DefaultContextProvider} from '../context'
import {NodeEvents} from '../events'
import {ExternalInputPort} from '../port/external'
import {Constructor} from '../types'
import {mixin} from '../util'
import {$Node} from './node'
import {$Ports} from './ports'

const debug = d.debug('chix:flow')

export type $ContextExtensionType<T> = $Event & $Identity & $Node<T> & $Ports

export interface $Context {
  context: Context
  addContext: (context: Context) => void
  contextProvider: DefaultContextProvider
  clearContextProperty: (port: string) => void
  setContextProperty: (port: string, data: any, trigger?: boolean) => void
  setContextProvider: (provider: DefaultContextProvider) => void
}

export function $Context<T extends Constructor<$ContextExtensionType<T>>>(
  Base: T
): Constructor<$Context> & T {
  return class Context$ extends Base implements $Context {
    public contextProvider: DefaultContextProvider =
      new DefaultContextProvider()

    get context(): Context {
      const context: {[key: string]: any} = {}

      forOf((name: string, port: ExternalInputPort) => {
        context[name] = port.context
      }, this.ports.input)

      return context
    }

    public addContext(context: Context): void {
      debug('%s: addContext', this.identifier)
      forOf((port: string, val: any) => {
        const portDef = this.getPortDefinition(port, 'input')

        this.getNode(portDef.nodeId).setContextProperty(portDef.name, val)
      }, context)
    }

    public setContextProperty(
      port: string,
      data: any,
      trigger?: boolean
    ): void {
      const portDef = this.getPortDefinition(port, 'input')

      this.getNode(portDef.nodeId).setContextProperty(
        portDef.name,
        data,
        trigger
      )

      // TODO: test if it succeeded
      this.event(NodeEvents.CONTEXT_UPDATE, {
        data,
        node: this,
        port,
      })
    }

    /**
     * Clears the context set at the internal port.
     *
     * Note: the event is triggered for this node with the external port name.
     *
     * @param port
     */
    public clearContextProperty(port: string): void {
      const portDef = this.getPortDefinition(port, 'input')
      const inputPort = this.getNode(portDef.nodeId).getInputPort(portDef.name)

      const context = inputPort.context?.read(inputPort)

      inputPort.clearContext()

      this.event(NodeEvents.CONTEXT_CLEAR, {
        node: this,
        port,
        context,
      })
    }

    /**
     *
     * Add a new context provider.
     *
     * A context provider pre-processes the raw context
     *
     * This is useful for example when using the command line.
     * All nodes which do not have context set can be asked for context.
     *
     * E.g. database credentials could be prompted for after which all
     *      input is fullfilled and the flow will start to run.
     *
     * @param {DefaultContextProvider} provider
     * @private
     *
     */
    public setContextProvider(provider: DefaultContextProvider): void {
      this.contextProvider = provider
    }
  }
}

export namespace $Context {
  export function create(Base: any) {
    return mixin(
      $Context,
      $Event.create,
      $Identity.create,
      $Node.create,
      $Ports.create
    )(Base)
  }
}
