import {Providers} from '@chix/common'
import {Constructor} from '../types'
import {mixin} from '../util'
import {$Flow} from './flow-interface'

export interface $Providers {
  provider: string | undefined
  providers: Providers | undefined
}

export type $ProvidersExtensionType = $Providers & $Flow

export function $Providers<T extends Constructor<$ProvidersExtensionType>>(
  Base: T
): Constructor<$Providers> & T {
  return class Providers$ extends Base implements $Providers {
    protected _provider: any

    get provider(): string | undefined {
      return this._provider || this._node.provider
    }

    set provider(val) {
      this._provider = val
    }

    get providers(): Providers | undefined {
      return this._node.providers
    }
  }
}

export namespace $Providers {
  export function create(Base: any) {
    return mixin($Providers)(Base)
  }
}
