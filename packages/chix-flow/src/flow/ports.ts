import {ExternalPort, ExternalPortsDefinition} from '@chix/common'
import {forOf} from '@fbpx/lib'
import {$Event, $ExportCommon, $Identity, $PortsCommon} from '../common'
import {Connector} from '../connector'
import {FlowEvents} from '../events'
import {Link} from '../link'
import {PortGroup} from '../node'
import {Packet} from '../packet'
import {
  ExternalInputPort,
  ExternalOutputPort,
  ExternalStartPort,
  InputPort,
  OutputPort,
} from '../port/'
import {Constructor} from '../types'
import {mixin} from '../util'
import {$Action} from './action'
import {$Export} from './export'
import {$Link} from './link'
import {$Node} from './node'

export interface PortOutput {
  node: any // export
  port: string
  out: Packet
  action?: (name: string) => any // not sure if this is still correct..
}

export type $PortsExtensionType<T> = $Action &
  $Event &
  $ExportCommon &
  $Identity &
  $Link &
  $Node<T>

export interface $Ports {
  filled: number
  inPorts: string[]
  outPorts: string[]
  linkMap: Map<string, Link>
  ports: ExternalPortsDefinition
  portExists: (type: PortGroup, port: string) => boolean
  portIsOpen: (port: string) => boolean
  getPortDefinition: (
    port: string,
    type: PortGroup,
    createStartPort?: boolean
  ) => ExternalInputPort | ExternalOutputPort
  getPort: (
    type: PortGroup,
    name: string
  ) => ExternalInputPort | ExternalOutputPort
  getInputPort: (name: string) => ExternalInputPort
  getOutputPort: (name: string) => ExternalOutputPort
  getPortOption: (type: PortGroup, port: string, option: string) => any
  setPortOption: (
    type: PortGroup,
    port: string,
    opt: string,
    value: any
  ) => void
  exposePort: (
    type: PortGroup,
    nodeId: string,
    port: string,
    name: string
  ) => void
  removePort: (type: PortGroup, name: string) => void
  renamePort: (type: PortGroup, from: string, to: string) => void
  addPort: (
    type: PortGroup,
    name: string,
    externalPortDefinition: ExternalPort
  ) => void
  closePort: (port: string) => void
  getPortType: (kind: PortGroup, port: string | string[]) => string
  inputPortAvailable: (target: Connector) => boolean
  createPorts: (ports: ExternalPortsDefinition) => void
}

export function $Ports<T extends Constructor<$PortsExtensionType<T>>>(
  Base: T
): Constructor<$Ports> & T {
  return class Ports$ extends Base implements $Ports, $PortsCommon {
    public ports: ExternalPortsDefinition = {
      input: {},
      output: {},
    }

    public linkMap: Map<string, Link> = new Map()

    /* Now just done by Flow
    constructor(...args: any[]) {
      super(...args)

      const { node } = getNodeArgument(args) as { node: FlowDefinition }

      if (node.ports) {
        this.ports = JSON.parse(JSON.stringify(node.ports))
      }

      // Warning: is in between but moved to map mixin.
      // addMap
    }
    */

    get filled() {
      let filled = 0

      if (this.ports.input) {
        forOf((_name: any, port: ExternalInputPort) => {
          if (port.filled) {
            filled++
          }
        }, this.ports.input)
      }

      return filled
    }

    get inPorts() {
      return this.ports.input ? Object.keys(this.ports.input) : []
    }

    get outPorts() {
      return this.ports.output ? Object.keys(this.ports.output) : []
    }

    // TODO: has been moved from Flow to here
    public initPortOptions(): void {
      // Init port options.
      for (const port in this.ports.input) {
        if (this.ports.input.hasOwnProperty(port)) {
          // This flow's port
          const thisPort = this.ports.input[port]

          // set port option
          if ((thisPort as any).options) {
            throw Error('Deprecated, use settings')
            /*
              for (const opt in thisPort.options) {
                if (thisPort.options.hasOwnProperty(opt)) {
                  this.setPortOption(
                    'input',
                    port,
                    opt,
                    thisPort.options[opt])
                }
              }
            */
          }
        }
      }
    }

    /**
     *
     * Checks whether the port exists at the node
     * this Flow is relaying for.
     *
     * @param {String} type
     * @param {String} port
     */
    public portExists(type: PortGroup, port: string): boolean {
      // this returns whether this port exists for _us_
      // it only considers the exposed ports.
      const portDef = this.getPortDefinition(port, type, false)
      return this.getNode(portDef.nodeId).portExists(type, portDef.name)
    }

    /**
     *
     * Checks whether the port is open at the node
     * this Flow is relaying for.
     *
     * @param {String} port
     */
    public portIsOpen(port: string): boolean {
      // the port open logic is about _our_ open and exposed ports.
      // yet of course it should check the real node.
      // so also delegate.
      const portDef = this.getPortDefinition(port, 'input')
      // Todo there is no real true false in portIsOpen?
      // it will fail hard.
      const node = this.getNode(portDef.nodeId)

      return node.portIsOpen(portDef.name)
    }

    /**
     *
     * Get _this_ Flow's port definition.
     *
     * The definition contains the _real_ port name
     * of the node _this_ port is relaying for.
     *
     * @param {String} port
     * @param {PortGroup} type
     * @param {boolean} _createStartPort
     */
    public getPortDefinition(
      port: string,
      type: PortGroup,
      _createStartPort?: boolean
    ): ExternalInputPort | ExternalOutputPort {
      if (port === ':start' && type === 'input') {
        // && _createStartPort && !this.portExists('input', ':start')) {
        this.addPort('input', ':start', {
          name: ':start',
          required: false,
          type: 'any',
        } as any)
      }

      const ports = (this.ports as any)[type]

      if (ports.hasOwnProperty(port)) {
        return ports[port]
      }

      throw new Error(
        [
          `Unable to find exported port definition for ${type} port '${port}' (${this.ns}:${this.name})`,
          `Available ports: ${Object.keys(ports).toString()}`,
        ].join('\n\t')
      )
    }

    public getPort(
      type: PortGroup,
      name: string
    ): ExternalInputPort | ExternalOutputPort {
      return this.getPortDefinition(name, type)
    }

    public getInputPort(name: string): ExternalInputPort {
      return this.getPortDefinition(name, 'input') as ExternalInputPort
    }

    public getOutputPort(name: string): ExternalOutputPort {
      return this.getPortDefinition(name, 'output') as ExternalOutputPort
    }

    /**
     *
     * Get the port option at the node
     * this flow is relaying for.
     *
     * @param {String} type
     * @param {String} port
     * @param {String} option
     */
    public getPortOption(type: PortGroup, port: string, option: string): any {
      // Exposed ports can also have options set.
      // if this is _our_ port (it is exposed)
      // just delegate this to the real node.
      const portDef = this.getPortDefinition(port, type)
      // Todo there is no real true false in portIsOpen?
      // it will fail hard.
      return this.getNode(portDef.nodeId).getPortOption(
        type,
        portDef.name,
        option
      )
    }

    /**
     *
     * Sets an input port option.
     *
     * The node schema for instance can specify whether a port is persistent.
     *
     * At the moment a connection can override these values.
     * It's a way of saying I give you this once so take care of it.
     *
     * Ok, with forks running this should eventually be much smarter.
     * If there are long running flows, all instances should have their
     * ports updated.
     *
     * Not sure when setPortOption is called, if it is called during 'runtime'
     * there is no problem and we could just set it on the current Actor.
     * I could also just already fix it and update baseActor and all _actors.
     * which would be sufficient.
     */
    public setPortOption(
      type: PortGroup,
      port: string,
      opt: string,
      value: any
    ): void {
      const portDef = this.getPortDefinition(port, type)
      this.getNode(portDef.nodeId).setPortOption(type, portDef.name, opt, value)
    }

    public exposePort(
      type: PortGroup,
      nodeId: string,
      port: string,
      name: string
    ): void {
      const node = this.getNode(nodeId)

      const ports = (node.ports as any)[type]

      if (ports) {
        for (const p in ports) {
          if (ports.hasOwnProperty(p)) {
            if (p === port) {
              this.addPort(type, name, {
                name: port,
                nodeId,
              })

              break
            }
          }
        }
      }

      this.event(FlowEvents.ADD_PORT, {
        name: port,
        node: this.export(),
        nodeId,
        port: name,
        type,
      })
    }

    public removePort(type: PortGroup, name: string): void {
      if (this.portExists(type, name)) {
        this._deletePort(type, name)

        this.event(FlowEvents.REMOVE_PORT, {
          node: this.export(),
          port: name,
          type,
        })
      }
    }

    public renamePort(type: PortGroup, from: string, to: string): void {
      const ports = (this.ports as any)[type]
      if (ports[from]) {
        ports[to] = ports[from]
        ports[to].name = to

        for (const link of this.links.values()) {
          if (
            type === 'input' &&
            link.target.id === this.id &&
            link.target.port === from
          ) {
            link.target.port = to
          } else if (
            type === 'output' &&
            link.source.id === this.id &&
            link.source.port === from
          ) {
            link.source.port = to
          }
        }

        this._deletePort(type, from)

        this.event(FlowEvents.RENAME_PORT, {
          from,
          node: this.export(),
          to,
          type,
        })
      }
    }

    public addPort(
      type: PortGroup,
      name: string,
      externalPortDefinition: ExternalPort
    ): void {
      const ports = (this.ports as any)[type]

      if (!ports) {
        ports[type] = {}
      }

      if (type === 'input' && name === ':start') {
        ports[name] = new ExternalStartPort(externalPortDefinition, this as any)
      } else {
        const internalPort = this.getNode(
          (externalPortDefinition as any).nodeId
        ).getPort(type, externalPortDefinition.name)

        if (type === 'input') {
          ports[name] = new ExternalInputPort(
            externalPortDefinition,
            internalPort as InputPort | ExternalInputPort
          )
        } else {
          ports[name] = new ExternalOutputPort(
            externalPortDefinition,
            internalPort as OutputPort | ExternalOutputPort
          )
        }
      }

      const _port = ports[name]
      _port.setParent(this)
    }

    /**
     *
     * Close the port of the node we are relaying for
     * and also close our own port.
     *
     * @param {String} port
     */
    public closePort(port: string): void {
      // delegate this to the real node
      // only if this is one of _our_ exposed nodes.
      const portDef = this.getPortDefinition(port, 'input')

      if (port !== ':start') {
        this.getNode(portDef.nodeId).closePort(portDef.name)
      }
    }

    public getPortType(kind: PortGroup, port: string | string[]): string {
      if (port === ':start') {
        return 'any'
      }

      let portName

      if (Array.isArray(port)) {
        if (port.length < 1) {
          throw Error('Cannot determine port.')
        }
        portName = port[0]
      } else {
        portName = port
      }

      const portDef = this.getPortDefinition(portName, 'input')
      const node = this.getNode(portDef.nodeId)
      const type = node.getPortType(kind, portDef.name)

      if (type) {
        return type
      }

      throw Error('Unable to determine port type')
    }

    public inputPortAvailable(target: Connector): boolean {
      if (!target.port) {
        throw Error(`Target port is not defined.`)
      }

      if (target.action && !this.isAction()) {
        return this.action(target.action).inputPortAvailable(target)
      } else {
        if (target.port === ':start') {
          return true
        } else {
          const portDef = this.getPortDefinition(target.port, 'input')

          if (!target.wire) {
            throw Error('Port is not connected')
          }

          if (!this.linkMap.has(target.wire.id)) {
            throw Error('Cannot find internal link within linkMap')
          }

          const link = this.linkMap.get(target.wire.id) as Link

          if (link.target) {
            const targetNode = this.getNode(portDef.nodeId)

            return targetNode.inputPortAvailable(link.target)
          }

          throw Error(`Internal link does not contain a target.`)
        }
      }
    }

    /**
     *
     * Receive a ports definition and creates the Port instances
     *
     * @param {Object} ports
     * @param {Object} ports.input
     * @param {Object} ports.output
     */
    public createPorts(
      ports: ExternalPortsDefinition = {
        input: {},
        output: {},
      }
    ): void {
      // initialize both input and output ports might
      // one of them be empty.
      if (!ports.input) {
        ports.input = {}
      }
      if (!ports.output) {
        ports.output = {}
      }

      this.ports = {input: {}, output: {}}

      forOf(
        (name: string, portDefinition: ExternalPort) =>
          this.addPort('input', name, portDefinition),
        ports.input
      )
      forOf(
        (name: string, portDefinition: ExternalPort) =>
          this.addPort('output', name, portDefinition),
        ports.output
      )

      // Always add complete port, :start port will be added dynamically
      // this.addPort('output', ':complete', {name: ':complete', type: 'any'})
    }

    public sendPortOutput(port: string, packet: Packet): void {
      const out: PortOutput = {
        node: this.export(),
        out: packet,
        port,
      }

      if (this.isAction()) {
        out.action = this.action
      }

      packet.release(this)

      this.event(FlowEvents.OUTPUT, out)
    }

    private _deletePort(type: PortGroup, name: string) {
      const ports = (this.ports as any)[type]

      delete ports[name]
    }
  }
}

export namespace $Ports {
  export function create(Base: any) {
    return mixin(
      $Ports,
      $Action.create,
      $Event.create,
      $Export.create,
      $Identity.create,
      $Link.create,
      $Node.create
    )(Base)
  }
}
