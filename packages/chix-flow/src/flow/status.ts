export const $Status = {
  HOLD: 'hold',
  RUNNING: 'running',
  STOPPED: 'stopped',
}
