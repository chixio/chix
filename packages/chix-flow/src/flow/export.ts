import {$Identity} from '../common'
import {$ExportCommon} from '../common/interfaces'
import {Constructor} from '../types'
import {mixin} from '../util'
import {$Control} from './control'
import {$Ports} from './ports'
import {$Process} from './process'

export type $ExportExtensionType<T> = $Control &
  $Identity &
  $Process<T> &
  $Ports

export function $Export<T extends Constructor<$ExportExtensionType<T>>>(
  Base: T
): Constructor<$ExportCommon> & T {
  return class Export$ extends Base implements $ExportCommon {
    /**
     *
     * Return a serializable export of this flow.
     *
     * @public
     */
    public export(): any {
      return {
        active: this.active,
        id: this.id,
        identifier: this.identifier,
        inPorts: this.inPorts,
        name: this.name,
        ns: this.ns,
        outPorts: this.outPorts,
        pid: this.pid,
        ports: this.ports,
        provider: this.provider,
        // filled: this.filled,
        // context: this.context,
        // input: this._filteredInput(),
        // nodeTimeout: this.nodeTimeout,
        // inputTimeout: this.inputTimeout
      }
    }
  }
}

export namespace $Export {
  export function create(Base: any) {
    return mixin(
      $Export,
      $Control.create,
      $Identity.create,
      $Process.create,
      $Ports.create
    )(Base)
  }
}
