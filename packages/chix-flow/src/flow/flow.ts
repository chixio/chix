import {Flow as FlowDefinition} from '@chix/common'
import {DependencyLoader, Loader, RequireDependencyLoader} from '@chix/loader'
import * as uuid from 'uuid'
import {DefaultContextProvider} from '../context'
import {IoHandler} from '../io/handler'
import {xNode} from '../node'
import {ProcessManager} from '../process'
import {Constructor, MixinParams} from '../types'
import {BaseFlow} from './BaseFlow'
import {$Flow} from './flow-interface'
import {FlowMixin, FlowType} from './mixin'

function FlowMix<T>(Base: T): Constructor<FlowType<T>> & T {
  return FlowMixin(Base)
}

export type NodeTypes = {
  [name: string]: typeof xNode
}

export class Flow extends FlowMix(BaseFlow) implements $Flow {
  public static nodeTypes: NodeTypes = {
    xNode,
  }

  /**
   * Create an xFlow
   *
   * @public
   */
  public static async create(
    id: string,
    node: FlowDefinition,
    loader?: Loader,
    ioHandler?: IoHandler,
    processManager?: ProcessManager,
    dependencyLoader?: DependencyLoader
  ): Promise<Flow> {
    const flow = new Flow(
      id,
      node,
      loader,
      ioHandler,
      processManager,
      dependencyLoader,
      true
    )

    await flow.init()

    return flow
  }

  /**
   *
   * Register extra node types:
   *
   * Current types available are:
   *
   *  - ReactNode
   *  - PolymerNode
   *
   */
  public static registerNodeType(Type: any): void {
    this.nodeTypes[Type.name] = Type
  }

  public type = 'flow'

  public contextProvider: DefaultContextProvider = new DefaultContextProvider()

  public _delay = 0
  public active = false
  public runCount = 0
  public _interval = 100
  public _inputTimeout = null
  public nodeTimeout = 3000
  public inputTimeout = 3000
  public trigger?: boolean

  public _node: FlowDefinition

  // circumvent circular import
  protected Self: Constructor<Flow>

  constructor(
    id: string,
    node: FlowDefinition,
    loader: Loader = new Loader(),
    ioHandler: IoHandler = new IoHandler(),
    processManager: ProcessManager = new ProcessManager(),
    dependencyLoader: DependencyLoader = new RequireDependencyLoader(),
    guardInstantiation: boolean = false
  ) {
    super({
      id,
      node,
      ioHandler,
      loader,
      processManager,
      dependencyLoader,
    } as MixinParams)

    if (!guardInstantiation) {
      throw Error('Use `await Flow.create()` to instantiate a new Flow')
    }

    // this.Self = Flow
    this.Self = Flow

    if (!id) {
      throw Error('xFlow requires an id')
    }

    if (!node) {
      throw Error('xFlow requires a map')
    }

    this.setLoader(loader)
    this.setIoHandler(ioHandler)
    this.setProcessManager(processManager)
    this.setDependencyLoader(dependencyLoader)

    this._node = {
      ...node,
      id,
    }
  }

  public async init() {
    // Need to think about how to implement this for flows
    // this.ports.output[':complete'] = { type: 'any' }
    const node = this._node

    if (node.nodeTimeout) {
      this.nodeTimeout = node.nodeTimeout
    }

    if (node.inputTimeout) {
      this.inputTimeout = node.inputTimeout
    }

    if (node.ports) {
      this.ports = JSON.parse(JSON.stringify(node.ports))
    }

    if (node.main) {
      node.nodes = node.nodes.map((_node) => {
        if (!_node.id) {
          return {
            ..._node,
            id: uuid.v4(),
          }
        }

        return _node
      })
    }

    await this.addMap(node)

    this.createPorts(this.ports)

    // this.setup()
    // this.initPortOptions()

    // this.listenForOutput()

    this.setStatus('created')
  }
}
