import {ExternalPort} from '@chix/common'
import {Connector} from '../connector'
import {PortGroup} from '../node'
import {InputPort, OutputPort} from '../port'
import {Constructor} from '../types'
import {mixin} from '../util'
import {$Node} from './node'

export type $PortExtensionType<T> = $Node<T>

export interface $Port {
  plugPort: (
    type: PortGroup,
    connector: Connector
  ) => InputPort | OutputPort | ExternalPort
  unplugPort: (type: PortGroup, connector: Connector) => any
}

export function $Port<T extends Constructor<$PortExtensionType<T>>>(
  Base: T
): Constructor<$Port> & T {
  return class Port$ extends Base implements $Port {
    /**
     *
     * Plugs a source into a target node
     *
     *
     * @param {PortGroup} type
     * @param {Connector} connector
     */
    public plugPort(
      type: PortGroup,
      connector: Connector
    ): InputPort | OutputPort | ExternalPort {
      return this.getNode(connector.id as string)
        .getPort(type, connector.port as string)
        .plug(connector) as any
    }

    /**
     *
     * Unplugs a port for the node specified.
     *
     * @param {PortGroup} type
     * @param {Connector} connector
     */
    public unplugPort(type: PortGroup, connector: Connector): any {
      if (this.hasNode(connector.id as string)) {
        return this.getNode(connector.id as string)
          .getPort(type, connector.port as string)
          .unplug(connector)
      }

      return false
    }
  }
}

export namespace $Port {
  export function create(Base: any) {
    return mixin($Port, $Node.create)(Base)
  }
}
