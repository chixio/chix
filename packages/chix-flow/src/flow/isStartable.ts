import {Constructor} from '../types'
import {mixin} from '../util'

export interface $IsStartable {
  isStartable: () => boolean
}

export function $IsStartable<T extends Constructor>(
  Base: T
): Constructor<$IsStartable> & T {
  return class IsStartable$ extends Base implements $IsStartable {
    public isStartable(): boolean {
      return true
    }
  }
}
export namespace $IsStartable {
  export function create(Base: any) {
    return mixin($IsStartable)(Base)
  }
}
