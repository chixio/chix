import {Link as LinkDefinition} from '@chix/common'
import d from 'debug'
import {$Event, $Identity} from '../common/'
import {FlowEvents} from '../events'
import {IIP} from '../IIP'
import {Link} from '../link'
import {Constructor} from '../types'
import {mixin} from '../util'
import {$Control} from './control'
import {$Node} from './node'
import {$Port} from './port'
import {$Process} from './process'

const debug = d.debug('chix:actor')

export type $LinkExtensionType<T> = $Control &
  $Event &
  $Identity &
  $Node<T> &
  $Port &
  $Process<T>

export interface $Link {
  links: Map<string, Link>
  addLink: (link: Link) => Link
  getLink: (id: string) => Link
  getLinks: () => Link[]
  findLink: (linkDefinition: LinkDefinition) => Link
  removeLink: (linkDefinition: LinkDefinition) => any
  removeLinks: (nodeId: string) => void
}

export function $Link<T extends Constructor<$LinkExtensionType<T>>>(
  Base: T
): Constructor<$Link> & T {
  return class Link$ extends Base implements $Link {
    public links = new Map()

    /**
     * Adds a link from nodes port to the other
     *
     * Each link is registered with the IOHandler
     *
     * Emits the addLink event
     *
     * @param {xLink} link
     * @fires Flow#addLink
     */
    public addLink(link: Link): Link {
      debug('%s: addLink()', this.identifier)
      let sourceNode

      if (!(link instanceof Link)) {
        throw Error('Link must be of type Link')
      }

      const existingLink = this._findLink(link)

      if (!(link instanceof IIP) && existingLink) {
        console.log('WARNING: link already added, FIXME')
        this.event(FlowEvents.ADD_LINK, existingLink)
        return existingLink
      }

      // CHANGE THIS, source id should not be the graph id but a unique id.
      if (link.source.id !== this.id) {
        // Warn: IIP has our own id
        sourceNode = this.getNode(link.source.id as string)
        if (!sourceNode.portExists('output', link.source.port as string)) {
          if (!sourceNode.ports.output) {
            throw Error(
              `Target node (${sourceNode.identifier}) does not have any input ports named ${link.source.port}`
            )
          } else {
            throw Error(
              [
                `Source node (${sourceNode.identifier}) does not have an output port named '${link.source.port}'\n`,
                `\tOutput ports available:\t${Object.keys(
                  sourceNode.ports.output
                ).join(', ')}`,
              ].join('\n')
            )
          }
        }
      }

      const targetNode = this.getNode(link.target.id as string)

      debug(
        '%s: %s %s -> %s %s',
        this.identifier,
        sourceNode ? sourceNode.identifier : '',
        link.source ? link.source.port : '',
        link.target.port,
        targetNode.identifier
      )

      if (
        link.target.port !== ':start' &&
        !targetNode.portExists('input', link.target.port as string)
      ) {
        if (!targetNode.ports.input) {
          throw Error(
            `Target node (${targetNode.identifier}) does not have any input ports named ${link.target.port}`
          )
        } else {
          const inputPorts = Object.keys(targetNode.ports.input)

          throw Error(
            [
              `Target node (${targetNode.identifier}) does not have an input port named '${link.target.port}'\n\n`,
              `\tInput ports available:\t${inputPorts.join(', ')}\n`,
            ].join('\n')
          )
        }
      }

      // const targetNode = this.getNode(link.target.id)

      // FIXME: rewriting sync property
      // to contain the process id of the node it's pointing
      // to not just the nodeId defined within the graph
      if (link.target.has('sync')) {
        link.target.set('sync', this.getNode(link.target.get('sync')).pid)
      }

      link.graphId = this.id

      if (!this.pid) {
        throw Error('Unable to connect link without known graph pid')
      }

      link.graphPid = this.pid

      if (link.source.id) {
        if (link.source.id === this.id) {
          link.setSourcePid(this.pid || this.id)
        } else {
          link.setSourcePid(this.getNode(link.source.id).pid as string)
        }
      }

      link.setTargetPid(this.getNode(link.target.id as string).pid as string)

      this.links.set(link.id, link)

      this.ioHandler.connect(link)

      this.plugPort('input', link.target)

      if (link.source.id !== this.id) {
        this.plugPort('output', link.source)
      }

      // outputs are not plugged, also look at ioHandler.connect

      link.on('change', () => {
        this.event(FlowEvents.CHANGE_LINK, link)
      })

      // bit inconsistent with event.node
      // should be event.link
      this.event(FlowEvents.ADD_LINK, link)

      return link
    }

    /**
     * Get link by it's id
     *
     * @param {String} id - Id of the link
     * @returns {*}
     */
    public getLink(id: string): Link {
      return this.links.get(id)
    }

    /**
     * Find a link by it's structure.
     *
     * Can be used to find the link when it's ID is unknown
     *
     * @param {Object} ln
     * @returns {T}
     */
    public findLink(ln: LinkDefinition | Link): Link {
      const foundLink = this._findLink(ln)

      if (!foundLink) {
        throw Error('findLink: Unable to find link.')
      }

      return foundLink
    }

    public hasLink(ln: LinkDefinition): boolean {
      return Boolean(this._findLink(ln))
    }

    /**
     *
     * Removes link
     *
     * Disconnects the link from the IOHandler and removes it.
     *
     * @param {Link} ln
     * @fires Flow#removeLink
     * @public
     */
    public removeLink(ln: Link | LinkDefinition): any {
      const link = this.links.get(ln.id)

      if (!link) {
        // TODO: Seems to happen with ip directly to subgraph (non-fatal)
        console.warn('FIXME: cannot find link')

        return this
      }

      this.unplugPort('input', link.target)

      if (link.source.id !== this.id) {
        this.unplugPort('output', link.source)
      }

      this.ioHandler.disconnect(link)

      // io handler could do this.
      // removelink on the top-level actor/graph
      // is not very useful

      /*
      if (this[what].has(link.id)) {
        const oldLink = this[what].get(link.id)

        this[what].delete(link.id)

        this.event(FlowEvents.REMOVE_LINK, oldLink)

        return this
      }
      */
      if (this.links.has(link.id)) {
        const oldLink = this.links.get(link.id)

        this.links.delete(link.id)

        this.event(FlowEvents.REMOVE_LINK, oldLink)

        return this
      }

      throw Error(`Unable to remove link with id: ${link.id}`)
    }

    public getLinks(): Link[] {
      return Array.from(this.links.values()).filter(
        (link) => !(link instanceof IIP)
      ) as Link[]
    }

    public hasLinks(nodeId?: string): boolean {
      if (nodeId) {
        for (const link of this.links.values()) {
          if (link.source.id === nodeId || link.target.id === nodeId) {
            return true
          }
        }

        return false
      }

      return this.links.size > 0
    }

    public removeLinks(nodeId: string): void {
      for (const link of this.links.values()) {
        if (link.source.id === nodeId || link.target.id === nodeId) {
          this.removeLink(link)
        }
      }
    }

    private _findLink(ln: LinkDefinition | Link): Link | undefined {
      const foundLink = Array.from(this.links.values()).find((_link) => {
        const link = {source: {}, ..._link} as LinkDefinition

        if (
          ln.source &&
          link.source &&
          ln.source.id === link.source.id &&
          ln.target &&
          link.target &&
          ln.target.id === link.target.id &&
          ln.source.port === link.source.port &&
          ln.target.port === link.target.port
        ) {
          return (
            (!ln.source.setting ||
              !ln.source.setting.index ||
              !ln.source.setting ||
              !ln.source.setting.index ||
              !link.source.setting ||
              ln.source.setting.index === link.source.setting.index) &&
            (!ln.target.setting ||
              !ln.target.setting.index ||
              !link.target.setting ||
              !link.target.setting.index ||
              !link.target.setting ||
              ln.target.setting.index === link.target.setting.index)
          )
        }

        return false
      })

      return foundLink
    }
  }
}

export namespace $Link {
  export function create(Base: any) {
    return mixin(
      $Link,
      $Control.create,
      $Event.create,
      $Identity.create,
      $Node.create,
      $Port.create,
      $Process.create
    )(Base)
  }
}
