import {Flow as FlowDefinition} from '@chix/common'
import {DefaultContextProvider} from '../context'

export interface $Flow {
  type: string
  contextProvider: DefaultContextProvider
  active: boolean
  runCount: number
  nodeTimeout: number
  inputTimeout: number
  trigger?: boolean
  _node: FlowDefinition
}
