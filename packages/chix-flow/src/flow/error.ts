import {LinkEvents} from '../events'
import {xNode} from '../node'
import {Constructor} from '../types'
import {mixin} from '../util'
import {$Node} from './node'

export type $FlowErrorExtensionType<T> = $Node<T>

export interface $FlowError {
  error: (node: xNode, error: Error | string) => Error
}

export function $FlowError<T extends Constructor<$FlowErrorExtensionType<T>>>(
  Base: T
): Constructor<$FlowError> & T {
  return class FlowError$ extends Base implements $FlowError {
    public error(node: xNode, error: Error | string): Error {
      const _error = error instanceof Error ? error : Error(error)

      const errorObject = {
        msg: error,
        node: node.export(),
      }

      node.setStatus('error')

      node.event(LinkEvents.ERROR, errorObject)

      return _error
    }
  }
}

export namespace $FlowError {
  export function create(Base: any) {
    return mixin($FlowError, $Node.create)(Base)
  }
}
