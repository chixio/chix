import d from 'debug'

import {Flow as FlowDefinition, Node, NodeDefinition} from '@chix/common'
import {Loader} from '@chix/loader'

import {$Event, $Identity} from '../common'
import {DefaultContextProvider} from '../context'
import {FlowEvents} from '../events'
import {IoHandler} from '../io/handler'
import {xNode} from '../node/node'
import {Constructor} from '../types'
import {mixin} from '../util'
import {validate} from '../validate'

import {$Dependency} from './dependency'
import {Flow} from './flow'
import {$Loader} from './loader'
import {$Port} from './port'
import {$Ports} from './ports'
import {$Process} from './process'
import {$Shutdown} from './shutdown'

export type $NodeExtensionType<T> = $Event &
  $Identity &
  $Port &
  $Ports &
  $Loader &
  $Dependency &
  $Process<T> &
  $Shutdown

const debug = d.debug('chix:actor')

export interface $Node<T> {
  nodes: Map<string, xNode | Flow>
  // registerNodeType: (Type: any) => void
  createNode: (
    node: Node,
    nodeDefinition?: NodeDefinition
  ) => Promise<xNode | Flow>
  addNode: (node: Node, nodeDefinition: NodeDefinition) => Promise<any>
  renameNode: (nodeId: string) => any
  removeNode: (nodeId: string) => Promise<void>
  removeNodes: () => void
  getNodes: () => (xNode | Flow)[]
  hasNode: (id: string) => boolean
  getNode: (id: string) => xNode | Flow
  // registerNodeType(Type: any): void
}

export function $Node<T extends Constructor<$NodeExtensionType<T>>>(
  Base: T
): Constructor<$Node<T>> & T {
  return class Node$ extends Base implements $Node<T> {
    // TODO: this was first defined in actor so remove it from there..
    public nodes: Map<string, xNode | Flow> = new Map()

    // single(ton) instances
    // public loader: Loader = new Loader()
    public loader!: Loader
    public ioHandler: IoHandler = new IoHandler()
    public contextProvider: DefaultContextProvider =
      new DefaultContextProvider()
    public clear!: () => void

    /**
     *
     * Create/instantiate  a node
     *
     * Node at this stage is nothing more then:
     *
     *  { ns: "fs", name: "readFile" }
     *
     * @param {Object} node - Node as defined within a map
     * @param {Object} nodeDefinition - Node Definition
     * @fires Flow#addNode
     * @public
     */
    public async createNode(
      node: Node,
      nodeDefinition?: NodeDefinition
    ): Promise<xNode | Flow> {
      if (!nodeDefinition) {
        throw new Error(
          `Failed to get node definition for ${node.ns}:${node.name}`
        )
      }

      if (!node.id) {
        throw Error('Node should have an id')
      }

      if (this.hasNode(node.id)) {
        console.log('Warning: Node already added, skipping... FIXME')
        this.event(FlowEvents.ADD_NODE, {node: this.getNode(node.id)})
        return this.getNode(node.id)
      }

      if (!nodeDefinition.ports) {
        nodeDefinition.ports = {}
      }

      // allow instance to overwrite other node definition data also.
      // probably make much more overwritable, although many
      // should not be overwritten, so maybe just keep it this way.
      if (node.title) {
        nodeDefinition.title = node.title
      }

      if (node.description) {
        nodeDefinition.description = node.description
      }

      const identifier =
        node.title || [node.ns, '::', node.name, '-', this.nodes.size].join('')

      let _node

      if (nodeDefinition.type === 'flow') {
        const flow = node as Node

        if (!flow.id) {
          throw Error('Flow id missing')
        }

        _node = await this.create(
          flow.id as string,
          node,
          nodeDefinition as FlowDefinition
        )
        _node.identifier = identifier

        this.nodes.set(flow.id, _node)

        debug('%s: created %s', this.identifier, _node.identifier)
      } else {
        const cls = nodeDefinition.type || 'xNode'

        const nodeTypes = (this.constructor as any).nodeTypes

        if (nodeTypes.hasOwnProperty(cls)) {
          _node = await nodeTypes[cls].create({
            nodeId: node.id,
            identifier,
            nodeDefinition: nodeDefinition as xNode | Flow,
            dependencyLoader: this.dependencyLoader,
          })

          this.nodes.set(node.id, _node)

          debug('%s: created %s', this.identifier, _node.identifier)

          // register and set pid, xFlow/actor adds itself to it (hack)
          this.processManager.register(_node)

          if (_node.hasShutdown()) {
            // TODO: implement shutdown
            _node.on('execute', () => {
              console.log('will execute')
            })
          }
        } else {
          throw Error(`Unknown node type: ${cls}`)
        }
      }

      _node.setParent(this)

      if (node.provider) {
        _node.provider = node.provider
      }

      if (node.context) {
        this.contextProvider.addContext(
          _node,
          node.context,
          nodeDefinition.ports.input
        )
      }

      this.event(FlowEvents.ADD_NODE, {node: _node})

      return _node

      // CAN BE REMOVED, disposale can be done as soon as the IO manager writes
      /*
     _node.on('freePort', function freePortHandlerActor (event) {
     let links
     let i

     debug('%s:%s freePortHandler', self.identifier, event.port)

     // get all current port connections
     links = this.portGetConnections(event.port)

     if (links.length) {
     if (event.link) {
     // remove the link belonging to this event
     if (event.link.has('dispose')) {
     // TODO: remove cyclic all together, just use core/forEach
     //  this will cause bugs if you send multiple cyclics because
     //  the port is never unplugged..
     if (!event.link.target.has('cyclic')) {
     self.removeLink(event.link)
     }
     }
     }
     }
     })
     */
    }

    /**
     * Creates a new sub flow instance
     *
     * @param _id
     * @param _flow
     * @param flowDefinition
     */
    public async create(
      _id: string,
      _flow: Node,
      flowDefinition: FlowDefinition
    ) {
      console.error('Fix me: Not merging external ports')
      /*
      if (!flow.ports) {
        throw Error('Flow does not have any ports.')
      }

      if (!flowDefinition.ports) {
        throw Error('Flow Definition does not contain any ports.')
      }

      for (const type in flow.ports) {
        if (
          flow.ports.hasOwnProperty(type) &&
          flowDefinition.ports.hasOwnProperty(type)
        ) {
          const flowPorts = (flow.ports as any)[type] as ExternalPorts
          const flowDefinitionPorts = (flowDefinition.ports as any)[type] as ExternalPorts

          for (const name in flowPorts) {
            if (
              flowPorts.hasOwnProperty(name) &&
              flowDefinitionPorts.hasOwnProperty(name)
            ) {
              for (const property in flowPorts[name]) {
                if (flowPorts[name].hasOwnProperty(property)) {
                  // add or overwrite it.
                  (flowDefinitionPorts[name] as any)[property] =
                    (flowPorts[name] as any)[property]
                }
              }
            }
          }
        }
      }
      */

      validate.flow(flowDefinition)

      return (this as any).Self.create(
        _id,
        flowDefinition,
        this.loader, // single(ton) instance
        this.ioHandler, // single(ton) instance
        this.processManager, // single(ton) instance
        this.dependencyLoader
      )
    }

    /**
     *
     * Adds a node to the map.
     *
     * The object format is like it's defined within a map.
     *
     * Right now this is only used during map loading.
     *
     * For dynamic loading care should be taken to make
     * this node resolvable by the loader.
     *
     * Which means the definition should either be found
     * at the default location defined within the map.
     * Or the node itself should carry provider information.
     *
     * A provider can be defined as:
     *
     *  - url:        https://serve.rhcloud.com/flows/{ns}/{name}
     *  - file:       ./{ns}/{name}
     *  - namespace:  MyNs
     *
     * Namespaces are defined within the map, so MyNs will point to
     * either the full url or filesystem location.
     *
     * Once a map is loaded _all_ nodes will carry the full url individually.
     * The namespace is just their to simplify the json format and for ease
     * of maintainance.
     *
     *
     * @param {Object} node
     * @param {NodeDefinition} nodeDefinition
     * @fires Flow#addNode
     * @public
     */
    public async addNode(node: Node, nodeDefinition: NodeDefinition) {
      await this.createNode(node, nodeDefinition)

      return this
    }

    /**
     *
     * Renames a node
     *
     * Renames the id of a node.
     * This should not rename the real id.
     *
     * @param {string} _nodeId
     * @public
     */
    public renameNode(_nodeId: string) {
      throw Error('renameNode not implemented')

      // console.log(nodeId, cb)
    }

    /**
     *
     * Removes a node
     *
     * Warning:
     *  Does not automatically removes the links.
     *  Use $Link#removeLinks in order to do that.
     *
     * @param {string} nodeId
     * @fires Flow#removeNode
     * @public
     */
    public async removeNode(nodeId: string): Promise<void> {
      // should wait for IO, especially there is a chance
      // system events are still spitting.
      // nodeId is an actual object here, the subgraph.

      // register and set pid
      await this.processManager.unregister(this.getNode(nodeId))

      const oldNode = this.getNode(nodeId).export()

      this.nodes.delete(nodeId)

      this.event(FlowEvents.REMOVE_NODE, {
        node: oldNode,
      })
    }

    public removeNodes() {
      this.clear()
    }

    /**
     *
     * Get all nodes.
     *
     * @return {Array<xNode | Flow>} nodes
     * @public
     *
     */
    public getNodes(): (xNode | Flow)[] {
      return Array.from(this.nodes.values())
    }

    /**
     *
     * Check if this node exists
     *
     * @param {String} id
     * @return {Object} node
     * @public
     */
    public hasNode(id: string) {
      return this.nodes.has(id)
    }

    /**
     *
     * Get a node by it's id.
     *
     * @param {String} id
     * @return {Object} node
     * @public
     */
    public getNode(id: string): xNode | Flow {
      if (this.nodes.has(id)) {
        return this.nodes.get(id) as xNode | Flow
      } else {
        for (const node of this.nodes.values()) {
          if (node.id === id || node.identifier === id) {
            return node
          }
        }
      }

      // :iip sets self as source
      if (id === this.id) {
        return this as any
      }

      throw new Error(`Node ${id} does not exist`)
    }
  }
}

export namespace $Node {
  export function create(Base: any) {
    return mixin(
      $Node,
      $Event.create,
      $Identity.create,
      $Port.create,
      $Ports.create,
      $Process.create,
      $Shutdown.create
    )(Base)
  }
}
