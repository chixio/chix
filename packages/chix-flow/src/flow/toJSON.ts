import {$Identity, $Meta} from '../common'
import {Constructor} from '../types'
import {mixin} from '../util'
import {validate} from '../validate'
import {$Link} from './link'
import {$Node} from './node'
import {$Ports} from './ports'
import {$Process} from './process'
import {$Providers} from './providers'

export interface $ToJSON {
  toJSON: (asProcess?: boolean) => any
}

export type $ToJSONExtensionType<T> = $Identity &
  $Link &
  $Meta &
  $Node<T> &
  $Ports &
  $Process<T> &
  $Providers

export function $ToJSON<T extends Constructor<$ToJSONExtensionType<T>>>(
  Base: T
): Constructor<$ToJSON> & T {
  return class ToJSON$ extends Base implements $ToJSON {
    /**
     *
     * Export this modified instance to a node definition.
     *
     * @public
     */
    public toJSON(asProcess: boolean = false): any {
      const nodeDefinition: any = {
        description: this.description,
        id: this.id,
        links: [],
        name: this.name,
        nodes: [],
        ns: this.ns,
        ports: this.ports,
        providers: this.providers,
        title: this.title,
        type: this.type as 'flow',
      }

      for (const node of this.nodes.values()) {
        const _node = {
          id: node.id,
          ns: node.ns,
          name: node.name,
          title: node.title,
          description: node.description,
          context: node.context,
          metadata: node.metadata,
          provider: node.provider,
        }
        if (asProcess) {
          ;(_node as any).pid = node.pid
        }
        nodeDefinition.nodes.push(_node)
      }

      for (const link of this.links.values()) {
        nodeDefinition.links.push(link)
      }

      if (asProcess) {
        nodeDefinition.pid = this.pid
      } else {
        validate.flow(nodeDefinition)
      }

      return JSON.parse(JSON.stringify(nodeDefinition))
    }
  }
}

export namespace $ToJSON {
  export function create(Base: any) {
    return mixin(
      $ToJSON,
      $Identity.create,
      $Link.create,
      $Meta.create,
      $Node.create,
      $Ports.create,
      $Process.create,
      $Providers.create
    )(Base)
  }
}
