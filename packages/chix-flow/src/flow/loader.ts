import {ILoader} from '@chix/loader'
import {Constructor} from '../types'
import {mixin} from '../util'

export interface $Loader {
  loader: ILoader
  setLoader: (loader: ILoader) => void
}

export function $Loader<T extends Constructor>(
  Base: T
): Constructor<$Loader> & T {
  return class Loader$ extends Base implements $Loader {
    // instantiating will cause circular with chix/loader which will load flow
    public loader!: ILoader
    /**
     * Adds the definition Loader
     *
     * This provides an api to get the required node definitions.
     *
     * The loader should already be initialized
     *
     * e.g. the remote loader will already have loaded the definitions.
     * and is ready to respond to getNodeDefinition(ns, name, type, provider)
     *
     * e.g. An async loader could do something like this:
     *
     *   loader(flow, function() { actor.setLoader(loader); }
     *
     * With a sync loader it will just look like:
     *
     * actor.setLoader(loader)
     *
     * @public
     */
    public setLoader(loader: ILoader): void {
      this.loader = loader
    }
  }
}

export namespace $Loader {
  export function create(Base: any) {
    return mixin($Loader)(Base)
  }
}
