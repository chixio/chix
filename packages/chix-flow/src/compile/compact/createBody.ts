import {NodeDefinition} from '@chix/common'
import {ArrayOutput} from '../../node/output'

export function createBody(
  output: ArrayOutput,
  nodeDefinition: NodeDefinition
) {
  if (nodeDefinition.ports.output === undefined) {
    throw Error('Output ports are not defined')
  }
  return [
    output[0],
    '.',
    nodeDefinition.name, // could break existing, but then should be fixed.
    // output[1],
    '(',
    output.length > 2 ? output.slice(2).join(',') + ', ' : '',
    'function ' + nodeDefinition.name + 'Callback',
    '(', // would like that to be output instead of cb..
    Object.keys(nodeDefinition.ports.output), // must be in order
    ') {',
    'cb({',
    Object.keys(nodeDefinition.ports.output)
      .map((k) => {
        return k + ': ' + k
      })
      .join(', '),
    '});',
    '});',
  ].join('')
}
