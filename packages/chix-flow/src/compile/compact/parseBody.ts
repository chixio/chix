export function parseBody(fn: string) {
  return fn
    .replace(/output\s*=/, '')
    .replace(/(\[|\])/g, '')
    .replace(/;/g, '')
    .trim()
    .split(',')
}
