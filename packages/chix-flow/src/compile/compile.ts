import {InputPortsDefinition, NodeDefinition} from '@chix/common'
import {MockDependencyLoader} from '@chix/loader'
import {Packet} from '../packet/packet'
import {NodeBox} from '../sandbox/node'
import {safeName} from '../util/safeName'
import * as compact from './compact'
import {createPortBox} from './createPortBox'

export type InputType = {
  [key: string]: any
}

export type PreloadedNodeExtras = {
  on: any
}

export type PreloadedNodeDefinition = NodeDefinition & PreloadedNodeExtras

const mockDependencyLoader = new MockDependencyLoader()

/**
 *
 * Used to create a non dynamic definition for this node.
 * Having the node & port boxes pre-compiled
 */
export function compile(nodeDefinition: NodeDefinition) {
  if (typeof nodeDefinition.fn !== 'string') {
    throw Error('Could not find function body `fn` at nodeDefinition.fn')
  }

  const fn = nodeDefinition.fn
  delete nodeDefinition.fn

  const nodebox = new NodeBox(safeName(nodeDefinition.name))

  const state = {}
  const output = {}

  const input: InputType = {}

  if (nodeDefinition.ports.input) {
    const inputPorts: InputPortsDefinition = nodeDefinition.ports.input

    if (Object.keys(inputPorts).length) {
      Object.keys(inputPorts).forEach((key: string) => {
        input[key] = Packet.create(true)
      })
    }
  }

  nodebox.set('input', input)

  // const pc = new PacketContainer(input)

  nodebox.set('$', input)

  // done() is added to the nodebox

  // tslint:disable only-arrow-functions
  nodebox.set('done', function () {})
  nodebox.set('cb', function () {})
  // tslint:enable only-arrow-functions

  nodebox.set('state', state)

  if (nodeDefinition.dependencies && nodeDefinition.dependencies.npm) {
    nodebox.addDependencies(
      nodeDefinition.dependencies.npm,
      mockDependencyLoader
    )
  }

  if (nodeDefinition.expose) {
    nodebox.expose(nodeDefinition.expose)
  }

  nodebox.set('output', output)

  if (
    /output(\.[A-z]+)?\s+=/.test(fn) &&
    // should not match = function
    /output\s*=\s*function/.test(fn) === false
  ) {
    // array variant
    if (/output\s*=\s*\[/.test(fn)) {
      nodeDefinition.fn = nodebox.compile(
        compact.createBody(compact.parseBody(fn), nodeDefinition)
      )
    } else {
      // Then consider the whole body as our function.
      nodebox.compile(fn)
      // Function(this.name, nodebox.code.replace(/^return /, ''))
      nodeDefinition.fn = nodebox.fn
    }

    return nodeDefinition
  } else {
    nodebox.compile(fn)
    nodebox.run()

    // port functions

    if (nodeDefinition.ports.input) {
      Object.keys(nodeDefinition.ports.input).forEach((port: string) => {
        const nodeboxOnInput = (nodebox.on as any).input

        if (!nodeboxOnInput) {
          throw Error('nodebox.on.input not defined')
        }
        if (nodeboxOnInput[port]) {
          // should then be created as portbox.
          const pb = createPortBox(
            nodeDefinition,
            nodeboxOnInput[port].toString(),
            ('__' + port + '__').toUpperCase()
          )
          ;(nodeDefinition.ports.input as any)[port].fn = pb.fn
          // State, only necessary when there are async..
          nodeDefinition.state = state
        }
      })
    }

    const preloadedNodeDefinition: PreloadedNodeDefinition = {
      ...nodeDefinition,
      on: {},
    }

    // Start/shutdown
    ;['start', 'shutdown'].forEach((key) => {
      if ((nodebox.on as any)[key]) {
        const pb = createPortBox(
          preloadedNodeDefinition,
          (nodebox.on as any)[key].toString(),
          ('__on' + key + '__').toUpperCase()
        )

        pb.compile()

        preloadedNodeDefinition.on[key] = pb.fn
      }
    })

    // NodeBox main()
    if (typeof nodebox.output === 'object' && nodebox.output.out) {
      // If it is an object we can just use that as the body.
      preloadedNodeDefinition.fn = nodebox.fn
    } else if (typeof nodebox.output === 'function') {
      // could change this to default and not use function at all.
      preloadedNodeDefinition.fn = nodebox.output
        .toString()
        .replace(/function.*{/, '')
        .replace(/}.*$/, '')
        .trim()
        // preferred is now to just use output()
        .replace(/cb\s*\(/g, 'output(')

      nodebox.clear() // reset

      preloadedNodeDefinition.fn = nodebox.compile(
        preloadedNodeDefinition.fn as string
      )
    }

    return preloadedNodeDefinition
  }
}
