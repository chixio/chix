import {NodeDefinition} from '@chix/common'
import {MockDependencyLoader} from '@chix/loader'
import {PortBox} from '../sandbox/port'

const mockDependencyLoader = new MockDependencyLoader()

export function createPortBox(
  nodeDefinition: NodeDefinition,
  fn: string,
  name: string
) {
  const portBox = new PortBox(name)

  portBox.set('state', nodeDefinition.state)
  portBox.set('output', {})

  if (nodeDefinition.dependencies && nodeDefinition.dependencies.npm) {
    portBox.addDependencies(
      nodeDefinition.dependencies.npm,
      mockDependencyLoader
    )
  }

  const strippedFunction = fn.slice(fn.indexOf('{') + 1, fn.lastIndexOf('}'))

  portBox.compile(strippedFunction)

  return portBox
}
