import {Context} from '@chix/common'
import {Flow} from '../flow/flow'
import {xNode} from '../node/node'

/**
 *
 * Default Context Provider
 *
 * @constructor
 * @public
 */

export class DefaultContextProvider {
  public addContext(node: xNode | Flow, defaultContext: Context, _schema: any) {
    if (typeof defaultContext !== 'undefined') {
      node.addContext(defaultContext)
    }
  }
}
