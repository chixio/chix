import {SourceConnectorSettings, TargetConnectorSettings} from '@chix/common'
import d from 'debug'
import {ConnectorEvents} from './events'
import {Link} from './link'
import {Packet} from './packet'
import {Setting} from './setting'

const debug = d.debug('chix:connector')

/**
 *
 * Connector
 *
 * The thing you plug into a port.
 *
 * Contains information about port and an optional
 * action to perform within the node (sub graph)
 *
 * Can also contains port specific settings.
 *
 * An xLink has a source and a target connector.
 *
 * ................... xLink ....................
 *
 *  -------------------.    .------------------
 * | Source Connector -------  Target Connector |
 *  ------------------'     `------------------
 *
 * When a link is plugged into a node, we do so
 * by plugging the target connector.
 *
 * @constructor
 * @public
 *
 */
export class Connector extends Setting {
  /**
   *
   * Create
   *
   * Creates a connector
   *
   * @param {String} id
   * @param {String} port
   * @param {Object} settings
   * @param {String} action
   */
  public static create(
    id: string,
    port: string,
    settings?: any,
    action?: string
  ) {
    const c = new Connector(settings)

    c.plug(id, port, action)

    return c
  }

  public action: string | undefined
  public id: string | null = null
  public pid: string | null = null
  public port: string | null = null
  public wire: Link | null = null
  public declare setting: SourceConnectorSettings | TargetConnectorSettings

  /**
   *
   * Plug
   *
   * @param {String} id
   * @param {String} port
   * @param {String} action
   */
  public plug(id: string, port: string, action?: string) {
    debug('plug %s:%s', id, port)
    this.id = id
    this.port = port

    if (action) {
      this.action = action
    }
  }

  /**
   * Write packet to this connector
   *
   * @param packet
   * @fires ConnectorEvents.DATA
   */
  public write(packet: Packet) {
    const index = this.get('index')

    debug(
      'write %s:%s %s',
      this.id,
      this.port,
      index === undefined ? '' : `[${index}]`
    )

    this.emit(ConnectorEvents.DATA, packet, this)
  }

  /**
   *
   * Register process id this connector handles.
   *
   */
  public setPid(pid: string) {
    this.pid = pid
  }

  public toJSON() {
    const json: {[key: string]: any} = {
      id: this.id,
      port: this.port,
      setting: undefined,
    }

    if (this.setting && Object.keys(this.setting).length > 0) {
      json.setting = JSON.parse(JSON.stringify(this.setting))
    }

    if (this.action) {
      json.action = this.action
    }

    return json
  }
}
