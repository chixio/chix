import {EventEmitter} from 'events'
import {SettingEvents} from './events'

export type SettingType = {[key: string]: any}

/**
 *
 * Setting
 *
 * Both used by Connector and xLink
 *
 * @constructor
 * @public
 *
 */
export class Setting extends EventEmitter {
  public setting: SettingType = {}

  constructor(settings?: SettingType) {
    super()

    if (settings) {
      this.configure(settings)
    }
  }

  public configure(settings: SettingType = {}) {
    for (const key in settings) {
      if (settings.hasOwnProperty(key)) {
        this.set(key, settings[key])
      }
    }
  }

  /**
   * Clears settings
   */
  public clearSettings(): void {
    this.setting = {}
  }

  /**
   *
   * Set
   *
   * Sets a setting
   *
   * @param {String} name
   * @param {any} val
   */
  public set(name: string, val: any) {
    if (undefined !== val) {
      this.setting[name] = val

      this.emit(SettingEvents.CHANGE, this, 'setting', this.setting)
    }
  }

  /**
   *
   * Get
   *
   * Returns the setting or undefined.
   *
   * @returns {any}
   */
  public get(name: string): any {
    return this.setting ? this.setting[name] : undefined
  }

  /**
   *
   * Delete a setting
   *
   * @returns {any}
   */
  public del(name: string): void {
    if (this.setting && this.setting.hasOwnProperty(name)) {
      delete this.setting[name]
    }
  }

  /**
   *
   * Check whether a setting is set.
   *
   * @returns {any}
   */
  public has(name: string): boolean {
    return this.setting && this.setting.hasOwnProperty(name)
  }
}
