export class IOEvents {
  /**
   * Send event.
   *
   * @event IO#Send
   * @type {object}
   * @property {id} id - graphId
   * @property {node} node - node which meta property was set
   */
  public static readonly SEND = 'send'

  /**
   * Connect event.
   *
   * @event IO#Connect
   * @type {Link} link - The connected link
   */
  public static readonly CONNECT = 'connect'

  /**
   * Disconnect event.
   *
   * @event IO#Connect
   * @type {Link} link - The disconnected link
   */
  public static readonly DISCONNECT = 'disconnect'

  /**
   * Drop event.
   *
   * @event IO#Drop
   * @type {Packet} p - The dropped packet
   */
  public static readonly DROP = 'drop'

  /**
   * Data event.
   *
   * @event IO#Data
   * @type {Any} data - Plain data contained within the packet
   */
  public static readonly DATA = 'data'

  /**
   * Packet event.
   *
   * @event IO#Packet
   * @type {Link} link - The Link
   * @type {Any} data - The packet
   */
  public static readonly PACKET = 'packet'

  /**
   * Receive event.
   *
   * @event IO#Receive
   * @type {Link} link - The Link which just received data
   */
  public static readonly RECEIVE = 'receive'
}
