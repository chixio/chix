/**
 * If expose is `true` the event will also be send out using an output port
 * In such a case the port/event name is prefixed with a ':'
 */
import {xEvents} from '../types'

export const NodeEvents: xEvents = {
  /**
   * Started event.
   *
   * @event Node#started
   * @type {object}
   * @property {node} node - Node being stopped
   */
  STARTED: {name: 'started'},

  /**
   * Complete event.
   *
   * @event Node#complete
   * @type {object}
   * @property {node} node - Node being stopped
   */
  COMPLETE: {name: 'complete', expose: true},

  /**
   * Shutdown event.
   *
   * @event Node#shutdown
   * @type {object}
   * @property {node} node - Node being stopped
   */
  SHUTDOWN: {name: 'shutdown', expose: true},

  /**
   * Start event.
   *
   * @event Node#start
   * @type {object}
   * @property {node} node - Node being stopped
   */
  START: {name: 'start', expose: true},

  /**
   * Stop event.
   *
   * @event Node#stop
   * @type {object}
   * @property {node} node - Node being stopped
   */
  STOP: {name: 'stop', expose: true},

  /**
   * Status Update event.
   *
   * @event Node#stop
   * @type {object}
   * @property {node} node - Node being stopped
   */
  STATUS_UPDATE: {name: 'statusUpdate', expose: true},

  /**
   * Execute event.
   *
   * @event Node#execute
   * @type {object}
   * @property {node} node - Node being stopped
   */
  EXECUTE: {name: 'execute'},

  /**
   * Executed event.
   *
   * @event Node#executed
   * @type {object}
   * @property {node} node - Node being stopped
   */
  EXECUTED: {name: 'executed'},

  /**
   * Portfill event.
   *
   * @event Node#portfill
   * @type {object}
   * @property {node} node - Node being stopped
   */
  PORT_FILL: {name: 'portFill'},

  /**
   * Error event.
   *
   * @event Node#error
   * @type {object}
   * @property {node} node - Node being stopped
   */
  ERROR: {name: 'error', expose: true},

  /**
   * Output event.
   *
   * @event Node#output
   * @type {object}
   * @property {node} node - Node being stopped
   */
  OUTPUT: {name: 'output'},

  /**
   * Context update event.
   *
   * @event Node#contextUpdate
   * @type {object}
   * @property {node} node - Node being stopped
   */
  CONTEXT_UPDATE: {name: 'contextUpdate', expose: true},

  /**
   * Context clear event.
   *
   * @event Node#contextClear
   * @type {object}
   * @property {node} node - Node being stopped
   */
  CONTEXT_CLEAR: {name: 'contextClear', expose: true},

  /**
   * Timeout event.
   *
   * @event Node#nodeTimeout
   * @type {object}
   * @property {node} node - Node being stopped
   */
  TIMEOUT: {name: 'nodeTimeout', expose: true},
}
