import {xEvents} from '../types'

export const FlowEvents: xEvents = {
  /**
   * Start event.
   *
   * @event Flow#start
   * @type {object}
   * @property {node} node - Node being started
   */
  START: {name: 'start'},

  /**
   * Stop event.
   *
   * @event Flow#stop
   * @type {object}
   * @property {node} node - Node being stopped
   */
  STOP: {name: 'stop'},

  /**
   * Metadata event.
   *
   * @event Flow#metadata
   * @type {object}
   * @property {id} id - graphId
   * @property {node} node - node which meta property was set
   */
  METADATA: {name: 'metadata'},

  /**
   * Add IIP event.
   *
   * @event Flow#addIIP
   * @type {Link} Link - IIP Link which was added
   */
  ADD_IIP: {name: 'addIIP'},

  /**
   * Remove IIP event.
   *
   * @event Flow#removeIIP
   * @type {Link} oldLink - IIP Link which was removed
   */
  REMOVE_IIP: {name: 'removeIIP'},

  /**
   * Add Link event.
   *
   * @event Flow#addLink
   * @type {Link} link - Link which was added
   */
  ADD_LINK: {name: 'addLink'},

  /**
   * Change Link event.
   *
   * @event Flow#changeLink
   * @type {Link} link - Link which was changed
   */
  CHANGE_LINK: {name: 'changeLink'},

  /**
   * Remove Link event.
   *
   * @event Flow#removeLink
   * @type {Link} oldLink - Link which was removed
   */
  REMOVE_LINK: {name: 'removeLink'},

  /**
   * Add node event.
   *
   * @event Flow#addNode
   * @type {Object}
   * @property {Node} node - Node which was added
   */
  ADD_NODE: {name: 'addNode'},

  /**
   * Remove node event.
   *
   * @event Flow#removeNode
   * @type {Object}
   * @property {Node} oldNode - Node which was removed
   */
  REMOVE_NODE: {name: 'removeNode'},

  /**
   * Add Port event.
   *
   * @event Flow#addPort
   * @type {Object}
   * @property {Node} node - Node to which port was added
   * @property {String} port - Port name which was added
   * @property {String} nodeId - Internal nodeId
   * @property {String} name - Internal Port name
   * @property {String} type - Type input|output
   */
  ADD_PORT: {name: 'addPort'},

  /**
   * Remove Port event.
   *
   * @event Flow#removePort
   * @type {Object}
   * @property {Node} node - Node to which port was removed
   * @property {String} port - Port name which was removed
   * @property {String} type - Type input|output
   */
  REMOVE_PORT: {name: 'removePort'},

  /**
   * Rename Port event.
   *
   * @event Flow#renamePort
   * @type {Object}
   * @property {Node} node - Node which port was changed
   * @property {String} from - Old port name
   * @property {String} to - New port name
   */
  RENAME_PORT: {name: 'renamePort'},

  /**
   * Port Output event.
   *
   * @event Flow#output
   * @type {Object}
   * @property {Node} node - Node
   * @property {String} port - port name
   * @property {String} out - Output packet
   * @property {String} action - Optional current action
   */
  OUTPUT: {name: 'output'},

  /**
   * Port Output event.
   *
   * @event Flow#error
   * @type {Object}
   * @property {Node} node - Node
   * @property {Error} msg - The Error
   */
  ERROR: {name: 'error', expose: true},
}
