export class SettingEvents {
  /**
   * Change event
   *
   * @event Setting#change
   * @type {Object}
   */
  public static readonly CHANGE = 'change'
}
