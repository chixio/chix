export class ConnectorEvents {
  /**
   * Data event
   *
   * @event Connector#data
   * @type {Packet}
   */
  public static readonly DATA = 'data'
}
