export class PMEvents {
  /**
   * Add Process event
   *
   * @event Process#addProcess
   * @type {object}
   * @property {node} node - Added Process
   */
  public static readonly ADD_PROCESS = 'addProcess'

  /**
   * Remove Process event
   *
   * @event Process#removeProcess
   * @type {object}
   * @property {node} node - Removed Process
   */
  public static readonly REMOVE_PROCESS = 'removeProcess'

  /**
   * Start Process event
   *
   * @event Process#startProcess
   * @type {object}
   * @property {node} node - Process to be started
   */
  public static readonly START_PROCESS = 'startProcess'

  /**
   * Stop Process event
   *
   * @event Process#stopProcess
   * @type {object}
   * @property {node} node - Stopped Process
   */
  public static readonly STOP_PROCESS = 'stopProcess'

  /**
   * Process Status event
   *
   * @event Process#processStatus
   * @type {object}
   * @property {node} node - Process Status
   */
  public static readonly PROCESS_STATUS = 'processStatus'

  /**
   * Error event
   *
   * @event Process#error
   * @type {object}
   * @property {node} node - The errored process
   */
  public static readonly ERROR = 'error'

  /**
   * Change Pid event
   *
   * @event Process#changePid
   * @type {object}
   * @property {node} node - Process which pid was changed
   */
  public static readonly CHANGE_PID = 'changePid'

  /**
   * Report event
   *
   * @event Process#report
   * @type {object}
   * @property {Object} report - Object containing report
   */
  public static readonly REPORT = 'report'
}
