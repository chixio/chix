# Events




### Connector

Event: 'data'
  - p <Packet> The packet being written
  - connector <Connector> The connector itself

Emitted when data is written to a connector.

### Flow

Event: 'start'
  - event <Object> The node started

Object containing 'node' as property
Emitted when a node is started.

Event: 'stop'
  - event <Object> The node stopped

Object containing 'node' as property
Emitted when a node is stopped.



### IO

### Link

### Node

### PM

### Port

### Setting
