export class LinkEvents {
  /**
   * Change event
   *
   * @event Link#change
   * @type {object}
   */
  public static readonly CHANGE = 'change'

  /**
   * Clear event
   *
   * @event Link#clear
   * @type {object}
   */
  public static readonly CLEAR: 'clear'

  /**
   * Error event
   *
   * @event Link#error
   * @type {object}
   */
  public static readonly ERROR: {name: 'error'; expose: true}
}
