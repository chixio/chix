import {
  AsyncPort as AsyncPortDefinition,
  Port as PortDefinition,
} from '@chix/common'
import {AsyncInputPort} from './asyncInput'
import {InputPort} from './input'
import {OutputPort} from './output'

export type PortTypes = AsyncInputPort | InputPort | OutputPort

export function portFactory(type: string, portDefinition: PortDefinition) {
  // TODO: async can actually be an object port.
  if (type === 'input') {
    if (portDefinition.fn) {
      return new AsyncInputPort(portDefinition as AsyncPortDefinition)
    }

    return new InputPort(portDefinition)
  }

  return new OutputPort(portDefinition)
}
