import {Flow} from '../../flow'
import {xNode} from '../../node'
import {defaulter} from './defaulter'

/**
 *  Packet
 * - fill with persist, done
 * - fill with context, done
 * - fill with defaults
 *  Object:
 *  - enrich with defaults
 *
 * defaults -> context
 * defaults -> persist
 * defaults -> input
 *
 * @param {Node} node The node
 */
export function fill(node: xNode | Flow) {
  // const {ports: {input: ports}} = node
  const ports = node.ports.input

  // For every non-connected port fill the defaults
  for (const port in ports) {
    if (ports.hasOwnProperty(port)) {
      const inputPort = node.getInputPort(port)

      if (inputPort.hasConnections() && !inputPort.persist) {
        // mainly to not fill async ports with connects
        // but do have defaults
        // nop
      } else {
        // seems the return is not correct no more also.
        // const ret = defaulter(
        defaulter(
          node,
          port
          // []
        )

        // TODO: Is not using the report from defaulter anymore, check if that's correct

        /* Is not determined by error anymore but port state.
        if (<Error>ret instanceof Error) {
          return ret
        }
        */
      }
    }
  }

  return true
}
