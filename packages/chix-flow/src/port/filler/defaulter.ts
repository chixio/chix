import {Port} from '../port'
import {check} from './check'

/**
 *
 * @param {xNode} node
 * @param {String} port
 * @private
 */
// TODO: recheck this
export function defaulter(node: any, port: string /*, path*/) {
  const ret = check(
    node,
    node.ports.input,
    port,
    node.input,
    node.context,
    node.persist
    // path
  )

  if (!ret && !node.ports.input[port].async) {
    if (port[0] !== ':') {
      return Port.SYNC_PORTS_UNFULFILLED
      /*
        // fail hard
        return Error(util.format(
          '%s: Cannot determine input for port `%s`',
          node.identifier,
          port
        ))
      */
    }
  }

  return ret
}
