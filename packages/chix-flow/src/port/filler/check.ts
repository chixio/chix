import {xNode} from '../../node'
import {Packet} from '../../packet'
import {Port} from '../port'
import {checkProperties} from './checkProperties'

/**
 *
 * @param {xNode} node Node
 * @param {Object} schema Current object schema
 * @param {String} port Port
 * @param {Object} input the input to be filled
 * @param {Object} context Current level context
 * @param {Object} persist Current level persist
 */
export function check(
  node: xNode,
  schema: any,
  port: string,
  input: any,
  context: any,
  persist: any
): number {
  let ret

  // check whether input was defined for this port
  if (!input.hasOwnProperty(port)) {
    // This will not really work for persisted indexes
    // or at least it should check whether the array is full after
    // this fill
    ret = Port.NOT_FILLED

    if (persist && persist.hasOwnProperty(port)) {
      input[port] = persist[port].clone(node)

      ret = Port.PERSISTED_SET
    } else if (context && context.hasOwnProperty(port)) {
      // if there is context, use that.
      input[port] = context[port].clone(node)

      ret = Port.CONTEXT_SET
      // check the existence of default (a value of null is also valid)
    } else if (schema[port].hasOwnProperty('default')) {
      input[port] = Packet.create(
        schema[port].default,
        node.getPortType('input', port)
      ).setOwner(node)

      ret = Port.DEFAULT_SET
    } else if (schema[port].required === false) {
      // filled with packet, but the value is undefined.
      input[port] = Packet.create(
        undefined,
        node.getPortType('input', port)
      ).setOwner(node)

      ret = Port.NOT_REQUIRED
    }

    // if it's an object, fill in defaults
    if (schema[port].properties) {
      //
      let init
      let obj = {}

      // initialize packet on the first level
      if (!input[port]) {
        init = true
        input[port] = Packet.create(obj, 'object').setOwner(node)
      } else {
        obj = input[port].read(node) || {}
      }

      checkProperties(node, schema[port], obj, port)

      // TODO: check ret value correctness
      if (!Object.keys(obj).length && init) {
        // remove empty packet again
        delete input[port]
        ret = Port.NOT_FILLED
      } else {
        ret = Port.FILLED
      }
    }

    if (!ret) {
      throw Error('Could not determine port state.')
    }

    return ret
  }

  return Port.FILLED
}
