import {xNode} from '../../node'

/**
 * Fills properties with defaults
 *
 * Note how context and persist make no sense here...
 * It's already filled.
 *
 * @param {xNode} node Node
 * @param {Object} schema Current object schema
 * @param {Object} input the input to be filled
 * @param {String} port The port
 */

export function checkProperties(
  node: xNode,
  schema: any,
  input: any,
  port: string
): void {
  for (const prop in schema.properties) {
    if (schema.properties.hasOwnProperty(prop)) {
      const property = schema.properties[prop]

      // check the existence of default (a value of null is also valid)
      if (!input.hasOwnProperty(prop)) {
        if (property.hasOwnProperty('default')) {
          input[prop] = property.default
        } else if (property.required === false) {
          // filled with packet, but the value is undefined.
          // input[key] = null; // undefined not possible at this level.
          input[prop] = undefined
        } else {
          if (property.type === 'object') {
            if (property.hasOwnProperty('properties')) {
              if (!input.hasOwnProperty(prop)) {
                // always fill with empty object?
                input[prop] = {}
              }
              return checkProperties(node, property, input[prop], port)
            }
          }
          // fail check
          // return or throw?
          // return throw Error(util.format(
          throw Error(
            `${node.identifier}: Cannot determine input for port \`${port}\``
          )
        }
      }
    }
  }

  return
}
