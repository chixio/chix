import {PacketContainer} from '../packet'
import {AsyncInputPort} from './asyncInput'

/**
 *
 * @param {PacketContainer} pc PacketContainer
 * @private
 */
export function runPortBox(
  this: AsyncInputPort,
  packetContainer: PacketContainer
) {
  // setup is done elsewhere, should be done over here
  const sb = this.fn

  /*
   this.event(':start', {
   node: this.export()
   })
   */
  sb.set('$', packetContainer)

  // this is the nodes state, guess there should be a setState() thing
  sb.set('state', this.state)

  // this.setStatus('running')

  // NOTE: (this) has changed now also, but I think it's not used anyway,
  //       it is what is bound to the function
  const ret = sb.run(this)

  // this.nodebox.state = this.state = sb.state
  this.state = sb.state

  if (ret === false) {
    throw Error('Returning false from sandbox deprecated')
  }

  this.runCount++
}
