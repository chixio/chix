import {Port as PortDefinition} from '@chix/common'
import {forOf} from '@fbpx/lib'
import {EventEmitter} from 'events'
import {$Parent} from '../common'
import {Connector} from '../connector'
import {Link} from '../link'
import {Packet} from '../packet'
import {Constructor} from '../types'
import {mixin} from '../util'
import {validate} from '../validate'

export type PortType<T> = $Parent<T>

function PortMix<T>(Base: T): Constructor<PortType<T>> & T {
  return mixin($Parent)(Base)
}

/**
 * Port
 *
 * Distinct between input & output port
 * Most methods are for input ports.
 *
 * @example
 *
 *  const port = new Port()
 *  port.receive(p)
 *  port.receive(p); // queued (arrayed)
 *  port.read(); // must test whether it's filled, will sweep along all ports
 *  port.read() // and actually takes the value if the condition is met for *all* ports
 *  port.receive(p)
 *
 *  @param {Object} def Port Definitions
 *  @param {Boolean} def.required whether this port requires input
 *  @param {any} def.default default value for this port
 *  @param {Boolean|Array} def.persist whether incoming values should persist
 *                                     If value is an array it corresponds to an index
 *                                     position within the array port.
 *  @param {Object} def.settings The port settings
 *  @param {Object} def.settings The port settings
 */
export abstract class Port extends PortMix(EventEmitter) {
  public static EMPTY = 0
  public static FILLED = 1
  public static PERSIST = 2
  public static DIRECT = 3
  public static CONTEXT = 4
  public static DEFAULT = 5

  // TODO: these were missing.
  public static NOT_REQUIRED = 6
  public static NOT_FILLED = 7
  public static PERSISTED_SET = 8
  public static CONTEXT_SET = 9
  public static DEFAULT_SET = 10
  public static SYNC_PORTS_UNFULFILLED = 11

  public static Message = [
    'not filled',
    'connection filled',
    'connection persisted',
    'direct fill',
    'context',
    'default',
    'required',
  ]

  // portFactory creates cyclic reference, use portFactory directly.
  /*
  public static create(type: string, name: string, portDefinition: PortDefinition) {
   console.log('portDefinition?', portDefinition)
    return portFactory(type, {
      ...portDefinition,
      name
    })
  }
  */

  public name: string = ''
  public fn: any
  public indexed = false
  public async: boolean
  public _open = false

  public type: string
  public args: any[] = []

  public fills = 0
  public reads = 0
  public runCount = 0

  public _connections: Link[] = []
  public portDefinition: PortDefinition

  protected constructor(portDefinition: PortDefinition) {
    super()

    /*
    for (const key in portDefinition) {
      if (portDefinition.hasOwnProperty(key)) {
        (this as any)[key]  = (portDefinition as any)[key]
      }
    }
    */
    this.portDefinition = portDefinition

    Object.assign(this, portDefinition)

    if (!this.name) {
      console.log(portDefinition)

      throw Error('Port name is required')
    }

    if (!(this as any).type) {
      console.log(portDefinition)

      throw Error('Port type is required')
    }

    this.async = !!this.fn
  }

  /**
   * Whether this port is open
   *
   * @returns {boolean}
   */
  public isOpen(): boolean {
    return this._open
  }

  /**
   * Whether this port is closed
   *
   * @returns {boolean}
   */
  public isClosed(): boolean {
    return !this._open
  }

  /**
   * Open port
   */
  public open(): void {
    this._open = true
  }

  /**
   *  Reports whether we can provide a next packet
   */
  // public abstract filled (): boolean

  public abstract read(): Packet

  /**
   *
   * Used from within a component to close the port
   *
   * A component receives an open port.
   * When the port closes it's ready to be filled.
   * This also means there are two sides on a port
   * Open for input and open for output to the component.
   *
   */
  public close(): void {
    this._open = false
  }

  public abstract connect(link: Link): void

  public abstract plug(connector: Connector): void

  public abstract disconnect(link: Link): void

  public abstract unplug(connector: Connector): void

  /**
   * Test whether the link is connected to this port
   *
   * @param link
   * @returns {boolean}
   */
  public hasConnection(link: Link): boolean {
    return this._connections && this._connections.indexOf(link) >= 0
  }

  /**
   * Whether this port has any connections
   *
   * @returns {boolean}
   */
  public hasConnections(): boolean {
    return this._connections.length > 0
  }

  /**
   * Get all connections
   *
   * @returns {boolean}
   */
  public getConnections(): Link[] {
    return this._connections
  }

  public isFilled(): void {
    throw Error('method not implemented')
  }

  /**
   * Reset this port
   * Sets `fills`, `reads` and `runCount` to zero.
   *
   * Also closes the port again.
   */
  public reset(): void {
    this.fills = 0
    this.reads = 0
    this.runCount = 0

    this.close()
  }

  public isAvailable(): void {}

  /**
   * Get Port Option
   *
   * @param opt
   * @returns {*}
   */
  public getOption(opt: string): any {
    if (this.hasOwnProperty(opt)) {
      return (this as any)[opt]
    } else {
      return undefined
    }
  }

  /**
   *
   * Sets an input port option.
   *
   * The node schema for instance can specify whether a port is persistent.
   *
   * At the moment a connection can override these values.
   * It's a way of saying I give you this once so take care of it.
   *
   */
  public setOption(opt: string, value: any): void {
    ;(this as any)[opt] = value
  }

  /**
   *
   * TODO: implementation was incorrect, started as all ports level, check this.
   * Seems to have never surfaced because this method is not used..
   *
   * @param {Object} options
   */
  public setOptions(options: {[key: string]: any}): any {
    forOf((opt: string, val: any) => this.setOption(opt, val), options)
  }

  public destroy(): void {
    throw Error('method destroy() not implemented')
  }

  /**
   * convert to function if it's not already a function
   *
   * @param {Packet} packet
   * @private
   */
  public _handleFunctionType(packet: Packet): void {
    if (this.type === 'function') {
      const data = packet.read(this)

      if (data !== null) {
        const type = typeof data

        if (type === 'string') {
          // args, can be used as a hint for named params
          // if there are no arg names defined, use arguments
          const args = this.args ? this.args : []

          // tslint:disable-next-line:function-constructor
          packet.write(this, new Function(...args, data))
        } else {
          if (type !== 'function' && type !== 'object') {
            const node = this.getParent()

            throw Error(
              `${node.identifier}: port ${this.name} expects a function`
            )
          }
        }
      }
    }
  }

  public _validatePacket(packet: Packet, index?: string | number): boolean {
    const data = packet.read(this)

    if (index !== undefined) {
      // fix this, def should be properly set for port, not only this.type
      return true
    }
    // allow null always
    if (data !== null) {
      if (!validate.data(this.type, data)) {
        // @ts-ignore
        let real = Object.prototype.toString.call(data).match(/\s(\w+)/)[1]

        if (
          data &&
          typeof data === 'object' &&
          data.constructor.name === 'Object'
        ) {
          const tmp = Object.getPrototypeOf(data).constructor.name

          if (tmp) {
            real = tmp
          }
        }

        // improve this error handling, emit the error.
        // TODO: move that is connected check to this.error()
        // see sendPortOutput()
        this.emit(
          'error',
          Error(`Expected '${this.type}' got '${real}' on port '${this.name}'`)
        )
      }
    }

    return true
  }

  public createPacketFromValue(packet: any): Packet {
    let type

    if (!this.type) {
      type = typeof packet
    } else if (this.type === 'enum') {
      type = 'string'
      // TODO: check if value is within enum values.
    } else {
      type = this.type
    }

    return Packet.create(packet, type)
  }

  public toJSON() {
    return this.portDefinition
  }
}
