import {Port as PortDefinition, TargetConnectorSettings} from '@chix/common'
import d from 'debug'
import {Connector} from '../connector'
import {ConnectorEvents, PortEvents} from '../events'
import {Link} from '../link'
import {Packet} from '../packet/'
import {Port} from './port'

const debug = d.debug('chix:inputPort')

export type IndexedDataArray = any[]
export type IndexedDataObject = {[index: string]: any}

// indexed ports are an array
// export type Persist = boolean | number[] | string[]
export type Persist = any | any[]

export class InputPort extends Port {
  public data: Packet[] = []
  public context?: Packet
  public default?: Packet
  public required: boolean = true

  private __persist?: Packet
  private _isCycling: boolean = false
  private _persist: boolean

  constructor(portDefinition: PortDefinition) {
    super(portDefinition)

    this.receiveListener = this.receiveListener.bind(this)

    if (portDefinition) {
      if (portDefinition.hasOwnProperty('default')) {
        this.setDefault(portDefinition.default)
      }

      if (portDefinition.context) {
        this.setContext(portDefinition.context)
      }

      if (portDefinition.required === false) {
        this.required = false
      }
    }

    this.reset()
  }

  public get persist() {
    return this._persist
  }

  public set persist(persist: boolean) {
    this._persist = persist
  }

  public receiveListener(packet: Packet, target: Connector) {
    this.receive(packet, target.get('index'), target)
  }

  public close() {
    super.close()
    debug('close %s port', this.name)
  }

  public isSync() {
    return true
  }

  public isAsync() {
    return false
  }

  public cycle(packet: Packet, index?: string | number, target?: Connector) {
    const values = packet.read(this)

    this._isCycling = true

    if (Array.isArray(values) && this.type !== 'array') {
      for (const value of values) {
        this.receive(packet.clone(this).write(this, value), index, target)
      }
    }

    this._isCycling = false
  }

  /**
   * Receive a packet
   *
   * Packets are always accepted, unless the data is non-consumable.
   * In which case there will be an error state and everything halts.
   *
   */
  public receive(
    value: Packet | any,
    index?: number | string,
    target?: Connector
  ) {
    const packet = Packet.isPacket(value)
      ? value
      : this.createPacketFromValue(value)

    packet.setOwner(this)

    if (!this._isCycling && target && target.has('cyclic')) {
      this.cycle(packet, index, target)
      return this
    }

    if (this._validatePacket(packet, index)) {
      if (index === undefined) {
        this.fill(packet, target)
      } else {
        this.indexed = true
        this.fillIndex(packet, index, target)
      }
    }

    return this
  }

  public fill(packet: any, target?: Connector) {
    if (this.hasParent()) {
      const _node = this.getParent()

      debug(
        'fill -> (%s) %s',
        this.name,
        _node ? _node.title || _node.name : ''
      )
    }

    // how to get the link back and thus the index?
    if (!Packet.isPacket(packet)) {
      packet = this.createPacketFromValue(packet).setOwner(this)
    }

    // if the persist is based on the connection, persist should be on that connection.
    if (
      this.persist ||
      (target && (target.setting as TargetConnectorSettings).persist)
    ) {
      this.__persist = packet.clone(this)
    }

    this._handleFunctionType(packet)

    if (this._validatePacket(packet)) {
      this.data.push(packet)
      this.fills++

      this.emit(PortEvents.FILL)
    }

    return this
  }

  public fillIndex(packet: Packet, index: string | number, target?: Connector) {
    if (this.hasParent()) {
      const _node = this.getParent()

      debug(
        'fill -> [%s] (%s) %s',
        index,
        this.name,
        _node ? _node.title || _node.name : ''
      )
    }

    for (const packetData of this.data) {
      // test whether our position is free
      const value = packetData.read(this)

      if (value[index] === undefined) {
        value[index] = packet.read(this)

        Packet.metaMerge(packetData._meta, packet._meta)

        this.emit(PortEvents.FILL)

        return
      }
    }

    // could not fill push a new packet on the queue
    let data: IndexedDataArray | IndexedDataObject

    if (this.type === 'object') {
      data = {[index]: packet.read(this)}
    } else if (this.type === 'array') {
      data = [] as IndexedDataArray
      ;(data as any)[index] = packet.read(this)
    } else {
      throw Error('index on non object/array port')
    }

    this.fill(this.createPacketFromValue(data).setOwner(this), target)
  }

  public reset() {
    this.clearInput()
    super.reset()
    return this
  }

  get filled() {
    return this.isFilled() !== Port.EMPTY
  }

  public isFilled() {
    if (this.hasConnections()) {
      if (this.indexed) {
        if (this.data.length) {
          const data = this.data[0].read(this)

          // todo should be able to set deep properties.
          if (typeof data === 'object' && this.type === 'object') {
            if (Object.keys(data).length !== this._connections.length) {
              return Port.EMPTY
            }
          } else if (Array.isArray(data) && this.type === 'array') {
            if (
              this._connections.length &&
              data.length !== this._connections.length
            ) {
              return Port.EMPTY
            }
            for (const packet of data) {
              if (packet === undefined) {
                return Port.EMPTY
              }
            }
          } else {
            throw Error('non array/object port cannot be indexed')
          }
        }
      }
      if (this.data.length) {
        return Port.FILLED
      } else if (this.__persist) {
        return Port.PERSIST
      }
    } else if (this.data.length) {
      if (this.indexed) {
        const data = this.data[0].read(this)
        for (const packet of data) {
          if (packet === undefined) {
            return Port.EMPTY
          }
        }
      }
      return Port.DIRECT
    } else if (this.context) {
      return Port.CONTEXT
    } else if (this.default) {
      return Port.DEFAULT
    } else if (this.required === false) {
      return Port.NOT_REQUIRED
    }
    return Port.EMPTY
  }

  public isRequired() {
    return Boolean(this.required)
  }

  /**
   * Reads the first item
   * before reading first test with isFilled()
   */
  public read(): Packet {
    // this.indexed = false

    if (this.hasConnections()) {
      if (this.data.length) {
        this.indexed = false // new input will toggle it again
        this.reads++
        return (this.data.shift() as Packet).release(this)
      } else if (this.__persist) {
        this.reads++
        return this.__persist.clone(this).release(this)
      }
      throw Error(
        [
          'Unable to read port data:',
          `\tPort '${this.name}' is connected yet the data queue is empty and persist is not set`,
          '\tEnsure port can provide data before reading from it.',
        ].join('\n')
      )
    } else if (this.data.length) {
      this.reads++
      return (this.data.shift() as Packet).release(this)
    } else if (this.context) {
      this.reads++
      return this.context.clone(this).release(this)
    } else if (this.default) {
      this.reads++
      return this.default.clone(this).release(this)
    } else if (!this.required) {
      return new Packet(undefined)
    }

    throw Error(
      [
        'Unable to read port data:',
        `\tPort '${this.name}' is required yet has no context nor default and the data queue is empty`,
        '\tEnsure port can provide data before reading from it.',
      ].join('\n')
    )
  }

  public peek(): Packet {
    if (this.hasConnections()) {
      if (this.data.length) {
        this.indexed = false // new input will toggle it again
        return (this.data[0] as Packet).release(this)
      } else if (this.__persist) {
        return this.__persist.clone(this).release(this)
      }
    } else if (this.data.length) {
      return (this.data[0] as Packet).release(this)
    } else if (this.context) {
      return this.context.clone(this).release(this)
    } else if (this.default) {
      return this.default.clone(this).release(this)
    } else if (!this.required) {
      return new Packet(undefined)
    }
    throw Error('Unable to read port data')
  }

  // TODO: emit the right event, right now IO handler is doing this I think
  public connect(link: Link) {
    if (!this.hasConnection(link)) {
      this.open()
      debug('Connecting link')
      link.target.on(ConnectorEvents.DATA, this.receiveListener)
      this._connections.push(link)
    } else {
      console.log('Link already connected')
    }
    return this
  }

  public plug(target: Connector): InputPort {
    if (target.wire && !this.hasConnection(target.wire)) {
      this.open()
      const index = target.get('index')
      if (index !== undefined && this.type === 'any') {
        this.type = isNaN(parseInt(index, 10)) ? 'object' : 'array'
      }
      debug(
        'plug %s -> %s(%s)',
        target.wire.source.port,
        index === undefined ? '' : `[${index}] `,
        target.port
      )
      target.on(ConnectorEvents.DATA, this.receiveListener)
      /*
      if (target.setting) {
        // shouldn't overwrite options, test options both on port and optionally the connection
        // this.setOptions(target.setting)
      }
      */
      this._connections.push(target.wire)
    } else {
      console.log('Link already connected')
    }
    return this
  }

  public disconnect(link: Link) {
    if (this.hasConnection(link)) {
      if (this._connections.length === 1) {
        this.close()
        if (this.__persist) {
          this.__persist = undefined
        }
      }
      link.target.removeListener(ConnectorEvents.DATA, this.receiveListener)
      this._connections.splice(this._connections.indexOf(link), 1)
    } else {
      console.log('Link already disconnected')
    }
  }

  public unplug(target: Connector): void {
    if (target.wire && this.hasConnection(target.wire)) {
      const index = target.get('index')
      if (index !== undefined && this.type === 'any') {
        this.type = isNaN(parseInt(index, 10)) ? 'object' : 'array'
      }
      debug(
        'unplug %s -> %s(%s)',
        target.wire.source.port,
        index === undefined ? '' : `[${index}] `,
        target.port
      )
      if (this._connections.length === 1) {
        this.close()
        if (this.__persist) {
          this.__persist = undefined
        }
      }
      target.removeListener(ConnectorEvents.DATA, this.receiveListener)
      this._connections.splice(this._connections.indexOf(target.wire), 1)
    } else {
      console.log('Link already disconnected')
    }
  }

  public clearInput() {
    this.data = []
    return this
  }

  public setDefault(val: any) {
    const p = Packet.isPacket(val) ? val : this.createPacketFromValue(val)

    this._handleFunctionType(p)

    if (this._validatePacket(p)) {
      this.default = p
    }

    return this
  }

  public hasDefault() {
    return this.default !== undefined
  }

  public clearDefault() {
    this.default = undefined
    return this
  }

  // TODO: set indexed default
  public setContext(value: any, trigger?: boolean) {
    // OK, value is already a packet yet it contains undefined.
    if (value === undefined) {
      throw Error('Refuse to set context to undefined')
    }

    const packet = Packet.isPacket(value)
      ? value
      : this.createPacketFromValue(value)

    this._handleFunctionType(packet)

    if (this._validatePacket(packet)) {
      this.context = packet

      if (trigger) {
        this.emit(PortEvents.FILL)
      }
    }

    return this
  }

  public hasContext() {
    return this.context !== undefined
  }

  public clearContext() {
    this.context = undefined
    return this
  }

  public setPersist(value: Persist) {
    this.persist = value

    return this
  }

  public hasPersist(): boolean {
    return this.__persist !== undefined
  }

  public clearPersist() {
    this.__persist = undefined
    return this
  }

  public destroy() {
    this.clearContext()
    this.clearPersist()
    this.clearInput()
  }
}
