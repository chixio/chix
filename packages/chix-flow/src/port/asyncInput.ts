import {AsyncPort as AsyncPortDefinition} from '@chix/common'
import {PacketContainer} from '../packet'
import {InputPort} from './input'
import {runPortBox} from './runPortBox'

export class AsyncInputPort extends InputPort {
  public state: any

  constructor(portDefinition: AsyncPortDefinition) {
    super(portDefinition)

    if (portDefinition.hasOwnProperty('default')) {
      this.setDefault(portDefinition.default)
    }

    if (portDefinition.context) {
      this.setContext(portDefinition.context)
    }

    if (!portDefinition.fn) {
      throw Error('AsyncInputPort requires a function')
    }

    this.reset()
  }

  public isSync(): boolean {
    return false
  }

  public isAsync(): boolean {
    return true
  }

  public setState(state: any): void {
    this.state = state
  }

  /**
   *
   * @param {Packets[]} params Array of param/input Packets
   * @param {Object} state Object
   */
  public run(params: PacketContainer, state: any): void {
    this.setState(state)

    runPortBox.call(this, params)
  }
}
