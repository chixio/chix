import {ExternalPort} from '@chix/common'
import {PortEvents} from '../../events'
import {Packet} from '../../packet'
import {OutputPort} from '../output'
import {externalToInternalFilter} from './util'

export class ExternalOutputPort extends OutputPort {
  public nodeId: string
  public _port: OutputPort

  constructor(
    externalPort: ExternalPort,
    internalPort: OutputPort | ExternalOutputPort
  ) {
    super(externalToInternalFilter(externalPort, internalPort))

    this.nodeId = externalPort.nodeId
    this.name = externalPort.name
    this._port = internalPort

    internalPort.on(PortEvents.DATA, (packet: Packet) => this.write(packet))
  }
}
