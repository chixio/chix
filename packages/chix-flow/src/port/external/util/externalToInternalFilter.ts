import {ExternalPort, Port as InputPortDefinition} from '@chix/common'
import _ from 'lodash'

export function externalToInternalFilter(
  externalPort: ExternalPort,
  internalPort: any
): InputPortDefinition {
  return {
    type: internalPort.type,
    ..._.omit(externalPort, ['nodeId']),
  } as InputPortDefinition
}
