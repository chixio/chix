import {ExternalPort} from '@chix/common'
import {Connector} from '../../connector'
import {Link} from '../../link'
import {Packet} from '../../packet'
import {InputPort, Persist} from '../input'
import {externalToInternalFilter} from './util'

export class ExternalInputPort extends InputPort {
  public nodeId: string
  public _port: InputPort | ExternalInputPort
  constructor(
    externalPort: ExternalPort,
    internalPort: InputPort | ExternalInputPort
  ) {
    super(externalToInternalFilter(externalPort, internalPort))
    this.nodeId = externalPort.nodeId
    this.name = externalPort.name
    this._port = internalPort

    if (externalPort) {
      if (externalPort.hasOwnProperty('default')) {
        this.setDefault(externalPort.default)
      }

      if (externalPort.context) {
        this.setContext(externalPort.context)
      }
    }
  }

  public isSync(): boolean {
    return this._port.isSync()
  }

  public receive(packet: Packet | any, index?: number | string) {
    this._port.receive(packet, index)

    return this
    // throw Error('Receive is bypassed')
  }

  public fill(_packet: Packet | any) {
    console.error('Fill is bypassed for external input port')

    return this
  }

  public isFilled(): number {
    return this._port.isFilled()
  }

  public isRequired(): boolean {
    return this._port.isRequired()
  }

  public read(): Packet {
    return this._port.read()
  }

  public connect(link: Link) {
    this._port.connect(link)
    return this
  }

  public plug(target: Connector) {
    this._port.plug(target)
    return this
  }

  public disconnect(link: Link) {
    this._port.disconnect(link)
  }

  public unplug(target: Connector) {
    this._port.unplug(target)
  }

  public clearInput() {
    if (this._port) {
      this._port.clearInput()
    }
    return this
  }

  public setDefault(value: any) {
    if (this._port) {
      this._port.setDefault(value)
    }
    return this
  }

  public hasDefault(): boolean {
    return this._port.hasDefault()
  }

  public clearDefault() {
    this._port.clearDefault()
    return this
  }

  public setContext(value: any) {
    if (this._port) {
      this._port.setContext(value)
    }
    return this
  }

  public hasContext(): boolean {
    return this._port.hasContext()
  }

  public clearContext() {
    this._port.clearContext()
    return this
  }

  public setPersist(value: Persist) {
    this._port.setPersist(value)

    return this
  }

  public hasPersist(): boolean {
    return this._port.hasPersist()
  }

  public clearPersist() {
    this._port.clearPersist()
    return this
  }

  public destroy() {
    this._port.destroy()
  }
}
