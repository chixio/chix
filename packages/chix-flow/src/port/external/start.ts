import {ExternalPort} from '@chix/common'
import {Connector} from '../../connector'
import {Flow} from '../../flow'
import {Link} from '../../link'
import {Packet} from '../../packet'
import {InputPort} from '../input'

export class ExternalStartPort extends InputPort {
  public nodeId: string
  public actor: Flow
  constructor(externalPort: ExternalPort, actor: Flow) {
    super({
      name: externalPort.name,
      type: ':start',
    })
    this.nodeId = externalPort.nodeId
    this.name = externalPort.name
    this.actor = actor
  }
  public isSync() {
    return true
  }

  public receive(_packet: Packet, _index?: string | number) {
    this.actor.push()

    return this
  }

  public fill(_packet: Packet | any) {
    console.error('Fill is bypassed')

    return this
  }

  public isFilled() {
    return super.isFilled()
  }

  public isRequired() {
    return false
  }

  public read() {
    return super.read()
  }

  public connect(link: Link) {
    return super.connect(link)
  }

  public plug(target: Connector) {
    return super.plug(target)
  }

  public disconnect(link: Link) {
    return super.disconnect(link)
  }

  public unplug(target: Connector) {
    return super.unplug(target)
  }

  public clearInput() {
    return super.clearInput()
  }

  public setDefault(_value: any) {
    console.error('Default not supported')

    return this
  }

  public hasDefault() {
    console.error('Default not supported')

    return false
  }

  public clearDefault() {
    console.error('Default not supported')

    return this
  }

  public setContext(_value: any) {
    console.error('Context not supported')

    return this
  }

  public hasContext() {
    console.error('Context not supported')

    return false
  }

  public clearContext() {
    console.error('Context not supported')

    return this
  }

  public setPersist(_value: boolean) {
    console.error('Persist not supported')

    return this
  }

  public hasPersist() {
    console.error('Persist not supported')

    return false
  }

  public clearPersist() {
    console.error('Persist not supported')

    return this
  }

  public destroy() {
    super.destroy()
  }
}
