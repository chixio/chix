import {Port as PortDefinition} from '@chix/common'
import d from 'debug'
import {Connector} from '../connector'
import {PortEvents} from '../events'
import {Link} from '../link'
import {Packet} from '../packet'
import {Port} from './port'

const debug = d.debug('chix:outputPort')

export class OutputPort extends Port {
  public data: Packet[] = []
  public pull = false

  constructor(portDefinition: PortDefinition) {
    super(portDefinition)

    this._writeListener = this._writeListener.bind(this)
  }

  public peek(): Packet | null {
    if (this.data.length) {
      return this.data[0]
    }

    return null
  }

  public size(): number {
    return this.data.length
  }

  public read(): Packet {
    if (this.data.length) {
      return this.data.shift() as Packet
    }

    if (this.hasParent()) {
      throw Error(
        `Unable to read from port ${(this as any).parent().identifier}(${
          this.name
        })`
      )
    }

    throw Error(`Unable to read from main port (${this.name})`)
  }

  public write(packet: any) {
    if (!Packet.isPacket(packet)) {
      packet = this.createPacketFromValue(packet)
    } else {
      packet.type = this.type
    }
    // validate type (if validate)
    this.fills++

    if (this.pull) {
      this.data.push(packet)
    } else {
      this.emit(PortEvents.DATA, packet)
    }
  }

  public open() {
    if (this.isClosed()) {
      super.open()
      debug('open %s port', this.name)
      this.on(PortEvents.DATA, this._writeListener)
    }
  }

  public close() {
    if (this.isOpen()) {
      super.close()
      debug('close %s port', this.name)
      this.removeListener(PortEvents.DATA, this._writeListener)
    }
  }

  public connect(link: Link) {
    if (!this.hasConnection(link)) {
      this.open()
      this._connections.push(link)
    } else {
      console.log('Link already connected')
    }
  }

  public plug(source: Connector) {
    if (source.wire) {
      if (this.hasConnection(source.wire)) {
        console.log('Link already connected')
      } else {
        this.open()
        debug('plug (%s) -> %s', source.port, source.wire.target.port)
        if (source.setting) {
          this.setOptions(source.setting)
        }
        this._connections.push(source.wire)
      }
    } else {
      throw Error('Wireless connector.')
    }
  }

  public disconnect(link: Link) {
    if (this.hasConnection(link)) {
      if (this._connections.length === 1) {
        this.close()
      }

      return this._connections.splice(this._connections.indexOf(link), 1)
    }

    console.log('Link already disconnected')

    return undefined
  }

  public unplug(target: Connector) {
    if (target.wire && this.hasConnection(target.wire)) {
      if (this._connections.length === 1) {
        this.close()
      }
      // target.wire.removeListener(PortEvents.DATA, this.receiveListener)
      return this._connections.splice(this._connections.indexOf(target.wire), 1)
    }

    console.log('Link already disconnected')

    return undefined
  }

  public destroy() {
    this.removeAllListeners(PortEvents.DATA)
  }

  public _send(connection: Link, packet: Packet): void {
    process.nextTick(() => {
      connection.source.write(packet)
    })
  }

  public _writeListener(packet: Packet) {
    debug('%s writing to %d connections', this.name, this._connections.length)
    let _cp = packet

    for (const connection of this._connections) {
      if (this.hasParent()) {
        const node = this.getParent()

        debug(
          'write %s (%s) -> %s',
          node ? node.title || node.name : '',
          this.name,
          connection.target.port
        )
      }

      this._send(connection, _cp)

      _cp = _cp.clone(_cp.owner) // cheat owner for now
    }

    this.reads +=
      this.listenerCount(PortEvents.DATA) - 1 + this._connections.length
  }
}
