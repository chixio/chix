export function getDescriptor(name: string, instance: any) {
  let obj = instance.constructor.prototype
  let descriptor

  while (obj) {
    descriptor = Object.getOwnPropertyDescriptor(obj, name)

    obj = obj.prototype
  }

  return descriptor
}
