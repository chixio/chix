import {getDescriptor} from './getDescriptor'

export function hasGetter(name: string, instance: any) {
  const descriptor = getDescriptor(instance, name)

  return descriptor && typeof descriptor.get === 'function'
}
