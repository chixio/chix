import {getDescriptor} from './getDescriptor'

export function hasSetter(name: string, instance: any) {
  const descriptor = getDescriptor(instance, name)

  return descriptor && typeof descriptor.set === 'function'
}
