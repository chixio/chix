import {mixin} from '../util'
import {$Connect} from './connect'
import {$Control} from './control'
import {$Disconnect} from './disconnect'
import {$Pointer} from './pointer'
import {$Sync} from './sync'

export type IoHandlerMixinType = $Connect &
  $Control &
  $Disconnect &
  $Pointer &
  $Sync

export const IoHandlerMixin = mixin(
  $Connect,
  $Control,
  $Disconnect,
  $Pointer,
  $Sync
)
