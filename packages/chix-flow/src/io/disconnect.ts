import d from 'debug'
import {EventEmitter} from 'events'
import {ConnectorEvents, IOEvents} from '../events'
import {Link} from '../link'
import {Constructor} from '../types'
import {$Connect} from './connect'
import {$Control} from './control'
import {$Pointer} from './pointer'
import {$Sync} from './sync'

const debug = d.debug('chix:io')

export type $DisconnectExtensionType = $Connect &
  $Control &
  $Pointer &
  $Sync &
  EventEmitter

export interface $Disconnect {
  disconnect(link: Link): void
}

export function $Disconnect<T extends Constructor<$DisconnectExtensionType>>(
  Base: T
): Constructor<$Disconnect> & T {
  return class Disconnect$ extends Base implements $Disconnect {
    /**
     *
     * Disconnects a link
     *
     * @param {xLink} link
     * @fires IO#disconnect
     */
    public disconnect(link: Link): void {
      // unregister the connection
      if (link.ioid !== undefined && this.connections.has(link.ioid)) {
        this.connections.delete(link.ioid)
      } else {
        throw Error('Cannot disconnect an unknown connection')
      }

      if (this.syncedTargetMap.has(link.target.pid as string)) {
        this.removeSyncedTarget(link)
      }

      if (this.pointerPorts.has(link.source.pid as string)) {
        this.removePointerPort(link)
      }

      debug('%s: disconnected', link.ioid)

      delete link.ioid

      link.source.removeListener(ConnectorEvents.DATA, this.onDataHandler)

      // used by actor to close ports
      this.emit(IOEvents.DISCONNECT, link)
    }
  }
}
