import {BaseHandler} from './BaseHandler'
import {IoHandlerMixin} from './mixin'

/**
 * Io Handler
 *
 * It should know:
 *
 *  - the connections.
 *  - relevant source & target connection settings.
 *
 * Connection settings can overlap with port settings.
 * Connection settings take precedence over port settings.
 *
 * @constructor
 * @public
 */
export class IoHandler extends IoHandlerMixin(BaseHandler) {
  // public CHI: any
}
