import d from 'debug'
import {EventEmitter} from 'events'
import {v4 as uuid} from 'uuid'
import {ConnectorEvents, IOEvents} from '../events'
import {Link} from '../link'
import {Constructor} from '../types'
import {$Control} from './control'
import {$Pointer} from './pointer'
import {$Sync} from './sync'

const debug = d.debug('chix:io')

export type $ConnectExtensionType = $Control & EventEmitter & $Pointer & $Sync

export interface $Connect {
  connections: Map<string, Link>
  connect(link: Link): void
  get(link: Link): Link
}

export function $Connect<T extends Constructor<$ConnectExtensionType>>(
  Base: T
): Constructor<$Connect> & T {
  return class Connect$ extends Base implements $Connect {
    public connections: Map<string, Link> = new Map()
    /**
     *
     * Connects ports together using the link information provided.
     *
     * @param {xLink} link
     * @fires IO#connect
     * @public
     */
    public connect(link: Link): void {
      if (!link.source || !link.source.id || !link.source.port) {
        throw Error('Link requires a source')
      }

      if (!link.source.pid) {
        link.source.pid = link.source.id
      }

      if (!link.ioid) {
        link.ioid = uuid()
      }

      if (!link.target || !link.target.id || !link.target.port) {
        throw Error('Link requires a target')
      }

      if (!link.target.pid) {
        link.target.pid = link.target.id
      }

      this.connections.set(link.ioid, link)

      if (link.target.has('sync')) {
        this.addSyncedTarget(link)
      }

      if (link.source.get('pointer')) {
        this.addPointerPort(link)
      }

      debug('%s: link connected', link.ioid)

      link.source.on(ConnectorEvents.DATA, this.onDataHandler)

      this.emit(IOEvents.CONNECT, link)
    }

    public get(link: Link): Link {
      if (link.ioid !== undefined && this.connections.has(link.ioid)) {
        return this.connections.get(link.ioid) as Link
      }

      throw Error('Unable to get link.')
    }
  }
}
