import d from 'debug'
import {EventEmitter} from 'events'
import {Connector} from '../connector'
import {IOEvents} from '../events'
import {Link} from '../link'
import {Packet} from '../packet'
import {Constructor} from '../types'
import {handleIndex} from './util'

const debug = d.debug('chix:io')

export type HeldItem = [Link, Packet]
export type HeldItems = HeldItem[]

export type $ControlExtensionType = EventEmitter

export interface $Control {
  CHI: any
  hold(): void
  onDataHandler(packet: Packet, source: Connector): void
  step(): void
  resume(): void
  send(link: Link, packet: Packet | any, step?: boolean): void
  __sendData(link: Link, packet: Packet): void
}

export function $Control<T extends Constructor<$ControlExtensionType>>(
  Base: T
): Constructor<$Control> & T {
  return class Control$ extends Base implements $Control {
    public CHI: any // FIXME: not used anymore
    private _heldItems: HeldItems = []
    private _hold = false
    private _shutdown = false

    /**
     * @param {Packet} p - Packet
     * @param {Connector} source - Source Connector
     * @fires IO#send
     */

    public reset(): void {
      this._shutdown = true
    }

    /**
     *
     * @param {Packet} packet - The dropped packet
     * @param {String} origin - Origin
     * @fires IO#drop
     */
    public drop(packet: Packet, origin?: Link): void {
      // TODO: drop data/packet gracefully
      debug('IoHandler: Dropping packet %s %s', packet, origin)

      this.emit(IOEvents.DROP, packet)
    }

    public onDataHandler = (packet: Packet, source: Connector): void => {
      const wire = source.wire

      if (!wire) {
        throw Error('Could not find link on connector.')
      }

      if (source.get('collect')) {
        debug('%s: collecting packets', wire.ioid)

        throw Error('Collect not implemented')
        // this.CHI.collect(wire, packet)

        // return // will be handled by event
      }

      this.emit(IOEvents.SEND, wire)

      this.send(wire, packet)
    }

    public hold(): void {
      this._hold = true
      this._heldItems = []
    }

    public step(): void {
      if (this._heldItems.length) {
        const [link, packet] = this._heldItems.shift() as HeldItem

        this.send(link, packet, true)
      }
    }

    public resume(): void {
      this._hold = false
      let item
      if (this._heldItems.length) {
        // tslint:disable-next-line no-conditional-assignment
        while ((item = this._heldItems.shift())) {
          this.send(item[0], item[1], true)
        }
      }
    }

    /**
     *
     * The method to provide input to this io handler.
     *
     * @param {Link} link
     * @param {Packet} packet
     * @param {boolean} step
     *
     */
    public send(link: Link, packet: Packet | any, step?: boolean): void {
      if (this._hold && !step) {
        this._heldItems.push([link, packet])
      } else {
        if (!Packet.isPacket(packet)) {
          throw Error('send expects a packet')
        }

        debug('sending packet')

        /*
        if (link.source.has('pointer')) { // is just a boolean
          debug('%s: handling pointer', link.ioid)
          p.setOwner(link)
          p = p.clone(link)
          p.release(link)

          // Create an identifier
          const pp = this.getSendPorts(link.source.pid)
          pp.unshift(link.source.pid)
          const identifier = pp.join('-')
          // The source node+port are pointed to.
          // The packet has it's chi updated with the
          // source.pid as key and an assigned item id as value
          this.CHI.pointer(
            link.source.pid,
            link.source.port,
            p,
            identifier
          )
        }
        if (link.target.has('sync')) {
          debug('%s: handling sync port', link.ioid)
          const syncPorts = this.getSyncedTargetPorts(link.target)
          this.CHI.sync(
            link,
            link.target.get('sync'), // originId
            p,
            syncPorts
          )
          // always return, react on CHI.on('synced')
          return
        }
        */
        this.__sendData(link, packet)
      }
    }

    /**
     *
     * The method to provide input to this io handler.
     *
     * Ok, what misses here is info on how to find the actor
     * Who needs the information
     *
     *
     * Actor:
     *
     *  ioHandler.listenTo(Object.keys(this.nodes),
     *
     * @param {Connector} target
     * @param {object} input
     * @param {object} chi
     * @fires IO#data
     * @fires IO#packet
     * @fires IO#receive
     * @private
     */

    /*
     *
     * Send Data
     *
     * @param {xLink} link - Link to write to
     * @param {Packet} data - The input data
     * @private
     */
    public __sendData(link: Link, packet: Packet): void {
      if (this._shutdown) {
        // TODO:: probably does not both have to be dropped
        // during __sendData *and* during output
        packet.release(link)

        this.drop(packet, link)
      } else {
        /* Do this with a component
        if (link.target.has('cyclic') &&
          Array.isArray(p.read(link)) // second time it's not an array anymore
        ) {
          debug('%s: cycling', link.ioid)
          // grouping
          // The counter part will be 'collect'
          const g = this.CHI.group()
          if (p.read(link).length === 0) {
            return false
          }
          const data = JSON.parse(JSON.stringify(p.read(link)))
          for (let i = 0; i < data.length; i++) {
            // create new packet
            const newPacket = Packet.create(
              data[i],
              typeof data[i] // not sure if this will always work.
            ).setOwner(link)
            // this is a copy taking place..
            newPacket.set('chi', p.chi ? JSON.parse(JSON.stringify(p.chi)) : {})
            g.item(newPacket.chi)
            this.send(link, newPacket)
          }
          g.done()
          return null
        }
        */

        let packetClone: Packet

        if (link.source.has('index') && !packet.hasOwnProperty('index')) {
          // already done during clone
          // cp.chi = JSON.parse(JSON.stringify(cp.chi))
          // const cp = packet.clone(link); // important!
          packetClone = packet.clone(packet.owner) // important!

          if (
            undefined ===
            packetClone.read(packet.owner)[link.source.get('index')]
          ) {
            debug(
              '%s: INDEX UNDEFINED %s',
              link.ioid,
              link.source,
              packetClone.read(packetClone.owner)
            )
            return undefined // nop
          } else {
            handleIndex(link, packetClone.read(packetClone.owner), packetClone)
            // cp.write(link, handleIndex(link, cp.read(link), cp))
          }
        } else {
          packetClone = packet
        }

        // better call it pointer
        if (link.target.has('mask')) {
          packetClone.point(packetClone.owner, link.target.get('mask'))
        }

        this.emit(IOEvents.DATA, {
          data: packetClone.read(link), // only emit the data
          link,
          // data: cp.read(link) // only emit the data
        })

        this.emit(IOEvents.PACKET, {
          data: packetClone,
          link,
        })

        debug('writing packet %s -> %s', link.source.port, link.target.port)

        process.nextTick(() => {
          packetClone.release(link)

          link.target.write(packetClone)
        })

        this.emit(IOEvents.RECEIVE, link)
      }
    }
  }
}
