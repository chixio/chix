import d from 'debug'
import {Connector} from '../connector'
import {Link} from '../link'
import {Constructor} from '../types'
import {$Control} from './control'

const debug = d.debug('chix:io')

export type $SyncExtensionType = $Control

export interface $Sync {
  syncedTargetMap: Map<string, {[key: string]: string[]}>
  addSyncedTarget(link: Link): void
  getSyncedTargetPorts(target: Connector): string[]
  removeSyncedTarget(link: Link): void
  sendSynced(targetId: string, data: any): void
}

export function $Sync<T extends Constructor<$SyncExtensionType>>(
  Base: T
): Constructor<$Sync> & T {
  return class Sync$ extends Base implements $Sync {
    public syncedTargetMap = new Map()

    // build the syncedTargetMap, it contains a port array
    // (the group that wants a sync with some originId
    public addSyncedTarget(link: Link): void {
      if (!this.syncedTargetMap.has(link.target.pid)) {
        this.syncedTargetMap.set(link.target.pid, {})
      }
      if (!this.syncedTargetMap.get(link.target.pid)[link.target.get('sync')]) {
        this.syncedTargetMap.get(link.target.pid)[link.target.get('sync')] = []
      }
      this.syncedTargetMap
        .get(link.target.pid)
        [link.target.get('sync')].push(link.target.port)
      debug(
        '%s: syncing source port `%s` with target port %s',
        link.ioid,
        link.target.get('sync'),
        link.target.port
      )
    }

    public removeSyncedTarget(link: Link): void {
      const tgt = this.syncedTargetMap.get(link.target.pid)
      tgt.splice(tgt.indexOf(link.target.port), 1)
      if (tgt.length === 0) {
        this.syncedTargetMap.delete(link.target.pid)
      }
    }

    public getSyncedTargetPorts(target: Connector): string[] {
      const originId = target.get('sync')
      if (!this.syncedTargetMap.has(target.pid)) {
        throw new Error(`Unknown sync: ${target.pid}`)
      }
      if (!this.syncedTargetMap.get(target.pid).hasOwnProperty(originId)) {
        throw new Error(`Unknown sync with: ${originId}`)
      }
      // returns the ports array, those who want to sync with originId
      return this.syncedTargetMap.get(target.pid)[originId]
    }

    /**
     *
     * Send synchronized input
     *
     * TODO: Input is synced here then we
     *   throw it into the input sender.
     *   They probably stay synced, but
     *   it's not enforced anywhere after this.
     *
     * @param {string} targetId
     * @param {object} data
     */
    public sendSynced(_targetId: string, data: any): void {
      for (const targetPort in data) {
        if (data.hasOwnProperty(targetPort)) {
          const synced = data[targetPort]
          // keep in sync, do not use setImmediate
          debug('%s: sendSynced', synced.link.ioid)
          this.__sendData(synced.link, synced.p)
        }
      }
    }
  }
}
