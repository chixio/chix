import {Link} from '../../link'
import {Packet} from '../../packet'

/**
 * Handles the index
 *
 * TODO: packet is still needed, because index is set
 *
 * @param {Link} link
 * @param {any} data
 * @param {Packet} packet
 * @public
 */
export function handleIndex(link: Link, data: any, packet: Packet) {
  // TODO: data should be better defined and a typed object
  const index = link.source.get('index')

  if (/^\d+/.test(index)) {
    // numeric
    if (Array.isArray(data)) {
      if (index < data.length) {
        // new remember index.
        packet.point(link, index)
        // packet.index = index
        // return data[index]
      } else {
        throw Error(
          `index[] out-of-bounds on array output port ${link.source.port}`
        )
      }
    } else {
      throw Error(
        `Got index[] on array output port ${link.source.port}, but data is not of the array type`
      )
    }
  } else {
    if (typeof data === 'object') {
      if (data.hasOwnProperty(index)) {
        // TODO: test with dot-object
        // new remember index.
        packet.point(link, index)
        // p.index = index
        // return data[index]
      } else {
        // maybe do not fail hard and just send to the error port.
        console.log(packet)
        throw Error(
          `Property ${index} not found on object output port ${link.source.port}`
        )
      }
    } else {
      throw Error(`Got index[] on non-object output port ${link.source.port}`)
    }
  }
}
