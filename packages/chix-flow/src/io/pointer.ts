import d from 'debug'
import {Link} from '../link'
import {Constructor} from '../types'

const debug = d.debug('chix:io')

export interface $Pointer {
  pointerPorts: Map<string, string[]>
  addPointerPort(link: Link): void
  removePointerPort(link: Link): void
}

export function $Pointer<T extends Constructor>(
  Base: T
): Constructor<$Pointer> & T {
  return class Pointer$ extends Base implements $Pointer {
    public pointerPorts: Map<string, string[]> = new Map()

    public addPointerPort(link: Link): void {
      if (!link.source.pid) {
        throw Error('Could not find pid for source.')
      }

      if (!this.pointerPorts.has(link.source.pid)) {
        this.pointerPorts.set(link.source.pid, [])
      }

      const pointers = this.pointerPorts.get(link.source.pid) as string[]

      pointers.push(link.source.port as string)

      debug('%s: added pointer port `%s`', link.ioid, link.source.port)
    }

    public removePointerPort(link: Link): void {
      const src = this.pointerPorts.get(link.source.pid as string)

      if (!src) {
        throw Error(`Could not find ${link.source.pid}`)
      }

      src.splice(src.indexOf(link.source.port as string), 1)

      if (src.length === 0) {
        this.pointerPorts.delete(link.source.pid as string)
      }
    }

    public getPointerPorts(originId: string): string[] {
      if (this.pointerPorts.has(originId)) {
        return this.pointerPorts.get(originId) as string[]
      } else {
        throw new Error(`${originId} s has no pointer ports`)
      }
    }
  }
}
