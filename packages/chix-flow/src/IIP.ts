import {Link as LinkDefinition} from '@chix/common'
import {Link} from './link'

export class IIP extends Link {
  public static create(linkDefinition: LinkDefinition = {}): IIP {
    return super.create(linkDefinition, IIP)
  }
}
