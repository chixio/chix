import {EventEmitter} from 'events'
import {Packet} from '../packet'
import {Constructor, xEvent} from '../types'
import {mixin} from '../util'
import {$PortsCommon} from './interfaces'

export type ListenerFunction = any

export type $EventExtensionType = $PortsCommon

export interface $Event {
  listeners: (event: string | symbol) => ListenerFunction[]
  emit: (eventName: string, args: any[]) => void
  event: (xevent: xEvent, data: any) => void
  on: (eventName: string, callback: (result: any) => void) => void
  once: (eventName: string, callback: (result: any) => void) => void
  removeListener: (
    eventName: string,
    listenerFunction: (result: any) => void
  ) => void
}

export function $Event<T extends Constructor<$EventExtensionType>>(
  Base: T
): Constructor<$Event> & T {
  return class Event$ extends Base implements $Event {
    private _eventEmitter = new EventEmitter()

    constructor(...args: any[]) {
      super(...args)

      this._eventEmitter.listeners = this._eventEmitter.listeners.bind(
        this._eventEmitter
      )
    }
    public get listeners(): (event: string | symbol) => ListenerFunction[] {
      return this._eventEmitter.listeners
    }

    public emit(eventName: string, ...args: any[]) {
      this._eventEmitter.emit(eventName, ...args)
    }

    public on(eventName: string, callback: (result: any) => void): void {
      this._eventEmitter.on(eventName, callback)
    }

    public once(eventName: string, callback: (result: any) => void): void {
      this._eventEmitter.once(eventName, callback)
    }

    public removeListener(
      eventName: string,
      listenerFunction: (result: any) => void
    ): void {
      this._eventEmitter.removeListener(eventName, listenerFunction)
    }

    /**
     * Does both send events through output ports
     * and emits them.
     *
     * @param {xEvent} xevent
     * @param {data} data
     * @protected
     */
    public event(xevent: xEvent, data: any): void {
      if (xevent.expose) {
        this.sendPortOutput(`:${xevent.name}` as any, Packet.create(data))
      }
      // process.nextTick(() => {
      this.emit(xevent.name, data)
      // })
    }
  }
}

export namespace $Event {
  export function create(Base: any) {
    return mixin($Event)(Base)
  }
}
