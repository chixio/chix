import {Flow} from '../flow'
import {xNode} from '../node'
import {Constructor} from '../types'
import {mixin} from '../util'

export interface $Parent<T> {
  parent: xNode | Flow | null
  setParent: (node: xNode | Flow) => void
  getParent: () => xNode | Flow
  hasParent: () => boolean
}
export function $Parent<T extends Constructor>(
  Base: T
): Constructor<$Parent<T>> & T {
  return class Parent$ extends Base implements $Parent<T> {
    public parent: xNode | Flow | null = null

    public setParent(node: xNode | Flow): void {
      this.parent = node
    }

    public getParent(): xNode | Flow {
      if (this.hasParent()) {
        return this.parent as xNode | Flow
      }

      throw Error('Node has no parent.')
    }

    public hasParent(): boolean {
      return Boolean(this.parent)
    }
  }
}

export namespace $Parent {
  export function create(Base: any) {
    return mixin($Parent)(Base)
  }
}
