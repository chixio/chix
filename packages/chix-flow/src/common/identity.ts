import {Constructor} from '../types'
import {getNodeArgument, mixin, NodeArguments} from '../util'

export interface $Identity {
  /**
   * @property {String} id
   * @public
   */
  id?: string

  /**
   * @property {String} ns
   * @public
   */
  ns: string

  provider: string

  /**
   * @property {String} name
   * @public
   */
  name: string
  type: string

  /**
   * @property {String} identifier
   * @public
   */
  identifier: string
}

export function $Identity<T extends Constructor>(
  Base: T
): Constructor<$Identity> & T {
  return class Identity$ extends Base implements $Identity {
    public id?: string
    public provider: string = ''
    public ns: string = ''
    public name: string = ''
    public type: string = 'Undefined'
    private _identifier: string | undefined

    constructor(...args: any[]) {
      super(...args)
      const {id, node} = getNodeArgument(args) as NodeArguments

      if (!id) {
        throw Error(`Node id is required.`)
      }

      if (!node.ns) {
        throw Error(`Node ns is required.`)
      }

      if (!node.name) {
        throw Error(`Node name is required.`)
      }

      if (!node.type) {
        throw Error(`Node type is required.`)
      }

      this.id = id
      this.name = node.name
      this.ns = node.ns
      this.type = node.type

      if (node.provider) {
        this.provider = node.provider
      }
    }

    set identifier(value) {
      this._identifier = value
    }

    get identifier() {
      return this._identifier || `${this.ns}:${this.name}`
    }
  }
}

export namespace $Identity {
  export function create(Base: any) {
    return mixin($Identity)(Base)
  }
}
