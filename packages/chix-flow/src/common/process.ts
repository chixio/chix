import {Flow} from '../flow'
import {xNode} from '../node'

export type Process = (xNode | Flow) & {pid: string}
