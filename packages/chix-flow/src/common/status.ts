import {NodeEvents} from '../events/node'
import {Constructor} from '../types'
import {mixin} from '../util'
import {$Event} from './event'
import {$ExportCommon} from './interfaces'

export type $StatusExtensionType = $Event & $ExportCommon

export interface $Status {
  /**
   * @property {String} status
   * @public
   */
  status: string

  /**
   *
   * Set node status.
   *
   * Contain states like:
   *
   *  - Hold
   *  - Complete
   *  - Ready
   *  - Error
   *  - Timeout
   *  - etc.
   *
   * @param {String} status
   * @private
   */
  setStatus: (status: string) => void

  /**
   *
   * Get the current node status.
   *
   * This is unused at the moment.
   *
   * @public
   */
  getStatus: () => string
}

export function $Status<T extends Constructor<$StatusExtensionType>>(
  Base: T
): Constructor<$Status> & T {
  return class Status$ extends Base implements $Status {
    public status = 'unknown'

    public setStatus(status: string): void {
      this.status = status

      /**
       * Status Update Event.
       *
       * Fired multiple times on output
       *
       * Once for every output port.
       *
       * @event BaseNode#statusUpdate
       * @type {object}
       * @property {object} node - An export of this node
       * @property {string} status - The status
       */
      this.event(NodeEvents.STATUS_UPDATE, {
        node: this.export(),
        status: this.status,
      })
    }

    public getStatus(): string {
      return this.status
    }
  }
}

export namespace $Status {
  export function create(Base: any) {
    return mixin($Status, $Event.create)(Base)
  }
}
