// tslint:disable-next-line:no-implicit-dependencies
import {expect} from 'chai'
import {$Meta} from '../meta'

class TestBase {}
const Meta = $Meta(TestBase) as any

describe('Meta', () => {
  it('Meta data is not required', () => {
    // tslint:disable no-unused-expression
    new Meta()
    new Meta({})
    new Meta({node: {}})
    // tslint:enable no-unused-expression
  })

  it('Can set metadata', () => {
    const meta = new Meta({
      node: {
        title: 'Hello World',
        description: 'Hello World example',
        metadata: {
          x: 1,
          y: 2,
        },
      },
    })

    expect(meta.title).to.equal('Hello World')
    expect(meta.description).to.equal('Hello World example')
    expect(meta.metadata).to.deep.equal({
      x: 1,
      y: 2,
    })
  })

  it('Can set metadata through methods', () => {
    const meta = new Meta()
    meta.setTitle('Hello World')
    meta.setDescription('Hello World example')
    meta.setMetadata({
      x: 1,
      y: 2,
    })

    expect(meta.title).to.equal('Hello World')
    expect(meta.description).to.equal('Hello World example')
    expect(meta.metadata).to.deep.equal({
      x: 1,
      y: 2,
    })
  })

  it('Can set metadata through method chaining', () => {
    const meta = new Meta()

    meta
      .setTitle('Hello World')
      .setDescription('Hello World example')
      .setMetadata({
        x: 1,
        y: 2,
      })

    expect(meta.title).to.equal('Hello World')
    expect(meta.description).to.equal('Hello World example')
    expect(meta.metadata).to.deep.equal({
      x: 1,
      y: 2,
    })
  })
})
