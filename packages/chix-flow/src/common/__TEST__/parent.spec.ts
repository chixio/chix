// tslint:disable-next-line:no-implicit-dependencies
import {expect} from 'chai'
import {xNode} from '../../node'
import {$Parent} from '../parent'

class TestBase {}
const Parent = $Parent(TestBase)

describe('Parent', async () => {
  const node = await xNode.create({
    nodeId: 'my-id',
    nodeDefinition: {
      ns: 'hello',
      name: 'world',
      ports: {},
    },
  })
  const parent = new Parent()

  it('setParent', () => {
    parent.setParent(node)
  })

  it('getParent', () => {
    expect(parent.getParent()).to.eql(node)
  })

  it('hasParent', () => {
    expect(parent.hasParent()).to.be.equal(true)
  })
})
