// tslint:disable-next-line:no-implicit-dependencies
import {expect} from 'chai'
import {$Identity} from '../identity'

class TestBase {}
const Identity = $Identity(TestBase) as any

describe('Identity', () => {
  it('Requires node parameter', () => {
    expect(() => new Identity()).to.throw(Error, /node parameter is required/)
  })

  it('Requires node id', () => {
    expect(() => new Identity({node: {}})).to.throw(Error, /id is required/)
  })

  it('Requires node ns', () => {
    expect(() => new Identity({node: {id: 'hw'}})).to.throw(
      Error,
      /ns is required/
    )
  })

  it('Requires node name', () => {
    expect(() => new Identity({node: {id: 'hw', ns: 'hello'}})).to.throw(
      Error,
      /name is required/
    )
  })

  it('Requires node type', () => {
    expect(
      () => new Identity({node: {id: 'hw', ns: 'hello', name: 'world'}})
    ).to.throw(Error, /type is required/)
  })

  it('Builds correct identity', () => {
    const identity = new Identity({
      node: {
        id: 'hw',
        ns: 'hello',
        name: 'world',
        type: 'node',
      },
    })

    expect(identity.ns).to.equal('hello')
    expect(identity.name).to.equal('world')
    expect(identity.id).to.equal('hw')
    expect(identity.identifier).to.equal('hello:world')
  })

  it('Can override identifier', () => {
    const identity = new Identity({
      node: {
        id: 'hw',
        ns: 'hello',
        name: 'world',
        type: 'node',
      },
    })

    identity.identifier = 'custom-identifier'

    expect(identity.identifier).to.equal('custom-identifier')
  })
})
