import {Metadata} from '@chix/common'
import {forOf} from '@fbpx/lib'
import {Constructor} from '../types'
import {mixin} from '../util'

export type $MetaExtensionType = {}

export interface $Meta {
  /**
   * @property {String} title
   * @public
   */
  title: string

  /**
   * @property {String} description
   * @public
   */
  description: string

  /**
   * @property {Object} metadata
   * @public
   */
  metadata: Metadata
  setTitle: (title: string) => any
  setDescription: (description: string) => any
  setMetadata: (metadata: Metadata) => any
  hasMetadata: () => boolean
  setMeta: (key: string, value: any) => any
  removeMeta: (key: string) => void
}

export function $Meta<T extends Constructor<$MetaExtensionType>>(
  Base: T
): Constructor<$Meta> & T {
  return class Meta$ extends Base implements $Meta {
    public metadata: Metadata = {}
    public title = ''
    public description = ''

    constructor(...args: any[]) {
      super(...args)

      if (args.length) {
        const {node} = args[0]

        if (node) {
          this.title = node.title

          if (node.description) {
            this.description = node.description
          }

          if (node.metadata) {
            this.metadata = node.metadata || {}
          }
        }
      }
    }

    /**
     *
     * Set Title
     *
     * Used to set the title of a node *within* a graph.
     * This property overwrites the setting of the node definition
     * and is returned during toJSON()
     *
     * @param {string} title
     * @public
     */
    public setTitle(title: string): any {
      this.title = title

      return this
    }

    /**
     *
     * Set Description
     *
     * Used to set the description of a node *within* a graph.
     * This property overwrites the setting of the node definition
     * and is returned during toJSON()
     *
     * @param {string} description
     * @public
     */
    public setDescription(description: string): any {
      this.description = description

      return this
    }

    /**
     *
     * Set metadata
     *
     * Currently:
     *
     *   x: x position hint for display
     *   y: y position hint for display
     *
     * These values are returned during toJSON()
     *
     * @param {object} metadata
     * @public
     */
    public setMetadata(metadata: Metadata): any {
      forOf((name: string, val: any) => {
        this.setMeta(name, val)
      }, metadata)

      return this
    }

    public removeMeta(key: string): void {
      delete this.metadata[key]
    }

    public hasMetadata(): boolean {
      return Object.keys(this.metadata).length > 0
    }

    /**
     *
     * @param {string} key
     * @param {any} value
     */
    public setMeta(key: string, value: any): any {
      this.metadata[key] = value

      return this
    }
  }
}

export namespace $Meta {
  export function create(Base: any) {
    return mixin($Meta)(Base)
  }
}
