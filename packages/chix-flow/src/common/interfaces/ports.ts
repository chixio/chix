import {Packet} from '../../packet'

export interface $PortsCommon {
  sendPortOutput: (port: string, packet: Packet) => void
}
