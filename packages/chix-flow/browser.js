'use strict'

const xNode = require('./lib/node/node')
const xFlow = require('./lib/flow/flow')
const xLink = require('./lib/link')
const Actor = require('./lib/actor/actor')
const mapSchema = require('./schemas/map.json')
const nodeSchema = require('./schemas/node.json')
const stageSchema = require('./schemas/stage.json')
const Validate = require('./lib/validate')

module.exports = {
  Node: xNode,
  Flow: xFlow,
  Link: xLink,
  Actor: Actor,
  Validate: Validate,
  Schema: {
    Map: mapSchema,
    Node: nodeSchema,
    Stage: stageSchema
  }
}
