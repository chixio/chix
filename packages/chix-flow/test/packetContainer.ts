// tslint:disable:noImplicitAny

import {expect} from 'chai'
import {Packet, PacketContainer} from '@chix/flow'

describe('Packet Container:', () => {
  let packets
  let p1: Packet
  let p2: Packet
  let p3: Packet
  let pc: PacketContainer

  before(() => {
    p1 = Packet.create('1')
    p2 = Packet.create('2')
    p3 = Packet.create('3')

    packets = {
      in1: p1,
      in2: p2,
      in3: p3,
    }

    pc = new PacketContainer(packets)
  })

  it('Should be able to create packet', () => {
    expect(pc.create('new one')).to.be.instanceOf(Packet)
  })

  it('Should be able to wrap packets', () => {
    expect(pc.in1).to.eql('1')
    expect(pc.in2).to.eql('2')
    expect(pc.in3).to.eql('3')
  })

  it('Should be able to get packet', () => {
    expect(pc.get('in1')).to.eql(p1)
    expect(pc.get('in2')).to.eql(p2)
    expect(pc.get('in3')).to.eql(p3)
  })

  it('Should be able to assign', () => {
    pc.in1 = 'A'
    pc.in2 = 'B'
    pc.in3 = 'C'

    expect(pc.in1).to.eql('A')
    expect(pc.in2).to.eql('B')
    expect(pc.in3).to.eql('C')
  })

  it('Should be able to write', () => {
    expect(pc.write('in1', 'Ax')).to.eql(p1)
    expect(pc.write('in2', 'Bx')).to.eql(p2)
    expect(pc.write('in3', 'Cx')).to.eql(p3)

    expect(pc.in1).to.eql('Ax')
    expect(pc.in2).to.eql('Bx')
    expect(pc.in3).to.eql('Cx')
  })

  it('Should be able to clone', () => {
    const clone = pc.clone('in1')
    expect(clone).to.not.eql(p1) // c count increases
    expect(clone).to.not.equal(p1)
  })

  it('Should be able to clone & write', () => {
    const clone = pc.clone('in1', 'NEW')
    expect(clone).to.not.eql(p1)
    expect(clone).to.not.equal(p1)
    expect(clone.read()).to.eql('NEW')
  })
})
