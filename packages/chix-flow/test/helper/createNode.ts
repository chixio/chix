import {xNode} from '@chix/flow'

export async function createNode(): Promise<xNode> {
  return xNode.create({
    nodeId: 'node1',
    identifier: 'node1-1',
    nodeDefinition: {
      name: 'component',
      ns: 'my',
      ports: {},
    },
  })
}
