import {xNode} from '@chix/flow'
import {loadYaml} from './loadYaml'

export async function loadNode(name: string): Promise<xNode> {
  return xNode.create({
    nodeId: name,
    nodeDefinition: loadYaml(name),
    // identifier: name + '-0',
  })
}
