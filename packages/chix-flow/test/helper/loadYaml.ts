import * as fs from 'fs'
import * as yaml from 'js-yaml'
import * as path from 'path'
export function loadYaml(name: string): any {
  return yaml.load(
    fs.readFileSync(
      path.join(__dirname, '/../fixtures/', name + '.yaml'),
      'utf8'
    )
  )
}
