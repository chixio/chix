import {Flow as FlowDefinition} from '@chix/common'

import {Flow} from '@chix/flow'

export async function createFlow(
  id: string,
  flow: FlowDefinition
): Promise<Flow> {
  return Flow.create(id, flow)
}
