import {
  Flow as FlowDefinition,
  NodeDefinition,
  NodeDefinitions,
} from '@chix/common'

import {Actor, Flow} from '@chix/flow'

export async function createActor(
  flow: FlowDefinition,
  nodeDefinition?: NodeDefinition | NodeDefinitions,
  xFlow?: Flow
) {
  const actor = xFlow || new Actor()

  await actor.addMap(flow)

  if (nodeDefinition) {
    // fbp renderer sets default provider to remote.
    // FIX ME: should not matter.
    // If I add manually, it should be considered
    // as being loaded from remote.
    if (flow.providers) {
      delete flow.providers
    }

    if (Array.isArray(nodeDefinition)) {
      actor.loader.addNodeDefinitions('@', nodeDefinition)
    } else {
      actor.loader.addNodeDefinition('@', nodeDefinition as NodeDefinition)
    }
  }

  if (flow.nodeDefinitions) {
    actor.loader.addNodeDefinitions('@', flow.nodeDefinitions)

    delete flow.nodeDefinitions
  }

  return actor
}
