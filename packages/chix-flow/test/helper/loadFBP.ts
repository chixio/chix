import {Flow as FlowDefinition} from '@chix/common'
import {ChixParser} from '@chix/fbpx'
import * as fs from 'fs'
import * as path from 'path'

export function loadFBP(name: string): FlowDefinition {
  const parser = ChixParser()
  ;(parser.renderer as any).skipIDs = true

  return parser.parse(
    fs.readFileSync(
      path.join(__dirname, '/../fixtures/', name + '.fbp'),
      'utf8'
    )
  )
}
