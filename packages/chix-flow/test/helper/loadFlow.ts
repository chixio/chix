import {Flow as FlowDefinition} from '@chix/common'
import {Flow} from '@chix/flow'
import {createFlow} from './createFlow'
import {loadFBP} from './loadFBP'
import {loadYaml} from './loadYaml'

export async function loadFlow(
  id: string,
  name: string,
  type?: string
): Promise<Flow> {
  const flow: FlowDefinition =
    type === 'fbp' ? loadFBP(name) : (loadYaml(name) as FlowDefinition)

  return createFlow(id, flow)
}
