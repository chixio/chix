import {Flow as FlowDefinition} from '@chix/common'
import {Flow} from '@chix/flow'
import {createActor} from './createActor'
import {loadFBP} from './loadFBP'
import {loadYaml} from './loadYaml'

/**
 *
 * Initialize an actor with a yaml fixture file.
 *
 * @param {String} name yaml fixture name
 */
export async function loadActor(name: string, type?: string): Promise<Flow> {
  const flow: FlowDefinition =
    type === 'fbp' ? loadFBP(name) : (loadYaml(name) as FlowDefinition)

  return createActor(flow)
}
