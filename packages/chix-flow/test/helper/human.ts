import {NodeError} from '@chix/flow'

export function human(event: NodeError): void {
  console.warn(
    'NODE IN ERROR STATE, Solve this by listening to the ProcessManager:',
    event.msg
  )
}
