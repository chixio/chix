/* Circular dependency, disabled.
import {NodeDefinitions} from '@chix/common'
import {ChixParser} from '@chix/fbpx'
import {Flow} from '../../flow'
import {createActor} from './createActor'

// used with brfs in-browser
export async function loadActorFromFbp(
  content: string,
  nodeDefinitions: NodeDefinitions
): Promise<Flow> {
  const parser = ChixParser()
  ;(parser.renderer as any).skipIDs = true

  return createActor(parser.parse(content), nodeDefinitions)
}
*/

export {}
