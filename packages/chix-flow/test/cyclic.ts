import {expect} from 'chai'
import {loadActor} from './helper'

describe('Cyclic test:', () => {
  it('Should be able to handle port settings', (done) => {
    loadActor('cyclic').then((actor) => {
      const node1 = actor.getNode('1')

      const dataPort = node1.getInputPort('data')

      expect(dataPort.type).to.eql('string')
      expect(dataPort.fills).to.eql(0)

      let count = 0

      node1.on('executed', () => {
        count++
        if (count === 1) {
          expect(node1.runCount).to.eql(1)
          expect(dataPort.fills).to.eql(1)
        } else if (count === 2) {
          expect(node1.runCount).to.eql(2)
          expect(dataPort.fills).to.eql(2)
        } else if (count === 3) {
          expect(node1.runCount).to.eql(3)
          expect(dataPort.fills).to.eql(3)
          done()
        }
      })

      actor.sendIIP(
        {
          id: '1',
          port: 'data',
          setting: {
            cyclic: true,
          },
        },
        ['please', 'cycle', 'me']
      )
    })
  })
})
