import {expect} from 'chai'
import {Packet} from '@chix/flow'

describe('Packet', () => {
  describe('should be able to create packet', () => {
    let p: Packet
    let owner: any
    let nOwner: any
    let myObj: any

    it('should be able to create packet', () => {
      owner = {id: 'me', pid: 1}
      p = new Packet('some data', 'string').setOwner(owner)
    })

    it('should be able to read data', () => {
      expect(p.read(owner)).to.eql('some data')
    })

    it('should have an owner', () => {
      expect(p.hasOwner()).to.eql(true)
    })

    it('trail should be empty', () => {
      expect(p.trail).to.eql([])
    })

    describe('with incorrect owner', () => {
      it('should not be able to release', () => {
        expect(() => {
          p.release(null)
        }).to.throw(/expects an owner/)
      })
      it('should not be able to write', () => {
        expect(() => {
          p.write(undefined)
        }).to.throw(/expects an owner/)
      })
      it('should not be able to read', () => {
        expect(() => {
          p.read(undefined)
        }).to.throw(/expects an owner/)
      })
      it('should not change pointer', () => {
        expect(() => {
          p.point(undefined, '/')
        }).to.throw(/expects an owner/)
      })
      it('should not be able to clone', () => {
        expect(() => {
          p.clone(undefined)
        }).to.throw(/expects an owner/)
      })
    })

    describe('when packet is not released', () => {
      it('should not be able to take ownership', () => {
        expect(() => {
          p.setOwner({id: 'bla', pid: 3})
        }).to.throw(/refusing/i)
      })
    })

    describe('when packet is released', () => {
      it('should be able to take ownership', () => {
        nOwner = {id: 'new', pid: 2}
        p.release(owner)
        expect(p.hasOwner()).to.eql(false)
        p.setOwner(nOwner)
        expect(p.hasOwner()).to.eql(true)
      })
      it('trail should contain former owner', () => {
        expect(p.trail).to.eql([owner])
      })

      it('should be able to write', () => {
        myObj = {
          a: 1,
          nes: {
            ted: 'thing',
          },
          arr: [
            {
              as: 'such',
            },
            7,
          ],
        }
        p.write(nOwner, myObj)
        expect(p.read(nOwner)).to.equal(myObj)
      })

      it('should be able to set pointers', () => {
        p.point(nOwner, '/nes')
        expect(p.read(nOwner)).to.equal(myObj.nes)

        p.point(nOwner, '/a')
        expect(p.read(nOwner)).to.equal(myObj.a)
        expect(p.read(nOwner)).to.eql(1)

        p.point(nOwner, '/nes/ted')
        expect(p.read(nOwner)).to.equal(myObj.nes.ted)
        expect(p.read(nOwner)).to.eql('thing')

        p.point(nOwner, '/arr/0')
        expect(p.read(nOwner)).to.equal(myObj.arr[0])
        expect(p.read(nOwner)).to.eql({
          as: 'such',
        })

        p.point(nOwner, '')
        expect(p.read(nOwner)).to.equal(myObj)

        // Should slash be able to point to root?
        // p.point(nOwner, '/')
        // expect(p.read(nOwner)).to.eql(myObj)
      })

      it('should be able to set relative pointer', () => {
        p.point(nOwner, '/nes')
        expect(p.read(nOwner)).to.equal(myObj.nes)
        expect(p.read(nOwner)).to.eql({
          ted: 'thing',
        })
        p.point(nOwner, 'ted')
        expect(p.read(nOwner)).to.eql('thing')
      })

      it('should be able to set relative pointer to an array', () => {
        p.point(nOwner, '/arr')
        expect(p.read(nOwner)).to.equal(myObj.arr)
        expect(p.read(nOwner)).to.eql([
          {
            as: 'such',
          },
          7,
        ])
        p.point(nOwner, '1')
        expect(p.read(nOwner)).to.eql(7)
      })

      it('should be able to clone', () => {
        p.point(nOwner, '')
        const np = p.clone(nOwner)
        expect(np.read(nOwner)).to.eql(p.read(nOwner))
        // important not be the same reference
        expect(np.read(nOwner)).to.not.equal(p.read(nOwner))
      })

      it('must be able to set meta information', () => {
        p.meta('chi', 'gid', 1)
        p.meta('chi', 'item', 23)
        expect(p.meta('chi')).to.eql({gid: 1, item: 23})
      })

      it('must be able to get meta property', () => {
        expect(p.meta('chi', 'gid')).to.eql(1)
        expect(p.meta('chi', 'item')).to.eql(23)
      })

      it('meta should survive clone', () => {
        const np = p.clone(nOwner)
        expect(np.meta('chi')).to.eql({gid: 1, item: 23})
      })
      it('cloned meta data should be a copy', () => {
        const np = p.clone(nOwner)
        expect(np.meta('chi')).to.not.equal(p.meta('chi'))
      })
    })
  })
})
