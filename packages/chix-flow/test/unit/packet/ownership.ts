import {expect} from 'chai'
import {loadActor} from '../../helper'

// ownership is loose for now
describe.skip('Ownership:', () => {
  it('Should be unowned for output listeners', (done) => {
    loadActor('pass_function').then((actor) => {
      actor.processManager.on('error', (error) => {
        console.log(error)
      })

      const node1 = actor.getNode('1')
      const node2 = actor.getNode('2')

      node1.on('output', (event) => {
        if (event.port === 'out') {
          expect(event.out.hasOwner()).to.eql(false)
        }
      })

      node2.on('output', (event) => {
        if (event.port === 'out') {
          expect(event.out.hasOwner()).to.eql(false)
          done()
        }
      })

      actor.sendIIP(
        {
          id: '1',
          port: 'in',
        },
        ''
      )
    })
  })

  it('Output handler claims, does not release, ioHandler fails', async () => {
    const actor = await loadActor('pass_function')

    actor.processManager.on('error', (err) => {
      console.log(err)
    })

    const node1 = actor.getNode('1')

    node1.on('output', (event) => {
      if (event.port === 'out') {
        expect(event.out.hasOwner()).to.eql(false)
        event.out.setOwner(node1)
      }
    })
    expect(() => actor.sendIIP({id: '1', port: 'in'}, '')).to.throw(/Refusing/)
  })
})
