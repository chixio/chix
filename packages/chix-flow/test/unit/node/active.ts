import {expect} from 'chai'
import {loadActor} from '../../helper'

describe('Active state:', () => {
  it('Should be able to handle active state', async (done) => {
    const actor = await loadActor('active')
    const port = actor.getNode('1').getPort('input', 'data')

    expect(port.type).to.eql('string')
    expect(port.fills).to.eql(0)
    expect(port.runCount).to.eql(0)

    const node1 = actor.getNode('1')
    const node2 = actor.getNode('2')
    const node3 = actor.getNode('3')

    node1.on('executed', () => {
      expect(node1.active).to.eql(false)

      // run second node
      actor.sendIIP(
        {
          id: '2',
          port: 'data',
        },
        '/etc/issue'
      )
    })

    node2.on('executed', () => {
      expect(node2.active).to.eql(false)
      actor.sendIIP(
        {
          id: '3',
          port: 'data',
        },
        'I am still here!'
      )
    })

    // ASYNC
    node3.on('executed', () => {
      expect(node3.active).to.eql(false)
      done()
    })

    actor.on('error', (event) => console.log(event.msg))

    expect(node1.active).to.eql(false)
    actor.sendIIP(
      {
        id: '1',
        port: 'data',
      },
      'I am still here!'
    )
  })
})
