import {expect} from 'chai'
import * as util from 'util'
import {xNode, Packet} from '@chix/flow'
import {loadActor} from '../../helper'

describe('Input test:', () => {
  it('Bare node should not be able to :start if both ports have no context', (done) => {
    loadActor('two_input').then((actor) => {
      const node1 = actor.getNode('node1')

      node1.on('portFill', () => {
        expect(node1.runCount).to.eql(0)
        done()
      })
      /*
       actor.processManager.on('error', (event) => {
       event.msg).to.match(/does not match err/)
       done()
       })
       */

      actor.sendIIP(
        {
          id: 'node1',
          port: ':start',
        },
        ''
      )
    })
  })

  it('Bare node should not be able to :start if one port has no context', (done) => {
    loadActor('two_input').then((actor) => {
      const node1 = actor.getNode('node1')
      node1.setContextProperty('in1', '1')

      node1.on('portFill', () => {
        expect(node1.runCount).to.eql(0)
        done()
      })

      actor.sendIIP(
        {
          id: 'node1',
          port: ':start',
        },
        ''
      )
    })
  })

  it('Bare node should not be able to :start if one port has no default', (done) => {
    loadActor('two_input').then((actor) => {
      const node1 = actor.getNode('node1')

      node1.getInputPort('in1').setDefault('test')

      node1.on('portFill', () => {
        expect(node1.runCount).to.eql(0)
        done()
      })

      actor.sendIIP(
        {
          id: 'node1',
          port: ':start',
        },
        ''
      )
    })
  })

  it('Should be able to :start if both ports have defaults', (done) => {
    loadActor('two_input').then((actor) => {
      const node1 = actor.getNode('node1')
      node1.getInputPort('in1').setDefault('test')
      node1.getInputPort('in2').setDefault('ting')

      node1.on('portFill', () => {
        expect(node1.runCount).to.eql(1)
        done()
      })

      actor.sendIIP(
        {
          id: 'node1',
          port: ':start',
        },
        ''
      )
    })
  })

  it('Should be able to :start if both ports have context', (done) => {
    loadActor('two_input').then((actor) => {
      const node1 = actor.getNode('node1')

      node1.setContextProperty('in1', '1')
      node1.setContextProperty('in2', '1')

      node1.on('portFill', () => {
        expect(node1.runCount).to.eql(1)
        done()
      })

      actor.sendIIP(
        {
          id: 'node1',
          port: ':start',
        },
        ''
      )
    })
  })

  it('Should be able to :start if one port has context and one has a default', (done) => {
    loadActor('two_input').then((actor) => {
      const node1 = actor.getNode('node1')

      node1.setContextProperty('in1', '1')
      node1.getInputPort('in2').setDefault('ting')

      node1.on('portFill', () => {
        expect(node1.runCount).to.eql(1)
        done()
      })

      actor.sendIIP(
        {
          id: 'node1',
          port: ':start',
        },
        ''
      )
    })
  })

  it('Should be able to :start if one has a default and one is not required', (done) => {
    loadActor('two_input').then((actor) => {
      const node1 = actor.getNode('node1')

      node1.getInputPort('in1').required = false
      node1.getInputPort('in2').setDefault('ting')

      // this is a normal failure.
      // not required means undefined goes in.
      // yet the node itself directly sends it as output.
      // which is wrong.

      node1.on('portFill', () => {
        expect(node1.runCount).to.eql(1)
        done()
      })

      actor.sendIIP(
        {
          id: 'node1',
          port: ':start',
        },
        ''
      )
    })
  })

  it('Should be able to :start if one has context and one is not required', (done) => {
    loadActor('two_input').then((actor) => {
      const node1 = actor.getNode('node1')

      node1.getInputPort('in1').required = false
      node1.setContextProperty('in2', '1')

      node1.on('portFill', () => {
        expect(node1.runCount).to.eql(1)
        done()
      })

      actor.sendIIP(
        {
          id: 'node1',
          port: ':start',
        },
        ''
      )
    })
  })

  it('Should be able to :start if none is required', (done) => {
    loadActor('two_input').then((actor) => {
      const node1 = actor.getNode('node1')

      node1.getInputPort('in1').required = false
      node1.getInputPort('in2').required = false

      node1.on('portFill', () => {
        expect(node1.runCount).to.eql(1)
        done()
      })

      actor.sendIIP(
        {
          id: 'node1',
          port: ':start',
        },
        ''
      )
      // ok test this, sending multiple times, normal and with rejection
      // actor.sendIIP({ id: 'node1', port: ':start'}, '')
      // actor.sendIIP({ id: 'node1', port: ':start'}, '')
      // actor.sendIIP({ id: 'node1', port: ':start'}, '')
      // actor.sendIIP({ id: 'node1', port: ':start'}, '')
    })
  })

  it('Bare node should not be able to :start if one port is async and other no context', (done) => {
    loadActor('two_input_one_async').then((actor) => {
      // possibly this does not create an error.
      // it just awaits.. :start fill is succesful.
      const node1 = actor.getNode('node1')

      let count = 0
      node1.on('portFill', () => {
        expect(node1.runCount).to.eql(count)
        count++
        if (count === 1) {
          done()
        }
      })

      actor.sendIIP(
        {
          id: 'node1',
          port: ':start',
        },
        ''
      )

      node1.setContextProperty('syn2', 'bla')

      actor.sendIIP(
        {
          id: 'node1',
          port: 'asy1',
        },
        'test'
      )
    })
  })

  it.skip('Async node cannot have context', (done) => {
    loadActor('two_input_one_async').then((actor) => {
      const node1 = actor.getNode('node1')

      node1.setContextProperty('in1', '1')

      const io = actor.ioHandler
      io.monitor(':error', node1.pid, (packet: Packet) => {
        packet.setOwner(io)
        expect(packet.read(io).msg).to.be.instanceOf(Error)

        packet.release(io)
        done()
      })
      actor.processManager.once('error', (error: any) => {
        console.log(error.msg)
      })

      actor.sendIIP(
        {
          id: 'node1',
          port: ':start',
        },
        ''
      )
    })
  })

  // should do what
  it.skip('Node with one sync without defaults  and one async port, should reject the IIP', (done) => {
    loadActor('two_input_one_async').then((actor) => {
      // ok async node cannot have a default or context.,
      // so make this clear within these test.
      // actor.getNode('node1').setContextProperty('in1', '1')
      const node1 = actor.getNode('node1')

      const io = actor.ioHandler
      io.monitor(':complete', node1.pid, (packet: Packet) => {
        packet.setOwner(io)

        expect(util.isError(packet.read(io).msg)).to.eql(true)

        packet.release(io)

        done()
      })

      actor.sendIIP(
        {
          id: 'node1',
          port: ':start',
        },
        ''
      )
    })
  })

  // async?
  it.skip('Async should start three times with context', (done) => {
    loadActor('array_fill_start_3_times_with_context').then((actor) => {
      const io = actor.ioHandler

      const node1 = actor.getNode('node1')

      io.monitor(':start', node1.pid, (p: Packet) => {
        const data = p.read(node1)
        // TODO: NEVER STARTED

        const node = actor.getNode(data.node.id) as xNode

        expect(node.inPorts).to.have.length(3)
        expect(node.outPorts).to.eql(['out', ':complete'])
        expect(node.openPorts).to.eql(['in', 'expect'])
        expect(node.filled).to.eql(3)
        expect(node.context.msg).to.eql('Should persist')

        expect(node.getInputPort('expect').peek()).to.eql([
          'test1',
          'test2',
          'test3',
        ])
        expect(node.getInputPort('msg').peek()).to.eql('Should persist')

        // This is already the last running, check the other ones also
        expect(node.getInputPort('in').peek()).to.eql([
          'test1',
          'test2',
          'test3',
        ])
      })

      // TODO: also check openPorts very well.
      node1.on('output', (event) => {
        // it even hangs, must be the queue or something
        if (event.port === 'out') {
          expect(io.queueManager.inQueue).to.eql(0)
          expect(event.out.read(node1)).to.eql(['test1', 'test2', 'test3'])
          // darn, what could be wrong., why does chit hang..
          // ok :start :start :start works, but 3 times the same iip not.
          // console.log('output, so what is wrong', event.port, event.out)
          done()
        }
      })

      // this was hanging..
      actor.getNode('node1').setContextProperty('msg', 'Should persist')
      actor.getNode('node3').setContextProperty('length', 3)
      actor.sendIIP(
        {
          id: 'node1',
          port: 'expect',
        },
        ['test1', 'test2', 'test3']
      )
      actor.sendIIP(
        {
          id: 'node2',
          port: 'in',
        },
        'test1'
      )
      actor.sendIIP(
        {
          id: 'node2',
          port: 'in',
        },
        'test2'
      )
      actor.sendIIP(
        {
          id: 'node2',
          port: 'in',
        },
        'test3'
      )
    })
  })

  it('Send IIPs to both ports', (done) => {
    loadActor('two_input').then((actor) => {
      actor.getNode('node1').on('output', (event) => {
        if (event.port === 'out1') {
          expect(event.out).to.be.instanceOf(Packet)
          expect(event.out.read()).to.eql('test1')
          done()
        }
      })

      actor.sendIIP(
        {
          id: 'node1',
          port: 'in1',
        },
        'test1'
      )

      actor.sendIIP(
        {
          id: 'node1',
          port: 'in2',
        },
        'test2'
      )
    })
  })

  it.skip('Send three times to one async port and never to the other', async () => {
    const actor = await loadActor('two_input_one_async')

    const pm = actor.processManager

    let i = 0
    pm.on('error', (err) => {
      i++

      if (err) {
        /*
         console.trace(err)
         throw (err)
         */
      }

      switch (i) {
        case 1:
          // this one fulfills link1, link1 has no queue
          actor.getNode('node1').setStatus('running')
          actor.sendIIP(
            {
              port: 'syn2',
              id: 'node1',
            },
            'your-sync-value'
          )

          break

        case 2:
          // never reach because we are not in error anymore
          // throw Error('should not been reached')

          break

        default:
          throw Error('Should never reach this case')
      }
    })

    actor.sendIIP(
      {
        id: 'node1',
        port: 'asy1',
      },
      'test2'
    )
    actor.sendIIP(
      {
        id: 'node1',
        port: 'asy1',
      },
      'test3'
    )
    actor.sendIIP(
      {
        id: 'node1',
        port: 'asy1',
      },
      'test'
    )
  })
})
