import {expect} from 'chai'
import {loadActor} from '../../helper'

describe('Pass Function:', () => {
  it('Should survive', (done) => {
    loadActor('pass_function').then((actor) => {
      actor.processManager.on('error', (err) => {
        console.log(err)
      })

      const node1 = actor.getNode('1')
      const node2 = actor.getNode('2')

      node1.on('output', (event) => {
        if (event.port === 'out') {
          event.out.setOwner(node1)
          expect(typeof event.out.read(node1).send === 'function').to.eql(true)

          event.out.release(node1)
        }
      })

      node2.on('output', (event) => {
        if (event.port === 'out') {
          event.out.setOwner(node2)
          expect(event.out.read(node2)).to.eql('survived')
          event.out.release(node2)
          done()
        }
      })

      actor.sendIIP(
        {
          id: '1',
          port: 'in',
        },
        ''
      )
    })
  })
})
