import {expect} from 'chai'
import {loadActor} from '../../../helper'

describe('export', () => {
  it('Should be able to serialize export', async () => {
    const actor = await loadActor('two_input')
    const node1 = actor.getNode('node1')
    expect(() => JSON.stringify(node1.export())).to.not.throw()
  })
})
