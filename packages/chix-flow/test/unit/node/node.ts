import {expect} from 'chai'
import {InputPort, OutputPort} from '@chix/flow'
import {loadNode} from '../../helper'

describe('χNode:', () => {
  it('χNode should have structure', async () => {
    const node = await loadNode('node_sync')
    expect(node.id).to.eql('node_sync')
    expect(node.ns).to.eql('test')
    expect(node.name).to.eql('syncNode')
    expect(node.type).to.eql('node')
    expect(node.active).to.eql(false)
    expect(node.identifier).to.eql('test:syncNode')
    expect(node.ports).to.instanceOf(Object)

    expect(node.ports.input).instanceOf(Object)
    expect(node.ports.output).instanceOf(Object)
    expect(node.getOutputPort(':complete')).instanceOf(OutputPort)

    /*
    expect(node.runCount).to.eql(0)
    expect(node.filled).to.eql(0)
    expect(node.inPorts).to.be.an.array
    expect(node.outPorts).to.be.an.array
    expect(node.context).to.be.an.object
    expect(node.status).to.eql('created')
    */
  })

  describe('Should be able to alter ports', async () => {
    const node = await loadNode('string')

    it('create a port', () => {
      node.addPort('input', 'in2', {
        type: 'string',
      })
      expect(node.getInputPort('in2')).instanceOf(InputPort)
      expect(node.getInputPort('in2').type).to.eql('string')
    })

    it('rename a port', () => {
      node.renamePort('input', 'in2', 'in3')
      expect(node.getInputPort('in3')).instanceOf(InputPort)
      expect(node.portExists('input', 'in2')).to.eql(false)
      expect(node.getInputPort('in3').type).to.eql('string')
    })

    it('remove a port', () => {
      node.removePort('input', 'in3')

      expect(node.portExists('input', 'in3')).to.eql(false)
    })
  })
})
