import {Flow as FlowDefinition} from '@chix/common'
import {Loader} from '@chix/loader'
import {expect} from 'chai'
import {Actor, xNode, Packet} from '@chix/flow'

async function getActor(lines: string[]) {
  let flow: FlowDefinition
  let actor: Actor

  const fn = lines.join(';')

  flow = {
    description: 'On Test',
    links: [],
    name: 'test-flow',
    nodes: [
      {
        id: '1',
        name: 'async',
        ns: 'test',
      },
    ],
    ns: 'main',
    title: 'Test',
    type: 'flow',
  }

  const nodeDefinitions = [
    {
      async: true,
      expose: ['chi'],
      fn,
      name: 'async',
      ns: 'test',
      ports: {
        input: {
          in: {
            async: true,
            type: 'string',
          },
        },
        output: {
          out: {
            type: 'string',
          },
        },
      },
    },
  ]

  actor = new Actor()

  const loader = new Loader()

  // both start() run with nextTick
  // the second will call on.shutdown of the first
  loader.addNodeDefinitions('@', nodeDefinitions)

  actor.setLoader(loader)
  await actor.addMap(flow)

  return actor
}

describe('Async input test:', () => {
  let node: xNode

  describe('Should load input functions', () => {
    let actor: Actor

    before(async () => {
      const fn = [
        'state = 1',
        "on.input.in = function() { if($.in === 'Hi') { state++; }",
        'output({ out: $.get("in") })',
        '}',
      ]

      actor = await getActor(fn)
    })

    it('should increase state (Hi)', (done) => {
      // Make these once things work with ioHandler.monitor.

      node = actor.getNode('1') as xNode

      node.once('executed', () => {
        expect(node.getInputPort('in').fn.state).to.eql(2)
        expect(node.state).to.eql(2)
        expect(node.nodebox.state).to.eql(2)
        done()
      })

      actor.sendIIP(
        {
          id: '1',
          port: 'in',
        },
        'Hi'
      )
    })

    it('should maintain state', (done) => {
      node = actor.getNode('1') as xNode

      node.once('executed', () => {
        expect(node.getInputPort('in').fn.state).to.eql(2)
        expect(node.state).to.eql(2)
        expect(node.nodebox.state).to.eql(2)
        done()
      })

      // data !== Hi
      actor.sendIIP(
        {
          id: '1',
          port: 'in',
        },
        'Keep state'
      )
      actor.sendIIP(
        {
          id: '1',
          port: 'in',
        },
        'Oh Keep state'
      )
    })

    // Ok, does not seem to be stable.
    // sometimes it passes sometimes actual is 2 instead of 3
    // also it seems to fail on first run, reruns pass.
    it('should continue increasing state (Hi)', (done) => {
      node = actor.getNode('1') as xNode

      node.once('executed', () => {
        expect(node.getInputPort('in').fn.state).to.eql(3)
        expect(node.state).to.eql(3)
        expect(node.nodebox.state).to.eql(3)
        done()
      })

      // data === Hi
      actor.sendIIP(
        {
          id: '1',
          port: 'in',
        },
        'Hi'
      )
    })
  })

  // DEPRECATED: should not have access to node info
  it.skip('Should have access to node info', async (done) => {
    const fn = [
      'on.input.in = function() { ',
      'output({ out: $.create(this.inPorts) })',
      '}',
    ]

    const actor = await getActor(fn)
    const ioHandler = actor.ioHandler

    // ok out is the out port, does it make sense the output is a packet?
    ioHandler.monitor('out', actor.getNode('1').pid, (p: Packet) => {
      expect(p).to.be.instanceOf(Packet)
      expect(p.read(null)).to.eql(['in'])
      expect(p.chi).to.eql({})
      done()
    })

    actor.sendIIP(
      {
        id: '1',
        port: 'in',
      },
      'Hi'
    )
  })
  /*
    it('Should have access to source info', function(done) {

      const fn = [
        'on.input.in = function() { ',
        'output({ out: source })',
        '}'
      ]

      const actor = await getActor(fn)
      actor.getNode('1').on('output', function(data) {
        expect(data.out).to.eql({
          id: 'bla',
          port: 'bla'
        })
        done()
      })


      // NOTE: source is mainly meant to be send along with a link
      // with sendIIP it is a bit of cheating
      actor.sendIIP({ id: '1', port: 'in' }, 'Hi')
      //, {}, { id: 'bla', port: 'bla' })

    })
  */
})
