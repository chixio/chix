import {expect} from 'chai'
import {Link, Packet, InputPort} from '@chix/flow'

describe('Object Port:', () => {
  describe('a new Object port', () => {
    let port: InputPort
    let link1: Link
    let link2: Link
    let p1: Packet
    let p2: Packet
    before(() => {
      port = new InputPort({name: 'in', type: 'object'})
      link1 = Link.create({
        target: {
          id: 'target-id',
          port: 'target-port',
          setting: {index: 'prop1'},
        },
      })

      link2 = Link.create({
        target: {
          id: 'target-id2',
          port: 'target-port2',
          setting: {index: 'prop2'},
        },
      })
    })
    it('Has empty input', () => {
      expect(port.data).to.eql([])
    })
    it('Carries no context', () => {
      expect(port.hasContext()).to.eql(false)
    })
    it('Is not persisting', () => {
      expect(port.hasPersist()).to.eql(false)
    })
    it('Is not filled', () => {
      expect(port.isFilled()).to.eql(InputPort.EMPTY)
    })
    it('Is not open', () => {
      expect(port.isOpen()).to.eql(false)
    })
    it('Has no connections', () => {
      expect(port.hasConnections()).to.eql(false)
    })
    it('Has no fills', () => {
      expect(port.fills).to.eql(0)
    })
    it('Has no reads', () => {
      expect(port.reads).to.eql(0)
    })
    it('Has a zero runCount', () => {
      expect(port.runCount).to.eql(0)
    })

    describe('Can handle connections', () => {
      it('Can connect a link1', () => {
        port.plug(link1.target)
        expect(link1.target.listeners('data').length).to.eql(1)
      })

      it('Port has opened', () => {
        expect(port.isOpen()).to.eql(true)
      })

      it('Has link1 as connection', () => {
        expect(port.hasConnection(link1)).to.eql(true)
      })

      it('Has connections', () => {
        expect(port.hasConnections()).to.eql(true)
      })

      it('Can connect link2', () => {
        port.plug(link2.target)
        expect(link2.target.listeners('data').length).to.eql(1)
      })

      it('Has now two connections', () => {
        expect(port.getConnections().length).to.eql(2)
      })

      it('Has link2 as connection', () => {
        expect(port.hasConnection(link2)).to.eql(true)
      })

      // first try with only packet
      it('Link write will fill port at index `prop1`', () => {
        p1 = new Packet('p1data', 'string')
        expect(port.data.length).to.eql(0)
        link1.target.write(p1)
        expect(port.isFilled()).to.eql(InputPort.EMPTY)
      })

      it('Link write will fill port at index `prop2`', () => {
        p2 = new Packet('p2data', 'string')
        expect(port.data.length).to.eql(1)
        link2.target.write(p2)
        expect(port.isFilled()).to.eql(InputPort.FILLED)
      })

      it('Read will contain an object packet containing data from both our packets', () => {
        const p = port.read()
        const data = p.read(port)
        expect(data.prop1).to.eql('p1data')
        expect(data.prop2).to.eql('p2data')
      })

      it('Port is not filled anymore', () => {
        expect(port.isFilled()).to.eql(InputPort.EMPTY)
        expect(port.data.length).to.eql(0)
      })

      it('Can disconnect link1', () => {
        port.unplug(link1.target)
        expect(link1.target.listeners('data').length).to.eql(0)
      })

      it('Does not have link1 as connection', () => {
        expect(port.hasConnection(link1)).to.eql(false)
      })

      it('Has now one connection', () => {
        expect(port.getConnections().length).to.eql(1)
      })

      it('Port is still open', () => {
        expect(port.isOpen()).to.eql(true)
      })

      it('Can disconnect link2', () => {
        port.unplug(link2.target)
        expect(link2.target.listeners('data').length).to.eql(0)
      })

      it('Does not have link2 as connection', () => {
        expect(port.hasConnection(link2)).to.eql(false)
      })

      it('Port is now closed', () => {
        expect(port.isOpen()).to.eql(false)
      })

      it('Has no connections', () => {
        expect(port.getConnections().length).to.eql(0)
      })
    })
  })
})
