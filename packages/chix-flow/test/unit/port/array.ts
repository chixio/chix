import {expect} from 'chai'
import {Link, Packet, InputPort} from '@chix/flow'

describe('Array Port:', () => {
  let port: InputPort
  let link1: Link
  let link2: Link
  let p1: Packet
  let p2: Packet
  let p3: Packet
  let p4: Packet
  before(() => {
    port = new InputPort({name: 'in', type: 'array'})
    link1 = Link.create({target: {id: '1', port: 'test1', setting: {index: 1}}})
    link2 = Link.create({target: {id: '2', port: 'test2', setting: {index: 0}}})
  })
  it('Has empty input', () => {
    expect(port.data).to.eql([])
  })
  it('Carries no context', () => {
    expect(port.hasContext()).to.eql(false)
  })
  it('Is not persisting', () => {
    expect(port.hasPersist()).to.eql(false)
  })
  it('Is not filled', () => {
    expect(port.isFilled()).to.eql(0)
  })
  // test the states here
  it('Is not open', () => {
    expect(port.isOpen()).to.eql(false)
  })
  it('Has no connections', () => {
    expect(port.hasConnections()).to.eql(false)
  })
  it('Has no fills', () => {
    expect(port.fills).to.eql(0)
  })
  it('Has no reads', () => {
    expect(port.reads).to.eql(0)
  })
  it('Has a zero runCount', () => {
    expect(port.runCount).to.eql(0)
  })

  it('Can send packet at index', () => {
    p1 = new Packet('test', 'string')
    port.receive(p1, 1)
  })

  it('Has created an Array Packet', () => {
    expect(port.data.length).to.eql(1)
    expect(port.data[0]).to.be.instanceOf(Packet)
    expect(port.isFilled()).to.eql(InputPort.EMPTY)
  })

  it('Send to other index will fullfil', () => {
    p2 = new Packet('test2', 'string')
    port.receive(p2, 0)
    expect(port.data.length).to.eql(1)
    expect(port.data[0]).to.be.instanceOf(Packet)
    // data is not a reference?
    expect(port.isFilled()).to.eql(InputPort.DIRECT)
  })

  it('Read will give us the filled packet', () => {
    const owner = {}
    const p = port.read()
    p.setOwner(owner)
    const data = p.read(port)
    expect(data[0]).to.eql(p2.read(port))
    expect(data[1]).to.eql(p1.read(port))
    expect(port.isFilled()).to.eql(InputPort.EMPTY)
    expect(port.data.length).to.eql(0)
  })

  describe('Can handle connections', () => {
    it('Can connect a link1', () => {
      port.plug(link1.target)
    })

    it('Port has opened', () => {
      expect(port.isOpen()).to.eql(true)
    })

    it('Has link1 as connection', () => {
      expect(port.hasConnection(link1)).to.eql(true)
    })

    it('Has connections', () => {
      expect(port.hasConnections()).to.eql(true)
    })

    // first try with only packet
    it('Link write will fill port at index 1', () => {
      p3 = new Packet('...', 'string')
      expect(port.data.length).to.eql(0)
      link1.target.write(p3)
      expect(port.isFilled()).to.eql(0)
    })

    it('Can connect link2', () => {
      port.plug(link2.target)
    })

    it('Has now two connections', () => {
      expect(port.getConnections().length).to.eql(2)
    })

    it('Has link2 as connection', () => {
      expect(port.hasConnection(link2)).to.eql(true)
    })

    it('Link write will fill port at index 0', () => {
      p4 = new Packet('bla', 'string')
      expect(port.data.length).to.eql(1)
      link2.target.write(p4)
      expect(port.isFilled()).to.eql(InputPort.FILLED)
    })

    it('Read will contain an array packet containing both our packets', () => {
      const p = port.read()
      // p.setOwner(this)
      const data = p.read(port)
      expect(data[0]).to.eql(p4.read(port))
      expect(data[1]).to.eql(p3.read(port))
    })

    it('Port is not filled anymore', () => {
      expect(port.isFilled()).to.eql(InputPort.EMPTY)
      expect(port.data.length).to.eql(0)
    })

    it('Can disconnect link1', () => {
      port.unplug(link1.target)
    })

    it('Does not have link1 as connection', () => {
      expect(port.hasConnection(link1)).to.eql(false)
    })

    it('Has now one connection', () => {
      expect(port.getConnections().length).to.eql(1)
    })

    it('Port is still open', () => {
      expect(port.isOpen()).to.eql(true)
    })

    it('Can disconnect link2', () => {
      port.unplug(link2.target)
    })

    it('Does not have link2 as connection', () => {
      expect(port.hasConnection(link2)).to.eql(false)
    })

    it('Port is now closed', () => {
      expect(port.isOpen()).to.eql(false)
    })

    it('Has no connections', () => {
      expect(port.getConnections().length).to.eql(0)
    })
  })
})
