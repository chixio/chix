import {expect} from 'chai'
import {Link, Packet, OutputPort} from '@chix/flow'

describe('Port:', () => {
  describe('a new port', () => {
    let port: OutputPort
    let link1: Link
    let link2: Link
    before(() => {
      port = new OutputPort({name: 'out', type: 'string'})
      link1 = Link.create()
      link2 = Link.create()
    })
    it('Is not open', () => {
      expect(port.isOpen()).to.eql(false)
    })
    it('Has no connections', () => {
      expect(port.hasConnections()).to.eql(false)
    })
    it('Has no fills', () => {
      expect(port.fills).to.eql(0)
    })
    it('Has no reads', () => {
      expect(port.reads).to.eql(0)
    })
    it('Has a zero runCount', () => {
      expect(port.runCount).to.eql(0)
    })

    describe('Can handle connections', () => {
      it('Can connect a link1', () => {
        port.plug(link1.source)
      })

      it('Port has openend', () => {
        expect(port.isOpen()).to.eql(true)
      })

      it('Has link1 as connection', () => {
        expect(port.hasConnection(link1)).to.eql(true)
      })

      it('Has connections', () => {
        expect(port.hasConnections()).to.eql(true)
      })

      // Port write makes sense with no connection?
      it('Port write will send output', () => {})

      it('Can connect link2', () => {
        port.plug(link2.target)
      })

      it('Has now two connections', () => {
        expect(port.getConnections().length).to.eql(2)
      })

      it('Has link2 as connection', () => {
        expect(port.hasConnection(link2)).to.eql(true)
      })

      it('Can disconnect link1', () => {
        port.unplug(link1.target)
      })

      it('Does not have link1 as connection', () => {
        expect(port.hasConnection(link1)).to.eql(false)
      })

      it('Has now one connection', () => {
        expect(port.getConnections().length).to.eql(1)
      })

      it('Port is still open', () => {
        expect(port.isOpen()).to.eql(true)
      })

      it('Can disconnect link2', () => {
        port.unplug(link2.target)
      })

      it('Does not have link2 as connection', () => {
        expect(port.hasConnection(link2)).to.eql(false)
      })

      it('Port is now closed', () => {
        expect(port.isOpen()).to.eql(false)
      })

      it('Has no connections', () => {
        expect(port.getConnections().length).to.eql(0)
      })
    })
    describe('Receiving writes', () => {
      let p1: Packet
      let p2: Packet
      it('Should be able to write packet', (done) => {
        p1 = new Packet('A', 'string')
        function listen(p: Packet) {
          expect(p).to.eql(p1)

          expect(port.fills).to.equal(1)
          port.removeListener('data', listen)
          done()
        }
        port.once('data', listen)
        port.write(p1)
      })
      it('fills should be 1', () => {
        expect(port.fills).to.eql(1)
      })
      it('fills should increase', () => {
        p2 = new Packet('B', 'string')
        port.write(p2)
        expect(port.fills).to.eql(2)
      })
      describe('When connected', () => {
        it('Link should receive data', (done) => {
          port.connect(link1)
          link1.source.once('data', (_p: Packet) => {
            done()
          })
          port.write(p2)
        })
      })
    })
  })
})
