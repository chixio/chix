import {expect} from 'chai'
import nodeDefinitions from 'chix-test'
import {PortEvents, Link, xNode, Packet, InputPort} from '@chix/flow'
import {loadNode} from '../../helper'

describe('InputPort:', () => {
  describe('a new port', () => {
    let port: InputPort
    let link1: Link
    let link2: Link
    before(() => {
      port = new InputPort({name: 'in', type: 'string'})
      link1 = Link.create()
      link2 = Link.create()
    })
    it('Has empty input', () => {
      expect(port.data).to.eql([])
    })
    it('Carries no context', () => {
      expect(port.hasContext()).to.eql(false)
    })
    it('Is not persisting', () => {
      expect(port.hasPersist()).to.eql(false)
    })
    it('Is not filled', () => {
      expect(port.isFilled()).to.eql(InputPort.EMPTY)
    })
    it('Is not open', () => {
      expect(port.isOpen()).to.eql(false)
    })
    it('Has no connections', () => {
      expect(port.hasConnections()).to.eql(false)
    })
    it('Has no fills', () => {
      expect(port.fills).to.eql(0)
    })
    it('Has no reads', () => {
      expect(port.reads).to.eql(0)
    })
    it('Has a zero runCount', () => {
      expect(port.runCount).to.eql(0)
    })

    describe('Can handle connections', () => {
      it('Can connect a link1', () => {
        port.plug(link1.target)
        expect(link1.target.listeners('data').length).to.eql(1)
      })

      it('Port has opened', () => {
        expect(port.isOpen()).to.eql(true)
      })

      it('Has link1 as connection', () => {
        expect(port.hasConnection(link1)).to.eql(true)
      })

      it('Has connections', () => {
        expect(port.hasConnections()).to.eql(true)
      })

      it('Link write will fill port', () => {
        const p = new Packet('...', 'string')
        expect(port.data.length).to.eql(0)
        link1.target.write(p)
        expect(port.isFilled()).to.eql(InputPort.FILLED)
        expect(port.data.length).to.eql(1)
        port.reset()
        expect(port.data.length).to.eql(0)
      })

      it('Can connect link2', () => {
        port.plug(link2.target)
        expect(link2.target.listeners('data').length).to.eql(1)
      })

      it('Has now two connections', () => {
        expect(port.getConnections().length).to.eql(2)
      })

      it('Has link2 as connection', () => {
        expect(port.hasConnection(link2)).to.eql(true)
      })

      it('Can disconnect link1', () => {
        port.unplug(link1.target)
        expect(link1.target.listeners('data').length).to.eql(0)
      })

      it('Does not have link1 as connection', () => {
        expect(port.hasConnection(link1)).to.eql(false)
      })

      it('Has now one connection', () => {
        expect(port.getConnections().length).to.eql(1)
      })

      it('Port is still open', () => {
        expect(port.isOpen()).to.eql(true)
      })

      it('Can disconnect link2', () => {
        port.unplug(link2.target)
        expect(link2.target.listeners('data').length).to.eql(0)
      })

      it('Does not have link2 as connection', () => {
        expect(port.hasConnection(link2)).to.eql(false)
      })

      it('Port is now closed', () => {
        expect(port.isOpen()).to.eql(false)
      })

      it('Has no connections', () => {
        expect(port.getConnections().length).to.eql(0)
      })
    })
    describe('Receiving input', () => {
      let p1: Packet
      let p2: Packet
      it('Should be able to receive packet', () => {
        p1 = new Packet('A', 'string')
        port.receive(p1)
      })
      it('isFilled() should be true', () => {
        expect(port.isFilled()).to.eql(InputPort.DIRECT)
      })
      it('fills should be 1', () => {
        expect(port.fills).to.eql(1)
      })
      it('Adding a new packet should increase the queue', () => {
        p2 = new Packet('B', 'string')
        port.receive(p2)
      })
      it('fills should be 2', () => {
        expect(port.fills).to.eql(2)
        expect(port.data.length).to.eql(2)
      })
      describe('Reading port data', () => {
        it('Packet should equal the first packet', () => {
          const p = port.read()
          expect(p).to.eql(p1)
        })
        it('Port should still be filled', () => {
          expect(port.isFilled()).to.equal(InputPort.DIRECT)
        })
        it('Number of input packets should be 1', () => {
          expect(port.data.length).to.equal(1)
        })
        it('Next read should give second packet', () => {
          const p = port.read()
          expect(p).to.eql(p2)
        })
        it('Number of input packets should be 0', () => {
          expect(port.data.length).to.equal(0)
        })
        it('Read until empty', () => {
          const packets = [p1, p2]
          port.receive(p1)
          port.receive(p2)
          while (port.isFilled()) {
            expect(port.read()).to.eql(packets.shift())
          }
        })
      })
      describe('Context', () => {
        // tslint:disable no-shadowed-variable
        let p1: Packet
        let p2: Packet
        // tslint:enable no-shadowed-variable
        it('should be able to setContext', () => {
          port.setContext('Some Value')
        })
        it('isFilled should be true', () => {
          expect(port.isFilled()).to.eql(InputPort.CONTEXT)
        })
        it('read should give new (unowned) packet containing the context', () => {
          p1 = port.read()
          expect(p1).to.be.instanceOf(Packet)
          expect(p1.hasOwner()).to.eql(false)
        })
        it('new read should give different packet, but same content', () => {
          p2 = port.read()
          expect(p2).to.not.equal(p1)
          const d1 = p1.read(port)
          const d2 = p2.read(port)
          expect(d1).to.eql('Some Value')
          expect(d1).to.equal(d2)
        })
        it('if port is filled, context should be ignored', () => {
          port.receive('A')
          p1 = port.read()
          p1.setOwner(port)
          expect(p1).to.be.instanceOf(Packet)
          expect(p1.read(port)).to.eql('A')
          expect(port.data.length).to.eql(0)
          expect(port.hasContext()).to.eql(true)
        })
        it('multi input precedence', () => {
          port.receive('A')
          port.receive('B')
          const res = []
          for (let i = 0; i < 4; i++) {
            // @ts-ignore
            res.push(port.read().read(port))
          }
          expect(res).to.eql(['A', 'B', 'Some Value', 'Some Value'])
        })
        describe('With a connection', () => {
          it('Port is only filled if there is data', () => {
            port.plug(link1.target)
            expect(link1.target.listeners('data').length).to.eql(1)
            expect(port.isFilled()).to.eql(InputPort.EMPTY)
          })
        })
        it('context can be cleared', () => {
          expect(port.context?.read(port)).to.eql('Some Value')
          port.clearContext()
          expect(typeof port.context).to.eql('undefined')
        })
        // now there is a port object it makes sense to just have the port listen
        // to the event of the link itself.
        // so plug becomes somewhat different, connections make no sense.
        // if you connect and then still manually fill the port.
      })
    })
    describe('Persist', () => {
      // this was more complex also.
      // the link determines persistance, another link could have persist set to none.
      it('Should persist packet', () => {
        port.unplug(link1.target)
        expect(link1.target.listeners('data').length).to.eql(0)
        expect(port.data.length).to.eql(0)
        port.setOption('persist', true)
        expect(port.hasConnections()).to.eql(false)
        port.plug(link1.target)
        expect(link1.target.listeners('data').length).to.eql(1)
        expect(port.data.length).to.eql(0)
        link1.target.write(new Packet('A'))
        expect(port.data.length).to.eql(1)
        // normal read
        expect(port.read().read(port)).to.eql('A')
        expect(port.data.length).to.eql(0)
        // read from persist
        expect(port.read().read(port)).to.eql('A')
        expect(port.data.length).to.eql(0)
        expect(port.read().read(port)).to.eql('A')
        expect(port.data.length).to.eql(0)
      })
      it('Last packet will persist', () => {
        link1.target.write(new Packet('B'))
        expect(port.read().read(port)).to.eql('B')
        expect(port.read().read(port)).to.eql('B')
        expect(port.read().read(port)).to.eql('B')
      })
      it('Last disconnection will clear persisted value', () => {
        port.plug(link2.target)
        expect(link2.target.listeners('data').length).to.eql(1)
        expect(port.getConnections().length).to.eql(2)
        expect(port.hasPersist()).to.eql(true)
        port.disconnect(link2)
        expect(port.hasPersist()).to.eql(true)
        port.disconnect(link1)
        expect(port.hasPersist()).to.eql(false)
      })
    })
  })

  describe('Validation', async () => {
    it('Should validate string', (done) => {
      const inputPort = new InputPort({name: 'in', type: 'string'})

      expect(() => inputPort.fill(undefined)).to.throw(
        `Expected 'string' got 'Undefined' on port 'in'`
      )

      expect(() => inputPort.fill(1)).to.throw(
        `Expected 'string' got 'Number' on port 'in'`
      )
      expect(() => inputPort.fill(true)).to.throw(
        `Expected 'string' got 'Boolean' on port 'in'`
      )
      expect(() => inputPort.fill({})).to.throw(
        `Expected 'string' got 'Object' on port 'in'`
      )
      expect(() => inputPort.fill([])).to.throw(
        `Expected 'string' got 'Array' on port 'in'`
      )
      expect(() => inputPort.fill({})).to.throw(
        `Expected 'string' got 'Object' on port 'in'`
      )

      inputPort.once(PortEvents.FILL, () => done())

      inputPort.fill('A String')
    })

    it('Should validate number', (done) => {
      const inputPort = new InputPort({name: 'in', type: 'number'})

      expect(() => inputPort.fill(undefined)).to.throw(
        `Expected 'number' got 'Undefined' on port 'in'`
      )

      expect(() => inputPort.fill('A string')).to.throw(
        `Expected 'number' got 'String' on port 'in'`
      )
      expect(() => inputPort.fill(true)).to.throw(
        `Expected 'number' got 'Boolean' on port 'in'`
      )
      expect(() => inputPort.fill({})).to.throw(
        `Expected 'number' got 'Object' on port 'in'`
      )
      expect(() => inputPort.fill([])).to.throw(
        `Expected 'number' got 'Array' on port 'in'`
      )
      expect(() => inputPort.fill({})).to.throw(
        `Expected 'number' got 'Object' on port 'in'`
      )

      inputPort.once(PortEvents.FILL, () => done())

      inputPort.fill(3)
    })

    it('Should validate boolean', (done) => {
      const inputPort = new InputPort({name: 'in', type: 'boolean'})

      expect(() => inputPort.fill(undefined)).to.throw(
        `Expected 'boolean' got 'Undefined' on port 'in'`
      )

      expect(() => inputPort.fill('A string')).to.throw(
        `Expected 'boolean' got 'String' on port 'in'`
      )
      expect(() => inputPort.fill(3)).to.throw(
        `Expected 'boolean' got 'Number' on port 'in'`
      )
      expect(() => inputPort.fill({})).to.throw(
        `Expected 'boolean' got 'Object' on port 'in'`
      )
      expect(() => inputPort.fill([])).to.throw(
        `Expected 'boolean' got 'Array' on port 'in'`
      )
      expect(() => inputPort.fill({})).to.throw(
        `Expected 'boolean' got 'Object' on port 'in'`
      )

      inputPort.fill(true)
      expect(inputPort.fills).to.equal(1)
      expect(inputPort.reads).to.equal(0)
      expect(inputPort.read().read(inputPort)).to.eql(true, 'Boolean is true')
      expect(inputPort.reads).to.equal(1)
      expect(inputPort.filled).to.equal(false)

      inputPort.fill(1)
      expect(inputPort.filled).to.equal(true)
      expect(inputPort.fills).to.equal(2)
      expect(inputPort.read().read(inputPort)).to.eql(1, 'Boolean is 1')
      expect(inputPort.reads).to.equal(2)
      expect(inputPort.filled).to.equal(false)

      inputPort.fill(0)
      expect(inputPort.filled).to.equal(true)
      expect(inputPort.fills).to.equal(3)
      expect(inputPort.read().read(inputPort)).to.eql(0, 'Boolean is 0')
      expect(inputPort.reads).to.equal(3)
      expect(inputPort.filled).to.equal(false)

      inputPort.fill(Boolean(true))
      expect(inputPort.filled).to.equal(true)
      expect(inputPort.fills).to.equal(4)
      expect(inputPort.read().read(inputPort)).to.eql(
        true,
        'Boolean is Boolean(true)'
      )
      expect(inputPort.reads).to.equal(4)
      expect(inputPort.filled).to.equal(false)

      inputPort.fill(Boolean(false))
      expect(inputPort.filled).to.equal(true)
      expect(inputPort.fills).to.equal(5)
      expect(inputPort.read().read(inputPort)).to.eql(
        false,
        'Boolean is Boolean(false)'
      )
      expect(inputPort.reads).to.equal(5)
      expect(inputPort.filled).to.equal(false)

      done()
    })

    describe('Should be able to fill', async () => {
      const node = await loadNode('string')
      // tslint:disable-next-line:no-shadowed-variable
      const inputPort = node.getInputPort('in')
      inputPort.type = 'boolean'

      describe('Async ports multiple times', () => {
        it('for there is only one async port', (done) => {
          // tslint:disable-next-line:no-shadowed-variable

          expect(inputPort.type).to.eql('boolean')
          expect(inputPort.async).to.eql(true)

          inputPort.fill(true)

          inputPort.once(PortEvents.FILL, () => done())

          inputPort.fill(false)
        })

        it('for all ports are async', () => {
          // const conn2 = Connector.create('string-0', 'in2')

          // TODO: not sure what is tested here..

          // tslint:disable-next-line:no-shadowed-variable
          // const inputPort = node.getInputPort('in')

          expect(inputPort.type).to.eql('boolean')

          const inputPort2 = node.addPort('input', 'in2', {
            type: 'string',
            fn: () => {},
          }) as InputPort

          expect(inputPort2.async).to.eql(true)

          inputPort.fill(true)
          inputPort2.fill('test')

          // inputPort.fill(true)).to.be.instanceOf(Error)
        })
      })
    })
  })

  describe('Should load concrete definition', () => {
    it('A portbox should be able to accept input and run', async () => {
      const node = await xNode.create({
        nodeId: 'my-proc',
        nodeDefinition: nodeDefinitions.Repeat,
      })
      return new Promise((resolve) => {
        node.on('output', (output) => {
          if (output.port === 'out') {
            const packet = output.out.read()

            expect(packet).to.eql('testing')
            resolve()
          }
        })

        node.getInputPort('in').fill('testing')
      })
    })
  })
})
