import {expect} from 'chai'
import {xNode, Packet} from '@chix/flow'
import {loadNode} from '../../helper'

// TODO: confusing input example of the node itself.
function checkRest(node: xNode) {
  const level1Port = node.getInputPort('level1')

  expect(level1Port.peek().read(level1Port)).to.have.property('in2')

  expect(level1Port.peek().read(level1Port).in2).to.eql(true)

  expect(level1Port.peek().read(level1Port)).to.have.property('in3')
  expect(level1Port.peek().read(level1Port).in3 === undefined).to.eql(true)

  expect(level1Port.peek().read(level1Port)).to.have.property('level2')

  expect(level1Port.peek().read(level1Port).level2.in1).to.eql('level2')

  expect(level1Port.peek().read(level1Port).level2).to.have.property('in2')
  expect(level1Port.peek().read(level1Port).level2.in2 === undefined).to.eql(
    true
  )
}

// Is done on port level now.
describe.skip('Port Filler:', () => {
  let node: xNode

  before(async () => {
    node = await loadNode('node_defaults_object')
  })

  it('should be able to set object defaults', () => {
    const level1Port = node.getInputPort('level1')

    expect(level1Port.hasPersist()).to.eql(false)
    expect(level1Port.hasContext()).to.eql(false)

    // fill(node)

    // this port has level1.in1 set to default and thus should return a default.
    // I think that is what defaulter tried to do.
    // so even though you do something like 'my-setting' -> [in3] MyProcess
    // the rest of the object will use the defaults and the input is constructed.
    // it can be rather common with things like props for react components.
    // other wise you would have to provide the whole object even though all of those
    // are optional and already defined as defaults.
    // so rather important this still works.
    expect(level1Port.peek()).to.be.instanceOf(Packet)

    expect(level1Port.peek().read(level1Port)).to.be.an('object')

    expect(level1Port.peek().read(level1Port)).to.have.property('in1')

    expect(level1Port.peek().read(level1Port).in1).to.eql('level1')

    checkRest(node)
  })

  describe('With context on port1', () => {
    let _node: xNode
    let p1: Packet

    before(async () => {
      _node = await loadNode('node_defaults_object')
      p1 = new Packet({in1: 'test'}, 'object')

      _node.getInputPort('level1').setContext(p1)

      // fill(node)
    })

    it('Context should be set', () => {
      const level1Port = node.getInputPort('level1')

      expect(level1Port.peek().read(level1Port).in1).to.eql('test')
    })

    it('Rest should be filled with defaults', () => {
      checkRest(node)
    })
  })

  describe('With persist on port1', () => {
    // tslint:disable-next-line no-shadowed-variable
    let node: xNode
    let p1: Packet

    before(async () => {
      node = await loadNode('node_defaults_object')
      p1 = new Packet({in1: 'test'}, 'object')

      const level1Port = node.getInputPort('level1')

      level1Port.setPersist(p1)

      // fill(node)
    })

    it('Input should contain clone of persist', () => {
      const level1Port = node.getInputPort('level1')

      expect(level1Port.peek()).to.not.eql(p1)
      expect(level1Port.peek().c).to.eql(1)
      expect(level1Port.peek().read(level1Port).in1).to.eql('test')
    })

    it('Rest should be filled with defaults', () => {
      checkRest(node)
    })
  })
})
