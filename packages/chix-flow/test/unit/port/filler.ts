import {expect} from 'chai'
import {xNode, Packet, fill} from '@chix/flow'
import {loadNode} from '../../helper'

// is done on port level
describe.skip('Port Filler:', () => {
  describe('with a clean object', () => {
    let node: xNode
    before(async () => {
      node = await loadNode('node_defaults')
    })

    it('should be able to set primitive defaults', () => {
      // expect(node.input).to.eql({})
      // expect(node.persist).to.eql({})
      // expect(node.context).to.eql({})

      fill(node)

      const in1Port = node.getInputPort('in1')
      const in2Port = node.getInputPort('in2')
      const in3Port = node.getInputPort('in3')
      const in4Port = node.getInputPort('in4')
      const in5Port = node.getInputPort('in5')

      expect(in1Port.read().read(in1Port)).to.eql('test')
      expect(in2Port.read().read(in2Port)).to.eql(true)

      // not required
      expect(in3Port.read().read(node) === undefined).to.eql(true)

      // required, will lead to unfulfilled input (if correct)
      expect(in4Port === undefined).to.eql(true)
      expect(in5Port === undefined).to.eql(true)
    })
  })

  describe('With existing input on port 1', () => {
    let node: xNode
    before(async () => {
      node = await loadNode('node_defaults')
    })

    it('Input on port1 should not have been modified', () => {
      const in1Port = node.getInputPort('in1')
      const p1 = new Packet('testing', 'string')
      p1.setOwner(in1Port)

      // fill ports
      in1Port.fill(p1)

      fill(node)

      // must be unmodified
      expect(in1Port.peek()).to.eql(p1)
      expect(in1Port.peek().read(in1Port)).to.eql('testing')
    })

    it('Second port should be filled with default', () => {
      // should be filled with default
      const in2Port = node.getInputPort('in2')

      expect(in2Port.peek().read(in2Port)).to.eql(true)
    })

    it('Third port should have packet with undefined value', () => {
      // is that state correct? or should the port just not be filled..
      const in3Port = node.getInputPort('in3')
      expect(in3Port.peek().read(in3Port) === undefined).to.eql(true)
    })

    it('Fourth & fifth port should not have been set', () => {
      // is that state correct? or should the port just not be filled..
      const in4Port = node.getInputPort('in4')
      const in5Port = node.getInputPort('in5')
      expect(in4Port.peek().read(in4Port) === undefined).to.eql(true)
      expect(in5Port.peek().read(in5Port) === undefined).to.eql(true)
    })
  })

  describe('With context on port 1', () => {
    let node: xNode
    before(async () => {
      node = await loadNode('node_defaults')
    })

    it('Input on port1 should have a clone of the context', () => {
      const p1 = new Packet('testing', 'string')

      const in1Port = node.getInputPort('in1')

      // fill ports
      in1Port.setContext(p1)

      fill(node)

      // clone version 1
      expect(in1Port.peek().c).to.eql(1)
      expect(in1Port.peek() !== p1).to.eql(true)
      // node.input.in1).to.not.eql(p1); BUG?
      expect(in1Port.peek().read(in1Port)).to.eql('testing')
    })

    it('Second port should be filled with default', () => {
      const in2Port = node.getInputPort('in2')

      // should be filled with default
      expect(in2Port.peek().read(in2Port)).to.eql(true)
    })

    it('Third port should have packet with undefined value', () => {
      const in3Port = node.getInputPort('in3')

      // is that state correct? or should the port just not be filled..
      expect(in3Port.peek().read(in3Port) === undefined).to.eql(true)
    })

    it('Fourth & fifth port should not have been set', () => {
      const in4Port = node.getInputPort('in4')
      const in5Port = node.getInputPort('in5')

      // is that state correct? or should the port just not be filled..
      expect(in4Port.peek().read(in4Port) === undefined).to.eql(true)
      expect(in5Port.peek().read(in5Port) === undefined).to.eql(true)
    })
  })

  describe('With persist on port 1', () => {
    let node: xNode
    before(async () => {
      node = await loadNode('node_defaults')
    })

    it('Input on port1 should have a clone of persist', () => {
      const p1 = new Packet('testing', 'string')

      const in1Port = node.getInputPort('in1')

      // fill ports
      in1Port.setPersist(p1)

      fill(node)

      // clone version 1
      expect(in1Port.peek().c).to.eql(1)
      expect(in1Port.peek() !== p1).to.eql(true)
      // expect(node.input.in1).to.not.eql(p1)
      expect(in1Port.peek().read(in1Port)).to.eql('testing')
    })
  })

  describe('With both context & persist on port 1', () => {
    let node: xNode
    before(async () => {
      node = await loadNode('node_defaults')
    })

    it('Input on port1 should have a clone of persist', () => {
      const p1 = new Packet('testing', 'string')
      const p2 = new Packet('somecontext', 'string')

      const in1Port = node.getInputPort('in1')

      // fill ports
      in1Port.setPersist(p1)
      in1Port.setContext(p2)

      fill(node)

      // clone version 1
      expect(in1Port.peek().c).to.eql(1)
      expect(in1Port.peek() !== p1).to.eql(true)
      // node.input.in1).to.not.eql(p1)
      expect(in1Port.read().read(in1Port)).to.eql('testing')
    })
  })
})
