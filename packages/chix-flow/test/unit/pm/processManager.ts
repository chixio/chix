import {expect} from 'chai'
import {Flow, ProcessManager} from '@chix/flow'
import {createNode, loadActor} from '../../helper'

describe('ProcessManager', () => {
  it('should have a process map', () => {
    const pm = new ProcessManager()

    expect(pm.processes.size).to.eql(0)
  })

  it('should not be able to register a node with existing pid', async () => {
    const pm = new ProcessManager()

    // TODO: much to complex to just create a node
    // will be solved when nodes are not dynamic
    // In which caste we can just do new MathRandom()
    const node = await createNode()

    node.pid = '1'
    expect(() => {
      pm.register(node)
    }).to.throw(/existing process id/)
  })

  it('should be able to register a node', async () => {
    const pm = new ProcessManager()
    const node = await createNode()

    // maybe event.node instead of node
    pm.on('addProcess', (n) => {
      expect(n).to.be.an('object')
      expect(n.pid).to.be.a('string')
      expect(n).to.equal(node)
    })

    pm.register(node)

    const nodeId = node.id as string
    expect(typeof nodeId).to.equal('string')

    expect(pm.getById(nodeId)).to.eql(node)
    expect(pm.findBy('ns', 'my')).to.eql(node)
    expect(pm.findBy('name', 'component')).to.eql(node)

    const pid = node.pid as string
    expect(typeof pid).to.equal('string')

    expect(pm.get(pid)).to.eql(node)
  })

  it('should be able to unregister a process', async () => {
    const pm = new ProcessManager()
    const node = await createNode()

    pm.register(node)

    const process = await pm.unregister(node)

    expect(pm.processes.size).to.eql(0)

    expect(process).to.not.have.property('pid')

    return
  })

  it('Actor with subgraph, both graphs should use the same process manager', async () => {
    const actor = await loadActor('with_sub')

    actor.processManager.on('error', (err) => {
      console.log(err.msg)
    })

    // TODO: ioHandler should do the same kind of test
    // also use this subgraph thing in more places
    // to test general subgraph stuff.
    expect(actor.processManager).to.be.instanceOf(ProcessManager)

    const subgraph1 = actor.getNode('subgraph1') as Flow

    expect(subgraph1).to.be.instanceOf(Flow)

    const subgraph1ProcessManager = subgraph1.processManager

    expect(subgraph1ProcessManager).to.be.instanceOf(ProcessManager)
    expect(subgraph1ProcessManager).to.equal(actor.processManager)
  })

  it.skip('should be able to stop process', () => {})

  it.skip('should be able to hold process', () => {})

  it.skip('should be able to resume process', () => {})

  // big setup...l
  describe('Using subgraphs', () => {
    it('should lead to both graphs using the same processManager', async () => {
      const actor = await loadActor('with_sub')

      expect(actor.processManager).to.be.instanceOf(ProcessManager)

      const subgraph1 = actor.getNode('subgraph1') as Flow

      expect(subgraph1).to.be.instanceOf(Flow)

      const subgraph1ProcessManager = subgraph1.processManager

      expect(subgraph1ProcessManager).to.be.instanceOf(ProcessManager)
      expect(subgraph1ProcessManager).to.equal(actor.processManager)
    })

    it('processManager should contain all processes', async () => {
      const actor = (await loadActor('with_sub')) as Flow

      // connections should just be an array,
      // internal state can keep the mappings.

      // Main graph                  - 1
      // 3 nodes, one is a subgraph  - 3
      // subgraph contains 1 node    - 1
      //                             ---
      //                               5
      expect(actor.processManager.processes.size).to.eql(5)
    })

    it.skip('Subgraph/process should know parent process', () => {})

    it.skip('Unregister of parent process should unregister child processes', () => {})

    it.skip('Stopping parent process should stop child processes', () => {})

    it.skip('Resuming parent process should resume child processes', () => {})
  })

  describe('ProcessManager instances', () => {
    let actor1
    let actor2

    it('Main actors have different ProcessManager instances by default', async () => {
      actor1 = await loadActor('map')
      actor2 = await loadActor('map')

      expect(actor1.processManager).to.not.eql(actor2.processManager)
    })
  })
})
