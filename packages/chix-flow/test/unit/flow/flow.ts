import {expect} from 'chai'
import {Flow} from '@chix/flow'
import {loadFlow} from '../../helper'

describe('Flow:', () => {
  let flow: Flow

  before(async () => {
    flow = await loadFlow('test', 'subgraph')
  })

  it('Should be of type flow', () => {
    expect(flow.type).to.eql('flow')
  })

  it('Should have correct properties', () => {
    expect(flow.ns).to.eql('sub')
    expect(flow.name).to.eql('graph')
    expect(flow.title).to.eql('My SubGraph')
    expect(flow.description).to.eql('This is my subgraph.')
  })

  it('Should not be active', () => {
    expect(flow.active).to.equal(false)
  })

  it('Should have an identifier', () => {
    expect(flow.identifier).to.eql('sub:graph')
  })

  it('Should have correct ports', () => {
    expect(flow.ports.input).to.have.property('IN')
    expect(flow.ports.output).to.have.property('OUT')
  })

  describe('getNode()', () => {
    it('Should be able to get node by id', () => {
      const node = flow.getNode('node1')

      expect(node.ns).to.eql('world')
      expect(node.name).to.eql('hello')
    })
    it('Should be able to get node by identifier', () => {
      const node = flow.getNode('world::hello-0')

      expect(node.id).to.eql('node1')
    })
  })

  /*
  it.skip('Should have installed internal portHandlers', () => {
    let listeners
    let afterListener

    const node1 = flow.getNode('node1')
    const node2 = flow.getNode('node2')

    expect(flow.ports).to.be.an.object

    listeners = node1.listeners('output')
    afterListener = node1.afterListener('output')

    // match() doesn't seem to work, matches always.
    expect(afterListener.toString()).to.containEql('nodeOutputHandlerActor')

    listeners = node2.listeners('output')
    expect(listeners).to.have.length(1)

    afterListener = node2.afterListener('output')
    expect(afterListener.toString()).to.containEql('nodeOutputHandlerActor')

    expect(listeners[0].toString()).to.containEql('internalPortHandlerFlow')
  })
  */

  /*
  it.skip('Should listen to the freePort event', () => {
    let listeners

    // TODO: test if the freePort events do what they are supposed to do

    listeners = flow.getNode('node1').listeners('freePort')
    expect(listeners).to.have.length(2)

    // Normal free port handler
    expect(listeners[0].toString()).to.match('freePortHandlerActor')

    // free port handle for the internal port
    expect(listeners[1].toString()).to.match('freePortHandlerFlow')

    listeners = flow.getNode('node2').listeners('freePort')
    expect(listeners).to.have.length(1)
    expect(listeners[0].toString()).to.match('freePortHandlerActor')
  })
  */
})
