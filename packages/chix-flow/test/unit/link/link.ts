import {expect} from 'chai'
import {Connector, Link, Packet} from '@chix/flow'

describe('Link:', () => {
  let link: Link

  before(() => {
    link = Link.create()
  })

  it('Link should have proper structure', done => {
    expect(typeof link.id).to.equal('string', 'ID is a string')
    expect(typeof link.ioid).to.equal('string', 'ID is a string')

    link.setTarget('target1', 'in', {})

    expect(link.target).to.be.an.instanceof(Connector, 'Target is a Connector')

    link.setSource('target1', 'in', {})

    expect(link.source).to.be.an.instanceof(Connector, 'Source is a Connector')

    done()
  })

  it('Should be able to CRUD a setting on links', done => {
    link.set('test', '123')
    expect(link.get('test')).to.equal('123')
    link.set('test', '456')
    expect(link.get('test')).to.equal('456')
    link.del('test')
    expect(link.get('test') === undefined).to.equal(true)
    done()
  })

  it('Should be able to CRUD a setting on source connector', done => {
    link.source.set('test', '123')
    expect(link.source.get('test')).to.equal('123')
    link.source.set('test', '456')
    expect(link.source.get('test')).to.equal('456')
    link.source.del('test')
    expect(link.source.get('test') === undefined).to.equal(true)
    done()
  })

  it('Should be able to CRUD a setting on target connector', done => {
    link.target.set('test', '123')
    expect(link.target.get('test')).to.equal('123')
    link.target.set('test', '456')
    expect(link.target.get('test')).to.equal('456')
    link.target.del('test')
    expect(link.target.get('test') === undefined).to.equal(true)
    done()
  })

  it('Source Connector should have access to wire', done => {
    expect(link.source.wire as any).to.equal(link)
    done()
  })

  it('Target Connector should have access to wire', done => {
    // todo: should be a real wire instance, write to source connector
    // emit from target connector.
    expect(link.target.wire as any).to.equal(link)
    done()
  })

  it('Should be able to write and receive a data event', done => {
    // todo: should be a real wire instance, write to source connector
    // emit from target connector.
    const packet = new Packet('test!', 'string')

    packet.setOwner(link)

    link.target.on('data', p => {
      expect(p).to.equal(packet)
      done()
    })
    link.target.write(packet)
  })

  describe('Should emit change event', () => {
    it('when the title is changed', done => {
      link.once('change', (link, what, value) => {
        expect(link.metadata.title).to.eql('My Title')
        expect(what).to.eql('metadata')
        expect(value).to.eql({title: 'My Title'})
        done()
      })

      link.setTitle('My Title')
    })

    it('when settings are set', done => {
      link.once('change', (_link, what, value) => {
        expect(_link.metadata.title).to.eql('My Title')
        expect(what).to.eql('setting')
        expect(value).to.eql({persist: true})
        done()
      })

      // todo: should be a real wire instance, write to source connector
      // emit from target connector.
      link.set('persist', true)
    })
  })
})
