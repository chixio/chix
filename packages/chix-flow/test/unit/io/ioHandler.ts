import {assert, expect} from 'chai'
import {Actor, Flow, IoHandler, Link, Packet} from '@chix/flow'
import {loadActor} from '../../helper'

describe('IoHandler', () => {
  let ioHandler: IoHandler

  before(() => {
    ioHandler = new IoHandler()
  })

  describe('Construction', () => {
    it('should have empty connections map', () => {
      expect(ioHandler.connections.size).to.eql(0)
    })
  })

  describe('Connect', () => {
    it('should not be able to connect a link without source', () => {
      const link = new Link()
      expect(() => {
        ioHandler.connect(link)
      }).to.throw('Link requires a source')
    })

    it('should not be able to connect a link without target', () => {
      const link = new Link()

      link.setSource('my_id', 'my_port')
      expect(() => {
        ioHandler.connect(link)
      }).to.throw('Link requires a target')
    })

    describe('Adding one link', () => {
      let link1: Link
      let link2: Link
      let link3: Link

      before(() => {
        link1 = new Link()
        link1.setSource('source_node1', 'out')
        link1.setTarget('target_node1', 'in')

        link2 = new Link()
        link2.setSource('source_node2', 'out')
        link2.setTarget('target_node2', 'in')

        link3 = new Link()
        link3.setSource('source_node1', 'out')
        link3.setTarget('target_node1', 'in2')
      })

      it('should emit a connect event', (done) => {
        ioHandler.once('connect', (link: Link) => {
          expect(link).to.equal(link)
          done()
        })

        ioHandler.connect(link1)
      })

      describe('Connections', () => {
        it('must contain our connection', () => {
          assert.isTrue(ioHandler.connections.has(link1.ioid))
          expect(ioHandler.connections.get(link1.ioid)).to.equal(link1)
        })
      })

      describe('Adding second link with different source & target', () => {
        before(() => {
          ioHandler.connect(link2)
        })

        describe('Connections', () => {
          it('should have two connections registered', () => {
            expect(ioHandler.connections.size).to.eql(2)
          })
        })

        describe('When disconnected', () => {
          it('should emit a disconnect event', (done) => {
            ioHandler.once('disconnect', (link: Link) => {
              expect(link).to.equal(link2)
              done()
            })

            ioHandler.disconnect(link2)
          })

          describe('Connection', () => {
            it('Our link should not be registered anymore', () => {
              assert.isFalse(ioHandler.connections.has(link2.ioid))
            })
          })
        })
      })

      describe('Adding second link with same source & target', () => {
        before(() => {
          ioHandler.connect(link3)
        })

        describe('Connection', () => {
          it('Our link should not be registered anymore', () => {
            assert.isTrue(ioHandler.connections.has(link3.ioid))
            expect(ioHandler.connections.get(link3.ioid)).to.equal(link3)
          })
        })
      })

      describe('Sending data to link1', () => {
        it('should receive a data event', (done) => {
          const p = new Packet('hi', 'string')

          link1.target.once('data', (data) => {
            expect(data).to.equal(p)
            done()
          })

          ioHandler.send(link1, p)
        })

        describe.skip('If link1 contains a pointer', () => {
          //
          // which was something like:
          //
          //  *out
          //
          // The output just continues, however we have registered.
          // With CHI, and it fills our own chi, the one which goes
          // along with us, by itself this registration does nothing.
          //
          // It must be combined with sync ports.
          // When input ports are defined as sync, it will guarantee
          // .. somehow I fail to see the difference with groups.
          // This is more of an identifier, we came from this node.

          it('should receive a data event', (done) => {
            const input = 'hi'

            link1.source.set('pointer', true)

            link1.once('data', (data) => {
              link1.source.del('pointer')
              expect(data).to.equal(input)
              expect(link1.source.get('pointer') === undefined).to.equal(true)
              done()
            })

            ioHandler.send(link1, input)
          })
        })

        describe.skip("If link1's target expects sync", () => {
          it('should not receive a data event', (done) => {
            // ioHandler.send(link1, input)
            done()
          })
        })
      })
    })
  })

  describe('Actor with subgraph', () => {
    it('should lead to both graphs using the same ioHandler', async () => {
      const actor = await loadActor('with_sub')

      const subgraph1 = actor.nodes.get('subgraph1') as Flow

      expect(subgraph1.ioHandler).to.equal(actor.ioHandler)
    })

    it('ioHandler should contain all connections', async () => {
      const actor = await loadActor('with_sub')
      const link = actor.links.values().next().value

      assert.isTrue(actor.ioHandler.connections.has(link.ioid))
      expect(actor.ioHandler.connections.get(link.ioid)).to.equal(link)
    })
  })

  describe('When an actor removes the link', () => {
    it('the ioHandler should be updated to remove the connection', async () => {
      const actor = await loadActor('with_sub')

      const link = actor.links.values().next().value as Link
      const ioid = link.ioid
      const target = link.target.id as string
      const targetPort = link.target.port as string
      // const sourcePort = link.source.id

      expect(
        actor.getNode(target).getInputPort(targetPort).hasConnection(link)
      ).to.equal(true, 'should have connection.')

      actor.removeLink(link as any)

      // connections should just be an array,
      // internal state can keep the mappings.
      assert.isFalse(actor.ioHandler.connections.has(ioid))
      assert.isFalse(actor.ioHandler.syncedTargetMap.has(ioid))

      // extra checks
      expect(actor.links.has(link.id)).to.equal(false, 'link should be gone.')

      expect(
        actor.getNode(target).getInputPort(targetPort).hasConnection(link)
      ).to.equal(false, 'connection should be gone.')

      expect(link).to.not.have.property('ioid')
    })
  })

  describe('When the ioHandler removes a connection', () => {
    let actor: Actor
    let link: Link
    let ioid: string | undefined

    before(async () => {
      actor = await loadActor('with_sub')
    })

    it('the actor should retain the link, but without ioid', (done) => {
      link = actor.links.values().next().value
      ioid = link.ioid

      actor.ioHandler.on('disconnect', () => {
        expect(actor.links.has(link.id)).to.equal(true)
        expect(link).to.not.have.property('ioid')
        assert.isFalse(actor.ioHandler.connections.has(ioid))
        done()
      })

      actor.ioHandler.disconnect(link)
    })

    it('removing it twice should throw an error', () => {
      expect(() => {
        actor.ioHandler.disconnect(link)
      }).to.throw(/Cannot disconnect an unknown connection/)
    })

    it('reconnecting the link should be possible, gaining a new ioid', (done) => {
      actor.ioHandler.on('connect', () => {
        expect(actor.links.has(link.id)).to.equal(true)
        expect(link).to.have.property('ioid')
        expect(ioid).to.not.eql(link.ioid)
        assert.isTrue(actor.ioHandler.connections.has(link.ioid))
        done()
      })
      actor.ioHandler.connect(link)
    })
  })

  describe('Adding links with the same `id` twice', () => {
    let actor1: Actor
    let actor2: Actor
    let link1: Link
    let link2: Link

    it('Should be possible if they are added by different actors', async () => {
      // map is defined with link id's
      actor1 = await loadActor('map')

      // Note: At the moment link id's are not always stored within the map,
      // When links are not stored in the map id's are generated,
      // making them both unique even coming from the same subgraph.
      actor2 = await loadActor('map')

      link1 = actor1.links.values().next().value
      link2 = actor2.links.values().next().value

      expect(link1.id).to.eql(link2.id)
      expect(link1.ioid).to.not.eql(link2.ioid)
    })
  })

  describe('IoHandler instances', () => {
    let actor1: Actor
    let actor2: Actor

    it('different main actors have different IO Handler instances by default', async () => {
      actor1 = await loadActor('map')
      actor2 = await loadActor('map')

      expect(actor1.ioHandler).to.not.eql(actor2.ioHandler)
    })
  })

  describe.skip('Input & Output', () => {
    it('Test output() & receive() method', () => {})
  })
})
