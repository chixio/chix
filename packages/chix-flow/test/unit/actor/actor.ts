import {Loader} from '@chix/loader'
import {expect} from 'chai'
import {Actor, IoHandler, ProcessManager} from '@chix/flow'
// import { loadActor } from '../../helper'

describe('Actor:', () => {
  let actor: Actor

  it('Should be able to instantiate', () => {
    actor = new Actor()
    expect(actor).to.be.instanceOf(Actor)
    // tslint:disable-next-line:no-unused-expression
    expect(actor.id).to.not.be.empty
    expect(actor.type).to.eql('flow')
  })

  it('Should have an io handler', () => {
    expect(actor.ioHandler).to.be.instanceOf(IoHandler)
  })

  it('Should have a process manager', () => {
    expect(actor.processManager).to.be.instanceOf(ProcessManager)
  })

  it('Should have a default loader', () => {
    expect(actor.loader).to.be.instanceOf(Loader)
  })

  /*
  it.skip('Should be able to instantiate', (done) => {
    actor = loadActor('actions')
    const sub = actor.use('readFileWaitLog')
    sub.run()
    expect(sub.getNode('node2').context).to.eql({
      timeout: 0
    })
    sub.getNode('node3').on('complete', () => {
      setTimeout(() => {
        expect(sub.getNode('node1').runCount).to.eql(1)
        expect(sub.getNode('node2').runCount).to.eql(1)
        expect(sub.getNode('node3').runCount).to.eql(1)

        done()
      }, 500)
    })
  })
  */
})
