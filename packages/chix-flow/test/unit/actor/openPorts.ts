import {expect} from 'chai'
import {InputPort} from '@chix/flow'
import {loadActor} from '../../helper'

describe('Open Port Test:', () => {
  it('Connecting to a port multiple times should lead to one open port', async () => {
    const actor = await loadActor('openPorts')

    // There are already 2 connections to port in from node 2
    expect(actor.getNode('2').getInputPort('in').hasConnections()).to.equal(
      true
    )
    expect(
      actor.getNode('2').getInputPort('in').getConnections()
    ).to.have.length(2)

    // this tests if a third connection sending through an iip
    // doesn't create a wrong openPorts count
    actor.sendIIP(
      {
        id: '2',
        port: 'in',
      },
      'hi!'
    )
    expect(actor.getNode('2').getInputPort('in')).to.be.instanceOf(InputPort)
    expect(
      actor.getNode('2').getInputPort('in').getConnections()
    ).to.have.length(3)
  })
})
