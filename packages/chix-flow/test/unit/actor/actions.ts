/*
import {expect} from 'chai'
import { Actor } from '../../../actor'
import { loadActor } from '../../helper'

describe('Action test:', () => {
  it.skip('Action should be complete without callback', (done) => {
    const actor = loadActor('actions')
    const sub = actor.use('readFileWaitLog')
    sub.run()
    expect(sub.getNode('node2').context).to.eql({
      timeout: 0
    })
    sub.getNode('node3').on('complete', () => {
      setTimeout(() => {
        expect(sub.getNode('node1').runCount).to.eql(1)
        expect(sub.getNode('node2').runCount).to.eql(1)
        expect(sub.getNode('node3').runCount).to.eql(1)

        done()
      }, 500)
    })
  })

  it.skip('Should not have too many links', (done) => {

    // still diliberatly not providing a callback.
    // which is not implemented yet anyway.
    const actor = loadActor('too_many_links')
    const sub = actor.use('requireConnectRunTableListDb')
    expect(sub.map.links).to.have.length(4)

  })

  it.skip('Should run action with callback', (done) => {
    const actor = loadActor('actions')
    actor.action('readFileWaitLog').run((output) => {
      expect(Object.keys(output).length).to.eql(1)
      expect(output.node2).to.have.property('out')
      expect(output.node2.out).to.be.ok()

      done()
    })
  })

  it.skip('Should be able to use .use', (done) => {
    const actor = loadActor('actions')

    const context = {

    }

    const rfwl = actor.use('readFileWaitLog', context)
    expect(rfwl.map.env).to.eql('server')
    expect(rfwl.map.title).to.eql('readFileWaitLog')
    expect(rfwl.map.description).to.eql('')
    expect(rfwl.map.nodes).to.have.length(3)
    expect(rfwl.map.links).to.have.length(2)

    rfwl.run((output) => {
      expect(output).to.have.property('node2')
      expect(output.node2).to.have.property('out')
      expect(output.node2.out).to.be.ok()
      expect(Object.keys(output)).to.have.length(1)

      done()
    })
  })

  it.skip('Should be able to run, run, run', (done) => {
    const actor = loadActor('actions')

    const cb = (output, done) => {
      expect(output).to.have.property('node2')
      expect(output.node2).to.have.property('out')
      expect(output.node2.out).to.be.ok()
      Object.keys(output)).to.have.length(1)

      if (done) {
        done()
      }
    }

    actor.action('readFileWaitLog').run(cb)
    actor.action('readFileWaitLog').run(cb)
    actor.action('readFileWaitLog').run(cb)
    actor.action('readFileWaitLog').run(cb)
    actor.action('readFileWaitLog').run((output) => {
      cb(output, done)
    })
  })

  it.skip('Callback should have current Actor as this', (done) => {
    const actor = loadActor('actions')

    actor.action('readFileWaitLog').run(() => {
      expect(this).to.be.instanceof(Actor)

      done()
    })
  })

  it.skip('Should run only nodes from this action', (done) => {
    const actor = loadActor('actions')
    actor.action('readFileWaitLog').run((output) => {
      expect(this.getNode('node1').runCount).to.eql(1)

      expect(output.node2.out).to.be.ok()

      setTimeout(() => {
        // now test if node 2 and 3 are completed.
        expect(this.getNode('node2').runCount).to.eql(1)
        expect(this.getNode('node3').runCount).to.eql(1)
      }, 500)

      done()
    })
  })
})
 */
export {}
