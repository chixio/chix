import {loadActor} from '../../helper'

describe('On test:', () => {
  it('Should run on shutdown method before re-run', (done) => {
    loadActor('on').then((actor) => {
      const node1 = actor.getNode('1')

      node1.getOutputPort(':shutdown').on('data', () => {
        done()
      })

      actor.sendIIP(
        {
          id: '1',
          port: ':start',
        },
        ''
      )
      actor.sendIIP(
        {
          id: '1',
          port: ':start',
        },
        ''
      )
    })
  })
})
