import {expect} from 'chai'
import {Link} from '@chix/flow'
import {loadActor} from '../../helper'

describe('Flow test:', () => {
  it('Should add context', async () => {
    const actor = await loadActor('map')

    expect(Object.keys(actor.getNode('node1').context)).to.have.length(1)
  })

  it('Should fill ports', (done) => {
    loadActor('map').then((actor) => {
      const node1 = actor.getNode('node1')

      node1.getOutputPort(':complete').on('data', () => {
        expect(actor.getNode('node2').filled).to.eql(0)
        done()
      })

      actor.sendIIP(
        {
          id: 'node1',
          port: ':start',
        },
        ''
      )
    })
  })

  it('Should cleanup ports', (done) => {
    loadActor('map').then((actor) => {
      const node1 = actor.getNode('node1')

      node1.getOutputPort(':complete').on('data', () => {
        expect(actor.getNode('node1').filled).to.eql(1)
        done()
      })

      actor.sendIIP(
        {
          id: 'node1',
          port: ':start',
        },
        ''
      )
    })
  })

  it.skip('Within the callback this should be an instance of Actor', async () => {
    const actor = await loadActor('map')

    // expose port for callback
    actor.getNode('node1').getPort('output', 'out').setOption('expose', true)

    actor.sendIIP(
      {
        id: 'node1',
        port: ':start',
      },
      ''
    )
  })

  it('Should stop', async () => {
    const actor = await loadActor('ancestors')

    actor.sendIIP(
      {
        id: 'node1',
        port: ':start',
      },
      ''
    )
    actor.sendIIP(
      {
        id: 'node1',
        port: ':start',
      },
      ''
    )
    actor.sendIIP(
      {
        id: 'node1',
        port: ':start',
      },
      ''
    )
    actor.sendIIP(
      {
        id: 'node1',
        port: ':start',
      },
      ''
    )
    actor.sendIIP(
      {
        id: 'node1',
        port: ':start',
      },
      ''
    )
    actor.sendIIP(
      {
        id: 'node1',
        port: ':start',
      },
      ''
    )

    expect(actor.nodes.size).to.eql(4)

    expect(actor.links.size).to.eql(9) // 3 links, 6 IIPs
    expect(actor.iips.size).to.eql(6)
    actor.ioHandler.connections.forEach((link: Link) => {
      console.log(
        '%s:%s -> %s:%s',
        link.source.id,
        link.source.port,
        link.target.port,
        link.target.id
      )
    })
    expect(actor.ioHandler.connections.size).to.eql(9)

    // because the chain of events this is possible
    // stop at the ioHandler and queuHandler waits for this.
    // actor.ioHandler.on('drop', function(drop) {
    //    console.log(drop)
    // })

    // stop it immediately
    actor.stop()

    expect(actor.iips.size).to.eql(0)
    expect(actor.nodes.size).to.eql(4)
    expect(actor.links.size).to.eql(3)
    expect(actor.ioHandler.connections.size).to.eql(3)
  })

  // should determine what to do on reset.
  // more useful is too clear all queue data
  // clear the io manager, maybe rebuild all links.
  it('Should reset', async () => {
    const actor = await loadActor('ancestors')

    actor.sendIIP(
      {
        id: 'node1',
        port: ':start',
      },
      ''
    )

    expect(actor.nodes.size).to.eql(4)

    // impossible with setimmediate.
    // you do not know when the entire flow is finished.
    actor.reset()

    // check ioHandler

    // check queuemanager

    return
  })
})
