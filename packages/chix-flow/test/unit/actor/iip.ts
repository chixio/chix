import {assert, expect} from 'chai'
import {Actor, Link} from '@chix/flow'
import {loadActor} from '../../helper'

describe('IIPs', () => {
  let actor: Actor

  beforeEach(async () => {
    actor = await loadActor('map')
  })

  it('createIIP()', () => {
    const iip = actor.createIIP({
      data: 'bla',
      target: {
        id: 'node1',
        port: 'message',
      },
    })

    expect(iip).to.be.instanceOf(Link)

    expect(iip.data).to.eql('bla')

    // how many times this link filled a port
    expect(iip.fills).to.eql(0)

    // how many times this link was written to
    expect(iip.writes).to.eql(0)

    // On successful fill this link will seize to exist
    assert.isTrue(iip.setting.dispose)

    expect(iip.source.id).to.eql(actor.id)
    expect(iip.source.port).to.eql(':iip')

    expect(iip.target.id).to.eql('node1')
    expect(iip.target.port).to.eql('message')
  })

  it('connectIIP()', () => {
    const iip = actor.createIIP({
      data: 'bla',
      target: {
        id: 'node1',
        port: 'message',
      },
    })

    expect(actor.iips.size).to.eql(0)

    actor.connectIIP(iip)

    expect(actor.iips.size).to.eql(1)
  })

  it('sendIIP()', () => {
    const iip = actor.createIIP({
      data: 'bla',
      target: {
        id: 'node1',
        port: 'message',
      },
    })

    actor.connectIIP(iip)
    actor.__sendIIP(iip)

    expect(actor.iips.size).to.eql(1)
  })

  it('sendIIPs()', () => {
    actor.sendIIPs([
      {
        data: 'bla',
        target: {
          id: 'node1',
          port: 'message',
        },
      },
    ])

    expect(actor.iips.size).to.eql(1)
  })
})
