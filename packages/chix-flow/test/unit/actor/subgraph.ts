import {expect} from 'chai'
import {Flow} from '@chix/flow'
import {loadActor} from '../../helper'

describe('Connections', () => {
  describe('should autostart nodes which have', () => {
    it('context fulfilling all ports', (done) => {
      loadActor('with_sub').then((actor) => {
        const node1 = actor.getNode('node1')

        expect(node1.id).to.eql('node1')

        const node2 = actor.getNode('node2')

        expect(node2.id).to.eql('node2')

        const subgraphNode1 = (actor.getNode('subgraph1') as Flow).getNode(
          'subgraphNode1'
        )

        let c = 0

        const subgraphNode1CompletePort =
          subgraphNode1.getOutputPort(':complete')

        subgraphNode1CompletePort.on('data', (packet) => {
          const data = packet.read()

          expect(subgraphNode1.id).to.eql(data.node.id)

          expect(subgraphNode1.id).to.eql('subgraphNode1')

          expect(subgraphNode1.runCount).to.eql(1)

          if (++c === 3) {
            done()
          }
        })

        node1.getOutputPort(':complete').on('data', (p) => {
          expect(node1.id).to.eql('node1')
          expect(node1.runCount).to.eql(1)
          expect(node1.context.message.read(node1)).to.eql('Hello World!')

          p.release(node1)

          if (++c === 3) {
            done()
          }
        })

        node2.getOutputPort(':complete').on('data', (p) => {
          const data = p.setOwner(node2).read(node2)

          expect(data.node.id).to.eql('node2')
          expect(data.node.runCount).to.eql(1)

          p.release(node2)
          if (++c === 3) {
            done()
          }
        })

        expect(subgraphNode1.isStartable()).to.eql(false)

        // Set context on subgraph's node on port message (the only port )
        subgraphNode1.setContextProperty('message', 'Some value')

        expect(subgraphNode1.isStartable()).to.eql(true)

        actor.push()
      })
    })
  })

  describe('connected', () => {
    it('should run subgraph', (done) => {
      loadActor('with_sub_connected').then((actor) => {
        const node1 = actor.getNode('node1')

        expect(node1.id).to.eql('node1')

        const node2 = actor.getNode('node2')

        expect(node2.id).to.eql('node2')

        expect(actor.getNode('subgraph1')).to.be.instanceOf(Flow)

        const subgraphNode1 = (actor.getNode('subgraph1') as Flow).getNode(
          'subgraphNode1'
        )

        let c = 0

        subgraphNode1.getOutputPort(':complete').on('data', (packet) => {
          const data = packet.read()

          expect(subgraphNode1.id).to.eql(data.node.id)
          expect(subgraphNode1.id).to.eql('subgraphNode1')
          expect(subgraphNode1.runCount).to.eql(1)

          if (++c === 3) {
            done()
          }
        })

        node1.getOutputPort(':complete').on('data', (packet) => {
          expect(node1.id).to.eql('node1')
          expect(node1.runCount).to.eql(1)
          expect(node1.context.message.read(node1)).to.eql('Hello World!')

          packet.release(node1)

          if (++c === 3) {
            done()
          }
        })

        node2.getOutputPort(':complete').on('data', (p) => {
          const data = p.setOwner(node2).read(node2)

          expect(data.node.id).to.eql('node2')
          expect(data.node.runCount).to.eql(1)

          p.release(node2)
          if (++c === 3) {
            done()
          }
        })

        expect(subgraphNode1.isStartable()).to.eql(false)

        actor.push()
      })
    })
  })
})
