import {Node, NodeDefinition} from '@chix/common'
import {assert, expect} from 'chai'
import {Actor, xNode} from '@chix/flow'
import {loadYaml} from '../../../helper'

function testCreationalEvents(
  type: string,
  node: Node,
  nodeDefinition: NodeDefinition
) {
  describe('[Given a {type}]'.replace('{type}', type), function () {
    this.timeout(300)

    describe('Should emit creational events', () => {
      let actor: Actor

      beforeEach(() => {
        actor = new Actor()
      })

      it('Actor should emit an addNode event', async () => {
        return new Promise(async (resolve) => {
          actor.once('addNode', (event) => {
            expect(event.node.id).to.eql(node.id)
            expect(event.node.ns).to.eql(node.ns)
            expect(event.node.name).to.eql(node.name)

            resolve()
          })

          await actor.createNode(node, nodeDefinition)
        })
      })

      describe('The process manager', () => {
        it('should emit an addProcess event', (done) => {
          actor.processManager.on('addProcess', (proc: xNode) => {
            if (proc.id === node.id) {
              // expect(proc.id).to.eql(node.id)
              expect(proc.ns).to.eql(node.ns)
              expect(proc.name).to.eql(node.name)

              done()
            }
          })

          actor.createNode(node, nodeDefinition)
        })

        it('should be listening for start, stop and error events', (done) => {
          actor.processManager.once('addProcess', (proc: xNode) => {
            expect(proc.listeners('start')).to.have.lengthOf(1)
            expect(proc.listeners('stop')).to.have.lengthOf(1)
            expect(proc.listeners('error')).to.have.lengthOf(1)
            done()
          })

          actor.createNode(node, nodeDefinition)
        })
      })
    })
  })
}

describe('Actor:createNode', () => {
  describe('Without nodedefinition', () => {
    let actor: Actor

    before(() => {
      actor = new Actor()
    })

    it('Should not be able to create a node', async () => {
      try {
        await actor.createNode({ns: 'world', name: 'hello'} as any)
        assert.fail('Should not be able to create node')
      } catch (error) {
        expect(error).to.match(/Failed to get node definition/)
      }
    })
  })

  describe('With nodedefinition as second parameter', () => {
    let actor: Actor

    before(() => {
      actor = new Actor()
    })

    it('Should not be able to create a node without id', async () => {
      try {
        await actor.createNode(
          {
            ns: 'world',
            name: 'hello',
          } as any,
          {
            ns: 'world',
            name: 'hello',
            ports: {},
          }
        )
        assert.fail('Should not be able to create a node without id')
      } catch (error) {
        expect(error).to.match(/should have an id/)
      }
    })

    it('Should be able to create a node with id', async () => {
      await actor.createNode(
        {
          id: 'HelloWorld',
          ns: 'world',
          name: 'hello',
        },
        {
          ns: 'world',
          name: 'hello',
          title: 'Hi World',
          description: 'A world beyond',
          ports: {},
        }
      )
    })

    it('Actor should have stored the node', () => {
      expect(actor.getNode('HelloWorld')).to.be.instanceOf(xNode)
      // here a Map would be useful
      expect(actor.nodes.size).to.eql(1)
    })

    it('Node should have been registered with the processManager', () => {
      // unspecific
      expect(actor.processManager.getById('HelloWorld')).to.be.instanceOf(xNode)

      // specific
      expect(
        actor.processManager.getById('HelloWorld', actor)
      ).to.be.instanceOf(xNode)
    })

    it('Should have been assigned a process id', () => {
      expect(typeof actor.getNode('HelloWorld').pid).to.equal('string')
    })

    it("Should know it's parent", () => {
      expect(actor.getNode('HelloWorld').hasParent()).to.eql(true)
      expect(actor.getNode('HelloWorld').getParent()).to.eql(
        actor,
        'Matches actor'
      )
    })

    it('Should have been assigned an identifier', () => {
      expect(actor.getNode('HelloWorld').identifier).to.equal('world::hello-0')
    })

    it('Should inherit title and description', () => {
      expect(actor.getNode('HelloWorld').title).to.eql('Hi World')
      expect(actor.getNode('HelloWorld').description).to.eql('A world beyond')
    })

    it('Should be able to remove node', () => {
      actor.removeNode('HelloWorld')
      expect(Object.keys(actor.nodes)).to.have.lengthOf(0)
    })

    it('Node should be able to overwrite title & description', async () => {
      await actor.createNode(
        {
          id: 'HelloWorld2',
          ns: 'world',
          name: 'hello',
          title: 'Hey World!',
          description: 'A world beyond 2',
          context: {
            message: 'A world within',
          },
        },
        {
          ns: 'world',
          name: 'hello',
          title: 'Hi World',
          description: 'A world beyond',
          ports: {
            input: {message: {type: 'string'}},
          },
        }
      )
      expect(actor.getNode('HelloWorld2').title).to.eql('Hey World!')
      expect(actor.getNode('HelloWorld2').description).to.eql(
        'A world beyond 2'
      )
    })
  })

  // not sure if nothing will get modified..
  testCreationalEvents(
    'node',
    {
      id: 'HelloWorld2',
      name: 'hello',
      ns: 'world',
    },
    {
      ns: 'world',
      name: 'hello',
      ports: {},
    }
  )

  describe('With context', () => {
    let actor: Actor

    before(() => {
      actor = new Actor()
    })

    it('Node should have context set', async () => {
      const node = await actor.createNode(
        {
          id: 'HelloWorld2',
          ns: 'world',
          name: 'hello',
          title: 'Hey World!',
          description: 'A world beyond 2',
          context: {
            message: 'A world within.',
          },
        },
        {
          ns: 'world',
          name: 'hello',
          title: 'Hi World',
          description: 'A world beyond',
          ports: {
            input: {
              message: {
                type: 'string',
              },
            },
          },
        }
      )

      expect(actor.getNode('HelloWorld2').context).to.have.property('message')
      expect(actor.getNode('HelloWorld2').context.message.read(node)).to.eql(
        'A world within.'
      )
    })
  })

  describe('With provider property', () => {
    let actor: Actor

    before(() => {
      actor = new Actor()
    })

    // TODO: this must be forced to be a resolved provider
    it('Node should have provider set', async () => {
      await actor.createNode(
        {
          id: 'HelloWorld2',
          ns: 'world',
          name: 'hello',
          provider: 'x',
        },
        {
          ns: 'world',
          name: 'hello',
          title: 'Hi World',
          description: 'A world beyond',
          ports: {
            input: {
              message: {
                type: 'string',
              },
            },
          },
        }
      )

      expect(actor.getNode('HelloWorld2').provider).to.eql('x')
    })
  })

  describe('With a graph as node', () => {
    let actor: Actor

    testCreationalEvents(
      'subgraph',
      {
        id: 'my_id',
        ns: 'sub',
        name: 'graph',
      },
      loadYaml('subgraph_one_node')
    )

    before(() => {
      actor = new Actor()
    })

    describe('The ProcessManager', () => {
      // not sure if this should still be the case.
      // subgraphs as a node within the process manager...
      it.skip("Should contain 2 nodes (subgraph + it's node)", (done) => {
        actor.on('addNode', () => {
          expect(actor.processManager.processes.size).to.eql(2)

          const processes = actor.processManager.getProcesses()

          expect(processes[0].id).to.eql('flow:main')
          expect(processes[1].id).to.eql('node1')

          done()
        })

        actor.addMap(loadYaml('subgraph_one_node'))
      })
    })
    // TODO: status messages
  })
})
