import {expect} from 'chai'
import {Node, NodeDefinition} from '@chix/common'
import {Actor} from '@chix/flow'
import {loadYaml} from '../../../helper'

function testDestructionEvents(type: string, node: Node, def: NodeDefinition) {
  describe('[Given a {type}]'.replace('{type}', type), () => {
    describe('Should emit destruction events', () => {
      let actor: Actor

      beforeEach(async () => {
        actor = new Actor()

        await actor.createNode(node, def)
      })

      it('Actor should emit an removeNode event', (done) => {
        actor.once('removeNode', (ev) => {
          expect(ev.node.id).to.eql(node.id)
          expect(ev.node.ns).to.eql(node.ns)
          expect(ev.node.name).to.eql(node.name)
          done()
        })

        actor.removeNode(node.id)
      })

      describe('The process manager', () => {
        it('should emit an removeProcess event', (done) => {
          actor.processManager.once('removeProcess', (_node) => {
            expect(_node.id).to.eql(node.id)
            expect(_node.ns).to.eql(node.ns)
            expect(_node.name).to.eql(node.name)
            done()
          })

          actor.removeNode(node.id)
        })

        it('should stop listening for start, stop and error events', (done) => {
          actor.processManager.once('removeProcess', (_node) => {
            // weird..
            expect(_node.listeners('start')).to.have.lengthOf(0)
            expect(_node.listeners('stop')).to.have.lengthOf(0)
            expect(_node.listeners('error')).to.have.lengthOf(0)
            done()
          })

          actor.removeNode(node.id)
        })
      })
    })
  })
}

/**
 *
 * Will test whether each and every method is doing what it is suppose to
 *
 */
describe('Actor:removeNode', () => {
  describe('With a node', () => {
    // not sure if nothing will get modified..
    testDestructionEvents(
      'node',
      {
        id: 'HelloWorld2',
        name: 'hello',
        ns: 'world',
      },
      {
        name: 'hello',
        ns: 'world',
        ports: {},
      }
    )
  })

  describe('With a graph as node', () => {
    testDestructionEvents(
      'subgraph',
      {
        id: 'my_id',
        name: 'graph',
        ns: 'sub',
      },
      loadYaml('subgraph_one_node')
    )
  })
})
