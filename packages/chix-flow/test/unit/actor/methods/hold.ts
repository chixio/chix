import {expect} from 'chai'
import {loadActor} from '../../../helper'

describe('Actor:hold', () => {
  it('Should hold & release', (done) => {
    loadActor('hold').then((actor) => {
      const node1 = actor.getNode('node1')
      const node2 = actor.getNode('node2')
      const node3 = actor.getNode('node3')
      const node4 = actor.getNode('node4')

      // hold of 4, others are hold by it's setting: hold
      actor.hold('node4')

      // hold setting within the map does not seem to be used anywhere no more.
      // yet it is probably useful, to mark a target as hold.
      // not sure yet why though, but the UI worked that way.
      // Let's just ignore it for now, and hold node 3 overhere
      // (see hold.yaml)
      actor.hold('node3')

      node4.getOutputPort(':complete').on('data', () => {
        // console.log('node4 complete')

        expect(node1.runCount).to.eql(1)
        expect(node2.runCount).to.eql(1)
        expect(node3.runCount).to.eql(1)
        expect(node4.runCount).to.eql(1)
        done()
      })

      node3.getOutputPort(':complete').on('data', () => {
        // console.log('node3 complete')

        expect(node1.runCount).to.eql(1)
        expect(node2.runCount).to.eql(1)
        expect(node3.runCount).to.eql(1)
        expect(node4.runCount).to.eql(0)

        // release of 4
        actor.release('node4')
      })

      node2.getOutputPort(':complete').on('data', () => {
        // console.log('node2 complete')

        // node 2 > 3 is on hold (specified inside the map)
        expect(node1.runCount).to.eql(1)
        expect(node2.runCount).to.eql(1)
        expect(node3.runCount).to.eql(0)
        expect(node4.runCount).to.eql(0)

        expect(node3.status).to.eql('hold')

        // release of 3
        // returns the same as a fill (readyOrNot)
        // So it tells whether this release actually triggered the node
        // to start running.

        // 2 sends to 3 but 3 is on hold, what happens?
        // if correct it is accepted, and tries to run but then is halted.

        expect(node3.isHalted()).to.eql(true)

        node3.release()
      })

      expect(node3.status).to.eql('hold')
      expect(node4.status).to.eql('hold')

      expect(node3.isHalted()).to.eql(true, 'node3 is halted')
      expect(node4.isHalted()).to.eql(true, 'node4 is halted')

      actor.sendIIP(
        {
          id: 'node1',
          port: ':start',
        },
        ''
      )
    })
  })
})
