import {expect} from 'chai'
import {Actor, Connector} from '@chix/flow'

describe('Actor:plugPort', () => {
  let actor: Actor

  before(async () => {
    actor = new Actor()

    await actor.createNode(
      {
        id: 'HelloWorld',
        name: 'hello',
        ns: 'world',
      },
      {
        description: 'A world beyond',
        name: 'hello',
        ns: 'world',
        ports: {
          input: {
            in: {
              type: 'string',
            },
          },
        },
        title: 'Hi World',
        fn: () => {},
      }
    )
  })

  describe('The actor', () => {
    let target: Connector
    it('Should not be able to plug into non-existent port', () => {
      // non existing port
      expect(() => {
        target = new Connector()
        target.plug('HelloWorl', 'NI')

        actor.plugPort('input', target)
      }).to.throw(/does not exist/)
    })

    it('Should not be able to unplug non-existent port', () => {
      // non existing
      target = new Connector()
      target.plug('HelloWorl', 'in')

      expect(actor.unplugPort('input', target)).to.eql(false)
    })

    it('Should be able to plug port', () => {
      // existing port
      target = new Connector()
      target.plug('HelloWorld', 'in')

      actor.plugPort('input', target)
    })

    it.skip('Plugged port should have been opened', () => {
      // existing port
      expect(actor.getNode('HelloWorld').portIsOpen('in')).to.eql(true)
    })

    it('Plugged port without wire has no connection', () => {
      // TODO: not sure this fact has any value
      expect(
        actor.getNode('HelloWorld').getInputPort('in').hasConnections()
      ).to.eql(false)
    })

    it('should be able to unplug port', () => {
      // existing port
      actor.unplugPort('input', target)
      expect(
        actor
          .getNode('HelloWorld')
          .getInputPort(target.port as string)
          .hasConnections()
      ).to.eql(false)
    })
  })

  describe('When plugged by sending an IIP', () => {
    it('Node should have a connection', () => {
      actor.sendIIP(
        {
          id: 'HelloWorld',
          port: 'in',
        },
        'test'
      )
      // harder to test because it will quickly be gone.
      expect(
        actor.getNode('HelloWorld').getInputPort('in').hasConnections()
      ).to.eql(true)
    })
  })

  /*
    // non existing
    expect(actor.unplugPort('input', {
      id: 'HelloWorld',
      port: 'in'
    })).to.be.true
  */
})
