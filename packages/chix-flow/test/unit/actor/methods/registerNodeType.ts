import {expect} from 'chai'
import {Actor} from '@chix/flow'

describe('registerNodeType', () => {
  it('Should register node type', () => {
    const SomeNodeType = {name: 'someNodeType'}

    Actor.registerNodeType(SomeNodeType)

    expect(Actor.nodeTypes).to.hasOwnProperty('someNodeType')
    expect(Actor.nodeTypes.someNodeType.name).to.equal('someNodeType')
  })
})
