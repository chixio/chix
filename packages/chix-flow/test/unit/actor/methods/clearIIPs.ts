import {expect} from 'chai'
import {Actor} from '@chix/flow'
import {loadActor} from '../../../helper'

describe('Actor:clearIIPs', () => {
  let actor: Actor

  before(async () => {
    actor = await loadActor('map')
  })

  // iips behave like temporary links.
  // so e.g. during a reset, these should be cleared, but all normal
  // connections should not. An iip will go through the same life cycle
  // as normal links have, including their events etc.
  // also the queue manager only has notion of links, which can or can not be IIPs
  it('Should clear IIP', () => {
    // an iip is automatically disposed.
    actor.sendIIP(
      {
        id: 'node1',
        port: 'message',
      },
      'bla'
    )

    expect(actor.iips.size).to.eql(1)

    // TODO: what happens in between is not really tested
    actor.clearIIPs()

    expect(actor.iips.size).to.eql(0)
  })

  it('Clearing non existing IIP should silently fail', () => {
    actor.sendIIP(
      {
        id: 'node1',
        port: 'message',
      },
      'test'
    )

    expect(actor.iips.size).to.eql(1)

    actor.clearIIPs({
      id: 'node1',
      port: 'noexist',
    })

    expect(actor.iips.size).to.eql(1)

    actor.clearIIPs()

    expect(actor.iips.size).to.eql(0)
  })

  describe('Specifying target should clear the IIPs to that target', () => {
    it('Send first iip to node1:message', () => {
      actor.sendIIP(
        {
          id: 'node1',
          port: 'message',
        },
        'test2'
      )

      expect(actor.iips.size).to.eql(1)
    })

    it('Send second iip to node1:message', () => {
      actor.sendIIP(
        {
          id: 'node1',
          port: 'message',
        },
        'test3'
      )

      expect(actor.iips.size).to.eql(2)
    })

    it('Send third iip to node1:message', () => {
      actor.sendIIP(
        {
          id: 'node1',
          port: 'message',
        },
        'test3'
      )

      expect(actor.iips.size).to.eql(3)
    })

    // other target
    it('Send iip to node2:msg', () => {
      actor.sendIIP(
        {
          id: 'node2',
          port: 'msg',
        },
        'test3'
      )

      expect(actor.iips.size).to.eql(4)
    })

    it('Clear iips the iips to node1:message', () => {
      actor.clearIIPs({
        id: 'node1',
        port: 'message',
      })

      expect(actor.iips.size).to.eql(1)
    })

    it('Clear iips to node2:msg', () => {
      actor.clearIIPs({
        id: 'node2',
        port: 'msg',
      })

      expect(actor.iips.size).to.eql(0)
    })
  })
})
