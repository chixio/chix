import {expect} from 'chai'
import {Connector, Flow as Actor, Link} from '@chix/flow'
import {loadActor} from '../../../helper'

describe('Actor:changeLink', () => {
  let actor: Actor

  beforeEach(async () => {
    actor = await loadActor('map')
  })

  it('Should emit on target change', (done) => {
    const link = actor.getLink('my-link')

    link.target.once('change', (target: Connector) => {
      expect(target.get('persist')).to.eql(true)
      done()
    })

    link.target.set('persist', true)
  })

  it('Should emit on source change', (done) => {
    const link = actor.getLink('my-link')

    link.source.once('change', (source: Connector) => {
      expect(source.get('index')).to.eql(9)
      done()
    })

    link.source.set('index', 9)
  })

  it('Should emit on title change', (done) => {
    actor.once('changeLink', (link: Link) => {
      expect(link.id).to.eql('my-link')
      expect(link.metadata.title).to.eql('My Title')
      done()
    })

    // changes the metadata
    actor.getLink('my-link').setTitle('My Title')
  })

  it('Should be able to update entire link', (done) => {
    const link = actor.getLink('my-link')

    actor.once('changeLink', (ln: Link) => {
      // id should be unchanged
      expect(ln).to.eql(link)
      expect(ln.id).to.eql('my-link')
      expect(ln).to.have.property('metadata')
      expect(ln.metadata).to.eql({what: 'ever'})
      done()
    })

    // changes the metadata
    link.update({
      metadata: {
        what: 'ever',
      },
      source: {
        id: 'node1',
        port: 'out',
      },
      target: {
        id: 'node2',
        port: 'msg',
      },
    })
  })

  it('Should reset metadata', (done) => {
    const link = actor.getLink('my-link')

    actor.once('changeLink', (ln: Link) => {
      // id should be unchanged
      expect(ln).to.eql(link)
      expect(ln.id).to.eql('my-link')
      expect(ln).to.have.property('metadata')
      expect(ln.source.port).to.eql('tou')
      expect(ln.target.port).to.eql('gsm')
      expect(ln.metadata).to.eql({})
      done()
    })

    link.setMetadata({what: 'ever'})

    // reset the metadata
    link.update({
      source: {
        id: 'node1',
        port: 'tou',
      },
      target: {
        id: 'node2',
        port: 'gsm',
      },
    })
  })
})
