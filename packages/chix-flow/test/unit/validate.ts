import {validate} from '@chix/flow'
import {loadYaml} from '../helper'

/**
 *
 * Will test whether each and every method is doing what it is suppose to
 *
 */
describe('Validate', () => {
  describe('should be able to validate a flow', () => {
    it('with node definitions', () => {
      const map = loadYaml('map')
      validate.flow(map)
    })

    it('without node definitions', () => {
      const map = loadYaml('map')
      delete map.nodeDefinitions
      validate.flow(map)
    })
  })
})
