import * as fs from 'fs'
import * as path from 'path'
import {Context, Events} from '../src/schemas'

const writeFile = (json: any, file: string) => {
  fs.writeFileSync(
    path.join(__dirname, '../schemas', file),
    JSON.stringify(json, null, 2),
    'utf-8'
  )
}

writeFile(Context, 'context.json')
writeFile(Events, 'events.json')
