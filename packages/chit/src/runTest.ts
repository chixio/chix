import {NodeDefinition} from '@chix/common'
import {Actor} from '@chix/flow'
import {Loader} from '@chix/loader'

export async function runTest(nodeDefinition: NodeDefinition) {
  const loader = new Loader()

  loader.addNodeDefinition('@', nodeDefinition)

  const actor = new Actor()

  actor.setLoader(loader)

  return actor.createNode({
    id: 'test-node',
    name: nodeDefinition.name,
    ns: nodeDefinition.ns,
  })
}
