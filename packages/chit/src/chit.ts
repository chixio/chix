import {Command} from 'commander'
import * as fs from 'fs-extra'
import {globSync} from 'glob'
import * as jsonGate from 'json-gate'
import {extname, resolve} from 'path'

import {NodeDefinition, NodeSchema} from '@chix/common'
import {runTest} from './runTest'
import {validateTwigs} from './validateTwigs'

const program = new Command()
const nodeSchema = jsonGate.createSchema(NodeSchema)

let globPattern = '/nodes/**/node.js*'

const {version} = fs.readJSONSync(resolve(__dirname, '../package.json'))

program
  .version(version)
  .usage('[options]')
  .option('-g, --glob', 'globbing pattern default: ' + globPattern)
  .option(
    '-r, --run-twigs',
    'run the twigs, only use this for non-listening stuff'
  )
  .option('-n, --test [node]', 'test node')
  .option('-b, --base-dir [name]', 'base dir')
  .option('-v, --verbose', 'be verbose')
  .parse(process.argv)

const options = program.opts()

const dirname = options.baseDir ? options.baseDir : process.cwd()

globPattern = options.glob || globPattern

const pkg = fs.readJSONSync(resolve(dirname, 'package.json'))

;(async () => {
  if (!pkg.chix) {
    throw Error(
      [
        'The package.json of package',
        `"${pkg.name}"`,
        'does not have a "chix" section',
        '\n\n\t',
        'Note: chit must be run from a nodule directory',
        '\n',
      ].join(' ')
    )
  }

  if (options.verbose) {
    console.log('Globbing' + dirname + globPattern)
  }

  const files = globSync(dirname + globPattern)

  for (const file of files) {
    const node: Partial<NodeDefinition> = {}

    const parts = file.split('/')
    const nodule = parts[1]
    const ext = extname(file)

    if (options.verbose) {
      console.log('Reading file:', file)
    }

    const contents = fs.readFileSync(file, 'utf-8')

    if (ext === '.js') {
      node.fn = contents // warning: relies on .js before .json
    } else if (ext === '.json') {
      const json = JSON.parse(contents)

      json.fn = node.fn

      if (node.env) {
        json.env = node.env
      } else if (pkg.chix.env) {
        json.env = pkg.chix.env
      }

      if (options.test && options.test === nodule) {
        nodeSchema.validate(json)

        const testFile = ['./nodes', nodule, 'test.json'].join('/')

        if (fs.existsSync(testFile)) {
          await runTest(JSON.parse(fs.readFileSync(testFile, 'utf-8')))
        }
      } else {
        // validate json
        nodeSchema.validate(json)
        if (options.verbose) {
          console.log('validated:', file)
        }
      }
    }
  }

  const twigFiles = globSync(dirname + '/twigs/*.fbp')

  validateTwigs(pkg.chix.name, twigFiles)
})()
