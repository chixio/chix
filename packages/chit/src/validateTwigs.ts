import {FlowSchema} from '@chix/common'
import * as fs from 'fs-extra'
import * as jsonGate from 'json-gate'
import * as path from 'path'
import {getParser} from './getParser'

const mapSchema = jsonGate.createSchema(FlowSchema)

export function validateTwigs(ns: string, twigFiles: string[]) {
  for (const twigFile of twigFiles) {
    const parser = getParser()

    const flow = parser.parse(fs.readFileSync(twigFile, 'utf-8'))
    const cname = path.basename(twigFile, '.fbp')

    flow.ns = ns
    flow.name = cname

    mapSchema.validate(flow)
  }
}
