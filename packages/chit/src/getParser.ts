import {ChixRenderer, FBPParser} from '@chix/fbpx'

export function getParser(useUUID?: boolean) {
  const parser = new FBPParser()
  const renderer = new ChixRenderer()

  renderer.skipIDs = !useUUID
  parser.addRenderer(renderer)

  return parser
}
