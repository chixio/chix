Chit - Chiχ Package Tester
==========================

Will test a Chiχ Package repository for correctness.


Test example:

```bash
chit -r
```
