Chiχ Loader
=========

[![Build Status](https://travis-ci.org/psichi/chix-loader.png)](https://travis-ci.org/psichi/chix-loader)

This is the base component loader for Chiχ.

Chiχ uses node definitions to describe each of it's components.

The loader takes care of loading these definitions and caching them.

On top of the base loaders there are loaders which load the definitions from
a remote server or from the file system.

The base loader accepts definitions added manually.

Definitions are indexed by provider url, but the manual loader uses '@' to indicate
the definitions are local.

Example usage:
```javascript

var Loader = require('chix-loader');

var loader = new Loader();

loader.addNodeDefinitions('@', [{
  ns: 'my', name: 'component',
  ports: {
    input: {
      'in': {
          type: 'string'
      }
    },
    output: {
      out: {
          type: 'string'
        }
      }
    }
  }, {
    ns: 'my', name: 'component2',
    ports: { input: { in: { type: 'string' } } }
}]);

// retrieve a node definitions from a provider
loader.getNodeDefinitionFrom('@', 'my', 'component');

// Checks whether the definition is available
loader.hasNodeDefinition('@', 'my', 'component'); // true

// Given a node and it's map, will try and load the (remote) node.
// TODO: Explain provider structure within a map
loader.getNodeDefinition({
 ns: 'my',
 name: 'component'
}, map);

```

# collecting dependencies

It's also the loaders job to collect the dependencies needed to run a graph.
This is rather simple if a graph only contains non-composite nodes, but quickly
becomes more complex when subgraphs are used and the number of subgraphs are unknown.
The loader keeps track of which requires are needed.
