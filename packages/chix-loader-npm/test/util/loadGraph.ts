import * as fs from 'fs'
// tslint:disable-next-line:no-implicit-dependencies
import * as yaml from 'js-yaml'
import * as path from 'path'

export function loadGraph(name: string) {
  return yaml.load(
    fs.readFileSync(
      path.resolve(__dirname, '../fixtures/', `${name}.yaml`),
      'utf8'
    )
  )
}
