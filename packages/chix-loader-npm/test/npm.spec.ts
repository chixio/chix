import {Flow as FlowDefinition} from '@chix/common'
import {assert, expect} from 'chai'
import * as path from 'path'
import {NPMLoader} from '@chix/loader-npm'
import {loadGraph} from './util'

const testOptions = {
  paths: [
    path.resolve(__dirname, '../../../../'), // lerna root
  ],
}

describe('NPM Loader:', () => {
  let loader: NPMLoader
  let graph: FlowDefinition

  before(() => {
    loader = new NPMLoader(testOptions)
  })

  it('Should not have a default provider', () => {
    expect(loader).to.not.have.property('defaultProvider')
  })

  it('The graph fixture itself should be correct', () => {
    graph = loadGraph('npm') as FlowDefinition

    expect(graph).to.not.have.property('providers')

    expect(graph.nodes).to.be.an('array')

    expect(graph.nodes[0]).to.not.have.property('provider')
    expect(graph.nodes[1]).to.not.have.property('provider')
  })

  // this asumes chix-test is installed as dev dependency
  it('Should be able to load @ definitions', async () => {
    let result

    result = await loader.load(graph)

    const {nodeDefinitions} = result

    expect(nodeDefinitions).to.have.property('@')
    expect(nodeDefinitions['@']).to.have.property('test')
    expect(nodeDefinitions['@'].test).to.have.property('Repeat')
    expect(nodeDefinitions['@'].test).to.have.property('Drop')

    assert.isTrue(loader.hasNodeDefinition('@', 'test', 'Repeat'))
    assert.isTrue(loader.hasNodeDefinition('@', 'test', 'Drop'))

    /* add several other nodes to test/ which does include this stuff.
      result).to.have.property('dependencies')
      result.dependencies).to.eql({
        npm: {
          bogus: '0.x.x'
        }
      })
    */

    result = await loader.loadNodeDefinitionFrom('@', 'test', 'Repeat')

    /*
      const expected = {
        npm: {
          bogus: '0.x.x'
        }
      }
    */
    expect(result.dependencies).to.eql({})

    result = await loader.loadNodeDefinitionFrom('@', 'test', 'Drop')

    expect(result.nodeDefinition.ns).to.eql('test')
    expect(result.nodeDefinition.name).to.equal('Drop')
    expect(result.dependencies).to.eql({})
  })

  it('Should be able to preload providers', async () => {
    const npmLoader = new NPMLoader(testOptions)

    const {provider, nodeDefinitions} = await npmLoader.preload()

    expect(provider).to.eql('@')

    expect(nodeDefinitions).to.eql(loader.getNodeDefinitions('@'))
  })
})
