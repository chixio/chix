import {
  Flow as FlowDefinition,
  NodeDefinitionProviderMap,
  NodeDefinitions,
} from '@chix/common'
import {Loader, LoaderResponse, PreloadResult} from '@chix/loader'
import Debug from 'debug'
import {globSync} from 'glob'
import * as path from 'path'

const debug = Debug('chix-loader:npm')

export interface NPMLoaderOptions {
  paths?: string[]
}

/**
 * Loads and saves definitions from npm.
 *
 * If npm is used, graphs cannot contain providers.
 *
 * All components must be available through npm installed packages.
 */
export class NPMLoader extends Loader {
  public name = 'NPMLoader'
  public pkgKey = 'chix'
  public sources = {
    '@': {},
  }
  public options = {
    paths: [process.cwd()],
  }

  constructor(options?: NPMLoaderOptions) {
    super()

    if (options && options.paths && Array.isArray(options.paths)) {
      this.options.paths.push(...options.paths)
    }
    this._init()
  }

  public _init() {
    function load(dir: string, self: NPMLoader) {
      const pattern = path.resolve(dir) + '/node_modules/*/package.json'

      let cnt = 0
      const packageFiles = globSync(pattern)

      packageFiles.forEach((packageFile) => {
        const pkg = require(packageFile)
        const chixSection = pkg[self.pkgKey]

        if (chixSection) {
          debug('Found `%s` package', chixSection.name)

          cnt++

          const loc = path.dirname(packageFile)
          const nodule = require(loc)

          Object.keys(nodule).forEach((key) => {
            self.addNodeDefinition('@', nodule[key])

            debug('Added @ %s:%s', nodule[key].ns, nodule[key].name)
          })

          // npm 2, nested does not happen anymore, but let's just keep it
          load(path.join(dir, pkg.name), self)
        }
      })

      if (cnt > 0) {
        debug('Found %d packages within %s', cnt, dir)
      }
    }

    this.options.paths.forEach((dir) => load(dir, this))
  }

  public async preload(): Promise<PreloadResult> {
    return {
      provider: '@',
      nodeDefinitions: this.getNodeDefinitions('@') as NodeDefinitions,
    }
  }

  /**
   * Node Definitions from npm are already preloaded.
   *
   * In this case load just checks whether graph dependencies are
   * all known to the loader or else send an error back to the callback.
   */
  public async load(
    flow: FlowDefinition | FlowDefinition[]
  ): Promise<LoaderResponse> {
    const flows = Array.isArray(flow) ? flow : [flow]

    flows.forEach((_flow) => {
      if (!this.isResolved(_flow)) {
        const unresolved = this.getUnresolved(_flow)

        const node = unresolved[0]

        throw Error(
          `NodeDefinition for ${node.ns}:${node.name} not installed\n\tPlease install it first using \`npm i chix-${node.ns}\``
        )
      }
    })

    return {
      providerLocation: 'npm', // not used I hope
      // these should probably only be the resolved ones from the map
      nodeDefinitions: this.getNodeDefinitions() as NodeDefinitionProviderMap,
    }
  }

  /**
   *
   * NPM loader does not store it's definitions
   *
   */
  public saveNodeDefinition() {
    // nop
  }
}
