import * as fs from 'fs'
import * as path from 'path'

const pkg = JSON.parse(
  fs.readFileSync(path.resolve(__dirname, '../package.json'), 'utf-8')
)

const contents = `
export const name = '${pkg.name}'
export const version = '${pkg.version}'
`.trim()

fs.writeFileSync(path.resolve(__dirname, '../src/version.ts'), contents)
