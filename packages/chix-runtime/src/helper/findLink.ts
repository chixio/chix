import {Shared} from '@chix/fbp-protocol'
import {Flow} from '@chix/flow'

export function findLink(graph: Flow, ln: Shared.Edge) {
  let link

  for (const l of graph.links.values()) {
    if (
      l.source.id === ln.src.node &&
      l.source.port === ln.src.port &&
      l.target.port === ln.tgt.port &&
      l.target.id === ln.tgt.node
    ) {
      link = l
      break
    }
  }

  return link
}
