import {Link as LinkDefinition} from '@chix/common'
import {Graph} from '@chix/fbp-protocol'
import {Link} from '@chix/flow'
import {config} from '../config'

export function updateLinkFromPayload(
  link: Link,
  payload: Graph.Input.ChangeEdgePayload
) {
  let persist
  // TODO: must ensure persist is reset (update probably does)
  if (payload.metadata && payload.metadata.route === config.persist_route) {
    persist = true
  }

  const linkDefinition: LinkDefinition = {
    source: {
      id: payload.src.node,
      port: payload.src.port,
      setting: {
        index: payload.src.index,
      },
    },
    metadata: payload.metadata,
    target: {
      id: payload.tgt.node,
      port: payload.tgt.port,
      setting: {
        index: payload.tgt.index,
        persist,
      },
    },
  }

  // metadata should survive and be set on link.
  // will trigger the change event.
  link.update(linkDefinition)
}
