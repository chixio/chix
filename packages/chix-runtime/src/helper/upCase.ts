export function upCase(port: string, upper?: boolean) {
  if (upper) {
    return port.toUpperCase()
  }

  return port
}
