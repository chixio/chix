import {upCase} from './upCase'
import {Link} from '@chix/common'

export function toDataId(link: Link, upper?: boolean) {
  const from = [link.source.id + '()', upCase(link.source.port, upper)].join(
    ' '
  )
  const to = [upCase(link.target.port, upper), link.target.id + '()'].join(' ')

  if (link.source.port === ':iip') {
    return 'DATA -> ' + to
  }

  return [from, '->', to].join(' ')
}
