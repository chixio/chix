import {Link} from '@chix/flow'

export function toPayload(link: Link) {
  if (!link.graphPid) {
    throw Error('Graph Pid must be set.')
  }
  return {
    // id: link.ioid,
    id: link.id,
    src: {
      node: link.source.id,
      port: link.source.port,
    },
    tgt: {
      node: link.target.id,
      port: link.target.port,
    },
    graph: link.graphPid,
    // subgraph: not sure..
  }
}
