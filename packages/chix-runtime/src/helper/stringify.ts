// Safe, fast & compact serialization
// https://github.com/noflo/noflo-runtime-base/blob/master/src/protocol/Network.coffee#L19
import isBuffer from 'is-buffer'

export function stringify(data: any) {
  if (data === null) {
    return null
  }
  if (isBuffer(data)) {
    data = data.slice(0, 20)
  }
  let ret = ''
  if (data.toJSON) {
    ret = data.toJSON()
  }

  if (data.toString) {
    ret = data.toString()
    if (ret === '[object Object]') {
      try {
        ret = JSON.parse(JSON.stringify(data))
      } catch (ignore) {}
    }
  } else {
    ret = data
  }

  return ret
}
