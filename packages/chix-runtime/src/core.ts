/* global console */
import {ILogger} from '@chix/common'
import {EventEmitter} from 'events'
import {nop, NopLogger} from './logger'

export type CoreOptions = {
  logger?: ILogger
}

/**
 *
 * Core object
 *
 * Mainly used for a consistent logging interface
 *
 */
export class Core extends EventEmitter {
  public options: CoreOptions
  public logger: ILogger
  constructor(options: CoreOptions) {
    super()
    this.options = options || {}

    if (this.options.logger) {
      if (this.options.logger.constructor.name === 'Console') {
        this.logger = Object.create(console)
        this.logger.debug = this.logger.info
        this.logger.addLevel = nop
      } else {
        this.logger = this.options.logger
      }
    } else {
      this.logger = new NopLogger()
    }
  }
}
