export type FlowType = 'chix' | 'noflo'

const defaultProvider = 'https://api.chix.io/v1'

export interface RuntimeConfig {
  providers: {
    remote: {
      provider: string
      collection: string
    }
    npm: {
      provider: string
    }
  }

  source: {
    flowType: FlowType
  }

  persist_route: number
}

export class Config implements RuntimeConfig {
  public _provider: string = 'npm'

  public providers = {
    remote: {
      provider: `${defaultProvider}/nodes/rhalff/{ns}/{name}`,
      collection: `${defaultProvider}/nodes/rhalff`,
    },
    npm: {
      provider: '@',
    },
  }

  public source = {
    flowType: 'chix' as FlowType,
  }

  public persist_route = 10

  constructor(config_?: RuntimeConfig) {
    Object.assign(this, config_)
  }

  get provider() {
    return this.providers[this._provider].provider
  }

  public setProvider(name) {
    this._provider = name
  }
}

export const config = new Config()
