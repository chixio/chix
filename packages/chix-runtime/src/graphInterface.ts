import {ILogger} from '@chix/common'
import {Config} from '@chix/config'
import browserify from 'browserify'
import * as fs from 'fs'
import {IncomingMessage, ServerResponse} from 'http'
import * as path from 'path'
import replaceStream from 'replacestream'

const config = new Config()

export function serveMe(
  response: ServerResponse,
  file: string,
  logger: ILogger
) {
  return (): void => {
    logger.info('GraphInterface, serving', file)

    fs.readFile(file, 'utf8', (err, data) => {
      if (err) {
        response.writeHead(404)
        response.end()
      } else {
        response.writeHead(200, {
          'Content-Type': 'text/html',
        })
        response.end(data)
      }
    })
  }
}

export type GraphInterfaceOptions = {
  interface?: string
}

export function GraphInterface(
  options: GraphInterfaceOptions,
  logger: ILogger
) {
  return (request: IncomingMessage, response: ServerResponse): void => {
    let file

    const page = options.interface || 'default'

    if (/bundle\.js$/.test(request.url)) {
      response.setHeader('content-type', 'application/javascript')

      const name = request.url.split('/')[1]
      const stream = fs
        .createReadStream(`./www/${name}.js`, 'utf-8')
        .pipe(replaceStream('ACCESS_TOKEN', config.get('access_token')))

      const b = browserify(stream).bundle()
      b.on('error', console.error)
      b.pipe(response)

      logger.info('GraphInterface, compiled', request.url)
    } else {
      const d = request.url.replace(/^\//, '') || page + '.html'
      file = path.resolve(__dirname, '../www/' + d)

      fs.exists(file, serveMe(response, file, logger))
    }
  }
}
