export function nop() {
  return
}

export class NopLogger {
  public info = nop
  public warn = nop
  public debug = nop
  public error = nop
  public addLevel = nop
}
