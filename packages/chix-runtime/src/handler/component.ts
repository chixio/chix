import {Transporter, TransporterOptions} from '../transporter'
import * as commands from './protocol/fbp/component/index'
import {Loader} from '@chix/loader'
import {ProcessManager} from '@chix/flow'
import {BaseTransport} from '@chix/transport'
import {ILogger} from '@chix/common'

export type ComponentHandlerOptions = {
  compat?: boolean
} & TransporterOptions

export class ComponentHandler extends Transporter {
  public static handle(
    loader: Loader,
    transport: BaseTransport,
    processManager: ProcessManager,
    logger?: ILogger,
    options?: ComponentHandlerOptions
  ) {
    return new ComponentHandler({
      ...options,
      logger,
      transport,
    }).handle(loader, processManager)
  }

  public commands: any
  public options: ComponentHandlerOptions = {}

  constructor(options: ComponentHandlerOptions) {
    super({
      logger: options.logger,
      transport: options.transport,
    })
    this.commands = {
      getsource: commands.getsource.bind(this),
      list: commands.list.bind(this),
      source: commands.source.bind(this),
    }

    this.options.compat = !!options.compat

    this.onComponent = this.onComponent.bind(this)
  }
  public onComponent(data, conn) {
    if (commands[data.command]) {
      this.logger.debug('component', data.command, data.payload)
      this.commands[data.command](
        data.payload,
        conn,
        this.transport,
        this.processManager,
        this.loader,
        this.logger
      )
    } else {
      this.transport.reply(
        'component',
        'error',
        {
          message: 'Unknown command:' + data.command,
        },
        conn
      )
    }
  }

  /**
   *
   * Handles the Components
   *
   * Usage:
   *
   *   var componentHandler = new ComponentHandler()
   *   componentHandler.handle(loader, transport)
   *
   *
   * @param {Loader} loader
   * @param {ProcessManager} processManager
   */
  public handle(loader: Loader, processManager: ProcessManager) {
    // fix this
    this.loader = loader
    this.processManager = processManager
    this.transport.on('component', this.onComponent)
  }
}
