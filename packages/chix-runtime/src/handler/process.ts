import {ILogger} from '@chix/common'
import {ProcessManager} from '@chix/flow'
import {BaseTransport} from '@chix/transport'
import {Transporter, TransporterOptions} from '../transporter'
import * as fbpxCommands from './protocol/fbpx/process/index'

const commands = {
  ...fbpxCommands,
}

export type ProcessHandlerOptions = {} & TransporterOptions

export class ProcessHandler extends Transporter {
  public static handle(
    processManager: ProcessManager,
    transport: BaseTransport,
    logger?: ILogger,
    options?: ProcessHandlerOptions
  ) {
    const handler = new ProcessHandler({
      logger,
      transport,
      ...options,
    })

    handler.handle(processManager)

    return handler
  }

  public options: ProcessHandlerOptions = {}

  constructor(options: ProcessHandlerOptions) {
    super({
      logger: options.logger,
      transport: options.transport,
    })

    this.onProcess = this.onProcess.bind(this)
  }

  public onProcess(data, conn) {
    if (commands[data.command]) {
      this.logger.debug('process', data.command, data.payload)
      commands[data.command].apply(this, [data.payload, conn])
    }
  }

  /**
   *
   * Handles the Process
   *
   * Usage:
   *
   *   const processHandler = new ProcessHandler()
   *   processHandler.handle(transport)
   *
   * @param {ProcessManager} processManager
   */
  public handle(processManager: ProcessManager) {
    this.processManager = processManager
    this.transport.on('process', this.onProcess)
  }
}
