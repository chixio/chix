import {Network, Shared} from '@chix/fbp-protocol'
import {Flow, ProcessManager} from '@chix/flow'
import * as tv4 from 'fbp-protocol'
import {Transporter, TransporterOptions} from '../transporter'
import * as commands from './protocol/fbp/network/index'
import {BaseTransport} from '@chix/transport'
import {ILogger} from '@chix/common'

export type NetworkHandlerOptions = {} & TransporterOptions

export class NetworkHandler extends Transporter {
  public static handle(
    processManager: ProcessManager,
    transport: BaseTransport,
    logger?: ILogger
  ) {
    const networkHandler = new NetworkHandler({
      logger,
      transport,
    })

    networkHandler.handle(processManager)

    return networkHandler
  }
  constructor(options: NetworkHandlerOptions) {
    super({
      logger: options.logger,
      transport: options.transport,
    })

    this.onNetwork = this.onNetwork.bind(this)
  }

  public onNetwork(message: Shared.Message, conn) {
    const {payload, command} = message

    // message.payload.graph is correct, update tests!
    /* Test this per message, not all need .graph to be available
    if (!payload || !payload.graph) {
      this.logger.warn('networkHandler', 'No graph specified', message)
      this.transport.reply('network', 'error', {
        message: Error('No graph specified')
      }, conn)
      return
    }
    */

    // We get the graphid but we need the process_id
    // this.logger.warn('dump graph', 'darn', message)

    let graph: Flow

    const schema = `/network/input/${command}`

    const result = tv4.validateMultiple(message, schema)

    if (!result.valid) {
      const error = result.errors[0]

      this.transport.reply(
        'network',
        'error',
        {
          message: `${command}: ${error.dataPath}: ${error.message}`,
        },
        conn
      )

      return
    }

    if (payload && payload.graph) {
      graph = this.processManager.getById(payload.graph) as Flow

      if (!graph) {
        graph = this.processManager.getProcess(payload.graph) as Flow
      }

      if (!graph) {
        this.logger.warn('networkHandler', 'Unable to find graph', message)

        const error = Error('Unable to find graph')

        const errorPayload: Network.Output.ErrorPayload = {
          message: error.toString(),
        }

        this.transport.reply('network', 'error', errorPayload, conn)

        return
      }
    }

    if (commands[command]) {
      // validate payload here.
      this.logger.debug('network', command, payload)
      commands[command].apply(this, [payload, conn, graph])
    } else {
      this.logger.warn('networkHandler', 'Unknown command: ' + command)

      const error = Error('Unknown command')

      const errorPayload: Network.Output.ErrorPayload = {
        message: error.toString(),
      }

      this.transport.reply('network', 'error', errorPayload, conn)
    }
  }

  /**
   *
   * Handles the Network
   *
   * Usage:
   *
   *   const networkHandler = new NetworkHandler()
   *   networkHandler.handle(processManager, ioHandler, transport)
   *
   * @param {ProcessManager} processManager
   */
  public handle(processManager: ProcessManager) {
    this.processManager = processManager
    this.transport.on('network', this.onNetwork)
  }
}
