import {Flow as FlowDefinition, IIP as IIPDefinition} from '@chix/common'
import {FBPParser} from '@chix/fbpx'
import {Flow, IoHandler, ProcessManager} from '@chix/flow'
import {DependencyLoader, Loader} from '@chix/loader'
import {Actor as FlowMonitor} from '@chix/monitor-npmlog'
import {get} from 'lodash'
import {config} from '../config'
import {Transporter, TransporterOptions} from '../transporter'
import * as fbpCommands from './protocol/fbp/graph/index'
import * as fbpxCommands from './protocol/fbpx/graph/index'

const commands = {
  ...fbpCommands,
  ...fbpxCommands,
}

export type ProcessListeners = {
  [key: string]: (event: any) => void
}

export type GraphHandlerOptions = {} & TransporterOptions

export type AddGraphOptions = {
  no_ids?: boolean
  start?: boolean
  install?: boolean
  iips?: IIPDefinition[]
}

export type AddGraphDefaults = {
  ns?: string
  name?: string
}

const _KEY_ = 'GraphHandler'

export class GraphHandler extends Transporter {
  public static handle(
    processManager,
    io,
    loader,
    transport,
    logger?,
    dependencyLoader?
  ) {
    const graphHandler = new GraphHandler({
      logger,
      transport,
    })

    graphHandler.handle(processManager, io, loader, dependencyLoader)

    return graphHandler
  }

  public processListeners: ProcessListeners
  public ioHandler: IoHandler
  public dependencyLoader: DependencyLoader

  constructor(options: GraphHandlerOptions) {
    super({
      logger: options.logger,
      transport: options.transport,
    })
    this.onGraph = this.onGraph.bind(this)
    this.processListeners = {}
  }

  /**
   *
   * Handles the graph
   *
   * Usage:
   *
   *   const graphHandler = new GraphHandler()
   *   graphHandler.handle(processManager, transport)
   *
   * The graph handler expects the processManager to already contain the graphs.
   * These graphs can be initialized using the network protocol.
   *
   * These graphs are created during `clear`
   *
   * One important thing is subscription.
   * Noflo keeps the Graphs in Graphs.coffee. payload.id is the key.
   * init and subscribe graph happens during clear.
   */
  public handle(
    processManager: ProcessManager,
    ioHandler: IoHandler,
    loader: Loader,
    dependencyLoader?: DependencyLoader
  ) {
    this.processManager = processManager
    this.ioHandler = ioHandler
    this.loader = loader
    this.dependencyLoader = dependencyLoader

    this.transport.on('graph', this.onGraph)
  }

  public onGraph(data, conn) {
    let graph

    console.log('GOT GRAPH PROTOCOL MESSAGE', data)

    if (data.command === 'error') {
      console.log('FIX ME:', data)
      throw Error('gimme a trace')
    }

    // check is wrong, clear _does_ need graph right?
    if (data.command !== 'clear' && data.command !== 'create') {
      if (!data.payload.graph) {
        this.logger.warn('GraphHandler', 'No graph specified', data.payload)
        return this.transport.reply(
          'graph',
          'error',
          {
            message: Error('No graph specified'),
          },
          conn
        )
      }

      graph = this.processManager.getById(data.payload.graph)

      if (!graph) {
        // new preferred way
        try {
          graph = this.processManager.getProcess(data.payload.graph)
        } catch (error) {
          this.logger.warn('GraphHandler', 'Unable to find graph', data.payload)

          return this.transport.reply(
            'graph',
            'error',
            {
              message: Error('Requested graph not found'),
            },
            conn
          )
        }
      }

      // TODO: buggy for sure
      if (!this.processListeners[data.payload.graph]) {
        this.processListeners[data.payload.graph] = (event) => {
          this.transport.sendAll('network', 'error', {
            message: event.msg,
            graph,
          })
        }

        // TODO: how to know squeeze the graph/network in here?
        this.processManager.on(
          'error',
          this.processListeners[data.payload.graph]
        )
      }
    }

    // funky construction
    if (commands.hasOwnProperty(data.command)) {
      this.logger.debug(
        'graph',
        data.command,
        data.payload,
        graph && graph.identifier
      )
      commands[data.command].apply(this, [graph, data.payload, conn, 0])
    } else {
      this.logger.warn('graph', 'Unknown command', data.command)
      this.transport.reply(
        'graph',
        'error',
        {
          message: Error('Unknown command'),
          graph: graph.id,
        },
        conn
      )
    }
  }

  // used by cli to feed an initial graph
  public async loadFbp(
    contents: string,
    parser: FBPParser,
    options: AddGraphOptions = {},
    defaults: AddGraphDefaults = {}
  ) {
    const graph = parser.parse(contents)

    if (!graph.ns) {
      if (!defaults.ns) {
        throw Error('Graph has no ns and no default.ns specified')
      }

      graph.ns = defaults.ns
    }

    if (!graph.name) {
      if (!defaults.name) {
        throw Error('Graph has no name and no default.name specified')
      }

      graph.name = defaults.name
    }

    return this.initMap(graph, options.start, parser.getIIPs(), options.install)
  }

  public async loadJson(
    graph: FlowDefinition,
    options: AddGraphOptions = {},
    defaults: AddGraphDefaults = {}
  ) {
    // read fbpx, support others later

    if (!graph.ns) {
      if (!defaults.ns) {
        throw Error('Graph has no ns and no default.ns specified')
      }

      graph.ns = defaults.ns
    }

    if (!graph.name) {
      if (!defaults.name) {
        throw Error('Graph has no name and no default.name specified')
      }

      graph.name = defaults.name
    }

    return this.initMap(graph, options.start, options.iips, options.install)
  }

  public nodeExists(graph, nodeId: string, conn) {
    if (graph.hasNode(nodeId)) {
      return true
    } else if (this.findNode(graph, {'metadata.nofloId': nodeId})) {
      return true
    }

    this.transport.reply(
      'graph',
      'error',
      {
        message: ['Node:', nodeId, 'does not exist'].join(' '),
      },
      conn
    )

    return false
  }

  public findNode(graph, query) {
    const keys = Object.keys(query)

    return graph.nodes.find((node) => {
      for (const key of keys) {
        const prop = get(node, key)

        if (prop !== query[key]) {
          return false
        }
      }

      return true
    })
  }

  public async initMap(
    flowDefinition: FlowDefinition,
    start?: boolean,
    iips?: IIPDefinition[],
    install?: boolean
  ) {
    if (!flowDefinition.id) {
      throw Error('Runtime requires graphs to have an id set')
    }

    if (!flowDefinition.ns) {
      throw Error(
        'Runtime requires graphs to have a `ns` or `package` property'
      )
    }

    if (!flowDefinition.name) {
      throw Error('Runtime requires graphs to have a name property')
    }

    if (this.loader.name === 'NPMLoader') {
      this.loader.addNodeDefinition('@', flowDefinition)
    } else {
      if (!flowDefinition.providers) {
        flowDefinition.providers = {}
      }
      if (!flowDefinition.providers.hasOwnProperty('@')) {
        flowDefinition.providers['@'] = {
          url: config.provider,
        }
      }
      this.loader.addNodeDefinition(config.provider, flowDefinition)
    }

    await this.loader.load(flowDefinition)

    if (install) {
      await this.dependencyLoader.load(this.loader.getDependencies())

      this.logger.info(_KEY_, 'Requirements are installed...')
    }

    const flow = await Flow.create(
      flowDefinition.id, // TODO: always generate id?
      flowDefinition,
      this.loader as any,
      this.ioHandler,
      this.processManager,
      this.dependencyLoader
    )

    // send all output to UI
    if (this.logger.hasOwnProperty('on')) {
      // npm log specific
      const logger: any = this.logger

      logger.on('log.info', (msg) => {
        // url: for an image.
        this.transport.sendAll(
          'network',
          'output',
          {
            message: msg.message,
            type: 'message',
            graph: flow.id,
          },
          flow.id
        )
      })

      logger.on('log.verbose', (msg) => {
        this.transport.sendAll(
          'network',
          'output',
          {
            message: msg.message,
            type: 'message',
            graph: flow.id,
          },
          flow.id
        )
      })

      logger.on('log.warn', (msg) => {
        this.transport.sendAll(
          'network',
          'error',
          {
            message: msg.message,
            graph: flow.id,
          },
          flow.id
        )
      })

      logger.on('log.error', (msg) => {
        this.transport.sendAll(
          'network',
          'error',
          {
            message: msg.message,
            graph: flow.id,
          },
          flow.id
        )
      })
    }

    // Log output
    FlowMonitor(this.logger, flow)

    if (start) {
      if (iips && iips.length > 0) {
        flow.sendIIPs(iips)
      }

      flow.push()
    }

    // used in test and clear
    this.emit('addgraph', flow)

    return flow

    /*
    .catch((error) => {
      this.logger.error(
        'GraphHandler:initMap',
        error.toString()
      )

    return error
    })
    */
  }
}
