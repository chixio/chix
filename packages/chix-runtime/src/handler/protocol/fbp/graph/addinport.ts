import {Graph} from '@chix/fbp-protocol'
import {GraphHandler} from '../../../graph'

export function addinport<T extends GraphHandler>(
  this: T,
  graph,
  {node, port, public: name}: Graph.Input.AddInportPayload,
  conn
) {
  if (this.nodeExists(graph, node, conn)) {
    graph.exposePort('input', node, port, name)
  }
}
