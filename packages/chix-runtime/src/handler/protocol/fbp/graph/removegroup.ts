import {Graph} from '@chix/fbp-protocol'
import {GraphHandler} from '../../../graph'

export function removegroup<T extends GraphHandler>(
  this: T,
  _graph,
  _payload: Graph.Input.RemoveGroupPayload,
  _conn
) {}
