import {Graph} from '@chix/fbp-protocol'
import * as helper from '../../../../helper'
import {GraphHandler} from '../../../graph'

export function removeedge<T extends GraphHandler>(
  this: T,
  graph,
  payload: Graph.Input.RemoveEdgePayload
) {
  const link = helper.findLink(graph, payload)

  if (!link) {
    // removal of a node already removes it's connections.
    this.logger.debug('GraphHandler', 'Link already gone skipping')
  } else {
    graph.removeLink(link)
  }
}
