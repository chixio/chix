import {Flow as FlowDefinition} from '@chix/common'
import {Graph} from '@chix/fbp-protocol'
import {Flow} from '@chix/flow'
import * as _s from 'underscore.string'
import {GraphHandler} from '../../../graph'

// Initialize an empty graph
// @see https://github.com/noflo/noflo-runtime-base/blob/master/src/protocol/Graph.coffee#L47

/*
 BaseTransport Receive: { protocol: 'graph',
 BaseTransport   command: 'clear',
 BaseTransport   payload:
 BaseTransport    { id: 'MyCoolGraph',
 BaseTransport      name: 'MyCoolGraph',
 BaseTransport      library: 'ChixProtocol',
 BaseTransport      main: false } }

 { protocol: 'graph',
 BaseTransport   command: 'clear',
 BaseTransport   payload:
 BaseTransport    { id: 'main',
 BaseTransport      name: 'main',
 BaseTransport      library: 'my-test',
 BaseTransport      main: true,
 BaseTransport      icon: '',
 BaseTransport      description: '' } }
 TODO: I do nothing with the fact of the graph being main.
 If a graph is specified on the command line that will be the one
 being reported as running.
 TODO: clear command should wait for a lot of stuff
 */
export async function clear<T extends GraphHandler>(
  this: T,
  _bogus,
  payload: Graph.Input.ClearPayload,
  conn
) {
  const transport = this.transport
  const logger = this.logger

  // for main graphs this is always <my-lib>/main
  if (!payload.id) {
    this.transport.reply(
      'graph',
      'error',
      {
        message: Error('No graph ID provided'),
      },
      conn
    )
  } else {
    const graph = this.processManager.getById(payload.id) as Flow
    if (graph) {
      await graph.clear()

      logger.info('GraphHandler', 'Graph cleared')

      // this should destroy the entire graph.
      // not sure, if all nodes are de-registered from the process manager
      // this clearing is somewhat hard, done properly it will create an
      // event fest.
      this.logger.warn('GraphHandler:', 'Graph already registered')
      return
    }
  }

  payload.name = payload.name || 'Untitled'

  // TODO: use the component name or id for ns:name
  const flowDefinition: FlowDefinition = {
    id: payload.id, // I use uuid, what is the UI sending?
    // determine how payload.library could be used
    ns: payload.library || 'flowhub',
    name: _s.dasherize(payload.name.toLowerCase()),
    type: 'flow',
    title: payload.name,
    description: payload.name || '',
    ports: {},
    nodes: [],
    links: [],
  }

  flowDefinition.providers = {}

  // convenience emit.
  // will be send maybe a bit too early.. FIX: that
  this.on('addgraph', onAddGraph)

  function onAddGraph(graph) {
    // fbp-test: sending baseDir back is a bit odd
    //
    const _payload: Graph.Output.ClearPayload = {
      id: graph.id,
      // this is not known to chix-flow, chix-runtime keeps track of `main`
      main: payload.main, // phantom property
      // name: graph.metadata.name
      name: payload.name,
    }

    transport.sendAll('graph', 'clear', _payload, graph.id)
  }

  this.initMap(flowDefinition, false) // do not start graph
    .then((_result) => {
      // nothing send back
    })
    .catch((error) => {
      // send back error
      this.logger.error('GraphHandler:clear', error.toString(), payload)

      process.nextTick(() => {
        this.transport.reply(
          'graph',
          'error',
          {
            message: error.toString(),
            graph: payload.id,
          },
          conn
        )
      })
    })
}
