import {Graph} from '@chix/fbp-protocol'
import {LoadDefinitionResult} from '@chix/loader'
import {config} from '../../../../config'
import {GraphHandler} from '../../../graph'

export type WithProviderPayload = {
  metadata?: {
    provider?: string
  }
}

export async function addnode<T extends GraphHandler>(
  this: T,
  graph,
  payload: Graph.Input.AddNodePayload & WithProviderPayload,
  conn
) {
  if (!payload.id) {
    this.logger.warn('GraphHandler', 'no node id')
  }

  const parts = payload.component.split('/')

  if (parts.length !== 2) {
    this.logger.warn('GraphHandler', 'Unrecognized component')
    return
  }

  const [ns, name] = parts

  if (!payload.metadata) {
    payload.metadata = {
      provider: config.provider,
    }
  } else if (!payload.metadata.provider) {
    payload.metadata.provider = config.provider
  }

  const {nodeDefinition, dependencies}: LoadDefinitionResult =
    await this.loader.loadNodeDefinitionFrom(
      payload.metadata.provider,
      ns,
      name
    )

  await this.dependencyLoader.load(dependencies)

  try {
    const node = await graph.createNode(
      {
        id: payload.id, // id we cannot decide ourself probably.
        ns,
        name,
        provider: '@', // see above for where it points to
        metadata: payload.metadata, // todo split title from metadata ?
      },
      nodeDefinition
    )

    if (nodeDefinition.type === 'flow') {
      this.transport.subscribe(conn, 'graph', node.pid)
    }
  } catch (error) {
    this.logger.error('GraphHandler:addnode', error.toString(), payload)

    this.transport.reply(
      'graph',
      'error',
      {
        message: error.toString(),
        graph: payload.id,
      },
      conn
    )
  }
}
