import {Graph} from '@chix/fbp-protocol'
import {GraphHandler} from '../../../graph'

export function changegroup<T extends GraphHandler>(
  this: T,
  _graph,
  _payload: Graph.Input.ChangeGroupPayload,
  _conn
) {}
