import {Graph} from '@chix/fbp-protocol'
import {GraphHandler} from '../../../graph'

export function removeinport<T extends GraphHandler>(
  this: T,
  graph,
  payload: Graph.Input.RemoveInportPayload,
  _conn
) {
  graph.removePort('input', payload.public)
}
