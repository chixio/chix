import {Graph} from '@chix/fbp-protocol'
import {GraphHandler} from '../../../graph'

export function removeoutport<T extends GraphHandler>(
  this: T,
  graph,
  payload: Graph.Input.RemoveOutportPayload
) {
  graph.removePort('output', payload.public)
}
