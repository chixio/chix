import {IIP as IIPDefinition} from '@chix/common'
import {Graph} from '@chix/fbp-protocol'
import {GraphHandler} from '../../../graph'

export const RETRY_LIMIT = 23
export const RETRY_INTERVAL = 500

export function addinitial<T extends GraphHandler>(
  this: T,
  graph,
  payload: Graph.Input.AddInitialPayload,
  conn,
  retries: number
) {
  // Change the conn instead of iip.
  const iip: IIPDefinition = {
    target: {
      id: payload.tgt.node,
      port: payload.tgt.port,
    },
  }

  if (payload.tgt.index) {
    iip.target.setting.index = payload.tgt.index
  }

  // data is in payload.src.data

  // better to set conn
  // graph.sendIIP(iip, payload.src.data)

  if (graph.hasNode(payload.tgt.node)) {
    // TODO: find out why this happens. seems upon first load.
    if (payload.src.data !== null && payload.src.data !== undefined) {
      graph
        .getNode(payload.tgt.node)
        .setContextProperty(payload.tgt.port, payload.src.data)

      this.logger.info(
        'GraphHandler:addinitial',
        'Set context',
        iip,
        payload.src.data
      )

      if (graph.status === 'running') {
        this.logger.info(
          'GraphHandler:addinitial',
          'Send IIP on running graph',
          iip,
          payload.src.data
        )

        graph.clearIIPs(iip)
        graph.sendIIP(iip.target, payload.src.data)
      } else {
        graph.sendIIP(iip.target, payload.src.data)
        graph.push()
      }
    } else {
      this.logger.warn(
        'GraphHandler',
        'Send IIP: Ignoring `null` data',
        iip,
        payload.src.data
      )
    }
  } else {
    // ok makes no sense, it just has to wait for the install to be finished.
    // but the iips, links etc, just get send.
    //
    if (retries === RETRY_LIMIT) {
      this.logger.error(
        'GraphHandler',
        'Failed to add link, giving up after %s retries',
        retries
      )
    } else {
      setTimeout(() => {
        this.logger.warn('GraphHandler', 'Target node not ready yet, retrying')
        addinitial.apply(this, [graph, payload, conn, ++retries])
      }, RETRY_INTERVAL)
    }
  }
}
