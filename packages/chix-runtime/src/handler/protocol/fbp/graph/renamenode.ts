import {Graph} from '@chix/fbp-protocol'
import {GraphHandler} from '../../../graph'

export function renamenode<T extends GraphHandler>(
  this: T,
  graph,
  payload: Graph.Input.RenameNodePayload,
  conn
) {
  if (this.nodeExists(graph, payload.from, conn)) {
    const node = graph.getNode(payload.from)

    node.setTitle(payload.to)

    this.transport.sendAll('graph', 'renamenode', payload, graph.id)
  }
}
