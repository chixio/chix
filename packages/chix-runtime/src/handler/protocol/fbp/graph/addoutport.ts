import {Graph} from '@chix/fbp-protocol'
import {Packet} from '@chix/flow'
import {GraphHandler} from '../../../graph'

// this is sending to all for now.
export function packetHandler(transport, graphId: string, port: string) {
  return (packet: Packet) => {
    transport.sendAll('runtime', 'packet', {
      port,
      event: 'data',
      graph: graphId,
      payload: packet.read(packet.owner),
    })
  }
}

export function addoutport<T extends GraphHandler>(
  this: T,
  graph,
  {node, port, public: name}: Graph.Input.AddOutportPayload,
  conn
) {
  const graphId = graph.id

  if (this.nodeExists(graph, node, conn)) {
    graph.exposePort('output', node, port, name)

    // probably not the correct place to do this.
    // also this should first plug, to have a proper cleanup
    graph
      .getNode(node)
      .getOutputPort(port)
      .on('data', packetHandler(this.transport, graphId, name))
  }
}
