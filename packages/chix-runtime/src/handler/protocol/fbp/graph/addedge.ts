import {Graph} from '@chix/fbp-protocol'
import {Flow, Link} from '@chix/flow'
import {config} from '../../../../config'
import {GraphHandler} from '../../../graph'

export function addedge<T extends GraphHandler>(
  this: T,
  graph: Flow,
  payload: Graph.Input.AddEdgePayload,
  conn,
  retries: number
) {
  if (graph.hasNode(payload.tgt.node) && graph.hasNode(payload.src.node)) {
    // payload.metadata .. unused.
    const link = Link.create({
      source: {
        id: payload.src.node,
        port: payload.src.port,
        setting: {
          index: payload.src.index,
        },
      },
      target: {
        id: payload.tgt.node,
        port: payload.tgt.port,
        setting: {
          index: payload.tgt.index,
        },
      },
    })

    if (payload.metadata) {
      link.setMetadata(payload.metadata)

      if (payload.metadata.route === config.persist_route) {
        link.target.set('persist', true)
      }
    }
    graph.addLink(link)
  } else {
    if (retries === 23) {
      this.logger.error(
        'GraphHandler',
        'Failed to add link, giving up after %s retries',
        retries
      )
    } else {
      setTimeout(() => {
        this.logger.warn(
          'GraphHandler',
          'Target or Source node not ready yet, retrying'
        )
        addedge.apply(this, [graph, payload, conn, ++retries])
      }, 500)
    }
  }
}
