import {Graph} from '@chix/fbp-protocol'
import {GraphHandler} from '../../../graph'

export function changenode<T extends GraphHandler>(
  this: T,
  graph,
  payload: Graph.Input.ChangeNodePayload,
  conn
) {
  if (this.nodeExists(graph, payload.id, conn)) {
    const node = graph.getNode(payload.id)

    if (payload.metadata) {
      Object.keys(payload.metadata).forEach((key) => {
        if (payload.metadata[key] === null) {
          node.removeMeta(key)
        } else {
          node.setMeta(key, payload.metadata[key])
        }
      })

      payload.metadata = JSON.parse(JSON.stringify(node.metadata))
    }

    // send the same out, probably not in the spec.
    // would update all connected.
    // only all others should receive...
    // this is done by monitor
    this.transport.sendAll('graph', 'changenode', payload, graph.id)
  }
}
