import {Graph} from '@chix/fbp-protocol'
import {Flow} from '@chix/flow'
import {GraphHandler} from '../../../graph'

export function addgroup<T extends GraphHandler>(
  this: T,
  _graph: Flow,
  _payload: Graph.Input.AddGroupPayload,
  _conn
) {}
