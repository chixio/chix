import {Graph} from '@chix/fbp-protocol'
import {GraphHandler} from '../../../graph'

export function renameinport<T extends GraphHandler>(
  this: T,
  graph,
  payload: Graph.Input.RenameInportPayload
) {
  graph.renamePort('input', payload.from, payload.to)
}
