import {Graph} from '@chix/fbp-protocol'
import {findLink, updateLinkFromPayload} from '../../../../helper'
import {GraphHandler} from '../../../graph'

export function changeedge<T extends GraphHandler>(
  this: T,
  graph,
  payload: Graph.Input.ChangeEdgePayload
) {
  const link = findLink(graph, payload)

  updateLinkFromPayload(link, payload)
}
