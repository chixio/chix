import {Graph} from '@chix/fbp-protocol'
import {GraphHandler} from '../../../graph'

export function removenode<T extends GraphHandler>(
  this: T,
  graph,
  payload: Graph.Input.RemoveNodePayload,
  conn
) {
  if (this.nodeExists(graph, payload.id, conn)) {
    if (graph.hasLinks(payload.id)) {
      // triggers remove node sends first.
      graph.removeLinks(payload.id)
    }

    graph.removeNode(payload.id)
  }
}
