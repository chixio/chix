import {Graph} from '@chix/fbp-protocol'
import {GraphHandler} from '../../../graph'

export function renamegroup<T extends GraphHandler>(
  this: T,
  _graph,
  _payload: Graph.Input.RenameGroupPayload,
  _conn
) {}
