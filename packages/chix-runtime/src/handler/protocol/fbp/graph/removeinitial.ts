import {Graph} from '@chix/fbp-protocol'
import {realPort} from '@chix/noflo'
import {GraphHandler} from '../../../graph'

export function removeinitial<T extends GraphHandler>(
  this: T,
  graph,
  payload: Graph.Input.RemoveInitialPayload,
  conn
) {
  // iips are not remembered, conn is.
  if (this.nodeExists(graph, payload.tgt.node, conn)) {
    const node = graph.getNode(payload.tgt.node)
    const port = realPort(node, 'input', payload.tgt.port)

    node.clearContextProperty(port)
  }
}
