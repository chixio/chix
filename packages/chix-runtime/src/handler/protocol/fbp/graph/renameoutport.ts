import {Graph} from '@chix/fbp-protocol'
import {GraphHandler} from '../../../graph'

export function renameoutport<T extends GraphHandler>(
  this: T,
  graph,
  payload: Graph.Input.RenameOutportPayload
) {
  graph.renamePort('output', payload.from, payload.to)
}
