import {
  Flow as FlowDefinition,
  ILogger,
  Node,
  NodeDefinition,
} from '@chix/common'
import {Component} from '@chix/fbp-protocol'
import {ProcessManager} from '@chix/flow'
import {Loader} from '@chix/loader'
import {chixToNoFlo} from '@chix/noflo'
import {BaseTransport} from '@chix/transport'

import {config} from '../../../../config'

import {ComponentHandler} from '../../../component'

export function sendSource(
  transport: BaseTransport,
  conn,
  payload: Component.Input.SourcePayload
) {
  // be lazy with sending source back
  setTimeout(() => {
    transport.reply('component', 'source', payload, conn)
  }, 0)
}

export function createDefinitionFunc(loader: Loader, provider: string) {
  return function getNodeDefinition(
    _node: Node
  ): FlowDefinition | NodeDefinition {
    return loader.getNodeDefinitionFrom(provider, _node.ns, _node.name)
  }
}

export function getsource<T extends ComponentHandler>(
  this: T,
  payload: Component.Input.GetSourcePayload,
  conn,
  transport: BaseTransport,
  processManager: ProcessManager,
  loader: Loader,
  logger: ILogger
) {
  let graph
  let nodeDefinition

  logger.info('Component:getsource', 'loading source')

  const parts = payload.name.split('/')
  const provider =
    payload.metadata && payload.metadata.provider
      ? payload.metadata.provider
      : config.provider

  if (parts.length === 1) {
    // let's assume it's a uuid & thus a graph
    // an unexpected id?
    graph = processManager.getById(payload.name)

    if (!graph) {
      graph = processManager.getProcess(payload.name)
    }

    if (!graph) {
      logger.error('Unable to find graph ' + payload.name)
    }

    if (!graph.ns) {
      logger.error('Graph should have a namespace')
    }
    if (!graph.name) {
      logger.error('Graph should have a `name` property')
    }

    // nodeDefinition = loader.getNodeDefinitionFrom(provider, graph.ns, graph.name)
    nodeDefinition = loader.getNodeSource(provider, graph.ns, graph.name)
  } else {
    const [ns, name] = parts
    nodeDefinition = loader.getNodeSource(provider, ns, name)
    graph = {
      ns,
      name,
    }
  }

  // const getNodeDefinition = createDefinitionFunc(loader, provider)

  // there should be a constant update of the nodeDefinition.
  if (nodeDefinition) {
    if (nodeDefinition.type === 'flow') {
      // see: https://github.com/noflo/noflo-runtime-base/blob/master/src/protocol/Component.coffee#L30
      sendSource(transport, conn, {
        name: graph.name, // should be name
        library: graph.ns, // should be ns
        language: 'json',
        code: JSON.stringify(
          this.options.compat
            ? chixToNoFlo(nodeDefinition, {persist_route: config.persist_route})
            : nodeDefinition,
          null,
          2
        ),
        // tests: ...
      })
    } else {
      /*
      var n = Object.create(nodeDefinition)

      var fn = Function(n.fn)
      n.fn = 'XXXX'

      var fnName = _s.camelize(nodeDefinition.ns + '_' + nodeDefinition.name)

      var src = js2coffee.build(
        'module.exports = ' +
        JSON.stringify(nodeDefinition, null, 2)
        .replace(/XXXX/gm, fn.toString())
        .replace('anonymous', fnName)
      )

      logger.info('Converted to coffeescript', fnName)
      */
      let code

      try {
        code = JSON.stringify(
          nodeDefinition,
          (_key: string, val: any) => {
            if (typeof val === 'function') {
              return val.toString()
            }

            return val
          },
          2
        )
      } catch (e) {
        // processManager source loading does not work
        code = 'TODO'
      }
      sendSource(transport, conn, {
        name: payload.name,
        library: 'ChixProtocol',
        language: 'coffeescript',
        // when loaded directly from npm the source code is different.
        // the source however should not be what is already `compiled`
        code,
        // tests: ...
      })
    }
  } else {
    // Be less harsh and just send something back
    logger.error(
      'Could not find source definition for:\n' + graph.ns + ':' + graph.name
    )
    sendSource(transport, conn, {
      name: payload.name,
      language: 'coffeescript',
      // library: ...
      library: 'ChixProtocol',
      code: '# no source',
      // tests: ...
    })
    /*
  transport.reply('component', 'error', {
    message: Error(
      'Could not find source definition for:\n' +
      graph.ns + ':' + graph.name
    )
  }, conn)
  */
  }
}
