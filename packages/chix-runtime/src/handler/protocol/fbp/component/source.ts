import {ILogger} from '@chix/common'
import {Component} from '@chix/fbp-protocol'
import {ProcessManager} from '@chix/flow'
import {Loader} from '@chix/loader'
import {BaseTransport} from '@chix/transport'
import {ComponentHandler} from '../../../component'

export function source<T extends ComponentHandler>(
  this: T,
  _data: Component.Input.SourcePayload,
  _conn,
  _transport: BaseTransport,
  _pm: ProcessManager,
  _loader: Loader,
  _logger: ILogger
) {}
