import {
  Flow as FlowDefinition,
  ILogger,
  NodeDefinition,
  Port as PortDefinition,
} from '@chix/common'
import {Component, Shared} from '@chix/fbp-protocol'
import {forOf} from '@fbpx/lib'
import {ProcessManager} from '@chix/flow'
import {Loader} from '@chix/loader'
import {BaseTransport} from '@chix/transport'
import {config} from '../../../../config'
import {ComponentHandler} from '../../../component'

// compat fix, there can be multiple types.
// just flatten it to `any`
export function typeCompat(type: string) {
  return Array.isArray(type) ? 'any' : type
}

export function list<T extends ComponentHandler>(
  this: T,
  _data: Component.Input.ListPayload,
  conn,
  transport: BaseTransport,
  _processManager: ProcessManager,
  loader: Loader,
  logger: ILogger
) {
  // this is not correct, a provider's name is missing.
  // currently only the full url template makes them distinct.
  // sending several providers to the UI looses the origin information.
  // fixed by hardcoding my 'rhalff' url in the config.
  const nodeDefinitions = loader.getNodeDefinitions(config.provider)

  const componentTotal = Object.keys(nodeDefinitions).length

  if (nodeDefinitions) {
    logger.info('Component:list', 'loading %d packages', componentTotal)

    forOf(
      (
        _ns: string,
        _name: string,
        nodeDefinition: NodeDefinition | FlowDefinition
      ) => {
        const inPorts: Shared.PortDefinition[] = []
        const outPorts: Shared.PortDefinition[] = []

        // graphs definitions can have no ports
        if (nodeDefinition.ports) {
          const inputPorts = nodeDefinition.ports.input || {}
          const outputPorts = nodeDefinition.ports.output || {}

          forOf((portName: string, portDefinition: PortDefinition) => {
            inPorts.push({
              id: portName,
              type: typeCompat(portDefinition.type),
              required: portDefinition.required,
              // mocha magically converts HTMLElement to a real object.
              // TODO: force both input & output to have type information
              addressable: portDefinition.type
                ? portDefinition.type.toString().toLowerCase() === 'array'
                : undefined,
              description: portDefinition.description,
              values: portDefinition.enum,
              default: portDefinition.default || portDefinition.context,
            })
          }, inputPorts)

          forOf((portName: string, portDefinition: PortDefinition) => {
            outPorts.push({
              id: portName,
              type: typeCompat(portDefinition.type),
              required: portDefinition.required,
              // mocha magically converts HTMLElement to a real object.
              addressable: portDefinition.type
                ? portDefinition.type.toString().toLowerCase() === 'array'
                : undefined,
              description: portDefinition.description,
            })
          }, outputPorts)

          // add :start port to all
          inPorts.push({
            id: ':start',
            type: 'any',
            description: 'Start',
            // TEST if this works, otherwise remove
            required: false,
          })

          // add :complete port to all
          outPorts.push({
            id: ':complete',
            type: 'any',
            description: 'Complete',
          })
        }

        // Components are send per component

        // self.logger.debug('component', 'Sending component %s', name)

        transport.reply(
          'component',
          'component',
          {
            name: [nodeDefinition.ns, nodeDefinition.name].join('/'),
            description: nodeDefinition.description,
            subgraph: nodeDefinition.type === 'flow',
            icon: 'spinner', // start with spinner
            inPorts,
            outPorts,
          },
          conn
        )
      },
      nodeDefinitions
    )

    transport.reply('component', 'componentsready', componentTotal, conn)
  } else {
    logger.warn('ComponentHandler', 'Loaded Node Definitions for:', '@')
  }
}
