import {Network} from '@chix/fbp-protocol'
import {NetworkHandler} from '../../../network'

export function debug<T extends NetworkHandler>(
  this: T,
  _data: Network.Input.DebugPayload,
  _conn,
  graph
) {
  this.logger.info('networkHandler', 'debug', graph.title)
  this.logger.warn('networkHandler', 'debug not yet implemented')
}
