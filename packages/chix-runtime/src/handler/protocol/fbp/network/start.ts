import {Network} from '@chix/fbp-protocol'
import {NetworkHandler} from '../../../network'

export function start<T extends NetworkHandler>(
  this: T,
  _data: Network.Input.StartPayload,
  _conn,
  graph
) {
  this.processManager.start(graph)
}
