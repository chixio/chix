import {Network} from '@chix/fbp-protocol'
import {NetworkHandler} from '../../../network'

export function edges<T extends NetworkHandler>(
  this: T,
  data: Network.Input.EdgesPayload,
  _conn,
  _graph
) {
  // Send every time edges are selected
  this.logger.info('networkHandler', 'Edges NOP', data)
}
