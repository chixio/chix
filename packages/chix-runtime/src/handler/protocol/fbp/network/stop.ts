import {Network} from '@chix/fbp-protocol'
import {NetworkHandler} from '../../../network'

export async function stop<T extends NetworkHandler>(
  this: T,
  _data: Network.Input.StopPayload,
  _conn,
  graph
) {
  await this.processManager.stop(graph)
}
