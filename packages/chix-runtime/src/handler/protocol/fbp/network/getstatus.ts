import {Network} from '@chix/fbp-protocol'
import {NetworkHandler} from '../../../network'

export function getstatus<T extends NetworkHandler>(
  this: T,
  {graph: graphIdOrPid}: Network.Input.GetStatusPayload,
  conn,
  graph
) {
  this.transport.reply(
    'network',
    'status',
    {
      graph: graphIdOrPid,
      // running: graph.status === 'running'
      // running: graph.status === 'created' || graph.status === 'running'
      running: graph.status === 'running',
      uptime: 99999, // TODO: implement this
    },
    conn
  )
}
