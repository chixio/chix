import {Runtime} from '@chix/fbp-protocol'
import * as pkg from '../../../../version'
import {RuntimeHandler} from '../../../runtime'

export function getruntime<T extends RuntimeHandler>(
  this: T,
  _payload: Runtime.Input.GetRuntimePayload,
  processManager,
  conn,
  type
) {
  this.logger.info(
    'runtime:getruntime',
    'Sending capabilities',
    this.options.capabilities
  )

  const reply: Runtime.Output.RuntimePayload = {
    type: type || pkg.name,
    version: pkg.version,
    capabilities: this.options.capabilities,
  }

  if (processManager.hasMainGraph()) {
    const graph = processManager.getMainGraph()

    reply.graph = graph.id
  }

  reply.graphs = processManager
    .getMainGraphs()
    .map((graph) => graph.toJSON(true))

  this.transport.reply('runtime', 'runtime', reply, conn)
}
