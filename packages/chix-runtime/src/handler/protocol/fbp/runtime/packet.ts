import {Runtime} from '@chix/fbp-protocol'
import {Flow, ProcessManager} from '@chix/flow'
import {RuntimeHandler} from '../../../runtime'

export function packet<T extends RuntimeHandler>(
  this: T,
  data: Runtime.Input.PacketPayload,
  processManager: ProcessManager,
  conn
) {
  this.logger.info('runtime:packet', 'Receiving packet')

  const port = data.port
  const event = data.event
  const payload = data.payload
  const graphId = data.graph
  const graph = processManager.getById(graphId) as Flow

  if (event === 'data') {
    if (true || graph.hasParent()) {
      // TODO: restore all the plug methods
      // graph.getParent().sendIIP({

      // this maybe works but is not how it should be
      graph.sendIIP(
        {
          id: graph.id,
          port,
        },
        payload
      )
    }
    // throw Error('Expected graph to have parent')
    /*
    graph
      .getInputPort(port)
      .receive(payload)
    */
  } else {
    this.transport.reply(
      'runtime',
      'error',
      {
        message: Error(`Packet event ${event} not supported`),
      },
      conn
    )
  }
}
