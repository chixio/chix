import {Process} from '@chix/fbp-protocol'
import {ProcessHandler} from '../../../process'

export async function getStatus<T extends ProcessHandler>(
  this: T,
  {reqId, pid, graph}: Process.Input.GetStatusPayload,
  conn
) {
  const process = this.processManager.get(pid)
  const payload: Process.Output.StatusPayload = {
    reqId,
    status,
    graph,
    process: process.toJSON(),
  }

  this.transport.reply('process', 'getstatus', payload, conn)
}
