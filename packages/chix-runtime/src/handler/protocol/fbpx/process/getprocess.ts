import {Process} from '@chix/fbp-protocol'

import {GraphHandler} from '../../../graph'

export async function getprocess<T extends GraphHandler>(
  this: T,
  {pid}: Process.Input.GetProcessPayload,
  conn
) {
  const process = this.processManager.get(pid)

  this.transport.reply(
    'process',
    'getprocess',
    {
      pid,
      process: process.toJSON(true),
    },
    conn
  )
}
