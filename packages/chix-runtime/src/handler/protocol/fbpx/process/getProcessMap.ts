import {Process} from '@chix/fbp-protocol'
import {Flow} from '@chix/flow'

import {GraphHandler} from '../../../graph'

export async function getprocessmap<T extends GraphHandler>(
  this: T,
  {pid}: Process.Input.GetProcessMapPayload,
  conn
) {
  const process = this.processManager.get(pid)

  console.log('GOT PROCESS!', process)

  if (process.type === 'flow') {
    console.log('REPLYING')
    this.transport.reply(
      'process',
      'getprocessmap',
      {
        pid,
        processes: (process as Flow).getProcessMap(),
      },
      conn
    )
  } else {
    console.log('NOT A FLOW!')
  }
}
