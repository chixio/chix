import * as _s from 'underscore.string'
import {RuntimeHandler} from '../../../runtime'

export async function list<T extends RuntimeHandler>(
  this: T,
  _bogus,
  _payload,
  conn
) {
  const graphs = this.processManager.getMainGraphs()

  const payload = graphs.map((flow) => flow.toJSON(true))

  this.transport.reply('runtime', 'list', payload, conn)
}
