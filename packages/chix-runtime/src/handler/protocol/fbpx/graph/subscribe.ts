import {Graph} from '@chix/fbp-protocol'
// import { config } from '../../../../config'
import {GraphHandler} from '../../../graph'

export function subscribe<T extends GraphHandler>(
  this: T,
  _graph,
  {graph}: Graph.Input.SubscribePayload,
  conn
) {
  this.transport.subscribe(conn, 'graph', graph)
  this.transport.reply('graph', 'subscribe', {graph}, conn)

  /* Do not send graph back, do this elsewhere.
  graph.nodes.forEach(addNode)
  graph.links.forEach(addLink)

  function addNode (node) {
    logger.info('graph:addnode', 'Sending node', node.identifier)

    // send only to this subscriber
    transport.reply('graph', 'addnode', {
      id: node.id,
      // component: event.node.title,
      // not sure what this should be.
      component: node.ns + '/' + node.name,
      graph: graph.id,
      metadata: node.export()
    }, conn)
  }

  function addLink (link) {
    logger.info('graph:addedge', 'Sending edge', link.id)

    const _payload: Graph.Input.AddEdgePayload = {
      src: {
        node: link.source.id,
        port: link.source.port,
        index: link.source.get('index')
      },
      tgt: {
        node: link.target.id,
        port: link.target.port,
        index: link.target.get('index')
      },
      graph: graph.id
    }

    if (link.target.has('persist')) {
      _payload.metadata = {
        route: config.persist_route
      }
    }
    transport.reply('graph', 'addedge', _payload, conn)
  }
  */
}
