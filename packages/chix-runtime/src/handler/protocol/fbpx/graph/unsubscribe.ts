import {Graph} from '@chix/fbp-protocol'
import {GraphHandler} from '../../../graph'

export function unsubscribe<T extends GraphHandler>(
  this: T,
  _graph,
  {graph}: Graph.Input.UnsubscribePayload,
  conn
) {
  this.transport.unsubscribe(conn, 'graph', graph)
  this.transport.reply('graph', 'unsubscribe', {graph}, conn)
}
