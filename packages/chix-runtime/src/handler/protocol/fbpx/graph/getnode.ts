import {Graph} from '@chix/fbp-protocol'
import {GraphHandler} from '../../../graph'

export async function getnode<T extends GraphHandler>(
  this: T,
  _graph,
  {
    graph: graphId, // pid
    node: nodeId,
  }: Graph.Input.GetNodePayload,
  conn
) {
  /*
  const process = this.processManager.getProcess(graphId) as Flow
  const node = process.getNode(nodeId)
  */
  const node = this.processManager.getProcess(nodeId)

  this.transport.reply(
    'graph',
    'getnode',
    {
      graph: graphId,
      node: node.toJSON(true),
    },
    conn
  )
}
