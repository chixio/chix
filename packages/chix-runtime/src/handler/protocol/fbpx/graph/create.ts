import {Graph} from '@chix/fbp-protocol'
import {GraphHandler} from '../../../graph'

export async function create<T extends GraphHandler>(
  this: T,
  _bogus,
  payload: Graph.Input.CreatePayload,
  conn
) {
  const transport = this.transport
  const {flow, install = true, start = false, iips, subscribe = true} = payload

  let _flow
  // try {
  _flow = await this.loadJson(flow, {
    install,
    start,
    iips,
  })

  /* flow.toJSON returns context as packets so fix that */
  /*
    const _payload = {
      graph: flow.toJSON(true),
      install,
      start,
      subscribe
    }
   */

  const _payload: Graph.Output.CreatePayload = {
    graph: _flow.id,
    flow: {
      ..._flow.toJSON(true),
    },
    install,
    start,
    subscribe,
  }

  if (subscribe) {
    this.transport.subscribe(conn, 'graph', _flow.id)
  }

  transport.sendAll<Graph.Output.CreatePayload>(
    'graph',
    'create',
    _payload,
    _flow.id
  )

  if (subscribe) {
    this.transport.reply('graph', 'subscribe', {graph: _flow.id}, conn)
  }
  /*
  } catch (error) {
    // send back error
    this.logger.error('GraphHandler:create', error.toString(), payload)

    process.nextTick(() => {
      this.transport.reply('graph', 'error', {
        message: error.toString(),
        graph: _flow ? _flow.id : null
      }, conn)
    })
  }
     */
}
