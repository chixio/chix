import {Shared} from '@chix/fbp-protocol'
import {ProcessManager} from '@chix/flow'
import {BaseTransport} from '@chix/transport'
import {Transporter, TransporterOptions} from '../transporter'
import * as fbpCommands from './protocol/fbp/runtime/index'
import * as fbpxCommands from './protocol/fbpx/runtime/index'
import {ILogger} from '@chix/common'

const commands = {
  ...fbpCommands,
  ...fbpxCommands,
}

export type RuntimeHandlerOptions = {
  type?: string
  capabilities?: Shared.ExtendedCapabilities
} & TransporterOptions

export class RuntimeHandler extends Transporter {
  public static handle(
    processManager: ProcessManager,
    transport: BaseTransport,
    logger?: ILogger,
    options?: RuntimeHandlerOptions
  ) {
    const h = new RuntimeHandler({
      logger,
      transport,
      ...options,
    })

    h.handle(processManager)

    return h
  }

  public options: RuntimeHandlerOptions = {}

  constructor(options: RuntimeHandlerOptions) {
    super({
      logger: options.logger,
      transport: options.transport,
    })

    if (options.type) {
      this.options.type = options.type
    }

    if (options.capabilities) {
      this.options.capabilities = options.capabilities
    }

    this.onRuntime = this.onRuntime.bind(this)
  }

  public onRuntime(data, conn) {
    if (commands[data.command]) {
      this.logger.debug('runtime', data.command, data.payload)
      commands[data.command].apply(this, [
        data.payload,
        this.processManager,
        conn,
        this.options.type,
      ])
    }
  }

  /**
   *
   * Handles the Runtime
   *
   * Usage:
   *
   *   const runtimeHandler = new RuntimeHandler()
   *   runtimeHandler.handle(transport)
   *
   * @param {ProcessManager} processManager
   */
  public handle(processManager: ProcessManager) {
    this.processManager = processManager
    this.transport.on('runtime', this.onRuntime)
  }
}
