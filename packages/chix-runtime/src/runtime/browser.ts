/**
 * Runtime setup for Chix
 *
 * Browser Version.
 */
import LogT from 'logt'

import {ILogger} from '@chix/common'
import {Shared} from '@chix/fbp-protocol'
import {IoHandler, ProcessManager} from '@chix/flow'
import {DependencyLoader, Loader} from '@chix/loader'
import {Loader as LoaderMonitor} from '@chix/monitor-npmlog'
import {BaseTransport} from '@chix/transport'

import {
  ComponentHandler,
  GraphHandler,
  NetworkHandler,
  RuntimeHandler,
} from '../handler'
import {ProcessHandler} from '../handler/process'
import {RuntimeHandlerOptions} from '../handler/runtime'
import {GraphMonitor, NetworkMonitor, ProcessMonitor} from '../monitor'
import {ExternalRuntimeApi} from './api'

export type ChixBrowserRuntimeOptions = {
  type?: string
  transport: BaseTransport
  loader: Loader
  logger?: ILogger
  dependencyLoader?: DependencyLoader
} & RuntimeHandlerOptions

export function chixBrowserRuntime({
  type,
  dependencyLoader,
  transport,
  loader,
  logger,
}: ChixBrowserRuntimeOptions): ExternalRuntimeApi {
  // Initialize process manager
  const processManager = new ProcessManager()

  // Initialize IoHandler
  const io = new IoHandler()

  if (!logger) {
    logger = new LogT(5)
  }

  LoaderMonitor(logger, loader)

  const capabilities: Shared.ExtendedCapabilities = [
    'protocol:graph',
    'protocol:component',
    'protocol:network',
    'protocol:runtime',
    'protocol:process',
    'component:setsource',
    'component:getsource',
  ]

  // Handles component commands
  if (capabilities.includes('protocol:component')) {
    ComponentHandler.handle(loader, transport, processManager /*, logger*/)
  }

  // Monitors the network for changes
  if (capabilities.includes('protocol:network')) {
    // Handles network commands
    NetworkHandler.handle(processManager, transport, logger)
    NetworkMonitor.monitor(processManager, io, transport, logger)
  }

  // Monitors the processes for changes
  if (capabilities.includes('protocol:process')) {
    ProcessHandler.handle(processManager, transport, logger)
    ProcessMonitor.monitor(processManager, transport, logger)
  }

  if (capabilities.includes('protocol:runtime')) {
    // Handles runtime commands
    RuntimeHandler.handle(processManager, transport, logger, {
      type,
      capabilities,
    })
  }

  let gh: GraphHandler
  if (capabilities.includes('protocol:graph')) {
    // Handles graph commands
    gh = GraphHandler.handle(
      processManager,
      io,
      loader,
      transport,
      logger,
      dependencyLoader
    )
    // Monitors the graphs for changes
    GraphMonitor.monitor(processManager, transport, logger)
  }

  return {
    gh,
    processManager,
    io,
    logger,
    loader,
    dependencyLoader,
  }
}
