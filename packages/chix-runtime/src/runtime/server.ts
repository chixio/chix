import * as fs from 'fs-extra'
import * as path from 'path'

import {ILogger} from '@chix/common'
import {Shared} from '@chix/fbp-protocol'
import {IoHandler, ProcessManager} from '@chix/flow'
import {DependencyLoader, RequireDependencyLoader} from '@chix/loader'
import {NPMLoader} from '@chix/loader-npm'
import {RemoteLoader} from '@chix/loader-remote'
import {Loader as LoaderMonitor} from '@chix/monitor-npmlog'
import {BaseTransport} from '@chix/transport'

import {GraphMonitor, NetworkMonitor, ProcessMonitor} from '../monitor'

import {
  ComponentHandler,
  GraphHandler,
  NetworkHandler,
  ProcessHandler,
  RuntimeHandler,
} from '../handler'

import {FBPParser} from '@chix/fbpx'
import {config} from '../config'
import {ExternalRuntimeApi} from './api'

export type ChixServerRuntimeOptions = {
  type?: 'chix' | 'noflo'
  capabilities?: Shared.ExtendedCapabilities
  file?: string
  fbpParser?: FBPParser
  loader?: 'remote' | 'npm'
  token?: string
  compat?: boolean
  purge?: boolean
  nocache?: boolean
  start?: boolean
  install?: boolean
  transport?: BaseTransport
  logger?: ILogger
  dependencyLoader?: DependencyLoader
}

// This is too specific
// I want to easily require this and use it.
// this must be able to run within the browser and within a WebWorker.
export async function chixServerRuntime(
  options: ChixServerRuntimeOptions = {}
): Promise<ExternalRuntimeApi> {
  // Initialize process manager
  const processManager = new ProcessManager()

  // Initialize IoHandler
  const io = new IoHandler()

  // Loader to use
  let loader
  if (options.loader === 'remote') {
    config.setProvider('remote')
    loader = new RemoteLoader({
      defaultProvider: 'https://api.chix.io/v1/nodes/{provider}/{ns}/{name}',
    })
    /* TODO: these options are for FSLoader, but aren't used.
             probably because FSLoader cannot be used in the browser.
    loader = new RemoteLoader({
      purge: options.purge,
      cache: !options.nocache
    })
    */
    loader.setAuthorizationHeader(`Bearer ${options.token}`)
  } else {
    config.setProvider('npm')
    loader = new NPMLoader()
  }

  const preload = false

  if (preload) {
    options.logger.info(
      'Runtime',
      'pre-loading components using %s',
      options.loader
    )

    await loader.preload(config.providers[options.loader])
  }

  /* All known capabilities of fbp-protocol
    - 'component:getsource'
    - 'component:setsource'
    - 'graph:readonly'
    - 'network:control'
    - 'network:data'
    - 'network:persist'
    - 'network:status'
    - 'protocol:component'
    - 'protocol:graph'
    - 'protocol:network'
    - 'protocol:runtime'
    - 'protocol:process'
    - 'protocol:trace'
  */

  // TODO: attach capabilities during install of functionality
  const capabilities =
    options.capabilities ||
    ([
      'component:getsource',
      'component:setsource',
      'protocol:component',
      'protocol:graph',
      'protocol:network',
      'protocol:runtime',
    ] as Shared.ExtendedCapabilities)

  if (!options.compat) {
    capabilities.push('protocol:process')
  }

  LoaderMonitor(options.logger, loader)

  // Handles component commands
  if (capabilities.indexOf('protocol:component') >= 0) {
    ComponentHandler.handle(
      loader,
      options.transport,
      processManager,
      options.logger,
      {
        compat: options.compat,
      }
    )
  }

  // Monitors the graphs for changes
  if (capabilities.indexOf('protocol:graph') >= 0) {
    GraphMonitor.monitor(processManager, options.transport, options.logger)
  }

  if (capabilities.indexOf('protocol:network') >= 0) {
    // Handles network commands
    NetworkHandler.handle(processManager, options.transport, options.logger)

    // Monitors the network for changes
    NetworkMonitor.monitor(
      processManager,
      io,
      options.transport,
      options.logger,
      options
    )
  }

  // Monitors the processes for changes
  if (capabilities.indexOf('protocol:process') >= 0) {
    ProcessHandler.handle(processManager, options.transport, options.logger)
    ProcessMonitor.monitor(processManager, options.transport, options.logger)
  }

  // Handles runtime commands
  if (capabilities.indexOf('protocol:runtime') >= 0) {
    RuntimeHandler.handle(processManager, options.transport, options.logger, {
      type: options.type,
      capabilities,
    })
  }

  const dependencyLoader =
    options.dependencyLoader ||
    new RequireDependencyLoader({installPath: process.cwd()})

  // Handles graph commands
  const gh = GraphHandler.handle(
    processManager,
    io,
    loader,
    options.transport,
    options.logger,
    dependencyLoader
  )

  const file = options.file

  if (file) {
    const name = path.basename(file).split('.').slice(0, -1)[0]

    const pkgFile = path.resolve(process.cwd(), './package.json')

    const ns = fs.existsSync(pkgFile)
      ? JSON.parse(fs.readFileSync(pkgFile, 'utf-8'))
          .name.replace('@', '')
          .replace(/\W+/g, '-')
      : 'main'

    const contents = await fs.readFile(file)

    await gh.loadFbp(contents.toString(), options.fbpParser, options, {
      ns,
      name,
    })
  }

  return {
    gh,
    processManager,
    io,
    logger: options.logger,
    loader,
    dependencyLoader,
  }
}
