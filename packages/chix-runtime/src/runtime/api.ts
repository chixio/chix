import {ILogger} from '@chix/common'
import {IoHandler, ProcessManager} from '@chix/flow'
import {DependencyLoader, Loader} from '@chix/loader'
import {GraphHandler} from '../handler'

export interface ExternalRuntimeApi {
  gh: GraphHandler
  logger: ILogger
  io: IoHandler
  loader: Loader
  dependencyLoader: DependencyLoader
  processManager: ProcessManager
}
