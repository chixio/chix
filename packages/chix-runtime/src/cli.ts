import {CertManager} from '@chix/cert'
import {Config} from '@chix/config'
import {schemas} from '@chix/fbp-protocol'
import {ChixParser as Parser, FSResolver} from '@chix/fbpx'
import * as commander from 'commander'
import * as flowhub from 'flowhub-registry'
import * as fs from 'fs-extra'
import * as http from 'http'
import * as https from 'https'
import * as noflo from 'noflo'
import * as runtime_noflo from 'noflo-runtime-websocket'
import * as npmlog from 'npmlog'
import * as path from 'path'
import * as util from 'util'
import * as pkg from './version'

import {
  WebSocketProxyServerTransport,
  WebSocketProxyTransport,
  WebSocketTransport,
  WebSocketTransportOptions,
} from '@chix/transport'

import {GraphInterface} from './graphInterface'
import {chixServerRuntime, ChixServerRuntimeOptions} from './runtime/server'

const logger = npmlog as any
const program = commander as any

const baseDir = process.env.PROJECT_HOME || process.cwd()
const interval = 10 * 60 * 1000

logger.addLevel(
  'debug',
  30,
  {
    fg: 'grey',
    bg: 'black',
  },
  'DEBUG'
)

program
  .version(pkg.version, '-V, --version')
  .usage('[options] <file ...>')
  .option('-t, --type [name]', 'Runtime type chix, noflo-nodejs', 'chix-nodejs')
  .option('-c, --cache', 'cache components')
  .option('-x, --compat', 'stay compatible with noflo format')
  .option('-p, --proxy', 'expose /proxy to proxy requests to e.g. a browser')
  .option('-r, --proxy-server [runtime-url]', 'proxy nodejs runtime')
  .option('-s, --secure', 'secure wss or ws')
  .option('-S, --secret', 'secret to be send with each request to the runtime')
  .option('-n, --start', 'start graph immediately (now)')
  .option('-u, --uppercase', 'force ports uppercase')
  .option('-h, --host [name]', 'hostname')
  .option('-p, --port [name]', 'port')
  .option('-I, --no-ids', 'do not use uuids')
  .option('-b, --bail', 'bail on errors')
  .option('-i, --interface [name]', 'choose a webserver interface')
  .option('-l, --loader [loader]', 'loader `remote` or `npm`', 'npm')
  .option('-L, --loglevel [level]', 'Log level')
  .option('-f, --flowhub', 'ping flowhub')
  .option('-v, --verbose', 'verbose output')
  .option('-C, --create-cert', 'Create SSL certificate for websocket host.')
  .allowUnknownOption() // allow node debugger options
  .parse(process.argv)

program.loglevel = program.loglevel || 'info'
;(logger as any).level = program.verbose ? 'verbose' : program.loglevel
;(logger as any).debug = (logger as any).verbose

if (program.flowhub) {
  program.no_ids = true
}

logger.level = 'info'
console.log('log level', logger.level)

program.port = program.port || process.env.CHIX_WEBSOCKET_PORT || 3569
program.host = program.host || process.env.CHIX_HOSTNAME || '127.0.0.1'
program.secure = program.secure || false

let flowhubRuntime
program.type = program.type.match(/noflo/) ? 'noflo-nodejs' : 'chix-nodejs'

export async function run() {
  if (program.flowhub) {
    // stored takes precedence.
    /*
    ['port', 'host', 'secure'].forEach(function (prop) {
      if (stored[prop] && program.hasOwnProperty(prop)) {
        console.warn(['Ignoring', prop, 'using: ', stored[prop]].join(' '))
        program[prop] = stored[prop]
      }
    })
    */

    const flowHubFile = path.resolve(process.cwd(), './flowhub.json')

    if (!fs.pathExistsSync(flowHubFile)) {
      logger.error(
        'Flowhub',
        util.format(
          'No configuration found at %s. Please run create it first.',
          flowHubFile
        )
      )
    }

    const stored = await fs.readJSON(flowHubFile)

    if (Object.keys(stored).length === 0) {
      process.exit(1)
    } else {
      flowhubRuntime = new flowhub.Runtime({
        label: stored.label,
        id: stored.id,
        user: stored.user,
        secret: stored.secret,
        protocol: 'websocket',
        address: `wss://${program.host}:${program.port}`,
        // type: program.type
        type: program.type,
        // type: 'chix-nodejs'
      })
    }
  }

  const graphInterfaceOptions = {
    interface: program.interface,
  }

  const serveMe = GraphInterface(graphInterfaceOptions, logger)

  let server

  if (program.secure) {
    if (!Config.hasGlobalConfigFile) {
      console.log('Global config file does not exists. Run fbpx login first.')
    }

    const options = {
      certDir: `${Config.globalConfigDir}/certs`,
      defaultCertAttrs: {
        countryName: 'NL',
        organizationName: 'ChixCertManager',
        ST: 'FL',
        OU: 'ChixCertManager SSL',
      },
    }
    const certManager = new CertManager(options)

    if (!certManager.rootCAFileExists) {
      await certManager.generateRootCA('FBPX')
    }

    if (!certManager.hostCertsExists(program.host)) {
      console.log(`No certificate exists for ${program.host}, generating...`)
    }

    const serverOptions: https.ServerOptions = await certManager.getCertificate(
      program.host
    )

    server = https.createServer(serverOptions, serveMe)
  } else {
    server = http.createServer(serveMe)
  }

  let listenMsg

  const loader = program.loader || 'remote'

  const wsProtocolString = program.secure ? 'wss://' : 'ws://'

  if (program.proxy) {
    // proxy doesn't run any graph itself
    WebSocketProxyTransport.create(server, {
      logger,
    })

    listenMsg = `proxy server listening at ${wsProtocolString}${program.host}:${program.port}/proxy`
  } else if (program.proxyServer) {
    // proxy doesn't run any graph itself
    WebSocketProxyServerTransport.create(server, {
      logger,
      url: program.proxyServer,
    })

    listenMsg = `proxy server listening at ${wsProtocolString}${program.host}:${program.port}`
  } else {
    if (program.type === 'noflo') {
      const file = program.args[0]
      const defaultPermissions = [
        'protocol:component',
        'protocol:runtime',
        'protocol:graph',
        'protocol:network',
        'component:getsource',
        'component:setsource',
      ]

      // difference with noflo-nodejs is that this runtime sends an empty capabilities array.
      // else no difference.
      runtime_noflo(server, logger, {
        defaultGraph: file, // not sure, seems I still have to manually load the graph? see noflo-nodejs
        baseDir,
        captureOutput: false,
        catchExceptions: true,
        permissions: {'+TwkVuF6': defaultPermissions},
        defaultPermissions,
      })
    } else {
      // program.schemas = loadSchemas(schemaPattern)
      program.schemas = schemas

      program.logger = logger

      const websocketTransportOptions: WebSocketTransportOptions = {
        catchExceptions: program.catchExpections,
        type: program.type || 'chix',
        schemas: program.schemas,
        // role: program.role || 'client',
        role: program.role || 'server', // not sure why the default was client?
        secret: program.secret,
        logger,
        bail: program.bail,
      }

      const transport = new WebSocketTransport(
        websocketTransportOptions,
        server
      )

      let token

      if (loader === 'remote') {
        const config = new Config()
        token = config.get('access_token')
      }

      const fbpParser = Parser({
        skipIDs: program.no_ids ? program.no_ids : true,
        defaultProvider: loader.name === 'NPMLoader' ? false : undefined,
      })

      fbpParser.addIncludeResolver(new FSResolver())

      const chixRuntimeOptions: ChixServerRuntimeOptions = {
        type: program.type || 'chix',
        // capabilities?:
        file: program.args[0],
        loader,
        compat: program.compat,
        purge: program.purge,
        nocache: program.nocache,
        start: program.start,
        fbpParser,
        token,
        transport,
        logger,
      }

      await chixServerRuntime(chixRuntimeOptions)
    }

    listenMsg =
      ' runtime listening at ' +
      (program.secure ? 'wss://' : 'ws://') +
      program.host +
      ':' +
      program.port
  }

  server.listen(program.port, () => {
    logger.info(program.type, listenMsg)

    if (!program.proxy && !program.proxyServer) {
      if (program.type === 'noflo') {
        const file = program.args[0]
        program.graph = file
        console.log('Loading main graph: ' + file)
        noflo.graph.loadFile(program.graph, (_graph) => {
          console.log('Loaded graph, bit late perhaps')
        })
      }

      if (loader === 'remote') {
        // defaultProvider makes less sense..
        logger.info(program.type, 'Using remote providers to load components.')
      } else {
        logger.info(program.type, `Using ${baseDir} for component loading`)
      }
    }

    if (program.flowhub) {
      // Register the runtime with Flowhub so that it will show up in the UI
      flowhubRuntime.register((error /*, ok*/) => {
        if (error) {
          console.error(error)

          logger.error(
            'Flowhub',
            `Registration with Flowhub failed: ${error.message}`
          )
          return
        }

        logger.info(
          'Flowhub',
          'Registered with Flowhub. The Runtime should now be accessible in the UI'
        )

        // Ping Flowhub periodically to let the user know that the runtime is still available
        setInterval(() => {
          flowhubRuntime.ping()
        }, interval)

        // Start capturing output so that we can send it to the UI
        // rt.startCapture()
      })
    }
  })
}
