# Runtime Iframe

Usage:
```typescript
import {chixBrowserRuntime} from '@chix/runtime'
import {WindowTransport} from '@chix/transport'
import {RemoteLoader} from '@chix/loader-remote'

const transport = WindowTransport({protocol: 'chix-runtime'})
const logger = null;
const loader = new RemoteLoader()

(window as any).runtime = chixBrowserRuntime(
  transport,
  logger,
  loader,
  {type: 'chix'}
)
```

