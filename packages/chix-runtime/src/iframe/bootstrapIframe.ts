import {RemoteLoader} from '@chix/loader-remote'
import {WindowTransport, WindowTransportOptions} from '@chix/transport'
import {chixBrowserRuntime, ChixBrowserRuntimeOptions} from '../runtime/browser'

type BootstrapIframeOptions = {} & WindowTransportOptions &
  ChixBrowserRuntimeOptions

export function bootstrapIframe(options: BootstrapIframeOptions) {
  const transport = new WindowTransport({
    protocol: (options && options.protocol) || 'chix',
  })
  const loader = new RemoteLoader({
    defaultProvider: 'https://api.chix.io/v1/nodes/{provider}/{ns}/{name}',
  })

  return chixBrowserRuntime({
    transport,
    loader,
    ...options,
  })
}
