import {ProcessManager} from '@chix/flow'
import {Transporter, TransporterOptions} from '../transporter'
import * as commands from './protocol/fbpx/process/'

export type ProcessMonitorOptions = {
  uppercase?: boolean
} & TransporterOptions

export class ProcessMonitor extends Transporter {
  public static monitor(processManager: ProcessManager, transport, logger?) {
    return new ProcessMonitor({
      logger,
      transport,
    }).monitor(processManager)
  }
  public options: ProcessMonitorOptions = {}
  constructor(options: ProcessMonitorOptions) {
    super({
      logger: options.logger,
      transport: options.transport,
    })

    this.options.uppercase = !!options.uppercase
  }

  /**
   *
   * Monitors the Processes
   *
   * Usage:
   *
   *   ProcessMonitor.monitor(processManager)
   *
   * Transport is an object containing send function.
   *
   * @param {ProcessManager} processManager
   */
  public monitor(processManager: ProcessManager) {
    // are these corrected related to network?
    // processManager.on('startProcess', commands.started.bind(this))
    processManager.on('error', commands.onProcessError.bind(this))
    // processManager.on('stopProcess', commands.stopped.bind(this))
    processManager.on('processStatus', commands.onProcessStatus.bind(this))
  }
}
