import {Flow, ProcessManager, xNode} from '@chix/flow'
import {Transporter, TransporterOptions} from '../transporter'
import * as commands from './protocol/fbp/graph/'
import {BaseTransport} from '@chix/transport'
import {ILogger} from '@chix/common'

export type GraphMonitorOptions = {} & TransporterOptions

export class GraphMonitor extends Transporter {
  /**
   *
   * Monitors the graph changes
   *
   * Usage:
   *
   *   var graphMonitor = new GraphMonitor(options)
   *   graphMonitor.monitor(ioHandler, transport)
   *
   * @param {ProcessManager} processManager
   * @param {BaseTransport} transport
   * @param {ILogger} logger
   */

  public static monitor(
    processManager: ProcessManager,
    transport: BaseTransport,
    logger?: ILogger
  ) {
    const graphMonitor = new GraphMonitor({
      logger,
      transport,
    })

    return graphMonitor.monitor(processManager)
  }
  constructor(options: GraphMonitorOptions) {
    super({
      logger: options.logger,
      transport: options.transport,
    })
    this.onAddProcess = this.onAddProcess.bind(this)
  }

  public onAddProcess(node: xNode | Flow) {
    if (node.type === 'flow') {
      const graph = node

      graph.on('addLink', commands.onAddEdge.bind(this, graph))

      // is handled by node contextUpdate & contextClear
      // graph.on('addIIP', commands.onAddInitial.bind(this, graph))
      graph.on('addGroup', commands.onAddGroup.bind(this, graph))

      graph.on('addNode', commands.onAddNode.bind(this, graph))
      graph.on('addPort', commands.onAddPort.bind(this, graph))
      graph.on('changeLink', commands.onChangeEdge.bind(this, graph))
      graph.on('changeGroup', commands.onChangeGroup.bind(this, graph))
      graph.on('metadata', commands.onChangeNode.bind(this, graph))
      graph.on('removeLink', commands.onRemoveEdge.bind(this, graph))
      graph.on('removeGroup', commands.onRemoveGroup.bind(this, graph))

      graph.on('removeNode', commands.onRemoveNode.bind(this, graph))
      graph.on('renameGroup', commands.onRenameGroup.bind(this, graph))
      graph.on('renameNode', commands.onRemoveNode.bind(this, graph))
    }
  }

  public monitor(processManager: ProcessManager) {
    processManager.on('addProcess', this.onAddProcess)
  }
}
