import {Network} from '@chix/fbp-protocol'
import {Link} from '@chix/flow'
import {toPayload} from '../../../../helper'
import {NetworkMonitor} from '../../../network'

export function onConnect<T extends NetworkMonitor>(this: T, link: Link) {
  //    self.logger.info('network:connect', 'Network connect', link.id)
  const payload: Network.Output.ConnectPayload = toPayload(link)

  this.transport.sendAll(
    'network',
    'connect',
    payload,
    link.graphPid // not sure, there is also graphPid
  )
}
