import {Network} from '@chix/fbp-protocol'
import {NetworkMonitor} from '../../../network'

// Stop, will really stop, which means flushing the queue.
// And dropping all output.
export function onEndProcess<T extends NetworkMonitor>(this: T, node) {
  if (node.type === 'flow') {
    this.logger.info('network:stopped', 'Network stopped', node.title)

    const payload: Network.Output.StoppedPayload = {
      time: new Date().toString(),
      // uptime: this.uptime, // have to track this.
      uptime: 9999, // TODO: implement this
      graph: node.pid,
    }

    this.transport.sendAll('network', 'stopped', payload)
  }
}
