import {Network} from '@chix/fbp-protocol'
import {stringify, toDataId, toPayload} from '../../../../helper'
import {NetworkMonitor} from '../../../network'

export function onData<T extends NetworkMonitor>(this: T, event) {
  const link = event.link
  const payload: Network.Output.DataPayload = {
    ...toPayload(link),
    data: stringify(event.data),
  }

  if (this.options.compat) {
    // payload.id = toDataId(link, this.options.uppercase)
    payload.id = toDataId(link, true)
  }

  this.logger.info('network:data', payload)

  this.transport.sendAll('network', 'data', payload, event.graphPid)
}

// message.tgt.port = upCase(message.tgt.port, this.options.uppercase)
// message.tgt.port = upCase(message.tgt.port, this.options.compat)
// message.src.port = upCase(message.src.port, this.options.uppercase)
// message.src.port = upCase(message.src.port, this.options.compat)
// TODO: references are not filtered / when instances are passed around
// normally they cannot be serialized to JSON, so perhaps only send their type like `Socket`
// delete message.id // link id is not necessary
