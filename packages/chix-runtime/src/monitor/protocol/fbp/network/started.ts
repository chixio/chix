import {Network} from '@chix/fbp-protocol'
import {NetworkMonitor} from '../../../network'

export function onStartProcess<T extends NetworkMonitor>(this: T, node) {
  if (node.type === 'flow') {
    this.logger.info('network:started', 'Network started', node.title)

    const payload: Network.Output.StartedPayload = {
      // time: data.start,
      time: new Date().toString(),
      graph: node.pid,
    }

    this.transport.sendAll('network', 'started', payload)
  }
}
