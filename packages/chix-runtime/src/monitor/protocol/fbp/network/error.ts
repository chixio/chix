import {Network} from '@chix/fbp-protocol'
import {NetworkMonitor} from '../../../network'

export function onProcessError<T extends NetworkMonitor>(this: T, event) {
  const node = event.node
  const graph = node.hasParent() ? node.getParent().pid : node.pid

  console.log('SENDING network/error', event)
  const payload: Network.Output.ErrorPayload = {
    graph,
    message: event.msg,
  }

  this.transport.sendAll('network', 'error', payload)
}
