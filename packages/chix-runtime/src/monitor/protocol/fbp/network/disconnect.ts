import {Network} from '@chix/fbp-protocol'
import {toPayload} from '../../../../helper'
import {NetworkMonitor} from '../../../network'

export function onDisconnect<T extends NetworkMonitor>(this: T, link) {
  // Temporary disable
  //    self.logger.info('network:disconnect', 'Network disconnect', link.id)
  //  Not sure why it was disabled?
  const payload: Network.Output.DisconnectPayload = toPayload(link)
  this.transport.sendAll(
    'network',
    'disconnect',
    payload,
    link.graphPid // TODO: not sure
  )
}
