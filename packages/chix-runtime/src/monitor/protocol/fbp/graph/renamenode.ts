import {GraphMonitor} from '../../../graph'

export function onRenameNode<T extends GraphMonitor>(this: T, _graph, event) {
  const node = event.node

  this.logger.info('graph:renamenode', 'Renaming node', node.identifier)
  // not used, not sure why one wants to rename the id..
  /*
    transport.sendAll('graph', 'renamenode', {
      from: data.from.id
      to: data.to.id,
      graph: this.id
    })
  */
}
