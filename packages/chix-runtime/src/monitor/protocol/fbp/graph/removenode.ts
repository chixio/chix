import {Graph} from '@chix/fbp-protocol'
import {GraphMonitor} from '../../../graph'

export function onRemoveNode<T extends GraphMonitor>(this: T, graph, event) {
  const node = event.node

  this.logger.info('graph:removenode', 'Removing node', node.identifier)

  const payload: Graph.Output.RemoveNodePayload = {
    id: node.id,
    graph: graph.id,
  }

  this.transport.sendAll('graph', 'removenode', payload)
}
