import {Graph} from '@chix/fbp-protocol'
import {GraphMonitor} from '../../../graph'

export function onRemoveInitial<T extends GraphMonitor>(this: T, graph, event) {
  const payload: Graph.Output.RemoveInitialPayload = {
    tgt: {
      node: event.node.id,
      port: event.port,
    },
    src: {
      data: event.context,
    },
    graph: graph.id,
  }

  this.transport.sendAll('graph', 'removeinitial', payload)
}
