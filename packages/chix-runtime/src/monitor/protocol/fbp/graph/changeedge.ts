import {Graph} from '@chix/fbp-protocol'
import {Link} from '@chix/flow'
import {config} from '../../../../config'
import {GraphMonitor} from '../../../graph'

export function onChangeEdge<T extends GraphMonitor>(
  this: T,
  graph,
  link: Link
) {
  this.logger.info('graph:changeedge', 'Changing edge', link.id)

  const payload: Graph.Output.ChangeEdgePayload = {
    src: {
      node: link.source.id,
      port: link.source.port,
      index: link.source.get('index'),
    },
    tgt: {
      node: link.target.id,
      port: link.target.port,
      index: link.target.get('index'),
    },
    graph: graph.id,
  }

  if (link.target.has('persist')) {
    payload.metadata = {
      route: config.persist_route,
    }
  }

  this.transport.sendAll('graph', 'changeedge', payload)
}
