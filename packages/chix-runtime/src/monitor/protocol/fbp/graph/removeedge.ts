import {Graph} from '@chix/fbp-protocol'
import {Link} from '@chix/flow'
import {GraphMonitor} from '../../../graph'

export function onRemoveEdge<T extends GraphMonitor>(
  this: T,
  graph,
  link: Link
) {
  this.logger.info('graph:removeedge', 'Removing edge', link.id)

  const payload: Graph.Output.RemoveEdgePayload = {
    src: {
      node: link.source.id,
      port: link.source.port,
      index: link.source.get('index'),
    },
    tgt: {
      node: link.target.id,
      port: link.target.port,
      index: link.target.get('index'),
    },
    graph: graph.id,
    // metadata:
  }

  this.transport.sendAll('graph', 'removeedge', payload)
}
