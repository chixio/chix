import {Graph} from '@chix/fbp-protocol'
import {GraphMonitor} from '../../../graph'
import {onAddInitial} from './addinitial'
import {onRemoveInitial} from './removeinitial'

export function onAddNode<T extends GraphMonitor>(this: T, graph, event) {
  const node = event.node

  this.logger.info('graph:addnode', 'Adding node', node.identifier)

  const payload: Graph.Output.AddNodePayload = {
    id: node.id,
    // component: node.title,
    // not sure what this should be.
    component: `${node.ns}/${node.name}`,
    graph: graph.id,
    metadata: node.metadata,
  }

  // send out our addition
  this.transport.sendAll('graph', 'addnode', payload)

  node.on('contextUpdate', onAddInitial.bind(this, graph))
  node.on('contextClear', onRemoveInitial.bind(this, graph))
}
