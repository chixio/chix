import {Graph} from '@chix/fbp-protocol'
import {GraphMonitor} from '../../../graph'

export function onAddPort<T extends GraphMonitor>(this: T, graph, event) {
  const {name, nodeId, port, type} = event

  const payload:
    | Graph.Output.AddInportPayload
    | Graph.Output.AddOutportPayload = {
    public: port,
    node: nodeId,
    port: name,
    graph: graph.id,
  }

  const cmd = type === 'input' ? 'addinport' : 'addoutport'

  this.transport.sendAll('graph', cmd, payload)
}
