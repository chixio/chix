import {Graph} from '@chix/fbp-protocol'
import {GraphMonitor} from '../../../graph'

// bound to contextUpdate
export function onAddInitial<T extends GraphMonitor>(this: T, graph, event) {
  const {data, node, port} = event

  const payload: Graph.Output.AddInitialPayload = {
    src: {
      data,
    },
    metadata: {}, // note just a hack to make the fbp-protocol test happy, context cannot contain metadata.
    tgt: {
      node: node.id,
      port,
      // index: not yet support by context.
    },
    graph: graph.id,
    // metadata:
  }

  this.transport.sendAll('graph', 'addinitial', payload)
}
