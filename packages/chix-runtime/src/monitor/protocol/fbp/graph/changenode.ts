import {Graph} from '@chix/fbp-protocol'
import {GraphMonitor} from '../../../graph'

export function onChangeNode<T extends GraphMonitor>(this: T, graph, event) {
  const node = event.node

  const payload: Graph.Output.ChangeNodePayload = {
    id: node.id,
    metadata: node.metadata,
    graph: graph.id,
  }

  this.transport.sendAll('graph', 'changenode', payload)
}
