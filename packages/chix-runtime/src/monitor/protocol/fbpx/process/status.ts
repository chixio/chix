// import { Process } from '@chix/fbp-protocol'
import {ProcessMonitor} from '../../../process'

export function onProcessStatus<T extends ProcessMonitor>(this: T, event) {
  const node = event.node
  this.logger.info(
    'process:status',
    event.status,
    node.title || node.identifier
  )
  this.transport.sendAll('process', 'status', {
    status: event.status,
    graph: node.parentId || node.id,
    process: node,
  })
}
