// import { Process } from '@chix/fbp-protocol'
import {ProcessMonitor} from '../../../process'

export function onProcessError<T extends ProcessMonitor>(this: T, event) {
  const node = event.node

  this.transport.sendAll('network', 'processerror', {
    id: node.id,
    graph: node.hasParent() ? node.getParent().id : node.id,
    error: event.msg.message,
  })

  this.transport.sendAll('process', 'error', {
    graph: node.hasParent() ? node.getParent().id : node.id,
    message: event.msg,
  })
}
