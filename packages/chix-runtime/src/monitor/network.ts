import {IoHandler, ProcessManager} from '@chix/flow'
import {Transporter, TransporterOptions} from '../transporter'
import * as commands from './protocol/fbp/network/'

export type NetworkMonitorOptions = {
  compat?: boolean
} & TransporterOptions

export class NetworkMonitor extends Transporter {
  public static monitor(
    processManager: ProcessManager,
    ioHandler: IoHandler,
    transport,
    logger?,
    options?: NetworkMonitorOptions
  ) {
    return new NetworkMonitor({
      ...options,
      logger,
      transport,
    }).monitor(processManager, ioHandler)
  }

  public options: NetworkMonitorOptions = {}

  constructor(options: NetworkMonitorOptions) {
    super({
      logger: options.logger,
      transport: options.transport,
    })

    this.options.compat = !!options.compat
  }

  /**
   *
   * Monitors the IOHandler
   *
   * Usage:
   *
   *   NetworkMonitor.monitor(ioHandler, transport)
   *
   * Transport is an object containing send function.
   *
   * @param {ProcessManager} processManager
   * @param {IoHandler} io
   */
  public monitor(processManager: ProcessManager, io: IoHandler) {
    // Listeners test whether this is a flow...
    processManager.on('startProcess', commands.onStartProcess.bind(this))
    processManager.on('error', commands.onProcessError.bind(this))
    processManager.on('stopProcess', commands.onEndProcess.bind(this))

    io.on('send', commands.onConnect.bind(this))
    if (this.options.compat) {
      io.on('data', commands.onData.bind(this))
    } else {
      // emit whole packet
      io.on('packet', commands.onData.bind(this))
    }
    io.on('receive', commands.onDisconnect.bind(this))
  }
}

/*

  Determine how and if to enable this.

  io.chi.on('begingroup', function onBeginGroup(group) {
    // this wants the link, but I don't send it.
    // group.info.gid
    // so let's just skip groups
    var p = toPayLoad(link)
    p.group = group.info.gid

    self.transport.sendAll('network', 'begingroup', p, graphId)
  })

  io.chi.on('endgroup', function onEndGroup(group) {
    const p = toPayLoad(link)
    p.group = group.info.gid

    self.transport.sendAll('network', 'endgroup', p, graphId)

  })
}
*/
