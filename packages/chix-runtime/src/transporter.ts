import {ProcessManager} from '@chix/flow'
import {Loader} from '@chix/loader'
import {BaseTransport} from '@chix/transport'
import {Core, CoreOptions} from './core'

export type TransporterOptions = {
  transport?: BaseTransport
} & CoreOptions

/**
 * Transporter
 *
 * Transporters can transport.
 */
export class Transporter extends Core {
  public transport: BaseTransport
  public processManager: ProcessManager
  public loader: Loader
  constructor(options: TransporterOptions) {
    super({
      logger: options.logger,
    })

    if (!options.transport) {
      console.log(options)
      throw Error('No transport option found.')
    }

    this.transport = options.transport
  }
}
