module.exports =
  title: "fs readFile"
  description: "fs readFile"
  ns: "fs"
  name: "readFile"
  env: "server"

  require:
    fs: "builtin"

  phrases:
    active: "Reading file {{input.file}} ({{input.options.encoding}}, {{input.options.flag}})"

  ports:
    input:
      file:
        title: "Filename"
        type: "string"
        required: true

      encoding:
        title: "Encoding"
        type: "string"
        default: "utf-8"

      flag:
        title: "Flag"
        type: "string"
        default: "r"

    output:
      error:
        type: "object"

      out:
        type: "object"

  fn: () ->
    output = [
      fs
      "readFile"
      input.file
      {
        flag: input.flag
        encoding: input.encoding
      }
    ]
