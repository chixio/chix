import 'should'

import {ProcessManager} from '@chix/flow'
import {Loader} from '@chix/loader'
import {DummyTransport} from '@chix/transport'
import {ComponentHandler} from '@chix/runtime'
import {schemas} from '@chix/fbp-protocol'

// TODO: this just loads the definitions from the live webserver.
// Doesn't matter that much I think..

describe('Component Handler:', () => {
  let loader: Loader

  before(() => {
    loader = new Loader()
  })

  it.skip('Should load node definitions', (done) => {
    const transport = new DummyTransport({
      logger: console,
      schemas,
    })

    console.log(loader)

    const processManager = new ProcessManager()

    ComponentHandler.handle(
      loader,
      transport,
      processManager
      /*, console */
    )

    transport.once('send', (data, conn) => {
      data.protocol.should.eql('component')
      data.command.should.eql('component')

      conn.should.eql('test-connection')

      done()
    })

    loader.addNodeDefinitions('https://api.chix.io/nodes/{ns}/{name}', [
      {
        ns: 'my',
        name: 'component',
        ports: {output: {out: {type: 'string'}}},
      },
      {
        ns: 'my',
        name: 'component2',
        ports: {input: {in: {type: 'string'}}},
      },
    ])

    // trigger component action
    transport.receive(
      {
        protocol: 'component',
        command: 'list',
      },
      'test-connection'
    )
  })
})
