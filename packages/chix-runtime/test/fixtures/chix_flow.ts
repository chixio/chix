export const chix_flow = {
  id: '3a40e679-b6f6-460e-98d9-92f5a4f7e330',
  type: 'flow',
  links: [
    {
      source: {
        id: 'Bot',
        port: 'bot',
      },
      target: {
        id: 'Msg',
        port: 'bot',
        setting: {
          persist: true,
        },
      },
      metadata: {
        title: 'Bot bot -> bot Msg',
      },
    },
    {
      source: {
        id: 'Bot',
        port: 'bot',
      },
      target: {
        id: 'Say',
        port: 'bot',
        setting: {
          persist: true,
        },
      },
      metadata: {
        title: 'Bot bot -> bot Say',
      },
    },
    {
      source: {
        id: 'Msg',
        port: 'from',
      },
      target: {
        id: 'Reply',
        port: 'replace',
      },
      metadata: {
        title: 'Msg from -> replace Reply',
      },
    },
    {
      source: {
        id: 'Msg',
        port: 'message',
      },
      target: {
        id: 'rChix',
        port: 'in',
      },
      metadata: {
        title: 'Msg message -> in rChix',
      },
    },
    {
      source: {
        id: 'rChix',
        port: 'out',
      },
      target: {
        id: 'Reply',
        port: ':start',
      },
      metadata: {
        title: 'rChix out -> :start Reply',
      },
    },
    {
      source: {
        id: 'Msg',
        port: 'message',
      },
      target: {
        id: 'rUptime',
        port: 'in',
      },
      metadata: {
        title: 'Msg message -> in rUptime',
      },
    },
    {
      source: {
        id: 'rUptime',
        port: 'out',
      },
      target: {
        id: 'Uptime',
        port: ':start',
      },
      metadata: {
        title: 'rUptime out -> :start Uptime',
      },
    },
    {
      source: {
        id: 'Reply',
        port: 'out',
      },
      target: {
        id: 'Say',
        port: 'in',
        setting: {
          index: 0,
        },
      },
      metadata: {
        title: 'Reply out -> in Say',
      },
    },
    {
      source: {
        id: 'Uptime',
        port: 'out',
      },
      target: {
        id: 'Say',
        port: 'in',
        setting: {
          index: 1,
        },
      },
      metadata: {
        title: 'Uptime out -> in Say',
      },
    },
  ],
  nodes: [
    {
      id: 'Bot',
      title: 'Bot',
      ns: 'irc',
      name: 'bot',
    },
    {
      id: 'Join',
      title: 'Join',
      ns: 'irc',
      name: 'join',
    },
    {
      id: 'Msg',
      title: 'Msg',
      ns: 'irc',
      name: 'message',
    },
    {
      id: 'Say',
      title: 'Say',
      ns: 'irc',
      name: 'say',
      context: {
        target: '#chix.io',
      },
    },
    {
      id: 'Reply',
      title: 'Reply',
      ns: 'string',
      name: 'replace',
      context: {
        in: 'how are you #?',
        match: '#',
      },
    },
    {
      id: 'rChix',
      title: 'rChix',
      ns: 'string',
      name: 'regexp',
      context: {
        regexp: 'chi',
      },
    },
    {
      id: 'rUptime',
      title: 'rUptime',
      ns: 'string',
      name: 'regexp',
      context: {
        regexp: 'up\\s*time',
      },
    },
    {
      id: 'Uptime',
      title: 'Uptime',
      ns: 'utils',
      name: 'exec',
      context: {
        in: 'uptime',
      },
    },
  ],
  title: 'IRC Bot',
  ns: 'irc',
  name: 'botje',
  description: 'Runs an IRC Client',
  providers: {
    '@': {
      url: 'https://api.chix.io/nodes/{ns}/{name}',
    },
  },
}
