/**
 * IIP conversion is not included
 */
export const noflo_graph = {
  properties: {
    name: 'IRC Bot',
  },
  connections: [
    /*
    {
      "tgt": {
        "process": "Bot",
        "port": "server"
      },
      "data": "irc.freenode.net"
    },
    {
      "tgt": {
        "process": "Bot",
        "port": "name"
      },
      "data": "chibot"
    },
    {
      "tgt": {
        "process": "Bot",
        "port": "channels",
        "index": 0
      },
      "data": "#chix.io"
    },
    */
    {
      metadata: {
        route: 10,
      },
      tgt: {
        process: 'Msg',
        port: 'bot',
      },
      src: {
        process: 'Bot',
        port: 'bot',
      },
    },
    {
      metadata: {
        route: 10,
      },
      tgt: {
        process: 'Say',
        port: 'bot',
      },
      src: {
        process: 'Bot',
        port: 'bot',
      },
    },
    {
      tgt: {
        process: 'Reply',
        port: 'replace',
      },
      src: {
        process: 'Msg',
        port: 'from',
      },
    },
    /*
    {
      "tgt": {
        "process": "Say",
        "port": "in"
      },
      "data": [
        "/msg nickserv identify itsme",
        "I'm back.."
      ]
    },
    */
    {
      tgt: {
        process: 'rChix',
        port: 'in',
      },
      src: {
        process: 'Msg',
        port: 'message',
      },
    },
    {
      tgt: {
        process: 'Reply',
        port: ':start',
      },
      src: {
        process: 'rChix',
        port: 'out',
      },
    },
    {
      tgt: {
        process: 'rUptime',
        port: 'in',
      },
      src: {
        process: 'Msg',
        port: 'message',
      },
    },
    {
      tgt: {
        process: 'Uptime',
        port: ':start',
      },
      src: {
        process: 'rUptime',
        port: 'out',
      },
    },
    {
      tgt: {
        process: 'Say',
        port: 'in',
        index: 0,
      },
      src: {
        process: 'Reply',
        port: 'out',
      },
    },
    {
      tgt: {
        process: 'Say',
        port: 'in',
        index: 1,
      },
      src: {
        process: 'Uptime',
        port: 'out',
      },
    },
  ],
  processes: {
    Bot: {
      component: 'irc/bot',
    },
    Join: {
      component: 'irc/join',
    },
    Msg: {
      component: 'irc/message',
    },
    Say: {
      component: 'irc/say',
    },
    Reply: {
      component: 'string/replace',
    },
    rChix: {
      component: 'string/regexp',
    },
    rUptime: {
      component: 'string/regexp',
    },
    Uptime: {
      component: 'utils/exec',
    },
  },
  exports: [],
}
