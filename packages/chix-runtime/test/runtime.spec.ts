import {schemas} from '@chix/fbp-protocol'
import {ProcessManager} from '@chix/flow'
import {DummyTransport} from '@chix/transport'
import 'should'
import {RuntimeHandler, version} from '@chix/runtime'

// TODO: this just loads the definitions from the live webserver.
// Doesn't matter that much I think..
describe('Runtime Handler:', () => {
  it('Should respond to getruntime', (done) => {
    const processManager = new ProcessManager()

    const transport = new DummyTransport({
      // logger: console,
      bail: true,
      schemas,
    })

    RuntimeHandler.handle(processManager, transport, console, {
      type: 'chix',
      capabilities: ['graph:readonly'],
    })

    transport.once('send', (data, conn) => {
      data.protocol.should.eql('runtime')
      data.command.should.eql('runtime')
      data.payload.version.should.eql(version)
      data.payload.capabilities.should.eql(['graph:readonly'])

      conn.should.eql('test-connection')
      // assume the data from the server is ok

      done()
    })

    // trigger component action
    transport.receive(
      {
        protocol: 'runtime',
        command: 'getruntime',
      },
      'test-connection'
    )
  })
})
