import {IoHandler, ProcessManager} from '@chix/flow'
import {Loader} from '@chix/loader'
import {DummyTransport} from '@chix/transport'
import 'should'
import {
  GraphHandler,
  NetworkHandler,
  GraphMonitor,
  NetworkMonitor,
} from '@chix/runtime'
import {schemas} from '@chix/fbp-protocol'

// TODO: this just loads the definitions from the live webserver.
// Doesn't matter that much I think..
describe('Network Handler:', () => {
  let processManager: ProcessManager
  let io: IoHandler
  let loader: Loader
  let transport: DummyTransport

  before(() => {
    processManager = new ProcessManager()
    io = new IoHandler()
    loader = new Loader()
    transport = new DummyTransport({
      bail: true,
      schemas,
    })

    NetworkHandler.handle(processManager, transport /*, console*/)

    NetworkMonitor.monitor(processManager, io, transport /*, console*/)

    GraphMonitor.monitor(processManager, transport /*, console*/)

    GraphHandler.handle(processManager, io, loader, transport /*, console*/)
  })

  it('Network start should fail if no graph id was specified', (done) => {
    transport.once('send', (data, conn) => {
      // TODO: bit of a wrong design here..
      data.protocol.should.eql('network')
      data.command.should.eql('error')
      data.payload.message
        .toString()
        .should.eql('Missing required property: graph')
      conn.should.eql('test-connection')

      done()
    })

    // trigger component action
    transport.receive(
      {
        protocol: 'network',
        command: 'start',
        payload: {},
        // graph: 'my-graph'
      },
      'test-connection'
    )
  })

  it('Network start should fail if graph not found', (done) => {
    transport.once('send', (data, conn) => {
      // err, ok bit of a wrong design here..
      data.protocol.should.eql('network')
      data.command.should.eql('error')
      data.payload.message.toString().should.eql('Error: Unable to find graph')
      conn.should.eql('test-connection')

      done()
    })

    // trigger component action
    transport.receive(
      {
        protocol: 'network',
        command: 'start',
        payload: {
          graph: 'my-graph',
        },
      },
      'test-connection'
    )
  })

  it('Network should start if graph was found', (done) => {
    transport.once('send', (data, _conn) => {
      console.log('send?')
      // first message
      data.protocol.should.eql('graph')
      data.command.should.eql('clear')

      // second message
      transport.once('send', (data, conn) => {
        console.log('send 2?')
        data.protocol.should.eql('network')
        data.command.should.eql('started')
        data.payload.graph.should.eql('my-graph')
        data.payload.should.not.have.property('command')
        data.payload.should.not.have.property('protocol')
        conn.should.eql('test-connection')
        done()
      })
    })

    processManager.once('addProcess', () => {
      console.log('Process added?')
      // trigger creation of a graph
      transport.receive(
        {
          protocol: 'network',
          command: 'start',
          payload: {
            graph: 'my-graph',
          },
        },
        'test-connection'
      )
    })

    // create a graph
    transport.receive(
      {
        protocol: 'graph',
        command: 'clear',
        payload: {
          id: 'my-graph',
          name: 'My Graph',
        },
      },
      'test-connection'
    )
  })

  it('Network should be able to tell it is running', (done) => {
    transport.once('send', (data, conn) => {
      data.protocol.should.eql('network')
      data.command.should.eql('status')
      data.payload.graph.should.eql('my-graph')
      data.payload.running.should.not.be.ok
      // data.payload.uptime.should.be.ok
      conn.should.eql('test-connection')

      done()
    })

    // trigger creation of a graph
    transport.receive(
      {
        protocol: 'network',
        command: 'getstatus',
        payload: {
          graph: 'my-graph',
        },
      },
      'test-connection'
    )
  })

  it('Network should fail to stop a non existing graph', (done) => {
    transport.once('send', (data, conn) => {
      data.protocol.should.eql('network')
      data.command.should.eql('error')
      data.payload.message.toString().should.eql('Error: Unable to find graph')
      conn.should.eql('test-connection')

      done()
    })

    // trigger creation of a graph
    transport.receive(
      {
        protocol: 'network',
        command: 'stop',
        payload: {
          graph: 'my-fault',
        },
      },
      'test-connection'
    )
  })

  it('Network should be able to stop if graph was found', (done) => {
    transport.once('send', (data, conn) => {
      // transport.once('network:stopped', function(data, conn) {

      data.protocol.should.eql('network')
      data.command.should.eql('stopped')
      data.payload.graph.should.eql('my-graph')
      data.payload.should.not.have.property('command')
      data.payload.should.not.have.property('protocol')
      conn.should.eql('test-connection')

      done()
    })

    // trigger creation of a graph
    transport.receive(
      {
        protocol: 'network',
        command: 'stop',
        payload: {
          graph: 'my-graph',
        },
      },
      'test-connection'
    )
  })

  it('Network should be able to tell it stopped', (done) => {
    transport.once('send', (data, conn) => {
      data.protocol.should.eql('network')
      data.command.should.eql('status')
      data.payload.running.should.not.be.ok
      data.payload.graph.should.eql('my-graph')
      // data.payload.uptime.should.be.ok
      conn.should.eql('test-connection')

      done()
    })

    // trigger creation of a graph
    transport.receive(
      {
        protocol: 'network',
        command: 'getstatus',
        payload: {
          graph: 'my-graph',
        },
      },
      'test-connection'
    )
  })
})
