import {IoHandler, ProcessManager} from '@chix/flow'
import {Loader} from '@chix/loader'
import {DummyTransport} from '@chix/transport'
import 'should'
import {NetworkMonitor} from '@chix/runtime'
import {schemas} from '@chix/fbp-protocol'

describe('Dummy Transport Handler:', () => {
  let processManager: ProcessManager
  let io: IoHandler
  let transport: DummyTransport
  let loader: Loader
  before(() => {
    processManager = new ProcessManager()
    io = new IoHandler()
    transport = new DummyTransport({
      bail: true,
      schemas,
    })
    loader = new Loader()

    NetworkMonitor.monitor(processManager, io, transport /*, console*/)

    loader.addNodeDefinitions('@', [
      {
        ns: 'my',
        name: 'component',
        ports: {
          input: {
            in: {
              type: 'string',
            },
          },
          output: {
            out: {
              type: 'string',
            },
          },
        },
      },
      {
        ns: 'my',
        name: 'component2',
        ports: {input: {in: {type: 'string'}}},
      },
    ])
  })

  it('Should subscribe during clear', (done) => {
    transport.once('graph', (_payload, conn) => {
      transport.isSubscribed(conn, 'graph', 'my-id').should.equal(true)
      conn.should.eql('test-connection')
      done()
    })

    transport.receive(
      {
        protocol: 'graph',
        command: 'clear',
        payload: {
          id: 'my-id',
          name: 'My Graph',
        },
      },
      'test-connection'
    )
  })
})
