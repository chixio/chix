import {Flow, IoHandler, ProcessManager} from '@chix/flow'
import {Loader} from '@chix/loader'
import {DummyTransport} from '@chix/transport'
import {expect} from 'chai'
import * as fs from 'fs'
import * as path from 'path'
import 'should'
import {ComponentHandler, GraphHandler, NetworkMonitor} from '@chix/runtime'
import {schemas} from '@chix/fbp-protocol'
import {ChixParser} from '@chix/fbpx'

// TODO: this just loads the definitions from the live webserver.
// Doesn't matter that much I think..
describe('Graph Handler:', () => {
  let processManager: ProcessManager
  let io: IoHandler
  let loader: Loader
  let transport: DummyTransport

  before(() => {
    processManager = new ProcessManager()
    io = new IoHandler()
    transport = new DummyTransport({
      bail: true,
      schemas,
    })
    loader = new Loader()

    NetworkMonitor.monitor(processManager, io, transport /*, console*/)

    loader.addNodeDefinitions('https://api.chix.io/nodes/{ns}/{name}', [
      {
        ns: 'my',
        name: 'component',
        ports: {
          input: {
            in: {
              type: 'string',
            },
          },
          output: {
            out: {
              type: 'string',
            },
          },
        },
      },
      {
        ns: 'my',
        name: 'component2',
        ports: {
          input: {in: {type: 'string'}},
        },
      },
    ])
  })

  describe('Given an existing graph:', () => {
    const provider = 'https://api.chix.io/nodes/{ns}/{name}'
    let gh: GraphHandler
    let graph: Flow

    before(() => {
      gh = GraphHandler.handle(processManager, io, loader, transport, console)
    })

    it("It's definition should be known to the loader", async (done) => {
      gh.on('addgraph', (node) => {
        // the nodedefinition is available within '@'
        // however this was not so before.
        // So what does '@' actually mean.
        // There is the concept of namespaces.
        // and providers are grouped by namespaces.
        // So I'm assuming the whole loader thing is incorrect now.
        // but this should not be the case because I did not change the tests that much.
        // So let's look at the tests of chix-loader.
        // I think it still does it correctly.
        // Only difference is the direct usage of Loader (not FSLoader or NPMLoader)
        // So yes, '@' contains the default provider, it tells where to get the nodes
        // which do not carry a .provider property.
        //
        loader.hasNodeDefinition(provider, 'tests', 'myGraph').should.eql(true)

        /*
           {
  id: 'e67f5d0b-0d44-4e95-b617-09e78e3bde46',
  links: [],
  name: 'myGraph',
  nodes: [],
  ns: 'tests',
  ports: { input: {}, output: {} },
  provider: 'https://api.chix.io/nodes/{ns}/{name}',
  providers: {
  '@': { url: 'https://api.chix.io/nodes/{ns}/{name}' }
},
  title: 'Test Graph',
  type: 'flow'
}
        */

        const def = loader.getNodeDefinitionFrom(provider, 'tests', 'myGraph')
        def.name.should.eql('myGraph')
        def.ns.should.eql('tests')
        expect(def.title).to.eql('Test Graph')
        expect(def.type).to.eql('flow')
        graph = node
        done()
      })

      await gh.loadFbp(
        fs
          .readFileSync(path.join(__dirname, './fixtures/graph.fbp'))
          .toString(),
        ChixParser({skipIDs: true}),
        {no_ids: true}
      )
    })

    it('Should be able to send graph as source ', (done) => {
      // Handles component commands
      ComponentHandler.handle(loader, transport, processManager /*, logger*/)

      transport.on('send', (data) => {
        data.protocol.should.eql('component')
        data.command.should.eql('source')
        data.payload.should.be.an('object')
        data.payload.name.should.eql('myGraph')
        data.payload.library.should.eql('tests')
        data.payload.code.should.be.ok()
        done()
      })

      expect(graph.id).to.be.a('string')

      if (!graph.id) {
        console.log(graph)
        throw Error('no graph id')
      }

      transport.receive(
        {
          protocol: 'component',
          command: 'getsource',
          payload: {
            name: graph.id,
          },
        },
        'test-connection'
      )
    })
  })
})
