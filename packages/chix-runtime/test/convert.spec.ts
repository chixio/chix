import 'should'

import {chixToNoFlo} from '@chix/noflo'
import {chix_flow, noflo_graph} from './fixtures'

describe('Conversions:', () => {
  it.skip('Should convert chix flow to noflo graph', () => {
    // sig changed, needs getNodeDefinition
    chixToNoFlo(chix_flow).should.eql(noflo_graph)
  })
})
