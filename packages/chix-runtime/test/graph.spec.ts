/* global describe, it, before */

import {Flow, IoHandler, ProcessManager} from '@chix/flow'
import {Loader} from '@chix/loader'
import {DummyTransport} from '@chix/transport'
import 'should'
import {config, GraphHandler, GraphMonitor, NetworkMonitor} from '@chix/runtime'
import {schemas} from '@chix/fbp-protocol'

config.setProvider('remote')

// TODO: this just loads the definitions from the live webserver.
// Doesn't matter that much I think..

describe('Graph Handler:', () => {
  let processManager: ProcessManager
  let io: IoHandler
  let loader: Loader
  let transport: DummyTransport
  let testGraph: Flow
  let graphHandler: GraphHandler
  const conn = 'test-connection'

  before(() => {
    processManager = new ProcessManager()
    io = new IoHandler()
    transport = new DummyTransport({
      bail: true,
      schemas,
    })

    graphHandler = new GraphHandler({
      transport,
    })

    transport.register(conn)

    loader = new Loader()

    NetworkMonitor.monitor(processManager, io, transport /*, console*/)

    graphHandler.handle(processManager, io, loader)

    // Monitors the graphs for changes
    GraphMonitor.monitor(processManager, transport /*, console*/)

    loader.addNodeDefinitions('https://api.chix.io/v1/nodes/{ns}/{name}', [
      {
        ns: 'my',
        name: 'component',
        ports: {
          input: {
            in: {
              type: 'string',
            },
          },
          output: {
            out: {
              type: 'string',
            },
          },
        },
        fn: '',
      },
      {
        ns: 'my',
        name: 'component2',
        ports: {
          input: {
            in: {
              type: 'string',
            },
          },
        },
        fn: '',
      },
    ])
  })

  it('Should clear graph', (done) => {
    processManager.once('addProcess', (node) => {
      node.id.should.eql('my-id')
      node.title.should.eql('My Graph')
      node.description.should.eql('My Graph')
      node.type.should.eql('flow')

      // remember graph for the rest of the test
      testGraph = node

      // done()
    })

    transport.on('graph:clear', (data) => {
      data.protocol.should.eql('graph')
      data.command.should.eql('clear')
      data.payload.id.should.eql('my-id')
      data.payload.name.should.eql('My Graph')
      data.payload.main.should.eql(true)
      done()
    })

    transport.receive(
      {
        protocol: 'graph',
        command: 'clear',
        payload: {
          id: 'my-id',
          name: 'My Graph',
          main: true,
        },
      },
      conn
    )

    describe('Using this graph', () => {
      it('Should add Node', (done) => {
        processManager.once('addProcess', (node) => {
          node.id.should.eql('node-1')
          node.ns.should.eql('my')
          node.name.should.eql('component')
        })

        transport.once('graph:addnode', (data) => {
          if (data.protocol === 'graph') {
            data.command.should.eql('addnode')
            data.payload.graph.should.eql('my-id')
            data.payload.component.should.eql('my/component')
            done()
          }
        })

        transport.receive(
          {
            protocol: 'graph',
            command: 'addnode',
            payload: {
              id: 'node-1',
              component: 'my/component',
              graph: 'my-id',
              metadata: {
                // Warn, a bit confusing, internally there is
                // also a defaultProvider.
                provider: 'https://api.chix.io/v1/nodes/{ns}/{name}',
              },
            },
          },
          conn
        )
      })

      // for the linking
      it('Should add second node', (done) => {
        processManager.once('addProcess', (node) => {
          node.id.should.eql('node-2')
          node.ns.should.eql('my')
          node.name.should.eql('component2')
          done()
        })

        transport.receive(
          {
            protocol: 'graph',
            command: 'addnode',
            payload: {
              id: 'node-2',
              component: 'my/component2',
              graph: 'my-id',
              metadata: {
                // Warn, a bit confusing, internally there is
                // also a defaultProvider.
                provider: 'https://api.chix.io/v1/nodes/{ns}/{name}',
              },
            },
          },
          conn
        )
      })

      it('Should addedge', (done) => {
        testGraph.once('addLink', (link) => {
          // link.id.should.eql('my-link-id')
          link.source.id.should.eql('node-1')
          link.source.port.should.eql('out')
          link.source.setting.index.should.eql(1)
          link.target.id.should.eql('node-2')
          link.target.port.should.eql('in')
          link.target.setting.index.should.eql(3)

          done()
        })

        transport.receive(
          {
            protocol: 'graph',
            command: 'addedge',
            // id: 'my-link-id',
            payload: {
              src: {
                node: 'node-1',
                port: 'out',
                index: 1,
              },
              tgt: {
                node: 'node-2',
                port: 'in',
                index: 3,
              },
              graph: 'my-id',
            },
          },
          conn
        )
      })

      it('Should change edge (set properties)', (done) => {
        testGraph.once('changeLink', (link) => {
          // link.id.should.eql('my-link-id')
          link.source.id.should.eql('node-1')
          link.source.port.should.eql('out')
          link.source.get('index').should.eql(0)
          link.target.id.should.eql('node-2')
          link.target.port.should.eql('in')
          link.target.get('index').should.eql(1)
          link.target.get('persist').should.eql(true)
          link.metadata.route.should.eql(config.persist_route)

          done()
        })

        transport.receive(
          {
            protocol: 'graph',
            command: 'changeedge',
            // id: 'my-link-id',
            payload: {
              src: {
                node: 'node-1',
                port: 'out',
                index: 0,
              },
              metadata: {
                route: config.persist_route,
              },
              tgt: {
                node: 'node-2',
                port: 'in',
                index: 1,
              },
              graph: 'my-id',
            },
          },
          conn
        )
      })

      it('Should change edge (remove properties)', (done) => {
        testGraph.once('changeLink', (link) => {
          // link.id.should.eql('my-link-id')
          link.source.id.should.eql('node-1')
          link.source.port.should.eql('out')
          link.source.get('index').should.eql(1)
          link.target.id.should.eql('node-2')
          link.target.port.should.eql('in')
          link.target.get('index').should.eql(0)
          link.target.has('persist').should.eql(false)
          link.metadata.should.not.have.property('route')

          done()
        })

        // let's just use link.setting for metadata.
        transport.receive(
          {
            protocol: 'graph',
            command: 'changeedge',
            // id: 'my-link-id',
            payload: {
              src: {
                node: 'node-1',
                port: 'out',
                index: 1,
              },
              tgt: {
                node: 'node-2',
                port: 'in',
                index: 0,
              },
              graph: 'my-id',
            },
          },
          conn
        )
      })

      it('Should expose inport', (done) => {
        testGraph.once('addPort', (event) => {
          event.port.should.eql('IN')
          testGraph.ports.input.should.have.property('IN')
          done()
        })

        transport.receive(
          {
            protocol: 'graph',
            command: 'addinport',
            payload: {
              node: 'node-2',
              port: 'in',
              public: 'IN',
              graph: 'my-id',
            },
          },
          conn
        )
      })

      it('Should expose outport', (done) => {
        testGraph.once('addPort', (event) => {
          event.port.should.eql('OUT')
          testGraph.ports.output.should.have.property('OUT')
          // same as inport.
          // not sure if with define property or flattened withon
          // the JSON definition itself, the latter is best I think.
          // testGraph.ports.output.OUT.type.should.eql('string')
          done()
        })

        transport.receive(
          {
            protocol: 'graph',
            command: 'addoutport',
            payload: {
              node: 'node-1',
              port: 'out',
              public: 'OUT',
              graph: 'my-id',
            },
          },
          conn
        )
      })

      it('Should rename inport', (done) => {
        testGraph.once('renamePort', (event) => {
          event.from.should.eql('IN')
          event.to.should.eql('NI')
          testGraph.ports.input.should.not.have.property('IN')
          testGraph.ports.input.should.have.property('NI')
          done()
        })

        transport.receive(
          {
            protocol: 'graph',
            command: 'renameinport',
            payload: {
              node: 'node-2',
              from: 'IN',
              to: 'NI',
              graph: 'my-id',
            },
          },
          conn
        )
      })

      it('Should rename outport', (done) => {
        testGraph.once('renamePort', (event) => {
          event.from.should.eql('OUT')
          event.to.should.eql('TOU')
          testGraph.ports.output.should.not.have.property('OUT')
          testGraph.ports.output.should.have.property('TOU')
          done()
        })

        transport.receive(
          {
            protocol: 'graph',
            command: 'renameoutport',
            payload: {
              node: 'node-1',
              from: 'OUT',
              to: 'TOU',
              graph: 'my-id',
            },
          },
          conn
        )
      })

      it('Should export itself', (done) => {
        const nodeDefinition = testGraph.toJSON()

        nodeDefinition.id.should.eql('my-id')
        nodeDefinition.ns.should.eql('flowhub')
        nodeDefinition.name.should.eql('my-graph')
        nodeDefinition.title.should.eql('My Graph')
        nodeDefinition.type.should.eql('flow')
        nodeDefinition.description.should.eql('My Graph')
        nodeDefinition.nodes.should.have.length(2)
        nodeDefinition.nodes[0].should.eql({
          id: 'node-1',
          ns: 'my',
          name: 'component',
          provider: '@',
        })
        nodeDefinition.nodes[1].should.eql({
          id: 'node-2',
          ns: 'my',
          name: 'component2',
          provider: '@',
        })

        nodeDefinition.links.should.have.length(1)
        nodeDefinition.links[0].source.should.eql({
          setting: {
            index: 1,
          },
          id: 'node-1',
          port: 'out',
        })

        nodeDefinition.links[0].target.should.eql({
          setting: {
            index: 0,
          },
          id: 'node-2',
          port: 'in',
        })

        nodeDefinition.should.have.property('providers')

        nodeDefinition.providers.should.eql({
          '@': {
            // default provider
            url: 'https://api.chix.io/v1/nodes/rhalff/{ns}/{name}',
          },
        })

        done()
      })

      // BEGIN ACTIVE NETWORKING TEST
      it.skip('Should not be able to start transmission if the network was not started', (_done) => {})

      it.skip('The network should be able to send (std) out', (_done) => {})

      it.skip('The network should be able to send (std) error', (_done) => {})

      it('The network should send connect if there is transmission on an edge', (done) => {
        transport.once('network:connect', (data) => {
          data.protocol.should.eql('network')
          data.command.should.eql('connect')

          done()
        })

        // send to the in port and test if it is notified.
        testGraph.sendIIP(
          {
            id: 'node-1',
            port: 'in',
          },
          'test'
        )
      })

      it.skip('Loader should be able to give *current* definition', (_done) => {
        // is added internally, provider name is just a name
        // ok that's why `library` current graph does not per se have
        // a ns: name: pair at first, it should
        // GraphHandler. initMap *does* add ourselves,
        // so we should already be known to the loader.
        // however this test starts with a clean graph.
        /*
        loader.getNodeDefinition(
          'https://api.chix.io/v1/nodes/{ns}/{name}'
        )
        */
      })

      it.skip('The network should send begingroup if a group was started', (_done) => {})

      it.skip('The network should send `data` if there is data transmittion on an edge', (_done) => {})

      it.skip('The network should send `endgroup` if the group has ended', (_done) => {})

      /* Disconnect in chix means a link was disconnected
            it("The network should send disconnect if there was an end of transmission", function(done) {

            })
      */

      // END ACTIVE NETWORKING TEST

      // destruction of our graph.
      // must beware though, if it has become a subgraph/component
      // it should not be able to destruct.
      it('Should remove edge', (done) => {
        //
        testGraph.once('removeLink', (_link) => {
          // link is removed, but iip from the push is still there..
          // testGraph.links.size.should.eql(0)
          testGraph.getLinks().should.have.lengthOf(0)
          done()
        })

        transport.receive(
          {
            protocol: 'graph',
            command: 'removeedge',
            // id: 'my-link-id',
            payload: {
              src: {
                node: 'node-1',
                port: 'out',
                index: 1,
              },
              tgt: {
                node: 'node-2',
                port: 'in',
                index: 3,
              },
              graph: 'my-id',
            },
          },
          conn
        )
      })

      it('Should remove Node 1', (done) => {
        testGraph.once('removeNode', (_node) => {
          testGraph.nodes.size.should.eql(1)
          done()
        })

        transport.receive(
          {
            protocol: 'graph',
            command: 'removenode',
            payload: {
              id: 'node-1',
              graph: 'my-id',
            },
          },
          conn
        )
      })

      it('Should remove Node 2', (done) => {
        testGraph.once('removeNode', (_node) => {
          testGraph.nodes.size.should.eql(0)
          done()
        })

        transport.receive(
          {
            protocol: 'graph',
            command: 'removenode',
            payload: {
              id: 'node-2',
              graph: 'my-id',
            },
          },
          conn
        )
      })
    })
  })
})
