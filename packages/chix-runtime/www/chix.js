require('util.promisify').shim()
const Runtime = require('./lib/runtime')
const Transport = require('@chix/transport')

const log = {
  info: function () {
    console.info(Array.prototype.slice.call(arguments))
  },
  error: function () {
    console.error(Array.prototype.slice.call(arguments))
  },
  warn: function () {
    console.warn(Array.prototype.slice.call(arguments))
  },
  debug: function () {
    console.debug(Array.prototype.slice.call(arguments))
  },
  log: function () {
    console.log(Array.prototype.slice.call(arguments))
  }
}

const transport = new Transport.WebSocketBrowserTransport({
  logger: log,
  protocol: 'noflo-runtime',
  host: 'localhost',
  port: 4200 
})

const chixRuntimeOptions = {
  type: 'chix',
  // capabilities?:
  // args: program.args,
  loader: 'remote',
  compat: false,
  purge: true,
  nocache: true,
  logger: log,
  token: 'ACCESS_TOKEN'
}

window.runtime = Runtime.ChixRuntime(
  transport,
  log,
  chixRuntimeOptions
)
