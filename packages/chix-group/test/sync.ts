import 'should'

import {Packet} from '@chix/flow'
import * as jsc from 'js-combinatorics'
import {SendGroup, Sync, Group} from '@chix/group'
import {RunSequence} from './types'

export type Collected = [string, any][]

describe('sync:', () => {
  let p1: Packet
  let p2: Packet
  let p3: Packet
  let d1: Packet
  let d2: Packet
  let d3: Packet
  let sg: SendGroup
  let closePacket: Packet
  let openPacket: Packet

  let expected: [
    number,
    {
      in1: Packet
      in2: Packet
    }
  ][]

  function reset() {
    p1 = Packet.create('A')
    p2 = Packet.create('B')
    p3 = Packet.create('C')
    sg = SendGroup.create()

    // stamp the packets with our group information
    openPacket = sg.open()
    sg.write(p1)
    sg.write(p2)
    sg.write(p3)
    closePacket = sg.close()

    // Clone so it contains the same gid and itemId
    d1 = p1.clone(p1.owner)
    d1.write(d1.owner, 'A2')

    d2 = p2.clone(p2.owner)
    d2.write(d2.owner, 'B2')

    d3 = p3.clone(p3.owner)
    d3.write(d3.owner, 'C2')
    expected = [
      [1, {in1: p1, in2: d1}],
      [2, {in1: p2, in2: d2}],
      [3, {in1: p3, in2: d3}],
    ]
  }

  beforeEach(reset)

  function receivePackets(sync: Sync) {
    sync.add('in1', p1)
    sync.add('in2', d1)
    sync.add('in1', p2)
    sync.add('in1', p3)
    sync.add('in2', d3)
    sync.add('in2', d2)
    sync.receive(closePacket)
  }

  describe('Unordered should emit', () => {
    it('Should be able to receive unordered', (done) => {
      const sync = Sync.create(['in1', 'in2'])
      sync.receive(openPacket)
      let cnt = 0

      const collected: Collected = []
      sync.on('sync', (itemId, value) => {
        cnt++
        collected.push([itemId, value])
        if (cnt === 3) {
          done()
        }
      })
      receivePackets(sync)
    })
    it('Should be able to do so in any execution order', function (done) {
      // Tests 5040 combinations
      this.timeout(10000) // Note: not available if using -^ arrow function

      let runs = 0
      function run(runSequence: RunSequence) {
        runs++

        let cnt = 0

        reset()

        const sync = Sync.create(['in1', 'in2'], true)

        // receive open must always be first
        sync.receive(openPacket)
        const collected: Collected = []

        sync.on('sync', (itemId: string, value: any) => {
          cnt++
          collected.push([itemId, value])
          if (cnt === 3) {
            // sort them first so the values can be tested
            collected.sort((a, b) => (a[0] < b[0] ? -1 : 1))

            collected[0][0].should.eql(1)
            collected[0][1].in1.should.eql(p1)
            collected[0][1].in2.should.eql(d1)

            collected[1][0].should.eql(2)
            collected[1][1].in1.should.eql(p2)
            collected[1][1].in2.should.eql(d2)

            collected[2][0].should.eql(3)
            collected[2][1].in1.should.eql(p3)
            collected[2][1].in2.should.eql(d3)
          }
        })

        // test all permutations
        const execs = {
          add_p1: () => sync.add('in1', p1),
          add_p2: () => sync.add('in1', p2),
          add_p3: () => sync.add('in1', p3),
          add_d1: () => sync.add('in2', d1),
          add_d2: () => sync.add('in2', d2),
          add_d3: () => sync.add('in2', d3),
          close: () => sync.receive(closePacket),
        }

        runSequence.forEach((func: string) => execs[func]())

        if (runs === perms.length) {
          // all permutations have been executed, done!
          done()
        }
      }

      const perms = jsc.permutation([
        'add_p1',
        'add_p2',
        'add_p3',
        'add_d1',
        'add_d2',
        'add_d3',
        'close',
      ])

      perms.forEach((permutation: RunSequence) => run(permutation))
    })
  })

  describe('Ordered should emit', () => {
    it('`sync` events with ordered data', (done) => {
      const sync = Sync.create(['in1', 'in2'], true)
      sync.receive(openPacket)
      let cnt = 0

      const collected: Collected = []

      sync.on('sync', (itemId: string, value: any) => {
        cnt++
        collected.push([itemId, value])

        if (cnt === 3) {
          collected.should.eql(expected)
          done()
        }
      })

      receivePackets(sync)
    })

    it('`group` event with ordered data', (done) => {
      const sync = Sync.create(['in1', 'in2'], true)
      sync.receive(openPacket)

      sync.on('group', (group: Group) => {
        group.should.eql(expected)
        done()
      })

      receivePackets(sync)
    })

    it('Should be able to do so in any execution order', function (done) {
      // Tests 5040 combinations
      this.timeout(10000) // Note: not available if using -^ arrow function

      let runs = 0
      function run(runSequence: RunSequence) {
        runs++

        reset()

        const sync = Sync.create(['in1', 'in2'], true)

        // receive open must always be first
        sync.receive(openPacket)

        sync.on('group', (group) => group.should.eql(expected))

        // test all permutations
        const execs = {
          add_p1: () => sync.add('in1', p1),
          add_p2: () => sync.add('in1', p2),
          add_p3: () => sync.add('in1', p3),
          add_d1: () => sync.add('in2', d1),
          add_d2: () => sync.add('in2', d2),
          add_d3: () => sync.add('in2', d3),
          close: () => sync.receive(closePacket),
        }

        runSequence.forEach((func: string) => execs[func]())

        if (runs === perms.length) {
          // all permutations have been executed, done!
          done()
        }
      }

      const perms = jsc.permutation([
        'add_p1',
        'add_p2',
        'add_p3',
        'add_d1',
        'add_d2',
        'add_d3',
        'close',
      ])

      perms.forEach((permutation: RunSequence) => run(permutation))
    })
  })
})
