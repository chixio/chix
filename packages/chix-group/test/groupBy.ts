import 'should'

import {Packet} from '@chix/flow'
import * as jsc from 'js-combinatorics'
import {GroupBy, SendGroup} from '@chix/group'
import {RunSequence} from './types'

describe('groupBy:', () => {
  let p1: Packet
  let p2: Packet
  let p3: Packet
  let d1: Packet
  let d2: Packet
  let d3: Packet
  let sg: SendGroup
  let closePacket: Packet
  let openPacket: Packet
  let groupBy: GroupBy

  function reset() {
    p1 = Packet.create('A')
    p2 = Packet.create('B')
    p3 = Packet.create('C')
    sg = SendGroup.create()
    groupBy = GroupBy.create()

    // stamp the packets with our group information
    openPacket = sg.open()
    sg.write(p1)
    sg.write(p2)
    sg.write(p3)
    closePacket = sg.close()

    // create differentiator packets
    d1 = p1.clone(p1.owner)
    d1.write(d1.owner, '#our$Group1')
    d2 = p2.clone(p2.owner)
    d2.write(d2.owner, '#our$Group1')
    d3 = p3.clone(p3.owner)
    d3.write(d3.owner, 'diffr3nt$')
  }

  before(reset)

  it('Should be able to group by each differentiator', (done) => {
    // TODO: this works in any random order, should be included in test
    groupBy.receive(openPacket)
    let cnt = 0
    groupBy.on('group', (group, by) => {
      cnt++

      if (cnt === 1) {
        group.should.eql([p1, p2])
        by.should.eql('#our$Group1')
      } else {
        group.should.eql([p3])
        by.should.eql('diffr3nt$')
        done()
      }
    })

    groupBy.add(p1)
    groupBy.setBy(d1)
    groupBy.add(p2)
    groupBy.receive(closePacket)
    groupBy.setBy(d2)

    // first group ready..

    groupBy.add(p3)
    groupBy.setBy(d3)

    // second group ready..
  })

  it('Should be able to do so in any execution order', function (done) {
    // Tests 5040 combinations
    this.timeout(10000) // Note: not available if using -^ arrow function

    let runs = 0
    function run(runSequence: RunSequence) {
      runs++

      // console.log('Run %d/%d, permutation: %s', runs, perms.length, runSequence)

      // reset all used instances
      reset()

      let cnt = 0
      const subgroups: {
        [by: string]: any
      } = {}

      groupBy.on('group', (group, by) => {
        cnt++
        subgroups[by] = group
        if (cnt === 2) {
          const group1 = subgroups['#our$Group1']
          const group2 = subgroups.diffr3nt$

          group1.length.should.eql(2)
          group1.should.containEql(p1)
          group1.should.containEql(p2)

          group2.should.eql([p3])

          if (runs === perms.length) {
            // all permutations have been executed, done!
            done()
          }
        }
      })

      // receive open must always be first
      groupBy.receive(openPacket)

      // test all permutations
      const obj: {
        [key: string]: () => void
      } = {
        add_p1: () => groupBy.add(p1),
        add_p2: () => groupBy.add(p2),
        add_p3: () => groupBy.add(p3),
        add_d1: () => groupBy.setBy(d1),
        add_d2: () => groupBy.setBy(d2),
        add_d3: () => groupBy.setBy(d3),
        close: () => groupBy.receive(closePacket),
      }

      runSequence.forEach((func: string) => obj[func]())
    }

    const perms = jsc.permutation([
      'add_p1',
      'add_p2',
      'add_p3',
      'add_d1',
      'add_d2',
      'add_d3',
      'close',
    ])

    perms.forEach((permutation: RunSequence) => run(permutation))
  })
})
