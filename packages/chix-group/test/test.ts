import 'should'

import {Packet} from '@chix/flow'
import {RecvGroup} from '../recv'
import {SendGroup} from '../send'

describe('Group:', () => {
  let p1: Packet
  let p2: Packet
  let p3: Packet
  let sg: SendGroup
  let rg: RecvGroup
  let closePacket: Packet
  let openPacket: Packet

  before(() => {
    p1 = Packet.create('A')
    p2 = Packet.create('B')
    p3 = Packet.create('C')
  })

  it('Should be able to create send group', () => {
    sg = SendGroup.create()
  })
  it('Should be able to create receive group', () => {
    rg = RecvGroup.create()
  })
  it('Should be able to create open packet', () => {
    openPacket = sg.open()
    openPacket.should.be.instanceOf(Packet)
    openPacket.meta('chix-group', 'gid').should.be.ok()
  })
  it('Should be able to write packet', () => {
    sg.write(p1)
    sg.write(p2)
    sg.write(p3)
    sg.items.length.should.eql(3)
  })
  it('Should be able to create close packet', () => {
    closePacket = sg.close()
    closePacket.should.be.instanceOf(Packet)
    closePacket.meta('chix-group', 'gid').should.be.ok()
    const items = closePacket.meta('chix-group', 'items')
    items.length.should.eql(3)
  })
  it('Should not be able to write to closed group', () => {
    ;(() => sg.write(p3)).should.throw(/failed to write/)
  })
  it('Receiver have no groups yet', () => {
    rg.hasGroups().should.eql(false)
  })
  it('Receiver should not yet contain the group', () => {
    rg.hasGroup(openPacket.meta('chix-group', 'gid')).should.eql(false)
  })
  it('Should be able to receive open packet', () => {
    rg.receive(openPacket)
  })
  it('Receiver have groups', () => {
    rg.hasGroups().should.eql(true)
  })
  it('Receiver should now contain group', () => {
    rg.hasGroup(openPacket.meta('chix-group', 'gid')).should.eql(true)
  })
  it('Should be able to add incoming packets', () => {
    rg.add(p1)
    rg.add(p2)
  })
  it('Should be able to receive close packet', () => {
    rg.receive(closePacket)
  })
  it('Should be able to add new incoming packet, and emit group', (done) => {
    rg.on('group', (group) => {
      group.should.eql([p1, p2, p3])
      done()
    })
    rg.add(p3)
  })
})
