/**
 * Creates SubGroups from incoming groups
 *
 *  This should work even when the packets are out of order.
 */
import {Packet} from '@chix/flow'
import {forOf} from '@fbpx/lib'
import Debug from 'debug'
import {RecvGroup} from './recv'
import {Group, Ports} from './types'

const debug = Debug('chix:group:sync')

export class Sync extends RecvGroup {
  public static create(ports?: Ports, ordered?: boolean) {
    return new Sync(ports, ordered)
  }

  public ports: Ports = []
  public ordered: boolean

  constructor(ports: Ports, ordered?: boolean) {
    super()

    if (ports) {
      this.ports = ports
    }

    this.ordered = !!ordered

    if (this.ports.length < 2) {
      throw Error('Sync requires at least 2 ports to synchronize')
    }
  }

  public add(port: string | Packet, packet?: Packet) {
    if (typeof port !== 'string' || !(packet instanceof Packet)) {
      throw Error('Incorrect method invocation.')
    }

    if (!this.hasGroups()) {
      throw Error('no groups defined yet')
    }
    forOf((gid: string, group: Group) => {
      if (packet.hasMeta('chix-group', gid)) {
        const itemId = packet.meta('chix-group', gid)

        if (!group.items.has(itemId)) {
          // group.items.set(itemId, {})
          group.items.set(itemId, new Packet())
        }

        group.items.get(itemId)[port] = packet

        debug(
          'update group `%s`, size: %s, item: %s, port: %s',
          gid,
          group.items.size,
          itemId,
          port
        )
        this.emitIfComplete(gid, group)
      }
    }, this.groups)
  }
  public emitIfComplete(gid: string, group: Group) {
    debug(
      `${group.complete} && ${group.items.size} === ${
        group.expected && group.expected.length
      }`
    )

    if (
      group.complete &&
      group.expected &&
      group.items.size === group.expected.length
    ) {
      if (this.ordered) {
        this._ordered(gid, group)
      } else {
        // unordered
        this._unordered(gid, group)
      }
    }
  }

  /**
   * Send out ordered items.
   *
   * Sends out an `sync` event for each item
   *
   * Sends out one `group` event with all items together
   *
   * @param _gid
   * @param {Object} group
   */
  private _orderedSend(_gid: string, group: Group) {
    const sendSync = this.listenerCount('sync')
    const sendGroup = this.listenerCount('group')
    const grouped = []

    if (!group.expected) {
      throw Error('Group expected not set.')
    }

    for (const itemId of group.expected) {
      const item = group.items.get(itemId)

      if (sendSync) {
        this.emit('sync', itemId, item)
      }

      if (sendGroup) {
        grouped.push([itemId, item])
      }
    }

    if (sendGroup) {
      this.emit('group', grouped)
    }
  }

  /**
   * Determines whether ready to send the items
   *
   * @param {String} gid
   * @param {Object} group
   */
  private _ordered(gid: string, group: Group) {
    let send = 0

    if (!group.expected) {
      throw Error('Group expected not set.')
    }

    for (const item of group.items.entries()) {
      // const itemId = item[0]
      const val = item[1]

      if (Object.keys(val).length === this.ports.length) {
        send++
      }
    }

    if (send === group.expected.length) {
      this._orderedSend(gid, group)

      delete this.groups[gid] // done with this group
    }
  }

  /**
   * Sends out `sync` events as soon as an item set ready.
   *
   * This explicitly does not take item order into account.
   *
   * @param {String} gid
   * @param {Object} group
   */
  private _unordered(gid: string, group: Group) {
    let send = 0

    if (!group.expected) {
      throw Error('Group expected not set.')
    }

    for (const item of group.items.entries()) {
      const itemId = item[0]
      const val = item[1]

      if (val === null) {
        send++ // already send
      } else if (Object.keys(val).length === this.ports.length) {
        this.emit('sync', itemId, val)
        send++
        group.items.set(itemId, null)
      }
    }

    if (send === group.expected.length) {
      delete this.groups[gid] // done with this group
    }
  }
}
