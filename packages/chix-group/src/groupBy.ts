/**
 * Creates SubGroups from incoming groups
 *
 *  This should work even when the packets are out of order.
 */
import {Packet} from '@chix/flow'
import {forOf} from '@fbpx/lib'
import Debug from 'debug'
import {RecvGroup} from './recv'
import {Group, GroupByGroup} from './types'

const debug = Debug('chix:group:groupBy')

export class GroupBy extends RecvGroup {
  public static create() {
    return new GroupBy()
  }

  /**
   * Receives a differentiator for the itemId specified
   * within the metadata of the received packet.
   *
   * This meta data contains an itemId for a specific input group.
   *
   * This itemId will have to correspond with the received packets
   * On the other port
   *
   * When the grouping is done, the collected subgroups are emitted along with their differentiator
   *
   * This data can be used to send out the groups again.
   *
   * The groups will be emitted only after all subgroups are complete.
   *
   * @param {Packet} packet
   */
  public setBy(packet: Packet) {
    if (!this.hasGroups()) {
      throw Error('no groups defined yet')
    }

    forOf((gid: string, group: Group) => {
      if (packet.hasMeta('chix-group', gid)) {
        const itemId = packet.meta('chix-group', gid)

        const by = JSON.stringify(packet.read(packet.owner))

        if (!group.by) {
          group.by = {
            groups: {},
            total: 0, // to determine whether all are grouped
          }
        }

        if (!group.by.groups[by]) {
          group.by.groups[by] = []
        }

        group.by.groups[by].push(itemId)
        group.by.total++

        debug('added differentiator for item `%s` to group `%s`', itemId, gid)

        this.emitIfComplete(gid, group)
      }
    }, this.groups)
  }

  /**
   * Does the same as recv.emitIfComplete
   *
   * However it also tests if our `by` information is build
   *
   * @param {string} gid
   * @param {Group} group
   */
  public emitIfComplete(gid: string, group: Group) {
    debug(
      `${group.complete} && ${group.items.size} === ${
        group.expected && group.expected.length
      }`
    )
    if (
      group.complete &&
      group.expected &&
      group.by &&
      group.expected.length === group.items.size &&
      group.expected.length === group.by.total
    ) {
      forOf((by: string, items: GroupByGroup) => {
        const subgroup = items.map((itemId: string) => group.items.get(itemId))

        debug(
          'group `%s` complete, emitting %d items',
          by.substring(0, 10),
          group.items.size
        )

        this.emit('group', subgroup, JSON.parse(by))
      }, group.by.groups)

      delete this.groups[gid]
    }
  }
}
