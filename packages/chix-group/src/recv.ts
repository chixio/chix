import {Packet} from '@chix/flow'
import {forOf} from '@fbpx/lib'
import Debug from 'debug'
import {EventEmitter} from 'events'
import {Group, Groups} from './types'

const debug = Debug('chix:group:recv')

export class RecvGroup extends EventEmitter {
  public static create() {
    return new RecvGroup()
  }

  public groups: Groups = {}

  public hasGroup(gid: string) {
    return this.groups.hasOwnProperty(gid)
  }

  public hasGroups() {
    return !!Object.keys(this.groups).length
  }

  public add(packet: Packet) {
    if (!this.hasGroups()) {
      throw Error('no groups defined yet')
    }

    forOf((gid: string, group: Group) => {
      if (packet.hasMeta('chix-group', gid)) {
        const itemId = packet.meta('chix-group', gid)

        group.items.set(itemId, packet)

        debug('added item to group %s, size: %d', gid, group.items.size)

        this.emitIfComplete(gid, group)
      } else {
        debug('Warning: item does not belong to group %s', gid)
      }
    }, this.groups)
  }

  public receive(packet: Packet) {
    const gid = packet.meta('chix-group', 'gid')

    if (gid) {
      if (!this.hasGroup(gid)) {
        this.groups[gid] = {
          complete: false,
          items: new Map(),
        }

        debug('created group %s', gid)
      }
      if (packet.hasMeta('chix-group', 'items')) {
        const items = packet.meta('chix-group', 'items')

        this.groups[gid].expected = items
        this.groups[gid].complete = true

        debug('group %s complete, expecting %d items', gid, items.length)

        this.emitIfComplete(gid, this.groups[gid])
      }
    } else {
      throw Error('Non group packet received')
    }
  }

  public emitIfComplete(gid: string, group: Group) {
    debug(
      `${group.complete} && ${group.items.size} === ${
        group.expected && group.expected.length
      }`
    )

    if (
      group.complete &&
      group.expected &&
      group.expected.length === group.items.size
    ) {
      debug('group %s complete, emitting %d items', gid, group.items.size)

      this.emit('group', Array.from(group.items.values()))

      delete this.groups[gid]
    }
  }
}
