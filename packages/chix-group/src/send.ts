import {Packet} from '@chix/flow'
import Debug from 'debug'
import {v4} from 'uuid'

const debug = Debug('chix:group:send')

/**
 * Group Sender
 *
 * Group data is exchanged through meta information within the packet.
 */
export class SendGroup {
  public static create(): SendGroup {
    return new SendGroup()
  }

  public gid: string = v4()
  public items: number[] = []
  public closed: boolean = false

  public write(packet: Packet): Packet {
    if (this.closed) {
      throw Error('SendGroup already closed, failed to write')
    }
    const itemNo = this.items.length + 1

    this.items.push(itemNo)

    packet.meta('chix-group', this.gid, itemNo)

    debug('wrote group info to packet gid: ' + this.gid + ', itemNo: ' + itemNo)

    return packet
  }

  /**
   * Creates an open packet.
   */
  public open(): Packet {
    const packet = Packet.create({gid: this.gid}).meta(
      'chix-group',
      'gid',
      this.gid
    )

    debug('created open packet for gid: ' + this.gid)

    return packet
  }

  /**
   * Creates a close packet.
   */
  public close(): Packet {
    const packet = Packet.create({gid: this.gid, items: this.items})
      .meta('chix-group', 'gid', this.gid)
      .meta('chix-group', 'items', this.items)

    this.closed = true

    debug(
      'created close packet for gid: ' +
        this.gid +
        ', items: ' +
        this.items.length
    )

    return packet
  }
}
