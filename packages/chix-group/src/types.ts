import {Packet} from '@chix/flow'

export type GroupItems = any[]

export type Groups = {
  [gid: string]: Group
}

export type Ports = string[]

export type GroupByGroup = string[]

export type GroupByGroups = {
  [by: string]: GroupByGroup
}

export interface Group {
  expected?: GroupItems
  complete: boolean
  items: Map<string, Packet>
  by?: {
    groups: GroupByGroups
    total: number
  }
}
