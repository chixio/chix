[![Build Status](https://travis-ci.org/psichi/chix-group.png)](https://travis-ci.org/psichi/chix-group)

# Chiχ Group

### Example

```js
// Sending component
const Group = require('chix-group/send')
const g = Group.create()
output({out: g.open()})
output({out: g.write(packet)})
output({out: g.write(packet)})
output({out: g.write(packet)})
output({out: g.write(packet)})
output({xout: g.close()})

// Receiving component
const Group = require('chix-group/recv')
const g = Group.create()
g.on('group', (groupData) => {
  output({out: $.create(groupData)})
})

g.receive(p) <-- group start packet
g.add(p) <-- item packet
g.add(p) <-- item packet
g.receive(p) <-- group end packet

if (g.isComplete()) {
}
```

### groupBy

Creates SubGroups from incoming groups
This works even when the packets are out of order.

Receives a differentiator for the itemId specified
within the metadata of the received packet.

This meta data contains an itemId for a specific input group.

This itemId will have to correspond with the received packets on the other port.

When the grouping is done, the collected subgroups are emitted along with their differentiator

This data can be used to send out the groups again.

The groups will be emitted only after all subgroups are complete.

```javascript
const groupBy = require('chix-group/groupBy')

// .. sending node

// receive open packet
groupBy.receive(p)

// receive packet with differentiator
groupBy.setBy(p)

// receive packet incoming packet
groupBy.add(p)

// Listen for group event
// group is an array of data, by is the differentiator
groupBy.on('group', (group, by) => {
  console.log(group, by)
})
```

### Sync

Sync is used to synchronize at least 2 different inputs
where both input paths originate from the same group, yet went different paths.

Sync can be used to synchronise the paths again and send out the items in order.

```javascript
const Sync = require('chix-group/sync')

const sync = Sync(['in1', 'in2'], /* ordered (true|false) */)

// .. sending node

// receive open packet
sync.receive(p)

// receive packet from port in1
sync.add('in1', p)

// receive packet from port in2
sync.add('in2', p)

// some more packets
sync.add('in2', p)
sync.add('in1', p)
sync.add('in2', p)
sync.add('in1', p)
sync.receive(p) // receive close packet
sync.add('in1', p)
sync.add('in2', p)


// If ordered is true (the default) a group event is emitted
// which contains all items in order
sync.on('group', (group) => {
  console.log(group)
})

// both for unordered and ordered a sync event is emitted
// if ordered is true (the default) the events are emitted in order.
// else the events are emitted as soon as a combination of packets is ready,
// the item order of the original group in this case will not be garanteed.
sync.on('sync', (itemId, val) => {
  console.log(itemId, val)
})

```


