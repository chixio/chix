var gulp = require('gulp');
var push = require('./src/push');

gulp.task('default', function() {
  return gulp.src('./test/*.fbp')
    .pipe(push('fbps.json'))
    .pipe(gulp.dest('./browser'));
});
