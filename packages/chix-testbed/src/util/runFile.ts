import {Actor} from '@chix/flow'
import {Installer} from '@chix/install'
import {FSLoader} from '@chix/loader-fs'
import {Actor as LogMonitor} from '@chix/monitor-npmlog'
import {readFile} from 'fs-extra'
import * as path from 'path'
import {RunContext} from '../runTests'
import {createParser} from './createParser'

const prefix = 'runFile'

export function runFile(context: RunContext) {
  return async (file: string) => {
    context.currentFile = file

    const {config} = context

    context.logger.info(prefix, `Creating FSLoader.`)

    let loader

    try {
      loader = new FSLoader({
        purge: false,
        cache: true,
        defaultProvider: 'https://api.chix.io/v1/nodes/{provider}/{ns}/{name}',
      })

      loader.setAuthorizationHeader(`Bearer ${config.get('access_token')}`)
    } catch (error) {
      console.log(error)
      context.logger.error(file, `Failed to create FSLoader`)

      return
    }

    let contents
    try {
      context.logger.info(prefix, `Loading: ${file}`)

      contents = await readFile(file, 'utf-8')
    } catch (error) {
      console.log(error)
      context.logger.error(file, `Failed to read file.`)

      return
    }

    let flow
    let parser
    let iips

    try {
      context.logger.info(prefix, `Parsing: ${file}`)
      parser = createParser(
        {
          defaultProvider:
            'https://api.chix.io/v1/nodes/{provider}/{ns}/{name}',
        },
        path.resolve(__dirname, '../../test')
      )

      flow = parser.parse(contents)

      iips = parser.getIIPs()
    } catch (error) {
      console.log(error)
      context.logger.error(file, `Failed to parse fbp.`)

      return
    }

    context.currentFlow = flow

    try {
      context.logger.info(prefix, `Processing: ${flow.title}`)

      await loader.load(flow)
    } catch (error) {
      console.log(error)
      context.logger.error(file, `Failed to load flow ${flow.title}.`)
      context.logger.error(file, flow)

      return
    }

    let installer

    try {
      installer = new Installer({
        prefix: process.cwd(),
      })

      await installer.load(loader.getDependencies())
    } catch (error) {
      console.log(error)
      context.logger.error(
        file,
        `Failed to install dependencies for ${flow.title}.`
      )

      return
    }

    let actor

    try {
      actor = new Actor()

      actor.processManager.on('error', (error: Error) => {
        console.error(error)

        process.exit()
      })

      actor.setLoader(loader)
      await actor.addMap(flow)

      LogMonitor(context.logger, actor)
    } catch (error) {
      console.log(error)
      context.logger.error(file, `Failed to create actor for ${flow.title}.`)

      return
    }

    context.logger.info(prefix, `TEST ${file}`)

    try {
      actor.sendIIPs(iips)
    } catch (error) {
      console.log(error)
      context.logger.error(file, `Failed to send IIPs to ${flow.title}.`)

      return
    }

    try {
      actor.push()
    } catch (error) {
      console.log(error)
      context.logger.error(file, `Failed to start ${flow.title}.`)
    }
  }
}
