import {Flow as FlowDefinition} from '@chix/common'
import {AssertionError} from 'assert'
import * as colors from 'colors'
import {RunContext} from '../runTests'

/*
Err { name: 'AssertionError',
actual: [ 1, 2, 3, 4, 5 ],
expected: 5,
operator: '==',
message: 'Has a length of 5' }
*/
export function createAssertionErrorHandler(context: RunContext) {
  return function assertionErrorHandler(error: Error | AssertionError) {
    const currentFile = context.currentFile
    const currentFlow = context.currentFlow as FlowDefinition
    const flowTitle = colors.bold(
      currentFlow.title || `${currentFlow.ns}:${currentFlow.name}`
    )

    if (error.name === 'AssertionError') {
      const msg = [
        flowTitle,
        '-',
        error.message,
        '-',
        colors.red('FAILED'),
        '\n  file:',
        currentFile,
        '\n  Expected:',
        JSON.stringify((error as AssertionError).expected),
        '\n  Actual:',
        colors.red(JSON.stringify((error as AssertionError).actual)),
      ].join(' ')

      console.log(msg)
    } else {
      if (currentFlow) {
        console.log(`${flowTitle} ${currentFile}`)
      }

      throw error
    }

    process.exit(1)
  }
}
