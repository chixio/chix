import {
  ChixRenderer,
  ChixRenderOptions,
  FBPParser,
  FSResolver,
} from '@chix/fbpx'

export function createParser(
  options?: ChixRenderOptions,
  includePath?: string
): FBPParser {
  const parser = new FBPParser()

  parser.addRenderer(new ChixRenderer(options))
  parser.addIncludeResolver(new FSResolver(includePath))

  return parser
}
