import {Flow as FlowDefinition} from '@chix/common'
import {Config} from '@chix/config'
import * as colors from 'colors'
import * as glob from 'glob'
import * as Logger from 'npmlog'
import {createAssertionErrorHandler, runFile} from './util'

colors.setTheme({
  silly: 'rainbow',
  input: 'grey',
  verbose: 'cyan',
  prompt: 'grey',
  info: 'green',
  data: 'grey',
  help: 'cyan',
  warn: 'yellow',
  debug: 'blue',
  error: 'red',
})
;(Logger as any).addLevel(
  'debug',
  30,
  {
    fg: 'grey',
    bg: 'black',
  },
  'DEBUG'
)

export type RunContext = {
  currentFlow: FlowDefinition | null
  currentFile: string | null
  config: Config<any>
  logger: any // Logger
}

const prefix = 'runTests'

export async function runTests() {
  ;(Logger as any).level = 'debug'
  // Logger.level = 'error';

  const files = glob.sync('test/*.fbp')

  const context: RunContext = {
    logger: Logger,
    config: new Config<any>(),
    currentFile: null,
    currentFlow: null,
  }

  process.on('uncaughtException', createAssertionErrorHandler(context))

  try {
    await files.forEach(runFile(context))
  } catch (error) {
    context.logger.error(prefix, `ERROR: ${error}`)
  }
}
