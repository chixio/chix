import {File, GulpPlugin} from 'gulp-util'
import {join} from 'path'
import through from 'through'

module.exports = function (this: GulpPlugin, file) {
  const buffer = []

  const fileName = file

  const bufferContents = ({contents}) => buffer.push(contents.toString('utf8'))

  const endStream = () => {
    this.push(
      new File({
        cwd: process.cwd(),
        path: join(process.cwd(), fileName),
        contents: new Buffer(JSON.stringify(buffer, null, 2), 'utf8'),
      })
    )

    this.emit('end')
  }
  return through(bufferContents, endStream)
}
