chix-testbed
============

[![Build Status](https://travis-ci.org/psichi/chix-testbed.png?branch=master)](https://travis-ci.org/psichi/chix-testbed)

Testground for flows that won't flow.

Every .fbp test file follows the same pattern.

run `./chit` to test the .fbp files inside the test/ directory
