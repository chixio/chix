"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.forOf = void 0;
function forOf(fn, source) {
    if (typeof fn !== 'function') {
        throw new TypeError('First argument must be a function');
    }
    if (typeof source !== 'object') {
        throw new TypeError('Second argument must be an object');
    }
    const argLength = fn.length - 1;
    function iterate(obj, fnArgs, res) {
        for (const key in obj) {
            if (obj.hasOwnProperty(key)) {
                const myArgs = fnArgs.slice();
                myArgs.push(key);
                if (myArgs.length < argLength) {
                    iterate(obj[key], myArgs, res);
                }
                else {
                    myArgs.push(obj[key]);
                    const ret = fn.apply(this, myArgs);
                    if (ret !== undefined) {
                        res.push(ret);
                    }
                }
            }
        }
        return res;
    }
    return iterate(source, [], []);
}
exports.forOf = forOf;
//# sourceMappingURL=forOf.js.map