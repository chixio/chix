import {Flow as FlowDefinition} from '@chix/common'
import {ILoader, LoaderResponse} from '@chix/loader'
import {RemoteLoader, RemoteLoaderOptions} from '@chix/loader-remote'
import * as fs from 'fs'
import * as util from 'util'
import {
  LoadCacheEvent,
  LoadFileEvent,
  LoadNodeResult,
  NodeDefinitionLocationFS,
  PurgeCacheEvent,
  WriteCacheEvent,
} from './types'
import {loadFile} from './util'

const readFile = util.promisify(fs.readFile)

let syncer: () => void

function nop() {}

export type FSLoaderOptions = {
  cache?: boolean
  purge?: boolean
}

/**
 *  Loads and saves definitions from/to the local cache file.
 */
export class FSLoader extends RemoteLoader implements ILoader {
  public name = 'FSLoader'
  public opts: FSLoaderOptions = {}

  public _cacheFile = './.x.cache'

  constructor({
    cache,
    purge,
    defaultProvider,
  }: FSLoaderOptions & RemoteLoaderOptions) {
    super({defaultProvider})

    this.opts.cache = Boolean(cache)
    this.opts.purge = Boolean(purge)

    // purge the cache file
    if (this.opts.purge) {
      try {
        fs.unlinkSync(this._cacheFile)

        this.emit('purgeCache', {
          file: this._cacheFile,
        } as PurgeCacheEvent)
      } catch (error) {
        nop()
      }
    }

    if (this.opts.cache && !this.opts.purge) {
      if (syncer) {
        process.removeListener('exit', syncer)
      } else {
        // re-used
        process.on('SIGINT', () => {
          process.exit()
        })
      }

      syncer = this.syncState.bind(this)
      process.on('exit', syncer)
    }
  }

  /**
   *
   *  Loads all definitions from the database.
   *  Any missing pieces will be loaded by remote Loader.
   *
   *  If you use `update` all definitions in the database will be refreshed.
   *
   * @param {Object} flow
   * @param {Boolean} update
   */
  public async load(
    flow: FlowDefinition[] | FlowDefinition,
    update: boolean = false
  ): Promise<LoaderResponse> {
    if (this.opts.cache) {
      try {
        const content = await readFile(this._cacheFile, 'utf8')

        const cache = JSON.parse(content)

        this.setDependencies(cache.dependencies)
        this.setNodeDefinitions(cache.nodeDefinitions)

        this.emit('loadCache', {
          file: this._cacheFile,
          cache,
        } as LoadCacheEvent)

        // Start the remote loader, It will not load any known definitions.
        return super.load(flow, update)
      } catch (error) {
        if ((error as any).code !== 'ENOENT') {
          throw Error((error as any).toString())
        }
      }
    }

    return super.load(flow, update)
  }

  /**
   *
   * Loads a local file
   *
   * @param {NodeDefinitionLocationFS} location
   */
  public async loadFile(
    location: NodeDefinitionLocationFS
  ): Promise<LoadNodeResult> {
    this.emit('loadFile', {path: location.path} as LoadFileEvent)

    return loadFile(location)
  }

  /**
   * Saves the internal state to disk
   */
  public syncState() {
    // save internal state to disk
    fs.writeFileSync(
      this._cacheFile,
      JSON.stringify(
        {
          dependencies: this.getDependencies(),
          nodeDefinitions: this.getNodeDefinitions(),
        },
        null,
        2
      )
    )

    this.emit('writeCache', {
      file: this._cacheFile,
    } as WriteCacheEvent)
  }
}
