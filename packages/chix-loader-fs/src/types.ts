import {
  Flow as FlowDefinition,
  NodeDefinition,
  NodeDefinitions,
  NodeDependencies,
} from '@chix/common'

// Note: Structure is the same but let's keep the distinction
export type Dependencies = NodeDependencies

export type DependencyTypes = 'npm'

export interface BaseNodeDefinitionLocation {
  providerLocation: string
}

export type ProviderDef = {
  ns: string
  name: string
  providerLocation: string
  url: string
}

export type LoadDefinitionResult = {
  nodeDefinition: NodeDefinition
  dependencies: Dependencies
}

export type Location = string

export type WillLoad = Location[]

export interface WorkloadBase {
  ns: string
  name: string
  providerLocation: string
}

export interface FSWorkload extends WorkloadBase {
  path: string
}

export interface RemoteWorkload extends WorkloadBase {
  url: string
}

export type Workload = (FSWorkload | RemoteWorkload)[]

export interface XCacheFile {
  dependencies: NodeDependencies
  nodeDefinitions: NodeDefinitions
}

export interface WriteCacheEvent {
  file: string
}

export interface PurgeCacheEvent {
  file: string
}

export interface LoadCacheEvent {
  file: string
  cache: XCacheFile
}

export interface NodeDefinitionLocationFS extends BaseNodeDefinitionLocation {
  path: string
}

export interface LoadFileEvent {
  path: string
}

export interface LoadUrlEvent {
  url: string
}

export type LoadNodeResult = {
  providerLocation: string
  nodeDef: NodeDefinition | FlowDefinition
}

export type PreloadResult = {
  provider: string
  nodeDefinitions: NodeDefinitions
}

export type PreloadConfig = {
  provider: string
  collection: string
}

export interface NodeDefinitionLocationRemote
  extends BaseNodeDefinitionLocation {
  url: string
}
