import {Flow as FlowDefinition, NodeDefinition} from '@chix/common'
import {ChixRenderer, FBPParser} from '@chix/fbpx'
import * as fs from 'fs'
import * as yaml from 'js-yaml'
import * as util from 'util'
import {LoadNodeResult, NodeDefinitionLocationFS} from '../types'

const readFile = util.promisify(fs.readFile)

export function createParser() {
  const parser = new FBPParser()
  const renderer = new ChixRenderer()

  renderer.skipIDs = true
  parser.addRenderer(renderer)

  return parser
}

/**
 *
 * Loads an .fbp or .json file from disk and returns the nodeDefinition.
 *
 * e.g.
 *   provider ./{ns}/{name} as x
 *   MyGraph(x:common/graph)
 */
export async function loadFile(
  location: NodeDefinitionLocationFS
): Promise<LoadNodeResult> {
  const {providerLocation, path} = location
  const parser = createParser()
  const extension = path.split('.').pop()

  const content = await readFile(path, 'utf8')

  if (/^\s*{/.test(content)) {
    // test for json
    const nodeDef = JSON.parse(content)

    return {
      providerLocation,
      nodeDef,
    }
  } else if (path.indexOf('.fbp') !== -1) {
    const nodeDef = parser.parse(content)

    return {
      providerLocation,
      nodeDef,
    }
  } else if (extension === 'yml' || extension === 'yaml') {
    const nodeDef = yaml.load(content) as NodeDefinition | FlowDefinition

    return {
      providerLocation,
      nodeDef,
    }
  }

  throw Error(`Could not recognize file format for: ${path}`)
}
