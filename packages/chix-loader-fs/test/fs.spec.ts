import {Flow, NodeDependencies, Providers} from '@chix/common'
import {expect} from 'chai'
import ecstatic from 'ecstatic'
import * as http from 'http'
import * as path from 'path'
import {FSLoader} from '@chix/loader-fs'
import {loadGraph} from './util'

const port = 2300

describe('FSLoader:', () => {
  let server: http.Server
  let loader: FSLoader

  before(() => {
    loader = new FSLoader({defaultProvider: ''})

    // load fixture.
    const dir = path.join(__dirname, '/fixtures/providers')

    // serve providers directory
    server = http.createServer(ecstatic({root: dir})).listen(port)

    console.info('Test server started on port', port)
  })

  after(() => {
    // stop server
    server.close(() => {
      console.info('Testserver stopped')
    })
  })

  it('Should have a default provider', () => {
    loader = new FSLoader({defaultProvider: ''})

    expect(loader.defaultProvider).to.be.a('string')
  })

  it('The graph fixture itself should be correct', () => {
    // Y @ X
    const graph = loadGraph('graph') as Flow

    const providers = graph.providers as Providers

    // setup check, whether the graph itself is ok
    expect(providers).to.be.an('object')

    expect(providers.Y).to.haveOwnProperty('name')
    expect(providers.Y).to.haveOwnProperty('url')

    expect(providers.Q).to.haveOwnProperty('name')
    expect(providers.Q).to.haveOwnProperty('url')

    expect(providers['@']).to.haveOwnProperty('url')

    expect(graph.nodes).to.be.an('array')

    expect(graph.nodes[0].provider).to.eql('Y')
    expect(graph.nodes[1]).to.not.have.property('provider')
    expect(graph.nodes[2].provider).to.eql('Q')
  })

  // Resolving of the map must always be done.
  // but then it detects the url is already loaded
  // so all it needs to do is resolve the provider for the node.
  // and set node.provider to the url instead of the namespace
  it('Should be able to load providers', async () => {
    let result
    const graph = loadGraph('graph') as Flow

    result = await loader.load(graph)

    expect(result.nodeDefinitions).to.have.property(
      `http://localhost:${port}/Y/{ns}/{name}.json`
    )
    expect(
      result.nodeDefinitions[`http://localhost:${port}/Y/{ns}/{name}.json`]
    ).to.have.property('math')

    expect(result.nodeDefinitions).to.have.property(
      `http://localhost:${port}/Q/{ns}/{name}.json`
    )
    expect(
      result.nodeDefinitions[`http://localhost:${port}/Q/{ns}/{name}.json`]
    ).to.have.property('console')

    expect(result.nodeDefinitions).to.have.property(
      `http://localhost:${port}/@/{ns}/{name}.json`
    )
    expect(
      result.nodeDefinitions[`http://localhost:${port}/@/{ns}/{name}.json`]
    ).to.have.property('math')

    const dependencies = result.dependencies as NodeDependencies

    expect(dependencies).to.eql({
      npm: {
        bogus: '0.x.x',
      },
    })

    result = await loader.loadNodeDefinitionFrom(
      `http://localhost:${port}/Q/{ns}/{name}.json`,
      'console',
      'log'
    )

    const expected = {
      npm: {
        bogus: '0.x.x',
      },
    }

    expect(result.dependencies).to.eql(expected)

    expect(loader.getDependencies('npm')).to.eql(expected.npm)

    result = await loader.loadNodeDefinitionFrom(
      `http://localhost:${port}/Q/{ns}/{name}.json`,
      'console',
      'graph'
    )

    expect(result.nodeDefinition.ns).to.eql('console')
    expect(result.nodeDefinition.name).to.eql('graph')

    expect(result.dependencies).to.eql({
      npm: {
        bogus: '0.x.x',
      },
    })
  })

  it('Should be able to preload providers', async () => {
    const fsLoader = new FSLoader({defaultProvider: ''})

    const result = await fsLoader.preload({
      provider: `http://localhost:${port}/Y/{ns}/{name}.json`,
      collection: `http://localhost:${port}/Y/nodes.json`,
    })

    expect(result.provider).to.eql(
      `http://localhost:${port}/Y/{ns}/{name}.json`
    )

    expect(result.nodeDefinitions).to.equal(
      fsLoader.getNodeDefinitions(`http://localhost:${port}/Y/{ns}/{name}.json`)
    )
  })
})
