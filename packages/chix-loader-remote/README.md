Chiχ Remote Loader
=========

This is the Remote component loader for Chiχ.

Chiχ uses node definitions to describe each of it's components.

The loader takes care of loading these definitions and caching them.

### Base Functionality

It's possible to manually add node definitions to the remote loader.

Definitions set in this manner will prevent the remote loader from having to do unnecessary lookups.

Example usage:
```javascript

import { RemoteLoader } from '@chix/loader-remote'

const loader = new RemoteLoader()

loader.addNodeDefinitions('@', [{
  ns: 'my', name: 'component',
  ports: {
    input: {
      'in': {
          type: 'string'
      }
    },
    output: {
      out: {
          type: 'string'
        }
      }
    }
  }, {
    ns: 'my', name: 'component2',
    ports: { input: { in: { type: 'string' } } }
}]);

// retrieve a node definitions from a provider
loader.getNodeDefinitionFrom('@', 'my', 'component');

// Checks whether the definition is available
loader.hasNodeDefinition('@', 'my', 'component'); // true

```

## On demand node retrievable

```typescript
// Given a node and the flow it is contained in, will try and load the (remote) node.
const nodeDefinition = await loader.getNodeDefinition({
 ns: 'my',
 name: 'component'
}, flow);
```

## Preloading

Sometimes it can be convenient to preload all nodes at a given url.

Preload retrieves all nodes available at a certain provider.

# collecting dependencies

It's also the loaders job to collect the dependencies needed to run a graph.
This is rather simple if a graph only contains non-composite nodes, but quickly
becomes more complex when subgraphs are used and the number of subgraphs are unknown.
The loader keeps track of which requires are needed.
