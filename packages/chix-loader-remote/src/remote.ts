import {
  Flow as FlowDefinition,
  NodeDefinition,
  NodeDefinitionProviderMap,
  NodeDefinitions,
} from '@chix/common'
import {
  Loader,
  LoaderResponse,
  LoadNodeResult,
  LoadUrlEvent,
  PreloadConfig,
  PreloadResult,
} from '@chix/loader'
import Debug from 'debug'
import {NodeDefinitionLocationFS, NodeDefinitionLocationRemote} from './types'

export interface RemoteLoaderOptions {
  defaultProvider: string
}

const debug = Debug('chix-loader:remote')

export class RemoteLoader extends Loader {
  public name = 'RemoteLoader'
  public defaultProvider: string

  public workload = []

  public headers = {
    Authorization: '',
  }

  constructor({defaultProvider}: RemoteLoaderOptions) {
    super()
    this.defaultProvider = defaultProvider
  }

  public setAuthorizationHeader(value: string): void {
    this.headers.Authorization = value
  }

  /**
   *
   * Just load the whole map.
   * All remote needs is the providers sections and the nodes.
   *
   * Use `update` whenever you expect subflows or nodeDefinitions
   * have changed changed.
   */
  public async load(
    flows: FlowDefinition[] | FlowDefinition,
    update: boolean = false
    // dependencies : disabled
  ): Promise<LoaderResponse> {
    if (!Array.isArray(flows)) {
      return this._loadRemote([flows], update /*, dependencies*/)
    } else {
      return this._loadRemote(flows, update /*, dependencies*/)
    }
  }

  /**
   * Preloads all definitions at a providers location.
   *
   * From a remote api this is relatively easy,
   * It also understands the filesystem in which case we just use globbing
   *
   * For a remote api we can assume the structure is just correct.
   *
   * But for the file system some extra checks are made to ensure what we
   * are loading are actually component definitions.
   *
   * This method is used to prefill our loader, which means it will
   * hold more definitions then our registered nodes need.
   *
   * In that matter, a clean unused definitions would be handy.
   *
   * Provider is in the form of:
   *
   *   - http://serve.chix.io/nodes/{ns}/{name}
   *   - ./nodes/{ns}/{name}.json
   *   - /var/lib/chix/nodes/{ns}/{name}.fbp
   *
   * An optional second parameter can give a full url at which *all* definitions can be loaded.
   *
   * e.g. http://serve.chix.io/nodes
   *
   * For loading from the filesystem, it's just interpolated.
   *
   * @param {Object} config
   * @param {String} config.provider   provider url
   * @param {String} config.collection endpoint to get full collection
   */
  public async preload(config: PreloadConfig): Promise<PreloadResult> {
    if (!config.provider) {
      throw Error('missing provider value')
    }

    if (!config.collection) {
      throw Error('missing collection value')
    }

    // load collection and add nodefinitions.
    debug('Preload collection %s', config.collection)

    const res = await this.makeRequest(config.collection)

    const nodeDef = await res.json()

    this.addNodeDefinitions(config.provider, nodeDef)

    return {
      provider: config.provider,
      nodeDefinitions: this.getNodeDefinitions(
        config.provider
      ) as NodeDefinitions,
    }
  }

  public async _loadRemote(
    flows: FlowDefinition[],
    update: boolean = false
    // dependencies : disabled
  ): Promise<LoaderResponse> {
    const workload = this.getWorkLoad(flows, this.defaultProvider, update)

    if (workload.length) {
      const loadedNodes: LoadNodeResult[] = await Promise.all(
        workload.map((location) => this.loadNode(location))
      )
      const flowDefinitions: FlowDefinition[] = []

      for (const loadedNode of loadedNodes) {
        const {nodeDef: nodeDefinition, providerLocation} = loadedNode

        // Give extenders a chance to save this result
        this.saveNodeDefinition(providerLocation, nodeDefinition)

        this.addNodeDefinition(providerLocation, nodeDefinition)

        if ((nodeDefinition as NodeDefinition).dependencies) {
          this.dependencyManager.parseDependencies(
            nodeDefinition as NodeDefinition
          )
        }

        if ((nodeDefinition as FlowDefinition).type === 'flow') {
          const flow = nodeDefinition as FlowDefinition

          if (!flow.ns) {
            throw Error(`No Namespace found for Flow ${flow.title}`)
          }

          if (!flow.name) {
            throw Error(`No Name found for Flow ${flow.title}`)
          }

          flowDefinitions.push(flow)
        }
      }

      return this._loadRemote(
        flowDefinitions,
        update
        // dependencies
      )
    }

    return {
      nodeDefinitions: this.getNodeDefinitions() as NodeDefinitionProviderMap,
      dependencies: this.getDependencies(),
    }
  }

  /**
   *
   * Remote loader does not store it's definitions
   *
   */
  public saveNodeDefinition(
    _providerLocation: string,
    _nodeDefinition: NodeDefinition | FlowDefinition
  ) {
    // nop
  }

  /**
   * Loads the node
   *
   * Either remote, if there is an url property.
   * Or else tries to load the file if there is a path property.
   *
   * const d = {
   *   url: || path:      - the location
   *   providerLocation:  - provider url
   * }
   *
   * url & path is just the provider url with it's variables filled in.
   */
  public async loadNode(
    location: NodeDefinitionLocationFS | NodeDefinitionLocationRemote
  ): Promise<LoadNodeResult> {
    if ((location as NodeDefinitionLocationRemote).hasOwnProperty('url')) {
      return this.loadUrl(location as NodeDefinitionLocationRemote)
    } else if ((location as NodeDefinitionLocationFS).hasOwnProperty('path')) {
      return this.loadFile(location as NodeDefinitionLocationFS)
    }

    throw Error('Do not know how to load node')
  }

  public async loadFile(_location: any): Promise<LoadNodeResult> {
    throw Error('Path loading not supported.')
  }

  /**
   *
   * Loads a nodeDefinition URL
   *
   * @param {NodeDefinitionLocationRemote} location
   */
  public async loadUrl(
    location: NodeDefinitionLocationRemote
  ): Promise<LoadNodeResult> {
    const {providerLocation, url} = location

    this.emit('loadUrl', {url} as LoadUrlEvent)

    try {
      const data = await (await this.makeRequest(url)).json()

      if (!data.hasOwnProperty('fn') && url.endsWith('node.json')) {
        const fnUrl = url.replace(/node.json$/, 'node.js')
        this.emit('loadUrl', {url: fnUrl} as LoadUrlEvent)
        data.fn = await (await this.makeRequest(fnUrl)).text()
      }

      return {
        providerLocation,
        nodeDef: data,
      }
    } catch (error) {
      throw Error(`Failed to load URL: ${url} [${error}]`)
    }
  }

  public async makeRequest(url: string): Promise<Response> {
    let response: Response
    if (this.headers.Authorization) {
      response = await fetch(url, {
        headers: {Authorization: this.headers.Authorization},
      })
    } else {
      response = await fetch(url)
    }

    if (!response.ok) {
      throw Error(response.statusText)
    }

    return response
  }
}
