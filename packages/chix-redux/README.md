# Chiχ Redux middleware

Middleware to run dispatch actions to a graph and listen for output.

Graph output will cause the redux store to be updated

# Test

`npm run test`

Make sure google-chrome is installed prior to testing.
