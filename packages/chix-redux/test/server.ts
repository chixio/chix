// initialize sample server environment for test
import * as http from 'http'

import {chixServerRuntime} from '@chix/runtime'
import {WebSocketTransport} from '@chix/transport'
import {ChixParser} from '@chix/fbpx'

const serveMe = (_req, res) => {
  res.send('Runtime')
}

const server = http.createServer(serveMe)

const runtime = await chixServerRuntime({
  transport: new WebSocketTransport({}, server),
})

const fbp = `
title: Request a file
description: Simple file request
ns: test
name: graph

Request(superagent/api), End(superagent/end), Parse(json/parse), Path(json/path)

'https://api.chix.io/v1/nodes/rhalff/http/request' -> url Request

Request request -> request End
# End body -> json Path
# End error -> msg Log
<- body End

# Select the description field
# '/description' -> path Path

# Log it to the console
# Path matches -> msg Log
`

await runtime.gh.loadFbp(fbp, ChixParser())

server.listen(4321, () => {
  console.log('Test Runtime Running')
})
