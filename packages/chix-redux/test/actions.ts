import configureStore from 'redux-mock-store'
import * as test from 'tape'
import {RuntimeService} from '../runtime'

test('Runtime messages are converted to actions', (t) => {
  const opts = {
    host: 'localhost',
    port: 4321,
    role: 'client',
    secret: '+TwkVuF6',
  }

  const rs = RuntimeService.create(opts as any)

  const mockStore = configureStore([rs.middleware.bind(rs)])

  const store = mockStore({})
  rs.start(store.dispatch)

  const expected = [
    {
      type: 'app:connected',
      payload: true,
    },
    {
      type: 'fbp:recv:runtime:runtime',
      payload: {
        type: '@chix/runtime',
        version: '0.2.2',
        capabilities: [
          'protocol:graph',
          'protocol:component',
          'protocol:network',
          'protocol:runtime',
          'component:setsource',
          'component:getsource',
        ],
        graph: 'test/graph',
      },
    },
  ]

  rs.transport.on('runtime', () => {
    t.deepEqual(store.getActions(), expected)
    rs.stop()
    t.end()
  })
})
