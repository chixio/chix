import * as test from 'tape'
// import runtimeReducer from '../src/reducers/runtime'
import {components as componentsReducer} from '@chix/redux'

test('component reducer', (t) => {
  test('should return initial state', (_t) => {
    _t.deepEqual(componentsReducer(undefined, {} as any), {})
    _t.end()
  })

  test('should handle component receive', (_t) => {
    _t.deepEqual(
      componentsReducer(undefined, {
        type: 'fbp:recv:component:component',
        payload: {
          name: 'my-comp',
          ns: 'test',
        },
      }),
      {'my-comp': {name: 'my-comp', ns: 'test'}}
    )
    _t.end()
  })

  t.end()
})
