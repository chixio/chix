import {schemas} from '@chix/fbp-protocol'
import {ReduxTransport, ReduxTransportOptions} from './reduxTransport'

const protocols = ['graph', 'network', 'runtime', 'component', 'process']

/*
// global localstorage
import uuid from 'uuid'
function clientID () {
  let id = localStorage.getItem('clientId')
  if (!id) {
    id = uuid.v4()
    localStorage.setItem('clientId', id)
  }
  return id
}
*/
export type RuntimeServiceOptions = ReduxTransportOptions

export class RuntimeService {
  public static create(options: RuntimeServiceOptions) {
    return new RuntimeService(options)
  }

  public opts: RuntimeServiceOptions
  public transport: ReduxTransport
  public dispatch: any
  public started: boolean = false

  constructor(opts: RuntimeServiceOptions) {
    this.opts = opts || ({} as RuntimeServiceOptions)
    this.initHandler = this.initHandler.bind(this)
    this.stop = this.stop.bind(this)
    this.protocolHandler = this.protocolHandler.bind(this)
  }

  // intercepts input messages related to the runtime and sends them to it.
  public middleware(_store) {
    return (next) => (action) => {
      if (/^fbp:send/.test(action.type)) {
        const [protocol, command] = action.type.split(':').splice(2, 2)

        if (schemas[protocol] && schemas[protocol].input[command]) {
          // TODO: INJECT SECRET HERE
          this.transport.$send({
            protocol,
            command,
            payload: action.payload,
          })
        } else {
          console.error(
            'Unsupported protocol command: %s:%s',
            protocol,
            command
          )
        }
      } else {
        if (/^fbp:recv/.test(action.type)) {
          const [protocol, command] = action.type.split(':').splice(2, 2)
          if (command === 'error') {
            // log all errors
            console.error(action.payload)
          } else if (protocol === 'runtime' && command === 'runtime') {
            if (!/noflo/.test(action.payload.type)) {
              // subscribe to the graph, not part of fbp network protocol
              // is used to be part of sendAll()
              this.transport.$send({
                protocol: 'graph',
                command: 'subscribe',
                payload: {graph: action.payload.graph},
              })
            }
            this.dispatch({
              type: 'app:connected',
              payload: true,
            })
          }
        }
      }

      return next(action)
    }
  }

  public protocolHandler(data /* , conn */) {
    this.dispatch({
      type: `fbp:recv:${data.protocol}:${data.command}`,
      payload: data.payload,
    })
  }

  public initHandler() {
    this.transport.$send({
      protocol: 'runtime',
      command: 'getruntime',
    })
  }

  public start(dispatch) {
    if (!this.started) {
      this.dispatch = dispatch
      this.opts.dispatch = dispatch
      this.transport = new ReduxTransport(this.opts as any)
      this.transport.setSchemas(schemas as any)
      this.transport.on('connected', this.initHandler)
      this.transport.on('disconnected', this.stop)
      protocols.forEach((protocol) =>
        this.transport.on(protocol, this.protocolHandler)
      )
      this.started = true
    }
  }

  public stop() {
    if (this.started) {
      this.dispatch({
        type: 'app:connected',
        payload: false,
      })
      // todo: all transport should have proper close.
      // transport = null
      this.started = false
    }
  }
}
