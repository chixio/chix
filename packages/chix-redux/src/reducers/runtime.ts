import Immutable from 'seamless-immutable'

export interface FbpRecvRuntimeRuntimeAction {
  type: 'fbp:recv:runtime:runtime'
  payload: any
}

// probably do not use immutable, keep it simple
const initialState = Immutable({})

export function runtime(
  state = initialState,
  action: FbpRecvRuntimeRuntimeAction
) {
  if (action.type === 'fbp:recv:runtime:runtime') {
    return Immutable(action.payload)
  }
  return state
}
