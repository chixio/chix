import Immutable from 'seamless-immutable'

export interface FbpRecvNetworkStoppedAction {
  type: 'fbp:recv:network:stopped'
  payload: any
}

export interface FbpRecvNetworkStartedAction {
  type: 'fbp:recv:network:started'
  payload: any
}

export type FbpRecvNetworkAction =
  | FbpRecvNetworkStoppedAction
  | FbpRecvNetworkStartedAction

const initialState = Immutable({})

export function network(state = initialState, action: FbpRecvNetworkAction) {
  if (/^fbp:recv:network/.test(action.type)) {
    const command = action.type.split(':').pop()
    const {payload} = action

    if (['stopped', 'started'].indexOf(command) >= 0) {
      return Immutable({
        time: payload.time,
        uptime: payload.uptime,
        graph: payload.graph,
        status: command,
      })
    }
  }
  return state
}
