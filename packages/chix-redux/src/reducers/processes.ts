import Immutable from 'seamless-immutable'

export interface FbpRecvProcessStatusAction {
  type: 'fbp:recv:process:status'
  payload: any
}

const initialState = Immutable({})
// also not part of protocol..
export function processes(state = initialState, action) {
  if (action.type === 'fbp:recv:process:status') {
    // return state.set(action.payload.process.pid, Immutable(action.payload.process))
    return state.set(
      action.payload.process.id,
      Immutable(action.payload.process)
    )
  }

  return state
}
