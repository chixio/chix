import Immutable from 'seamless-immutable'

const initialState = null

export interface GraphMonitorGraphAction {
  type: 'graph:monitor:graph'
  payload: any
}

export interface FbpRecvGraphAddNodeAction {
  type: 'fbp:recv:graph:addnode'
  payload: any
}

export interface FbpRecvGraphRemoveNodeAction {
  type: 'fbp:recv:graph:removenode'
  payload: any
}

export type GraphAction =
  | GraphMonitorGraphAction
  | FbpRecvGraphAddNodeAction
  | FbpRecvGraphRemoveNodeAction

export function graph(state = initialState, action: GraphAction) {
  if (action.type === 'graph:monitor:graph') {
    console.log('setting graph', action.payload)
    return Immutable(action.payload)
  }
  if (action.type === 'fbp:recv:graph:addnode') {
    const {payload} = action
    const [ns, name] = payload.component.split('/')
    const nodes = state.nodes.asMutable()
    nodes.push({
      id: payload.id,
      ns,
      name,
      metadata: payload.metadata,
      // graph: payload.graph
    })
    return state.set('nodes', nodes)
  }
  if (action.type === 'fbp:recv:graph:removenode') {
    const {payload} = action
    // impossible UI has generated an id for this node to remove...
    const nodes = state.nodes.flatMap((node) => {
      if (node.id === payload.id) {
        return []
      }
      return node
    })
    const links = state.links.flatMap((link) => {
      if (link.source.id === payload.id || link.target.id === payload.id) {
        return []
      }
      return link
    })
    console.log('strange, seems to work? but does not update', nodes)
    // ah update is only based on same graph basis...
    return state.set('nodes', nodes).set('links', links)
  }
  /*
  if (action.type === 'graph:monitor:addedge') {
    return Immutable(action.payload)
  }
  if (action.type === 'graph:monitor:changenode') {
    return Immutable(action.payload)
  }
  if (action.type === 'graph:monitor:changeedge') {
    return Immutable(action.payload)
  }
  */
  return state
}
