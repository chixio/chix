import Immutable from 'seamless-immutable'

const initialState = Immutable([])

export interface FbpRecvGraphAddInitialAction {
  type: 'fbp:recv:graph:addinitial'
  payload: any
}

export function initials(
  state = initialState,
  action: FbpRecvGraphAddInitialAction
) {
  if (action.type === 'fbp:recv:graph:addinitial') {
    return state.concat([action.payload])
  }
  return state
}
