import Immutable from 'seamless-immutable'

export interface FbpRecvNetworkDataAction {
  type: 'fbp:recv:network:data'
  payload: any
}

const initialState = Immutable([])

export function packets(
  state = initialState,
  action: FbpRecvNetworkDataAction
) {
  if (action.type === 'fbp:recv:network:data') {
    return state.concat([action.payload])
  }

  return state
}
