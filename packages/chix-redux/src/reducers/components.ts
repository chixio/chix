import Immutable from 'seamless-immutable'

export interface ComponentAction {
  type: 'fbp:recv:component:component'
  payload: any
}

const initialState = Immutable({})

export function components(state = initialState, action: ComponentAction) {
  if (action.type === 'fbp:recv:component:component') {
    return state.set(action.payload.name, Immutable(action.payload))
  }
  return state
}
