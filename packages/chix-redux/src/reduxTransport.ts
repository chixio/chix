import {
  WebSocketBrowserTransport,
  WebSocketBrowserTransportOptions,
  // tslint:disable-next-line:no-submodule-imports
} from '@chix/transport/lib/transport/websocketBrowser'

export type ReduxTransportOptions = {
  dispatch: any
} & WebSocketBrowserTransportOptions

export class ReduxTransport extends WebSocketBrowserTransport {
  public dispatch: any
  constructor(options: ReduxTransportOptions) {
    super({
      url: options.url,
      protocol: options.protocol,
      // secure: options.secure,
      schemas: options.schemas,
      role: options.role,
      secret: options.secret,
      bail: options.bail,
    })
    if (!options.dispatch) {
      throw Error('Redux transport requires a dispatch option')
    }

    this.dispatch = options.dispatch
  }
  /**
   * Send the Error using a dispatch instead of sending it directly
   *
   * sendError method is called internally by the runtime client to send back errors.
   */
  public sendError(protocol, msg, _conn) {
    // dispatch it so it first runs through the system.
    this.dispatch({
      type: ['fbp', 'send', protocol, 'error'].join(':'),
      payload: msg,
    })
  }
}
