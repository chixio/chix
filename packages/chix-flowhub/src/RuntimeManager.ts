import * as request from 'superagent'

const DEFAULT_PING_INTERVAL = 10 * 60 * 1000

export interface RuntimeManagerOptions {
  uid?: string
  token: string
  api: string
  pingInterval?: number
}

export interface Runtime {
  id: string
  address: string
  type: string
  label: string
  description: string
  registered: string
  seen: string
  user: string
  secret: string | null
}

export class RuntimeManager {
  private options: RuntimeManagerOptions

  constructor(options: RuntimeManagerOptions) {
    if (!options.token) {
      throw Error('Runtime manager requires an api token.')
    }

    if (!options.api) {
      throw Error('Runtime manager requires an api option.')
    }

    this.options = options
  }

  public async register(runtime: Partial<Runtime>) {
    /*
		if (!runtime.id) {
			throw Error('Runtime registration requires an id')
		}
		*/

    if (!runtime.address) {
      throw Error('Runtime registration requires an address URL')
    }

    /*
		if (!runtime.protocol) {
			throw Error('Runtime registration requires a protocol')
		}
		*/

    if (!runtime.type) {
      throw Error('Runtime registration requires a type')
    }

    if (!runtime.label) {
      throw Error('Runtime registration requires a label')
    }

    const data = {
      protocol: 'websocket',
      ...runtime,
    }

    if (this.options.uid) {
      data.user = this.options.uid
    }

    const {body} = await request
      .put(`${this.options.api}/${runtime.id || ''}`)
      .set('Authorization', this.authorizationHeader)
      .send(data)

    return body
  }

  public async pingPoll(runtimeId: string, interval = DEFAULT_PING_INTERVAL) {
    return setInterval(() => {
      this.ping(runtimeId).catch((error) => {
        console.error(error)
        console.error(`Failed to ping to ${this.options.api}/${runtimeId}`)
      })
    }, interval)
  }

  public async ping(runtimeId: string) {
    const {body} = await request
      .post(`${this.options.api}/${runtimeId}`)
      .send({})

    return body
  }

  public async get(runtimeId: string) {
    const {body, status} = await request
      .get(`${this.options.api}/${runtimeId}`)
      .set('Authorization', this.authorizationHeader)

    if (status !== 200) {
      throw Error(`Request returned ${status}`)
    }

    return Object.keys(body).reduce((runtime, name) => {
      if (name === 'seen' || name === 'registered') {
        return {
          ...runtime,
          name: new Date(runtime[name]),
        }
      }

      return runtime
    }, body)
  }

  public async del(runtimeId: string) {
    const {body} = await request
      .del(`${this.options.api}/${runtimeId}`)
      .set('Authorization', this.authorizationHeader)

    return body
  }

  public async clearAll() {
    const runtimes = await this.list()

    await Promise.all(runtimes.map((runtime: any) => this.del(runtime.id)))
  }

  public async getRuntimeByLabel(label: string): Promise<Runtime | undefined> {
    return this.getRuntimeBy({label})
  }

  public async getRuntimeBy(query: {
    [key: string]: any
  }): Promise<Runtime | undefined> {
    return (await this.list()).find((item: Runtime) => {
      const keys = Object.keys(query)

      return (
        keys.reduce(
          (count, key) => ((item as any)[key] === query[key] ? ++count : count),
          0
        ) === keys.length
      )
    })
  }

  public async list(): Promise<Runtime[]> {
    const {status, body} = await request
      .get(`${this.options.api}/`)
      .set('Authorization', this.authorizationHeader)

    if (status !== 200) {
      throw Error(`Request returned ${status}`)
    }

    return body.map((runtime: Runtime) => ({
      ...runtime,
      registered: new Date(runtime.registered),
      seen: new Date(runtime.seen),
    }))
  }

  private get authorizationHeader() {
    return `Bearer ${this.options.token}`
  }
}
