# Chiχ Flowhub

Simple class to interact with the flowhub runtime.

Implementation adapted from: https://github.com/flowhub

Is used by the fbpx-cli to register and ping the flowhub runtime.
