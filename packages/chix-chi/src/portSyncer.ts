import {Link, Packet} from '@chix/flow'

export type SyncPort = string
export type SyncPorts = SyncPort[]

export type PortSyncerStore = {
  [itemId: string]: {
    [targetPort: string]: {
      link: Link
      p: Packet
    }
  }
}

/*
 * Serves as a gate for the synced ports.
 *
 * chi: {
 *   <nodeId>: <uuid>
 * }
 */
export class PortSyncer {
  public store: PortSyncerStore = {}
  public originId: string
  public syncPorts: SyncPorts

  constructor(originId: string, syncPorts: SyncPorts) {
    this.syncPorts = syncPorts
    this.originId = originId
  }

  public add(link: Link, packet: Packet) {
    if (!packet.chi.hasOwnProperty(this.originId)) {
      throw new Error(
        ['Origin Node', this.originId, 'not found within chi'].join(' ')
      )
    }

    if (this.syncPorts.indexOf(link.target.port) === -1) {
      throw new Error(
        ['Refuse to handle data for unknown port:', link.target.port].join('')
      )
    }

    const itemId = packet.chi[this.originId]

    if (!this.store.hasOwnProperty(itemId)) {
      this.store[itemId] = {}
    }

    this.store[itemId][link.target.port] = {link, p: packet}

    if (Object.keys(this.store[itemId]).length === this.syncPorts.length) {
      const dat = this.store[itemId]

      delete this.store[itemId]

      return dat
    } else {
      return undefined
    }
  }
}
