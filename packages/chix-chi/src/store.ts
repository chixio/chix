export type StoreType = {[id: string]: any}

export class Store {
  public store: StoreType = {}
  public order: string[] = []

  get length() {
    return this.order.length
  }

  public set(id: string, obj: any): void {
    this.store[id] = obj
    this.order.push(id)
  }

  public get(id: string): any {
    return this.store[id]
  }

  public del(id: string): void {
    this.order.splice(this.order.indexOf(id), 1)

    delete this.store[id]
  }

  public items(): StoreType {
    return this.store
  }

  public pop(): any {
    const id = this.order.pop()

    if (id) {
      const ret = this.store[id]

      delete this.store[id]

      return ret
    }

    throw Error('Cannot pop')
  }

  public shift(): any {
    const id = this.order.shift()

    if (id) {
      const ret = this.store[id]

      delete this.store[id]

      return ret
    }

    throw Error('Cannot shift')
  }

  public isEmpty(): boolean {
    return Object.keys(this.store).length === 0 && this.order.length === 0
  }
}
