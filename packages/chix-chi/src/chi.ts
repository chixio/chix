import {Link, Packet} from '@chix/flow'
import {EventEmitter} from 'events'
import {Callback, Group} from './group'
import {PortPointer} from './portPointer'
import {PortSyncer, SyncPorts} from './portSyncer'
import {Store} from './store'

export type Chi = {
  [key: string]: any
}

export type Queue = {
  [ioid: string]: {
    [gid: string]: string[]
  }
}

export class CHI extends EventEmitter {
  public groups = new Store() // for groups
  public pointers = new Store() // for 'pointers'
  public _sync = new Store() // for 'syncing'
  public queue: Queue = {}

  /**
   *
   * Creates a new group/collection
   *
   */
  public group(port?: string, callback?: Callback) {
    const _group = new Group(port, callback)

    this.groups.set(_group.gid(), _group)
    this.emit('begingroup', _group)

    return _group
  }

  /**
   * Simple way to give a unique common id to the data
   * at the output ports which want to be synced later on.
   *
   *  *in indicates we want to take along the common id of
   *  the other ports also pointing to the process.
   *
   *  Later this can be used with input ports that have
   *  been set to `sync` with the output originating from
   *  a process they specify.
   *
   * @param {String} sourceId
   * @param {String} port      The current port
   * @param {Packet} packet
   * @param {String} identifier Array of port names to sync
   */
  public pointer(
    sourceId: string,
    port: string,
    packet: Packet,
    identifier?: string
  ) {
    if (packet.chi.hasOwnProperty(sourceId)) {
      return
      /*
      throw new Error(
        'item already set'
      );
      */
    }

    let portPointer = this.pointers.get(sourceId)

    if (!portPointer) {
      portPointer = new PortPointer(identifier)

      this.pointers.set(sourceId, portPointer)
    }

    // will give the correct id, based on the ports queue
    packet.chi[sourceId] = portPointer.add(port)
  }

  public sync(
    link: Link,
    originId: string,
    packet: Packet,
    syncPorts: SyncPorts
  ) {
    let ps = this._sync.get(link.target.pid)

    if (!ps) {
      ps = new PortSyncer(originId, syncPorts)

      this._sync.set(link.target.pid, ps)
    }

    const result = ps.add(link, packet)

    if (result !== undefined) {
      this.emit('synced', link.target.pid, result)
    }
  }

  public collect(link: Link, packet: Packet) {
    let idx
    let mx = -1

    for (const gid_ in packet.chi) {
      if (packet.chi.hasOwnProperty(gid_)) {
        idx = this.groups.order.indexOf(gid_)
        mx = idx > mx ? idx : mx
      }
    }

    if (mx === -1) {
      throw Error('Could not match group')
    }

    const gid = this.groups.order[mx]

    if (!this.queue.hasOwnProperty(link.ioid)) {
      this.queue[link.ioid] = {}
    }

    if (!Array.isArray(this.queue[link.ioid][gid])) {
      this.queue[link.ioid][gid] = []
      this.groups.get(gid).on('complete', () => {
        this.readySend(link, gid, packet)
      })
    }

    this.queue[link.ioid][gid].push(packet.read(packet.owner))

    this.readySend(link, gid, packet)
  }

  public readySend(link: Link, gid: string, packet: Packet) {
    const group = this.groups.get(gid)

    if (
      // if group is complete
      group.complete &&
      // if queue length matches the group length
      this.queue[link.ioid][gid].length === group.length
    ) {
      // Important: group seizes to exist for _this_ path.
      delete packet.chi[gid]

      // Reusing last collected packet to write the group data
      // packet is not owned while arriving
      packet.setOwner(link)
      packet.write(link, this.queue[link.ioid][gid])

      // link.write(packet)
      link.target.write(packet)

      // reset
      this.queue[link.ioid][gid] = []

      /* Not sure..
        delete output.chi[gid];        // remove it for our requester
        // group still exists for other paths.
        delete this.store[gid];        // remove from the store
        this.groupOrder.splice(mx, 1); // remove from the groupOrder.
      */
    }
  }

  public merge(newChi: Chi, oldChi: Chi, unique?: boolean) {
    if (Object.keys(oldChi).length) {
      for (const c in oldChi) {
        if (oldChi.hasOwnProperty(c)) {
          if (newChi.hasOwnProperty(c) && newChi[c] !== oldChi[c]) {
            if (unique) {
              throw new Error('refuse to overwrite chi item')
            }
          } else {
            newChi[c] = oldChi[c]
          }
        }
      }
    }
  }
}
