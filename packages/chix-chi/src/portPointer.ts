import * as uuid from 'uuid'

export type PortPointerStore = {
  [port: string]: number[]
}

/**
 *
 * The PortPointer initializes each source with the same unique itemId.
 *
 * These are then increased per port, so their id's stay in sync.
 *
 * This does give the restriction both ports should give an equal amount of output.
 */
export class PortPointer {
  public static uid(prefix: string): string {
    return prefix + uuid.v4().split('-')[0]
  }

  public id: string
  public counter = 0
  public store: PortPointerStore = {}

  constructor(identifier: string) {
    this.id = PortPointer.uid(identifier ? identifier + '-' : '')
  }

  public add(port: string): string {
    if (!this.store.hasOwnProperty(port)) {
      this.store[port] = [this.counter]
    }

    let nr = this.store[port][this.store[port].length - 1]

    this.store[port].push(++nr)

    return this.id + '-' + nr
  }
}
