import {Packet} from '@chix/flow'
import {EventEmitter} from 'events'
import * as uuid from 'uuid'

export type GroupStore = Packet[]

export type Info = {
  items: string[]
  complete: boolean
  gid: string
}
export type Output = {[port: string]: Info}

export type Item = {[gid: string]: string}

export type Callback = (output: Output) => void

/**
 * Simple grouping.
 *
 * Group can be used from within the blackbox.
 *
 * Or is used during cyclic and collect mode.
 *
 * During cyclic mode it can be considered virtual grouping.
 *
 * The virtual group will be recollected during collect mode.
 *
 * For now this will be simple, there can only be one collector
 * and the group will seize too exists once it's collected.
 */
export class Group extends EventEmitter {
  // allow (tests) to mock gid.
  public static itemId = uuid.v4
  public static gid = (prefix: string) => prefix + uuid.v4()

  public end = this.done
  public cb: Callback
  public port: string
  public info: Info
  public store: GroupStore

  constructor(port: string, callback: Callback) {
    super()

    if (arguments.length !== 2) {
      throw new Error('Not enough arguments')
    }

    // these are undefined in virtual mode
    this.cb = callback
    this.port = port

    const prefix = 'gid-' + (port || '')

    this.info = {
      gid: Group.gid(prefix + '-'),
      complete: false,
      items: [],
    }

    this.store = []

    this.send()
  }

  get length() {
    return this.info.items.length
  }

  get complete() {
    return this.info.complete
  }

  /**
   *
   * Generates a new itemId and returns
   * The group and itemId
   *
   * Used like this:
   *
   *  cb({
   *    match: match
   *  }, g.item());
   *
   * The item id is sent across 'the wire' and we
   * maintain the total group info over here.
   *
   * [<gid>] = itemId
   * [<gid>] = itemId
   */
  public item(obj: Item = {}): Item {
    if (obj.hasOwnProperty(this.info.gid)) {
      throw Error('Object is already within group')
    }

    const id = Group.itemId()

    this.info.items.push(id)

    obj[this.info.gid] = id

    return obj
  }

  public collect(packet: Packet): void {
    this.store.push(packet)
  }

  public done(): void {
    this.info.complete = true
    this.send()

    this.emit('complete', this)
  }

  /***
   *
   * Sends the output using the callback of the
   * node who requested the group.
   *
   * In case of grouping during cyclic mode, for now,
   * there is nothing to send to. In which case
   * the callback is empty.
   */
  public send(): void {
    if (this.cb) {
      const output: Output = {}

      output[this.port] = {
        gid: this.info.gid,
        complete: !!this.info.complete,
        items: this.info.items,
      }

      this.cb(output)
    }
  }

  public items() {
    return this.info.items
  }

  public gid() {
    return this.info.gid
  }
}
