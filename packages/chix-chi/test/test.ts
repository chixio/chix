import test from 'tape'

// Note: CHI is a singleton the entire test
// uses it, and it never resets, that's on
// purpose.
import {CHI as _CHI, Group} from '@chix/chi'

const CHI = new _CHI()

// TODO: FIXME
type Target = any

// Overwrite, cannot test uuid's
let gid = 0
let itemId = 0

Group.gid = () => {
  gid++

  return 'group-' + gid
}
;(Group as any).itemId = () => {
  itemId++

  return 'item-' + itemId
}

test('Group creation', (t) => {
  t.equal(CHI.groups.isEmpty(), true, 'CHI.groups is empty')

  // var g = CHI.group(port, cb); << normal usage, port could become 'name'
  const g = CHI.group()

  t.equal(CHI.groups.length, 1, 'Store contains one group')

  t.equal(CHI.groups.get(g.gid()).gid(), g.gid(), 'This group is stored')

  t.end()
})

test('Adding items to the group', (t) => {
  const g = CHI.group()

  t.equal(CHI.groups.length, 2, 'Singleton Store now contains two groups')

  // add first group item
  const obj = {}

  g.item(obj)

  t.deepLooseEqual(
    obj,
    {'group-2': 'item-1'},
    'Object 1 knows group and itemId'
  )

  // add second group item
  const obj2 = {}

  g.item(obj2)

  t.deepLooseEqual(
    obj2,
    {'group-2': 'item-2'},
    'Object 2 knows group and itemId'
  )

  t.equal(g.length, 2, 'Group has two items')

  t.deepLooseEqual(
    g.items(),
    ['item-1', 'item-2'],
    'The items are item-1 and item-2'
  )

  t.throws(
    () => {
      g.item(obj2)
    },
    /Object is already within group/,
    'Object is already within group'
  )

  t.notOk(g.complete, 'The group is not complete yet')

  g.end()

  t.ok(g.complete, 'The group is complete')

  t.end()
})

// Err, does not work... :/
test('Test collecting groups', (t) => {
  // ok, seems collecting groups should still
  // just work.
  // However, it's very much bound to link
  // so maybe change it later
  // CHI.collect();

  t.end()
})

test('Add a pointer to CHI', (t: any) => {
  // this mimics different paths.
  const p = {chi: {}} as any
  const p2 = {chi: {}} as any

  CHI.pointer('someId', 'port1', p)
  CHI.pointer('otherId', 'port2', p)
  CHI.pointer('someId', 'port2', p2)
  CHI.pointer('otherId', 'port1', p2)

  t.deepEqual(p.chi, p2.chi, 'First round, Both the same group and itemId')

  CHI.pointer('someId', 'port1', p)
  CHI.pointer('otherId', 'port2', p)
  CHI.pointer('someId', 'port2', p2)
  CHI.pointer('otherId', 'port1', p2)

  t.deepEqual(p.chi, p2.chi, 'Second round, Both the same group and itemId')

  t.end()
})

test('Testing pointing multiple times to the same port', (t) => {
  const packet = {chi: {}} as any

  // both are item with id 1
  CHI.pointer('multi', 'port1', packet)
  t.equal((packet.chi as any).multi[(packet.chi as any).multi.length - 1], '1')

  CHI.pointer('multi', 'port1', packet)
  t.equal((packet.chi as any).multi[(packet.chi as any).multi.length - 1], '1')

  t.end()
})

test('Syncing to ports', (t) => {
  const p1 = {chi: {}} as any
  const p2 = {chi: {}} as any

  CHI.pointer('sourceId', 'port1', p1)
  CHI.pointer('sourceId', 'port2', p2)

  const data1 = {
    inter: 'esting',
    infor: 'mation',
  }

  const data2 = {
    other: 'information',
  }

  CHI.on('synced', (targetId, data) => {
    // console.log('TARGETID', targetId, 'DATA', JSON.stringify(data, null, 2));

    t.equal(targetId, 'target1', 'target1 is synced')
    t.deepLooseEqual(
      Object.keys(data),
      ['targetPort1', 'targetPort2'],
      'Received data for targetPort1&2'
    )

    t.equal(
      data.targetPort1.p.chi.sourceId,
      data.targetPort2.p.chi.sourceId,
      'ItemIds are the same'
    )

    t.equal(data.targetPort1.p.data, data1, 'Data for port1 is as expected')

    t.equal(data.targetPort2.p.data, data2, 'Data for port2 is as expected')

    t.equal(
      data.targetPort1.link.target.id,
      data.targetPort2.link.target.id,
      'Both have the same target'
    )

    t.end()
  })

  // This doesn't work if things are required to look
  // like links and packets. Makes it too complex.
  // pid should just be given as id, not sure
  // why link has to go in and out.
  // Because we have to write to it later on.
  // But this means link is data just traveling along
  // nothing more, the pid however is an id we use
  // which also happens to be within the link.
  // Both have the same target which is correct.
  // Somehow there is no check whether the data stays in sync.
  // This test is not sufficient because it does not send enough data.

  const target1 = {
    target: {
      pid: 'target1',
      id: 'target1',
      port: 'targetPort1',
    },
  } as Target

  CHI.sync(
    target1, // targetId
    'sourceId', // originId
    {
      data: data1, // data for this destination port
      chi: p1.chi,
    } as any, // tha chi
    [
      // ports we want to sync
      'targetPort1',
      'targetPort2',
    ]
  )

  const target2 = {
    target: {
      pid: 'target1', // targetId
      id: 'target1', // targetId
      port: 'targetPort2', // current port
    },
  } as Target

  CHI.sync(
    target2,
    'sourceId', // originId
    {
      data: data2, // data for this destination port
      chi: p2.chi,
    } as any, // tha chi
    [
      // ports we want to sync
      'targetPort1',
      'targetPort2',
    ]
  )
})

// UNTESTED the callback and sending of data.
// for xmatch and xin this is done twice.
// which works well. However, it should probably
// be done by eventemitter and not this way with a
// callback.
// on groupbegin you send, on group end you send again.
// maybe this makes sync ports much easier also.
// Some will be listening for group begin and end
// specifically on the process to which they have a pointer.
// I think there never is a need for one process to check
// 2 kind of groups.anyway it could.
// Just keep in mind generic id checking you can have several
// passes for different kind of stuff.
