export interface Process {
  component: string
  metadata?: {
    /**
     * X coordinate of a graph node
     */
    x?: number
    /**
     * Y coordinate of a graph node
     */
    y?: number
    [k: string]: any
  }
  [k: string]: any
}

export interface InPort {
  process: string
  port: string
  metadata?: {
    /**
     * X coordinate of a graph node
     */
    x?: number
    /**
     * Y coordinate of a graph node
     */
    y?: number
    [k: string]: any
  }
}

export interface OutPort {
  process: string
  port: string
  metadata?: {
    /**
     * X coordinate of a graph node
     */
    x?: number
    /**
     * Y coordinate of a graph node
     */
    y?: number
    [k: string]: any
  }
}

export interface Environment {
  /**
   * Runtime type the graph is for
   */
  type: string
  /**
   * HTML fixture for browser-based graphs
   */
  content?: string
  [k: string]: any
}

export interface Properties {
  /**
   * Name of the graph
   */
  name?: string
  /**
   * Information about the execution environment for the graph
   */
  environment?: Environment
  /**
   * Graph description
   */
  description?: string
  /**
   * Name of the icon that can be used for depicting the graph
   */
  icon?: string
  [k: string]: any
}

export interface GroupMetadata {
  description?: string
  [k: string]: any
}

export interface Group {
  name?: string
  nodes?: string[]
  metadata?: GroupMetadata
}

export interface SrcPort {
  process: string
  port: string
  index?: number
}

export interface TgtPort {
  process: string
  port: string
  index?: number
}

export interface ConnectionMetadata {
  /**
   * Route identifier of a graph edge
   */
  route?: number
  /**
   * JSON schema associated with a graph edge
   */
  schema?: string
  /**
   * Whether edge data should be treated as secure
   */
  secure?: boolean
  [k: string]: any
}

export interface Connection {
  src?: SrcPort
  tgt: TgtPort
  data?: any
  metadata?: ConnectionMetadata
}

/**
 * A graph of FBP processes and connections between them.
 * This is the primary way of specifying FBP programs.
 *
 */
export interface FbpGraph {
  /**
   * Whether the graph port identifiers should be treated as case-sensitive
   */
  caseSensitive?: boolean
  /**
   * User-defined properties attached to the graph.
   */
  properties?: Properties
  /**
   * Exported inports of the graph
   */
  inports?: {
    [k: string]: InPort
  }
  /**
   * Exported outports of the graph
   */
  outports?: {
    [k: string]: OutPort
  }
  /**
   * List of groups of processes
   */
  groups?: Group[]
  /**
   * The processes of this graph.
   * Each process is an instance of a component.
   *
   */
  processes?: {
    /**
     * This interface was referenced by `undefined`'s JSON-Schema definition
     * via the `patternProperty` "[a-zA-Z0-9_]+".
     */
    [k: string]: Process
  }
  /**
   * Connections of the graph.
   * A connection either connects ports of two processes, or specifies an IIP as initial input packet to a port.
   *
   */
  connections?: Connection[]
}
