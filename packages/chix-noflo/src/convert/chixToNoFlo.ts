import {Flow as FlowDefinition, Link, Node} from '@chix/common'
import {FbpGraph} from '../interfaces'
import {convertLink, ConvertLinkOptions} from './convertLink'
import {convertNode} from './convertNode'
import {stringifyObject} from './stringifyObject'

export type ChixToNoFlowOptions = ConvertLinkOptions

export function chixToNoFlo(
  flowDefinition: FlowDefinition,
  {persist_route}: ChixToNoFlowOptions = {
    persist_route: 5,
  }
) {
  const graph: FbpGraph = {
    properties: {
      name: flowDefinition.title,
    },
    connections: flowDefinition.links.map((link: Link) =>
      convertLink(link, {persist_route})
    ),
    processes: flowDefinition.nodes.reduce(convertNode, {}),
    // exports: []
  }

  flowDefinition.nodes.forEach((node: Node) => {
    // const nodeDefinition = getNodeDefinition(node)
    const filled: string[] = []

    if (node.context) {
      const context = node.context
      Object.keys(context).forEach((port) => {
        ;(graph.connections as any).push({
          data: stringifyObject(context[port]),
          tgt: {process: node.id, port},
        })

        filled.push(port)
      })
    }

    if (flowDefinition.ports && flowDefinition.ports.input) {
      const inputPorts = flowDefinition.ports.input
      Object.keys(inputPorts).forEach((port) => {
        const _port = inputPorts[port]

        if (
          _port &&
          _port.hasOwnProperty('default') &&
          filled.indexOf(port) === -1
        ) {
          ;(graph.connections as any).push({
            data: stringifyObject(_port.default),
            tgt: {process: node.id, port},
          })
        }
      })
    }
  })

  return graph
}
