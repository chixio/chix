import {
  Connector,
  Flow as FlowDefinition,
  IIP,
  Link,
  TargetConnectorSettings,
} from '@chix/common'
import {Connection, FbpGraph, Process} from '../interfaces'

export interface Result {
  iips: IIP[]
  links: Link[]
}

export function convertProcessToNode(key: string, item: Process) {
  const [ns, name] = item.component.split('/')

  return {
    id: key,
    title: item.label,
    name,
    ns,
  }
}

export function convertConnectionToLink(
  connection: Connection,
  options: {
    persist_route?: number
  } = {}
): Link {
  const {src, tgt, metadata} = connection

  const link: Link = {}

  if (src) {
    link.source = {
      id: src.process,
      port: src.port,
      setting: {
        index: src.index,
      },
    }
  }

  if (tgt) {
    const linkTarget: Connector = {
      id: tgt.process,
      port: tgt.port,
      setting: {
        index: tgt.index,
      },
    }

    if (
      metadata &&
      metadata.route &&
      metadata.route === options.persist_route
    ) {
      ;(linkTarget.setting as TargetConnectorSettings).persist = true
    }

    link.target = linkTarget
  }

  return link
}

export function noFloToChix(
  name: string,
  ns: string,
  graph: FbpGraph,
  options: {
    persist_route?: number
  } = {}
): FlowDefinition {
  const flow: FlowDefinition = {
    type: 'flow',
    // component name should reflect this.
    name,
    ns,
    nodes: [],
    links: [],
  }
  if (graph.properties) {
    const {name: title} = graph.properties

    if (title) {
      flow.title = title
    }
  }

  if (graph.processes) {
    flow.nodes = Object.entries(graph.processes).map(([key, item]) =>
      convertProcessToNode(key, item)
    )
  }

  if (graph.connections) {
    // const {iips, links} = graph
    const {links} = graph.connections
      .map((edge) => convertConnectionToLink(edge, options))
      .reduce(
        (result: Result, connection) => {
          if (!connection.source) {
            result.iips.push(connection as IIP)
          } else {
            result.links.push(connection)
          }
          return result
        },
        {
          iips: [],
          links: [],
        }
      )

    flow.links = links

    // do something with iips...
  }

  return flow
}
