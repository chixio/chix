import {Node} from '@chix/common'

export function convertNode(map: {[id: string]: any}, node: Node) {
  // note: this works, because we parse .fbp with the
  // skipIDs options, otherwise all process names would be uuid's
  map[node.id] = {
    component: node.ns + '/' + node.name,
  }

  return map
}
