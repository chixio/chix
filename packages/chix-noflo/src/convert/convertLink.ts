import {Connector, Link as LinkDefinition} from '@chix/common'
import {Connection, SrcPort, TgtPort} from '../interfaces'

export type ConvertLinkOptions = {
  persist_route?: number
}

export function convertLink(
  link: LinkDefinition,
  options: ConvertLinkOptions = {
    persist_route: 5,
  }
) {
  const source = link.source as Connector
  const target = link.target as Connector

  const connection: Connection = {
    src: {
      process: source.id,
      port: source.port,
    },
    tgt: {
      process: target.id,
      port: target.port,
    },
  }

  if (source.setting) {
    if (source.setting.hasOwnProperty('index')) {
      ;(connection.src as SrcPort).index = source.setting.index as number
    }
  }

  if (target.setting) {
    if (target.setting.hasOwnProperty('index')) {
      ;(connection.tgt as TgtPort).index = target.setting.index as number
    }
    if (target.setting.persist) {
      connection.metadata = {
        route: options.persist_route,
      }
    }
  }

  return connection
}
