// Initial data is sent as stringified string
export function stringifyObject(obj: any) {
  return typeof obj === 'object' ? JSON.stringify(obj) : obj
}
