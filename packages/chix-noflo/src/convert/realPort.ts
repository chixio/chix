import {Flow as FlowDefinition, NodeDefinition} from '@chix/common'

export function realPort(
  node: FlowDefinition | NodeDefinition,
  type: string,
  port: string
) {
  if (node.ports) {
    const ports = (node.ports as any)[type] as any
    if (ports) {
      const realNames = Object.keys(ports)
      if (realNames.indexOf(port) >= 0) {
        return port
      }
      for (const real of realNames) {
        if (port.toLowerCase() === real.toLowerCase()) {
          return real
        }
      }
    }
  }
  // Faulty port, let runtime throw the error
  return port
}
