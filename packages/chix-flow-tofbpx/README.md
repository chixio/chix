Chiχ Flow to FBPX
=========

Converts a graph in json format to .fbp format.

Used by [fbpx](https://github.com/psichi/fbpx) for the reverse option.

Usage:
```
$ fbpx -r flow.json | fbpx -j | \
  fbpx -r | fbpx -j | fbpx -r | \
  fbpx -j | fbpx -r | fbpx -j | \
  fbpx -r | fbpx -j | fbpx -r > flow.fbp
```
