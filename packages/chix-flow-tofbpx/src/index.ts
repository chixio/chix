import {
  Flow as FlowDefinition,
  Link as LinkDefinition,
  Node,
} from '@chix/common'
import camelCase from './camelcase'

export function convert(flow: FlowDefinition) {
  const toFBPX = new ToFBPX()

  return toFBPX.convert(flow)
}

export function deSlash(str: string) {
  return str.replace(/\/[A-z]/g, (a) => a[1].toUpperCase() + a.slice(2))
}

export class ToFBPX {
  public known: string[] = []

  public getTitle(node: Node, index?: number): string {
    const append = index || ''

    if (!index) {
      index = 1
    }

    const title = deSlash(
      camelCase(node.title || node.ns + ' ' + node.name, {}) + append
    )

    if (this.known.indexOf(title) >= 0) {
      return this.getTitle(node, ++index)
    }

    this.known.push(title)

    node.title = title

    return title
  }

  public convert(flow: FlowDefinition): string {
    const fbpx: string[] = []
    const nodes: {[nodeId: string]: Node} = {}

    if (flow.title) {
      fbpx.push(['title:\t\t', flow.title].join(''))
    }

    if (flow.description) {
      fbpx.push(['description:\t\t', flow.description].join(''))
    }

    const defs: string[] = []
    const context: string[] = []

    if (Array.isArray(flow.nodes)) {
      flow.nodes.forEach((node: Node) => {
        nodes[node.id] = node

        const title = this.getTitle(node)

        defs.push([title, '(', node.ns, '/', node.name, ')'].join(''))

        if (node.context) {
          for (const port in node.context) {
            if (node.context.hasOwnProperty(port)) {
              context.push(
                [
                  JSON.stringify(node.context[port], null, 2),
                  '->',
                  '@' + port,
                  title,
                  '\n',
                ].join(' ')
              )
            }
          }
        }
      })
    }

    fbpx.push(defs.join('\n'))

    fbpx.push('\n')

    if (context.length) {
      context.forEach((c) => {
        fbpx.push(c)
      })

      fbpx.push('\n')
    }

    if (Array.isArray(flow.links)) {
      flow.links.forEach((link: LinkDefinition) => {
        if (link.source && link.target) {
          fbpx.push(
            [
              nodes[link.source.id].title,
              link.source.port,
              '->',
              link.target.port,
              nodes[link.target.id].title,
            ].join(' ')
          )
        } else {
          throw Error('Invalid link.')
        }
      })
    }

    return fbpx.join('\n')
  }
}
