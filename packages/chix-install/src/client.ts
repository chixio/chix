import {NodeDependencies} from '@chix/common'
import * as scriptjs from 'scriptjs'

import {IChixInstaller} from './IChixInstaller'
// import { stripVersion } from './util'

export type Scripts = string[]

const scriptLoader = (scripts: Scripts) => {
  return new Promise((resolve) => {
    scriptjs(scripts, resolve)
  })
}

export interface AddedScripts {
  [url: string]: boolean
}

export class Installer implements IChixInstaller {
  public added: AddedScripts = {}
  public cdn = 'https://packages.chix.io'

  constructor(_options: any) {}

  public require(packagePath: string) {
    return require(packagePath)
  }

  public async load(dependencies: NodeDependencies): Promise<string[]> {
    if (Array.isArray(dependencies)) {
      throw Error('Use dependencies.npm, requires is DEPRECATED')
    }

    if (dependencies && Object.keys(dependencies).length) {
      if (dependencies.npm) {
        const scripts: Scripts = []

        const requires = Object.keys(dependencies.npm)

        requires.forEach((requireString) => {
          // e.g. chix-flow/schemas/node@latest
          const packageName = requireString.replace(/\/.*/, '')
          const version = dependencies.npm[requireString]

          try {
            require.resolve(requireString) // stripVersion(requireString))
          } catch (e) {
            const url = [this.cdn, '/', packageName, '@', version].join('') // encodeURIComponent(r)].join('')

            this.added[url] = false

            scripts.push(url)
          }
        })

        if (scripts.length) {
          // load the ones not loaded/loading
          await scriptLoader(scripts)

          // script loaded.
          scripts.forEach((url) => {
            this.added[url] = true
          })

          return scripts
        }

        return []
      }
    }

    return []
  }
}
