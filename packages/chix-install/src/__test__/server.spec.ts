import {expect} from 'chai'
import {Installer} from '../server'

describe('Server', () => {
  it('should install', async () => {
    expect(true).to.eql(true)

    const installer = new Installer({
      dry: true,
    })

    await installer.load({
      npm: {
        markdown: 'latest',
      },
    })
  })
})
