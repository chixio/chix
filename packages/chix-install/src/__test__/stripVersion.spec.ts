import {expect} from 'chai'
import {stripVersion} from '../util'

describe('Strip Version', () => {
  ;[
    ['@chix/flow@latest', '@chix/flow'],
    ['@chix/flow', '@chix/flow'],
    ['chix/flow@latest', 'chix/flow'],
    ['chix/flow', 'chix/flow'],
  ].forEach((pair) => {
    it(pair[0], async () => {
      expect(stripVersion(pair[0])).to.eql(pair[1])
    })
  })
})
