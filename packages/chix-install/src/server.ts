import {NodeDependencies} from '@chix/common'
// import {Installer as NPMInstaller} from '@npmcli/arborist'
import {IChixInstaller} from './IChixInstaller'
import {moduleNeedsInstallation} from './util'

export interface ServerOptions {
  saveDev?: boolean
  save?: boolean
  prefix?: string
  dry?: boolean
}

export class Installer implements IChixInstaller {
  public options: ServerOptions

  constructor({
    save = false,
    saveDev = false,
    prefix = process.cwd(),
    dry = false,
  }: ServerOptions) {
    this.options = {
      save,
      saveDev: !save && saveDev,
      prefix,
      dry,
    }
  }

  public require(packagePath: string) {
    return require(require.resolve(packagePath, {
      paths: [this.options.prefix],
    }))
  }

  /**
   * Takes an object of requires and a callback.
   *
   * Verbose is optional
   */
  public async load(dependencies: NodeDependencies): Promise<string[]> {
    if (!dependencies || Array.isArray(dependencies)) {
      throw Error('Loader needs a dependencies object, require DEPRECATED')
    }
    return new Promise<string[]>(async (resolve) => {
      const types = Object.keys(dependencies)
      if (types.length) {
        if (dependencies.npm) {
          const requires = Object.keys(dependencies.npm)

          const install = requires.filter(
            moduleNeedsInstallation(this.options.prefix)
          )

          if (install.length) {
            /* TODO: doesn't exist anymore replace with Yarn probably.
            const npmInstaller = new NPMInstaller(
              this.options.prefix,
              this.options.dry,
              install
            )
            npmInstaller.run(() => {
              resolve(install)
            })
             */
          } else {
            resolve([])
          }
        } else {
          const unknownTypes = types.filter((type) => type !== 'npm')

          throw Error(`Unknown dependencies type: ${unknownTypes}`)
        }
      } else {
        resolve([])
      }
    })
  }
}
