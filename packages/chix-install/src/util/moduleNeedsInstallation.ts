export function moduleNeedsInstallation(installPath: string) {
  return (moduleName: string) => {
    try {
      require(require.resolve(moduleName, {paths: [installPath]}))

      return false
    } catch (e) {
      return true
    }
  }
}
