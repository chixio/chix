export function stripVersion(moduleName: string): string {
  const index = moduleName.lastIndexOf('@')
  if (index > -1 && index !== 0) {
    return moduleName.slice(0, index)
  }

  return moduleName
}
