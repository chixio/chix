import {NodeDependencies} from '@chix/common'
export declare type Requires = any
export interface IChixInstaller {
  load(dependencies: NodeDependencies): Promise<string[]>
  require(packagePath: string)
}
