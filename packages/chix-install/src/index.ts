export * from './IChixInstaller'
import {Installer as ClientInstaller} from './client'
import {Installer as ServerInstaller} from './server'

export {ClientInstaller, ServerInstaller}
