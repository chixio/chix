import {NodeDefinition} from '@chix/common'
import {readJSON} from 'fs-extra'
import * as glob from 'glob'

async function asyncGlob(path: string): Promise<string[]> {
  return new Promise((resolve, reject) => {
    glob(path, async (error, files) => {
      if (error) {
        reject(error)
      } else {
        resolve(files)
      }
    })
  })
}

// can't just do this.
// seems the only way is to have
// react-docgen-typescript process them correctly
// as that's the one knowing the ast.
// that's a lot of work though

// join type to save original
const expand = (type: string) => type.split(' | ').concat(type)

// options is optional

// A set of types which should be parsed correctly or at least be handled

const knownTypes = ['string', 'number', 'function', 'any', 'boolean']
const unknownTypes = []
;(async () => {
  const files = await asyncGlob('./nodes/ant-design/*/node.json')

  const nodes = (await Promise.all(
    files.map(fileName => readJSON(fileName))
  )) as NodeDefinition[]

  for (const node of nodes) {
    for (const [name, port] of Object.entries(node.ports.input)) {
      if (!knownTypes.includes(port.type)) {
        if (!unknownTypes.includes(port.type)) {
          unknownTypes.push(port.type)
        }
      }
    }
  }

  unknownTypes.sort()

  const result = unknownTypes.map(type => expand(type))

  console.log(JSON.stringify(result, null, 2))
})()
