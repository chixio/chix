# Reactix

Tool to convert react repositories into nodules.

Minimal usage:
```
reactix ./my-react-components
```

This will traverse the directory and create definitions files for all react components found.
The default output dir is './nodes' within the current directory.

See `reactix --help` for some extra options.




