import {NodeDefinition} from '@chix/common'
import {ComponentDoc} from './parsers/ts'

/**
 * Could be used to generate extra nodes.
 * Which enable to execute these methods also.
 */
export function processMethods(
  _component: ComponentDoc,
  nodeDefinition: NodeDefinition
) {
  return nodeDefinition
}
