export interface Type {
  name: string
}

export function processType(type: Type): string {
  let newType: string

  switch (type.name) {
    case 'bool':
      newType = 'boolean'
      break
    case 'func':
      newType = 'function'
      break
    case 'custom':
      // call custom transform of type
      newType = 'string'
      break
    default:
      newType = type.name
  }

  return newType
}
