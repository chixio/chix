import * as ts from 'typescript'
import {Parser} from './parser'
import {ComponentDoc, ParserOptions} from './types'
export function parseWithProgramProvider(
  filePathOrPaths: string | string[],
  compilerOptions: ts.CompilerOptions,
  parserOpts: ParserOptions,
  programProvider?: () => ts.Program
): ComponentDoc[] {
  const filePaths = Array.isArray(filePathOrPaths)
    ? filePathOrPaths
    : [filePathOrPaths]

  const program = programProvider
    ? programProvider()
    : ts.createProgram(filePaths, compilerOptions)

  const parser = new Parser(program, parserOpts)

  const checker = program.getTypeChecker()

  return filePaths
    .map(filePath => {
      console.log('filePath', filePath)

      return filePath
    })
    .map(filePath => program.getSourceFile(filePath))
    .map(heh => {
      console.log('heh1', heh)

      return heh
    })
    .filter((sourceFile): sourceFile is ts.SourceFile => {
      const isSourcefile = typeof sourceFile !== 'undefined'

      return isSourcefile
    })
    .map(heh => {
      console.log('heh2', heh)

      return heh
    })
    .reduce<ComponentDoc[]>((docs, sourceFile) => {
      const moduleSymbol = checker.getSymbolAtLocation(sourceFile)

      if (!moduleSymbol) {
        return docs
      }

      Array.prototype.push.apply(
        docs,
        checker
          .getExportsOfModule(moduleSymbol)
          .map(exp =>
            parser.getComponentInfo(
              exp,
              sourceFile,
              parserOpts.componentNameResolver
            )
          )
          .filter((comp): comp is ComponentDoc => comp !== null)
          .filter((comp, index, comps) =>
            comps
              .slice(index + 1)
              .every(innerComp => innerComp!.displayName !== comp!.displayName)
          )
      )

      return docs
    }, [])
}
