import {defaultOptions, defaultParserOpts} from './defaults'
import {FileParser, ParserOptions} from './types'
import {withCompilerOptions} from './withCompilerOptions'

/**
 * Constructs a parser for a default configuration.
 */
export function withDefaultConfig(
  parserOpts: ParserOptions = defaultParserOpts
): FileParser {
  return withCompilerOptions(defaultOptions, parserOpts)
}
