import {defaultOptions, defaultParserOpts} from './defaults'
import {ParserOptions} from './types'
import {withCompilerOptions} from './withCompilerOptions'
/**
 * Parses a file with default TS options
 * @param filePath component file that should be parsed
 */
export function parse(
  filePathOrPaths: string | string[],
  parserOpts: ParserOptions = defaultParserOpts
) {
  console.log(defaultOptions, parserOpts, filePathOrPaths)
  return withCompilerOptions(defaultOptions, parserOpts).parse(filePathOrPaths)
}
