import * as path from 'path'
// We'll use the currentDirectoryName to trim parent fileNames
const currentDirectoryPath = process.cwd()
const currentDirectoryParts = currentDirectoryPath.split(path.sep)

export const currentDirectoryName =
  currentDirectoryParts[currentDirectoryParts.length - 1]
