import * as ts from 'typescript'

export function getTextValueOfClassMember(
  classDeclaration: any, // ts.ClassDeclaration,
  memberName: string
): string {
  const classDeclarationMembers = classDeclaration.members || []

  const [textValue] =
    classDeclarationMembers &&
    classDeclarationMembers
      .filter(member => ts.isPropertyDeclaration(member))
      .filter(member => {
        const name = ts.getNameOfDeclaration(member) as ts.Identifier
        return name && name.text === memberName
      })
      .map(member => {
        const property = member as ts.PropertyDeclaration
        return (
          property.initializer && (property.initializer as ts.Identifier).text
        )
      })

  return textValue || ''
}
