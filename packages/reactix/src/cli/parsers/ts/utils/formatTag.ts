import * as ts from 'typescript'

export function formatTag(tag: ts.JSDocTagInfo) {
  let result = '@' + tag.name
  if (tag.text) {
    result += ' ' + tag.text
  }
  return result
}
