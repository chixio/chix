import * as ts from 'typescript'
import {getDefaultExportForFile} from './getDefaultExportForFile'
import {getTextValueOfClassMember} from './getTextValueOfClassMember'
import {getTextValueOfFunctionProperty} from './getTextValueOfFunctionProperty'

export function computeComponentName(exp: ts.Symbol, source: ts.SourceFile) {
  const exportName = exp.getName()

  const statelessDisplayName = getTextValueOfFunctionProperty(
    exp,
    source,
    'displayName'
  )

  const statefulDisplayName =
    exp.valueDeclaration &&
    ts.isClassDeclaration(exp.valueDeclaration) &&
    getTextValueOfClassMember(exp.valueDeclaration, 'displayName')

  /*
  console.log(
    `Names: ${exportName}, ${statefulDisplayName}, ${statelessDisplayName}`,
    (exp.valueDeclaration as any).name.escapedText
    //    exp
  )
  */

  if (statelessDisplayName || statefulDisplayName) {
    return statelessDisplayName || statefulDisplayName || ''
  }

  if (exp.valueDeclaration && (exp.valueDeclaration as any).name) {
    return (exp.valueDeclaration as any).name.escapedText
  }

  if (
    exportName === 'default' ||
    exportName === '__function' ||
    exportName === 'Stateless' ||
    exportName === 'StyledComponentClass' ||
    exportName === 'StyledComponent' ||
    exportName === 'FunctionComponent' ||
    exportName === 'StatelessComponent'
  ) {
    return getDefaultExportForFile(source)
  } else {
    return exportName
  }
}
