import * as ts from 'typescript'
import {currentDirectoryName} from '../currentDirectory'
import {ParentType} from '../types'
import {isInterfaceOrTypeAliasDeclaration} from './isInterfaceOrTypeAliasDeclaration'

export function getParentType(prop: ts.Symbol): ParentType | undefined {
  const declarations = prop.getDeclarations()

  if (declarations == null || declarations.length === 0) {
    return undefined
  }

  // Props can be declared only in one place
  const {parent} = declarations[0]

  if (!isInterfaceOrTypeAliasDeclaration(parent)) {
    return undefined
  }

  const parentName = parent.name.text
  const {fileName} = parent.getSourceFile()

  const fileNameParts = fileName.split('/')
  const trimmedFileNameParts = fileNameParts.slice()

  while (trimmedFileNameParts.length) {
    if (trimmedFileNameParts[0] === currentDirectoryName) {
      break
    }
    trimmedFileNameParts.splice(0, 1)
  }
  let trimmedFileName
  if (trimmedFileNameParts.length) {
    trimmedFileName = trimmedFileNameParts.join('/')
  } else {
    trimmedFileName = fileName
  }

  return {
    fileName: trimmedFileName,
    name: parentName,
  }
}
