import * as ts from 'typescript'
import {JSDoc, ParserOptions} from './types'

export const defaultParserOpts: ParserOptions = {}

export const defaultOptions: ts.CompilerOptions = {
  jsx: ts.JsxEmit.React,
  module: ts.ModuleKind.CommonJS,
  target: ts.ScriptTarget.Latest,
}

export const defaultJSDoc: JSDoc = {
  description: '',
  fullComment: '',
  tags: {},
}
