import * as fs from 'fs'
import * as path from 'path'
import * as ts from 'typescript'
import {FileParser, ParserOptions} from './types'
import {withCompilerOptions} from './withCompilerOptions'

/**
 * Constructs a parser for a specified tsconfig file.
 */
export function withCustomConfig(
  tsconfigPath: string,
  parserOpts: ParserOptions
): FileParser {
  const basePath = path.dirname(tsconfigPath)
  const {config, error} = ts.readConfigFile(tsconfigPath, filename =>
    fs.readFileSync(filename, 'utf8')
  )

  if (error !== undefined) {
    // tslint:disable-next-line: max-line-length
    const errorText = `Cannot load custom tsconfig.json from provided path: ${tsconfigPath}, with error code: ${error.code}, message: ${error.messageText}`
    throw new Error(errorText)
  }

  const {options, errors} = ts.parseJsonConfigFileContent(
    config,
    ts.sys,
    basePath,
    {},
    tsconfigPath
  )

  if (errors && errors.length) {
    throw errors[0]
  }

  return withCompilerOptions(options, parserOpts)
}
