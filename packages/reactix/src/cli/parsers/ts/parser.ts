import * as ts from 'typescript'

import {buildFilter} from './buildFilter'
import {defaultJSDoc} from './defaults'
import {
  Component,
  ComponentDoc,
  ComponentNameResolver,
  JSDoc,
  Method,
  MethodParameter,
  ParserOptions,
  PropFilter,
  PropItemType,
  Props,
  StringIndexedObject,
} from './types'
import {
  computeComponentName,
  formatTag,
  getParentType,
  getPropertyName,
  statementIsClassDeclaration,
  statementIsStatelessWithDefaultProps,
} from './utils'

export class Parser {
  private readonly checker: ts.TypeChecker
  private readonly propFilter: PropFilter
  private readonly shouldExtractLiteralValuesFromEnum: boolean
  private readonly savePropValueAsString: boolean

  constructor(program: ts.Program, opts: ParserOptions) {
    const {savePropValueAsString, shouldExtractLiteralValuesFromEnum} = opts
    this.checker = program.getTypeChecker()
    this.propFilter = buildFilter(opts)
    this.shouldExtractLiteralValuesFromEnum = Boolean(
      shouldExtractLiteralValuesFromEnum
    )
    this.savePropValueAsString = Boolean(savePropValueAsString)
  }

  public getComponentInfo(
    exp: ts.Symbol,
    source: ts.SourceFile,
    componentNameResolver: ComponentNameResolver = () => undefined
  ): ComponentDoc | null {
    if (!!exp.declarations && exp.declarations.length === 0) {
      return null
    }

    const type = this.checker.getTypeOfSymbolAtLocation(
      exp,
      exp.valueDeclaration || exp.declarations![0]
    )
    let commentSource = exp
    const typeSymbol = type.symbol || type.aliasSymbol

    if (!exp.valueDeclaration) {
      if (!typeSymbol) {
        return null
      }
      exp = typeSymbol
      const expName = exp.getName()
      commentSource =
        expName === 'StatelessComponent' ||
        expName === 'Stateless' ||
        expName === 'StyledComponentClass' ||
        expName === 'StyledComponent' ||
        expName === 'FunctionComponent'
          ? this.checker.getAliasedSymbol(commentSource)
          : exp
    }

    // Skip over PropTypes that are exported
    if (
      typeSymbol &&
      (typeSymbol.getEscapedName() === 'Requireable' ||
        typeSymbol.getEscapedName() === 'Validator')
    ) {
      return null
    }

    const propsType =
      this.extractPropsFromTypeIfStatelessComponent(type) ||
      this.extractPropsFromTypeIfStatefulComponent(type)

    const resolvedComponentName = componentNameResolver(exp, source)
    const displayName =
      resolvedComponentName || computeComponentName(exp, source)
    const description = this.findDocComment(commentSource).fullComment
    const methods = this.getMethodsInfo(type)

    if (propsType) {
      const defaultProps = this.extractDefaultPropsFromComponent(exp, source)
      const props = this.getPropsInfo(propsType, defaultProps)

      for (const propName of Object.keys(props)) {
        const prop = props[propName]
        const component: Component = {name: displayName}
        if (!this.propFilter(prop, component)) {
          delete props[propName]
        }
      }

      return {
        description,
        displayName,
        methods,
        props,
      }
    } else if (description && displayName) {
      return {
        description,
        displayName,
        methods,
        props: {},
      }
    }

    return null
  }

  public extractPropsFromTypeIfStatelessComponent(
    type: ts.Type
  ): ts.Symbol | null {
    const callSignatures = type.getCallSignatures()

    if (callSignatures.length) {
      // Could be a stateless component.  Is a function, so the props object we're interested
      // in is the (only) parameter.

      for (const sig of callSignatures) {
        const params = sig.getParameters()
        if (params.length === 0) {
          continue
        }
        // Maybe we could check return type instead,
        // but not sure if Element, ReactElement<T> are all possible values
        const propsParam = params[0]
        if (propsParam.name === 'props' || params.length === 1) {
          return propsParam
        }
      }
    }

    return null
  }

  public extractPropsFromTypeIfStatefulComponent(
    type: ts.Type
  ): ts.Symbol | null {
    const constructSignatures = type.getConstructSignatures()

    if (constructSignatures.length) {
      // React.Component. Is a class, so the props object we're interested
      // in is the type of 'props' property of the object constructed by the class.

      for (const sig of constructSignatures) {
        const instanceType = sig.getReturnType()
        const props = instanceType.getProperty('props')

        if (props) {
          return props
        }
      }
    }

    return null
  }

  public extractMembersFromType(type: ts.Type): ts.Symbol[] {
    const methodSymbols: ts.Symbol[] = []

    /**
     * Need to loop over properties first so we capture any
     * static methods. static methods aren't captured in type.symbol.members
     */
    type.getProperties().forEach(property => {
      // Only add members, don't add non-member properties
      if (this.getCallSignature(property)) {
        methodSymbols.push(property)
      }
    })

    if (type.symbol && type.symbol.members) {
      type.symbol.members.forEach(member => {
        methodSymbols.push(member)
      })
    }

    return methodSymbols
  }

  public getMethodsInfo(type: ts.Type): Method[] {
    const members = this.extractMembersFromType(type)
    const methods: Method[] = []
    members.forEach(member => {
      if (!this.isTaggedPublic(member)) {
        return
      }

      const name = member.getName()
      const docblock = this.getFullJsDocComment(member).fullComment
      const callSignature = this.getCallSignature(member)
      const params = this.getParameterInfo(callSignature)
      const description = ts.displayPartsToString(
        member.getDocumentationComment(this.checker)
      )
      const returnType = this.checker.typeToString(
        callSignature.getReturnType()
      )
      const returnDescription = this.getReturnDescription(member)
      const modifiers = this.getModifiers(member)

      methods.push({
        description,
        docblock,
        modifiers,
        name,
        params,
        returns: returnDescription
          ? {
              description: returnDescription.toString(),
              type: returnType,
            }
          : null,
      })
    })

    return methods
  }

  public getModifiers(member: ts.Symbol) {
    const modifiers: string[] = []
    const flags = ts.getCombinedModifierFlags(member.valueDeclaration)
    const isStatic = (flags & ts.ModifierFlags.Static) !== 0 // tslint:disable-line no-bitwise

    if (isStatic) {
      modifiers.push('static')
    }

    return modifiers
  }

  public getParameterInfo(callSignature: ts.Signature): MethodParameter[] {
    return callSignature.parameters.map(param => {
      const paramType = this.checker.getTypeOfSymbolAtLocation(
        param,
        param.valueDeclaration
      )
      const paramDeclaration = this.checker.symbolToParameterDeclaration(
        param,
        undefined,
        undefined
      )
      const isOptionalParam: boolean = !!(
        paramDeclaration && paramDeclaration.questionToken
      )

      return {
        description:
          ts.displayPartsToString(
            param.getDocumentationComment(this.checker)
          ) || null,
        name: param.getName() + (isOptionalParam ? '?' : ''),
        type: {name: this.checker.typeToString(paramType)},
      }
    })
  }

  public getCallSignature(symbol: ts.Symbol) {
    const symbolType = this.checker.getTypeOfSymbolAtLocation(
      symbol,
      symbol.valueDeclaration!
    )

    return symbolType.getCallSignatures()[0]
  }

  public isTaggedPublic(symbol: ts.Symbol) {
    const jsDocTags = symbol.getJsDocTags()
    return Boolean(jsDocTags.find(tag => tag.name === 'public'))
  }

  public getReturnDescription(symbol: ts.Symbol) {
    const tags = symbol.getJsDocTags()
    const returnTag = tags.find(tag => tag.name === 'returns')
    if (!returnTag) {
      return null
    }

    return returnTag.text || null
  }

  public getDocgenType(propType: ts.Type): PropItemType {
    const propTypeString = this.checker.typeToString(propType)

    if (
      this.shouldExtractLiteralValuesFromEnum &&
      propType.isUnion() &&
      propType.types.every(type => type.isStringLiteral())
    ) {
      return {
        name: 'enum',
        raw: propTypeString,
        value: propType.types
          .map(type => ({
            value: type.isStringLiteral() ? `"${type.value}"` : undefined,
          }))
          .filter(Boolean),
      }
    }

    return {name: propTypeString}
  }

  public getPropsInfo(
    propsObj: ts.Symbol,
    defaultProps: StringIndexedObject<string> = {}
  ): Props {
    if (!propsObj.valueDeclaration) {
      return {}
    }
    const propsType = this.checker.getTypeOfSymbolAtLocation(
      propsObj,
      propsObj.valueDeclaration
    )
    const propertiesOfProps = propsType.getProperties()

    const result: Props = {}

    propertiesOfProps.forEach(prop => {
      const propName = prop.getName()

      // Find type of prop by looking in context of the props object itself.
      const propType = this.checker.getTypeOfSymbolAtLocation(
        prop,
        propsObj.valueDeclaration!
      )

      // tslint:disable-next-line:no-bitwise
      const isOptional = (prop.getFlags() & ts.SymbolFlags.Optional) !== 0

      const jsDocComment = this.findDocComment(prop)
      const hasCodeBasedDefault = defaultProps[propName] !== undefined

      let defaultValue = null

      if (hasCodeBasedDefault) {
        defaultValue = {value: defaultProps[propName]}
      } else if (jsDocComment.tags.default) {
        defaultValue = {value: jsDocComment.tags.default}
      }

      const parent = getParentType(prop)

      result[propName] = {
        defaultValue,
        description: jsDocComment.fullComment,
        name: propName,
        parent,
        required: !isOptional && !hasCodeBasedDefault,
        type: this.getDocgenType(propType),
      }
    })

    return result
  }

  public findDocComment(symbol: ts.Symbol): JSDoc {
    const comment = this.getFullJsDocComment(symbol)
    if (comment.fullComment || comment.tags.default) {
      return comment
    }

    const rootSymbols = this.checker.getRootSymbols(symbol)
    const commentsOnRootSymbols = rootSymbols
      .filter(x => x !== symbol)
      .map(x => this.getFullJsDocComment(x))
      .filter(x => !!x.fullComment || !!comment.tags.default)

    if (commentsOnRootSymbols.length) {
      return commentsOnRootSymbols[0]
    }

    return defaultJSDoc
  }

  /**
   * Extracts a full JsDoc comment from a symbol, even
   * though TypeScript has broken down the JsDoc comment into plain
   * text and JsDoc tags.
   */
  public getFullJsDocComment(symbol: ts.Symbol): JSDoc {
    // in some cases this can be undefined (Pick<Type, 'prop1'|'prop2'>)
    if (symbol.getDocumentationComment === undefined) {
      return defaultJSDoc
    }

    let mainComment = ts.displayPartsToString(
      symbol.getDocumentationComment(this.checker)
    )

    if (mainComment) {
      mainComment = mainComment.replace('\r\n', '\n')
    }

    const tags = symbol.getJsDocTags() || []

    const tagComments: string[] = []
    const tagMap: StringIndexedObject<string> = {}

    tags.forEach(tag => {
      const trimmedText = (tag.text.toString() || '').trim()
      const currentValue = tagMap[tag.name]
      tagMap[tag.name] = currentValue
        ? currentValue + '\n' + trimmedText
        : trimmedText

      if (tag.name !== 'default') {
        tagComments.push(formatTag(tag))
      }
    })

    return {
      description: mainComment,
      fullComment: (mainComment + '\n' + tagComments.join('\n')).trim(),
      tags: tagMap,
    }
  }

  public getFunctionStatement(statement: ts.Statement) {
    if (ts.isFunctionDeclaration(statement)) {
      return statement
    }

    if (ts.isVariableStatement(statement)) {
      const initializer =
        statement.declarationList &&
        statement.declarationList.declarations[0].initializer

      if (
        initializer &&
        (ts.isArrowFunction(initializer) ||
          ts.isFunctionExpression(initializer))
      ) {
        return initializer
      }
    }

    return null
  }

  public extractDefaultPropsFromComponent(
    symbol: ts.Symbol,
    source: ts.SourceFile
  ) {
    let possibleStatements = source.statements
      // ensure that name property is available
      .filter(stmt => !!(stmt as ts.ClassDeclaration).name)
      .filter(
        stmt =>
          this.checker.getSymbolAtLocation(
            (stmt as ts.ClassDeclaration).name!
          ) === symbol
      )

    if (!possibleStatements.length) {
      // if no class declaration is found, try to find a
      // expression statement used in a React.StatelessComponent
      possibleStatements = source.statements.filter(
        stmt => ts.isExpressionStatement(stmt) || ts.isVariableStatement(stmt)
      )
    }

    return possibleStatements.reduce((res, statement) => {
      if (statementIsClassDeclaration(statement) && statement.members.length) {
        const possibleDefaultProps = statement.members.filter(
          member =>
            member.name && getPropertyName(member.name) === 'defaultProps'
        )

        if (!possibleDefaultProps.length) {
          return res
        }

        const defaultProps = possibleDefaultProps[0]
        let initializer = (defaultProps as ts.PropertyDeclaration).initializer
        let properties = (initializer as ts.ObjectLiteralExpression).properties

        while (ts.isIdentifier(initializer as ts.Identifier)) {
          const defaultPropsReference = this.checker.getSymbolAtLocation(
            initializer as ts.Node
          )
          if (defaultPropsReference) {
            const declarations = defaultPropsReference.getDeclarations()

            if (declarations) {
              initializer = (declarations[0] as ts.VariableDeclaration)
                .initializer
              properties = (initializer as ts.ObjectLiteralExpression)
                .properties
            }
          }
        }

        let propMap = {}

        if (properties) {
          propMap = this.getPropMap(
            properties as ts.NodeArray<ts.PropertyAssignment>
          )
        }

        return {
          ...res,
          ...propMap,
        }
      } else if (statementIsStatelessWithDefaultProps(statement)) {
        let propMap = {}
        ;(statement as ts.ExpressionStatement).getChildren().forEach(child => {
          const {right} = child as ts.BinaryExpression
          if (right) {
            const {properties} = right as ts.ObjectLiteralExpression
            if (properties) {
              propMap = this.getPropMap(
                properties as ts.NodeArray<ts.PropertyAssignment>
              )
            }
          }
        })
        return {
          ...res,
          ...propMap,
        }
      }

      const functionStatement = this.getFunctionStatement(statement)

      // Extracting default values from props destructuring
      if (functionStatement && functionStatement.parameters.length) {
        const {name} = functionStatement.parameters[0]

        if (ts.isObjectBindingPattern(name)) {
          return {
            ...res,
            ...this.getPropMap(name.elements),
          }
        }
      }

      return res
    }, {})
  }

  public getLiteralValueFromPropertyAssignment(
    property: ts.PropertyAssignment | ts.BindingElement
  ): string | boolean | number | null | undefined {
    let {initializer} = property

    // Shorthand properties, so inflect their actual value
    if (!initializer) {
      if (ts.isShorthandPropertyAssignment(property)) {
        const symbol = this.checker.getShorthandAssignmentValueSymbol(property)
        const decl =
          symbol && (symbol.valueDeclaration as ts.VariableDeclaration)

        if (decl && decl.initializer) {
          initializer = decl.initializer!
        }
      }
    }

    if (!initializer) {
      return undefined
    }

    // Literal values
    switch (initializer.kind) {
      case ts.SyntaxKind.FalseKeyword:
        return this.savePropValueAsString ? 'false' : false
      case ts.SyntaxKind.TrueKeyword:
        return this.savePropValueAsString ? 'true' : true
      case ts.SyntaxKind.StringLiteral:
        return (initializer as ts.StringLiteral).text.trim()
      case ts.SyntaxKind.PrefixUnaryExpression:
        return this.savePropValueAsString
          ? initializer.getFullText().trim()
          : Number((initializer as ts.PrefixUnaryExpression).getFullText())
      case ts.SyntaxKind.NumericLiteral:
        return this.savePropValueAsString
          ? `${(initializer as ts.NumericLiteral).text}`
          : Number((initializer as ts.NumericLiteral).text)
      case ts.SyntaxKind.NullKeyword:
        return this.savePropValueAsString ? 'null' : null
      case ts.SyntaxKind.Identifier:
        // can potentially find other identifiers in the source and map those in the future
        return (initializer as ts.Identifier).text === 'undefined'
          ? 'undefined'
          : null
      case ts.SyntaxKind.PropertyAccessExpression:
      case ts.SyntaxKind.ObjectLiteralExpression:
      default:
        try {
          return initializer.getText()
        } catch (e) {
          return null
        }
    }
  }

  public getPropMap(
    properties: ts.NodeArray<ts.PropertyAssignment | ts.BindingElement>
  ): StringIndexedObject<string | boolean | number | null> {
    return properties.reduce((acc, property) => {
      if (ts.isSpreadAssignment(property) || !property.name) {
        return acc
      }

      const literalValue = this.getLiteralValueFromPropertyAssignment(property)
      const propertyName = getPropertyName(property.name)

      if (
        (typeof literalValue === 'string' ||
          typeof literalValue === 'number' ||
          typeof literalValue === 'boolean' ||
          literalValue === null) &&
        propertyName !== null
      ) {
        acc[propertyName] = literalValue
      }

      return acc
    }, {} as StringIndexedObject<string | boolean | number | null>)
  }
}
