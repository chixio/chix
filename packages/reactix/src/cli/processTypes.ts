import {NodeDefinition} from '@chix/common'
import {deriveDependencyFromPath} from './deriveDependencyFromPath'
import {deriveNameFromPath} from './deriveNameFromPath'
import {ComponentDoc} from './parsers/ts'
import {processDefault} from './processDefault'
import {processMethods} from './processMethods'
import {processType} from './processType'

export function processTypes(
  base: string,
  ns: string,
  path: string,
  component: ComponentDoc,
  scriptType: 'ts' | 'js'
): NodeDefinition {
  let name = component.displayName

  console.log(`Processing ${component.displayName} ${component}`)

  let dependencies
  if (scriptType === 'ts') {
    dependencies = {
      npm: {
        [`${ns}#${name}`]: 'latest',
      },
    }
  } else {
    if (!name) {
      name = deriveNameFromPath(path)
    }
    dependencies = deriveDependencyFromPath(base, ns, path, scriptType)
  }

  const nodeDefinition = {
    ns,
    type: 'ReactNode',
    dependencies,
    name,
    // for props, parent.name is better as it's the component name.
    ports: {
      input: {},
      output: {
        component: {
          title: name,
          type: 'Component',
        },
      },
    },
  }

  // remember that component was the origin and nodeDefinition got build on top.

  if (component.props) {
    for (const [propName, prop] of Object.entries(component.props)) {
      if (!prop.type) {
        console.error(
          `Property '${propName}' of ${nodeDefinition.ns}/${nodeDefinition.name} has no type:\n\n
\tPlease fix the propTypes definition.\n`
        )
        prop.type = {name: 'any'}
      }

      let type = processType(prop.type)

      nodeDefinition.ports.input[propName] = {}

      if (type === 'ReactNode[]') {
        type = 'children' // treated special in ReactNode

        // very generic test if type is function. e.g. () => void etc.
      } else if (type === 'function' || type[0] === '(') {
        nodeDefinition.ports.output[propName] = {
          type: 'any', // depends on callback/handler signature
        }

        // type itself becomes a simple true or false whether to enable this handler
        type = 'boolean'
        nodeDefinition.ports.input[propName].title = `Enable ${propName}`
      } else if (prop.type.name === 'enum') {
        type = 'string'

        nodeDefinition.ports.input[propName].enum = prop.type.value.map(item =>
          item.value.replace(/"/g, '')
        )
      } else if (type.endsWith('[]')) {
        type = 'array'
      } else if (type.startsWith('{')) {
        type = 'any'
      } else if (type.replace(/\W/g, '') !== type) {
        // force anything which is not word like to type any
        type = 'any'
      } else if (type === 'Booleanish') {
        type = 'boolean'
      }

      if (propName === 'children') {
        nodeDefinition.ports.input[propName].type = 'array'
      } else {
        nodeDefinition.ports.input[propName].type = type
      }
      nodeDefinition.ports.input[propName].name = propName

      if (prop.defaultValue) {
        nodeDefinition.ports.input[propName].default = processDefault(
          type,
          prop.defaultValue
        )
      } else {
        if (prop.hasOwnProperty('required')) {
          nodeDefinition.ports.input[propName].required = prop.required
        } else {
          nodeDefinition.ports.input[propName].required = false
        }
      }

      // give propType as hint
      // if (type !== prop.type.name) {
      //  nodeDefinition.ports.input[propName].propType = prop.type.name
      // }

      if (prop.description) {
        nodeDefinition.ports.input[propName].description = prop.description
      }
    }
  }

  if (component.methods) {
    // not used yet
    processMethods(component, nodeDefinition)
  }

  return nodeDefinition
}
