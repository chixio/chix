import {readFileSync} from 'fs-extra'
import * as path from 'path'
import * as parser from 'react-docgen'
import {IComponentDefinitionResolver, ParseResult} from '../types'
import {ComponentDoc, parse as parseTs} from './parsers/ts'

export function parse(
  resolver: IComponentDefinitionResolver,
  filePath: string
): ParseResult {
  const source = readFileSync(filePath).toString()

  const ext = path.extname(filePath)

  let result: ComponentDoc[]
  let type: 'ts' | 'js'

  if (ext === '.ts' || ext === '.tsx') {
    type = 'ts'
    result = parseTs(path.join(process.cwd(), filePath), {
      shouldExtractLiteralValuesFromEnum: true,
    })
  } else if (ext === '.js' || ext === '.jsx') {
    throw Error('not supported')
    type = 'js'
    result = [parser.parse(source, resolver)]
  } else {
    throw Error(`Don't know how to handle file with extension ${ext}`)
  }

  return {
    result,
    type,
  }
}
