// ignore computed

export type DefaultValue = {
  value: string
}

export function processDefault(type: string, defaultValue: DefaultValue) {
  if (defaultValue.value === "''") {
    switch (type) {
      case 'object':
        return {}
      case 'string':
        return ''
      case 'enum':
        return ''
      case 'number':
        return 0
      case 'function':
        return ''
      case 'boolean':
        return false
      case 'array':
        return []
    }
    return null
  }

  try {
    return JSON.parse(defaultValue.value)
  } catch (err) {
    console.log(`Error parsing:  ${defaultValue.value}`)
    return null
  }
}
