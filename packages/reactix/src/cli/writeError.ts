export function writeError(message: string | Error, filePath?: string) {
  if (filePath) {
    process.stderr.write(`Error with path ${filePath}:`)
  }
  process.stderr.write(`${message}\n`)
  if (message instanceof Error) {
    process.stderr.write(message.stack + '\n')
  }
}
