import * as dir from 'node-dir'
import {IComponentDefinitionResolver} from '../types'
import {exitWithError} from './exitWithError'
import {parse} from './parse'
import {writeError} from './writeError'

export function traverseDir(
  filePath: string,
  extensions: RegExp,
  excludePatterns: string[],
  ignoreDir: string[],
  resolver: IComponentDefinitionResolver,
  result: any,
  done: () => void
) {
  dir.readFiles(
    filePath,
    {
      match: extensions,
      exclude: excludePatterns,
      excludeDir: ignoreDir,
    },
    (
      error: any,
      _content: string | Buffer,
      filename: string,
      next: () => void
    ): void => {
      if (error) {
        exitWithError(error)
      }
      try {
        result[filename] = parse(resolver, filename)
      } catch (error) {
        writeError(error.toString(), filename)
      }
      next()
    },
    (error: Error | null) => {
      if (error) {
        writeError(error)
      }
      done()
    }
  )
}
