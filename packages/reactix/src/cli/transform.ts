import {NodeDefinition} from '@chix/common'
import {ParseResultMap} from '../types'
import {processTypes} from './processTypes'

export function transform(
  base: string,
  ns: string,
  resultMap: ParseResultMap,
  options: any = {}
): NodeDefinition[] {
  console.log(`Processing components...`)

  const nodeDefinitions = []
  for (const [path, {result, type}] of Object.entries(resultMap)) {
    for (const component of result) {
      const nodeDefinition = processTypes(base, ns, path, component, type)

      if (
        options.forceChildren &&
        !nodeDefinition.ports.input.hasOwnProperty('children')
      ) {
        ;(nodeDefinition.ports.input as any).children = {
          type: 'array',
          required: false,
        }
      }

      nodeDefinitions.push(nodeDefinition)

      console.log('%s:%s', nodeDefinition.ns, nodeDefinition.name)
    }
  }

  console.log(`Processed ${nodeDefinitions.length} components`)

  return nodeDefinitions
}
