// name is sub path minus file extension (most flexible)
export function deriveNameFromPath(path: string) {
  return path
    .replace(/\.\w+$/, '')
    .split('/')
    .slice(3)
    .join('/')
}
