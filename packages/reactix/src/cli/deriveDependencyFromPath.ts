import {NodeDependencies} from '@chix/common'

export function deriveDependencyFromPath(
  base: string,
  ns: string,
  path: string,
  type: 'ts' | 'js'
): NodeDependencies {
  path = path.replace(base, '').replace(/\.\w+$/, '')

  const segments = path.split('/').slice(1)
  let requirePath: string

  if (type === 'ts') {
    requirePath = [ns, segments.pop()].join('#')
  } else {
    requirePath = [ns, segments.pop()].join('/')
  }

  return {
    npm: {
      [requirePath]: 'latest',
    },
  }
}
