import {writeError} from './writeError'

export function exitWithError(error: string | Error) {
  writeError(error)
  process.exit(1)
}
