import {NodeDefinition} from '@chix/common'
import * as fs from 'fs'
import * as yaml from 'js-yaml'
import mkdirp from 'mkdirp'
import * as path from 'path'

export function writeNodes(
  targetDir: string,
  components: NodeDefinition[],
  options: any = {}
) {
  const promises: Promise<string>[] = []

  const seen: any = {}
  components.forEach((node) => {
    // if MyApp/MyApp name is MyApp
    // if MyApp/MyAppList name is MyApp list
    // so basically the name is the last in path
    // const dir = path.join(process.cwd(), 'nodes', node.ns, node.name);
    console.log('Target Dir', targetDir, node)
    const dir = path.join(targetDir, node.ns, node.name)

    // strange, seems to be duplicate, not different nodes.
    if (Object.keys(seen).includes(dir)) {
      console.error(`Already processed ${dir} skipping`)
      seen[dir]++
    } else {
      seen[dir] = 0
    }

    if (seen[dir] === 0) {
      promises.push(
        new Promise(async (resolve, reject) => {
          try {
            await mkdirp(dir)
            fs.writeFile(
              path.join(dir, options.yaml ? `node.yml` : `node.json`),
              options.asYaml ? yaml.dump(node) : JSON.stringify(node, null, 2),
              (error2) => {
                if (error2) {
                  return reject(error2)
                }

                resolve(dir)
              }
            )
          } catch (error) {
            return reject(error)
          }
        })
      )
    }
  })

  return Promise.all(promises)
}
