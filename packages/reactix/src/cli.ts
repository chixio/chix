// tslint:disable:no-var-requires
import {readJSONSync} from 'fs-extra'
import {glob} from 'glob'
import * as path from 'path'
import {writeError} from './cli/'
import {parse as parseTs} from './cli/parsers/ts'
import {transform} from './cli/transform'
import {writeNodes} from './cli/writeNodes'
import {ParseResultMap} from './types'

const pkg = readJSONSync(path.join(__dirname, '../package.json'))

/**
 * We read from the source however material-ui's package
 * has the files in the root.
 *
 * This can be different per package, so must create a universal solution.
 * Probably by having a build.json per lib which is capable of defining
 * these filters.
 *
 * @param path
 * @returns {*}
 */
const argv = require('nomnom')
  .script(pkg.name)
  .help(
    'Extract meta information from React components.\n' +
      'If a directory is passed, it is recursively traversed.'
  )
  .options({
    ns: {
      help: 'Namespace to use: e.g. react-test',
    },
    path: {
      position: 0,
      help: 'A component file or directory. If no path is provided it reads from stdin.',
      metavar: 'PATH',
      list: true,
    },
    out: {
      abbr: 'o',
      help: 'target directory',
      metavar: 'FILE',
    },
    pretty: {
      help: 'pretty print JSON',
      flag: true,
    },
    yaml: {
      abbr: 'y',
      help: 'save nodes as yaml',
      flag: true,
      default: true,
    },
    forceChildren: {
      abbr: 'c',
      help: 'force each node to have a children prop',
      flag: true,
      default: false,
    },
    extension: {
      abbr: 'x',
      help: 'File extensions to consider. Repeat to define multiple extensions. Default:',
      list: true,
      default: ['js', 'jsx'],
    },
    excludePatterns: {
      abbr: 'e',
      full: 'exclude',
      help: 'Filename pattern to exclude. Default:',
      list: true,
      default: [],
    },
    ignoreDir: {
      abbr: 'i',
      full: 'ignore',
      help: 'Folders to ignore. Default:',
      list: true,
      default: ['node_modules', '__tests__'],
    },
    resolver: {
      help: 'Resolver name (findAllComponentDefinitions, findExportedComponentDefinition) or path to a module that exports a resolver.',
      metavar: 'RESOLVER',
      default: 'findExportedComponentDefinition',
    },
  })
  .parse()

const targetDir = argv.out || path.join(process.cwd(), '/nodes')
const ns = argv.ns
const inputPaths = argv.path || []
/*
const ignoreDir = argv.ignoreDir
const excludePatterns = argv.excludePatterns
*/
// let resolver: IComponentDefinitionResolver

if (!ns) {
  console.error('ns is required')
  process.exit(1)
}

/*
if (argv.resolver) {
  try {
    // Look for built-in resolver
    resolver = require(`react-docgen/dist/resolver/${argv.resolver}`).default
  } catch (e) {
    if (e.code !== 'MODULE_NOT_FOUND') {
      throw e
    }
    const resolverPath = path.resolve(process.cwd(), argv.resolver)
    try {
      // Look for local resolver
      resolver = require(resolverPath)
    } catch (e) {
      if (e.code !== 'MODULE_NOT_FOUND') {
        throw e
      }
      exitWithError(
        `Unknown resolver: "${argv.resolver}" is neither a built-in resolver ` +
          `nor can it be found locally ("${resolverPath}")`
      )
    }
  }
}
*/

if (inputPaths.length === 0) {
  throw Error('Path required')
} else {
  ;(async () => {
    const patterns = inputPaths.map((filePath) =>
      path.join(`${filePath}/**/*.tsx`)
    )

    for (const pattern of patterns) {
      const result: ParseResultMap = {}

      const files = await glob(pattern)

      // program.getSource is not finding the source.

      for (const filePath of files) {
        try {
          console.log('FilePath', filePath)
          result[filePath] = {
            result: parseTs(filePath, {
              shouldExtractLiteralValuesFromEnum: true,
            }),
            type: 'ts',
          }
          // result[filePath] = parse(resolver, filePath)
        } catch (error) {
          writeError(error.toString(), filePath)
        }
      }

      const resultsPaths = Object.keys(result)

      if (resultsPaths.length === 0) {
        process.exit(1)
      }

      console.log('Full result', result)

      const nodeDefinitions = transform(pattern, ns, result, argv)

      await writeNodes(targetDir, nodeDefinitions, argv)
    }
  })()
}
