import {ASTNode, NodePath} from 'ast-types'
import {ComponentDoc} from './cli/parsers/ts'

export type IComponentDefinitionResolver = (
  ast: ASTNode
) => typeof NodePath | void

export interface ParseResult {
  result: ComponentDoc[]
  type: 'ts' | 'js'
}

export interface ParseResultMap {
  [path: string]: ParseResult
}
