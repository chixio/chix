import * as acorn from 'acorn-jsx'

const code = `
<Toolbar>
  <ToolbarGroup firstChild={true}>
    <DropDownMenu value={this.state.value} onChange={this.handleChange}>
      <MenuItem value={1} primaryText="All Broadcasts" />
      <MenuItem value={2} primaryText="All Voice" />
      <MenuItem value={3} primaryText="All Text" />
      <MenuItem value={4} primaryText="Complete Voice" />
      <MenuItem value={5} primaryText="Complete Text" />
      <MenuItem value={6} primaryText="Active Voice" />
      <MenuItem value={7} primaryText="Active Text" />
    </DropDownMenu>
  </ToolbarGroup>
  <ToolbarGroup>
    <ToolbarTitle text="Options" />
    <FontIcon className="muidocs-icon-custom-sort" />
    <ToolbarSeparator />
    <RaisedButton label="Create Broadcast" primary={true} />
    <IconMenu
      iconButtonElement={
        <IconButton touch={true}>
          <NavigationExpandMoreIcon />
        </IconButton>
      }
    >
      <MenuItem primaryText="Download" />
      <MenuItem primaryText="More Info" />
    </IconMenu>
  </ToolbarGroup>
</Toolbar>
`

const ast = acorn.parse(code, {
  plugins: {jsx: true},
})

interface CloseAt {
  name: string
  idx?: number
  at: number
}

const closeAt: CloseAt[] = []

const getCloseUs = (pos: number) => {
  return closeAt
    .sort((left, right) => {
      return left.at - right.at
    })
    .filter((me, idx) => {
      me.idx = idx
      return me.at < pos
    })
}

const parseIt = item => {
  if (item.type === 'JSXElement') {
    if (item.openingElement) {
      const closeUs = getCloseUs(item.openingElement.start)

      if (closeUs.length) {
        closeUs.forEach(me => {
          console.log('closed %s', me.name)
          if (!me.idx) {
            throw Error('idx not set')
          }
          closeAt.splice(me.idx, 1)
        })
      }

      console.log('opened', item.openingElement.name.name)
      if (item.closingElement) {
        closeAt.push({
          name: item.closingElement.name.name,
          at: item.closingElement.start,
        })
      }
      // else self closing
    }

    if (item.children) {
      item.children.forEach(_item => {
        parseIt(_item)
      })
    }
  }
}

parseIt(ast.body[0].expression)

console.log(JSON.stringify(ast.body[0].expression, null, 2))
