// tslint:disable only-arrow-functions object-literal-sort-keys no-implicit-dependencies
import * as chai from 'chai'
// import * as path from 'path'
import {parse} from '../cli/parsers/ts'

describe('parse', () => {
  it('should be able to parse react class', () => {
    const result = parse(
      '../../../../repos/antd/ant-design/components/breadcrumb/Breadcrumb.tsx'
      /*
      {
        shouldExtractLiteralValuesFromEnum: true,
      }
      */
    )

    // the react component should be detected.
    // and if it's detected it's always that class or function name.

    console.log(JSON.stringify(result, null, 2))

    chai.expect(result.length).to.eql(1)
    chai.expect(result[0].displayName).to.eql('Alert')
  })
})
