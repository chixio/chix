'use strict'

const Mustache = require('mustache')
const truncate = require('dots')
const fs = require('fs')
const path = require('path')

const tpls = {
  simple: fs.readFileSync(path.join(__dirname, '/templates/simple.tpl'), 'utf8'),
  compact: fs.readFileSync(path.join(__dirname, '/templates/compact.tpl'), 'utf8')
}

function escape (val) {
  if (val) {
    if (typeof val === 'string') {
      return JSON.stringify(val).replace(/^\"|\"$/g, "'")
    } else if (typeof val === 'object') {
      return JSON.stringify(val).replace(/"/g, '\\"')
    }
  }
}

// :start port
function fixPort (port) {
  return port.replace(':', '')
}

function getInputPorts (node, map, input) {
  const inputs = []
  const known = []
  map.links.forEach(function (link) {
    if (link.target.id === node.id &&
      known.indexOf(fixPort(link.target.port)) === -1) {
      inputs.push(
        '<INPUT_' + fixPort(link.target.port) + '>' + fixPort(link.target.port)
      )
      known.push(fixPort(link.target.port))
    }
  })

  input.forEach(function (iip) {
    if (iip.target.id === node.id &&
      known.indexOf(fixPort(iip.port)) === -1) {
      inputs.push('<INPUT_' + fixPort(iip.port) + '>' + fixPort(iip.port))
      known.push(fixPort(iip.port))
    }
  })

  return inputs
}

function getOutputPorts (node, map, output) {
  const outputs = []
  const known = []
  map.links.forEach(function (link) {
    if (link.source.id === node.id &&
      known.indexOf(fixPort(link.source.port)) === -1) {
      outputs.push(
        '<OUTPUT_' + fixPort(link.source.port) + '>' + fixPort(link.source.port)
      )
      known.push(fixPort(link.source.port))
    }
  })

  output.forEach(function (out) {
    if (out.source.id === node.id &&
      known.indexOf(fixPort(out.port)) === -1) {
      outputs.push('<OUTPUT_' + fixPort(out.port) + '>' + fixPort(out.port))
      known.push(fixPort(out.port))
    }
  })

  return outputs
}

/**
 *
 * Create a dot digraph definition from a flow.
 *
 * @param {map} map
 * @return {string}
 * @api public
 */
function ToDot (map, input, template) {
  const iips = []
  const outs = []
  let externalPort
  let i
  template = template || 'simple'

  if (input) {
    input.forEach(function (iip, i) {
      iips.push({
        idx: i,
        process: iip.process,
        target: iip.target,
        port: fixPort(iip.port),
        data: escape(truncate(iip.data, 30)),
        index: iip.target.settings && iip.target.settings.index
          ? iip.target.settings.index
          : ''
      // context: iip.context
      })
    })

    // generate example external port names as input to the graph
    i = input.length
  }

  if (map.ports) {
    for (externalPort in map.ports.input) {
      // nodeId name
      // are treated as IIPs
      iips.push({
        idx: i++,
        target: {
          id: map.ports.input[externalPort].nodeId
        },
        process: map.ports.input[externalPort].nodeId,
        port: fixPort(map.ports.input[externalPort].name),
        data: '<' + externalPort + '>',
        index: map.ports.input[externalPort].index
      })
    }

    // generate example external port names as output to the graph
    i = 0
    for (externalPort in map.ports.output) {
      outs.push({
        idx: i++,
        source: {
          id: map.ports.output[externalPort].nodeId
        },
        process: map.ports.output[externalPort].nodeId,
        port: fixPort(map.ports.output[externalPort].name),
        data: '<' + externalPort + '>',
        index: map.ports.output[externalPort].index
      })
    }
  }

  // view object
  const view = {
    title: map.title,
    description: escape(map.description),
    nodes: [],
    links: [],
    iips: iips,
    outs: outs
  }

  const node_map = {}

  map.nodes.forEach(function (node) {
    const id = 'x' + node.id.split('-').pop()
    const inputPorts = getInputPorts(node, map, iips)
    const outputPorts = getOutputPorts(node, map, outs)
    const n = {
      name: id,
      description: escape(node.description),
      label: node.title || node.ns + '::' + node.name,
      fn: node.fn
    }

    var node_string = []
    if (inputPorts.length) {
      n.inputs = '{' + inputPorts.join('|') + '}'
      node_string.push(n.inputs)
    }

    node_string.push(n.label)

    n.outputs = ''
    if (outputPorts.length) {
      n.outputs = '{' + outputPorts.join('|') + '}'
      node_string.push(n.outputs)
    }

    n.node = '{' + node_string.join('|') + '}'

    view.nodes.push(n)

    node_map[node.id] = id
  })

  map.links.forEach(function (link) {
    const ss = link.source.setting || {}
    const ts = link.target.setting || {}
    view.links.push({
      label: link.source.port + '  ' + link.target.port,
      source: node_map[link.source.id],
      source_idx: ss.index ? ' [' + ss.index + ']' : '',
      source_port: fixPort(link.source.port),
      in: fixPort(link.target.port),
      out: fixPort(link.source.port),
      target: node_map[link.target.id],
      target_idx: ts.index ? '[' + ts.index + '] ' : '',
      target_port: fixPort(link.target.port)
    })
  })

  return Mustache.render(tpls[template], view)
}

module.exports = ToDot
