export function escape(value: string | object): string {
  if (value) {
    if (typeof value === 'string') {
      return JSON.stringify(value).replace(/^\"|\"$/g, "'")
    } else if (typeof value === 'object') {
      return JSON.stringify(value).replace(/"/g, '\\"')
    }
  }

  return ''
}
