// :start port
export function fixPort(port: string) {
  return port.replace(':', '')
}
