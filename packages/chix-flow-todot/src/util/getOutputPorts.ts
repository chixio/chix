import {Flow, Link, Node} from '@chix/common'
import {ViewIIP} from '../types'
import {fixPort} from './fixPort'

export function getOutputPorts(node: Node, map: Flow, output: ViewIIP[]) {
  const outputs: string[] = []
  const known: string[] = []

  map.links.forEach((link: Link) => {
    if (
      link.source &&
      link.source.id === node.id &&
      known.indexOf(fixPort(link.source.port)) === -1
    ) {
      outputs.push(
        '<OUTPUT_' + fixPort(link.source.port) + '>' + fixPort(link.source.port)
      )
      known.push(fixPort(link.source.port))
    }
  })

  output.forEach((out) => {
    if (out.source.id === node.id && known.indexOf(fixPort(out.port)) === -1) {
      outputs.push('<OUTPUT_' + fixPort(out.port) + '>' + fixPort(out.port))
      known.push(fixPort(out.port))
    }
  })

  return outputs
}
