import {Flow, Link, Node} from '@chix/common'
import {ViewIIP} from '../types'
import {fixPort} from './fixPort'

export function getInputPorts(node: Node, map: Flow, input: ViewIIP[]) {
  const inputs: string[] = []
  const known: string[] = []

  map.links.forEach((link: Link) => {
    if (
      link.target &&
      link.target.id === node.id &&
      known.indexOf(fixPort(link.target.port)) === -1
    ) {
      inputs.push(
        '<INPUT_' + fixPort(link.target.port) + '>' + fixPort(link.target.port)
      )
      known.push(fixPort(link.target.port))
    }
  })

  input.forEach((iip) => {
    if (iip.target.id === node.id && known.indexOf(fixPort(iip.port)) === -1) {
      inputs.push('<INPUT_' + fixPort(iip.port) + '>' + fixPort(iip.port))
      known.push(fixPort(iip.port))
    }
  })

  return inputs
}
