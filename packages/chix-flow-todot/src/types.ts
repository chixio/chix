export type ViewNode = {
  name: string
  description: string
  label: string
  fn?: string
  inputs: string
  outputs: string
  node?: string
}

export type IIP = {
  process: string
  target: {
    id: string
    settings?: {
      index: number
    }
  }
  port: string
  data: string
}

export type ViewIIP = {
  idx: number
  process: string
  target: {
    id: string
  }
  source: {
    id: string
  }
  port: string
  data: string
  index: string
}

export type ViewLink = {
  label: string
  source: string
  source_idx: string
  source_port: string
  in: string
  out: string
  target: string
  target_idx: string
  target_port: string
}

export type View = {
  title: string
  description: string
  nodes: ViewNode[]
  links: ViewLink[]
  iips: ViewIIP[]
  outs: ViewIIP[]
}
