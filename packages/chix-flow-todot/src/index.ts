import {ExternalPort, Flow, Node} from '@chix/common'
import {forOf} from '@fbpx/lib'
import * as truncate from 'dots'
import * as Mustache from 'mustache'
import {templates} from './templates'
import {IIP, View, ViewIIP, ViewLink, ViewNode} from './types'
import {escape} from './util/escape'
import {fixPort} from './util/fixPort'
import {getInputPorts} from './util/getInputPorts'
import {getOutputPorts} from './util/getOutputPorts'
export * from './types'

/**
 * Create a dot digraph definition from a flow.
 *
 * @param {Flow} flow
 * @param {IIP[]} input
 * @param {string} template
 * @returns {string}
 * @constructor
 */
export function ToDot(flow: Flow, input: IIP[], template: string = 'simple') {
  const iips: ViewIIP[] = []
  const outs: ViewIIP[] = []
  let i = 0

  if (input) {
    input.forEach((iip, index) => {
      iips.push({
        idx: index,
        process: iip.process,
        target: iip.target,
        port: fixPort(iip.port),
        data: escape(truncate(iip.data, 30)),
        index:
          iip.target.settings && iip.target.settings.index
            ? `${iip.target.settings.index}`
            : '',
        // context: iip.context
      } as ViewIIP)
    })

    // generate example external port names as input to the graph
    i = input.length
  }

  if (flow.ports) {
    if (flow.ports.input) {
      forOf((externalPort: string, port: ExternalPort) => {
        // nodeId name
        // are treated as IIPs
        iips.push({
          idx: i++,
          target: {
            id: port.nodeId,
          },
          process: port.nodeId,
          port: fixPort(port.name),
          data: '<' + externalPort + '>',
          index: port.index || '',
        } as ViewIIP)
      }, flow.ports.input)
    }

    // generate example external port names as output to the graph
    i = 0

    if (flow.ports.output) {
      forOf((externalPort: string, port: ExternalPort) => {
        outs.push({
          idx: i++,
          source: {
            id: port.nodeId,
          },
          process: port.nodeId,
          port: fixPort(port.name),
          data: '<' + externalPort + '>',
          index: port.index || '',
        } as ViewIIP)
      }, flow.ports.output)
    }
  }

  // view object
  const view: View = {
    title: flow.title || '',
    description: flow.description ? escape(flow.description) : '',
    nodes: [],
    links: [],
    iips,
    outs,
  }

  const node_map: {[key: string]: string} = {}

  // seems to be incorrect, node doesn't contain the full description
  flow.nodes.forEach((node: Node) => {
    const id = 'x' + node.id.split('-').pop()
    const inputPorts = getInputPorts(node, flow, iips)
    const outputPorts = getOutputPorts(node, flow, outs)
    const viewNode: ViewNode = {
      name: id,
      description: node.description ? escape(node.description) : '',
      label: node.title || node.ns + '::' + node.name,
      inputs: '',
      outputs: '',
      // fn: node.fn
    }

    const node_string: string[] = []

    if (inputPorts.length) {
      viewNode.inputs = '{' + inputPorts.join('|') + '}'
      node_string.push(viewNode.inputs)
    }

    node_string.push(viewNode.label)

    viewNode.outputs = ''
    if (outputPorts.length) {
      viewNode.outputs = '{' + outputPorts.join('|') + '}'
      node_string.push(viewNode.outputs)
    }

    viewNode.node = '{' + node_string.join('|') + '}'

    view.nodes.push(viewNode)

    node_map[node.id] = id
  })

  flow.links.forEach((link) => {
    const {source, target} = link

    if (!source) {
      throw Error('Link source not set.')
    }

    if (!target) {
      throw Error('Link target not set.')
    }

    const sourceSetting = source.setting || {}
    const targetSetting = target.setting || {}

    view.links.push({
      label: source.port + '  ' + target.port,
      source: node_map[source.id],
      source_idx: sourceSetting.index ? ' [' + sourceSetting.index + ']' : '',
      source_port: fixPort(source.port),
      in: fixPort(target.port),
      out: fixPort(source.port),
      target: node_map[target.id],
      target_idx: targetSetting.index ? '[' + targetSetting.index + '] ' : '',
      target_port: fixPort(target.port),
    } as ViewLink)
  })

  return Mustache.render(templates[template], view)
}
