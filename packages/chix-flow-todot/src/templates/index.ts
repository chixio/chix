import * as fs from 'fs'
import * as path from 'path'

export const simple = fs.readFileSync(
  path.join(__dirname, './simple.tpl'),
  'utf8'
)
export const compact = fs.readFileSync(
  path.join(__dirname, './compact.tpl'),
  'utf8'
)
export const templates: {[key: string]: string} = {
  simple,
  compact,
}
