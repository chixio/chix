digraph structs {
  label="{{title}}";

  node [shape=record];

  {{#nodes}}
  {{name}} [shape="record",label="{{&node}}",comment="{{this.description}}"];
  {{/nodes}}

  {{#links}}
  {{source}}:{{source_port}} -> {{target}}:{{target_port}};
  {{/links}}

  {{#iips}}
  xIIP{{idx}} [shape="box",label="{{&data}}",comment="{{this.description}}"];
  xIIP{{idx}} -> x{{process}}:{{port}};
  {{/iips}}

}
