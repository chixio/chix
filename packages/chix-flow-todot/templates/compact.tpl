digraph structs {
  label="{{title}}";

  node [shape=record];

  {{#nodes}}
  {{name}} [shape="record", label="{{&node}}",comment="{{this.description}}"];
  {{/nodes}}

  {{#links}}
  {{source}}:OUTPUT_{{source_port}} -> {{target}}:INPUT_{{target_port}};
  {{/links}}

  {{#iips}}
  xIIP{{idx}} [shape="plaintext",label="{{&data}}",comment="{{this.description}}"];
  xIIP{{idx}} -> x{{process}}:INPUT_{{port}};
  {{/iips}}

}
