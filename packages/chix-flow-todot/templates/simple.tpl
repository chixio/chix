digraph {
  label="{{title}}";
  {{#nodes}}
  {{name}} [shape="box",label="{{&label}}",comment="{{this.description}}"];
  {{/nodes}}
  {{#links}}
  {{source}} -> {{target}} [samehead="{{target}}{{out}}",sametail="{{source}}{{in}}",arrowtail="dot",arrowhead="dot",label="{{out}}{{source_idx}} →  {{target_idx}}{{in}}"];
  {{/links}}
  {{#iips}}
  xIIP{{idx}} [shape="box",label="{{data}}",comment="{{this.description}}"];
  xIIP{{idx}} -> x{{process}} [samehead="{{process}}{{port}}",sametail="xIIP{{idx}}",arrowtail="dot",arrowhead="dot",label="IIP →  {{index}}{{port}}"];
  {{/iips}}
}
