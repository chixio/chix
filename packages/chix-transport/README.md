Chiχ Transport
====

[![Build Status](https://travis-ci.org/psichi/chix-transport.png)](https://travis-ci.org/psichi/chix-transport)

General purpose transporter.

It's job is:

  - receiving
  - sending
  - keep track of client scope.

Includes several transports:

 - websocket
 - websocket browser (client)
 - websocket client (server)
 - websocket proxy (server)
 - chrome (unfinished)
 - window (unfinished)
 - dummy

These transports will only accept input and output as defined by the provided (JSON) schemas.

When messages are received their payload will be received and emitted, it's then up to the listener
what to do with these messages.

When the listener has done it's logic it can choose to send back the data by using the
 send() method of the transport(s).

 Schema:

 Both input & output messages have the below structure:

 ```JSON
  {
    protocol: 'your-protocol',
    command: '<action>'
    payload: ...data
  }
 ```

 The schema's must define which commands are available and what their payload structure
 will look like, this is done by creating json schema's for them.

## Examples:

 All transport requires a set of schemas.
### Schemas

Default set of schemas is available through [chix-runtime/schemas](https://github.com/psichi/chix-runtime)

```js
const schemas = require('chix-runtime/schemas')
````

These define the valid incoming and outgoing messages, any unknown/invalid messages will send out an error message.

> Note: The `role` will determine what is considered incoming and outgoing.

### Dummy
 ```js
import DummyTransport from 'chix-transport/dummy'
const transport = new DummyTransport({
  schemas: schemas
})

const connection = 'fake-conn'

// incoming
transport.receive({
  payload: { bogus: 'data' }
}, connection)

// outgoing
transport.reply('protocol', 'command', {
  bogus: 'data'
}, connection)

 ```

### Websocket

Websocket server

 ```js
import WebSocketTransport from 'chix-transport/websocket'
import http from 'http'

const port = 8000

const server = http.createServer((req, res) => res.send('You are being served'))

const options = {
  protocol: 'noflo',
  schemas: schemas,
  secure: true
}

const transport = new WebsocketTransport(options, server)

server.listen(port, () => {
  console.log('Listening on port %s', port)
})
 ```

### Websocket Browser

In-browser server/client

 ```js
import WebSocketBrowser from 'chix-transport/websocketBrowser'

const options = {
  protocol: 'noflo',
  schemas: schemas,
  secure: true,
  host: 'www.example.com',
  port: 8000,
  mount: '',
  secret: '123456', // client mode requires secret
  role: 'client'    // 'server' or 'client' mode
}

const transport = new WebsocketBrowser(options, server)
 ```

### Websocket Client

 Server side client

 ```js
import WebSocketClient from 'chix-transport/websocketClient'

const options = {
  protocol: 'noflo',
  schemas: schemas,
  secure: true,
  host: 'www.example.com',
  port: 8000,
  mount: '',
  secret: '123456', // client mode requires secret
  role: 'client'    // 'server' or 'client' mode
}

const transport = new WebsocketClient(options, server)
 ```

### Websocket Proxy

Proxy requests by being both client and server

 ```js
import WebSocketProxy from 'chix-transport/websocketProxy'
import http from 'http'

const port = 8000

const server = http.createServer((req, res) => res.send('You are being served'))

const options = {
  protocol: 'noflo',
  schemas: schemas,
  secure: true
}

const transport = new WebsocketProxy(options, server)

server.listen(port, () => {
  console.log('Listening on port %s', port)
})
 ```

### Chrome

Meant to be used within chrome content scripts (not used yet)

 ```js
import ChromeTransport from 'chix-transport/chrome'

const options = {
  protocol: 'noflo',
  schemas: schemas
}

const transport = new ChromeTransport(options)
 ```
