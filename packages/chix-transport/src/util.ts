import 'colors'
import {render} from 'prettyjson'
import * as _s from 'underscore.string'

const host = 'Flowhub'

// order of appearance
const order = ['graph', 'id', 'name', 'component', 'library', 'main']

export function printmsg(logger: any, data: any, dir: string, client: string) {
  let key
  const pp: {[key: string]: any} = {}
  const s = []

  for (key in data.payload) {
    if (data.payload.hasOwnProperty(key)) {
      // what to pp
      if (typeof data.payload[key] === 'object') {
        //  if (Object.keys(data.payload[key]).length > 0) {
        pp[key] = data.payload[key]
        //  }
        // larger text pp
      } else if (data.payload[key].length > 36) {
        pp[key] = data.payload[key]
      } else {
        // on the same line
        s.push({name: key, value: data.payload[key]})
      }
    }
  }

  s.sort((a, b) => {
    const posA = order.indexOf(a.name)
    const posB = order.indexOf(b.name)
    return posA < posB || posB === -1 ? -1 : 1
  })

  const d: string[] = []

  s.forEach((x) => {
    d.push(_s.rpad((x.name.bold as any).green + ': ' + x.value, 30))
  })

  let extra = render(pp)
  extra = '\n' + extra || ''
  extra = extra.replace(/\n/gm, '\n' + _s.repeat(' ', 38))

  if (
    data.protocol === 'component' &&
    (data.command === 'component' || data.command === 'source')
  ) {
    // be a bit more silent for component
    extra = ''
  }

  logger.debug(
    host,
    '%s %s %s | %s %s',
    dir.blue,
    _s.lpad((client as any).bold, 16),
    _s.rpad(data.protocol.blue + ':' + data.command.cyan, 20),
    d.join(' '),
    extra
  )
}
