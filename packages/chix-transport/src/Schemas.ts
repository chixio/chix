import {createSchema} from 'json-gate'

export type SchemaKind = 'input' | 'output'

export type Command = {
  [key: string]: any
  __validate: () => void
}

export type SchemasType = {
  [name: string]: {
    input?: {
      [command: string]: any
    }
    output?: {
      [command: string]: any
    }
  }
}

export class Schemas {
  public map = new Map()

  public load(schemas: SchemasType) {
    Object.keys(schemas).forEach((name) => {
      const doc = schemas[name] as any

      if (doc) {
        ;['input', 'output'].forEach((type) => {
          if (doc.hasOwnProperty(type)) {
            for (const command of Object.keys(doc[type])) {
              doc[type][command].__validate = createSchema(
                doc[type][command]
              ).validate
            }
          }
        })

        this.map.set(name, doc)
      }
    })
  }

  public has(it: string) {
    return this.map.has(it)
  }

  public get(it: string) {
    return this.map.get(it)
  }

  public validate(type: string, protocol: string, command: string, data: any) {
    if (
      !this.map.get(protocol) ||
      !this.map.get(protocol).hasOwnProperty(type) ||
      !this.map.get(protocol)[type].hasOwnProperty(command)
    ) {
      return Error('Unknown protocol message: ' + protocol + ':' + command)
    }

    try {
      this.map.get(protocol)[type][command].__validate(data)
      return true
    } catch (e) {
      return e
    }
  }
}
