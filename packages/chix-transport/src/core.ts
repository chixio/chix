/* global console */

// TODO: same as used in chix-runtime
import {EventEmitter} from 'events'
import {nop, NopLogger} from './logger/nop'

export type CoreOptions = {
  logger?: any
}

/**
 * Core object
 *
 * Mainly used for a consistent logging interface
 */
export class Core extends EventEmitter {
  public logger: any
  constructor(options: CoreOptions = {}) {
    super()

    if (options.logger) {
      if (options.logger.constructor.name === 'Console') {
        this.logger = Object.create(console)
        this.logger.debug = this.logger.log
        this.logger.addLevel = nop
      } else {
        this.logger = options.logger
      }
    } else {
      this.logger = new NopLogger()
    }
  }
}
