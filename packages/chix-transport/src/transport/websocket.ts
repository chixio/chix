import * as httpServer from 'http'
import {server as WebSocketServer} from 'websocket'
import {Connection, Data} from '../types'
import {BaseTransport, BaseTransportOptions} from './base'

export type WebSocketTransportOptions = {
  catchExceptions?: boolean
  type?: 'chix' | 'noflo'
} & BaseTransportOptions

/**
 *
 * Websocket Transport
 *
 * Usage:
 *
 *  const nm = new NetworkMonitor({
 *    transport: new WebSocketTransport({ logger: console }),
 *    logger: console
 *  })
 *  nm.monitor(ioHandler)
 *
 * @params {Server} http HTTP Server
 * @params {Object} options Options
 * @params {String} options.catchExceptions Send all exceptions to UI.
 * @params {String} options.type runtime type chix
 */
export class WebSocketTransport extends BaseTransport {
  public options: WebSocketTransportOptions
  public ws: WebSocketServer
  public pendingRequests: any[] = [] // TODO: not used?

  // constructor(http, options) {
  constructor(options: WebSocketTransportOptions = {}, http?: any) {
    // TODO: http as first param is a bit weird
    super(options)

    if (!http) {
      http = httpServer.createServer((_req, res) => {
        res.writeHead(200, {
          'Content-Type': 'application/json',
        })
        res.end(
          JSON.stringify({
            connections: this.connections.length,
            pendingRequests: this.pendingRequests.length,
          })
        )
      })

      this.logger.info('wsServer', 'created http server')
    }

    this.options = options

    this.ws = new WebSocketServer({
      httpServer: http,
    })

    this.ws.on('request', (req) => {
      let conn: Connection

      try {
        conn = req.accept('noflo', req.origin)
        this.logger.info('wsServer', 'connection accepted', conn.protocol)
        console.log(conn.protocol)
      } catch (e) {
        console.log(e)
        this.logger.error('wsServer', e)
        return
      }

      this.register(conn)

      conn.on('message', (message: any) => {
        if (message.type === 'utf8') {
          const payload = JSON.parse(message.utf8Data)
          this.receive(payload, conn)
        } else {
          this.logger.warn('wsServer', 'unknown message type', message)
        }
      })

      conn.on('close', (message: any) => {
        this.logger.info('wsServer', 'connection close', message)

        this.unregister(conn)
      })
    })

    if (options.catchExceptions) {
      process.on('uncaughtException', this.uncaughtExceptionHandler)
    }
  }

  public uncaughtExceptionHandler = (error: any) => {
    // todo: send all will not work here,
    // subscription is always about a graph
    this.sendAll<any>('network', 'error', {
      message: error.toString(),
    })
  }

  public close() {
    if (this.options.catchExceptions) {
      process.off('uncaughtException', this.uncaughtExceptionHandler)
    }
    if (this.ws) {
      this.ws.shutDown()
    }
  }

  public noCircularJSON(data: any) {
    return JSON.stringify(data, (_key, val) => {
      if (
        val &&
        typeof val === 'object' &&
        !Array.isArray(val) &&
        val.constructor &&
        val.constructor.name !== 'Object'
      ) {
        return '[' + val.constructor.name + ']'
      }

      return val
    })
  }

  /**
   *
   * Send the response.
   *
   */
  public $send(data: Data, conn: Connection) {
    // todo not sure if the secret has to be send back as reply.
    // this.setSecret(data)

    if (data.command === 'error') {
      if (data.payload.message && data.payload.message.message) {
        data.payload.message = data.payload.message.message
      }
    }

    conn.sendUTF(this.noCircularJSON(data))
  }
}
