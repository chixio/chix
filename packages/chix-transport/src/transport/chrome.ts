import {Data} from '../types'
import {BaseTransport, BaseTransportOptions} from './base'

export type ChromeTransportOptions = {
  protocol: string
} & BaseTransportOptions

/**
 * Chrome Transport
 *
 * To be used within content scripts
 *
 */
export class ChromeTransport extends BaseTransport {
  public connected = false
  public options: ChromeTransportOptions
  public port: any

  constructor(options: ChromeTransportOptions) {
    super(options)

    options.protocol = options.protocol || 'noflo'

    this.options = options

    this.logger.info('ChromeTransport', 'Connected')

    this.connected = true
    this.emit('connected')

    this.port = (window as any).chrome.runtime.connect()

    // The listening part
    window.addEventListener('message', this.messageHandler)
  }
  public messageHandler = (event: any) => {
    /*
       if (event.source !== window) {
       return
       }
       */

    if (event.type === 'message' && event.data.protocol && event.data.command) {
      if (!this.isRegistered(event.source)) {
        this.register(event.source)
      }

      this.receive(event.data, event.source)
    }
  }

  public close() {
    window.removeEventListener('message', this.messageHandler)
  }

  /**
   *
   * Send the response.
   *
   */
  public $send(data: Data /* , conn */) {
    this.setSecret(data)
    this.port.postMessage(data)
    // else {
    //  this.logger.warn('ChromeTransport:', 'connection already closed')
  }
}
