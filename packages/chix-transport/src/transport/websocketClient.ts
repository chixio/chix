import WebSocketClient from 'websocket'
import {Connection, Data} from '../types'
import {BaseTransport, BaseTransportOptions} from './base'

export type WebSocketClientTransportOptions = {
  host?: string
  port?: string
  mount?: string
  protocol?: string
  secure?: boolean
} & BaseTransportOptions

/**
 *
 * Websocket Client Transport to be used on the server
 *
 * Usage:
 *
 *  const nm = new NetworkMonitor({
 *    transport: new WebSocketClientTransport({ logger: console }),
 *    logger: console
 *  })
 *  nm.monitor(ioHandler)
 *
 *  Note: sendAll just keeps on working, although we are always
 *        just one connection, we could register to several server though.
 *
 * @params {Object} options Options
 * @params {String} options.host ws hostname
 * @params {String} options.port ws port
 * @params {String} options.mount ws `mount` point
 * @params {String} options.protocol noflo
 */
export class WebSocketClientTransport extends BaseTransport {
  public ws: WebSocketClient.client
  public options: WebSocketClientTransportOptions

  constructor(options: WebSocketClientTransportOptions) {
    super(options)

    if (!options.host) {
      throw Error('Must provide a hostname')
    }

    if (!options.port) {
      throw Error('Must provide a portname')
    }

    options.protocol = options.protocol || 'noflo'

    this.options = options

    const url = [
      options.secure ? 'wss://' : 'ws://',
      options.host,
      ':',
      options.port,
      '/',
      options.mount || '',
    ].join('')

    this.ws = new WebSocketClient.client()
    this.ws.on('connect', (conn: any) => {
      this.logger.info('wsClient', 'WebSocket client connected', conn.protocol)

      // self.register(conn)

      conn.on('message', (message: any) => {
        if (message.type === 'utf8') {
          const payload = JSON.parse(message.utf8Data)
          this.receive(payload, conn)
        } else {
          this.logger.warn('wsClient', 'unknown message type', message)
        }
      })

      conn.on('error', (error: Error) => {
        this.logger.error('Connection Error: ' + error.toString())
      })

      conn.on('close', (message: any) => {
        this.logger.info('wsClient', 'connection close', message)

        // self.unregister(conn)
      })
    })

    this.ws.on('connectFailed', (error: Error) => {
      this.logger.warn('wsClient', 'Connect Error: ' + error.toString())
    })

    this.ws.connect(url, options.protocol)
  }

  /**
   *
   * Send the response.
   *
   */
  public $send(data: Data, conn: Connection): void {
    this.setSecret(data)

    conn.sendUTF(JSON.stringify(data))
  }

  public close() {
    if (this.ws) {
      this.ws.abort()
    }
  }
}
