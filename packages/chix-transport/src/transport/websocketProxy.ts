import {router as WebSocketRouter, server as WebSocketServer} from 'websocket'
import {Connection, Data} from '../types'
import {printmsg} from '../util'
import {BaseTransport, BaseTransportOptions} from './base'

export type WebSocketProxyTransportOptions = {} & BaseTransportOptions

/**
 *
 * Websocket Transport Proxy
 *
 * Can be used standalone.
 *
 * The proxied result is available at /proxy
 *
 * UI talks to /
 *
 * Client who want's to proxy talks to /proxy
 *
 * Usage:
 *
 *  new WebSocketProxyTransport({ logger: console }),
 *
 * @params {Server} http HTTP Server
 * @params {Object} options Options
 * @params {String} options.catchExceptions Send all exceptions to UI.
 * @params {String} options.type runtime type chix
 */
export class WebSocketProxyTransport extends BaseTransport {
  public static create(http: any, options: WebSocketProxyTransportOptions) {
    return new WebSocketProxyTransport(http, options)
  }

  public ws: WebSocketServer
  public router: WebSocketRouter
  public pendingRequests: any[] = [] // TODO: not used?

  constructor(http: any, options: WebSocketProxyTransportOptions) {
    super(options)

    this.options = options

    this.ws = new WebSocketServer({
      httpServer: http,
    })

    this.router = new WebSocketRouter()
    this.router.attachServer(this.ws)

    // The UI talking
    this.router.mount('*', 'noflo', (req) => {
      const conn = req.accept(req.origin)

      this.logger.info('wsProxyServer', 'connection accepted from', req.origin)

      this.register(conn, conn.protocol)

      // send to the other mount point subscribers
      conn.on('message', (message: any) => {
        if (message.type === 'utf8') {
          const data = JSON.parse(message.utf8Data)

          this.logger.debug(
            'wsProxyServer',
            'received %s:%s',
            data.protocol,
            data.command
          )

          const all = this.getAll('noflo-runtime')

          if (all.length === 0) {
            this.logger.debug(
              'wsProxyServer',
              'request ignored, client list empty'
            )
          } else {
            // Send this info to all graphRunners
            all.forEach((client) => {
              // there is info in the schema.
              printmsg(this.logger, data, '  → ', client.remoteAddress)

              throw Error('disabled until used')

              /*
                this.send(data, client)
              */
            })
          }

          // we don't receive.
          // this.receive(payload, conn)
        } else {
          this.logger.warn(
            'wsProxyServer',
            'unknown message type',
            conn.protocol,
            message
          )
        }
      })

      conn.on('close', (message) => {
        this.logger.info(
          'wsProxyServer',
          `connection from ${conn.remoteAddress} closed`,
          message
        )

        this.unregister(conn, conn.protocol)
      })
    })

    // The Runtime within the browser talking
    this.router.mount('*', 'noflo-runtime', (req) => {
      const conn = req.accept(req.origin)

      // Graph Runners.
      this.register(conn, conn.protocol)

      this.logger.info(
        'wsProxyServer',
        'connection accepted from',
        conn.remoteAddress
      )

      // send to the other mountpoint subscribers
      // This will work, but with multiple it will not ofcourse.
      // Anyway let's just send every message from every client back.
      conn.on('message', (message: any) => {
        if (message.type === 'utf8') {
          const data = JSON.parse(message.utf8Data)

          // Send this info to all graphRunners
          this.getAll('noflo').forEach((client) => {
            if (data.command !== 'component') {
              printmsg(this.logger, data, '←   ', client.remoteAddress)
            }
            throw Error('disabled until used.')
            /*
                    this.send(data, client)
                */
          })

          // we don't receive.
          // this.receive(payload, conn)
        } else {
          this.logger.warn(
            'wsProxyServer',
            'unknown message type',
            conn.protocol,
            message
          )
        }
      })

      conn.on('close', (message) => {
        this.logger.info(
          'wsProxyServer',
          `connection from ${conn.remoteAddress} closed`,
          message
        )

        this.unregister(conn, conn.protocol)
      })
    })
  }

  public close() {
    if (this.router) {
      this.router.detachServer()
    }

    this.ws.shutDown()
  }

  /**
   *
   * Send the response.
   *
   */
  public $send(data: Data, conn: Connection) {
    conn.sendUTF(JSON.stringify(data))
  }
}
