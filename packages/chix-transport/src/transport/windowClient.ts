import {Connection, Data} from '../types'
import {BaseTransport, BaseTransportOptions} from './base'

export type WindowClientTransportOptions = {
  protocol: 'chix' | 'noflo'
  targetIFrameSelector: string
} & BaseTransportOptions

/**
 * Window Client Transport
 *
 * Connects to a runtime running in an iframe
 */
export class WindowClientTransport extends BaseTransport {
  public connected: boolean = false
  public targetWindow: Window

  constructor(options: WindowClientTransportOptions) {
    super(options)

    const element = document.querySelector(options.targetIFrameSelector)

    if (!(element instanceof HTMLIFrameElement && element.contentWindow)) {
      throw Error('Unable to determine content window')
    }

    this.targetWindow = element.contentWindow

    this.logger.info('windowClientTransport', 'Connected')

    this.register(this.targetWindow)

    // so instantiators can listen for the connected event after construction
    setTimeout(() => {
      this.connected = true
      this.emit('connected')
    })

    window.addEventListener('message', this.messageHandler)
  }

  public messageHandler = (event: any) => {
    if (event.source !== this.targetWindow) {
      return
    }

    if (event.type === 'message' && event.data.protocol && event.data.command) {
      this.receive(event.data, this.targetWindow)
    }
  }

  public close() {
    window.removeEventListener('message', this.messageHandler)
  }

  public $send(data: Data, _conn: Connection) {
    this.setSecret(data)

    // conn.postMessage(data, this.targetWindow)
    this.targetWindow.postMessage(this.serialize(data), '*') // this.targetWindow)
  }

  /**
   * Ensures toJSON() is called on the object.
   *
   * Which keeps the behavior the same as the other transports.
   */
  private serialize(data: any) {
    return JSON.parse(JSON.stringify(data))
  }
}
