import {Core, CoreOptions} from '../core'
import {Schemas, SchemasType} from '../Schemas'
import {Connection, Data} from '../types'

export type WithGraphId = {
  graph?: string
}

export type BaseTransportOptions = {
  schemas?: any
  role?: 'client' | 'server'
  secret?: string
  bail?: boolean
} & CoreOptions

export type IoModes = 'input' | 'output'

export type Connections = {
  [protocol: string]: Connection[]
}

export type Subscription = {
  [topic: string]: {
    [graphId: string]: Connection[]
  }
}

const errorMap: {[key: string]: string} = {
  "Error: JSON object property 'graph' is required": 'No graph specified',
}

export function friendlyMessage(message: string) {
  if (errorMap[message]) {
    return errorMap[message]
  }

  return message
}

/**
 * Base Transport
 */
export abstract class BaseTransport extends Core {
  public _subscription: Subscription = {}
  public options: BaseTransportOptions
  public receiveMode: IoModes
  public replyMode: IoModes
  public connections: Connections = {}
  public protocolSchema!: Schemas

  constructor(options: BaseTransportOptions) {
    super(options)

    this.options = options

    if (this.options.schemas) {
      this.setSchemas(this.options.schemas)
    }

    // The clients can be used both ways, but we need to
    // know what their role is, normal role for a client
    // is being the runtime, runtime will be referred to as `server` role
    // a UI or whatever commander will be referred to as `client` role
    // default role is `server`
    if (this.options.role === 'client') {
      this.receiveMode = 'output'
      this.replyMode = 'input'

      if (!this.options.secret) {
        throw Error('In client mode options requires a secret')
      }
    } else {
      this.receiveMode = 'input'
      this.replyMode = 'output'
    }
  }

  public sendAll<T = any>(
    protocol: string,
    command: string,
    data: T & WithGraphId,
    graphId?: string
  ) {
    // data would contain `graph`, topic tells us we need graph.
    // TODO: merge this with this.connections.
    let graph
    if (!graphId) {
      if (!data.graph) {
        throw Error('Could not determine graphId.')
      }
      graph = data.graph
    } else {
      graph = graphId
    }

    const topic = 'graph'

    // temporary hack to force sending all subscribers.
    const sendToAll = true

    if (this._subscription.hasOwnProperty(topic)) {
      /* DISABLED, always send to all
      if (this._subscription[topic].hasOwnProperty(graph)) {
        this._subscription[topic][graph].forEach((conn) => {
          if (command === 'error') {
            this.sendError(protocol, data, conn)
          } else {
            this._send({
              protocol,
              command,
              payload: data
            }, conn)
          }
        })

      } else if (sendToAll) {
      */
      if (sendToAll) {
        Object.keys(this._subscription[topic]).forEach((_graph) => {
          this._subscription[topic][_graph].forEach((conn) => {
            if (command === 'error') {
              this.sendError(protocol, data, conn)
            } else {
              this._send(
                {
                  protocol,
                  command,
                  payload: data,
                },
                conn
              )
            }
          })
        })
      } else {
        // For now just do not send back.
        // subgraphs are not subscribed, so just do not send info about them
        // e.g. add link will also trigger for subcomponents beneath.
        // The question is, do we need to know about those additions at all.
        // I think not..
        console.log('protocol:' + protocol, 'topic:' + topic, data)

        this.logger.warn(
          'BaseTransport',
          `\`${topic}\` property \`${graph}\` not found'`
        )
      }
    }
  }

  /**
   *
   * Internal Receive
   *
   * Emits the validated input for processing.
   *
   */
  public receive(data: Data, conn: Connection) {
    this.logger.debug('BaseTransport', 'Receive:', data)

    const ret = this.validate(
      this.receiveMode,
      data.protocol,
      data.command,
      data.payload
    )

    if (ret instanceof Error) {
      const msg = {
        message: ret.toString(),
        graph: data.payload && data.payload.graph && data.payload.graph,
      }
      this.logger.error('BaseTransport', 'Error:', msg)
      this.sendError(data.protocol || 'runtime', msg, conn)
      // this._send(msg, conn)
    } else {
      // this should be generic
      // Graph subscription
      if (data.protocol === 'graph' && data.command === 'clear') {
        this.subscribe(conn, 'graph', data.payload.id)
      } else if (data.protocol === 'network' && data.command === 'getstatus') {
        this.subscribe(conn, 'graph', data.payload.graph)
      }

      this.emit(data.protocol, data, conn)
    }
  }

  /**
   *
   * Validates incoming and outgoing messages.
   *
   */
  public validate(type: string, protocol: string, command: string, data: Data) {
    if (!this.protocolSchema) {
      throw Error('Schemas required')
    }
    return this.protocolSchema.validate(type, protocol, command, data)
  }

  public setSchemas(schemas: SchemasType) {
    this.protocolSchema = new Schemas()
    this.protocolSchema.load(schemas)
  }

  /**
   *
   * Generic method to send out the errors
   * Can be used during extension to intercept all errors.
   *
   * @param protocol
   * @param payload
   * @param conn
   */
  public sendError(protocol: string, payload: any, conn?: Connection) {
    if (payload.message) {
      if (payload.message instanceof Error) {
        payload.message = payload.message.message
      }

      payload.message = friendlyMessage(payload.message)
    }

    const msg = {
      protocol,
      command: 'error',
      payload,
    }
    this._send(msg, conn)
  }

  /**
   *
   * Internal reply method.
   *
   * Does the generic stuff then uses the send method of the transport.
   *
   */
  public reply(
    protocol: string,
    command: string,
    payload: any,
    conn?: Connection
  ) {
    /*
    if (payload === undefined) {
      throw Error('payload must not be undefined')
    }
    */

    /*
     if (data.command === 'error' && data.payload.message instanceof Error) {
     data.payload.message.toString()
     }
     */
    if (command === 'error') {
      this.sendError(protocol, payload, conn)
    } else {
      const ret = this.validate(this.replyMode, protocol, command, payload)
      if (ret instanceof Error) {
        if (this.options.bail) {
          throw ret
        } else {
          this.logger.warn('BaseTransport', ret)
          // this.sendError(protocol, ret, conn)
          // direct reply instead of send the error forward
          this.emit(
            protocol,
            {
              protocol,
              command: 'error',
              payload: ret,
            },
            conn
          )
          // this._send(msg, conn)
        }
      } else {
        const msg = {
          protocol,
          command,
          payload,
        }
        this._send(msg, conn)
      }
    }
  }

  public _send(data: Data, conn?: Connection) {
    // too many messages for component
    // solve this with something like debug: true
    if (data.command !== 'component') {
      this.logger.debug(
        'BaseTransport',
        'Send',
        `${data.protocol}:${data.command}`,
        data
      )
    }

    return this.$send(data, conn)
  }

  public $send(_data: Data, _conn?: Connection) {
    throw Error('Send method not implemented')
  }

  public getAll(protocol = 'default'): Connection[] {
    return this.connections[protocol] || []
  }

  public register(conn: Connection, protocol = 'default') {
    if (!this.connections.hasOwnProperty(protocol)) {
      this.connections[protocol] = []
    }

    this.connections[protocol].push(conn)

    this.emit('register', conn)
  }

  public isRegistered(conn: Connection, protocol = 'default') {
    return (
      this.connections[protocol] &&
      this.connections[protocol].indexOf(conn) >= 0
    )
  }

  public unregister(conn: Connection, protocol = 'default') {
    if (this.connections[protocol].indexOf(conn) === -1) {
      return
    }

    this.connections[protocol].splice(
      this.connections[protocol].indexOf(conn),
      1
    )
    this.emit('unregister', conn)
    // unsubscribe from everything
  }

  /**
   *
   * Used to subscribe a connection to a key value pair.
   *
   * 'graph': [<id>, <id> ]
   *
   * transport.subscribe(conn, 'graph', value)
   *
   * This method is called by handlers so when the monitor does a sendAll
   * the transport is able to determine who is all.
   *
   * Subscribe is done during graph creation.
   *
   */
  public subscribe(conn: Connection, key: string, value: string): void {
    if (!this._subscription.hasOwnProperty(key)) {
      this._subscription[key] = {}
    }

    if (!this._subscription[key].hasOwnProperty(value)) {
      this._subscription[key][value] = []
    }

    if (this._subscription[key][value].indexOf(conn) === -1) {
      this._subscription[key][value].push(conn)
    }

    this.emit('subscribe', conn, key, value)

    this.logger.info(
      'BaseTransport',
      'Connection subscribed to %s %s',
      key,
      value
    )
  }

  public isSubscribed(conn: Connection, key: string, value: string): boolean {
    if (!this._subscription.hasOwnProperty(key)) {
      return false
    }
    if (!this._subscription[key].hasOwnProperty(value)) {
      return false
    }
    return this._subscription[key][value].indexOf(conn) >= 0
  }

  public unsubscribe(conn: Connection, key: string, value: string): void {
    if (
      this._subscription.hasOwnProperty(key) &&
      this._subscription[key].hasOwnProperty(value)
    ) {
      this._subscription[key][value].splice(
        this._subscription[key][value].indexOf(conn),
        1
      )

      this.emit('unsubscribe', conn, key, value)

      this.logger.info(
        'BaseTransport',
        'Connection unsubscribed from %s %s',
        key,
        value
      )
    }
  }

  public setSecret(data: Data): void {
    if (this.options.secret) {
      data.secret = this.options.secret
    }
  }

  public abstract close(): void
}
