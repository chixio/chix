/* tslint:disable: only-arrow-functions */

export interface ReconnectingWebSocketOptions {
  debug?: boolean
  automaticOpen?: boolean
  reconnectInterval?: number
  maxReconnectInterval?: number
  reconnectDecay?: number
  timeoutInterval?: number
  maxReconnectAttempts?: null | number
  binaryType?: BinaryType
}

/**
 * This function generates an event that is compatible with standard
 * compliant browsers and IE9 - IE11
 *
 * This will prevent the error:
 * Object doesn't support this action
 *
 * http://stackoverflow.com/questions/19345392/why-arent-my-parameters-getting-passed-through-to-a-dispatched-event/19345563#19345563
 * @param eventName String The name that the event should use
 * @param args Object an optional object that the event will use
 */
function generateEvent(eventName: string, args?: any): any {
  const event = document.createEvent('CustomEvent')

  event.initCustomEvent(eventName, false, false, args)

  return event
}

export class ReconnectingWebSocket {
  /**
   * Whether all instances of ReconnectingWebSocket should log debug messages.
   * Setting this to true is the equivalent of setting all instances of ReconnectingWebSocket.debug to true.
   */
  public static debugAll = false

  public static CONNECTING = WebSocket.CONNECTING
  public static OPEN = WebSocket.OPEN
  public static CLOSING = WebSocket.CLOSING
  public static CLOSED = WebSocket.CLOSED

  /** The URL as resolved by the constructor. This is always an absolute URL. Read only. */
  public url: string

  /** The number of attempted reconnects since starting, or the last successful connection. Read only. */
  public reconnectAttempts: number = 0

  /**
   * The current state of the connection.
   * Can be one of: WebSocket.CONNECTING, WebSocket.OPEN, WebSocket.CLOSING, WebSocket.CLOSED
   * Read only.
   */
  public readyState = WebSocket.CONNECTING

  /**
   * A string indicating the name of the sub-protocol the server selected; this will be one of
   * the strings specified in the protocols parameter when creating the WebSocket object.
   * Read only.
   */
  public protocol: string | null = null

  public addEventListener: any
  public removeEventListener: any
  public dispatchEvent: any

  public ws: WebSocket | null = null
  public forcedClose = false
  public timedOut = false

  /** Whether this instance should log debug messages. */
  private debug = false

  /** Whether or not the websocket should attempt to connect immediately upon instantiation. */
  private automaticOpen = true

  /** The number of milliseconds to delay before attempting to reconnect. */
  private reconnectInterval = 1000

  /** The maximum number of milliseconds to delay a re-connection attempt. */
  private maxReconnectInterval = 30000

  /** The rate of increase of the reconnect delay. Allows reconnect attempts to back off when problems persist. */
  private reconnectDecay = 1.5

  /** The maximum time in milliseconds to wait for a connection to succeed before closing and retrying. */
  private timeoutInterval = 2000

  /** The maximum number of reconnection attempts to make. Unlimited if null. */
  private maxReconnectAttempts = null

  /** The binary type, possible values 'blob' or 'arraybuffer', default 'blob'. */
  private binaryType: BinaryType = 'blob'

  private eventTarget = document.createElement('div')

  private protocols: string[]

  constructor(
    url: string,
    protocols: string | string[],
    options: ReconnectingWebSocketOptions
  ) {
    if (options) {
      Object.keys(options).forEach((key) => {
        if (this.hasOwnProperty(key)) {
          ;(this as any)[key] = (options as any)[key]
        }
      })
    }

    this.url = url
    this.protocols = Array.isArray(protocols) ? protocols : [protocols]

    this.eventTarget.addEventListener('open', (event) => this.onopen(event))
    this.eventTarget.addEventListener('close', (event) => this.onclose(event))
    this.eventTarget.addEventListener('connecting', (event) =>
      this.onconnecting(event)
    )
    this.eventTarget.addEventListener('message', (event) =>
      this.onmessage(event)
    )
    this.eventTarget.addEventListener('error', (event) => this.onerror(event))

    // Expose the API required by EventTarget
    this.addEventListener = this.eventTarget.addEventListener.bind(
      this.eventTarget
    )
    this.removeEventListener = this.eventTarget.removeEventListener.bind(
      this.eventTarget
    )
    this.dispatchEvent = this.eventTarget.dispatchEvent.bind(this.eventTarget)

    if (this.automaticOpen === true) {
      this.open()
    }
  }
  /**
   * Transmits data to the server over the WebSocket connection.
   *
   * @param data a text string, ArrayBuffer or Blob to send to the server.
   */
  public send(
    data: string | ArrayBuffer | SharedArrayBuffer | Blob | ArrayBufferView
  ) {
    if (this.ws) {
      if (this.debug || ReconnectingWebSocket.debugAll) {
        console.debug('ReconnectingWebSocket', 'send', this.url, data)
      }
      return this.ws.send(data)
    }

    throw Error('INVALID_STATE_ERR : Pausing to reconnect websocket')
  }
  /**
   * Closes the WebSocket connection or connection attempt, if any.
   * If the connection is already CLOSED, this method does nothing.
   */
  public close(code: number, reason: string) {
    // Default CLOSE_NORMAL code
    if (typeof code === 'undefined') {
      code = 1000
    }
    this.forcedClose = true
    if (this.ws) {
      this.ws.close(code, reason)
    }
  }

  /**
   * Additional public API method to refresh the connection if still open (close, re-open).
   * For example, if the app suspects bad data / missed heart beats, it can try to refresh.
   */
  public refresh() {
    if (this.ws) {
      this.ws.close()
    }
  }

  /**
   * An event listener to be called when the WebSocket connection's readyState changes to OPEN;
   * this indicates that the connection is ready to send and receive data.
   */
  public onopen = function (_event: any) {}
  /** An event listener to be called when the WebSocket connection's readyState changes to CLOSED. */
  public onclose = function (_event: any) {}
  /** An event listener to be called when a connection begins being attempted. */
  public onconnecting = function (_event: any) {}
  /** An event listener to be called when a message is received from the server. */
  public onmessage = function (_event: any) {}
  /** An event listener to be called when an error occurs. */
  public onerror = function (_event: any) {}

  private open(reconnectAttempt: boolean = false) {
    try {
      this.ws = new WebSocket(this.url, this.protocols)
      this.ws.binaryType = this.binaryType

      if (reconnectAttempt) {
        if (this.maxReconnectAttempts !== null) {
          if (this.reconnectAttempts > Number(this.maxReconnectAttempts)) {
            return
          }
        }
      } else {
        this.eventTarget.dispatchEvent(generateEvent('connecting'))
        this.reconnectAttempts = 0
      }

      if (this.debug || ReconnectingWebSocket.debugAll) {
        console.debug('ReconnectingWebSocket', 'attempt-connect', this.url)
      }

      const timeout = setTimeout(() => {
        if (this.debug || ReconnectingWebSocket.debugAll) {
          console.debug('ReconnectingWebSocket', 'connection-timeout', this.url)
        }
        this.timedOut = true
        if (this.ws) {
          this.ws.close()
        }
        this.timedOut = false
      }, this.timeoutInterval)

      this.ws.onopen = (_event) => {
        clearTimeout(timeout)
        if (this.debug || ReconnectingWebSocket.debugAll) {
          console.debug('ReconnectingWebSocket', 'onopen', this.url)
        }
        this.protocol = (this.ws as WebSocket).protocol
        this.readyState = WebSocket.OPEN
        this.reconnectAttempts = 0

        const event = generateEvent('open')
        event.isReconnect = reconnectAttempt

        reconnectAttempt = false

        this.eventTarget.dispatchEvent(event)
      }

      this.ws.onclose = (event) => {
        clearTimeout(timeout)
        this.ws = null
        if (this.forcedClose) {
          this.readyState = WebSocket.CLOSED
          this.eventTarget.dispatchEvent(generateEvent('close'))
        } else {
          this.readyState = WebSocket.CONNECTING

          const e = generateEvent('connecting')
          e.code = event.code
          e.reason = event.reason
          e.wasClean = event.wasClean

          this.eventTarget.dispatchEvent(e)
          if (!reconnectAttempt && !this.timedOut) {
            if (this.debug || ReconnectingWebSocket.debugAll) {
              console.debug('ReconnectingWebSocket', 'onclose', this.url)
            }
            this.eventTarget.dispatchEvent(generateEvent('close'))
          }

          const _timeout =
            this.reconnectInterval *
            Math.pow(this.reconnectDecay, this.reconnectAttempts)

          setTimeout(
            () => {
              this.reconnectAttempts++
              this.open(true)
            },
            _timeout > this.maxReconnectInterval
              ? this.maxReconnectInterval
              : _timeout
          )
        }
      }
      this.ws.onmessage = (event) => {
        if (this.debug || ReconnectingWebSocket.debugAll) {
          console.debug(
            'ReconnectingWebSocket',
            'onmessage',
            this.url,
            event.data
          )
        }

        const e = generateEvent('message')
        e.data = event.data

        this.eventTarget.dispatchEvent(e)
      }
      this.ws.onerror = (event) => {
        if (this.debug || ReconnectingWebSocket.debugAll) {
          console.debug('ReconnectingWebSocket', 'onerror', this.url, event)
        }
        this.eventTarget.dispatchEvent(generateEvent('error'))
      }
    } catch (error) {
      this.onerror(error)
    }
  }
}
