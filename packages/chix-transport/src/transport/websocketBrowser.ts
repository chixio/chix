/* global WebSocket */
import {
  ReconnectingWebSocket,
  ReconnectingWebSocketOptions,
} from './lib/ReconnectingWebsocket'

import {Connection, Data} from '../types'
import {BaseTransport, BaseTransportOptions} from './base'

export type WebSocketBrowserTransportOptions = {
  url?: string
  protocol?: string
} & ReconnectingWebSocketOptions &
  BaseTransportOptions

/**
 *
 * Websocket Client Transport to be used in the browser
 *
 * Usage:
 *
 *  const nm = new NetworkMonitor({
 *    transport: new WebSocketBrowserTransport({ logger: console }),
 *    logger: console
 *  })
 *  nm.monitor(ioHandler)
 *
 *  Note: sendAll just keeps on working although we are always
 *        just one connection, we could register to several server though.
 */
export class WebSocketBrowserTransport extends BaseTransport {
  public connected: boolean = false
  public conn: Connection

  constructor(options: WebSocketBrowserTransportOptions) {
    super(options)

    if (!options.url) {
      throw Error('Must provide an url')
    }

    options.protocol = options.protocol || 'noflo'

    this.options = options

    const {
      url,
      protocol,
      schemas,
      role,
      secret,
      bail,
      logger,
      ...websocketOptions
    } = options

    this.conn = new ReconnectingWebSocket(url, protocol, {
      ...websocketOptions,
      automaticOpen: false,
    })

    this.conn.onopen = () => {
      this.logger.info(
        'wsClient',
        'WebSocket client connected',
        options.protocol
      )
      this.connected = true
      this.emit('connected')
      // self.register(conn)
    }

    this.conn.onmessage = (event: any) => {
      const payload = JSON.parse(event.data)

      this.receive(payload, this.conn)
    }

    this.conn.onclose = (event: any) => {
      this.logger.info('wsClient', 'connection close', event.data)
      this.connected = false
      this.emit('disconnected')
      // self.unregister(conn)
    }

    this.conn.onerror = (error: Error) => {
      this.logger.error('wsClient', 'Connection Error: ', error)
      this.emit('error', error)
    }

    this.conn.open()
  }

  public close() {
    this.conn.close()
  }

  /**
   *
   * Send the response.
   *
   */
  public $send(data: Data /* , conn */) {
    if (this.conn.readyState === WebSocket.OPEN) {
      this.setSecret(data)
      this.conn.send(JSON.stringify(data))
    } else {
      this.logger.warn(
        'WebSocketBrowserTransport:',
        'connection already closed'
      )
    }
  }
}
