import * as uuid from 'uuid'
import {Connection, Data} from '../types'
import {BaseTransport, BaseTransportOptions} from './base'

export type WindowTransportOptions = {
  protocol: 'chix' | 'noflo'
} & BaseTransportOptions

/**
 * Window Transport
 */
export class WindowTransport extends BaseTransport {
  public options: WindowTransportOptions
  public connected: boolean = false
  public originId = uuid.v4()

  constructor(options: WindowTransportOptions) {
    super(options)

    options.protocol = options.protocol || 'noflo'

    this.options = options

    this.logger.info('windowTransport', 'Connected')

    this.connected = true
    this.emit('connected')

    // The listening part
    window.addEventListener('message', this.messageHandler)

    /* TODO: handle error and disconnect (if possible)
     conn.onerror = function(error) {
     this.logger.error('wsClient', 'Connection Error: ',  error)
     }

     conn.onclose = function(event) {
     this.logger.info('wsClient', 'connection close', event.data)
     this.connected = false
     this.emit('disconnected')

     // this.unregister(conn)
     }
     */
  }

  public messageHandler = (event: any) => {
    if (this.messageWasSendByUs(event)) {
      return
    }

    // so send has easy access to the origin.
    event.source.origin = event.origin

    if (event.type === 'message' && event.data.protocol && event.data.command) {
      if (!this.isRegistered(event.source)) {
        this.register(event.source)
      }
      this.receive(event.data, event.source)
    }
  }

  public close() {
    window.removeEventListener('message', this.messageHandler)
  }

  /**
   * Send the response.
   */
  public $send(data: Data, conn: Connection) {
    this.setSecret(data)

    conn.postMessage(this.serialize(this.withOriginId(data)), conn.origin)
  }

  private messageWasSendByUs(event: any) {
    return event.data && event.data.originId === this.originId
  }

  private withOriginId(data: any) {
    return {
      ...data,
      originId: this.originId,
    }
  }
  /**
   * Ensures toJSON() is called on the object.
   *
   * Which keeps the behavior the same as the other transports.
   */
  private serialize(data: any) {
    return JSON.parse(JSON.stringify(data))
  }
}
