import {Connection, Data} from '../types'
import {BaseTransport} from './base'

/**
 *
 * Dummy Transport
 *
 * Used for testing.
 *
 * Use the emit method to trigger receives.
 *
 * Usage:
 *
 * var transport = DummyTransport()
 *
 * transport.emit('network',{
 *  protocol: 'network'
 *  command: 'start',
 *  payload: ...,
 * }, {
 *
 *   connection: ... intercept.
 *
 * })
 *
 * @params {Object} options Options
 */
export class DummyTransport extends BaseTransport {
  public $send(data: Data, conn: Connection): void {
    this.setSecret(data)
    this.emit('send', data, conn)

    // always listening for e.g. `network:start`
    this.emit(data.protocol + ':' + data.command, data, conn)
  }

  public close() {}
}
