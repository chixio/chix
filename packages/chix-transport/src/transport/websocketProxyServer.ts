import * as httpServer from 'http'
import {client as WebSocketClient, server as WebSocketServer} from 'websocket'
import {Connection, Data} from '../types'
import {printmsg} from '../util'
import {BaseTransport, BaseTransportOptions} from './base'

function defaultConductor(transport: any) {
  return {
    send: (payload: any, clientConn: any, _runtimeConn: any) =>
      transport.send(payload, clientConn),
    recv: (payload: any, runtimeConn: any, _clientConn: any) =>
      transport.send(payload, runtimeConn),
  }
}

export type Conductor = {
  send: (payload: any, clientConn: any, runtimeConn: any) => void
  recv: (payload: any, runtimeConn: any, clientConn: any) => void
}

export type WebSocketProxyServerTransportOptions = {
  url?: string
  protocol?: string
  catchExceptions?: boolean
  type?: 'chix' | 'noflo'
  conductor?: Conductor
} & BaseTransportOptions

/**
 *
 * Websocket Proxy Server Transport
 *
 * Will try to connect to a runtime server.
 * If it succeeds will start a websocket server which will listen for incomming messages.
 *
 * Each message received will directly be send to the attached runtime.
 *
 * Any messages received from the runtime will be send back directly to our client (The UI)
 *
 * @params {Server} http HTTP Server
 * @params {Object} options Options
 * @params {String} options.url Websocket url runtime.
 * @params {String} options.protocol Protocol noflo.
 * @params {String} options.catchExceptions Send all exceptions to UI.
 * @params {String} options.type runtime type chix
 * @params {String} options.conductor Message conductor Send & Receive negotiator
 * @params {HttpServer} http Http Server instance
 */
export class WebSocketProxyServerTransport extends BaseTransport {
  public static create(
    http: any,
    options: WebSocketProxyServerTransportOptions
  ) {
    return new WebSocketProxyServerTransport(options, http)
  }

  public options: WebSocketProxyServerTransportOptions = {}
  public pendingRequests: string[] = []

  // start the client first, the one connecting to noflo-nodejs

  // cannot use this transport, because it will try to .receive()
  // so just use bare
  public client = new WebSocketClient()

  public conductor: Conductor

  public conn: Connection = null
  public serverConn: Connection = null

  public server: WebSocketServer | undefined

  constructor(options: WebSocketProxyServerTransportOptions, http: any) {
    super(options)

    if (!options.url) {
      throw Error('WebSocketProxyServerTransport: runtime url option required')
    }

    this.conductor = options.conductor
      ? (options as any).conductor(this)
      : defaultConductor(this)

    if (!http) {
      http = httpServer.createServer((_req, res) => {
        res.writeHead(200, {
          'Content-Type': 'application/json',
        })
        res.end(
          JSON.stringify({
            connections: this.connections.length,
            pendingRequests: this.pendingRequests.length,
          })
        )
      })
      this.logger.info('wsServer', 'created http server')
    }

    options.protocol = options.protocol || 'noflo'

    this.options = options

    this.conn = null
    this.serverConn = null

    this.startServer(http)

    // there is always just one client
    this.client.on('connect', (conn: Connection) => {
      this.conn = conn

      this.logger.info(
        'wsClient',
        'WebSocket client connected',
        this.conn.remoteAddress
      )

      this.conn.on('message', (message: any) => {
        if (message.type === 'utf8') {
          const payload = JSON.parse(message.utf8Data)
          printmsg(this.logger, payload, '←   ', conn.remoteAddress)
          this.emit('send', payload, conn.remoteAddress)

          this.conductor.send(payload, this.serverConn, conn)
          // this.send(payload, this.serverConn)
        } else {
          this.logger.warn('wsClient', 'unknown message type', message)
        }
      })

      this.conn.on('error', (error: Error) => {
        this.logger.error('Connection Error: ' + error.toString())
      })

      this.conn.on('close', (message: Error) => {
        this.logger.info('wsClient', 'connection close', message)
        // this.unregister(conn)
      })
    })

    this.client.on('connectFailed', (error: Error) => {
      this.logger.warn('wsClient', 'Connect Error: ' + error.toString())
    })

    this.client.connect(options.url, options.protocol)
    // need the send back part
  }

  public startServer(http: any) {
    this.server = new WebSocketServer({
      httpServer: http,
    })

    this.server.on('request', (req) => {
      let conn: Connection

      try {
        conn = req.accept('noflo', req.origin)

        this.serverConn = conn
        this.logger.info('wsServer', 'connection accepted', conn.remoteAddress)
      } catch (e) {
        console.log(e)
        this.logger.error('wsServer', e)
        return
      }

      conn.on('message', (message: any) => {
        if (message.type === 'utf8') {
          const payload = JSON.parse(message.utf8Data)

          printmsg(this.logger, payload, '  → ', conn.remoteAddress)

          this.emit('recv', payload, conn.remoteAddress)
          this.conductor.recv(payload, this.conn, this.serverConn)
          // this.send(payload, this.conn)
        } else {
          this.logger.warn('wsServer', 'unknown message type', message)
        }
      })

      conn.on('close', (message: any) => {
        this.logger.info('wsServer', 'connection close', message)
      })
    })

    if (this.options.catchExceptions) {
      process.on('uncaughtException', this.uncaughtExceptionHandler)
    }
  }

  public uncaughtExceptionHandler = (error: any) => {
    this.send(
      {
        protocol: 'network',
        command: 'error',
        message: error.toString(),
      },
      this.conn
    )
  }

  public close() {
    if (this.options.catchExceptions) {
      process.off('uncaughtException', this.uncaughtExceptionHandler)
    }
    if (this.server) {
      this.server.shutDown()
    }

    if (this.client) {
      this.client.abort()
    }
  }

  public noCircularJSON(data: any) {
    return JSON.stringify(data, (_key, val) => {
      if (
        val &&
        typeof val === 'object' &&
        !Array.isArray(val) &&
        val.constructor &&
        val.constructor.name !== 'Object'
      ) {
        return '[' + val.constructor.name + ']'
      }
      return val
    })
  }

  /**
   *
   * Send the response.
   *
   */
  public send(data: Data, conn: Connection) {
    if (data.command === 'error') {
      if (data.payload.message && data.payload.message.message) {
        data.payload.message = data.payload.message.message
      }
    }
    conn.sendUTF(this.noCircularJSON(data))
  }
}
