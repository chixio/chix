export type Data = {
  secret?: string
  payload?: any

  protocol: string
  command: string

  message?: string
}

export type Connection = any
