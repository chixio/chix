import 'should'

import * as path from 'path'
import {Schemas} from '../Schemas'
import {loadSchemas} from './util'

describe('Schemas:', () => {
  let schemas: Schemas

  before(() => {
    schemas = new Schemas()
  })

  it('load()', () => {
    schemas.load(
      loadSchemas(path.join(__dirname, './schemas') + '/component.yml')
    )

    schemas.has('component').should.equal(true)
  })
  it('Properties', () => {
    Object.keys(schemas.get('component')).should.eql([
      'title',
      'description',
      'input',
      'output',
    ])
  })
  it('Install Input validators', () => {
    ;['error', 'list'].forEach((key) => {
      ;(
        typeof schemas.get('component').input[key].__validate === 'function'
      ).should.eql(true)
    })
  })
  it('Install Output validators', () => {
    ;['error', 'component'].forEach((key) => {
      ;(
        typeof schemas.get('component').output[key].__validate === 'function'
      ).should.eql(true)
    })
  })
})
