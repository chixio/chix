import * as fs from 'fs'
import {globSync} from 'glob'
import * as yaml from 'js-yaml'
import * as path from 'path'

export function loadSchemas(schemaPattern: string) {
  const files = globSync(schemaPattern)
  const schemas: {[key: string]: any} = {}

  files.forEach((file) => {
    const name = path.basename(file, '.yml')

    schemas[name] = yaml.load(fs.readFileSync(file, 'utf8'))
  })

  return schemas
}
