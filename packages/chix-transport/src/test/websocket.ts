import * as path from 'path'
import {WebSocketTransport} from '../transport/websocket'
import {WebSocketClientTransport} from '../transport/websocketClient'
import {loadSchemas} from './util'

describe.skip('Websocket Transport:', () => {
  let transport: WebSocketTransport
  let clientTransport: WebSocketClientTransport

  const connection = 'some-identifier'

  beforeEach(() => {
    const options = {
      schemas: loadSchemas(
        path.join(__dirname, './schemas') + '/component.yml'
      ),
    }

    transport = new WebSocketTransport(options)

    clientTransport = new WebSocketClientTransport({
      host: 'localhost',
      port: '3569',
      schemas: options.schemas,
    })
  })

  it('receive()', (done) => {
    // transport.register(connection)

    transport.once('send', (response) => {
      response.command.should.eql('error')
      response.payload.message.should.match(/Unknown.*message/)
      done()
    })

    // cannot just call it like this now, must make a real connection.

    clientTransport.$send(
      {
        bogus: 'data',
      } as any,
      connection
    )
  })

  describe('reply()', () => {
    it('Fail on protocol & command', (done) => {
      transport.once('send', (response) => {
        response.command.should.eql('error')
        response.payload.message.should.match(/Unknown.*message/)
        done()
      })
      transport.reply(
        'protocol',
        'command',
        {
          bogus: 'data',
        },
        connection
      )
    })

    describe('With correct protocol & command', () => {
      it('Should detect invalid data', (done) => {
        transport.once('send', (response) => {
          response.command.should.eql('error')
          response.payload.message.should.match(/not allowed/)
          done()
        })
        transport.reply(
          'component',
          'component',
          {
            bogus: 'data',
          },
          connection
        )
      })
      it('Should be able to send valid data', (done) => {
        const data = {
          name: 'a-name',
          description: 'Some description',
        }
        transport.once('send', (response) => {
          response.command.should.eql('component')
          response.payload.should.eql(data)
          done()
        })
        transport.reply('component', 'component', data, connection)
      })
    })
  })
})
