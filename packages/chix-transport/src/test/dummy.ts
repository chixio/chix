import 'should'

import * as path from 'path'
import {DummyTransport} from '../transport/dummy'
import {loadSchemas} from './util'
describe('Dummy Transport:', () => {
  let transport: DummyTransport
  const connection = 'some-identifier'

  beforeEach(() => {
    const options = {
      schemas: loadSchemas(
        path.join(__dirname, './schemas') + '/component.yml'
      ),
    }
    transport = new DummyTransport(options)
  })

  it('receive()', (done) => {
    // transport.register(connection)

    transport.once('send', (response) => {
      response.command.should.eql('error')
      response.payload.message.should.match(/Unknown.*message/)
      done()
    })

    transport.receive(
      {
        protocol: 'chix',
        command: 'someCommand',
        payload: {
          bogus: 'data',
        },
      },
      connection
    )
  })

  describe('reply()', () => {
    it('Fail on protocol & command', (done) => {
      transport.once('send', (response) => {
        response.command.should.eql('error')
        response.payload.message.should.match(/Unknown.*message/)
        done()
      })
      transport.reply(
        'protocol',
        'command',
        {
          bogus: 'data',
        },
        connection
      )
    })

    describe('With correct protocol & command', () => {
      it('Should detect invalid data', (done) => {
        transport.once('send', (response) => {
          response.command.should.eql('error')
          response.payload.message.should.match(/not allowed/)
          done()
        })
        transport.reply(
          'component',
          'component',
          {
            bogus: 'data',
          },
          connection
        )
      })
      it('Should be able to send valid data', (done) => {
        const data = {
          name: 'a-name',
          description: 'Some description',
        }
        transport.once('send', (response) => {
          response.command.should.eql('component')
          response.payload.should.eql(data)
          done()
        })
        transport.reply('component', 'component', data, connection)
      })
    })
  })
})
