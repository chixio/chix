[![Build Status](https://travis-ci.org/chixio/chix-context-provider-prompt.png)](https://travis-ci.org/chixio/chix-context-provider-prompt)

# Chiχ Prompt Context Provider

This context provider can be used to supply context for a flow from within a terminal.

Usage:
```ts
const actor = new Actor()

const contextProvider = new PromptContextProvider()

actor.setContextProvider(promptContextProvider)
```

