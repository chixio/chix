# Chix monorepo

This is the chix monorepo it contains all packages which make up the chix ecosystem.

All packages are written in typescript.

To manage the repository one can use the `chix-repo` util.

See: package/chix-repo for more information.

All packages follow the same layout:
```
packages/<package-name>/src
packages/<package-name>/package.json

packages/<package-name>/tsconfig.json # always extends ../../tsconfig.base.json
packages/<package-name>/tslint.json

# Contains the transpiled typescript code
packages/<package-name>/lib
```

Each package can be published independently using `npm-release`

```
cd packages/<package-name>/;
npm-release --patch
```

It is however more common to release all packages at once using:
```
./bin/chix-repo release --minor
```
This will republish *all* packages and keeps their versions in sync.

Each package can be build use tsc or `npm run build`
```
cd packages/<package-name>
npm run build 
```
This will build the source into `lib/`

Each packages also has a lint task available run it using `npm run lint`

To run the test for a package execute `npm run test`

# @chix/common

Most common types are available form the @chix/common package.
These types are used throughout most @chix packages.

# Lerna

Lerna is used to link packages together.

`lerna bootstrap` to link together all packages and install remaining dependencies.
`lerna link` to just link together the packages.
`lerna run build --no-bail` to build all packages.

# Cli utils

In order to use the development versions of the cli tools just make sure
they are available from within the path or just symlink them to a well known location
like e.g. `~/bin/..`

# Nixos

To step into the dev environment just execute `nix-shell` which will setup a node environment for you using the `default.nix` derivation.

It currently will automatically will make availabe yarn and lerna, it also makes available node_modules/.bin to the current path. 
