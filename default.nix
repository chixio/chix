with import <nixpkgs> {};

stdenv.mkDerivation {
  name = "node";
  buildInputs = [
    nodejs-10_x
    nodePackages.yarn
    nodePackages.lerna
    python3
  ];
  shellHook = ''
    export PATH="$PWD/node_modules/.bin/:./node_modules/.bin:$PATH"
    npm i
  '';
}
